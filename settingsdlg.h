#ifndef SETTINGSDLG_H
#define SETTINGSDLG_H

#include <QDialog>
#include "ui_settingsdlg.h"
#include "global.h"

namespace Ui {
class CSettingsDlg;
}

class CSettingsDlg : public QDialog
{
    Q_OBJECT
    
public:
    explicit CSettingsDlg(QWidget *parent = 0);
    ~CSettingsDlg();
    
private slots:
    void on_defaultSettingsBut_clicked();

public:
    Ui::CSettingsDlg *ui;
};

#endif // SETTINGSDLG_H
