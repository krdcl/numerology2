//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//		Copyright 2010. Intelliprotector.com
//		Web site: http://intelliprotector.com
//		Class: CIntelliProtector
//		Version: v3.1
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "IntelliProtector.h"
#include "declarations.h"

namespace IntelliProtectorService
{

	CIntelliProtector::CIntelliProtector(void)
	{
	}

	CIntelliProtector::~CIntelliProtector(void)
	{
	}

	// Function says is software protected or not
	// Protected: true
	// Protected: false
	BOOL CIntelliProtector::IsSoftwareProtected()
	{
 		int iProtected = 0;
 		ITP_IS_PROTECTED_V3(iProtected);

 		return iProtected == 1;
	}

	// Function says is software registered or not
	// Registered: true
	// Unregistered: false
	BOOL CIntelliProtector::IsSoftwareRegistered()
	{
 		int iType = 0;
 		ITP_GET_REGISTRATION_TYPE_V3(iType);

 		return iType == 2;
	}

	// Function returns length of the trial period in time units.
	int CIntelliProtector::GetTrialUnitsCount(int iDimension)
	{
		int iUnits = 0;
		ITP_GET_TRIAL_UNITS_V3(iUnits, iDimension);
		return iUnits;
	}

	// Function returns free evaluation days, which are left after activation (software can be used for free in these days).
	int CIntelliProtector::GetTrialDaysLeftCount()
	{
		int iDaysLeft = 0;
		ITP_GET_TRIAL_DAYS_LEFT_V3(iDaysLeft);
		return iDaysLeft;
	}

	// Function returns free evaluation period in the time units, which are left after activation.
	int CIntelliProtector::GetTrialUnitsLeftCount(int iDimension)
	{
		int iUnitsLeft = 0;
		ITP_GET_TRIAL_UNITS_LEFT_V3(iUnitsLeft, iDimension);
		return iUnitsLeft;
	}

	// Function returns free evaluation period in days.
	int CIntelliProtector::GetTrialDaysCount()
	{
		int iDaysCount = 0;
		ITP_GET_TRIAL_DAYS_V3(iDaysCount);
		return iDaysCount;
	}

	// Invokes registration window.
	void CIntelliProtector::ShowRegistrationWindow()
	{
		ITP_SHOW_REGISTRATION_WINDOW_V3;
	}

	// Says whether trial is over on not.
	BOOL CIntelliProtector::IsTrialElapsed()
	{
		if(CIntelliProtector::IsSoftwareRegistered())
			return FALSE;

		if(CIntelliProtector::GetTrialDaysLeftCount() > 0)
			return FALSE;

		// software is not registered and trial is over...
		return TRUE;
	}

	// Function returns buy now link.
	void CIntelliProtector::GetBuyNowLinkW(OUT wchar_t* pwcsLinkBuffer, int iMaxLength)
	{
		if(pwcsLinkBuffer == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalLinkBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalLinkBuffer, iMaxLength);

		ITP_GET_BUYNOW_URL_V3(pwcsLocalLinkBuffer, iMaxLength); 

		// copy local link to pchLinkBuffer
		wcscpy_s(pwcsLinkBuffer, iMaxLength, pwcsLocalLinkBuffer);

		// remove local variable
		delete[] pwcsLocalLinkBuffer;
		pwcsLocalLinkBuffer = NULL;
	}

	// Function returns buy now link.
	void CIntelliProtector::GetBuyNowLinkA(OUT char* pchLinkBuffer, int iMaxLength)
	{
		if(pchLinkBuffer == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalLinkBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalLinkBuffer, iMaxLength);

		ITP_GET_BUYNOW_URL_V3(pwcsLocalLinkBuffer, iMaxLength); 

		// convert local link to pchLinkBuffer
		WideCharToMultiByte(CP_ACP, 0, pwcsLocalLinkBuffer, -1, pchLinkBuffer, iMaxLength, NULL, NULL );

		// remove local variable
		delete[] pwcsLocalLinkBuffer;
		pwcsLocalLinkBuffer = NULL;
	}

	// Function returns license type id
	// Returns -1 if license is not available.
	int CIntelliProtector::GetLicenseType()
	{
 		int iType = 0;
		ITP_GET_LICENSE_TYPE_V3(iType);

 		return iType;
	}

	// Function returns the days, which are left before the license code expires. 
	// When 'retail' days will expires, the software will ask for new license code (renew subscription).
	int CIntelliProtector::GetLicenseExpirationDaysLeftCount()
	{
		int iRetailDaysLeft = 0;
		ITP_GET_LICENSE_EXPIRATION_DAYS_LEFT_V3(iRetailDaysLeft);
		return iRetailDaysLeft;
	}

	// Function returns the length of the 'retail' days. During these days the license code will be valid.
	int CIntelliProtector::GetLicenseExpirationDaysCount()
	{
		int iRetailDays = 0;
		ITP_GET_LICENSE_EXPIRATION_DAYS_V3(iRetailDays);
		return iRetailDays;
	}

	// Function returns the customer's name.
	void CIntelliProtector::GetCustomerNameW(OUT wchar_t* pwcsCustomerName, IN int iMaxLength)
	{
		if(pwcsCustomerName == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalNameBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalNameBuffer, iMaxLength);

		ITP_GET_CUSTOMER_NAME_V3(pwcsLocalNameBuffer, iMaxLength); 

		// copy local buffer to pchCustomerName
		wcscpy_s(pwcsCustomerName, iMaxLength, pwcsLocalNameBuffer);

		// remove local variable
		delete[] pwcsLocalNameBuffer;
		pwcsLocalNameBuffer = NULL;
	}

	// Function returns the customer's name.
	void CIntelliProtector::GetCustomerNameA(OUT char* pchCustomerName, IN int iMaxLength)
	{
		if(pchCustomerName == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalNameBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalNameBuffer, iMaxLength);

		ITP_GET_CUSTOMER_NAME_V3(pwcsLocalNameBuffer, iMaxLength); 

		// convert local buffer to pchCustomerName
		WideCharToMultiByte(CP_ACP, 0, pwcsLocalNameBuffer, -1, pchCustomerName, iMaxLength, NULL, NULL );

		// remove local variable
		delete[] pwcsLocalNameBuffer;
		pwcsLocalNameBuffer = NULL;
	}

	// Function returns the customer's email address.
	void CIntelliProtector::GetCustomerEMailW(OUT wchar_t* pwcsCustomerEMail, IN int iMaxLength)
	{
		if(pwcsCustomerEMail == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalEmailBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalEmailBuffer, iMaxLength);

		ITP_GET_CUSTOMER_EMAIL_V3(pwcsLocalEmailBuffer, iMaxLength); 

		// copy local buffer to pchCustomerEMail
		wcscpy_s(pwcsCustomerEMail, iMaxLength, pwcsLocalEmailBuffer);

		// remove local variable
		delete[] pwcsLocalEmailBuffer;
		pwcsLocalEmailBuffer = NULL;
	}

	// Function returns the customer's email address.
	void CIntelliProtector::GetCustomerEMailA(OUT char* pchCustomerEMail, IN int iMaxLength)
	{
		if(pchCustomerEMail == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalEmailBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalEmailBuffer, iMaxLength);

		ITP_GET_CUSTOMER_EMAIL_V3(pwcsLocalEmailBuffer, iMaxLength); 

		// convert local buffer to pchCustomerEMail
		WideCharToMultiByte(CP_ACP, 0, pwcsLocalEmailBuffer, -1, pchCustomerEMail, iMaxLength, NULL, NULL );

		// remove local variable
		delete[] pwcsLocalEmailBuffer;
		pwcsLocalEmailBuffer = NULL;
	}

	// Function returns the license code provided.
	void CIntelliProtector::GetLicenseCodeW(OUT wchar_t* pwcsLicense, IN int iMaxLength)
	{
		if(pwcsLicense == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalLicenseBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalLicenseBuffer, iMaxLength);

		ITP_GET_LICENSE_CODE_V3(pwcsLocalLicenseBuffer, iMaxLength); 

		// copy local buffer to pchLicense
		wcscpy_s(pwcsLicense, iMaxLength, pwcsLocalLicenseBuffer);

		// remove local variable
		delete[] pwcsLocalLicenseBuffer;
		pwcsLocalLicenseBuffer = NULL;
	}

	// Function returns the license code provided.
	void CIntelliProtector::GetLicenseCodeA(OUT char* pchLicense, IN int iMaxLength)
	{
		if(pchLicense == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalLicenseBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalLicenseBuffer, iMaxLength);

		ITP_GET_LICENSE_CODE_V3(pwcsLocalLicenseBuffer, iMaxLength); 

		// convert local buffer to pchLicense
		WideCharToMultiByte(CP_ACP, 0, pwcsLocalLicenseBuffer, -1, pchLicense, iMaxLength, NULL, NULL );

		// remove local variable
		delete[] pwcsLocalLicenseBuffer;
		pwcsLocalLicenseBuffer = NULL;
	}

	// Function register's the protected software by the license code provided.
	BOOL CIntelliProtector::RegisterSoftwareW(IN wchar_t* pwcsLicense)
	{
		int iStatus;
		ITP_REGISTER_SOFTWARE_V3(pwcsLicense, iStatus); 
		return iStatus == 1;
	}

	// Function register's the protected software by the license code provided.
	BOOL CIntelliProtector::RegisterSoftwareA(IN char* pchLicense)
    {
        int iStatus;
        int iLen = strlen(pchLicense) + 1;
        wchar_t* pwcsLocalLicense = new wchar_t[iLen];
        MultiByteToWideChar(CP_ACP, 0, pchLicense, -1, pwcsLocalLicense, iLen);
        ITP_REGISTER_SOFTWARE_V3(pwcsLocalLicense, iStatus);
        delete [] pwcsLocalLicense;
        return iStatus == 1;
	}

	// Function gets the date, when software has been activated
	void CIntelliProtector::GetCurrentActivationDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute)
	{
		int year, month, day, hour, minute;
		ITP_GET_CURRENT_ACTIVATION_DATE_V3(year, month, day, hour, minute);
		iYear = year; iMonth = month;  iDay = day; iHour = hour; iMinute = minute;
	}

	// Function gets the date, when license will be expired
	void CIntelliProtector::GetLicenseExpirationDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute)
	{
		int year, month, day, hour, minute;
		ITP_GET_LICENSE_EXPIRATION_DATE_V3(year, month, day, hour, minute);
		iYear = year; iMonth = month;  iDay = day; iHour = hour; iMinute = minute;
	}
	
	// Function gets the date, when software has been registered
	void CIntelliProtector::GetCurrentRegistrationDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute)
	{
		int year, month, day, hour, minute;
		ITP_GET_CURRENT_REGISTRATION_DATE_V3(year, month, day, hour, minute);
		iYear = year; iMonth = month;  iDay = day; iHour = hour; iMinute = minute;
	}

	// Function gets the first date, when software has been registered by current license code
	void CIntelliProtector::GetFirstRegistrationDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute)
	{
		int year, month, day, hour, minute;
		ITP_GET_FIRST_REGISTRATION_DATE_V3(year, month, day, hour, minute);
		iYear = year; iMonth = month;  iDay = day; iHour = hour; iMinute = minute;
	}

	// Function gets the date when order has been created
	void CIntelliProtector::GetOrderDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute)
	{
		int year, month, day, hour, minute;
		ITP_GET_ORDER_DATE_V3(year, month, day, hour, minute);
		iYear = year; iMonth = month;  iDay = day; iHour = hour; iMinute = minute;
	}

	// Function gets the date when order has been created
	void CIntelliProtector::GetOrderDate(OUT SYSTEMTIME* psysTyme)
	{
		if(psysTyme == NULL)
			return;

		int iYear, iMonth, iDay, iHour, iMinute;
		GetOrderDate(iYear, iMonth, iDay, iHour, iMinute);
		FillSystemTime(psysTyme, iYear, iMonth, iDay, iHour, iMinute);
	}

	// Function gets the date, when license will be expired
	void CIntelliProtector::GetLicenseExpirationDate(OUT SYSTEMTIME* psysTyme)
	{
		if(psysTyme == NULL)
			return;

		int iYear, iMonth, iDay, iHour, iMinute;
		GetLicenseExpirationDate(iYear, iMonth, iDay, iHour, iMinute);
		FillSystemTime(psysTyme, iYear, iMonth, iDay, iHour, iMinute);
	}
	
	// Function gets the date, when software has been activated
	void CIntelliProtector::GetCurrentActivationDate(OUT SYSTEMTIME* psysTyme)
	{
		if(psysTyme == NULL)
			return;

		int iYear, iMonth, iDay, iHour, iMinute;
		GetCurrentActivationDate(iYear, iMonth, iDay, iHour, iMinute);
		FillSystemTime(psysTyme, iYear, iMonth, iDay, iHour, iMinute);
	}

	// Function gets the date, when software has been registered
	void CIntelliProtector::GetCurrentRegistrationDate(OUT SYSTEMTIME* psysTyme)
	{
		if(psysTyme == NULL)
			return;

		int iYear, iMonth, iDay, iHour, iMinute;
		GetCurrentRegistrationDate(iYear, iMonth, iDay, iHour, iMinute);
		FillSystemTime(psysTyme, iYear, iMonth, iDay, iHour, iMinute);
	}

	// Function gets the first date, when software has been registered by current license code
	void CIntelliProtector::GetFirstRegistrationDate(OUT SYSTEMTIME* psysTyme)
	{
		if(psysTyme == NULL)
			return;

		int iYear, iMonth, iDay, iHour, iMinute;
		GetFirstRegistrationDate(iYear, iMonth, iDay, iHour, iMinute);
		FillSystemTime(psysTyme, iYear, iMonth, iDay, iHour, iMinute);
	}

	// Function returns version of the protection engine.
	void CIntelliProtector::GetIntelliProtectorVersionW(OUT wchar_t* pwcsVersion, IN int iMaxLength)
	{
		if(pwcsVersion == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalVersionBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalVersionBuffer, iMaxLength);

		ITP_GET_INTELLIPROTECTOR_VERSION_V3(pwcsLocalVersionBuffer, iMaxLength); 

		// copy local buffer to pchLicense
		wcscpy_s(pwcsVersion, iMaxLength, pwcsLocalVersionBuffer);

		// remove local variable
		delete[] pwcsLocalVersionBuffer;
		pwcsLocalVersionBuffer = NULL;
	}
	
	// Function returns version of the protection engine.
	void CIntelliProtector::GetIntelliProtectorVersionA(OUT char* pchVersion, IN int iMaxLength)
	{
		if(pchVersion == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalVersionBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalVersionBuffer, iMaxLength);

		ITP_GET_INTELLIPROTECTOR_VERSION_V3(pwcsLocalVersionBuffer, iMaxLength); 

		// convert local buffer to pchLicense
		WideCharToMultiByte(CP_ACP, 0, pwcsLocalVersionBuffer, -1, pchVersion, iMaxLength, NULL, NULL );

		// remove local variable
		delete[] pwcsLocalVersionBuffer;
		pwcsLocalVersionBuffer = NULL;
	}

	// Function returns count of free launches in the trial period.
	int CIntelliProtector::GetTrialLaunchesCount()
	{
		int iLaunchesCount = 0;
		ITP_GET_TRIAL_LAUNCHES_V3(iLaunchesCount);
		return iLaunchesCount;
	}

	// Function returns count of free launches in the trial period, which are left after activation.
	int CIntelliProtector::GetTrialLaunchesLeftCount()
	{
		int iLaunchesLeftCount = 0;
		ITP_GET_TRIAL_LAUNCHES_LEFT_V3(iLaunchesLeftCount);
		return iLaunchesLeftCount;
	}

	// Function gets the date, when support will be expired
	void CIntelliProtector::GetSupportExpirationDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute)
	{
		int year, month, day, hour, minute;
		ITP_GET_SUPPORT_EXPIRATION_DATE_V3(year, month, day, hour, minute);
		iYear = year; iMonth = month; iDay = day; iHour = hour; iMinute = minute;
	}

	// Function gets the date, when support will be expired
	void CIntelliProtector::GetSupportExpirationDate(OUT SYSTEMTIME* psysTyme)
	{
		if(psysTyme == NULL)
			return;

		int iYear, iMonth, iDay, iHour, iMinute;
		GetSupportExpirationDate(iYear, iMonth, iDay, iHour, iMinute);

		FillSystemTime(psysTyme, iYear, iMonth, iDay, iHour, iMinute);
	}

	// Function returns the days, which are left for the support period.
	// The license code will be valid for product versions which are released during support period.
	int CIntelliProtector::GetSupportExpirationDaysLeftCount()
	{
		int iDaysLeft = 0;
		ITP_GET_SUPPORT_EXPIRATION_DAYS_LEFT_V3(iDaysLeft);
		return iDaysLeft;
	}

	// Function returns length of the support period in days.
	int CIntelliProtector::GetSupportExpirationDaysCount()
	{
		int iDaysCount = 0;
		ITP_GET_SUPPORT_EXPIRATION_DAYS_V3(iDaysCount);
		return iDaysCount;
	}

	// Function returns the product version when support will be expired.
	void CIntelliProtector::GetSupportExpirationProductVersionW(OUT wchar_t* pwcsVersion, IN int iMaxLength)
	{
		if(pwcsVersion == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalVersionBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalVersionBuffer, iMaxLength);

		ITP_GET_SUPPORT_EXPIRATION_PRODUCT_VERSION_V3(pwcsLocalVersionBuffer, iMaxLength); 

		// copy local buffer to pchLicense
		wcscpy_s(pwcsVersion, iMaxLength, pwcsLocalVersionBuffer);

		// remove local variable
		delete[] pwcsLocalVersionBuffer;
		pwcsLocalVersionBuffer = NULL;
	}

	// Function returns the product version when support will be expired.
	void CIntelliProtector::GetSupportExpirationProductVersionA(OUT char* pchVersion, IN int iMaxLength)
	{
		if(pchVersion == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalVersionBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalVersionBuffer, iMaxLength);

		ITP_GET_SUPPORT_EXPIRATION_PRODUCT_VERSION_V3(pwcsLocalVersionBuffer, iMaxLength); 

		// convert local buffer to pchLicense
		WideCharToMultiByte(CP_ACP, 0, pwcsLocalVersionBuffer, -1, pchVersion, iMaxLength, NULL, NULL );

		// remove local variable
		delete[] pwcsLocalVersionBuffer;
		pwcsLocalVersionBuffer = NULL;
	}


	// Function returns the current product version.
	void CIntelliProtector::GetCurrentProductVersionW(OUT wchar_t* pwcsVersion, IN int iMaxLength)
	{
		if(pwcsVersion == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalVersionBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalVersionBuffer, iMaxLength);

		ITP_GET_CURRENT_PRODUCT_VERSION_V3(pwcsLocalVersionBuffer, iMaxLength); 

		// copy local buffer to pchLicense
		wcscpy_s(pwcsVersion, iMaxLength, pwcsLocalVersionBuffer);

		// remove local variable
		delete[] pwcsLocalVersionBuffer;
		pwcsLocalVersionBuffer = NULL;
	}

	// Function returns the current product version.
	void CIntelliProtector::GetCurrentProductVersionA(OUT char* pchVersion, IN int iMaxLength)
	{
		if(pchVersion == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalVersionBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalVersionBuffer, iMaxLength);

		ITP_GET_CURRENT_PRODUCT_VERSION_V3(pwcsLocalVersionBuffer, iMaxLength); 

		// convert local buffer to pchLicense
		WideCharToMultiByte(CP_ACP, 0, pwcsLocalVersionBuffer, -1, pchVersion, iMaxLength, NULL, NULL );

		// remove local variable
		delete[] pwcsLocalVersionBuffer;
		pwcsLocalVersionBuffer = NULL;
	}

	// Function returns the 'purchase renewal code' link.
	void CIntelliProtector::GetRenewalPurchaseLinkW(OUT wchar_t* pwcsLinkBuffer, int iMaxLength)
	{
		if(pwcsLinkBuffer == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalLinkBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalLinkBuffer, iMaxLength);

		ITP_GET_RENEWAL_PURCHASE_LINK_V3(pwcsLocalLinkBuffer, iMaxLength); 

		// copy local link to pchLinkBuffer
		wcscpy_s(pwcsLinkBuffer, iMaxLength, pwcsLocalLinkBuffer);

		// remove local variable
		delete[] pwcsLocalLinkBuffer;
		pwcsLocalLinkBuffer = NULL;
	}

	// Function returns the 'purchase renewal code' link.
	void CIntelliProtector::GetRenewalPurchaseLinkA(OUT char* pchLinkBuffer, int iMaxLength)
	{
		if(pchLinkBuffer == NULL)
			return;

		// The defines cannot use parameters from function directly, so creating local variable at first.
		wchar_t* pwcsLocalLinkBuffer = new wchar_t[iMaxLength];
		ZeroMemory(pwcsLocalLinkBuffer, iMaxLength);

		ITP_GET_RENEWAL_PURCHASE_LINK_V3(pwcsLocalLinkBuffer, iMaxLength); 

		// convert local link to pchLinkBuffer
		WideCharToMultiByte(CP_ACP, 0, pwcsLocalLinkBuffer, -1, pchLinkBuffer, iMaxLength, NULL, NULL );

		// remove local variable
		delete[] pwcsLocalLinkBuffer;
		pwcsLocalLinkBuffer = NULL;
	}

	// Function extends the expiration period of the license code by the renewal code.
	BOOL CIntelliProtector::RenewLicenseCodeW(IN wchar_t* pwcsRenewalCode)
	{
		int iStatus;
		ITP_RENEW_LICENSE_CODE_V3(pwcsRenewalCode, iStatus); 
		return iStatus == 1;
	}

	// Function extends the expiration period of the license code by the renewal code.
	BOOL CIntelliProtector::RenewLicenseCodeA(IN char* pchRenewalCode)
	{
		int iStatus;
		int iLen = strlen(pchRenewalCode) + 1;
		wchar_t* pwcsLocalRenewalCode = new wchar_t(iLen);
		MultiByteToWideChar(CP_ACP, 0, pchRenewalCode, -1,pwcsLocalRenewalCode, iLen);
		ITP_RENEW_LICENSE_CODE_V3(pchRenewalCode, iStatus); 
		delete [] pwcsLocalRenewalCode;
		return iStatus == 1;
	}

	// Function get the protection date
	void CIntelliProtector::GetProtectionDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute)
	{
		int year, month, day, hour, minute;
		ITP_GET_PROTECTION_DATE_V3(year, month, day, hour, minute);
		iYear = year; iMonth = month;  iDay = day; iHour = hour; iMinute = minute;
	}

	// Function get the protection date
	void CIntelliProtector::GetProtectionDate(OUT SYSTEMTIME* psysTyme)
	{
		if(psysTyme == NULL)
			return;

		int iYear, iMonth, iDay, iHour, iMinute;
		GetProtectionDate(iYear, iMonth, iDay, iHour, iMinute);
		FillSystemTime(psysTyme, iYear, iMonth, iDay, iHour, iMinute);
	}

	void CIntelliProtector::FillSystemTime(OUT SYSTEMTIME* psysTyme, int iYear, int iMonth, int iDay, int iHour, int iMinute)
	{
		if(psysTyme == NULL)
			return;

		memset(psysTyme, 0, sizeof(SYSTEMTIME));
		psysTyme->wYear = iYear;
		psysTyme->wMonth = iMonth;
		psysTyme->wDay = iDay;
		psysTyme->wHour = iHour;
		psysTyme->wMinute = iMinute;
	}

} // namespace
