#include "vbnum.h"
#include "settingsdlg.h"
#include "aboutdlg.h"

#include "regform.h"

#include "infodlg.h"

#ifdef Q_WS_WIN
#define MAC_ES_LIC
//#include "ProtectionV3/IntelliProtectorV3.h"
#include "IntelliProtector.h"
//#include "esellerate.h"
#else
#ifdef MAC_ES_LIC
#include "EWSLib.h"
#endif
#endif

#ifdef Q_WS_MAC
#include <QMacStyle>
#endif

#include "layout_painter/thumbailsettings.h"

struct sNumSettings m_GlobSettings;

QString qsGlobalDlgStyle;


//----------------------------------------------------------
CVBNum::CVBNum(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
    skipStartManual = false;
    isLoaded = false;
    inpDateFormat = false;
    mmcalcTo99 = CNumerology::MNM_11TO33;
    styleAt = 0;
#ifdef Q_OS_MAC
    qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/");
#else
    qsResPath = QCoreApplication::applicationDirPath() + QString("/images/");
#endif
    appstyle = new CAppStyle(qsResPath);
#ifndef APPSTORE
    registermode = 0;
#else
    registermode = 1;
#endif

    firstStart = false;

#ifndef APPSTORE
    nwmanager = new QNetworkAccessManager(this);
    connect(nwmanager, SIGNAL(finished(QNetworkReply*)),
        this, SLOT(replyFinished(QNetworkReply*)));
#endif

#ifndef APPSTORE
    m_publisherID = "cDiGpIjHmEoDiOfJnGdHnNcAlB";

    m_activationID = "dCoHjJpHbEmDkOhJhGcHbNfAiBjOcMnMfKiIhFeFpLoBeBoJiBaCaNgPlOiAeHiIkPiNiPpPnNoHfCjJbKpKeNcFhMdLmCkGaPdHcPpHjAlFnNhJpFeLbDmHgIfEcEnIaBhEaLhKkAaCdKiGlDbK";
#ifdef VER_PREM
    m_pReportLabel = new QLabel("Numerologist:", this);
    m_pReportLabel->resize(180, 25);

    m_pReportBox = new QComboBox(this);
    m_pReportBox->resize(160, 25);

    m_pReportBut = new QPushButton(this);
    m_pReportBut->resize(21, 25);
    connect(m_pReportBut, SIGNAL(clicked()), this, SLOT(OnReportManager()));

    connect(m_pReportBox, SIGNAL(currentIndexChanged(int)), this, SLOT(OnSelReport(int)));
#endif

    getpString(m_publisherID);
    getpString(m_activationID);

#ifdef Q_WS_WIN
    //eSellerate_InstallEngine();
    /*eSellerate_ErrorCode resultCode = eSellerate_InstallEngine();
    if (resultCode != eSellerate_SUCCESS)
    {
        //QString Msg = tr("Protection and activation service error: ") + QString::number(resultCode, 16);
        //QMessageBox::critical(NULL, tr(""), Msg, QMessageBox::Ok);
    }*/
#else
#ifdef MAC_ES_LIC
        QString engpath = QApplication::applicationDirPath()+"/EWSMacCompress.tar.gz";
		eWeb_InstallEngineFromPath(engpath.toAscii().constData());
        /*OSStatus resultCode = eWeb_InstallEngineFromPath(engpath.toAscii().constData());
        if (resultCode < 2000)
        {
            QString Msg = tr("Protection and activation service error: ") + QString::number(resultCode, 16);
            QMessageBox::critical(NULL, tr(""), Msg, QMessageBox::Ok);
            exit(-1);
        }*/
#endif
#endif

	lic = new CSecLic(this);
	lic->startLic();
#endif


#ifdef Q_WS_MAC
    new QShortcut(QKeySequence("Ctrl+M"), this, SLOT(showMinimized()));
#endif
    setWindowTitle("VeBest Numerology 7");
    m_bNameSel = false;

    loadRes(qsResPath);
    loadDB();

    blockUI = false;
    cmode = C_MODE_NUMEROLOGY;

    RecentClicked = false;

    checkCalculateData = false;

    m_pNumer = new CNumerology(this);
    m_pNumerPMatr = new CNumerologyPMatrix(this);
    m_pNumerPet = new CNumerology(this);

    m_pNumerComp = new CNumerology(this);
    m_pNumerPMatrComp = new CNumerologyPMatrix(this);

    m_pCompNumerology = new CCompNumerology(this);

    m_pChartNumerology = new CChartNumerology(this);

    m_pNumerAn = new CNumerology(this);

    m_pNumerCel = new CNumerology(this);

	hpimagepath = qsResPath + "doc/";
    m_pNumer->preloadRes(qsResPath + "doc/");
    m_pNumerCel->preloadRes(qsResPath + "doc2/");
    m_pNumerPMatr->resPath = qsResPath + "doc/";
    m_pNumerComp->preloadRes(qsResPath + "doc/");
    m_pNumerPMatrComp->resPath = qsResPath + "doc/";
    m_pCompNumerology->resPath = qsResPath + "doc/";
    //m_pChartNumerology->resPath = qsResPath;
   // m_pChartNumerology->preloadRes();

    m_pNumerAn->resPath = qsResPath + "doc/";

//	QString scrBarStyle = "QScrollBar:vertical { border: 0px; border-left: 1px solid #403935; background: qlineargradient(x1:0, y1:0, x2:1, y2:0, stop:0 #2c2420, stop:0.5 #352b27 stop:1 #312824); margin: 0px 0px 0px 0px; } \
//						   QScrollBar::handle:vertical { background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #44372f, stop:1 #4b3d34); border: none; min-height: 20px; border-radius: 4px; margin: 3px 3px 3px 3px; } \
//						   QScrollBar::handle:vertical:hover { background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #5b4a3f, stop:1 #5e4c41); } \
//						   QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical { border: none; background: transparent; height: 0px; width: 0px; subcontrol-position: top; subcontrol-origin: margin; } \
//						   QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical { background: none; }";


    //QString qsSelLabStyle = "QLabel { background: #44372f; border: none; }";
    //QString qsCtrlButStyle = "QWidget { color: #eddbb2; font-size: 12px; background: transparent; border: none; text-align: left; margin: 5px 5px 5px 10px; }";
	QString qsCtrlScrollStyle = "QWidget { background: transparent; border: none; }";
    //QString qsCtrlScrollAreaStyle = "QScrollArea { background: #2c2420; border: 1px solid #403935; } "  + scrBarStyle;
    //QString qsWVStyle = "QWidget { background-color: #FFFFFF; border: 1px solid #888888; }";

//	QString qsComboStyle = "QDateEdit QWidget { background: #2c2420; border: 1px solid #5c524c; color: #eddbb2; selection-background-color: #54443a; }\
//							QComboBox, QDateEdit { background: #2c2420; border: 1px solid #5c524c; color: #eddbb2; padding-left: 10px; }\
//						   QComboBox::drop-down, QDateEdit::drop-down { border:none; }\
//						   QComboBox::down-arrow, QDateEdit::down-arrow { image:url(" + qsResPath + "/darr.png); }\
//						   QComboBox::down-arrow:hover, QDateEdit::down-arrow:hover { image:url(" + qsResPath + "/darrh.png); }\
//						   QComboBox QAbstractItemView { background: #44372f; border: 1px solid #5c524c; color: #eddbb2; selection-background-color: #54443a; padding: 5px 5px 5px 5px; }" + scrBarStyle;

    //QString qsLineEditStyle = "QWidget { background-color: #2c2420; border: 1px solid #5c524c; color: #eddbb2;}";

//	QString qsCheckBoxStyle = "QCheckBox { background: none; color: #eddbb2; border:none; }\
//				   QCheckBox::indicator:unchecked { background: #2c2420; border: 1px solid #594f49; width: 12px; height: 12px; }\
//				   QCheckBox::indicator:checked { image:url(" + qsResPath + "/cbc.png); background: #2c2420; border: 1px solid #594f49; width: 12px; height: 12px; }";

    //QString qsMenuStyle = "QMenu { background: #2c2420; border: 1px solid #5c524c; color: #eddbb2; selection-background-color: #54443a; }";


	// Button Styles
//	QString qsPersSelButStyle = "QPushButton, QPushButton:pressed { border-image: url(" + qsResPath + "/pselbut.png); color: #eddbb2; padding-right:50px; font-size: 14px; } \
//								 QPushButton:hover { border-image: url(" + qsResPath + "/pselbuth.png); }";
	QString qsUpgradeButStyle = "QPushButton, QPushButton:pressed { border-image: url(" + qsResPath + "/upgbut.png); } \
																									  QPushButton:hover { border-image: url(" + qsResPath + "/upgbuth.png); }";
//	QString qsRecentPersStyle = "QPushButton { background: #44372f; border: 0px; border-radius: 6px; color: #eddbb2;} \
//								QPushButton:pressed { background: #2d241f; } \
//								QPushButton:hover { background: #54443a; }";

	// Menu buttons
	QString qsMenuButStyle = "QPushButton, QPushButton:pressed { background: none; border-image: url(" + qsResPath + "/menubut.png); }\
								QPushButton:hover { border-image: url(" + qsResPath + "/menubuth.png); }";
	QString qsResButStyle = "QPushButton, QPushButton:pressed { background: none; border-image: url(" + qsResPath + "/resbut.png); }\
								QPushButton:hover { border-image: url(" + qsResPath + "/resbuth.png); }";
	QString qsAboutButStyle = "QPushButton, QPushButton:pressed { background: none; border-image: url(" + qsResPath + "/infobut.png); }\
							  QPushButton:hover { border-image: url(" + qsResPath + "/infobuth.png); }";
    QString qsSaveButStyle = "QPushButton, QPushButton:pressed { background: none; border-image: url(" + qsResPath + "/savebut.png); }\
                                QPushButton:hover { border-image: url(" + qsResPath + "/savebuth.png); }";
    QString qsPrintButStyle = "QPushButton, QPushButton:pressed { background: none; border-image: url(" + qsResPath + "/printbut.png); }\
                                QPushButton:hover { border-image: url(" + qsResPath + "/printbuth.png); }";

	QString qsSaveBigButStyle = "QPushButton, QPushButton:pressed { border: none; background-image: url(" + qsResPath + "/savebbut.png); }\
					QPushButton:hover { background-image: url(" + qsResPath + "/savebbuth.png); }";
	QString qsOpenBigButStyle = "QPushButton, QPushButton:pressed { border: none; background-image: url(" + qsResPath + "/openbbut.png); }\
					QPushButton:hover { background-image: url(" + qsResPath + "/openbbuth.png); }";
	QString qsNewBigButStyle = "QPushButton, QPushButton:pressed { border: none; background-image: url(" + qsResPath + "/newbbut.png); }\
					QPushButton:hover { background-image: url(" + qsResPath + "/newbbuth.png); }";


    // Person selection
    //QString qsSelPersWidgStyle = "QWidget { background: #2c2420; border: none; }";
    //QString qsSelPersIWidgStyle = "QWidget { background: #2c2420; border: 1px solid #3e3733; }";
    //QString qsPersBkgStyle = "background-color: rgb(30, 25, 23); border: 1px solid #5c524c;";
    QString qsTitLabelStyle = "QLabel { background: none; border: none; color: #9cb62d; font: bold 12px; }";


    //setStyleSheet("QMainWindow { background-color: #1e1917; } QToolTip { background-color: #1e1917; color: #9cb62d; font: bold 12px; }");

	// Default dialogs style
	qsGlobalDlgStyle = "QPushButton { background: #44372f; border: 0px; border-radius: 6px; color: #eddbb2; min-width: 80px; min-height: 25px; } \
							   QPushButton:pressed { background: #2d241f; } \
							   QPushButton:hover { background: #54443a; }\
							   QCheckBox { background: none; color: #eddbb2; border:none; }\
							   QCheckBox::indicator:unchecked { background: #2c2420; border: 1px solid #594f49; width: 12px; height: 12px; }\
							   QCheckBox::indicator:checked { image:url(" + qsResPath + "/cbc.png); background: #2c2420; border: 1px solid #594f49; width: 12px; height: 12px; }\
							   QDialog { background: #2c2420; }\
							   QLabel { color: #9cb62d; }\
                               QLineEdit { background-color: #2c2420; border: 1px solid #5c524c; color: #eddbb2;}\
                               QTextEdit { background: #2c2420;  border: 1px solid #5c524c; color: #eddbb2; }";

    QString qsSULabelStyle = "QLabel { background: none; border: none; color: #eddbb2; font: 24px; }";



#ifndef APPSTORE
	m_pPurchasePRO = new QPushButton("", this);
	m_pPurchasePRO->resize(180, 30);
	QObject::connect(m_pPurchasePRO, SIGNAL(clicked()), this, SLOT(OnPurchasePRO()));
    m_pPurchasePRO->setStyleSheet(qsUpgradeButStyle);
#else
#ifdef VER_FREE
    m_pPurchasePRO = new QPushButton("", this);
    m_pPurchasePRO->resize(180, 30);
    QObject::connect(m_pPurchasePRO, SIGNAL(clicked()), this, SLOT(OnPurchasePRO()));
    m_pPurchasePRO->setStyleSheet(qsUpgradeButStyle);
#endif
#endif


    m_pGView = new QWidget(this);
    //m_pGView->setStyleSheet(qsWVStyle);
    m_pCtrlScrollArea = new QScrollArea(this);
    m_pCtrlScrollAreaLine = new QLabel(m_pCtrlScrollArea);
    m_pCtrlScrollAreaLine->resize(35, 1);
    //m_pCtrlScrollArea->setStyleSheet(qsCtrlScrollAreaStyle);
	m_pCtrlScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_pCtrlScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    m_pCtrlWidget = new QWidget(this);
#ifdef VER_FREE
	m_pCtrlWidget->resize(170, C_CTRL_BUT_H*6);
#else
	m_pCtrlWidget->resize(170, C_CTRL_BUT_H*9);
#endif
    m_pCtrlScrollArea->setWidget(m_pCtrlWidget);
    m_pCtrlWidget->setStyleSheet(qsCtrlScrollStyle);
    m_pCtrlWidget->show();

    m_pCtrlSelLabel = new QLabel(m_pCtrlWidget);
    m_pCtrlSelLabel->resize(C_CTRL_BUT_W, C_CTRL_BUT_H);
    m_pCtrlSelLabel->move(0,0);
    //m_pCtrlSelLabel->setStyleSheet(qsSelLabStyle);


    m_pCtrlButNumer = new QPushButton("Numerology", m_pCtrlWidget);
    m_pCtrlButNumer->resize(C_CTRL_BUT_W, C_CTRL_BUT_H);
    //m_pCtrlButNumer->setStyleSheet(qsCtrlButStyle);
    QObject::connect(m_pCtrlButNumer, SIGNAL(clicked()), this, SLOT(OnCtrlButNumer()));
    m_pCtrlButNumer->setIconSize(QSize(64, 60));

    m_pCtrlButPMatr  = new QPushButton("Psycho Matrix", m_pCtrlWidget);
    m_pCtrlButPMatr->resize(C_CTRL_BUT_W, C_CTRL_BUT_H);
    //m_pCtrlButPMatr->setStyleSheet(qsCtrlButStyle);
    QObject::connect(m_pCtrlButPMatr, SIGNAL(clicked()), this, SLOT(OnCtrlButPMatr()));  
    m_pCtrlButPMatr->setIconSize(QSize(64, 60));

    m_pCtrlButComp = new QPushButton("Love\r\nCompatibility", m_pCtrlWidget);
    m_pCtrlButComp->resize(C_CTRL_BUT_W, C_CTRL_BUT_H);
    //m_pCtrlButComp->setStyleSheet(qsCtrlButStyle);
    QObject::connect(m_pCtrlButComp, SIGNAL(clicked()), this, SLOT(OnCtrlButComp()));
    m_pCtrlButComp->setIconSize(QSize(64, 60));

    m_pCtrlButCel = new QPushButton("Celebrities", m_pCtrlWidget);
    m_pCtrlButCel->resize(C_CTRL_BUT_W, C_CTRL_BUT_H);
    //m_pCtrlButCel->setStyleSheet(qsCtrlButStyle);
    QObject::connect(m_pCtrlButCel, SIGNAL(clicked()), this, SLOT(OnCtrlButCel()));
    m_pCtrlButCel->setIconSize(QSize(64, 60));

    m_pCtrlButPet = new QPushButton("Pet", m_pCtrlWidget);
    m_pCtrlButPet->resize(C_CTRL_BUT_W, C_CTRL_BUT_H);
    //m_pCtrlButPet->setStyleSheet(qsCtrlButStyle);
    QObject::connect(m_pCtrlButPet, SIGNAL(clicked()), this, SLOT(OnCtrlButPet()));
    m_pCtrlButPet->setIconSize(QSize(64, 60));

#ifndef VER_FREE
    m_pCtrlButChart = new QPushButton("Graphic\r\nChart", m_pCtrlWidget);
    m_pCtrlButChart->resize(C_CTRL_BUT_W, C_CTRL_BUT_H);
    //m_pCtrlButChart->setStyleSheet(qsCtrlButStyle);
    QObject::connect(m_pCtrlButChart, SIGNAL(clicked()), this, SLOT(OnCtrlButChart()));
    m_pCtrlButChart->setIconSize(QSize(64, 60));

    m_pCtrlButAnalize = new QPushButton("Name\r\nAdvisor", m_pCtrlWidget);
    m_pCtrlButAnalize->resize(C_CTRL_BUT_W, C_CTRL_BUT_H);
    //m_pCtrlButAnalize->setStyleSheet(qsCtrlButStyle);
    QObject::connect(m_pCtrlButAnalize, SIGNAL(clicked()), this, SLOT(OnCtrlButAnalize()));
    m_pCtrlButAnalize->setIconSize(QSize(64, 60));

    m_pCtrlButForecast = new QPushButton("Forecast", m_pCtrlWidget);
    m_pCtrlButForecast->resize(C_CTRL_BUT_W, C_CTRL_BUT_H);
   // m_pCtrlButForecast->setStyleSheet(qsCtrlButStyle);
    QObject::connect(m_pCtrlButForecast, SIGNAL(clicked()), this, SLOT(OnCtrlButForecast()));
    m_pCtrlButForecast->setIconSize(QSize(64, 60));
#endif

	m_pGuideBut = new QPushButton("Guide", m_pCtrlWidget);
	m_pGuideBut->resize(C_CTRL_BUT_W, C_CTRL_BUT_H);
	QObject::connect(m_pGuideBut, SIGNAL(clicked()), this, SLOT(OnCtrlButGuide()));
	m_pGuideBut->setIconSize(QSize(64, 60));


#ifndef VER_FREE
    m_pCtrlButNumer->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_NUMEROLOGY));
    m_pCtrlButPMatr->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_PMATR));
    m_pCtrlButForecast->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_FORECAST));
    m_pCtrlButComp->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_COMP));
    m_pCtrlButCel->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_CEL));
    m_pCtrlButPet->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_PET));
    m_pCtrlButChart->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_CHART));
    m_pCtrlButAnalize->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_ANALIZE));
	m_pGuideBut->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_GUIDE));
#else
    m_pCtrlButNumer->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_NUMEROLOGY));
    m_pCtrlButPMatr->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_PMATR));
    m_pCtrlButComp->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_COMP));
    m_pCtrlButCel->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_CEL));
    m_pCtrlButPet->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_PET));
	m_pGuideBut->move(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_GUIDE));
#endif

    m_pAnimCtrlSel = new QPropertyAnimation(m_pCtrlSelLabel, "geometry", this);
    m_pAnimCtrlSel->setDuration(300);
    m_pAnimCtrlSel->setEasingCurve(QEasingCurve::OutBack);


    // Numerology

    m_pInfoMF = new CInfoWidget(appstyle, m_Res.titleBar, this);
    m_pInfoLC = new CInfoWidget(appstyle, m_Res.titleBar, this);
    m_pInfoAF = new CInfoWidget(appstyle, m_Res.titleBar, this);
    m_pInfoBR = new CInfoWidget(appstyle, m_Res.titleBar, this);
    m_pInfoName = new CInfoNameWidget(appstyle, m_Res.titleBar2, this);
    m_pInfoPE = new CInfoExprPlanWidget(appstyle, m_Res.titleBar2, this);
    m_pInfoE = new CInfoEssenceWidget(appstyle, m_Res.titleBar2, this);
    connect(this, SIGNAL(updateStyle()), m_pInfoE, SLOT(updateData()));
    m_pInfoCal = new CInfoCalendarWidget(appstyle, m_Res.titleBar2, this);

    int oy = -115;
    m_pInfoMF->resize(m_pInfoMF->width(), 135);
    m_pInfoAF->resize(m_pInfoAF->width(), 135);
    m_pInfoLC->resize(m_pInfoLC->width(), 135);
    //m_pInfoMF->move(10, 122+oy);
    //m_pInfoAF->move(10, 254+oy);
    //m_pInfoLC->move(10, 386+oy);
    //m_pInfoBR->move(10, 486+oy);

    m_pInfoMF->setWindowTitle("Main Features");
    m_pInfoMF->parList.append("Life Path");
    m_pInfoMF->parList.append("Birthday");
    m_pInfoMF->parList.append("Expression (M)");
    m_pInfoMF->parList.append("Heart Desire (M)");
    m_pInfoMF->parList.append("Personality (M)");
    m_pInfoMF->parList.append("Karmic Debts");
	m_pInfoMF->enableHover = true;
	connect(m_pInfoMF, SIGNAL(itemCliked(int)), this, SLOT(MFitemCliked(int)));

    m_pInfoAF->setWindowTitle("Additional Features");
    m_pInfoAF->parList.append("Maturity");
    m_pInfoAF->parList.append("Rational Thought");
    m_pInfoAF->parList.append("Karmic Lessons");
    m_pInfoAF->parList.append("Balance");
    m_pInfoAF->parList.append("Hidden Passion");
    m_pInfoAF->parList.append("Subcons.Confid.");
	m_pInfoAF->enableHover = true;
	connect(m_pInfoAF, SIGNAL(itemCliked(int)), this, SLOT(AFitemCliked(int)));

    m_pInfoLC->setWindowTitle("Life Cycles");
    m_pInfoLC->parList.append("Age span");
    m_pInfoLC->parList.append("Challenges");
    m_pInfoLC->parList.append("Pinnacles");
    m_pInfoLC->parList.append("Personal Year");
    m_pInfoLC->parList.append("LP Periods");
    m_pInfoLC->parList.append("LP Age Span");
    m_pInfoLC->skipMNH = true;
	m_pInfoLC->enableHover = true;
	m_pInfoLC->maxHStrings = 4;
	connect(m_pInfoLC, SIGNAL(itemCliked(int)), this, SLOT(LCitemCliked(int)));

    m_pInfoBR->setWindowTitle("Bridges");
    m_pInfoBR->parList.append("Life path - Expr.");
    m_pInfoBR->parList.append("Heart D. - Pers.");
	m_pInfoBR->skipMNH = true;

    m_pInfoName->move(220, 230);
    m_pInfoName->setWindowTitle("Name calculation");

    m_pInfoPE->move(220, 230);
    m_pInfoPE->setWindowTitle("Planes of Expression");
    m_pInfoPE->parList.append("");
    m_pInfoPE->parList.append("Creative");
    m_pInfoPE->parList.append("Dual");
    m_pInfoPE->parList.append("Grounded");

    m_pInfoE->move(220, 230);
    m_pInfoE->setWindowTitle("Essence and Transit");
/*    m_pInfoE->parList.append("");
	m_pInfoE->parList.append("");
    m_pInfoE->parList.append("Creative");
    m_pInfoE->parList.append("Dual");
    m_pInfoE->parList.append("Grounded");*/

    m_pInfoCal->move(220, 350);
    m_pInfoCal->setWindowTitle("Personal numbers calendar");
    m_pInfoCal->parList.append("Years ");
    m_pInfoCal->parList.append("Months");
    m_pInfoCal->parList.append("Days");

    m_pText = new QWebView(m_pGView);
    m_pTextPMatr = new QWebView(m_pGView);
    m_pTextComp = new QWebView(m_pGView);
    m_pTextCel = new QWebView(m_pGView);
    m_pTextPet = new QWebView(m_pGView);
    m_pTextAn = new QWebView(m_pGView);
	m_pTextGuide = new QWebView(m_pGView);

    m_pText->setStyleSheet("background: white;");
    m_pTextPMatr->setStyleSheet("background: white;");
    m_pTextComp->setStyleSheet("background: white;");
    m_pTextCel->setStyleSheet("background: white;");
    m_pTextPet->setStyleSheet("background: white;");
    m_pTextAn->setStyleSheet("background: white;");
	m_pTextGuide->setStyleSheet("background: white;");


    //m_pTextChart->setHtml("<img src='" + QApplication::applicationDirPath() + "/images/girl.png' width=100%>");
    //m_pTextChart->show();
    //m_pTextChart->load(QApplication::applicationDirPath() + "/images/girl.png");
    //m_pTextChart->move(-1000, -1000);
    //m_pTextChart->hide();
    //m_pTextChart->setShown(true);

    m_pText->move(1,1);
    m_pTextPMatr->move(1,1);
    m_pTextComp->move(1,1);
    m_pTextCel->move(1,1);
    m_pTextPet->move(1,1);
    m_pTextAn->move(1,1);
	m_pTextGuide->move(1,1);


	int globpx=8;
    int animWMoveDuration = 500;
    int animWMoveInOutDelta = 200;
    QEasingCurve animBCurve(QEasingCurve::OutBack);
    animBCurve.setOvershoot(0.5);
    QPropertyAnimation *anim;
    m_pAnimGrpSetNumer = new QParallelAnimationGroup(this);
    anim = new QPropertyAnimation(m_pInfoMF, "pos", m_pAnimGrpSetNumer);
    anim->setDuration(animWMoveDuration);
    anim->setEasingCurve(QEasingCurve::OutBack);
    anim->setEndValue(QPoint(globpx, 125+oy));
    m_pAnimGrpSetNumer->addAnimation(anim);

    anim = new QPropertyAnimation(m_pInfoAF, "pos", m_pAnimGrpSetNumer);
    anim->setDuration(animWMoveDuration);
    anim->setEasingCurve(QEasingCurve::OutBack);
    anim->setEndValue(QPoint(globpx, 267+oy));
    m_pAnimGrpSetNumer->addAnimation(anim);

    anim = new QPropertyAnimation(m_pInfoLC, "pos", m_pAnimGrpSetNumer);
    anim->setDuration(animWMoveDuration);
    anim->setEasingCurve(QEasingCurve::OutBack);
    anim->setEndValue(QPoint(globpx, 409+oy));
    m_pAnimGrpSetNumer->addAnimation(anim);

    anim = new QPropertyAnimation(m_pInfoBR, "pos", m_pAnimGrpSetNumer);
    anim->setDuration(animWMoveDuration);
    anim->setEasingCurve(QEasingCurve::OutBack);
    anim->setEndValue(QPoint(globpx, 552+oy));
    m_pAnimGrpSetNumer->addAnimation(anim);


    m_pAnimGrpResNumer = new QParallelAnimationGroup(this);
    anim = new QPropertyAnimation(m_pInfoMF, "pos", m_pAnimGrpResNumer);
    anim->setDuration(animWMoveDuration/3);
    anim->setEasingCurve(QEasingCurve::InBack);
    anim->setEndValue(QPoint(-300, 125+oy));
    m_pAnimGrpResNumer->addAnimation(anim);

    anim = new QPropertyAnimation(m_pInfoAF, "pos", m_pAnimGrpResNumer);
    anim->setDuration(animWMoveDuration/3);
    anim->setEasingCurve(QEasingCurve::InBack);
    anim->setEndValue(QPoint(-300, 267+oy));
    m_pAnimGrpResNumer->addAnimation(anim);

    anim = new QPropertyAnimation(m_pInfoLC, "pos", m_pAnimGrpResNumer);
    anim->setDuration(animWMoveDuration/3);
    anim->setEasingCurve(QEasingCurve::InBack);
    anim->setEndValue(QPoint(-300, 409+oy));
    m_pAnimGrpResNumer->addAnimation(anim);

    anim = new QPropertyAnimation(m_pInfoBR, "pos", m_pAnimGrpResNumer);
    anim->setDuration(animWMoveDuration/3);
    anim->setEasingCurve(QEasingCurve::InBack);
    anim->setEndValue(QPoint(-300, 552+oy));
    m_pAnimGrpResNumer->addAnimation(anim);

    m_pAnimGrpSetNumerName = new QPropertyAnimation(m_pInfoName, "pos", this);
    m_pAnimGrpSetNumerName->setDuration(animWMoveDuration+animWMoveInOutDelta);
    m_pAnimGrpSetNumerName->setEasingCurve(animBCurve);
    //QObject::connect(m_pAnimGrpSetNumerName, SIGNAL(finished()), this, SLOT(OnFinishedSU()));
#ifdef Q_WS_MAC
    QTimer::singleShot(2100, this, SLOT(OnFinishedSU()));
#else
	QTimer::singleShot(2600, this, SLOT(OnFinishedSU()));
#endif

    m_pAnimGrpResNumerName = new QPropertyAnimation(m_pInfoName, "pos", this);
    m_pAnimGrpResNumerName->setDuration(animWMoveDuration-animWMoveInOutDelta);
    m_pAnimGrpResNumerName->setEasingCurve(QEasingCurve::Linear);

    m_pAnimGrpSetNumerPE = new QPropertyAnimation(m_pInfoPE, "pos", this);
    m_pAnimGrpSetNumerPE->setDuration(animWMoveDuration+animWMoveInOutDelta);
    m_pAnimGrpSetNumerPE->setEasingCurve(animBCurve);

    m_pAnimGrpResNumerPE = new QPropertyAnimation(m_pInfoPE, "pos", this);
    m_pAnimGrpResNumerPE->setDuration(animWMoveDuration-animWMoveInOutDelta);
    m_pAnimGrpResNumerPE->setEasingCurve(QEasingCurve::Linear);

    m_pAnimNWGeom = new QPropertyAnimation(m_pGView, "geometry", this);
    m_pAnimNWGeom->setDuration(animWMoveDuration-animWMoveInOutDelta);
    m_pAnimNWGeom->setEasingCurve(QEasingCurve::Linear);

    // P.Matrix
    m_pInfoPM = new CInfoMatrWidget(appstyle, m_Res.titleBar2, this);
	m_pInfoPM->setWindowTitle("Psycho Matrix");
	connect(m_pInfoPM, SIGNAL(itemCliked(int)), this, SLOT(PMitemCliked(int)));
	m_pInfoPM->enableHover = true;

    m_pAnimGrpSetMatr = new QPropertyAnimation(m_pInfoPM, "pos", this);
    m_pAnimGrpSetMatr->setDuration(animWMoveDuration+animWMoveInOutDelta);
    m_pAnimGrpSetMatr->setEasingCurve(animBCurve);

    m_pAnimGrpResMatr = new QPropertyAnimation(m_pInfoPM, "pos", this);
    m_pAnimGrpResMatr->setDuration(animWMoveDuration-animWMoveInOutDelta);
    m_pAnimGrpResMatr->setEasingCurve(QEasingCurve::Linear);

    // Forecast
    m_pAnimGrpSetNumerE = new QPropertyAnimation(m_pInfoE, "pos", this);
    m_pAnimGrpSetNumerE->setDuration(animWMoveDuration+animWMoveInOutDelta);
    m_pAnimGrpSetNumerE->setEasingCurve(animBCurve);

    m_pAnimGrpResNumerE = new QPropertyAnimation(m_pInfoE, "pos", this);
    m_pAnimGrpResNumerE->setDuration(animWMoveDuration-animWMoveInOutDelta);
    m_pAnimGrpResNumerE->setEasingCurve(QEasingCurve::Linear);

    m_pAnimGrpSetNumerCal = new QPropertyAnimation(m_pInfoCal, "pos", this);
    m_pAnimGrpSetNumerCal->setDuration(animWMoveDuration+animWMoveInOutDelta);
    m_pAnimGrpSetNumerCal->setEasingCurve(animBCurve);

    m_pAnimGrpResNumerCal = new QPropertyAnimation(m_pInfoCal, "pos", this);
    m_pAnimGrpResNumerCal->setDuration(animWMoveDuration-animWMoveInOutDelta);
    m_pAnimGrpResNumerCal->setEasingCurve(QEasingCurve::Linear);


    // Compatibility
    m_pInfoCompPM = new CInfoCompMatrWidget(appstyle, m_Res.titleBar2, this);
    m_pInfoCMF = new CInfoWidget(appstyle, m_Res.titleBar, this);
    m_pInfoCFS = new CInfoWidget(appstyle, m_Res.titleBar, this);
    m_pInfoCLP = new CInfoWidget(appstyle, m_Res.titleBar, this);
    m_pInfoCMF->resize(m_pInfoCMF->width(), 120);
    m_pInfoCFS->resize(m_pInfoCFS->width(), 155);

	m_pInfoCompPM->setWindowTitle("Psycho Matrix");

	m_pInfoCMF->setWindowTitle("Compatibility");
    m_pInfoCMF->parList.append("Summary");
    m_pInfoCMF->parList.append("Life Path");
    m_pInfoCMF->parList.append("Birthday");
    m_pInfoCMF->parList.append("Family");
    m_pInfoCMF->parList.append("Name");
	m_pInfoCMF->skipMNH = true;

    m_pInfoCFS->setWindowTitle("Family stability");
    m_pInfoCFS->parList.append("Years together");
    m_pInfoCFS->parList.append("Home");
    m_pInfoCFS->parList.append("Spiritual");
    m_pInfoCFS->parList.append("Partner home");
    m_pInfoCFS->parList.append("Partner spiritual");
    m_pInfoCFS->parList.append("Home stability");
    m_pInfoCFS->parList.append("Spiritual stability");
	m_pInfoCFS->skipMNH = true;

    m_pInfoCLP->setWindowTitle("Compatibility details");
    m_pInfoCLP->parList.append("Life path");
    m_pInfoCLP->parList.append("Birth Day");
    m_pInfoCLP->parList.append("Name");
	m_pInfoCLP->skipMNH = true;

    m_pAnimGrpSetComp = new QParallelAnimationGroup(this);
    anim = new QPropertyAnimation(m_pInfoCMF, "pos", m_pAnimGrpSetComp);
    anim->setDuration(animWMoveDuration);
    anim->setEasingCurve(QEasingCurve::OutBack);
    anim->setEndValue(QPoint(globpx, 125+oy));
    m_pAnimGrpSetComp->addAnimation(anim);

    anim = new QPropertyAnimation(m_pInfoCFS, "pos", m_pAnimGrpSetComp);
    anim->setDuration(animWMoveDuration);
    anim->setEasingCurve(QEasingCurve::OutBack);
    anim->setEndValue(QPoint(globpx, 250+oy));
    m_pAnimGrpSetComp->addAnimation(anim);

    anim = new QPropertyAnimation(m_pInfoCLP, "pos", m_pAnimGrpSetComp);
    anim->setDuration(animWMoveDuration);
    anim->setEasingCurve(QEasingCurve::OutBack);
    anim->setEndValue(QPoint(globpx, 407+oy));
    m_pAnimGrpSetComp->addAnimation(anim);

    m_pAnimGrpResComp = new QParallelAnimationGroup(this);
    anim = new QPropertyAnimation(m_pInfoCMF, "pos", m_pAnimGrpResComp);
    anim->setDuration(animWMoveDuration/3);
    anim->setEasingCurve(QEasingCurve::InBack);
    anim->setEndValue(QPoint(-300, 125+oy));
    m_pAnimGrpResComp->addAnimation(anim);

    anim = new QPropertyAnimation(m_pInfoCFS, "pos", m_pAnimGrpResComp);
    anim->setDuration(animWMoveDuration/3);
    anim->setEasingCurve(QEasingCurve::InBack);
    anim->setEndValue(QPoint(-300, 250+oy));
    m_pAnimGrpResComp->addAnimation(anim);

    anim = new QPropertyAnimation(m_pInfoCLP, "pos", m_pAnimGrpResComp);
    anim->setDuration(animWMoveDuration/3);
    anim->setEasingCurve(QEasingCurve::InBack);
    anim->setEndValue(QPoint(-300, 407+oy));
    m_pAnimGrpResComp->addAnimation(anim);

    m_pAnimGrpSetCompPM = new QPropertyAnimation(m_pInfoCompPM, "pos", this);
    m_pAnimGrpSetCompPM->setDuration(animWMoveDuration+animWMoveInOutDelta);
    m_pAnimGrpSetCompPM->setEasingCurve(animBCurve);

    m_pAnimGrpResCompPM = new QPropertyAnimation(m_pInfoCompPM, "pos", this);
    m_pAnimGrpResCompPM->setDuration(animWMoveDuration-animWMoveInOutDelta);
    m_pAnimGrpResCompPM->setEasingCurve(QEasingCurve::Linear);

    // Pet
    m_pInfoMFPet = new CInfoWidget(appstyle, m_Res.titleBar, this);
    m_pInfoNamePet = new CInfoNameWidget(appstyle, m_Res.titleBar2, this);

    m_pInfoMFPet->resize(m_pInfoMF->width(), 300);
    m_pInfoMFPet->setWindowTitle("Main Features");
    m_pInfoMFPet->parList.append("Pet Number");
    m_pInfoMFPet->parList.append("Birthday Code");
    m_pInfoMFPet->parList.append("Day Code");

    m_pInfoNamePet->setWindowTitle("Name calculation");
    m_pInfoNamePet->resize(m_pInfoNamePet->width(),90);


    m_pInpP = new QWidget(this);

    m_pInpP->move(5, 5);
    m_pInpP->resize(200, 170);
    //m_pInpP->setStyleSheet(qsPersBkgStyle);

    QLabel *m_pInpPLable1 = new QLabel("Pet Name", m_pInpP);
    m_pInpPLable1->setStyleSheet(qsTitLabelStyle);
    m_pInpPLable1->resize(200, 25);
    m_pInpPLable1->move(0,0);
    m_pInpPLable1->setAlignment(Qt::AlignCenter);

    m_pPetName = new QLineEdit(m_pInpP);
    m_pPetName->move(20, 25);
    m_pPetName->resize(160, 25);
    //m_pPetName->setStyleSheet(qsLineEditStyle);

    QLabel *m_pInpPLable2 = new QLabel("Birth Date", m_pInpP);
    m_pInpPLable2->setStyleSheet(qsTitLabelStyle);
    m_pInpPLable2->resize(200, 25);
    m_pInpPLable2->move(0,60);
    m_pInpPLable2->setAlignment(Qt::AlignCenter);

    m_pPetBDate = new QDateEdit(m_pInpP);
    m_pPetBDate->move(20, 85);
    m_pPetBDate->resize(160, 25);
    m_pPetBDate->setCalendarPopup(true);
    //m_pPetBDate->setStyleSheet(qsComboStyle);

    //Graphic chart
    topLevelLabel = new CPainterBoard(m_pGView);
	topLevelLabel->setStyleSheet("QWidget, QLabel { background:none; border:none; }");
    comboChart = new QComboBox(m_pGView);
    //comboChart->setStyleSheet(qsComboStyle);
	comboChart->resize(150, 25);
//    comboChart->addItem("Layout 1");
//    comboChart->addItem("Layout 2");
//    comboChart->addItem("Layout 3");
//    comboChart->addItem("Layout 4");
//    comboChart->addItem("Layout 5");
//    comboChart->addItem("Layout 6");
//    comboChart->addItem("Layout 7");
//    comboChart->addItem("Layout 8");
//    comboChart->addItem("Layout 9");
//    comboChart->addItem("Layout 10");
//	comboChart->addItem("Blank V");
//	comboChart->addItem("Blank H");

    labelChart = new QLabel("Choose Layout" ,m_pGView);
    labelChart->setStyleSheet("color: #9cb62d; font: bold 12px;");

    chartEditorBut = new QPushButton("Edit", m_pGView);
    connect(chartEditorBut, SIGNAL(clicked()), this, SLOT(OnEditChart()));
    chartEditorBut->resize(60, 25);

    chartAddBut = new QPushButton("New", m_pGView);
    connect(chartAddBut, SIGNAL(clicked()), this, SLOT(OnAddChart()));
    chartAddBut->resize(60, 25);

    chartDellBut = new QPushButton("Delete", m_pGView);
    connect(chartDellBut, SIGNAL(clicked()), this, SLOT(OnDeleteChart()));
    chartDellBut->resize(60, 25);

#ifndef VER_PREM
    chartEditorBut->hide();
    chartAddBut->hide();
    chartDellBut->hide();
#endif

    //texture = new QPixmap;

    //texture->load(qsResPath + "/numl1.jpg");
    numTexture = 0;

    //topLevelLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    //topLevelLabel->setStyleSheet("QWidget, QLabel { background:transparent; border: none; }");

    xWP = this->width()+163;
    yHP = this->height()-20;

    topLevelLabel->resize(xWP, yHP);


    //texture->load(qsResPath + "/girl.png");

    //m_pChartNumerology->preloadRes(texture->toImage());

    slotGChart(0);

    connect(comboChart, SIGNAL(currentIndexChanged(int)), this, SLOT(slotGChart(int)));
    connect(m_pAnimNWGeom, SIGNAL(finished()), this, SLOT(slotResizeChart()));
    //connect(m_pAnCalcButton, SIGNAL(clicked()), this, SLOT(OnCalcAnNum()));


    //Analyzer
    //IC
    m_pInfoAnName = new CInfoNameWidget(appstyle, m_Res.titleBar2, this);

    //m_pInpAnalizer = new CInputWidget(m_Res.topResOpt2, m_Res.botRes2, m_Res.inpRes2, this);
    m_pInpAnalizer = new QWidget(this);
   // m_pInpAnalizer->setStyleSheet(qsPersBkgStyle);



    m_pInfoAnMF = new CInfoWidget(appstyle, m_Res.titleBar, this);
    m_pInfoAnMF->resize(m_pInfoMF->width(), 135);

    //m_pInfoAnMF->move(-200, 8);

    m_pInfoAnMF->setWindowTitle("Main Features");
    m_pInfoAnMF->parList.append("Date Summary");
    m_pInfoAnMF->parList.append("Name Summary");
    m_pInfoAnMF->parList.append("Vowels");
    m_pInfoAnMF->parList.append("Consonants");
    m_pInfoAnMF->parList.append("Missed");

    m_pInfoAnName->setWindowTitle("Name calculation");
	m_pInfoAnName->resize(m_pInfoAnName->width(), 100);

    m_pAnCalcButton = new QPushButton("CALCULATE", this);
   // m_pAnCalcButton->setStyleSheet(qsRecentPersStyle);
	m_pAnCalcButton->resize(100, 25);

    QObject::connect(m_pAnCalcButton, SIGNAL(clicked()), this, SLOT(OnCalcAnNum()));



    m_pMenuButton = new QPushButton("", this);
    m_pMenuButton->resize(20, 20);
    QObject::connect(m_pMenuButton, SIGNAL(clicked()), this, SLOT(OnMenu()));
	m_pMenuButton->setStyleSheet(qsMenuButStyle);

    m_pClearAll = new QPushButton("", this);
    m_pClearAll->resize(20, 20);
    QObject::connect(m_pClearAll, SIGNAL(clicked()), this, SLOT(OnResData()));
    m_pClearAll->setStyleSheet(qsResButStyle);

    m_pAbout = new QPushButton("", this);
    m_pAbout->resize(20, 20);
    QObject::connect(m_pAbout, SIGNAL(clicked()), this, SLOT(OnAbout()));
    m_pAbout->setStyleSheet(qsAboutButStyle);

    m_pSave = new QPushButton("", this);
    m_pSave->resize(20, 20);
    QObject::connect(m_pSave, SIGNAL(clicked()), this, SLOT(OnExportRTF()));
    m_pSave->setStyleSheet(qsSaveButStyle);

    m_pPrint = new QPushButton("", this);
    m_pPrint->resize(20, 20);
    QObject::connect(m_pPrint, SIGNAL(clicked()), this, SLOT(OnPrintPreview()));
    m_pPrint->setStyleSheet(qsPrintButStyle);

    //*IC
//#ifdef Q_WS_MAC
//    QMacStyle::setFocusRectPolicy(m_pPetName, QMacStyle::FocusDisabled);
//#endif
    m_pPetCalcButton = new QPushButton("CALCULATE", m_pInpP);
    m_pPetCalcButton->resize(160, 25);
    m_pPetCalcButton->move(20, 130);
    //m_pPetCalcButton->setStyleSheet(qsRecentPersStyle);
    QObject::connect(m_pPetCalcButton, SIGNAL(clicked()), this, SLOT(OnCalcPetNum()));

    //IC

    QLabel *m_pTitleEditLable1 = new QLabel("Express Numerology Analyzer", m_pInpAnalizer);
    m_pTitleEditLable1->setStyleSheet(qsTitLabelStyle);
    m_pTitleEditLable1->resize(580, 25);
    m_pTitleEditLable1->move(0,0);
    m_pTitleEditLable1->setAlignment(Qt::AlignCenter);

    QLabel *m_pTitleEditLable2 = new QLabel("Name / Title", m_pInpAnalizer);
    m_pTitleEditLable2->setStyleSheet(qsTitLabelStyle);
    m_pTitleEditLable2->resize(580, 25);
    m_pTitleEditLable2->move(0,23);
    m_pTitleEditLable2->setAlignment(Qt::AlignCenter);

    m_pTitleEdit = new QLineEdit(m_pInpAnalizer);
    m_pTitleEdit->resize(560, 22);
    m_pTitleEdit->move(20, 48);

    QLabel *m_pTitleEditLable3 = new QLabel("Date (Optional)", m_pInpAnalizer);
    m_pTitleEditLable3->setStyleSheet(qsTitLabelStyle);
    m_pTitleEditLable3->resize(580, 25);
    m_pTitleEditLable3->move(0,70);
    m_pTitleEditLable3->setAlignment(Qt::AlignCenter);

    m_pAnDate = new QDateEdit(m_pInpAnalizer);
    m_pAnDate->move(20, 96);
    m_pAnDate->resize(560, 22);
    m_pAnDate->setCalendarPopup(true);

    //m_pInpAnalizer->setStyleSheet(qsPersBkgStyle);
   // m_pTitleEdit->setStyleSheet(qsLineEditStyle);
    //m_pAnDate->setStyleSheet(qsComboStyle);

/*    m_pTitleEdit->setStyleSheet("QLineEdit { border: none; background: transparent; border-radius: 8px; padding: 1px 3px 1px 3px; } ");
#ifdef Q_WS_MAC
    QMacStyle::setFocusRectPolicy(m_pTitleEdit, QMacStyle::FocusDisabled);
#endif
    m_pAnDate->setStyleSheet("QDateEdit { border: none; background: transparent; border-radius: 8px; padding: 1px 3px 1px 3px; } \
                               QDateEdit::drop-down { border: none; } \
                               QDateEdit::down-arrow { image: url(" + qsResPath + "/darr.png); } \
                               QDateEdit::down-arrow:hover { image: url(" + qsResPath + "/darrh.png); } \
                               QDateEdit::down-arrow:on { top: 1px; left: 1px; } ");
*/
//    m_pInpAnalizer->wlabel = "Express Numerology Analyzer";
//    m_pInpAnalizer->parList.append("Name / Title");
//    m_pInpAnalizer->parListVal.append(m_pTitleEdit);
//    m_pInpAnalizer->parList.append("Date (Optional)");
//    m_pInpAnalizer->parListVal.append(m_pAnDate);

    m_pInpAnalizer->resize(m_pInfoNamePet->width(),140);
    //calcBWGeometry(C_WIDGID_PE).topLeft()
    //m_pInpAnalizer->move(calcBWGeometry(C_WIDGID_PE).topLeft());


    //*IC

    m_pAnimGrpSetPet = new QParallelAnimationGroup(this);
    anim = new QPropertyAnimation(m_pInfoMFPet, "pos", m_pAnimGrpSetPet);
    anim->setDuration(animWMoveDuration);
    anim->setEasingCurve(QEasingCurve::OutBack);
    anim->setEndValue(QPoint(globpx, 305+oy));
    m_pAnimGrpSetPet->addAnimation(anim);

    anim = new QPropertyAnimation(m_pInpP, "pos", m_pAnimGrpSetPet);
    anim->setDuration(animWMoveDuration);
    anim->setEasingCurve(QEasingCurve::OutBack);
    anim->setEndValue(QPoint(globpx, 125+oy));
    m_pAnimGrpSetPet->addAnimation(anim);

    m_pAnimGrpResPet = new QParallelAnimationGroup(this);
    anim = new QPropertyAnimation(m_pInfoMFPet, "pos", m_pAnimGrpResPet);
    anim->setDuration(animWMoveDuration/3);
    anim->setEasingCurve(QEasingCurve::InBack);
    anim->setEndValue(QPoint(-300, 305+oy));
    m_pAnimGrpResPet->addAnimation(anim);

    anim = new QPropertyAnimation(m_pInpP, "pos", m_pAnimGrpResPet);
    anim->setDuration(animWMoveDuration/3);
    anim->setEasingCurve(QEasingCurve::InBack);
    anim->setEndValue(QPoint(-300, 125+oy));
    m_pAnimGrpResPet->addAnimation(anim);


    m_pAnimGrpSetPetName = new QPropertyAnimation(m_pInfoNamePet, "pos", this);
    m_pAnimGrpSetPetName->setDuration(animWMoveDuration+animWMoveInOutDelta);
    m_pAnimGrpSetPetName->setEasingCurve(animBCurve);

    m_pAnimGrpResPetName = new QPropertyAnimation(m_pInfoNamePet, "pos", this);
    m_pAnimGrpResPetName->setDuration(animWMoveDuration-animWMoveInOutDelta);
    m_pAnimGrpResPetName->setEasingCurve(QEasingCurve::Linear);


    //IC
    m_pAnimGrpSetAnalize = new QParallelAnimationGroup(this);
    anim = new QPropertyAnimation(m_pInfoAnMF, "pos", m_pAnimGrpSetAnalize);
    anim->setDuration(animWMoveDuration);
    anim->setEasingCurve(QEasingCurve::OutBack);
    anim->setEndValue(QPoint(globpx, 123+oy));
    m_pAnimGrpSetAnalize->addAnimation(anim);

    m_pAnimGrpResAnalize = new QParallelAnimationGroup(this);
    anim = new QPropertyAnimation(m_pInfoAnMF, "pos", m_pAnimGrpResAnalize);
    anim->setDuration(animWMoveDuration/3);
    anim->setEasingCurve(QEasingCurve::InBack);
    anim->setEndValue(QPoint(-300, 123+oy));
    m_pAnimGrpResAnalize->addAnimation(anim);


    m_pAnimGrpSetAnalizeName = new QPropertyAnimation(m_pInpAnalizer, "pos", this);
    m_pAnimGrpSetAnalizeName->setDuration(animWMoveDuration+animWMoveInOutDelta);
    m_pAnimGrpSetAnalizeName->setEasingCurve(animBCurve);

    m_pAnimGrpResAnalizeName = new QPropertyAnimation(m_pInpAnalizer, "pos", this);
    m_pAnimGrpResAnalizeName->setDuration(animWMoveDuration-animWMoveInOutDelta);
    m_pAnimGrpResAnalizeName->setEasingCurve(QEasingCurve::Linear);

    m_pAnimGrpSetAnalizeNCalc = new QPropertyAnimation(m_pInfoAnName, "pos", this);
    m_pAnimGrpSetAnalizeNCalc->setDuration(animWMoveDuration+animWMoveInOutDelta);
    m_pAnimGrpSetAnalizeNCalc->setEasingCurve(animBCurve);

    m_pAnimGrpResAnalizeNCalc = new QPropertyAnimation(m_pInfoAnName, "pos", this);
    m_pAnimGrpResAnalizeNCalc->setDuration(animWMoveDuration-animWMoveInOutDelta);
    m_pAnimGrpResAnalizeNCalc->setEasingCurve(QEasingCurve::Linear);


    m_pAnimGrpSetAnalizeButton = new QPropertyAnimation(m_pAnCalcButton, "pos", this);
    m_pAnimGrpSetAnalizeButton->setDuration(animWMoveDuration+animWMoveInOutDelta);
    m_pAnimGrpSetAnalizeButton->setEasingCurve(animBCurve);

    m_pAnimGrpResAnalizeButton = new QPropertyAnimation(m_pAnCalcButton, "pos", this);
    m_pAnimGrpResAnalizeButton->setDuration(animWMoveDuration-animWMoveInOutDelta);
    m_pAnimGrpResAnalizeButton->setEasingCurve(QEasingCurve::Linear);

    //*IC




    // Today widget
    expandPanel = new CExpandWidget(appstyle, this);
    connect(this, SIGNAL(updateStyle()), expandPanel, SLOT(slotUpdate()));

    personalNumCalendar = new CInfoTodayWidget(appstyle, m_Res.todayWidg, this);
	personalNumCalendar->parList.append("Years");
	personalNumCalendar->parList.append("Months");
	personalNumCalendar->parList.append("Days");
	QObject::connect(personalNumCalendar, SIGNAL(clicked()), expandPanel, SLOT(slotExpand()));


    m_pBDateF = new QDateEdit(personalNumCalendar);
    m_pBDateF->move(30, 70);
    m_pBDateF->resize(135, 28);
    m_pBDateF->setDate(QDate::currentDate());
    m_pBDateF->setCalendarPopup(true);

    //m_pBDateF->setStyleSheet(qsComboStyle);
    QObject::connect(m_pBDateF, SIGNAL(dateChanged(QDate)), this, SLOT(slotPNC(QDate)));
	m_pBDateF->setDate(QDate::currentDate());


    m_pSelPersWidg = new QWidget(this);
    m_pSelPersWidgI_ = new QWidget(m_pSelPersWidg);
    m_pSelPersWidgI_->resize(1016, 570);

   //m_pSelPersWidg->setStyleSheet(qsSelPersWidgStyle);
   // m_pSelPersWidgI_->setStyleSheet(qsSelPersIWidgStyle);

    m_pSelPersBut = new QPushButton("ENTER YOUR NAME", this);
    startButAnim();
    m_pSelPersBut->resize(200, 80);
    //m_pSelPersBut->setStyleSheet(qsPersSelButStyle);
    QObject::connect(m_pSelPersBut, SIGNAL(clicked()), this, SLOT(OnNameSel()));

    m_pAnimNameSel = new QPropertyAnimation(m_pSelPersWidg, "pos", this);
    m_pAnimNameSel->setDuration(animWMoveDuration);

    bkg1 = new QWidget(m_pSelPersWidgI_);
    bkg1->move(8, 10);
    bkg1->resize(820, 240);
    //bkg1->setStyleSheet(qsPersBkgStyle);

    bkg2 = new QWidget(m_pSelPersWidgI_);
    bkg2->move(8, 260);
    bkg2->resize(820, 210);
    //bkg2->setStyleSheet(qsPersBkgStyle);

	QLabel *bkg1lab = new QLabel("Personal (Full Name, Short Name, Birth Date)", bkg1);
	bkg1lab->setStyleSheet(qsTitLabelStyle);
	bkg1lab->resize(bkg1->width(), 25);
	bkg1lab->move(0,0);
	bkg1lab->setAlignment(Qt::AlignCenter);

	QLabel *bkg2lab = new QLabel("Partner (Full Name, Short Name, Birth Date)", bkg2);
	bkg2lab->setStyleSheet(qsTitLabelStyle);
	bkg2lab->resize(bkg2->width(), 25);
	bkg2lab->move(0,0);
	bkg2lab->setAlignment(Qt::AlignCenter);


    //m_pInpM = new CInputWidget(m_Res.topResOpt2, m_Res.botRes2, m_Res.inpRes2, m_pSelPersWidg);
    //m_pInpO = new CInputWidget(m_Res.topRes, m_Res.botRes, m_Res.inpRes, m_pSelPersWidg);

    //m_pInpM->parList.append("Full Name");
    //m_pInpM->parList.append("Short Name");

    int pix=5, piy=30;
    int pix2=pix, piy2=piy+70;
    m_pFullNameEdit = new QLineEdit(bkg1);
    m_pFullNameEdit->move(pix, piy);
    m_pFullNameEdit->resize(250, 25);

	m_pFullNameEdit->setToolTip("Full Name");

    m_pShortNameEdit = new QLineEdit(bkg1);
    m_pShortNameEdit->move(265+pix, piy);
    m_pShortNameEdit->resize(215, 25);
    //m_pShortNameEdit->setStyleSheet(qsLineEditStyle);
	m_pShortNameEdit->setToolTip("Short Name");

    m_pBDate = new QDateEdit(bkg1);
    m_pBDate->move(490+pix, piy);
    m_pBDate->resize(120, 25);
    m_pBDate->setCalendarPopup(true);
    //m_pBDate->setStyleSheet(qsComboStyle);
	m_pBDate->setToolTip("Birth Date");
	


    m_pSex = new QComboBox(bkg1);
    m_pSex->resize(95, 25);
    m_pSex->move(620+pix, piy);
    m_pSex->addItem("Female");
    m_pSex->addItem("Male");
    //m_pSex->setStyleSheet(qsComboStyle);

    m_pInfoMFNS = new CInfoWidget(appstyle, m_Res.titleBar, bkg1);
    m_pInfoMFNS->setWindowTitle("Main Features");
    m_pInfoMFNS->parList.append("Life path");
    m_pInfoMFNS->parList.append("Karmic Debts");
    m_pInfoMFNS->parList.append("Expression (M)");
    m_pInfoMFNS->parList.append("Heart Desire (M)");
    m_pInfoMFNS->parList.append("Personality (M)");
    m_pInfoMFNS->parList.append("Karmic Lessons");
    m_pInfoMFNS->resize(m_pInfoMF->width(), 135);
    m_pInfoMFNS->move(pix2+605, piy2-30);

    m_pInfoNameNS = new CInfoNameWidget(appstyle, m_Res.titleBar2, bkg1);
    m_pInfoNameNS->setWindowTitle("Name calculation");
    m_pInfoNameNS->move(pix2, piy2-30);
    m_pInfoNameNS->m_bSymTrackEna = true;

    m_uCNC = new QCheckBox("Use Chaldean calculator", bkg1);
    m_uCNC->move(pix2, piy2+115);
    //m_uCNC->setStyleSheet(qsCheckBoxStyle);

	m_PMode = new QComboBox(bkg1);
	m_PMode->addItem("English");
	m_PMode->addItem("Universal");
	m_PMode->move(pix2+260, piy2+115-4);
	m_PMode->resize(100, 22);
	m_PMode->setCurrentIndex(0);

	m_PModeLabel = new QLabel("Phonetic:", bkg1);
	m_PModeLabel->move(pix2+200, piy2+115-4);
	m_PModeLabel->resize(60, 22);

    int pix3=5, piy3=30, piy4=piy3+40;
    m_pPFullNameEdit = new QLineEdit(bkg2);
    m_pPFullNameEdit->move(pix3, piy3);
    m_pPFullNameEdit->resize(250, 25);
    //m_pPFullNameEdit->setStyleSheet(qsLineEditStyle);
	m_pPFullNameEdit->setToolTip("Full Name");


    m_pPShortNameEdit = new QLineEdit(bkg2);
    m_pPShortNameEdit->move(265+pix3, piy3);
    m_pPShortNameEdit->resize(215, 25);
    //m_pPShortNameEdit->setStyleSheet(qsLineEditStyle);
	m_pPShortNameEdit->setToolTip("Short Name");


    m_pPBDate = new QDateEdit(bkg2);
    m_pPBDate->move(490+pix3, piy3);
    m_pPBDate->resize(120, 25);
    m_pPBDate->setCalendarPopup(true);
    //m_pPBDate->setStyleSheet(qsComboStyle);
	m_pPBDate->setToolTip("Birth Date");

    m_pInfoPMFNS = new CInfoWidget(appstyle, m_Res.titleBar, bkg2);
    m_pInfoPMFNS->setWindowTitle("Main Features");
    m_pInfoPMFNS->parList.append("Life path");
    m_pInfoPMFNS->parList.append("Karmic Debts");
    m_pInfoPMFNS->parList.append("Expression (M)");
    m_pInfoPMFNS->parList.append("Heart Desire (M)");
    m_pInfoPMFNS->parList.append("Personality (M)");
    m_pInfoPMFNS->parList.append("Karmic Lessons");
    m_pInfoPMFNS->resize(m_pInfoMF->width(), 300);
    m_pInfoPMFNS->move(pix2+605, piy4);

    m_pInfoNamePNS = new CInfoNameWidget(appstyle, m_Res.titleBar2, bkg2);
    m_pInfoNamePNS->setWindowTitle("Name calculation");
    m_pInfoNamePNS->move(pix2, piy4);
    m_pInfoNamePNS->m_bSymTrackEna = true;

	QObject::connect(m_pFullNameEdit, SIGNAL(textChanged(const QString &)), this, SLOT(OnTextChanged(const QString &)));
	QObject::connect(m_pShortNameEdit, SIGNAL(textChanged(const QString &)), this, SLOT(OnTextChanged(const QString &)));
	QObject::connect(m_pBDate, SIGNAL(dateChanged(const QDate &)), this, SLOT(OnDateChanged(const QDate &)));
	QObject::connect(m_pInfoNameNS, SIGNAL(symbolCliked(int,int)), this, SLOT(OnNameVWChanged(int,int)));
	QObject::connect(m_uCNC, SIGNAL(stateChanged(int)), this, SLOT(OnParsChanged(int)));
	QObject::connect(m_PMode, SIGNAL(currentIndexChanged(int)), this, SLOT(OnParsChanged(int)));
	QObject::connect(m_pPFullNameEdit, SIGNAL(textChanged(const QString &)), this, SLOT(OnPTextChanged(const QString &)));
	QObject::connect(m_pPShortNameEdit, SIGNAL(textChanged(const QString &)), this, SLOT(OnPTextChanged(const QString &)));
	QObject::connect(m_pPBDate, SIGNAL(dateChanged(const QDate &)), this, SLOT(OnDateChanged(const QDate &)));
	QObject::connect(m_pInfoNamePNS, SIGNAL(symbolCliked(int,int)), this, SLOT(OnNamePVWChanged(int,int)));


    m_pSaveFileButton = new QPushButton("", bkg1);
    m_pSaveFileButton->resize(24, 24);
    QObject::connect(m_pSaveFileButton, SIGNAL(clicked()), this, SLOT(OnSaveFile()));
    m_pSaveFileButton->move(725+pix,piy);

    m_pOpenFileButton = new QPushButton("", bkg1);
    m_pOpenFileButton->resize(24, 24);
    QObject::connect(m_pOpenFileButton, SIGNAL(clicked()), this, SLOT(OnOpenFile()));
    m_pOpenFileButton->move(756+pix,piy);

	m_pNewFileButton = new QPushButton("", bkg1);
	m_pNewFileButton->resize(24, 24);
	QObject::connect(m_pNewFileButton, SIGNAL(clicked()), this, SLOT(OnResDataI()));
	m_pNewFileButton->move(785+pix,piy);

    m_pSaveFileButton->setStyleSheet(qsSaveBigButStyle);
	m_pOpenFileButton->setStyleSheet(qsOpenBigButStyle);
	m_pNewFileButton->setStyleSheet(qsNewBigButStyle);

    for (int a=0; a<cRecentNum; a++)
    {
        QPushButton *but = new QPushButton(m_pSelPersWidgI_);
        but->move(835, a*62+10);
        but->resize(170, 52);
        //but->setStyleSheet(qsRecentPersStyle);
        QObject::connect(but, SIGNAL(clicked()), this, SLOT(OnRecentClicked()));
        m_pRecentList << but;
    }
	updateRecentList();

    m_pClearRecent = new QPushButton("Clear", m_pSelPersWidgI_);
    m_pClearRecent->resize(100,30);
    m_pClearRecent->move(900, 530);
    //m_pClearRecent->setStyleSheet(qsRecentPersStyle);
    QObject::connect(m_pClearRecent, SIGNAL(clicked()), this, SLOT(OnClearRecent()));


    // Menu
    m_ActionHelp = new QAction(m_Res.helpAbout, "Help", this);
    connect(m_ActionHelp, SIGNAL(triggered()), this, SLOT(OnHelp()));
    m_ActionSupport = new QAction("Support", this);
    connect(m_ActionSupport, SIGNAL(triggered()), this, SLOT(OnSupport()));
    m_ActionSettings = new QAction("Calculator Settings", this);
    connect(m_ActionSettings, SIGNAL(triggered()), this, SLOT(OnSettings()));


    actionPrint = new QAction(m_Res.filePrint, "Print", this);
    actionPrint->setShortcut(tr("Ctrl+P"));
    connect(actionPrint, SIGNAL(triggered()), this, SLOT(OnPrintPreview()));
    actionPrintAll = new QAction(m_Res.filePrint, "Print All", this);
    connect(actionPrintAll, SIGNAL(triggered()), this, SLOT(OnPrintPreviewAll()));
    actionSaveRTF = new QAction(m_Res.fileExportPDF, "Save Report", this);
    connect(actionSaveRTF, SIGNAL(triggered()), this, SLOT(OnExportRTF()));
    actionExportPDF = new QAction(m_Res.fileExportPDF, "Save to PDF", this);
    connect(actionExportPDF, SIGNAL(triggered()), this, SLOT(OnExportPDF()));
    actionExportPDFAll = new QAction(m_Res.fileExportPDF, "Save All to PDF", this);
    connect(actionExportPDFAll, SIGNAL(triggered()), this, SLOT(OnExportPDFAll()));
    actionRegister = new QAction("Register", this);
    connect(actionRegister, SIGNAL(triggered()), this, SLOT(OnRegister()));

    m_ActionAbout = new QAction(m_Res.helpAbout, "About", this);
    connect(m_ActionAbout, SIGNAL(triggered()), this, SLOT(OnAbout()));




    m_Menu.setTitle("Numerology");
    //m_Menu.setStyleSheet(qsMenuStyle);

    m_pStyleSubMenu = m_Menu.addMenu("Style");

    styleMenuGroup = new QActionGroup(this);

    for (int a=0; a < appstyle->stylesNames.size(); a++)
    {
        QAction *action = new QAction(appstyle->stylesNames.at(a), styleMenuGroup);
        action->setCheckable(true);
        m_pStyleSubMenu->addAction(action);
        QObject::connect(action, SIGNAL(triggered()), this, SLOT(OnStyleChange()));
    }



    m_Menu.addAction(m_ActionSettings);
    m_Menu.addSeparator();
    m_Menu.addAction(m_ActionAbout);
    m_Menu.addAction(m_ActionHelp);
    m_Menu.addAction(m_ActionSupport);
	m_Menu.addSeparator();
    m_Menu.addAction(actionPrint);
    m_Menu.addAction(actionPrintAll);
    m_Menu.addAction(actionExportPDF);
    m_Menu.addAction(actionExportPDFAll);
    m_Menu.addAction(actionSaveRTF);

    setDateFormat(inpDateFormat);

    getStatus();
#ifndef APPSTORE
    //if (!getStatus())
    {
		m_Menu.addSeparator();
        m_Menu.addAction(actionRegister);
    }
#endif

#ifdef VER_PREM
    CLayoutManagerDlg::loadReportSettings(&settingReport);
    m_pReportBox->addItem("Default");
    for (int i = 0; i < settingReport.size(); i++)
    {
        m_pReportBox->addItem(settingReport.at(i).name);
    }
#endif

#ifdef Q_OS_MAC
    //m_pReportBox->view()->setMinimumHeight((15 * m_pReportBox->count()) + 15);
#endif

    OnResData();


    m_iCMode = C_MODE_NUMEROLOGY;
	int wszh = 700, wszw = 1015;
    setMinimumSize(1015, 570);

	QDesktopWidget desktopWidg;
	QRect desktopWid = desktopWidg.availableGeometry(desktopWidg.screenNumber(this));
	if (desktopWid.height()<700)
		wszh = desktopWid.height();
	else
		wszh = 700;
	if (desktopWid.width()<1100)
		wszw = desktopWid.width();
	else
		wszw = 1100;
	resize(wszw, wszh);
#ifdef Q_WS_MAC
    move((desktopWid.width()-wszw)/2, (desktopWid.height()-wszh)/2);
#endif


	setMode(C_MODE_NUMEROLOGY);

    //QObject::connect(m_pBDateF, SIGNAL(dateChanged(QDate)), this, SLOT(slotPNC(QDate)));
    slotPNC(m_pBDateF->date());
    //if (m_pFullNameEdit->text().isEmpty() || m_pFullNameEdit->text().isNull())
 //   resizeEvent(0);

	//if (settings->checkUpdates>0)
	{
		QDateTime dt = lastUpdate;
		dt = dt.addDays(7);
		if (QDateTime::currentDateTime() > dt)
			checkForUpdates();
	}

    m_pStartUpScreen = new QWidget(this);
    appstyle->setWidgetStyle(m_pStartUpScreen, WTQsSelPersIWidg);
    m_pStartUpScreen->resize(wszw, wszh);
    //m_pStartUpScreen->setStyleSheet(qsSelPersIWidgStyle);
    QLabel *label1 = new QLabel(m_pStartUpScreen);
    label1->setPixmap(QPixmap::fromImage(m_Res.logoh));
    label1->resize(m_Res.logoh.size());
    label1->move((wszw-m_Res.logoh.width())/2,(wszh-m_Res.logoh.height())/2);
    label1->setStyleSheet("background:none; border: none;");

    QLabel *label2 = new QLabel("Loading...\r\n\r\n\r\nThank You for using VeBest Numerology,\r\nThe World's most advanced Numerology application!", m_pStartUpScreen);
    label2->setStyleSheet(qsSULabelStyle);
    label2->setWordWrap(true);
    label2->setAlignment(Qt::AlignHCenter);
    label2->move(0, label1->geometry().bottom()+20);
    label2->resize(wszw, 200);


    /* Moved to StartUP Slot!
    loadData();
    OnOpenFile(true);
    OnCalcNum();
    m_pSelPersBut->setText(buttonRun);
*/
    initStyle();



    connect(&persButAnum, SIGNAL(frameChanged(int)), this, SLOT(OnPersButAnim(int)));
    connect(&persButAnum, SIGNAL(finished()), this, SLOT(OnPersButAnimFinish()));
}
//----------------------------------------------------------
CVBNum::~CVBNum()
{
    if (isLoaded == true)
    {
        OnSaveFile(true);
        saveData();
    }
}
//----------------------------------------------------------
void CVBNum::setDateFormat(bool mode) {
    if (mode) {
        m_pPetBDate->setDisplayFormat("dd/MM/yyyy");
        m_pAnDate->setDisplayFormat("dd/MM/yyyy");
        m_pBDateF->setDisplayFormat("dd/MM/yyyy");
        m_pBDate->setDisplayFormat("dd/MM/yyyy");
        m_pPBDate->setDisplayFormat("dd/MM/yyyy");
    } else {
        m_pPetBDate->setDisplayFormat("MM/dd/yyyy");
        m_pAnDate->setDisplayFormat("MM/dd/yyyy");
        m_pBDateF->setDisplayFormat("MM/dd/yyyy");
        m_pBDate->setDisplayFormat("MM/dd/yyyy");
        m_pPBDate->setDisplayFormat("MM/dd/yyyy");
    }
}
//----------------------------------------------------------
void CVBNum::setMode(int mode)
{
    if (m_bNameSel)
        return;

    m_iCMode = mode;
    cmode = mode;
    //int w = this->width(), h = this->height();

    m_pAnimCtrlSel->stop();
    m_pAnimNWGeom->stop();

    m_pAnimGrpSetNumer->stop();
    m_pAnimGrpResNumer->stop();
    m_pAnimGrpSetNumerName->stop();
    m_pAnimGrpSetNumerPE->stop();
    m_pAnimGrpSetNumerE->stop();
    m_pAnimGrpSetNumerCal->stop();
    m_pAnimGrpResNumerName->stop();
    m_pAnimGrpResNumerPE->stop();
    m_pAnimGrpResNumerE->stop();
    m_pAnimGrpResNumerCal->stop();
    m_pAnimGrpSetMatr->stop();
    m_pAnimGrpResMatr->stop();
    m_pAnimGrpSetAnalizeName->stop();
    m_pAnimGrpResAnalizeName->stop();

    m_pAnimGrpSetAnalizeNCalc->stop();
    m_pAnimGrpResAnalizeNCalc->stop();

    m_pAnimGrpSetAnalizeButton->stop();
    m_pAnimGrpResAnalizeButton->stop();

    //m_pAnimGrpSetNumerWV->stop();
    //m_pAnimGrpResNumerWV->stop();

    QRect nwrectg = calcNWGeometry();
    QRect nwrect = nwrectg.adjusted(1,1,-1,-1);


    if ((mode==C_MODE_NUMEROLOGY)||(mode==C_MODE_FORECAST))
    {
        m_pText->resize(nwrect.size());
        m_pText->show();
        //m_pGView->centerOn(m_pText);

        m_pAnimGrpSetNumer->start();
    } else
    {
        m_pText->hide();

        if (mode!=C_MODE_CEL)
        {
            m_pAnimGrpResNumer->start();
        }
    }

    if (mode==C_MODE_NUMEROLOGY)
    {
        m_pAnimCtrlSel->setEndValue(QRect(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_NUMEROLOGY), C_CTRL_BUT_W, C_CTRL_BUT_H));

        m_pAnimGrpSetNumerName->setEndValue(calcBWGeometry(C_WIDGID_NAME).topLeft());
        m_pAnimGrpSetNumerPE->setEndValue(calcBWGeometry(C_WIDGID_PE).topLeft());

        m_pAnimGrpSetNumerName->start();
        m_pAnimGrpSetNumerPE->start();
        //m_pAnimGrpSetNumerWV->start();
    } else
    {
        m_pAnimGrpResNumerName->setEndValue(calcBWGeometry(C_WIDGID_NAME,true).topLeft());
        m_pAnimGrpResNumerPE->setEndValue(calcBWGeometry(C_WIDGID_PE,true).topLeft());

        m_pAnimGrpResNumerName->start();
        m_pAnimGrpResNumerPE->start();
        //m_pAnimGrpResNumerWV->start();
    }

    if (mode==C_MODE_PMATR)
    {
        m_pTextPMatr->resize(nwrect.size());
        m_pTextPMatr->show();
        m_pAnimCtrlSel->setEndValue(QRect(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_PMATR), C_CTRL_BUT_W, C_CTRL_BUT_H));
        m_pAnimGrpSetMatr->setEndValue(calcBWGeometry(C_WIDGID_MATR).topLeft());
        m_pAnimGrpSetMatr->start();
        //m_pAnimGrpSetMatrWV->start();
    } else
    {
        m_pTextPMatr->hide();
        m_pAnimGrpResMatr->setEndValue(calcBWGeometry(C_WIDGID_MATR,true).topLeft());
        m_pAnimGrpResMatr->start();
        //m_pAnimGrpResMatrWV->start();
    }

    if (mode==C_MODE_FORECAST)
    {
        m_pAnimCtrlSel->setEndValue(QRect(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_FORECAST), C_CTRL_BUT_W, C_CTRL_BUT_H));

        m_pAnimGrpSetNumerCal->setEndValue(calcBWGeometry(C_WIDGID_CAL).topLeft());
        m_pAnimGrpSetNumerE->setEndValue(calcBWGeometry(C_WIDGID_TES).topLeft());

        m_pAnimGrpSetNumerCal->start();
        m_pAnimGrpSetNumerE->start();
    } else
    {
        m_pAnimGrpResNumerCal->setEndValue(calcBWGeometry(C_WIDGID_CAL,true).topLeft());
        m_pAnimGrpResNumerE->setEndValue(calcBWGeometry(C_WIDGID_TES,true).topLeft());

        m_pAnimGrpResNumerE->start();
        m_pAnimGrpResNumerCal->start();
    }

    if (mode==C_MODE_COMP)
    {
        m_pTextComp->resize(nwrect.size());
        m_pTextComp->show();
        m_pAnimCtrlSel->setEndValue(QRect(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_COMP), C_CTRL_BUT_W, C_CTRL_BUT_H));
        m_pAnimGrpSetCompPM->setEndValue(calcBWGeometry(C_WIDGID_MATR).topLeft());
        m_pAnimGrpSetCompPM->start();
        //m_pAnimGrpSetCompWV->start();
        m_pAnimGrpSetComp->start();
    } else
    {
        m_pTextComp->hide();
        m_pAnimGrpResCompPM->setEndValue(calcBWGeometry(C_WIDGID_MATR,true).topLeft());
        m_pAnimGrpResCompPM->start();
        //m_pAnimGrpResCompWV->start();
        m_pAnimGrpResComp->start();
    }

    if (mode==C_MODE_CEL)
    {
        m_pTextCel->resize(nwrect.size());
        m_pTextCel->show();
        m_pAnimCtrlSel->setEndValue(QRect(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_CEL), C_CTRL_BUT_W, C_CTRL_BUT_H));

        m_pAnimGrpSetNumer->start();
        //m_pAnimGrpSetCelWV->start();
    } else
    {
        m_pTextCel->hide();
        if ((mode!=C_MODE_NUMEROLOGY)&&(mode!=C_MODE_FORECAST))
            m_pAnimGrpResNumer->start();
        //m_pAnimGrpResCelWV->start();
    }

    if (mode==C_MODE_PET)
    {
        m_pTextPet->resize(nwrect.size());
        m_pTextPet->show();
        m_pAnimCtrlSel->setEndValue(QRect(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_PET), C_CTRL_BUT_W, C_CTRL_BUT_H));
        m_pAnimGrpSetPetName->setEndValue(calcBWGeometry(C_WIDGID_PNAME).topLeft());
        m_pAnimGrpSetPetName->start();
        m_pAnimGrpSetPet->start();      

        //m_pAnimGrpSetPetWV->start();
    } else
    {
        m_pTextPet->hide();
        m_pAnimGrpResPetName->setEndValue(calcBWGeometry(C_WIDGID_PNAME,true).topLeft());
        m_pAnimGrpResPetName->start();
        m_pAnimGrpResPet->start();

        //m_pAnimGrpResPetWV->start();
    }

    if (mode==C_MODE_CHART)
    {
        resizeEvent(0);
       // OnUpdateListReport();
        //repaintChart();

        topLevelLabel->resize(nwrect.size().width(), nwrect.size().height() - 35);
        labelChart->move(250, nwrect.size().height() - 18);
        chartEditorBut->move(550, nwrect.size().height() - 23);
        chartAddBut->move(580 + 35, nwrect.size().height() - 23);
        chartDellBut->move(610 + 35 + 35, nwrect.size().height() - 23);
        comboChart->move(380, nwrect.size().height() - 23);
        //topLevelLabel->setScaledContents(true);
        //m_pTextChart->setDisabled(true);
        //m_pTextChart->setHtml("<img src='" + QApplication::applicationDirPath() + "/images/numl1.jpg' width=100%>");
        //m_pTextChart->setHtml(QApplication::applicationDirPath());
       // m_pTextChart->move(0, 0);

		xWP = topLevelLabel->width();
		yHP = topLevelLabel->height();
//		unsigned int xW = topLevelLabel->width();
//		unsigned int yH = topLevelLabel->height();
//		PixMapResize(texture->size().width(), texture->size().height(), xW, yH);
//		topLevelLabel->setPixmap(QPixmap::fromImage(m_pChartNumerology->numChart).scaled(topLevelLabel->width(), topLevelLabel->height(), Qt::KeepAspectRatio, Qt::SmoothTransformation));


        topLevelLabel->show();
        comboChart->show();
        labelChart->show();
#ifdef VER_PREM
        chartEditorBut->show();
        chartAddBut->show();
        chartDellBut->show();
#endif
        m_pAnimCtrlSel->setEndValue(QRect(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_CHART), C_CTRL_BUT_W, C_CTRL_BUT_H));
    } else
    {
        topLevelLabel->hide();
        comboChart->hide();
        labelChart->hide();
        chartAddBut->hide();
        chartDellBut->hide();
        chartEditorBut->hide();
        //m_pTextChart->move(-1000, -1000);
    }

    //IC
    if (mode == C_MODE_ANALIZE)
    {
        m_pTextAn->resize(nwrect.size());
        m_pTextAn->show();

        m_pAnimCtrlSel->setEndValue(QRect(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_ANALIZE), C_CTRL_BUT_W, C_CTRL_BUT_H));

        m_pAnimGrpSetAnalizeName->setEndValue(calcBWGeometry(C_WIDGID_ANALIZE).topLeft());
        m_pAnimGrpSetAnalizeName->start();

        m_pAnimGrpSetAnalizeNCalc->setEndValue((calcBWGeometry(C_WIDGID_ANALIZE).topLeft()) + QPoint(0, 180));
        m_pAnimGrpSetAnalizeNCalc->start();

        m_pAnimGrpSetAnalizeButton->setEndValue((calcBWGeometry(C_WIDGID_ANALIZE).topLeft()) + QPoint(500, 145));
        m_pAnimGrpSetAnalizeButton->start();

        m_pAnimGrpSetAnalize->start();
    }
    else
    {
        m_pTextAn->hide();

        m_pAnimGrpResAnalizeName->setEndValue(calcBWGeometry(C_WIDGID_ANALIZE, true).topLeft());
        m_pAnimGrpResAnalizeName->start();

        m_pAnimGrpResAnalizeNCalc->setEndValue(calcBWGeometry(C_WIDGID_ANALIZE, true).topLeft());
        m_pAnimGrpResAnalizeNCalc->start();

        m_pAnimGrpResAnalizeButton->setEndValue((calcBWGeometry(C_WIDGID_ANALIZE, true).topLeft()) + QPoint(500, 145));
        m_pAnimGrpResAnalizeButton->start();

        m_pAnimGrpResAnalize->start();
    }
    //*IC

	if (mode==C_MODE_GUIDE)
	{
		m_pTextGuide->resize(nwrect.size());
		m_pTextGuide->show();
		m_pAnimCtrlSel->setEndValue(QRect(0, C_CTRL_BUT_H_STEP*getSelIdx(C_MODE_GUIDE), C_CTRL_BUT_W, C_CTRL_BUT_H));
		//m_pAnimGrpSetPetName->setEndValue(calcBWGeometry(C_WIDGID_PNAME).topLeft());
		//m_pAnimGrpSetPetName->start();
		//m_pAnimGrpSetPet->start();      
	} else
	{
		m_pTextGuide->hide();
		//m_pAnimGrpResPetName->setEndValue(calcBWGeometry(C_WIDGID_PNAME,true).topLeft());
		//m_pAnimGrpResPetName->start();
		//m_pAnimGrpResPet->start();

		//m_pAnimGrpResPetWV->start();
	}


    m_pAnimNWGeom->setEndValue(nwrectg);
    m_pAnimNWGeom->start();
    m_pAnimCtrlSel->start();
}

void CVBNum::OnUpdateListReport()
{
    disconnect(comboChart, SIGNAL(currentIndexChanged(int)), this, SLOT(slotGChart(int)));
    comboChart->clear();

#ifdef VER_PREM
	QString reportPath = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + "/VeBest Numerology";
#else
#ifdef Q_OS_MAC
	QString reportPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/templates");
#else
	QString reportPath = QCoreApplication::applicationDirPath() + QString("/images/templates");
#endif
#endif
	QDir repDir(reportPath);
    QStringList repList = repDir.entryList(QStringList("*.vnr"));

    for (int i = 0; i < repList.size(); i++)
        comboChart->addItem(repList.at(i).left(repList.at(i).size() - 4), reportPath + "/" + repList.at(i));
    connect(comboChart, SIGNAL(currentIndexChanged(int)), this, SLOT(slotGChart(int)));
}

int CVBNum::getSelIdx(int mode)
{
    int i=mode;
    if (i<0)
        i = cmode;

#ifdef VER_FREE
    if (mode==C_MODE_NUMEROLOGY)
        return 0;
    else if (mode==C_MODE_PMATR)
        return 1;
    else if (mode==C_MODE_FORECAST)
        return 0;
    else if (mode==C_MODE_COMP)
        return 2;
    else if (mode==C_MODE_CEL)
        return 3;
    else if (mode==C_MODE_PET)
        return 4;
    else if (mode==C_MODE_CHART)
        return 0;
    else if (mode == C_MODE_ANALIZE)
        return 0;
	else if (mode == C_MODE_GUIDE)
		return 5;
#else
    if (mode==C_MODE_NUMEROLOGY)
        return 0;
    else if (mode==C_MODE_PMATR)
        return 1;
    else if (mode==C_MODE_FORECAST)
        return 2;
    else if (mode==C_MODE_COMP)
        return 3;
    else if (mode==C_MODE_CEL)
        return 4;
    else if (mode==C_MODE_PET)
        return 5;
    else if (mode==C_MODE_CHART)
        return 6;
    else if (mode == C_MODE_ANALIZE)
        return 7;
	else if (mode == C_MODE_GUIDE)
		return 8;
#endif
    return 0;
}

//----------------------------------------------------------
void CVBNum::OnNameSel()
{
    persButAnum.stop();
    appstyle->setWidgetStyle(m_pSelPersBut, WTQsPersSelBut);
    m_bNameSel = !m_bNameSel;
    m_pAnimNameSel->stop();
    if (m_bNameSel)
    {
       //
       m_pAnimNameSel->setEasingCurve(QEasingCurve::InOutExpo);
       m_pAnimNameSel->setEndValue(calcBWGeometry(C_WIDGID_NAMESEL, !m_bNameSel).topLeft());

       buttonRun = "APPLY";

    } else
    {
        m_pAnimNameSel->setEasingCurve(QEasingCurve::OutExpo);
        m_pAnimNameSel->setEndValue(calcBWGeometry(C_WIDGID_NAMESEL, !m_bNameSel).topLeft());

        OnCalcNum();
        if (m_pFullNameEdit->text().isEmpty() || m_pFullNameEdit->text().isNull())
        {
            buttonRun = "ENTER YOUR NAME";
            startButAnim();
        }
    }
    slotPNC(m_pBDateF->date());
    m_pAnimNameSel->start();

    QEventLoop event;
    connect(m_pAnimNameSel, SIGNAL(finished()), &event, SLOT(quit()));
    event.exec();
    m_pSelPersBut->setText(buttonRun);

    resizeEvent(0);
}
//----------------------------------------------------------
QRect CVBNum::calcNWGeometry(int mode)
{
    if (mode<0)
        mode = m_iCMode;

    int w = this->width(), h = this->height();
    int px=216;
    int lww=199;
    int rw=0;

    //QRect(10, 10, this->width()-10-200, h-this->height())

    switch(mode)
    {
    default:
    case C_MODE_NUMEROLOGY:
    case C_MODE_FORECAST:
        rw = w-px-lww;
        if (rw>1024) {
            rw=1024;
            px += (w-px-lww - rw)/2;
        }
        return QRect(px, 10, rw, h-265);
    case C_MODE_PMATR:
    case C_MODE_COMP:
        rw = w-px-lww;
        if (rw>1024) {
            rw=1024;
            px += (w-px-lww - rw)/2;
        }
        return QRect(px, 10, rw, h-314);
    case C_MODE_CEL:
        rw = w-px-lww;
        if (rw>1024) {
            rw=1024;
            px += (w-px-lww - rw)/2;
        }
        return QRect(px, 10, rw, h-20);
    case C_MODE_PET:
        rw = w-px-lww;
        if (rw>1024) {
            rw=1024;
            px += (w-px-lww - rw)/2;
        }
        return QRect(px, 10, rw, h-110);
    case C_MODE_CHART:
        return QRect(10, 10, w-10-lww, h-70);
    case C_MODE_ANALIZE: //275
        rw = w-px-lww;
        if (rw>1024) {
            rw=1024;
            px += (w-px-lww - rw)/2;
        }
        return QRect(px, 280, rw, h - 285);
	case C_MODE_GUIDE:
        rw = w-px-lww;
        if (rw>1024) {
            rw=1024;
            px += (w-px-lww - rw)/2;
        }
        return QRect(px, 10, rw, h-20);
    }
}
//----------------------------------------------------------
QRect CVBNum::calcBWGeometry(int widgId, bool outOfScreen)
{
    int w = this->width(), h = this->height();
    int ox = (w - 1024)/2;
    int ypos=-65;
    int ypos2=-65;
    int lww=200;

    int oos=0;
    if (outOfScreen)
        oos = 1000;

    switch (widgId)
    {
        case C_WIDGID_NAME:
            return QRect(lww+20+ox, h-334-ypos+20+oos, 0, 0);
        case C_WIDGID_PE:
            return QRect(lww+20+ox, h-188-ypos+15+oos, 0, 0);
        case C_WIDGID_CAL:
            return QRect(lww+20+ox, h-334-ypos+20+oos, 0, 0);
        case C_WIDGID_TES:
            return QRect(lww+20+ox, h-235-ypos+15+oos, 0, 0);
        case C_WIDGID_MATR:
            return QRect(lww+20+ox, h-373-ypos2+10+oos, 0, 0);
        case C_WIDGID_PNAME:
            return QRect(lww+20+ox, h-160-ypos2+oos, 0, 0);
        case C_WIDGID_NAMESEL:
            if (outOfScreen)
                return QRect(-w, 0, w, h);
            else
                return QRect(0, 0, w, h);
        case C_WIDGID_ANALIZE:
            if (outOfScreen)
            {
                return QRect(lww + 20 + ox, -1000, 0, 0);
            }
            else
            {
                return QRect(lww + 20 + ox, 10, 0, 0);
            }
        default:
            return QRect(0,0,0,0);
    }
}
//----------------------------------------------------------
void CVBNum::resizeEvent(QResizeEvent *e)
{
    int cw1 = 190, ch1 = 150;
	int w, h;
	if (e!=NULL)
	{
		w = e->size().width();
		h = e->size().height();
	} else
	{
		w = this->width();
		h = this->height();
	}
#ifdef VER_PREM
    m_pReportBox->move(w - 190, h - 170);
#endif

#ifndef APPSTORE
    m_pPurchasePRO->move(w - 190, h - 180);

	if (registermode == 1)
	{
#ifdef VER_PREM
        setWindowTitle("VeBest Numerology Everywhere 7");
        m_pCtrlScrollArea->resize(cw1-10, h-ch1-10 - 45);
#else
        m_pCtrlScrollArea->resize(cw1-10, h-ch1-10);
#endif
#ifdef VER_STD
        setWindowTitle("VeBest Numerology 7 Full");
#endif
		m_pPurchasePRO->hide();
		m_pCtrlScrollArea->move(w-cw1, 10);

	} else
	{
#ifdef VER_PREM
        setWindowTitle("VeBest Numerology Everywhere 7");
#endif
#ifdef VER_STD
        setWindowTitle("VeBest Numerology 7");
#endif

#ifdef VER_PREM
        m_pReportBox->move(w - 190, h - 215);
        m_pCtrlScrollArea->resize(cw1-10, h-ch1-50 - 50);
#else
        m_pCtrlScrollArea->resize(cw1-10, h-ch1-50);
#endif
		m_pPurchasePRO->show();
		m_pCtrlScrollArea->move(w-cw1, 10);
	}
#else
#ifdef VER_FREE
    setWindowTitle("VeBest Numerology 7 Lite");
	m_pPurchasePRO->move(w - 190, h - 180);
    m_pCtrlScrollArea->move(w-cw1, 10);
    m_pCtrlScrollArea->resize(cw1-10, h-ch1-50);
#else
	setWindowTitle("VeBest Numerology 7");
	m_pCtrlScrollArea->move(w-cw1, 10);
    m_pCtrlScrollArea->resize(cw1-10, h-ch1-10);
#endif
#endif

#ifdef VER_PREM
    m_pReportBut->move(m_pReportBox->x() +  m_pReportBox->width() - 1, m_pReportBox->y());
    m_pReportLabel->move(m_pReportBox->x(), m_pReportBox->y() - 25);
#endif


    QRect nwrectg = calcNWGeometry();
    QRect nwrect = nwrectg.adjusted(1,1,-1,-1);
    m_pGView->move(nwrectg.topLeft());
    m_pGView->resize(nwrectg.size().width(), nwrectg.size().height() + 50);
    //m_pTextAn->resize(nwrect.size());
    //m_pGView->centerOn(m_pText);
    m_pSelPersBut->move(8, h-m_pSelPersBut->height()-8);
    m_pSelPersWidg->setGeometry(calcBWGeometry(C_WIDGID_NAMESEL, !m_bNameSel));




    if (m_iCMode==C_MODE_NUMEROLOGY)
    {
        m_pText->resize(nwrect.size());
        m_pInfoName->move(calcBWGeometry(C_WIDGID_NAME).topLeft());
        m_pInfoPE->move(calcBWGeometry(C_WIDGID_PE).topLeft());
    } else
    {
        m_pInfoName->move(calcBWGeometry(C_WIDGID_NAME, true).topLeft());
        m_pInfoPE->move(calcBWGeometry(C_WIDGID_PE,true).topLeft());
    }
    if (m_iCMode==C_MODE_PMATR)
    {
        m_pTextPMatr->resize(nwrect.size());
        m_pInfoPM->move(calcBWGeometry(C_WIDGID_MATR).topLeft());
    }
    else
        m_pInfoPM->move(calcBWGeometry(C_WIDGID_MATR,true).topLeft());

    if (m_iCMode==C_MODE_FORECAST)
    {
        m_pText->resize(nwrect.size());
        m_pInfoCal->move(calcBWGeometry(C_WIDGID_CAL).topLeft());
        m_pInfoE->move(calcBWGeometry(C_WIDGID_TES).topLeft());
    } else
    {
        m_pInfoCal->move(calcBWGeometry(C_WIDGID_CAL,true).topLeft());
        m_pInfoE->move(calcBWGeometry(C_WIDGID_TES,true).topLeft());
    }

    if (m_iCMode==C_MODE_COMP)
    {
        m_pTextComp->resize(nwrect.size());
        m_pInfoCompPM->move(calcBWGeometry(C_WIDGID_MATR).topLeft());
    }
    else
        m_pInfoCompPM->move(calcBWGeometry(C_WIDGID_MATR,true).topLeft());
    if (m_iCMode==C_MODE_PET)
    {
        m_pTextPet->resize(nwrect.size());
        m_pInfoNamePet->move(calcBWGeometry(C_WIDGID_PNAME).topLeft());
    }
    else
        m_pInfoNamePet->move(calcBWGeometry(C_WIDGID_PNAME,true).topLeft());
    if (m_iCMode==C_MODE_CEL)
        m_pTextCel->resize(nwrect.size());
    //IC
    else
    if (m_iCMode == C_MODE_ANALIZE)
    {
        m_pTextAn->resize(nwrect.size());
        m_pInpAnalizer->move(calcBWGeometry(C_WIDGID_ANALIZE).topLeft());
        m_pInfoAnName->move((calcBWGeometry(C_WIDGID_ANALIZE).topLeft()) + QPoint(0, 180));
        m_pAnCalcButton->move((calcBWGeometry(C_WIDGID_ANALIZE).topLeft()) + QPoint(500, 145));
    }
    else
    {
        m_pInpAnalizer->move(calcBWGeometry(C_WIDGID_ANALIZE, true).topLeft());
    }
    if (m_iCMode == C_MODE_CHART)
    {
        labelChart->move(250, nwrect.size().height() - 18);
        chartEditorBut->move(550, nwrect.size().height() - 23);
        chartAddBut->move(580 + 35, nwrect.size().height() - 23);
        chartDellBut->move(610 + 35 + 35, nwrect.size().height() - 23);
        comboChart->move(380, nwrect.size().height() - 23);

        topLevelLabel->resize(nwrect.size().width(), nwrect.size().height() - 35);
        xWP = topLevelLabel->width();
        yHP = topLevelLabel->height();

        repaintChart();
//        unsigned int xW = topLevelLabel->width();
//        unsigned int yH = topLevelLabel->height();
//        PixMapResize(texture->size().width(), texture->size().height(), xW, yH);

//        topLevelLabel->setPixmap(QPixmap::fromImage(m_pChartNumerology->numChart).scaled(topLevelLabel->width(), topLevelLabel->height(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    }

	if (m_iCMode==C_MODE_GUIDE)
		m_pTextGuide->resize(nwrect.size());




    //*IC

	m_pSave->move(w - 190, h - 140);
	m_pPrint->move(w - 160, h - 140);

	m_pClearAll->move(w - 120, h - 140);

    m_pMenuButton->move(w - 70, h - 140);
    
	m_pAbout->move(w - 30, h - 140);

    personalNumCalendar->move(w - 190, h - 110);
    //expandPanel->resize(13, 98);
    expandPanel->move((w-190) - (expandPanel->width() - 13), h - 107);
	expandPanel->setPos((w-190) - (expandPanel->width() - 13), h - 107);

    m_pCtrlScrollAreaLine->move(m_pCtrlScrollArea->width() - 35,  m_pCtrlScrollArea->height() - 1);
}
//----------------------------------------------------------
void CVBNum::repaintChart()
{ 
#ifdef Q_OS_MAC
    topLevelLabel->setImage(getChart(topLevelLabel->size() * 2));
#else
    topLevelLabel->setImage(getChart(topLevelLabel->size()));
#endif
}
//----------------------------------------------------------
QImage CVBNum::getChart(QSize size)
{
    int value = comboChart->currentIndex();

    if (comboChart->count() <= 0)
        return QImage();

    if (comboChart->count() <= value)
        return QImage();

    QFile file(comboChart->itemData(value).toString());
    if (!file.open(QFile::ReadOnly))
        return QImage();

    QDataStream stream(&file);

    calcNumContainer calcContainer;
    calcContainer.m_pNumer = m_pNumer;
    calcContainer.m_pNumerComp = m_pNumerComp;
    calcContainer.m_pNumerPMatr = m_pNumerPMatr;
    calcContainer.m_pNumerPMatrComp = m_pNumerPMatrComp;
    CReportCreatorWin *dlg = new CReportCreatorWin(&calcContainer, appstyle, this, THUMB_MODE);
    dlg->mainWidget->pageArea->load(stream);
    file.close();

    QImage img = CManagerThumbnailDlg::slotPrintReport(0, dlg, size);

    delete dlg;

    if (getStatus() == false)
    {
#ifdef VER_PREM
        QString text = "Trial";
#else
        QString text = "Available only in Full Version";
#endif

        QFont font;
        font.setBold(true);
        font.setPixelSize((img.width() * 1.5) / text.size());

        QPainter painter(&img);
        painter.setFont(font);
        painter.setPen(QColor(128, 128, 128, 128));
        painter.drawText(0, 0, img.width(), img.height(), Qt::AlignCenter, text);
    }

    return img;
}
void CVBNum::OnTextChanged(const QString &)
{
    m_viFnvw.clear();
    m_viSnvw.clear();



//    m_viFnvw_l.clear();
//    m_viSnvw_l.clear();
//    m_viPFnvw_l.clear();
//    m_viPSnvw_l.clear();

//    QMessageBox msgBox;
//    msgBox.setText(QString::number(m_viFnvw_l.size()));
//    msgBox.exec();

    if (m_viFnvw_l.size() != 0 || m_viSnvw_l.size() != 0 || m_viPFnvw_l.size() != 0 || m_viPSnvw_l.size() != 0)
    {
        m_viFnvw = m_viFnvw_l;
        m_viSnvw = m_viSnvw_l;
        OnPreCalcNum();
    }
    else
    {
        OnPreCalcNum();
        for (int a=0; a<m_pNumer->numDet.fnameN1.size(); a++)
        {
            if (m_pNumer->numDet.fnameN1.at(a)!=' ')
                m_viFnvw.append(1);
            else if (m_pNumer->numDet.fnameN2.at(a)!=' ')
                m_viFnvw.append(2);
            else
                m_viFnvw.append(0);
        }

        for (int a=0; a<m_pNumer->numDet.snameN1.size(); a++)
        {
            if (m_pNumer->numDet.snameN1.at(a)!=' ')
                m_viSnvw.append(1);
            else if (m_pNumer->numDet.snameN2.at(a)!=' ')
                m_viSnvw.append(2);
            else
                m_viSnvw.append(0);
        }
    }

    //m_viFnvw.fill(0,m_pNumer->numDet.fnameR.size());
    //m_viSnvw.fill(0,m_pNumer->numDet.snameR.size());

    //m_pFullNameEdit->setText(m_pNumer->numDet.fnameR);
}
//----------------------------------------------------------
void CVBNum::OnNameVWChanged(int row, int idx)
{
    if (idx<0)
        return;

    if (row==0)
    {
        if (idx < m_viFnvw.size())
        {
            if (m_viFnvw[idx]==2)
                m_viFnvw[idx]=1;
            else if (m_viFnvw[idx]==1)
                m_viFnvw[idx]=2;
        }
    } else if (row==1)
    {
        if (idx<m_viSnvw.size())
        {
            if (m_viSnvw[idx]==2)
                m_viSnvw[idx]=1;
            else if (m_viSnvw[idx]==1)
                m_viSnvw[idx]=2;
        }
    }

    OnPreCalcNum();
}
//----------------------------------------------------------
void CVBNum::OnPTextChanged(const QString &)
{
    m_viPFnvw.clear();
    m_viPSnvw.clear();



//    m_viFnvw_l.clear();
//    m_viSnvw_l.clear();
//    m_viPFnvw_l.clear();
//    m_viPSnvw_l.clear();

//    QMessageBox msgBox;
//    msgBox.setText(QString::number(m_viFnvw_l.size()));
//    msgBox.exec();

    if (m_viFnvw_l.size() != 0 || m_viSnvw_l.size() != 0 || m_viPFnvw_l.size() != 0 || m_viPSnvw_l.size() != 0)
    {
        m_viPFnvw = m_viPFnvw_l;
        m_viPSnvw = m_viPSnvw_l;

        m_viFnvw_l.clear();
        m_viSnvw_l.clear();
        m_viPFnvw_l.clear();
        m_viPSnvw_l.clear();
        OnPreCalcNumP();
    }
    else
    {
        OnPreCalcNumP();
        for (int a=0; a<m_pNumerComp->numDet.fnameN1.size(); a++)
        {
            if (m_pNumerComp->numDet.fnameN1.at(a)!=' ')
                m_viPFnvw.append(1);
            else if (m_pNumerComp->numDet.fnameN2.at(a)!=' ')
                m_viPFnvw.append(2);
            else
                m_viPFnvw.append(0);
        }

        for (int a=0; a<m_pNumerComp->numDet.snameN1.size(); a++)
        {
            if (m_pNumerComp->numDet.snameN1.at(a)!=' ')
                m_viPSnvw.append(1);
            else if (m_pNumerComp->numDet.snameN2.at(a)!=' ')
                m_viPSnvw.append(2);
            else
                m_viPSnvw.append(0);
        }
    }
}
//----------------------------------------------------------
void CVBNum::OnNamePVWChanged(int row, int idx)
{
    if (idx<0)
        return;

    if (row==0)
    {
        if (idx<m_viPFnvw.size())
        {
            if (m_viPFnvw[idx]==2)
                m_viPFnvw[idx]=1;
            else if (m_viPFnvw[idx]==1)
                m_viPFnvw[idx]=2;
        }
    } else if (row==1)
    {
        if (idx<m_viPSnvw.size())
        {
            if (m_viPSnvw[idx]==2)
                m_viPSnvw[idx]=1;
            else if (m_viPSnvw[idx]==1)
                m_viPSnvw[idx]=2;
        }
    }

    OnPreCalcNumP();
}
//----------------------------------------------------------
void CVBNum::OnParsChanged(int)
{
	OnTextChanged("");
	OnPTextChanged("");
}
void CVBNum::OnDateChanged(const QDate &)
{
	OnParsChanged(0);
}
//----------------------------------------------------------
void CVBNum::OnHelp()
{
#ifdef VER_PREM
    QDesktopServices::openUrl(QUrl("http://www.vebest.com/products/hobby/numerologypro.html", QUrl::TolerantMode));
#else
	QDesktopServices::openUrl(QUrl("http://www.vebest.com/products/hobby/12-numerology/numerology6-help.html", QUrl::TolerantMode));
#endif
}
//----------------------------------------------------------
void CVBNum::OnSupport()
{
    QString mailreq = "mailto:support@vebest.com?Subject=Support request for";
#ifdef VER_PREM
    mailreq += " VeBest Numerology Everywhere";
#endif

#ifdef VER_FREE
    mailreq += " VeBest Numerology Lite";
#endif

#ifdef VER_STD
    mailreq += " VeBest Numerology";
#endif

#ifdef APPSTORE
    mailreq += " MAS";
#endif

#ifdef Q_WS_MAC
    mailreq += " for Mac";
#else
    mailreq += " for Win";
#endif

    QDesktopServices::openUrl(QUrl(mailreq, QUrl::TolerantMode));
}

//----------------------------------------------------------
void CVBNum::OnSettings()
{
    CSettingsDlg *dlg = new CSettingsDlg();
    dlg->ui->checkBoxLP1->setChecked(m_GlobSettings.calcLP1);
    dlg->ui->checkBoxLP2->setChecked(m_GlobSettings.calcLP2);
    dlg->ui->checkBoxLP3->setChecked(m_GlobSettings.calcLP3);
    dlg->ui->checkBoxNameDD->setChecked(m_GlobSettings.calcNameDD);
    dlg->ui->checkBoxNameWD->setChecked(m_GlobSettings.calcNameWD);
    dlg->ui->checkBoxSkipTrans->setChecked(m_GlobSettings.skipTransitFS);
    dlg->ui->checkBoxDateFormat->setChecked(inpDateFormat);
    if (mmcalcTo99==CNumerology::MNM_11TO99)
        dlg->ui->checkBoxExtendedMMto99->setChecked(true);

    if (dlg->exec() == QDialog::Accepted)
    {
        m_GlobSettings.calcLP1 = dlg->ui->checkBoxLP1->isChecked();
        m_GlobSettings.calcLP2 = dlg->ui->checkBoxLP2->isChecked();
        m_GlobSettings.calcLP3 = dlg->ui->checkBoxLP3->isChecked();
        m_GlobSettings.calcNameDD = dlg->ui->checkBoxNameDD->isChecked();
        m_GlobSettings.calcNameWD = dlg->ui->checkBoxNameWD->isChecked();
        m_GlobSettings.skipTransitFS = dlg->ui->checkBoxSkipTrans->isChecked();
        inpDateFormat = dlg->ui->checkBoxDateFormat->isChecked();
        if (dlg->ui->checkBoxExtendedMMto99->isChecked())
            mmcalcTo99 = CNumerology::MNM_11TO99;
        else
            mmcalcTo99 = CNumerology::MNM_11TO33;
#ifndef VER_PREM
        mmcalcTo99 = CNumerology::MNM_11TO33;
#endif

        setDateFormat(inpDateFormat);
		OnCalcNum();
    }
    delete dlg;
}
//----------------------------------------------------------
void CVBNum::OnAbout()
{
    CAboutDlg dlg(&m_Res.logo, &m_Res.logo, this);

    int x = (this->width()-dlg.width())/2 + this->x();
    if (x<0)
        x=0;

    int y = (this->height()-dlg.height())/2 + this->y();
    if (y<0)
        y=0;

    dlg.setStyleSheet(appstyle->updateColor(dlg.styleSheet()));

    dlg.move(x, y);

    if (dlg.exec() == QDialog::Accepted)
#ifdef VER_PREM
        QDesktopServices::openUrl(QUrl("http://www.vebest.com/products/hobby/numerologypro.html", QUrl::TolerantMode));
#else
		QDesktopServices::openUrl(QUrl("http://www.vebest.com/products/hobby/12-numerology.html", QUrl::TolerantMode));
#endif
}

//----------------------------------------------------------
void CVBNum::OnRecentClicked()
{
    QObject *obj = QObject::sender();
    for (int a=0; a<m_pRecentList.size(); a++)
    {
        if (obj == m_pRecentList.at(a) || RecentClicked == true)
        {
            if (a<m_pRecentEntries.size())
            {
                SUserInfo re = m_pRecentEntries.at(a);
                m_pBDate->setDate(re.birthDate);

                m_pFullNameEdit->setText(re.fullName);
                m_pShortNameEdit->setText(re.shortName);

                if (re.psex)
                    m_pSex->setCurrentIndex(0);
                else
                    m_pSex->setCurrentIndex(1);

                m_pPBDate->setDate(re.partnerBirthDate);

                m_pPFullNameEdit->setText(re.partnerFullName);
                m_pPShortNameEdit->setText(re.partnerShortName);

                m_viFnvw = re.fnvw;
                m_viSnvw = re.snvw;
                m_viPFnvw = re.pfnvw;
                m_viPSnvw = re.psnvw;

                m_uCNC->setChecked(re.uCNC);

                OnPreCalcNum();
                OnPreCalcNumP();
                break;
            }
        }
    }
}
//----------------------------------------------------------
void CVBNum::updateRecentList()
{
    QString months[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    for (int a=0; a<m_pRecentList.size(); a++)
    {
        if (a<m_pRecentEntries.size())
        {
            QString tit = m_pRecentEntries.at(a).fullName + "\r\n";
            tit += months[m_pRecentEntries.at(a).birthDate.month()-1] + " ";
            tit += QString::number(m_pRecentEntries.at(a).birthDate.day()) + ", ";
            tit += QString::number(m_pRecentEntries.at(a).birthDate.year());
            if (m_pRecentEntries.at(a).partnerFullName.size()>0)
            {
                tit += "\r\n" + m_pRecentEntries.at(a).partnerFullName;
            }
            m_pRecentList.at(a)->setText(tit);
            m_pRecentList.at(a)->show();
        } else
        {
            m_pRecentList.at(a)->hide();
        }
    }
}
//----------------------------------------------------------
void CVBNum::OnClearRecent()
{
    m_pRecentEntries.clear();
    updateRecentList();
}

//----------------------------------------------------------
void CVBNum::slotGChart(int value)
{
    if (comboChart->count() <= 0)
        return;

    if (comboChart->count() <= value)
        return;

    resizeEvent(0);
    //topLevelLabel->setPixmap(texture->scaled(topLevelLabel->size(), Qt::KeepAspectRatio));

//    if (value > 11) return;


//    numTexture = value;
//    switch (value)
//    {
//        case 0:
//            texture->load(qsResPath + "/numl1.jpg");
//        break;
//        case 1:
//            texture->load(qsResPath + "/numl2.jpg");

//        break;
//        case 2:
//            texture->load(qsResPath + "/numl3.jpg");

//        break;
//        case 3:
//            texture->load(qsResPath + "/numl4.jpg");

//        break;
//        case 4:
//            texture->load(qsResPath + "/numl5.jpg");

//        break;
//        case 5:
//            texture->load(qsResPath + "/numl6.jpg");

//        break;
//        case 6:
//            texture->load(qsResPath + "/numl7.jpg");

//        break;
//        case 7:
//            texture->load(qsResPath + "/numl8.jpg");

//        break;
//        case 8:
//            texture->load(qsResPath + "/numl9.jpg");

//        break;
//        case 9:
//            texture->load(qsResPath + "/numl10.jpg");

//        break;
//		case 10:
//			texture->load(qsResPath + "/numl11.jpg");

//		break;
//		case 11:
//			texture->load(qsResPath + "/numl12.jpg");

//			break;
//    }

//    m_pChartNumerology->preloadRes(texture->toImage());
//    unsigned int xW = topLevelLabel->width();
//    unsigned int yH = topLevelLabel->height();
//    PixMapResize(texture->size().width(), texture->size().height(), xW, yH);

//    if (checkCalculateData == true)
//    {
//        if (!getStatus())
//        {
//            m_pChartNumerology->calculate(m_pNumer, m_pNumerComp, m_pNumerPMatr, m_pNumerPMatrComp, true, true, numTexture);
//        }
//        else
//        {
//            m_pChartNumerology->calculate(m_pNumer, m_pNumerComp, m_pNumerPMatr, m_pNumerPMatrComp, true, false, numTexture);
//        }
//    }


//    topLevelLabel->setPixmap(QPixmap::fromImage(m_pChartNumerology->numChart).scaled(topLevelLabel->width(), topLevelLabel->height(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
}
//----------------------------------------------------------
void CVBNum::PixMapResize(unsigned int toWidth, unsigned int toHeight, unsigned int &fromtWidth, unsigned int &fromHeight)
{
    unsigned int hTmp = fromHeight;
    double pTmp = 0;

    if (toWidth > fromtWidth)
    {
        pTmp = (double)toWidth/(double)fromtWidth;

        fromtWidth = (double)toWidth/pTmp;
        fromHeight = (double)toHeight/pTmp;
    }

    if (fromHeight > hTmp)
    {
        pTmp = (double)fromHeight/(double)hTmp;

        fromtWidth = (double)fromtWidth/pTmp;
        fromHeight = (double)fromHeight/pTmp;
    }
}
//----------------------------------------------------------
void CVBNum::slotResizeChart()
{
//    if (m_iCMode == C_MODE_CHART)
//    {
//        if (xWP != topLevelLabel->width() && yHP != topLevelLabel->height())
//        {
//            xWP = topLevelLabel->width();
//            yHP = topLevelLabel->height();
//            unsigned int xW = topLevelLabel->width();
//            unsigned int yH = topLevelLabel->height();
//            PixMapResize(texture->size().width(), texture->size().height(), xW, yH);
//            //topLevelLabel->setPixmap(texture->scaled(xW, yH, Qt::KeepAspectRatio, Qt::SmoothTransformation));

//        }
//    }
}
//----------------------------------------------------------
void CVBNum::OnMenu()
{
    QPoint pos = mapToGlobal(m_pMenuButton->pos());
    m_Menu.popup(QPoint(pos.x(),pos.y()+20));
}
//----------------------------------------------------------
#ifdef VER_PREM
void CVBNum::OnReportManager()
{
    QString curReportName = m_pReportBox->currentText();

    CLayoutManagerDlgWin *dlg = new CLayoutManagerDlgWin(appstyle, curReportName, this);
    dlg->exec();

    if (dlg->mainWidget->res == 0)
    {
        m_pReportBox->clear();
        CLayoutManagerDlg::loadReportSettings(&settingReport);
        m_pReportBox->addItem("Default");
        for (int i = 0; i < settingReport.size(); i++)
        {
            m_pReportBox->addItem(settingReport.at(i).name);
        }
#ifdef Q_OS_MAC
        //m_pReportBox->view()->setMinimumHeight((15 * m_pReportBox->count()) + 15);
#endif
        int repId = m_pReportBox->findText(dlg->mainWidget->curSetting_);
        if (repId >= 0)
        {
            m_pReportBox->setCurrentIndex(repId);
        }
        else
        {
            if (m_pReportBox->count() > 0)
                m_pReportBox->setCurrentIndex(0);
        }
    }
    else
    {
        int repId = m_pReportBox->findText(curReportName);
        if (repId >= 0)
        {
            m_pReportBox->setCurrentIndex(repId);
        }
        else
        {
            if (m_pReportBox->count() > 0)
                m_pReportBox->setCurrentIndex(0);
        }
    }

    delete dlg;
}
#endif
//----------------------------------------------------------
void CVBNum::OnPrintPreview()
{
#ifdef VER_PREM
    if (getStatus() == false)
    {
        OnPurchasePRO();
        return;
    }
#endif


    if (blockUI)
    {
        QMessageBox::critical(NULL, tr("Error"), tr("Current report generation in progress!"), QMessageBox::Ok);
        return;
    }
    blockUI = true;

    QPrinter printer(QPrinter::HighResolution);
    QPrintDialog *dialog = new QPrintDialog(&printer, this);
    dialog->setWindowTitle(tr("VeBest Numerology Report"));
    if (dialog->exec() == QDialog::Accepted)
    {
        printer.setDocName("VeBest Numerology Report");
        printPreview(&printer);
    }
    blockUI = false;
}
//----------------------------------------------------------
void CVBNum::OnPrintPreviewAll()
{
#ifdef VER_PREM
    if (getStatus() == false)
    {
        OnPurchasePRO();
        return;
    }
#endif
    if (blockUI)
    {
        QMessageBox::critical(NULL, tr("Error"), tr("Current report generation in progress!"), QMessageBox::Ok);
        return;
    }
	if(!askReportsList())
		return;

    blockUI = true;
    QPrinter printer(QPrinter::HighResolution);
    QPrintDialog *dialog = new QPrintDialog(&printer, this);
    dialog->setWindowTitle(tr("VeBest Numerology Report"));
    if (dialog->exec() == QDialog::Accepted)
    {
        printer.setDocName("VeBest Numerology Report");
        printPreviewAll(&printer);
    }
    blockUI = false;
}
//----------------------------------------------------------
void CVBNum::printPreview(QPrinter *printer)
{
//            QMessageBox msgBox;
//            msgBox.setText(QString::number(cmode));
//            msgBox.exec();



    {
        if (cmode != C_MODE_CHART)
        {
            QString src;
            if ((cmode == C_MODE_NUMEROLOGY)||(cmode == C_MODE_FORECAST))
                embedImagesToHtml(m_pNumer->htmlDoc, src, false);
            else if (cmode == C_MODE_PMATR)
                embedImagesToHtml(m_pNumerPMatr->htmlDoc, src, false);
            else if (cmode == C_MODE_COMP)
                embedImagesToHtml(m_pCompNumerology->htmlDoc, src, false);
            else if (cmode == C_MODE_PET)
				embedImagesToHtml(m_pNumerPet->htmlDoc, src, false);
            else if (cmode == C_MODE_ANALIZE)
                embedImagesToHtml(m_pNumerAn->htmlDoc, src, false);
            else if (cmode == C_MODE_CEL)
                embedImagesToHtml(m_pNumerCel->htmlDoc, src, false);


//            QStringList tmList = src.split("<body>");

//            if (tmList.size() >= 2)
//                src = tmList.at(1);

//            tmList = src.split("</body>");
//            if (tmList.size() >= 2)
//                src = tmList.at(0);


//            QFile ff("c:/123.html");
//            if (ff.open(QFile::WriteOnly))
//            {
//                QByteArray bytarr;
//                bytarr.append(src);
//                ff.write(bytarr);
//                ff.close();
//            }


//            src.replace("<table border=\"0\"><tr><td>", "<br>");
//            src.replace("</td></tr></table>", "");



//            QStringList srcList = src.split("<br>");
//            src.clear();

//            for (int i = 0; i < srcList.size(); i++)
//            {
//                src.append("<table border=\"0\"><tr><td>");
//                src.append(srcList.at(i));
//                src.append("</td></tr></table>");
//            }

            if (src.size() < 300)
                return;

            QWebPage *wp = new QWebPage();
            wp->settings()->setAttribute(QWebSettings::AutoLoadImages, true);
            QEventLoop loop;
            QObject::connect(wp, SIGNAL(loadFinished(bool)), &loop, SLOT(quit()));
            QTimer::singleShot(10000, &loop, SLOT(quit()));

            wp->setViewportSize(QSize(800,800));
            QWebFrame *wf = wp->mainFrame();
            wf->setHtml(src, QUrl());

            if ((cmode != C_MODE_PET) && (cmode != C_MODE_ANALIZE))
                loop.exec();

            QWebElementCollection wec;
            wec = wf->documentElement().findAll("table.maindoc");
//QTextDocument *document = new QTextDocument();
            int ppos = 0;
            QPainter *p = new QPainter();
            p->begin(printer);
            double sc = printer->pageRect().width()/800;
            int pageh = printer->pageRect().height()/sc;
            int pageox = printer->pageRect().x()/sc;
            int pageoy = printer->pageRect().y()/sc;
            for (int n=0; n<wec.count(); n++)
            {
                int h = wec.at(n).geometry().height();
                if (ppos+h > pageh-50)
                {
                    ppos = 0;
                    printer->newPage();
                }

                //document->setHtml(wec.at(n).webFrame()->toHtml());
//                document->print(printer);
                p->resetTransform();
                p->scale(sc, sc);
                p->translate(pageox, pageoy + ppos);
                wec.at(n).render(p);
                qDebug() << n << ":" << wec.at(n).toPlainText().left(16);
                ppos += h;
            }
            p->end();
            delete p;
            //delete document;
            delete wp;
        }
        else if (cmode == C_MODE_CHART)
        {
            int value = comboChart->currentIndex();

            if (comboChart->count() <= 0)
                return;

            if (comboChart->count() <= value)
                return;

            QFile file(comboChart->itemData(value).toString());
            if (!file.open(QFile::ReadOnly))
                return;

            QDataStream stream(&file);

            calcNumContainer calcContainer;
            calcContainer.m_pNumer = m_pNumer;
            calcContainer.m_pNumerComp = m_pNumerComp;
            calcContainer.m_pNumerPMatr = m_pNumerPMatr;
            calcContainer.m_pNumerPMatrComp = m_pNumerPMatrComp;
            CReportCreatorWin *dlg = new CReportCreatorWin(&calcContainer, appstyle, this, THUMB_MODE);
            dlg->mainWidget->pageArea->load(stream);
            file.close();

            m_pChartNumerology->preloadRes(CManagerThumbnailDlg::slotPrintReport(0, dlg));

            delete dlg;


            QPainter painter(printer);

            //QRect r = printer->pageRect();
            QRect r = painter.viewport();

            int cx, cy, cw, ch, ox=0, oy=0;
            cx = r.x();
            cy = r.y();
            cw = r.width();
            ch = r.height();

            double k = 0.96;
            double sc = calcFitScale(m_pChartNumerology->numChart.width(), m_pChartNumerology->numChart.height(), cw, ch, ox, oy);
            sc *= k;
            cw = (int)((double)m_pChartNumerology->numChart.width()*sc);
            ch = (int)((double)m_pChartNumerology->numChart.height()*sc);
            cx += ox + ((double)cw*(1.0-k)*0.5);
            cy += oy + ((double)ch*(1.0-k)*0.5);

            QImage *img = &m_pChartNumerology->numChart;
            painter.drawImage(QRectF(cx,cy,cw,ch), *img, QRectF(0,0,img->width(), img->height()));
            painter.end();
        }
    }
}
//----------------------------------------------------------
void CVBNum::printPreviewAll(QPrinter *printer)
{
    int value = comboChart->currentIndex();

    if (comboChart->count() <= 0)
        return;

    if (comboChart->count() <= value)
        return;


    QFile file(comboChart->itemData(value).toString());
    if (!file.open(QFile::ReadOnly))
        return;

    QDataStream stream(&file);

    calcNumContainer calcContainer;
    calcContainer.m_pNumer = m_pNumer;
    calcContainer.m_pNumerComp = m_pNumerComp;
    calcContainer.m_pNumerPMatr = m_pNumerPMatr;
    calcContainer.m_pNumerPMatrComp = m_pNumerPMatrComp;
    CReportCreatorWin *dlg = new CReportCreatorWin(&calcContainer, appstyle, this, THUMB_MODE);
    dlg->mainWidget->pageArea->load(stream);
    file.close();

    m_pChartNumerology->preloadRes(CManagerThumbnailDlg::slotPrintReport(0, dlg));

    delete dlg;

    QList<int>lsz;
    lsz << 1 << 3 << 4 << 7 << 5 << 6 << 2;

    QPainter *p = new QPainter();
    p->begin(printer);

    int pidx=0;

    for (int a=0; a<lsz.size(); a++)
    {
        int lcmode = lsz.at(a);

        if ((lcmode==1)&&(!m_repList.rep[0]))
            continue;
        if ((lcmode==3)&&(!m_repList.rep[1]))
            continue;
        if ((lcmode==4)&&(!m_repList.rep[2]))
            continue;
        if ((lcmode==7)&&(!m_repList.rep[3]))
            continue;
        if ((lcmode==2)&&(!m_repList.rep[4]))
            continue;

        if (lcmode != 2)
        {
            QString src;
            if (lcmode == 1)
                embedImagesToHtml(m_pNumer->htmlDoc, src, false);
            else if (lcmode == 3)
                embedImagesToHtml(m_pNumerPMatr->htmlDoc, src, false);
            else if (lcmode == 4)
                embedImagesToHtml(m_pCompNumerology->htmlDoc, src, false);
            else if (lcmode == 5)
                src = m_pNumerPet->htmlDoc;
            else if (lcmode == 6)
                embedImagesToHtml(m_pNumerAn->htmlDoc, src, false);
            else if (lcmode == 7)
                embedImagesToHtml(m_pNumerCel->htmlDoc, src, false);

            if (src.size()<100)
                continue;

            if (pidx>0)
                printer->newPage();
            pidx++;

            QWebPage *wp = new QWebPage();
            wp->settings()->setAttribute(QWebSettings::AutoLoadImages, true);
            QEventLoop loop;
            QObject::connect(wp, SIGNAL(loadFinished(bool)), &loop, SLOT(quit()));
            QTimer::singleShot(10000, &loop, SLOT(quit()));

            wp->setViewportSize(QSize(800,800));
            QWebFrame *wf = wp->mainFrame();
            wf->setHtml(src, QUrl());

            if ((cmode != C_MODE_PET) && (cmode != C_MODE_ANALIZE))
                loop.exec();

            QWebElementCollection wec;
            wec = wf->documentElement().findAll("table");

            int ppos = 0;
            double sc = printer->pageRect().width()/800;
            int pageh = printer->pageRect().height()/sc;
            int pageox = printer->pageRect().x()/sc;
            int pageoy = printer->pageRect().y()/sc;
            for (int n=0; n<wec.count(); n++)
            {
                int h = wec.at(n).geometry().height();
                if (ppos+h > pageh-50)
                {
                    ppos = 0;
                    printer->newPage();
                }

                p->resetTransform();
                p->scale(sc, sc);
                p->translate(pageox, pageoy + ppos);
                wec.at(n).render(p);
                ppos += h;
            }
            delete wp;
        }
        else if (lcmode == 2)
        {
            if (pidx>0)
                printer->newPage();
            pidx++;

            p->resetTransform();
            QRect r = p->viewport();

            int cx, cy, cw, ch, ox=0, oy=0;
            cx = r.x();
            cy = r.y();
            cw = r.width();
            ch = r.height();

            double k = 0.96;
            double sc = calcFitScale(m_pChartNumerology->numChart.width(), m_pChartNumerology->numChart.height(), cw, ch, ox, oy);
            sc *= k;
            cw = (int)((double)m_pChartNumerology->numChart.width()*sc);
            ch = (int)((double)m_pChartNumerology->numChart.height()*sc);
            cx += ox + ((double)cw*(1.0-k)*0.5);
            cy += oy + ((double)ch*(1.0-k)*0.5);

            QImage *img = &m_pChartNumerology->numChart;
            p->drawImage(QRectF(cx,cy,cw,ch), *img, QRectF(0,0,img->width(), img->height()));
        }
    }

    p->end();
    delete p;
}
//----------------------------------------------------------
void CVBNum::embedImagesToHtml(QString &src, QTextStream &dst, bool addAutoPrint)
{
    QString psrc;
    QStringList tlst;
    if (addAutoPrint)
    {
        tlst = src.split("<body>");
        if (tlst.size()==2)
        {
            psrc = tlst.at(0)
            + tr("<body onload=\"window.print()\">")
            + tlst.at(1);
        }
    }

    QStringList srcLst;
    if (addAutoPrint)
        srcLst = psrc.split("<img src=\"");
    else
        srcLst = src.split("<img src=\"");

    for (int a=0; a<srcLst.size(); a++)
    {
        if (a==0)
            dst << srcLst.at(a);
        else
        {
            QStringList sl = srcLst.at(a).split("\"");
            if (sl.size()>1)
            {
                QString fn = sl.at(0);
                if (fn.endsWith("png", Qt::CaseInsensitive))
                    dst << "<img src=\"data:image/png;base64,";
                else if (fn.endsWith("jpg", Qt::CaseInsensitive))
                    dst << "<img src=\"data:image/jpg;base64,";

				if (fn.startsWith("file:///", Qt::CaseInsensitive)) {
					fn.remove("file:///", Qt::CaseInsensitive);
				}
				QFile res1(fn);
                res1.open(QFile::ReadOnly);
                QByteArray bares1 = res1.readAll();
                dst << bares1.toBase64();
                res1.close();
            }
            for (int i=1; i<sl.size(); i++)
            {
                dst << "\"";
                dst << sl.at(i);
            }
        }
    }
}
//----------------------------------------------------------
void CVBNum::embedImagesToHtml(QString &src, QString &dst, bool addAutoPrint)
{
    QString psrc;
    QStringList tlst;
    if (addAutoPrint)
    {
        tlst = src.split("<body>");
        if (tlst.size()==2)
        {
            psrc = tlst.at(0)
            + tr("<body onload=\"window.print()\">")
            + tlst.at(1);
        }
    }

    QStringList srcLst;
    if (addAutoPrint)
        srcLst = psrc.split("<img src=\"");
    else
        srcLst = src.split("<img src=\"");

    for (int a=0; a<srcLst.size(); a++)
    {
        if (a==0)
            dst += srcLst.at(a);
        else
        {
            QStringList sl = srcLst.at(a).split("\"");
            if (sl.size()>1)
            {
                QString fn = sl.at(0);
                if (fn.endsWith("png", Qt::CaseInsensitive))
                    dst += "<img src=\"data:image/png;base64,";
                else if (fn.endsWith("jpg", Qt::CaseInsensitive))
                    dst += "<img src=\"data:image/jpg;base64,";

				if (fn.startsWith("file:///", Qt::CaseInsensitive)) {
					fn.remove("file:///", Qt::CaseInsensitive);
				}
                QFile res1(fn);
                res1.open(QFile::ReadOnly);
                QByteArray bares1 = res1.readAll();
                dst += bares1.toBase64();
                res1.close();
            }
            for (int i=1; i<sl.size(); i++)
            {
                dst += "\"";
                dst += sl.at(i);
            }
        }
    }
}
//----------------------------------------------------------
void CVBNum::OnExportRTF()
{
#ifdef VER_PREM
    if (getStatus() == false)
    {
        OnPurchasePRO();
        return;
    }
#endif
    QString fileName;
    if (cmode == C_MODE_CHART)
        fileName = QFileDialog::getSaveFileName(this, QString("Save to PNG"),
                                                QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation), "PNG Files (*.png)");
    else
        fileName = QFileDialog::getSaveFileName(this, QString("Save to HTML"),
                                                QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation), "HTML Files (*.html)");
    if (!fileName.isEmpty())
    {
        if (QFileInfo(fileName).suffix().isEmpty())
        {
            if (cmode == C_MODE_CHART)
                fileName.append(".png");
            else
                fileName.append(".html");
        }
        if ((cmode == C_MODE_NUMEROLOGY) || (cmode == C_MODE_FORECAST))
        {
            QFile f(fileName);
            f.open(QFile::WriteOnly);
            QTextStream s(&f);
            //s << m_pNumer->m_TextDoc.toHtml();
            //s << m_pNumer->htmlDoc;
            embedImagesToHtml(m_pNumer->htmlDoc, s);
            f.close();
        }
        else if (cmode == C_MODE_CHART)
        {

            getChart().save(fileName);
        }
        else if (cmode == C_MODE_PMATR)
        {
            QFile f(fileName);
            f.open(QFile::WriteOnly);
            QTextStream s(&f);
            embedImagesToHtml(m_pNumerPMatr->htmlDoc, s);
            f.close();
        }
        else if (cmode == C_MODE_COMP)
        {
            QFile f(fileName);
            f.open(QFile::WriteOnly);
            QTextStream s(&f);
            embedImagesToHtml(m_pCompNumerology->htmlDoc, s);
            f.close();
        }
        else if (cmode == C_MODE_PET)
        {
            QFile f(fileName);
            f.open(QFile::WriteOnly);
            QTextStream s(&f);
            embedImagesToHtml(m_pNumerPet->htmlDoc, s);
            f.close();
        }
        else if (cmode == C_MODE_ANALIZE)
        {
            QFile f(fileName);
            f.open(QFile::WriteOnly);
            QTextStream s(&f);
            embedImagesToHtml(m_pNumerAn->htmlDoc, s);
            f.close();
        }
        else if (cmode == C_MODE_CEL)
        {
            QFile f(fileName);
            f.open(QFile::WriteOnly);
            QTextStream s(&f);
            embedImagesToHtml(m_pNumerCel->htmlDoc, s);
            f.close();
        }
    }
}
//----------------------------------------------------------
void CVBNum::OnExportPDF()
{
#ifdef VER_PREM
    if (getStatus() == false)
    {
        OnPurchasePRO();
        return;
    }
#endif
    if (blockUI)
    {
        QMessageBox::critical(NULL, tr("Error"), tr("Report generation in progress!"), QMessageBox::Ok);
        return;
    }

    QString fileName = QFileDialog::getSaveFileName(this, QString("Export to PDF"),
                                                    QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation), "PDF Files (*.pdf)");
    if (!fileName.isEmpty())
    {
        if (QFileInfo(fileName).suffix().isEmpty())
            fileName.append(".pdf");

        QPrinter printer(QPrinter::HighResolution);
        printer.setDocName("VeBest Numerology Report");
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOutputFileName(fileName);
        blockUI = true;
        printPreview(&printer);
        blockUI = false;
    }
}
//----------------------------------------------------------
void CVBNum::OnExportPDFAll()
{
#ifdef VER_PREM
    if (getStatus() == false)
    {
        OnPurchasePRO();
        return;
    }
#endif
    if (blockUI)
    {
        QMessageBox::critical(NULL, tr("Error"), tr("Report generation in progress!"), QMessageBox::Ok);
        return;
    }
	if(!askReportsList())
		return;

    QString fileName = QFileDialog::getSaveFileName(this, QString("Export to PDF"),
                                                    QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation), "PDF Files (*.pdf)");
    if (!fileName.isEmpty())
    {
        if (QFileInfo(fileName).suffix().isEmpty())
            fileName.append(".pdf");

        QPrinter printer(QPrinter::HighResolution);
        printer.setDocName("VeBest Numerology Report");
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOutputFileName(fileName);
        blockUI = true;
        printPreviewAll(&printer);
        blockUI = false;
    }
}


bool CVBNum::askReportsList()
{
    for (int a=0; a<10; a++)
        m_repList.rep[a]=true;

    QDialog *dlg = new QDialog();
	QString numType = " Western Numerology";
	if (m_uCNC->isChecked())
		numType = " Chaldean Numerology";
    QCheckBox *box1 = new QCheckBox(numType, dlg);
    QCheckBox *box2 = new QCheckBox(" Psychomatrix", dlg);
    QCheckBox *box3 = new QCheckBox(" Compatibility", dlg);
    QCheckBox *box4 = new QCheckBox(" Celebrities", dlg);
    QCheckBox *box5 = new QCheckBox(" Graphic Chart", dlg);
    int x=10, y=10, sy=30, w=200, h=25;
    box1->setGeometry(x, y+sy*0, w, h);
    box2->setGeometry(x, y+sy*1, w, h);
    box3->setGeometry(x, y+sy*2, w, h);
    box4->setGeometry(x, y+sy*3, w, h);
    box5->setGeometry(x, y+sy*4, w, h);
    box1->setChecked(true);
    box2->setChecked(true);
    box3->setChecked(true);
    box4->setChecked(true);
    box5->setChecked(true);
    int dw=w+x*2, dh=215;
    dlg->resize(dw, dh);
    QPushButton *butOk = new QPushButton("Ok", dlg);
    butOk->setGeometry(dw/2-40, dh-40, 80, 30);
    QObject::connect(butOk, SIGNAL(clicked()), dlg, SLOT(accept()));
    dlg->setStyleSheet(qsGlobalDlgStyle);

    dlg->setStyleSheet(appstyle->updateColor(dlg->styleSheet()));

	int res = dlg->exec();
    if (!box1->isChecked())
        m_repList.rep[0]=false;
    if (!box2->isChecked())
        m_repList.rep[1]=false;
    if (!box3->isChecked())
        m_repList.rep[2]=false;
    if (!box4->isChecked())
        m_repList.rep[3]=false;
    if (!box5->isChecked())
        m_repList.rep[4]=false;

    delete dlg;

	if(res==QDialog::Accepted)
		return true;
	else
		return false;

}

//----------------------------------------------------------
#define TCHAR char
//----------------------------------------------------------
//void Crypt(char *inp, unsigned int inplen, char* key, unsigned int keylen)
//{
//    TCHAR Sbox[257], Sbox2[257];
//    unsigned long i, j, t, x;

//    static const char OurUnSecuredKey[] = "VebestKeyDefault" ;
//    static const unsigned int OurKeyLen = strlen(OurUnSecuredKey);
//    TCHAR temp , k;
//    i = j = k = t =  x = 0;
//    temp = 0;

//        for (int a=0;a<257;a++)
//        {
//            Sbox[a] = 0;
//            Sbox2[a] = 0;
//        }

//    for(i = 0; i < 256U; i++)
//    {
//        Sbox[i] = (TCHAR)i;
//    }

//    j = 0;
//    if(keylen)
//    {
//        for(i = 0; i < 256U ; i++)
//        {
//            if(j == keylen)
//            {
//                j = 0;
//            }
//            Sbox2[i] = key[j++];
//        }
//    }
//    else
//    {
//        for(i = 0; i < 256U ; i++)
//        {
//            if(j == OurKeyLen)
//            {
//                j = 0;
//            }
//            Sbox2[i] = OurUnSecuredKey[j++];
//        }
//    }

//    j = 0;

//    for(i = 0; i < 256; i++)
//    {
//        j = (j + (unsigned long) Sbox[i] + (unsigned long) Sbox2[i]) % 256U ;
//        temp =  Sbox[i];
//        Sbox[i] = Sbox[j];
//        Sbox[j] =  temp;
//    }

//    i = j = 0;
//    for(x = 0; x < inplen; x++)
//    {
//        i = (i + 1U) % 256U;
//        j = (j + (unsigned long) Sbox[i]) % 256U;
//        temp = Sbox[i];
//        Sbox[i] = Sbox[j] ;
//        Sbox[j] = temp;
//        t = ((unsigned long) Sbox[i] + (unsigned long) Sbox[j]) %  256U ;
//        k = Sbox[t];
//        inp[x] = (inp[x] ^ k);
//    }
//}
////----------------------------------------------------------
//char randstart[]="Afh9-lj[[[dsfm9i6jDgfbvBV1";
//----------------------------------------------------------

QString getClipboardText()
{
    QClipboard * clipboard = QApplication::clipboard();
    QString text = clipboard->text();

    return text;
}

QString filterSerialNumber(QString text)
{
    text = text.simplified();

    text = text.toUpper();
    QString rtext = "";
    for (int a=0; a<text.size(); a++) {
        QChar ch = text[a];
        if (ch.isLetter() || ch.isDigit() || (ch==QChar('-'))) {
            rtext += ch;
        }
    }

    return rtext;
}



void CVBNum::OnRegister()
{    
//#ifdef nocompile    //must uncomment
#ifndef APPSTORE
    QString sernum = m_serialNumber;
    bool ok = false;
    /*sernum = QInputDialog::getText(this, tr("Software registration"),
        tr("Serial number: "), QLineEdit::Normal,
        sernum, &ok);*/



    CRegform rform;

    rform.setStyleSheet(appstyle->updateColor(rform.styleSheet()));

    if (sernum.length()<5) {
        QString ct = getClipboardText();
        if ((ct.length()>3)&&(ct.length()<100))
            ct = filterSerialNumber(ct);
        if (ct.startsWith(LICPREF) || ct.startsWith(LICPREF2))
            sernum = ct;
    }
    rform.ui.lineEdit->setText(sernum);
#ifdef Q_WS_WIN
        //QString s = "kAjEjLpDoEgCcPeNaClChJfFmAnMgIjOiPfLbAlAcJkDjEkPlFeHfPfJpMiGbFgNlNlKcNkJdKeGjDePeMpMaKiElLeNaFfAjJaBmJkCiDpFlNpOdDdNdDpGeIgFkAmLhCbAmOhLbAdCdLkAfEiMmOkLmMcBoInOiEhE";
        QString s = "kAjEjLpDoEgCcPaNdCdDlMgEhEkMaJpPaOhPhAgBoJbGmFgKkFfHePjNiMfDpBnMeMgPdMoNpNbAjFoOnJoInPkBlPkImAdFhMpEbPpCiCpAfP";
#else
        QString s = "kAjEjLpDoEgCcPaNdCdDlMgEhEkMaJpPaOhPhAgBoJbGmFgKkFfHePjNiMfDpBnMeMgPdMoNpNoFoBoKgJjIpPdBgOcIjBbBnMhFmLjGeDgBePgN";
#endif
    getpString(s);
    rform.caddr = s;
#ifdef VER_PREM
	rform.caddr = "http://www.vebest.com/products/hobby/numerologypro.html";
#endif
    if (rform.exec() == QDialog::Accepted)
    {
        ok = true;
        sernum = rform.ui.lineEdit->text();
    }

	if (ok)
	{
		sernum = sernum.toUpper();
		m_serialNumber = "";
		for (int a=0; a<sernum.size(); a++) {
			QChar ch = sernum[a];
			if (ch.isLetter() || ch.isDigit() || (ch==QChar('-'))) {
				m_serialNumber += ch;
			}
		}

		saveData();
	}
    if (ok && !m_serialNumber.isEmpty())
    {
#ifdef VER_PREM
        if (m_serialNumber.startsWith(LICPREF)||m_serialNumber.startsWith(LICPREF2))
		{
			CSecLicDlg *dlg = new CSecLicDlg();

			lic->registerSN(m_serialNumber);
			dlg->startRegistration(lic);
			delete dlg;

			getStatus();
			resizeEvent(0);
			repaint();
		} else
		{
			QString Msg = tr("Error 10: Wrong serial number!");
			if (m_serialNumber.startsWith("VE"))
				Msg = tr("Error 11: Trying to register wrong version!\r\nDownload Numerology Everywhere!");
			if (m_serialNumber.startsWith("VN"))
				Msg = tr("Error 11: Trying to register wrong version!\r\nDownload Standard Numerology!");
			QMessageBox::critical(NULL, tr(""), Msg, QMessageBox::Ok);
		}
#else
#ifdef Q_WS_WIN
        if (m_serialNumber.startsWith("IPRL"))
        {
            /*char * csLicense = "IPRL-6H9DB8-3HCCLE85-83FD9LL7-76FH8A4B-A9-D243";
            if (IntelliProtectorService::CIntelliProtector::RegisterSoftwareA(csLicense))
            {
                QString Msg = tr("Software successfully registered!");
                QMessageBox::information(NULL, tr("Registration"), Msg, QMessageBox::Ok);
            } else
            {
                QString Msg = tr("Error! Wrong serial number or Internet connection fault!");
                QMessageBox::critical(NULL, tr(""), Msg, QMessageBox::Ok);
            }*/

            char sercode[1024];
#ifdef _MSC_VER
            strcpy_s(sercode, 1000, m_serialNumber.toAscii().constData());
#else
			strcpy(sercode, m_serialNumber.toAscii().constData());
#endif
            if (IntelliProtectorService::CIntelliProtector::RegisterSoftwareA(sercode))
            {
                QString Msg = tr("Software successfully registered!");
                QMessageBox::information(NULL, tr("Registration"), Msg, QMessageBox::Ok);
            } else
            {
                QString Msg = tr("Error! Wrong serial number or Internet connection fault!");
                QMessageBox::critical(NULL, tr(""), Msg, QMessageBox::Ok);
            }
        }
#endif
#ifdef MAC_ES_LIC
        else if (m_serialNumber.startsWith("VNUM"))
        {
            if (getStatus())
            {
                QString Msg = tr("Software successfully registered!");
                QMessageBox::information(NULL, tr("Registration"), Msg, QMessageBox::Ok);
            } else
            {
                QString Msg = tr("Error! Wrong serial number or Internet connection fault!");
                QMessageBox::critical(NULL, tr(""), Msg, QMessageBox::Ok);
            }
        }
#endif
#ifdef Q_WS_WIN
        else
#endif
        if (m_serialNumber.startsWith(LICPREF) || m_serialNumber.startsWith(LICPREF2))
        {
            CSecLicDlg *dlg = new CSecLicDlg();

            lic->registerSN(m_serialNumber);
            dlg->startRegistration(lic);
            delete dlg;

            getStatus();
            resizeEvent(0);
            repaint();
        } else
		{
			QString Msg = tr("Error! Wrong serial number!");
			QMessageBox::critical(NULL, tr(""), Msg, QMessageBox::Ok);
		}
#endif
    }
	if (registermode==1)
		OnCalcNum();
#else
    // App Store
    CRegform rform;
    rform.ui.layoutWidget->move(90, 320);
    rform.setMinimumSize(380, 360);
    rform.setMaximumSize(380, 360);
    rform.resize(380, 360);
    rform.ui.lineEdit->hide();
    rform.ui.pushButton->hide();
    rform.setWindowTitle("Upgrade");

    if (rform.exec() == QDialog::Accepted)
    {
        QDesktopServices::openUrl(QUrl("macappstore://itunes.apple.com/app/id485398669?mt=12", QUrl::TolerantMode));
    }
#endif
//#endif
}
//----------------------------------------------------------
void CVBNum::setFreeManual() {
	manualStartPage = QString("_index.html");
#ifdef Q_OS_MAC
    QString qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/manual/");
#else
	QString qsResPath = QCoreApplication::applicationDirPath() + QString("/images/manual/");
#endif
	m_pTextGuide->load(QUrl("file:///" + qsResPath + manualStartPage));
}

void CVBNum::setFullManual() {
	manualStartPage = QString("index.html");
#ifdef Q_OS_MAC
    QString qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/manual/");
#else
	QString qsResPath = QCoreApplication::applicationDirPath() + QString("/images/manual/");
#endif
	m_pTextGuide->load(QUrl("file:///" + qsResPath + manualStartPage));
}

//----------------------------------------------------------
bool CVBNum::getStatus()
{
#ifdef APPSTORE
#ifdef VER_FREE
	setFreeManual();
    m_pPurchasePRO->show();
    return false;
#else
	setFullManual();
    return true;
#endif
#else
    registermode = 0;
	setFreeManual();
    if (m_serialNumber.count()!=0)
    {
#ifdef VER_PREM
        if (m_serialNumber.startsWith(LICPREF)||m_serialNumber.startsWith(LICPREF2))
        {
            if (lic->getStatus()==1)
            {
				setFullManual();
                registermode = 1;
                m_pPurchasePRO->hide();
                return true;
            } else
            {
                return false;
            }
        }
#else
        if (m_serialNumber.startsWith(LICPREF)||m_serialNumber.startsWith(LICPREF2))
        {
            if (lic->getStatus()==1)
            {
				setFullManual();
                registermode = 1;
                m_pPurchasePRO->hide();
                return true;
            } else
            {
                return false;
            }
        }/* else if (m_serialNumber.startsWith("VNUM"))
        {
#ifdef Q_WS_WIN
            eSellerate_DaysSince2000 timeStamp;
            timeStamp = eSellerate_DateActivation(m_publisherID.toAscii().constData(),
                m_activationID.toAscii().constData(),
                m_serialNumber.toAscii().constData());
            if (timeStamp + 95 < eSellerate_Today())
#else
#ifdef MAC_ES_LIC
                       unsigned int timeStamp;
                       timeStamp = eWeb_DateActivation(m_publisherID.toAscii().constData(),
                               m_activationID.toAscii().constData(),
                               m_serialNumber.toAscii().constData());
                       if (timeStamp + 95 < eWeb_Today())
#endif
#endif
            {
#ifdef Q_WS_WIN
                                eSellerate_ErrorCode resultCode = eSellerate_ActivateSerialNumber(m_publisherID.toAscii().constData(),
                    m_activationID.toAscii().constData(),
                    m_serialNumber.toAscii().constData(),
                    0);
                if (resultCode != eSellerate_SUCCESS)
#else
#ifdef MAC_ES_LIC
                               OSStatus resultCode = eWeb_ActivateSerialNumber(m_publisherID.toAscii().constData(),
                                       m_activationID.toAscii().constData(),
                                       m_serialNumber.toAscii().constData(),
                                       0);
                               if (resultCode != E_SUCCESS)
#endif
#endif

#ifndef MAC_ES_LIC
#else
                {
                    QString Msg = tr("Error:") + QString::number(resultCode) + tr("! Unable to update software license.\r\nEnable Internet access and restart application.\r\nProgramm will run in Trial mode!");
                    QMessageBox::critical(NULL, tr(""), Msg, QMessageBox::Ok);

                    return false;
                }
#endif

                registermode = 1;
                m_pPurchasePRO->hide();
                return true;
            }
#ifdef Q_WS_WIN
            eSellerate_DaysSince2000 etoday = eSellerate_Today();
            if (timeStamp + 90 < etoday)
            {
                eSellerate_ActivateSerialNumber(m_publisherID.toAscii().constData(),
                    m_activationID.toAscii().constData(),
                    m_serialNumber.toAscii().constData(),
                    0);
            }
#else
#ifdef MAC_ES_LIC
                        unsigned int etoday = eWeb_Today();
                        if (timeStamp + 90 < etoday)
                        {
                                eWeb_ActivateSerialNumber(m_publisherID.toAscii().constData(),
                                        m_activationID.toAscii().constData(),
                                        m_serialNumber.toAscii().constData(),
                                        0);
                        }
#endif
#endif
            registermode = 1;
            m_pPurchasePRO->hide();
            return true;
                }*/
#ifdef Q_WS_WIN
        else if (m_serialNumber.startsWith("IPRL"))
        {
            if (IntelliProtectorService::CIntelliProtector::IsSoftwareRegistered())
            {
                registermode = 1;
                m_pPurchasePRO->hide();
                return true;
            }
        }
#endif

        QString Msg = tr("Error! Wrong serial number!");
        QMessageBox::critical(NULL, tr(""), Msg, QMessageBox::Ok);

#endif
        return false;
    } else
    {
        return false;
    }
#endif
}
//----------------------------------------------------------
//bool EncryptString( QString &cr )
//{
//    char buf[4096];
//    int clen;

//    if ( cr == "" )
//    {
//        cr = "JShd7w0-vcnb6943";
//    }

//    cr = cr + "A" + cr + "A";

//    strcpy( buf, cr.toAscii().data() );

//    clen = strlen(buf);
//    Crypt( buf, clen, ckey, strlen(ckey) );
//    cr = "";
//    for ( int a=0; a<clen; a++ )
//    {
//        cr += (buf[a] & 0x0f) + 'a';
//        cr += ((buf[a] >> 4) & 0x0f) + 'A';
//    }

//    return true;
//}
//----------------------------------------------------------
void CVBNum::saveData()
{
    /*QString AppPath;
    getDefaultFolder(AppPath);

    QDir dir(AppPath);
    if (!dir.exists(AppPath))
    {
        dir.mkpath(AppPath);
        firstStart = true;
    }*/

  //  setDialog.param1 = checkBoxLP1

//            checkBoxLP1
//            checkBoxLP2
//            checkBoxLP3
//            checkBoxNameWD
//            checkBoxNameDD
//    QStandardPaths::writableLocation(QStandardPaths::DataLocation);
//    QDesktopServices::storageLocation(QDesktopServices::DataLocation);
#ifdef VER_PREM
    QString appName = Q_APP_TARGET;
    QString FileName = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
    if (FileName.endsWith(appName))
        FileName.chop(appName.size()+1);
    FileName += "/vbnum_prem.cfg";
#else
    QString appName = Q_APP_TARGET;
    QString FileName = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
    if (FileName.endsWith(appName))
        FileName.chop(appName.size()+1);
    FileName += "/vbnum6.cfg";
#endif

    QFile f(FileName);

    if (!f.exists())
        firstStart = true;

//    QString str;
//    QDate dat;
        //int i;
    QString sn = m_serialNumber;
    EncryptString(sn);
    if(f.open(QIODevice::WriteOnly))
    {
        QDataStream s(&f);

        s << QString("BAPWNUMFILE");
        //i = users.size();
        s << sn;
        s << lastUpdate;

        QByteArray globS = QByteArray((const char*)&m_GlobSettings, sizeof(m_GlobSettings));

        s << globS;
        s << skipStartManual;
        s << styleAt;
        s << inpDateFormat;
        s << mmcalcTo99;
        /*s << i;
        for (int n=0; n<i; n++)
        {
            SUserInfo inf = users[n];
            str = inf.fullName;
            s << str;
            str = inf.shortName;
            s << str;
            dat = inf.birthDate;
            s << dat;

            s << QString("SEPARATOR");
        }*/
        f.close();
    }
}
//----------------------------------------------------------
//bool DecryptString( QString &cr )
//{
//    char buf[4096];
//    int clen;

//    if ( cr.size() < 8 )
//        return false;

//    clen = cr.size()/2;
//    for ( int a=0; a<clen; a++ )
//    {
//        buf[a] = (cr[a*2].toAscii() - 'a') + (cr[a*2+1].toAscii() - 'A')*16;
//    }
//    Crypt( buf, clen, ckey, strlen(ckey) );

//    cr = "";

//    for ( int a=0; a<clen/2; a++ )
//    {
//        if ( buf[a] != buf[clen/2+a] )
//            return false;
//    }

//    for ( int a=0; a<clen/2-1; a++ )
//        cr += buf[a];

//    if ( cr == "JShd7w0-vcnb6943" )
//    {
//        cr = "";
//    }

//    return true;
//}
//----------------------------------------------------------
void CVBNum::loadData()
{
    m_serialNumber = "";

    users.clear();
    //QString AppPath;
    //getDefaultFolder(AppPath);
#ifdef VER_PREM
    QString appName = Q_APP_TARGET;
    QString FileName = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
    if (FileName.endsWith(appName))
        FileName.chop(appName.size()+1);
    FileName += "/vbnum_prem.cfg";
#else
    QString appName = Q_APP_TARGET;
    QString FileName = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
    if (FileName.endsWith(appName))
        FileName.chop(appName.size()+1);
    FileName += "/vbnum6.cfg";
#endif
    QFile f(FileName);

    QString str;
        //int i;

    bool ver6 = f.open(QIODevice::ReadOnly);
    bool ver5 = false;
#ifndef VER_PREM
	if (ver6 == false)
    {
        FileName = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
            if (FileName.endsWith(appName))
                FileName.chop(appName.size()+1);
            FileName += "/vbnum5.cfg";
        f.setFileName(FileName);
        ver5 = f.open(QIODevice::ReadOnly);
    }
#endif

    if(ver6 == true || ver5 == true)
    {
        QDataStream s(&f);
        s >> str;
        if (str==QString("BAPWNUMFILE"))
        {
            s >> m_serialNumber;
            s >> lastUpdate;
            if (ver6 == true)
            {
                QByteArray globS;// = QByteArray((const char*)&m_GlobSettings, sizeof(m_GlobSettings));
                s >> globS;
                if (globS.size() == sizeof(m_GlobSettings))
                    memcpy(&m_GlobSettings, globS.data(), sizeof(m_GlobSettings));
            }
            if (!s.atEnd())
                s >> skipStartManual;
            if (!s.atEnd())
                s >> styleAt;
            if (!s.atEnd())
                s >> inpDateFormat;
            if (!s.atEnd())
                s >> mmcalcTo99;
#ifndef VER_PREM
            mmcalcTo99 = CNumerology::MNM_11TO33;
#endif
            //OnSettings();
            /*s >> i;
            for (int n=0; n<i; n++)
            {
                SUserInfo inf;
                s >> str;
                inf.fullName = str;
                s >> str;
                inf.shortName = str;
                s >> dat;
                inf.birthDate = dat;

                inf.entryText = genEntryText(inf);

                s >> str;
                if ( str != QString("SEPARATOR"))
                    break;

                users.append(inf);
            }*/
        }
        f.close();
    } else
    {
        m_serialNumber = "";
        lastUpdate = QDateTime(QDate(2000, 1, 1), QTime(0,0,0));
    }
    DecryptString(m_serialNumber);

    //for (int n=0; n<users.size(); n++)
    //	m_pNameCombo->addItem(users[n].entryText);
}

void CVBNum::checkForUpdates()
{
#ifndef APPSTORE
	QNetworkRequest request;
	request.setUrl(QUrl(UPDURL));
	request.setHeader(QNetworkRequest::ContentTypeHeader,QVariant("application/x-www-form-urlencoded"));
	request.setRawHeader("User-Agent", "Check for updates");
	nwmanager->get(request);

    lastUpdate = QDateTime::currentDateTime();
#endif
}

//----------------------------------------------------------
void CVBNum::replyFinished(QNetworkReply *reply)
{
#ifndef APPSTORE
    if (reply->error() == QNetworkReply::NoError)
    {
        QByteArray bytes = reply->readAll();
        QString repstr(bytes);

        QStringList replistc = repstr.split(",");
        if (replistc.size()>=1)
        {
            bool ok=false;
            int mcv = replistc.at(0).toInt(&ok, 16);
            if (ok && (mcv > VBNUMVER))
            {
                // New version available
                int res = QMessageBox::question(NULL, "VeBest Numerology Update", "New version of VeBest Numerology available for download\r\nWould you like to update?", QMessageBox::Yes, QMessageBox::No);
                if (res==QMessageBox::Yes)
                {
                    QDesktopServices::openUrl(QUrl(UPDDWNURL, QUrl::TolerantMode));
                }
            }
        }
    }

#else
    Q_UNUSED(reply);
#endif
}
//----------------------------------------------------------
void CVBNum::OnPurchasePRO()
{
    OnRegister();
}
//----------------------------------------------------------
void CVBNum::OnFinishedSU()
{
    loadData();
    setDateFormat(inpDateFormat);

    if (styleAt < (unsigned int)appstyle->stylesNames.size())
        m_pStyleSubMenu->actions().at(styleAt)->setChecked(true);

    OnStyleChange();

    OnOpenFile(true);
    OnCalcNum();

    if (buttonRun != "ENTER YOUR NAME")
    {
        persButAnum.stop();
        appstyle->setWidgetStyle(m_pSelPersBut, WTQsPersSelBut);
    }

    m_pSelPersBut->setText(buttonRun);

    OnUpdateListReport();
    repaintChart();

    delete m_pStartUpScreen;

    resizeEvent(0);


    isLoaded = true;

	//skipStartManual =false;
    if (skipStartManual == false)
    {
        CManualVieW *dlg = new CManualVieW(appstyle, &skipStartManual, this);
        dlg->exec();
		delete dlg;
    }
}
//----------------------------------------------------------
void CVBNum::OnEditChart()
{
    int value = comboChart->currentIndex();
    if (comboChart->count() <= 0)
        return;
    if (comboChart->count() <= value)
        return;

    QFile file(comboChart->itemData(value).toString());
    if (!file.open(QFile::ReadOnly))
        return;

    QDataStream stream(&file);

    calcNumContainer calcContainer;
    calcContainer.m_pNumer = m_pNumer;
    calcContainer.m_pNumerComp = m_pNumerComp;
    calcContainer.m_pNumerPMatr = m_pNumerPMatr;
    calcContainer.m_pNumerPMatrComp = m_pNumerPMatrComp;
    CReportCreatorWin *dlg = new CReportCreatorWin(&calcContainer, appstyle, this, THUMB_MODE, &registermode);
    dlg->mainWidget->path = comboChart->itemData(value).toString();
    dlg->mainWidget->init();
    dlg->mainWidget->pageArea->load(stream);
    file.close();

    connect(dlg->mainWidget, SIGNAL(rigister()), this, SLOT(OnPurchasePRO()));

    dlg->exec();

    delete dlg;
//    if (comboChart->count() == 10)
//    {
//        return;
//    }

    OnUpdateListReport();
    if (comboChart->count() <= 0)
        return;
    if (comboChart->count() <= value)
        return;
    comboChart->setCurrentIndex(value);
    resizeEvent(0);
}
//----------------------------------------------------------
void CVBNum::OnAddChart()
{
    int value = comboChart->currentIndex();
    calcNumContainer calcContainer;
    calcContainer.m_pNumer = m_pNumer;
    calcContainer.m_pNumerComp = m_pNumerComp;
    calcContainer.m_pNumerPMatr = m_pNumerPMatr;
    calcContainer.m_pNumerPMatrComp = m_pNumerPMatrComp;
    CReportCreatorWin *dlg = new CReportCreatorWin(&calcContainer, appstyle, this, THUMB_MODE, &registermode);
    dlg->mainWidget->pageArea->m_FileName = "";
    dlg->mainWidget->init();

    connect(dlg->mainWidget, SIGNAL(rigister()), this, SLOT(OnPurchasePRO()));

    dlg->mainWidget->OnProperties();

    dlg->exec();
    delete dlg;

    OnUpdateListReport();

    if (comboChart->count() <= 0)
        return;
    if (comboChart->count() <= value)
        return;
    comboChart->setCurrentIndex(value);
    resizeEvent(0);
}
//----------------------------------------------------------
void CVBNum::OnDeleteChart()
{
    int value = comboChart->currentIndex();
    if (comboChart->count() <= 0)
        return;
    if (comboChart->count() <= value)
        return;

    if (CInfoDlgWin::showAskMessageBox(appstyle, "Question", "Do you want to remove \"" + QFileInfo(comboChart->itemData(value).toString()).fileName() + "\"?", this) != 0)
        return;

    QFile::remove(comboChart->itemData(value).toString());

    OnUpdateListReport();
    resizeEvent(0);
}
//----------------------------------------------------------
void CVBNum::initStyle()
{
    QImage img;

    img.load(qsResPath+"numer-but.png");
    img = CAppStyle::drawRectToImage(img, 0, 0, 59, 59, 1, appstyle->updateColor(QColor("#5c524c")));
    m_pCtrlButNumer->setIcon(QIcon(QPixmap::fromImage(img)));

    img.load(qsResPath+"matr-but.png");
    img = CAppStyle::drawRectToImage(img, 0, 0, 59, 59, 1, appstyle->updateColor(QColor("#5c524c")));
    m_pCtrlButPMatr->setIcon(QIcon(QPixmap::fromImage(img)));

    img.load(qsResPath+"love-but.png");
    img = CAppStyle::drawRectToImage(img, 0, 0, 59, 59, 1, appstyle->updateColor(QColor("#5c524c")));
    m_pCtrlButComp->setIcon(QIcon(QPixmap::fromImage(img)));

    img.load(qsResPath+"cel-but.png");
    img = CAppStyle::drawRectToImage(img, 0, 0, 59, 59, 1, appstyle->updateColor(QColor("#5c524c")));
    m_pCtrlButCel->setIcon(QIcon(QPixmap::fromImage(img)));

    img.load(qsResPath+"pet-but.png");
    img = CAppStyle::drawRectToImage(img, 0, 0, 59, 59, 1, appstyle->updateColor(QColor("#5c524c")));
    m_pCtrlButPet->setIcon(QIcon(QPixmap::fromImage(img)));

	img.load(qsResPath+"guide-but.png");
	img = CAppStyle::drawRectToImage(img, 0, 0, 59, 59, 1, appstyle->updateColor(QColor("#5c524c")));
	m_pGuideBut->setIcon(QIcon(QPixmap::fromImage(img)));


#ifndef VER_FREE
    img.load(qsResPath+"chart-but.png");
    img = CAppStyle::drawRectToImage(img, 0, 0, 59, 59, 1, appstyle->updateColor(QColor("#5c524c")));
    m_pCtrlButChart->setIcon(QIcon(QPixmap::fromImage(img)));

    img.load(qsResPath+"an-but.png");
    img = CAppStyle::drawRectToImage(img, 0, 0, 59, 59, 1, appstyle->updateColor(QColor("#5c524c")));
    m_pCtrlButAnalize->setIcon(QIcon(QPixmap::fromImage(img)));

    img.load(qsResPath+"fore-but.png");
    img = CAppStyle::drawRectToImage(img, 0, 0, 59, 59, 1, appstyle->updateColor(QColor("#5c524c")));
    m_pCtrlButForecast->setIcon(QIcon(QPixmap::fromImage(img)));
#endif


    appstyle->setWidgetStyle(this, WTDlg);

    appstyle->setWidgetStyle(m_pFullNameEdit, WTEdit);
    appstyle->setWidgetStyle(m_pPetName, WTEdit);
    appstyle->setWidgetStyle(m_pTitleEdit, WTEdit);
    appstyle->setWidgetStyle(m_pShortNameEdit, WTEdit);
    appstyle->setWidgetStyle(m_pPFullNameEdit, WTEdit);
    appstyle->setWidgetStyle(m_pPShortNameEdit, WTEdit);

    appstyle->setWidgetStyle(m_pPetBDate, WTCombo);
    appstyle->setWidgetStyle(comboChart, WTCombo);
    appstyle->setWidgetStyle(m_pAnDate, WTCombo);
    appstyle->setWidgetStyle(m_pBDateF, WTCombo);
    appstyle->setWidgetStyle(m_pBDate, WTCombo);
    appstyle->setWidgetStyle(m_pSex, WTCombo);
    appstyle->setWidgetStyle(m_pPBDate, WTCombo);

    appstyle->setWidgetStyle(chartEditorBut, WTButton);
    appstyle->setWidgetStyle(chartAddBut, WTButton);
    appstyle->setWidgetStyle(chartDellBut, WTButton);

    appstyle->setWidgetStyle(m_pCtrlButNumer, WTCtrlButStyle);
    appstyle->setWidgetStyle(m_pCtrlButPMatr, WTCtrlButStyle);
    appstyle->setWidgetStyle(m_pCtrlButComp, WTCtrlButStyle);
    appstyle->setWidgetStyle(m_pCtrlButCel, WTCtrlButStyle);
    appstyle->setWidgetStyle(m_pCtrlButPet, WTCtrlButStyle);
#ifndef VER_FREE
	appstyle->setWidgetStyle(m_pCtrlButChart, WTCtrlButStyle);
	appstyle->setWidgetStyle(m_pCtrlButAnalize, WTCtrlButStyle);
	appstyle->setWidgetStyle(m_pCtrlButForecast, WTCtrlButStyle);
#endif
	appstyle->setWidgetStyle(m_pGuideBut, WTCtrlButStyle);

	for (int i = 0; i < m_pRecentList.size(); i++)
    {
        appstyle->setWidgetStyle(m_pRecentList.at(i), WTButton);
    }

    appstyle->setWidgetStyle(m_pAnCalcButton, WTButton);
    appstyle->setWidgetStyle(m_pPetCalcButton, WTButton);
    appstyle->setWidgetStyle(m_pClearRecent, WTButton);

    appstyle->setWidgetStyle(m_pSelPersBut, WTQsPersSelBut);
    appstyle->setWidgetStyle(m_pCtrlSelLabel, WTQsSelLab);
    appstyle->setWidgetStyle(m_pInpAnalizer, WTQsPersBkg);
    appstyle->setWidgetStyle(m_pInpP, WTQsPersBkg);
    appstyle->setWidgetStyle(bkg1, WTQsPersBkg);
    appstyle->setWidgetStyle(bkg2, WTQsPersBkg);
    appstyle->setWidgetStyle(m_pSelPersWidgI_, WTQsSelPersIWidg);
    appstyle->setWidgetStyle(&m_Menu, WTDlg);
#ifdef VER_PREM
    appstyle->setWidgetStyle(m_pReportBox, WTCombo);
    appstyle->setWidgetStyle(m_pReportLabel, WTLabel);

    appstyle->setWidgetStyle(m_pReportBut, WTButRect);

    QString style = m_pReportBut->styleSheet();
    style.replace("IMAGE_S", "rep_man.png");
    style.replace("IMAGE_H", "rep_manh.png");
    style.replace("IMAGE_P", "rep_manh.png");

    m_pReportBut->setStyleSheet(style);
#endif

    appstyle->setWidgetStyle(m_uCNC, WTDlg);
	appstyle->setWidgetStyle(m_PMode, WTCombo);
	appstyle->setWidgetStyle(m_PModeLabel, WTLabel);

    QImage hline(qsResPath + "hline.png");
    hline = appstyle->updateImage(hline);
    m_pCtrlScrollAreaLine->setPixmap(QPixmap::fromImage(hline));
}
//----------------------------------------------------------
void CVBNum::OnStyleChange()
{
    for (int a = 0; a < styleMenuGroup->actions().size(); a++)
    {
        if (styleMenuGroup->actions().at(a)->isChecked())
        {
            OnApplyStyle(a);
            styleAt = a;
            return;
        }
    }
}
//----------------------------------------------------------
void CVBNum::OnApplyStyle(int a)
{
    styleMenuGroup->actions().at(a)->setChecked(true);
    appstyle->setGlobalStyle(a);

    initStyle();
    emit updateStyle();
    update();
}
//----------------------------------------------------------
void CVBNum::OnSelReport(int)
{
    OnCalcNum();
}
//----------------------------------------------------------
bool CVBNum::rmDir(const QString &dirPath)
{
    QDir dir(dirPath);
    if (!dir.exists())
        return true;
    foreach(const QFileInfo &info, dir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot))
    {
        if (info.isDir())
        {
            if (!rmDir(info.filePath()))
                return false;
        } else
        {
            if (!dir.remove(info.fileName()))
                return false;
        }
    }
    QDir parentDir(QFileInfo(dirPath).path());
    return parentDir.rmdir(QFileInfo(dirPath).fileName());
}
//----------------------------------------------------------
bool CVBNum::cpDir(const QString &srcPath, const QString &dstPath)
{
    rmDir(dstPath);
    QDir parentDstDir(QFileInfo(dstPath).path());
    if (!parentDstDir.mkdir(QFileInfo(dstPath).fileName()))
        return false;

    QDir srcDir(srcPath);

    QFileInfoList list = srcDir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);

    foreach(const QFileInfo &info, list)
    {
        QString srcItemPath = srcPath + "/" + info.fileName();
        QString dstItemPath = dstPath + "/" + info.fileName();
        if (info.isDir())
        {
            if (!cpDir(srcItemPath, dstItemPath))
                return false;
        }
        else
        if (info.isFile())
        {
            if (!QFile::copy(srcItemPath, dstItemPath))
                return false;
        }

    }
    return true;
}
//----------------------------------------------------------
bool CVBNum::removeDir(const QString &dirName)
{
    bool result = true;
    QDir dir(dirName);

    if (dir.exists(dirName))
    {
        QFileInfoList list = dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst);
        Q_FOREACH(QFileInfo info, list)
        {
            if (info.isDir())
                result = removeDir(info.absoluteFilePath());
            else
                result = QFile::remove(info.absoluteFilePath());

            if (!result)
                return result;
        }
        result = dir.rmdir(dirName);
    }
    return result;
}
//----------------------------------------------------------
void CVBNum::OnPersButAnim(int val)
{
    QColor genCol(appstyle->updateColor(QString("#44372f")));
    int h;
    int s;
    int v;
    int a;
    genCol.getHsv(&h, &s, &v, &a);

    genCol.setHsv(h, s, v + val, a);

    QString nemStyle = appstyle->defQsPersSelButStyle;
    nemStyle.replace(appstyle->updateColor(QString("#44372f")), genCol.name());

    m_pSelPersBut->setStyleSheet(nemStyle);
}
//----------------------------------------------------------
void CVBNum::OnPersButAnimFinish()
{
    if (persButAnum.direction() == QTimeLine::Forward)
        persButAnum.setDirection(QTimeLine::Backward);
    else
        persButAnum.setDirection(QTimeLine::Forward);

    persButAnum.start();
}
//----------------------------------------------------------
void CVBNum::startButAnim()
{
    persButAnum.setDirection(QTimeLine::Forward);
    persButAnum.setCurveShape(QTimeLine::EaseInOutCurve);
    persButAnum.setStartFrame(0);
    persButAnum.setEndFrame(20);
    persButAnum.start();
}
//----------------------------------------------------------
void CVBNum::showHelper(int numerHelperID, QString imgName)
{
	QString det;

#ifdef VER_FREE
    return;
	int rmode = 0;
#else
	int rmode = registermode;
#endif
	if (rmode != 1) {
		CReportTools rep(NULL);
		rep.startDoc("");
		rep.startChap();
		rep.addPar("<b>Free version contains only fully functional Calculator and basic readings.</b>");
		rep.addPar("<b>Upgrade to Full or PRO version to get access to numerology guide, explanation of all these numbers, their meaning and calculation instructions.</b>");
		rep.endChap();
		rep.endDoc();
		det = rep.htmlDoc;
	} else {
		det = CNumerology::genHelperText(numerHelperID, imgName.size()>0 ? hpimagepath + imgName : QString());
	}

	if (det.size()>0)
	{
		CInfoDetailsView *dlg = new CInfoDetailsView(appstyle, det, this);
		dlg->exec();
		delete dlg;
	}
}


void CVBNum::MFitemCliked(int idx)
{
	if (idx==0)
		showHelper(NH_LifePath, "res01.jpg");
	else if (idx==1)
		showHelper(NH_BirthDay, "res02.jpg");
	else if (idx==2)
		showHelper(NH_Expression, "res03.jpg");
	else if (idx==3)
		showHelper(NH_HeartDesire, "res04.jpg");
	else if (idx==4)
		showHelper(NH_Personality, "res05.jpg");
	else if (idx==5)
		showHelper(NH_KarmicDebt, "res06.jpg");
}

void CVBNum::AFitemCliked(int idx)
{
	if (idx==0)
		showHelper(NH_Maturity, "res07.jpg");
	else if (idx==1)
		showHelper(NH_RationalThought, "res08.jpg");
	else if (idx==2)
		showHelper(NH_KarmicLessons, "res09.jpg");
	else if (idx==3)
		showHelper(NH_Balance, "res10.jpg");
	else if (idx==4)
		showHelper(NH_HiddenPassion, "res11.jpg");
	else if (idx==5)
		showHelper(NH_SubconsciousConfidence, "res12.jpg");
}

void CVBNum::LCitemCliked(int idx)
{
	if (idx==0)
		showHelper(NH_Challenges, "res14.jpg");
	else if (idx==1)
		showHelper(NH_Challenges, "res14.jpg");
	else if (idx==2)
		showHelper(NH_Pinnacles, "res13.jpg");
	else if (idx==3)
		showHelper(NH_PersonalYear, "res15.jpg");
/*	else if (idx==4)
		showHelper(***);
	else if (idx==5)
		showHelper(***);*/
}

void CVBNum::PMitemCliked(int idx)
{
	if (idx==1)
		showHelper(NH_PMCharacter, "res16.jpg");
	else if (idx==2)
		showHelper(NH_PMHealth, "res19.jpg");
	else if (idx==3)
		showHelper(NH_PMLuck, "res22.jpg");
	else if (idx==4)
		showHelper(NH_PMPurposeful, "res25.jpg");

	else if (idx==6)
		showHelper(NH_PMEnergy, "res17.jpg");
	else if (idx==7)
		showHelper(NH_PMIntuition, "res20.jpg");
	else if (idx==8)
		showHelper(NH_PMDuty, "res23.jpg");
	else if (idx==9)
		showHelper(NH_PMFamilyFormation, "res26.jpg");

	else if (idx==11)
		showHelper(NH_PMInterest, "res18.jpg");
	else if (idx==12)
		showHelper(NH_PMLabour, "res21.jpg");
	else if (idx==13)
		showHelper(NH_PMMemory, "res24.jpg");
	else if (idx==14)
		showHelper(NH_PMStability, "res27.jpg");

	else if (idx==15)
		showHelper(NH_PMSexualLife, "res32.jpg");
	else if (idx==16)
		showHelper(NH_PMSelfAppraisal, "res28.jpg");
	else if (idx==17)
		showHelper(NH_PMFinance, "res29.jpg");
	else if (idx==18)
		showHelper(NH_PMTalent, "res30.jpg");
	else if (idx==19)
		showHelper(NH_PMSpirituality, "res31.jpg");
}
