#include <QtGui/QApplication>
#include "vbnum.h"
#include "global.h"
#include "eula.h"

#ifdef Q_WS_WIN
#include <Windows.h>
#endif


int main(int argc, char *argv[])
{
#ifdef Q_WS_WIN
    CreateMutex(0, true, TEXT("Global\\BAPW_NUM_start_once_v7"));
	if (GetLastError() == ERROR_ALREADY_EXISTS)
		exit(0);
#else
    //qt_force_trolltech_settings_to_app_area(true);
#endif
	QApplication a(argc, argv);

#ifndef APPSTORE
    QString appName = Q_APP_TARGET;
    QString fileName = QDesktopServices::storageLocation(QDesktopServices::DataLocation);

    if (fileName.endsWith(appName))
        fileName.chop(appName.size()+1);
#ifdef VER_PREM
    fileName += "/vbnum_prem.cfg";
#else
    fileName += "/vbnum6.cfg";
#endif

    QFileInfo fi(fileName);
	if (!fi.exists())
	{
		CEulaDlg *eula = new CEulaDlg();
		eula->setWindowTitle("VeBest Numerology");
		eula->exec();
		if (eula->res != 0)
		{
			delete eula;
			exit(0);
			return 0;
		}

//        QString reportPath = QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/vbnum_report";
//        CVBNum::removeDir(reportPath);
//        QDir().mkpath(reportPath);
//#ifdef Q_OS_MAC
//        QString qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/templates");
//#else
//        QString qsResPath = QCoreApplication::applicationDirPath() + QString("/images/templates");
//#endif
//        CVBNum::cpDir(qsResPath, reportPath);
    }
#endif
    //QDir().mkpath(QDesktopServices::storageLocation(QDesktopServices::DataLocation) + "/vbnum_report");


#ifdef VER_PREM
    QString reportPath = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + "/VeBest Numerology";

    QDir().mkpath(reportPath);
#ifdef Q_OS_MAC
    QString qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/templates");
#else
    QString qsResPath = QCoreApplication::applicationDirPath() + QString("/images/templates");
#endif

    QDir dirT(reportPath);
    QStringList contentTarget = dirT.entryList(QStringList("*.vnr"));

    QDir dirC(qsResPath);
    QStringList contentSource = dirC.entryList(QStringList("*.vnr"));


    for (int i = 0; i < contentSource.size(); i++)
    {
        if (!contentTarget.contains(contentSource.at(i)))
        {
            QFile::copy(qsResPath + "/" + contentSource.at(i), reportPath + "/" + contentSource.at(i));
            //qDebug() << (qsResPath + "/" + contentSource.at(i)) << " ---> " << (reportPath + "/" + contentSource.at(i));
        }
    }
#endif

    CVBNum w;
	w.show();
	return a.exec();
}


