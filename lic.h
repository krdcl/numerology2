#ifndef CSECLICMC_H
#define CSECLICMC_H

#include <QObject>
#include <QString>
#include <QtNetwork>
#include <QSettings>
#include <QWidget>
#include <QDialog>
#include <QPushButton>
#include <QProgressBar>
#include <QLabel>
#include <QTimer>
#include "global.h"

#ifdef VER_PREM
#define LICREGGROUP "licbapwe"
#define LICPREF "VE01"
#define LICPREF2 "VMU1"
#define LICSRVURL "http://lic.vebest.net/validate.php"

#define UPDURL "http://lic.vebest.net/vbnumeupd.php"
#ifdef Q_WS_MAC
#define UPDDWNURL "http://www.vebest.com/products/hobby/numerologypro.html"
#else
#define UPDDWNURL "http://www.vebest.com/products/hobby/numerologypro.html"
#endif

#else
#define LICREGGROUP "licbapw"
#define LICPREF "VN01"
#define LICPREF2 "VSU1"
#define LICSRVURL "http://lic.vebest.net/validate.php"

#define UPDURL "http://lic.vebest.net/vbnumupd.php"
#ifdef Q_WS_MAC
#define UPDDWNURL "http://www.vebest.com/products/hobby/numerology-mac.html"
#else
#define UPDDWNURL "http://www.vebest.com/products/hobby/12-numerology.html"
#endif
#endif


class CSecLic : public QObject
{
	Q_OBJECT

public:
	CSecLic(QObject *parent);
	~CSecLic();

	void registerSN(QString sn);

	void startLic();
	int validateSN(QString sn, QString hwcode1, QString hwcode2="", QString hwcode3="", bool activate=false);
	int getStatus() { return status; }
	QString getStatusText() { return retMessage; }

	void storeData();

	QString retMessage;

	QString rm1, rm2, rm3, rm4;
private:
	QNetworkAccessManager *manager;
	QByteArray postData;

	int status;
	QString regName;
	int startUpdates;
	QString serialNum;
	int pvalid;

	void initLic();

	QString checkHW();

public slots:
	void replyFinished(QNetworkReply *reply);
};


class CSecLicDlg : public QDialog
{
	Q_OBJECT

public:
	CSecLicDlg(QWidget *parent = 0, Qt::WFlags flags = 0);
	~CSecLicDlg();

	void startRegistration(CSecLic *liccl);

	QPushButton *okBut;
	QProgressBar *progressBar;
	QLabel *statLabel;
	QTimer *tmr;
	CSecLic *lic;

public slots:
	void OnOk();
	void OnTmr();
};


class CDENDE16
{
public:

	unsigned short pkax,pkbx,pkcx,pkdx,pksi,pktmp,x1a2;
	unsigned short pkres,pki,inter,cfc,cfd,compte;
	unsigned short x1a0[8];
	unsigned char cle[17];
	short pkc, plainlen, ascipherlen;

	char *plainText, *ascCipherText;

	void pkfin(void);
	void pkcode(void);
	void pkassemble(void);

	void ascii_encrypt128(char *in);
	void ascii_decrypt128(char *in);

	CDENDE16();

private:
	char key[256];

};


#endif // CSECLICMC_H
