#include "eula.h"


void CEulaDlg::OnOk()
{
	res = 0;
	accept();
}


void CEulaDlg::OnCancel()
{
	reject();
}


CEulaDlg::CEulaDlg(QWidget *parent)
: QDialog(parent)
{
	res = -1;

	int sx = 400, sy=400;
	setMaximumSize(sx, sy);
	setMinimumSize(sx, sy);

    //QDesktopWidget desktopWid;
    //move((desktopWid.width()-sx)/2,(desktopWid.height()-sy)/2);

	m_pOk = new QPushButton("Accept", this);
	m_pOk->resize(80, 20);
	QObject::connect(m_pOk, SIGNAL(clicked()), this, SLOT(OnOk()));

	m_pCancel = new QPushButton("Reject", this);
	m_pCancel->resize(80, 20);
	QObject::connect(m_pCancel, SIGNAL(clicked()), this, SLOT(OnCancel()));

	QString text;

	text = "End User License Agreement\r\n\r\n\
(C) 2009-2017 VeBest. <http://www.vebest.com>\r\n\r\n\
The VeBest Numerology is distributed as try-before-you-buy. This means:\r\n\r\n\
1. All copyrights to The VeBest Numerology are exclusively owned by the author - VeBest.\r\n\r\n\
2. Anyone may use this software during unlimited test period. During test period, if you wish to continue to use Full VeBest Numerology, you must purchase it.\r\n\r\n\
3. If you detect errors before or after registration, you accept them if you register. Any description of bugs will be accepted, but we cannot guarantee that we will be able to fix them.\r\n\r\n\
4. One registered copy of this software may either be used by a single user who uses the software personally on one computer, or installed on a single workstation used non-simultaneously by multiple users. The licensed VeBest Numerology software may not be rented or leased. In case of sharing your license information your registration key will be blacklisted and result in immediate and automatic termination of this license.\r\n\r\n\
5. The VeBest Numerology trial version may be freely distributed, provided the distribution package is not modified. No person or company may charge a fee for the distribution of Best Numerology without written permission from the copyright holder.\r\n\r\n\
6. All VeBest Numerology IS DISTRIBUTED \"AS IS\". NO WARRANTY OF ANY KIND IS EXPRESSED OR IMPLIED. YOU USE AT YOUR OWN RISK. THE AUTHOR WILL NOT BE LIABLE FOR DATA LOSS, DAMAGES, LOSS OF PROFITS OR ANY OTHER KIND OF LOSS WHILE USING OR MISUSING THIS SOFTWARE.\r\n\r\n\
7. You may not use, copy, emulate, clone, rent, lease, sell, modify,  decompile, disassemble, otherwise reverse engineer, or transfer program, or any subset of the program, except as provided for in this agreement. Any such unauthorized use shall result in immediate and automatic termination of this license and may result in criminal and/or civil prosecution. All rights not expressly granted here are reserved by VeBest.\r\n\r\n\
8. Installing and using VeBest Numerology signifies acceptance of these terms and conditions of the license.\r\n\r\n\
9. If you do not agree with the terms of this license you must remove VeBest Numerology files from your storage devices and cease to use the product.\r\n\r\n\
10. Even though VeBest Numerology can help you see patterns, improve your intuition and make better decisions, information delivered by VeBest products and features should be used with common sense. VeBest is not responsible for how you interpret or apply provided information. VeBest is not responsible for the accuracy of provided readings and their usage.\r\n\r\n\
11. Without prejudice to any other rights, VeBest may terminate this EULA if you fail to comply with the terms and conditions of this EULA.\r\n\r\n\
12. WE RESERVE THE RIGHTS TO MAKE CHANGES TO THIS AGREEMENT WITHOUT ANY PRIOR NOTICE AT OUR SOLE DISCRETION.\r\n\r\n\
";

	edit = new QPlainTextEdit(this);
	edit->setPlainText(text);
	edit->setReadOnly(true);


	int w = this->width();
	int h = this->height();

	edit->move(10, 10);
	edit->resize(w-20, h-50);

	m_pOk->move(w/2-90, h-30);
	m_pCancel->move(w/2+10, h-30);
}

CEulaDlg::~CEulaDlg()
{
}
