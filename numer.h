#ifndef NUMEROLOGY_H
#define NUMEROLOGY_H

#include <QtGui>

#ifndef VER_MOBILE
#include "numer_interp.h"
#include "global.h"
#include "layoutmanagerdlg.h"
#include "reporttools.h"
#else
#include "reporttools.h"
#include "../dto.h"
#endif

enum ReportType {RTNumerology = 0, RTPsychoMatrix = 1, RTLoveCompatibility = 2, RTCelebrities = 3, RTPet = 4};

enum NumerologyType {NTHeader = 0, NTLifePath = 1, NTBrithday = 2, NTExpression = 3, NTMinorExpression = 4, NTHeartDesire = 5, NTMinorHeartDesire = 6,
NTPersonality = 7, NTMaturity = 8, NTRationalThought = 9, NTKarmicLessons = 10, NTBalanceNumber = 11, NTHiddenPassion = 12,
NTSubconsciousConfidence = 13, NTPinnacles = 14, NTChallenges = 15, NTPersonalYear = 16, NTPersonalMonth = 17, NTPersonalDay = 18,
NTTransits = 19, NTEssence = 20, NTMinorPersonality = 21, NTKarmicDebts = 22, NTKarmicDebt = 23, NTCompatible = 24, NTCompound = 25, NTPlanesExpression = 26,
NTPlanesExpressionP = 27, NTPlanesExpressionM = 28, NTPlanesExpressionE = 29, NTPlanesExpressionI = 30, NTBridgeLPE = 31, NTBridgeLPEDet = 32, NTBridgeHDP = 33, NTBridgeHDPDet = 34, NTCustomText = 35, NT_END=36};

enum PsychoMatrixType {PMTHeader = 0, PMTCharacter = 1, PMTEnergy = 2, PMTInterest = 3, PMTHealth = 4, PMTIntuition = 5, PMTLabour = 6, PMTLuck = 7,
PMTDuty = 8, PMTMemory = 9, PMTPurposeful = 10, PMTFamilyFormation = 11, PMTStability = 12, PMTSelfAppraisal = 13, PMTFinance = 14,
PMTTalent = 15, PMTSpirituality = 16, PMTSexualLife = 17, PMTCustomText = 18, PMT_END=19};

enum LoveCompatibilityType {LCTHeader = 0, LCTLifePath = 1, LCTBirthday = 2,  LCTName = 3, LCTPsychoMatrix = 4, LCTFamily = 5, LCTCarnalInterests = 6, LCTDetailedBirthday = 7,
LCTCharacter = 8, LCTEnergy = 9, LCTFinal = 10, LCTCustomText = 11};

enum CelebritiesType {CTHeader = 0, CTLifePath = 1, CTBrithday = 2, CTExpression = 3, CTCustomText = 4};

enum PetType {PTHeader = 0, PTPet = 1, PTDayForecast = 2, PTCustomText = 3};


// Helpers
enum { NH_LifePath=0, NH_BirthDay=1, NH_Expression=2, NH_PlanesExpression=3, NH_HeartDesire=4,
NH_Balance=5, NH_Personality=6, NH_KarmicLessons=7, NH_PersonalYear=8, NH_PersonalMonth=9,
NH_KarmicDebt=10, NH_Maturity=11, NH_HiddenPassion=12, NH_SubconsciousConfidence=13, NH_Pinnacles=14,
NH_Challenges=15, NH_Transits=16, NH_Essence=17, NH_PMCharacter=18, NH_PMEnergy=19,
NH_PMInterest=20, NH_PMHealth=21, NH_PMIntuition=22, NH_PMLabour=23, NH_PMLuck=24,
NH_PMDuty=25, NH_PMMemory=26, NH_PMPurposeful=27, NH_PMFamilyFormation=28, NH_PMStability=29,
NH_PMSelfAppraisal=30, NH_PMFinance=31, NH_PMTalent=32, NH_PMSpirituality=33, NH_PMSexualLife=34, NH_RationalThought=35 };

enum { NHT_Description=0, NHT_CalcInfo=1, NHT_Values=2 };

enum { NUM_VOW_MODE_ENGLISH=0, NUM_VOW_MODE_LATIN=1 };

class CNumerology : public CReportTools
{
	Q_OBJECT

public:
    int m_BaseNums[128];

	int m_LifePath;
	int m_cNum1, m_cNum2, m_cNum3, m_cNum4;
	int m_cyear, m_PersYear;
    int m_PELet[128];
    int m_Let[128];
	int m_CalcMode[33];
	int m_CalcModeID;
    int m_MasterNumberModeID;
	QDate m_bdate;
	int m_expression;
    QString m_sTransit1, m_sTransit2, m_sTransit3;
    int m_EssenceS[128];
    int m_Essence[128];

	QString fullName;
	QString shortName;
	QDate m_cdate;

    QString bres[128];

	enum { CM_WESTERN=0, CM_CHALDEAN=1 };
    enum { MNM_11TO33=0, MNM_11TO99=1 };


	struct SNumerDetails
	{
		QString lifePath;
		QString birthDay;
		QString expression;
		QString mexpression;
		QString heartDesire;
		QString mheartDesire;
		QString personality;
		QString mpersonality;
		QString maturity;
		QString rationalThought;
		QString karmicDebts;
		QString karmicLessons;
		QString challenges;
		QString pinnacles;
		QString brLPE;
		QString brHDP;
		QString balance;
		QString persYear;
		QString hiddenPassion;
		QString subconsciousConfidence;
		QString ageSpan;
        QString lpCycles;
        QString lpAgeSpan;

		QString PE[20];
		QString PENum[20];
		QString PECnt[20];
		QString PERep[20];



		QString fnameN1;
		QString fnameN2;
		QString fnameR;
		QString fnameR1;
		QString fnameR2;

		QString snameN1;
		QString snameN2;
		QString snameR;
		QString snameR1;
		QString snameR2;

        int compound;

        SNumerDetails()
        {
            compound = 0;
        }
	} numDet;


	QTextDocument m_TextDoc;

    int downOnce(int num);
    int downToBase(int num);
    int downToBase(int num, int customBaseNums[128]);
    static int downToDigit(int num);
    int statDownToBase(int num);

    bool isBase(int num);
    bool isCarmic(int num);
    bool isMasterNumber(int num, int extendedTO99);

	int letterToNumber(QChar ch);
	int letterToNumberChaldean(QChar ch);
	int wordToNumber(QString str);
	void calcNamesNumbers(QString str, QList<int> &res);
	void calcNamesNumbersP(QString str, QList<int> &res, bool Vow, int pmode);
    void calcNamesNumbersP(QString str, QList<int> &res, bool Vow, QVector<int> &vw, int pmode);

    static int isVow(QString str, int pos, int mode=NUM_VOW_MODE_ENGLISH);

	static int getPEIndex(QChar ch);

	static QString getHelperString(int numHelperID, int numHelperType) {
		return numerHelpers[numHelperID*4+numHelperType];
	}

	static QString genHelperText(int numHelperID, QString imgName=QString()) {
		int cinfo = numHelperID;
		if ((cinfo>NH_PMCharacter) && (cinfo<=NH_PMSexualLife))
			cinfo = NH_PMCharacter;

		CReportTools rep(NULL);
		rep.startDoc("");
		rep.startChap();
		if (imgName.size()>0)
			rep.addImage(imgName);
		rep.addPar(getHelperString(numHelperID, NHT_Description));
		rep.addPar("");
		rep.endChap();
		rep.startChap();
		rep.addPar(getHelperString(cinfo, NHT_CalcInfo));
		rep.addPar("");
		rep.endChap();
		rep.startChap();
		rep.addPar(getHelperString(numHelperID, NHT_Values));
		rep.addPar("");
		rep.endChap();
		rep.endDoc();
		return rep.htmlDoc;
	}

public:
	CNumerology(QObject *parent);
	~CNumerology();


	void setCalcMode(int mode);
    void setMasterNumbersMode(int mode);

	void draw(QPainter *pPntr);

    bool calculate(QString fullName, QString shortName, QVector<int> &fnvw, QVector<int> &snvw, bool useCNC, QDate bdate, QDate cdate, bool trial, RepSettings *repList, int curRep, int pmode, bool skipReport=false);
    bool calculateDynamic(bool useCNC, bool trial, RepSettings *repList, int curRep, int *baseNums, int m_mexpression, int m_hearDesire, int m_mhearDesire, int m_personality, int m_mpers, int m_Maturity, int m_RationalThought, QList<int> *m_KarmicLessons, int m_Balance,
                          QList<int> *m_HiddenPassion, int m_SubconsciousConfidence, int planesExpression[4], int m_LPEBridge, int m_LPEBridgeDet, int m_HDPBridge, int m_HDPBridgeDet);
	bool calculateAn(QString fullName, QDate bdate, bool trial);
    bool calculateCel(QString fullName, QDate bdate, bool useCNC, bool trial, RepSettings *repList, int curRep, int pmode);
    bool calculateCelDynamic(QDate bdate, RepSettings *repList, int curRep, bool trial);
    bool calculatePet(QString fullName, QString shortName, QDate bdate, QDate cdate, bool trial, RepSettings *repList, int curRep);

    QString getCalculateHeaderC(int *baseNums, int *m_spNum, int *m_pNum, bool useCNC, QList<int>ids);
    void s_spNum(int *m_spNum, int *m_pNum);
    void calculateGenTable(QList<int> ids);

	void clear();
	void resetData();
};

#endif // NUMEROLOGY_H
