#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H

#include <QDialog>
#include "appstyle.h"

class CTextEditor : public QDialog
{
    Q_OBJECT
public:
    CTextEditor(CAppStyle *style, QStringList *tL, QList<QImage> *iL, QList<bool> *dL, QString *contList, int type, int id, QWidget *parent = 0);
    int res;

    static QString upHtmlFontSize(QString html, int addSize);

signals:

private slots:
    void OnOk();
    void OnCancel();

    void OnTextItalic();
    void OnTextBold();
    void OnTextUnderline();
    void OnTextColor();

    void OnImage();

    void OnTextFamily(const QString &Family);
    void OnTextSize(const QString &SizeStr);
    void currentCharFormatChanged(const QTextCharFormat &format);

    void setCurList(int list);
    void defSet(bool val);
    void editText();

private:

    int fontScale;

    int gType;
    QString *contentList;
    QStringList *p_textList;
    QList<QImage> *p_imageLIst;
    QList<bool> *p_defailtList;

    QStringList textList;
    QList<QImage> imageLIst;
    QList<bool> defailtList;

    QString qsResPath;
    CAppStyle *appstyle;

    QColor globalColor;

    QPushButton *m_pOk;
    QPushButton *m_pCancel;

    QFontComboBox *m_pFontCombo;
    QComboBox *m_pSizeCombo;
    QTextEdit *m_pTextEdit;

    QToolButton *actionTextBold;
    QToolButton *actionTextItalic;
    QToolButton *actionTextUnderline;

    QComboBox *listBox;

    QToolButton *actionTextColor;
    QToolButton *actionSetImage;

    QCheckBox *defCheck;

    void mergeFormatOnWordOrSelection(const QTextCharFormat &format);
    void fontChanged(const QFont &f);
    void colorChanged(const QColor &c);

protected:
    void resizeEvent(QResizeEvent *event);
	void keyPressEvent(QKeyEvent *e)
	{
		if ((e->key()==Qt::Key_Enter) || (e->key()==Qt::Key_Return) || (e->key()==Qt::Key_Escape)) {}
		else { QDialog::keyPressEvent(e); return; }
	}
};

class CImagePreview : public QDialog
{
    Q_OBJECT
public:
    CImagePreview(CAppStyle *style, QImage *img, int type, QWidget *parent = 0);

private:
    QImage *image;
    QImage gImg;
    CAppStyle *appstyle;

    int gType;

    QPushButton *m_pOk;
    QPushButton *m_pCancel;
    QPushButton *m_pDef;
    QPushButton *m_pOpen;

    QLabel *label;

    void updateImage();

protected:
    void resizeEvent(QResizeEvent *event);

private slots:
    void OnOk();
    void OnCancel();
    void OnImage();
    void OnDefImage();

};


#endif // TEXTEDITOR_H
