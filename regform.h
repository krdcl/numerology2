#ifndef REGFORM_H
#define REGFORM_H

#include <QDialog>
#include "ui_cregform.h"

class CRegform : public QDialog
{
	Q_OBJECT

public:
	CRegform(QWidget *parent = 0);
	~CRegform();

	Ui::Dialog ui;

	QString caddr;

private slots:
	void on_pushButton_clicked();
};

#endif // REGFORM_H
