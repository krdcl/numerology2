/********************************************************************************
** Form generated from reading UI file 'settingsdlg.ui'
**
** Created: Tue 18. Dec 13:30:25 2012
**      by: Qt User Interface Compiler version 4.7.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGSDLG_H
#define UI_SETTINGSDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_CSettingsDlg
{
public:
    QDialogButtonBox *buttonBox;
    QPushButton *defaultSettingsBut;
    QCheckBox *checkBoxLP1;
    QLabel *label;
    QCheckBox *checkBoxLP2;
    QCheckBox *checkBoxLP3;
    QLabel *label_2;
    QCheckBox *checkBoxNameWD;
    QCheckBox *checkBoxNameDD;
    QLabel *label_3;
    QCheckBox *checkBoxSkipTrans;
    QCheckBox *checkBoxExtendedMMto99;
    QLabel *label_4;
    QCheckBox *checkBoxDateFormat;

    void setupUi(QDialog *CSettingsDlg)
    {
        if (CSettingsDlg->objectName().isEmpty())
            CSettingsDlg->setObjectName(QString::fromUtf8("CSettingsDlg"));
        CSettingsDlg->setWindowModality(Qt::ApplicationModal);
        CSettingsDlg->resize(400, 310);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(40);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CSettingsDlg->sizePolicy().hasHeightForWidth());
        CSettingsDlg->setSizePolicy(sizePolicy);
        int dlgH = 370;
        CSettingsDlg->setMinimumSize(QSize(400, dlgH));
        CSettingsDlg->setMaximumSize(QSize(400, dlgH));
        buttonBox = new QDialogButtonBox(CSettingsDlg);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(50, dlgH-40, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        defaultSettingsBut = new QPushButton(CSettingsDlg);
        defaultSettingsBut->setObjectName(QString::fromUtf8("defaultSettingsBut"));
        defaultSettingsBut->setGeometry(QRect(10, dlgH-40, 191, 32));
        checkBoxLP1 = new QCheckBox(CSettingsDlg);
        checkBoxLP1->setObjectName(QString::fromUtf8("checkBoxLP1"));
        checkBoxLP1->setGeometry(QRect(30, 40, 241, 20));
        label = new QLabel(CSettingsDlg);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 20, 241, 16));
        checkBoxLP2 = new QCheckBox(CSettingsDlg);
        checkBoxLP2->setObjectName(QString::fromUtf8("checkBoxLP2"));
        checkBoxLP2->setGeometry(QRect(30, 60, 241, 20));
        checkBoxLP3 = new QCheckBox(CSettingsDlg);
        checkBoxLP3->setObjectName(QString::fromUtf8("checkBoxLP3"));
        checkBoxLP3->setGeometry(QRect(30, 80, 241, 20));
        label_2 = new QLabel(CSettingsDlg);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(20, 120, 241, 16));
        checkBoxNameWD = new QCheckBox(CSettingsDlg);
        checkBoxNameWD->setObjectName(QString::fromUtf8("checkBoxNameWD"));
        checkBoxNameWD->setGeometry(QRect(30, 140, 301, 20));
        checkBoxNameDD = new QCheckBox(CSettingsDlg);
        checkBoxNameDD->setObjectName(QString::fromUtf8("checkBoxNameDD"));
        checkBoxNameDD->setGeometry(QRect(30, 160, 301, 20));

        label_3 = new QLabel(CSettingsDlg);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(20, 200, 241, 16));
        checkBoxSkipTrans = new QCheckBox(CSettingsDlg);
        checkBoxSkipTrans->setObjectName(QString::fromUtf8("checkBoxNameWD"));
        checkBoxSkipTrans->setGeometry(QRect(30, 220, 301, 20));
        checkBoxExtendedMMto99 = new QCheckBox(CSettingsDlg);
        checkBoxExtendedMMto99->setObjectName(QString::fromUtf8("checkBoxNameEMM"));
        checkBoxExtendedMMto99->setGeometry(QRect(30, 240, 301, 20));

        label_4 = new QLabel(CSettingsDlg);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(20, 280, 241, 16));
        checkBoxDateFormat = new QCheckBox(CSettingsDlg);
        checkBoxDateFormat->setObjectName(QString::fromUtf8("checkBoxDateFormat"));
        checkBoxDateFormat->setGeometry(QRect(30, 300, 301, 20));


        retranslateUi(CSettingsDlg);
        QObject::connect(buttonBox, SIGNAL(accepted()), CSettingsDlg, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), CSettingsDlg, SLOT(reject()));

        QMetaObject::connectSlotsByName(CSettingsDlg);
    } // setupUi

    void retranslateUi(QDialog *CSettingsDlg)
    {
        CSettingsDlg->setWindowTitle(QApplication::translate("CSettingsDlg", "Dialog", 0, QApplication::UnicodeUTF8));
        defaultSettingsBut->setText(QApplication::translate("CSettingsDlg", "Restore Default Settings", 0, QApplication::UnicodeUTF8));
        checkBoxLP1->setText(QApplication::translate("CSettingsDlg", "= DIG(DIG(Y) + DIG(M) + DIG(D))", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("CSettingsDlg", "Life Path Master Number Calculation:", 0, QApplication::UnicodeUTF8));
        checkBoxLP2->setText(QApplication::translate("CSettingsDlg", "= DIG(Y +M + D)", 0, QApplication::UnicodeUTF8));
        checkBoxLP3->setText(QApplication::translate("CSettingsDlg", "= Sum of all digits", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("CSettingsDlg", "Name calculation:", 0, QApplication::UnicodeUTF8));
        checkBoxNameWD->setText(QApplication::translate("CSettingsDlg", "Down to digit every word", 0, QApplication::UnicodeUTF8));
        checkBoxNameDD->setText(QApplication::translate("CSettingsDlg", "Reduce Master Numbers to Digit", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("CSettingsDlg", "Additional:", 0, QApplication::UnicodeUTF8));
        checkBoxSkipTrans->setText(QApplication::translate("CSettingsDlg", "Don't generate missed transits", 0, QApplication::UnicodeUTF8));
        checkBoxExtendedMMto99->setText(QApplication::translate("CSettingsDlg", "Calculate 11,22...99 Master numbers", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("CSettingsDlg", "Format:", 0, QApplication::UnicodeUTF8));
        checkBoxDateFormat->setText(QApplication::translate("CSettingsDlg", "Date Input - DD/MM/YYYY", 0, QApplication::UnicodeUTF8));
    } // retranslateUi
};

namespace Ui {
    class CSettingsDlg: public Ui_CSettingsDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGSDLG_H
