#include "infodlg.h"

CInfoDlg::CInfoDlg(CAppStyle *style, QWidget *parent, qreal sc, int wi)
	: QWidget(parent)
{

    qWi = wi;
    appstyle = style;
	res = -1;
    scale = 1;

    if (sc != 0)
        scale = sc;

    m_pOk = new QPushButton("Ok", this);
    m_pOk->resize(80 * scale, 20 * scale);
	QObject::connect(m_pOk, SIGNAL(clicked()), this, SLOT(OnOk()));

    m_pCancel = new QPushButton("Cancel", this);
    m_pCancel->resize(80 * scale, 20 * scale);
	QObject::connect(m_pCancel, SIGNAL(clicked()), this, SLOT(OnCancel()));

	winText = new QLabel(this);
	winText->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

	initStyle();

    int w = this->width();
    int h = this->height();

    winText->resize(w-32, h-80);
    winText->move(16, 20);

#ifdef Q_WS_WIN
    m_pOk->move(w/2-(90 * scale), h-(30 * scale));
    m_pCancel->move(w/2+(10 * scale), h-(30 * scale));
#else
    m_pOk->move(w/2+(10 * scale), h-(30 * scale));
    m_pCancel->move(w/2-(90 * scale), h-(30 * scale));
#endif
}


CInfoDlg::~CInfoDlg()
{
}

void CInfoDlg::initStyle()
{
    appstyle->setWidgetStyle(this->parentWidget(), WTDlg);

    appstyle->setWidgetStyle(m_pOk, WTIOkButton);
    appstyle->setWidgetStyle(m_pCancel, WTICancelButton);
    winText->setStyleSheet("font-size: " + QString::number(12.0 * scale) + "px;");

    parentWidget()->setWindowTitle("Information");

    int wszx = qWi * scale, wszy = 230 * scale;
	resize(wszx, wszy);
    parentWidget()->resize(wszx, wszy);
    parentWidget()->setFixedSize(wszx, wszy);
    parentWidget()->setWindowFlags(Qt::Dialog | Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::CustomizeWindowHint);

    int px=0, py=0;
    if (parentWidget()->parentWidget() != NULL && parentWidget()->parentWidget()->parentWidget() != NULL)
    {
        px = parentWidget()->parentWidget()->parentWidget()->x() + (parentWidget()->parentWidget()->parentWidget()->width()-this->width())/2;
        py = parentWidget()->parentWidget()->parentWidget()->y() + (parentWidget()->parentWidget()->parentWidget()->height()-this->height())/2;

        if (px<0) px=0; if (py<0) py=0;

        parentWidget()->move(px, py);
        ASCheckScreenBoundary(this->parentWidget());
    } else
    if (parentWidget()->parentWidget() != NULL)
    {
        px = parentWidget()->parentWidget()->x() + (parentWidget()->parentWidget()->width()-this->width())/2;
        py = parentWidget()->parentWidget()->y() + (parentWidget()->parentWidget()->height()-this->height())/2;

        if (px<0) px=0; if (py<0) py=0;

        parentWidget()->move(px, py);
        ASCheckScreenBoundary(this->parentWidget());
    }

    // else
//    {
//        QDesktopWidget desktopWid;
//        px = (desktopWid.width()-this->width())/2;
//        py = (desktopWid.height()-this->height())/2;
//    }

}



void CInfoDlg::OnOk()
{
	res = 0;
	parentWidget()->close();
}


void CInfoDlg::OnCancel()
{
    res = -1;
	parentWidget()->close();
}

void CInfoDlg::OnMidle()
{
    res = 1;
    parentWidget()->close();
}

void CInfoDlg::OnDownload()
{
    res = 1;
    parentWidget()->close();
}

void CInfoDlg::OnWhatNes()
{
    QDesktopServices::openUrl(QUrl("http://lignup.com/multicollector-log.html", QUrl::TolerantMode));
}

void CInfoDlg::OnGetLicense()
{
    QDesktopServices::openUrl(QUrl("http://lignup.com/mcbuy.html", QUrl::TolerantMode));
}


CInfoDlgWin::CInfoDlgWin(CAppStyle *style, QWidget *parent, qreal sc, int wi)
: QDialog(parent)
{
    mainWidget = new CInfoDlg(style, this, sc, wi);
}

CInfoDlgWin::~CInfoDlgWin()
{

}

int CInfoDlgWin::showAskMessageBox(CAppStyle *style, QString winTitle, QString text, QWidget *parent)
{
    int res = -1;
    CInfoDlgWin *dlg = new CInfoDlgWin(style, parent);
    dlg->setWindowTitle(winTitle);
    dlg->mainWidget->winText->setText(text);
    dlg->mainWidget->m_pOk->setText("Yes");
    dlg->mainWidget->m_pCancel->setText("No");
    dlg->exec();
    res = dlg->mainWidget->res;
    delete dlg;
    return res;
}

int CInfoDlgWin::showInfoMessageBox(CAppStyle *style, QString winTitle, QString text, QWidget *parent)
{
    CInfoDlgWin *dlg = new CInfoDlgWin(style, parent);

    dlg->setWindowTitle(winTitle);
	dlg->mainWidget->winText->setText(text);
#ifdef Q_WS_WIN
    dlg->mainWidget->m_pOk->move(dlg->mainWidget->m_pOk->x()+(50), dlg->mainWidget->m_pOk->y());
#else
    dlg->mainWidget->m_pOk->move(dlg->mainWidget->m_pOk->x()-(50), dlg->mainWidget->m_pOk->y());
#endif
	dlg->mainWidget->m_pCancel->hide();
	dlg->exec();
	int res = dlg->mainWidget->res;
	delete dlg;
	return res;
}


int CInfoDlgWin::showEnterNameDlg(CAppStyle *style, QString &name, QWidget *parent)
{
    QString winTitle = "Edit name";
    QString text = "Enter new name";

    CInfoDlgWin *dlg = new CInfoDlgWin(style, parent);
    dlg->setWindowTitle(winTitle);
    dlg->mainWidget->winText->setText(text);

    dlg->mainWidget->winText->move(dlg->mainWidget->winText->x(), 30);
    dlg->mainWidget->winText->resize(380, 40);
    QLineEdit *edit = new QLineEdit(dlg->mainWidget);
    edit->move(10, 70);
    edit->resize(380, 22);
    edit->setText(name);
    dlg->mainWidget->appstyle->setWidgetStyle(edit, WTEdit);
    QString style_ = edit->styleSheet();
    style_.replace("font-size: 12px;", "font-size: " + QString::number(12.0) + "px;");
    edit->setStyleSheet(style_);

    dlg->exec();
    int res = dlg->mainWidget->res;
    if (res==0)
        name = edit->text();

    delete dlg;
    return res;
}
