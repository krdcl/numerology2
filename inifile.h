#ifndef IniFileH
#define IniFileH

#include <QtGui>
#include <vector>

//-----------------------------------------------------------------------------
//CIniFile
//-----------------------------------------------------------------------------
class CIniFile
{
	//recent info
	int m_MaxRecentFiles;
	std::vector<QString> m_RecentFiles;
	bool isRecentFileInList(QString FileName);

	//email info
	//std::vector<QString> m_MailInfo;// 0-SMTPServer,1-MailFrom,2-MailTo,3-Login

public:
	CIniFile();
	~CIniFile();

	void clear();

	void addRecentFile(QString Name);
	std::vector<QString> getRecentFiles();

	//void GetMailInfo(QString &SMTPServer, QString &MailFrom,QString &MailTo, QString &Login);
	//void SetMailInfo(QString &SMTPServer, QString &MailFrom,QString &MailTo, QString &Login);

	void load();
	void save();
};

#endif