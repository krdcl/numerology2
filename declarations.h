//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//		Copyright 2010. Intelliprotector.com
//		Web site: http://www.intelliprotector.com
//		Version: v3.0
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

// Begin of the encryption
#define ITP_ENCRYPTION_START_V3	  \
	__asm _emit 0xeb  __asm _emit 0x29 __asm _emit 0x40 __asm _emit 0x34 __asm _emit 0x8f \
	__asm _emit 0xe8  __asm _emit 0xcc __asm _emit 0xc3 __asm _emit 0x09 __asm _emit 0xb6 \
	__asm _emit 0xe8  __asm _emit 0x4d __asm _emit 0xdb __asm _emit 0xda __asm _emit 0x4a \
	__asm _emit 0x31  __asm _emit 0x9f __asm _emit 0x36 __asm _emit 0x07 __asm _emit 0xb6 \
	__asm _emit 0x02  __asm _emit 0x41 __asm _emit 0x3e __asm _emit 0xa1 __asm _emit 0xc8 \
	__asm _emit 0xfd  __asm _emit 0x66 __asm _emit 0x43 __asm _emit 0x4f __asm _emit 0x90 \
	__asm _emit 0x40  __asm _emit 0xd6 __asm _emit 0x9a __asm _emit 0xac __asm _emit 0xc2 \
	__asm _emit 0x3c  __asm _emit 0xf2 __asm _emit 0x34 __asm _emit 0x09 __asm _emit 0xfd \
	__asm _emit 0xf1  __asm _emit 0x2c __asm _emit 0x0d0


// End of the encryption
#define ITP_ENCRYPTION_STOP_V3	\
	__asm _emit 0xeb __asm _emit 0x29 __asm _emit 0x4d __asm _emit 0x52 __asm _emit 0x5a \
	__asm _emit 0xec __asm _emit 0x4a __asm _emit 0x53 __asm _emit 0x3a __asm _emit 0xf4 \
	__asm _emit 0x57 __asm _emit 0xcf __asm _emit 0xa1 __asm _emit 0xae __asm _emit 0x43 \
	__asm _emit 0xbd __asm _emit 0xb3 __asm _emit 0xcf __asm _emit 0x35 __asm _emit 0x57 \
	__asm _emit 0xf2 __asm _emit 0xbe __asm _emit 0x6d __asm _emit 0x6d __asm _emit 0x56 \
	__asm _emit 0x55 __asm _emit 0x12 __asm _emit 0xc6 __asm _emit 0x8b __asm _emit 0x36 \
	__asm _emit 0x4e __asm _emit 0x63 __asm _emit 0xaf __asm _emit 0xb3 __asm _emit 0xb6 \
	__asm _emit 0x8a __asm _emit 0xa5 __asm _emit 0xbd __asm _emit 0xf4 __asm _emit 0x38 \
	__asm _emit 0xa0 __asm _emit 0x45 __asm _emit 0x81


// Invokes registration window.
#define ITP_SHOW_REGISTRATION_WINDOW_V3 \
	__asm _emit 0xeb __asm _emit 0x14 __asm _emit 0x30 __asm _emit 0x4e __asm _emit 0xe8 \
	__asm _emit 0x5e __asm _emit 0xbf __asm _emit 0xa5 __asm _emit 0x44 __asm _emit 0xd5 \
	__asm _emit 0x98 __asm _emit 0x0a __asm _emit 0xaf __asm _emit 0xe1 __asm _emit 0x7f \
	__asm _emit 0xb9 __asm _emit 0x72 __asm _emit 0x85 __asm _emit 0xd7 __asm _emit 0x47 \
	__asm _emit 0x1f __asm _emit 0x53


// Function returns free evaluation period in the time units, which are left after activation.
#define ITP_GET_TRIAL_UNITS_LEFT_V3(iUnits, iDimension)  iUnits = -1; \
	__asm _emit 0xeb __asm _emit 0x2e __asm _emit 0x35 __asm _emit 0x8a __asm _emit 0xa5 \
	__asm _emit 0x16 __asm _emit 0x68 __asm _emit 0xcf __asm _emit 0x48 __asm _emit 0x10 \
	__asm _emit 0xb0 __asm _emit 0xa0 __asm _emit 0xc1 __asm _emit 0xb5 __asm _emit 0xd9 \
	__asm _emit 0x8f __asm _emit 0xd6 __asm lea eax, [iUnits] __asm _emit 0x7b __asm _emit 0xc4 \
	__asm _emit 0x43 __asm _emit 0xc2 __asm mov eax, [iDimension] __asm _emit 0x87 __asm _emit 0x99 \
	__asm _emit 0x5d __asm _emit 0x8d __asm _emit 0xa6 __asm _emit 0x77 __asm _emit 0x9a \
	__asm _emit 0xca __asm _emit 0x6a __asm _emit 0x4c __asm _emit 0x59 __asm _emit 0xd3 \
	__asm _emit 0xef __asm _emit 0x19 __asm _emit 0x43 __asm _emit 0x0a __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90


// Function returns free evaluation period in days, which are left after activation.
#define ITP_GET_TRIAL_DAYS_LEFT_V3(iDaysLeft) iDaysLeft = -1; \
	__asm _emit 0xeb __asm _emit 0x22 __asm _emit 0xcc  __asm _emit 0x88  __asm _emit 0xfe \
	__asm _emit 0x7e __asm _emit 0xc7 __asm _emit 0xdb  __asm _emit 0x42  __asm _emit 0x0b \
	__asm _emit 0xb9 __asm _emit 0x12 __asm _emit 0x05  __asm _emit 0x3c  __asm _emit 0x7d \
	__asm _emit 0x9e __asm _emit 0x56 __asm lea eax, [iDaysLeft] __asm _emit 0xc4 __asm _emit 0xbc \
	__asm _emit 0x4d __asm _emit 0x1e __asm _emit 0x8e __asm _emit 0xc5  __asm _emit 0xf0  \
	__asm _emit 0x5f  __asm _emit 0x86 __asm _emit 0xb7 __asm _emit 0x10 __asm _emit 0xe8 \
	__asm _emit 0xa0 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90


// Function returns length of the trial period in days.
#define ITP_GET_TRIAL_DAYS_V3(iDaysCount) iDaysCount = -1; \
	__asm _emit 0xeb __asm _emit 0x22 __asm _emit 0x63 __asm _emit 0x01 __asm _emit 0x11 \
	__asm _emit 0x8f __asm _emit 0x89 __asm _emit 0xea __asm _emit 0x49 __asm _emit 0xcb \
	__asm _emit 0x8d __asm _emit 0xbb __asm _emit 0x8d __asm _emit 0xc9 __asm _emit 0x1a \
	__asm _emit 0x52 __asm _emit 0x82 __asm lea eax, [iDaysCount] __asm _emit 0x40 __asm _emit 0xbf \
	__asm _emit 0x45 __asm _emit 0x3d __asm _emit 0x84 __asm _emit 0x72  __asm _emit 0xbd  \
	__asm _emit 0x3d  __asm _emit 0x67 __asm _emit 0x59 __asm _emit 0x50 __asm _emit 0xc8 \
	__asm _emit 0x0a __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90


// Function returns length of the trial period in time units.
#define ITP_GET_TRIAL_UNITS_V3(iUnits, iDimension)  iUnits = -1; \
	__asm _emit 0xeb __asm _emit 0x2e __asm _emit 0x3c __asm _emit 0x8b __asm _emit 0x1d \
	__asm _emit 0x8a __asm _emit 0x09 __asm _emit 0x3c __asm _emit 0x56 __asm _emit 0x45 \
	__asm _emit 0x8a __asm _emit 0x9d __asm _emit 0x0a __asm _emit 0xa8 __asm _emit 0xbf \
	__asm _emit 0x5e __asm _emit 0xc5 __asm lea eax, [iUnits] __asm _emit 0x68 __asm _emit 0x80 \
	__asm _emit 0x10 __asm _emit 0xbe __asm mov eax, [iDimension] __asm _emit 0x57 __asm _emit 0x5b \
	__asm _emit 0xa9 __asm _emit 0x40 __asm _emit 0xae __asm _emit 0x76 __asm _emit 0xee \
	__asm _emit 0xe8 __asm _emit 0x31 __asm _emit 0x69 __asm _emit 0x77 __asm _emit 0x73 \
	__asm _emit 0x39 __asm _emit 0xe6 __asm _emit 0xfe __asm _emit 0xa8 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90


// Function says is software registered or not
// Registered: 2
// Unregistered: 1
#define ITP_GET_REGISTRATION_TYPE_V3(iValue) iValue = -1; \
	__asm _emit 0xeb __asm _emit 0x22 __asm _emit 0x79 __asm _emit 0x7d __asm _emit 0x80 \
	__asm _emit 0x0b __asm _emit 0xfb __asm _emit 0x1c __asm _emit 0x4f __asm _emit 0x3b \
	__asm _emit 0x83 __asm _emit 0x05 __asm _emit 0x54 __asm _emit 0xf6 __asm _emit 0xbf \
	__asm _emit 0xef __asm _emit 0x2e __asm lea eax, [iValue] __asm _emit 0x2a __asm _emit 0x94 \
	__asm _emit 0x49 __asm _emit 0x94 __asm _emit 0xaa __asm _emit 0x69 __asm _emit 0x6d  \
	__asm _emit 0x5c __asm _emit 0x28 __asm _emit 0xec __asm _emit 0x5d __asm _emit 0x53 \
	__asm _emit 0x22 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90


// Function returns 'purchase license code' link.
#define ITP_GET_BUYNOW_URL_V3(pwcLocalLinkBuffer, iMaxLength) \
	__asm _emit 0xeb __asm _emit 0x41 __asm _emit 0xaf __asm _emit 0x8c __asm _emit 0x18 \
	__asm _emit 0x53 __asm _emit 0xff __asm _emit 0x71 __asm _emit 0xe2 __asm _emit 0x4e \
	__asm _emit 0x8d __asm _emit 0x4f __asm _emit 0x48 __asm _emit 0xa8 __asm _emit 0xa8 \
	__asm _emit 0x54 __asm _emit 0xd0 __asm _emit 0xb1 __asm _emit 0xf0 __asm _emit 0x32 \
	__asm _emit 0x92 __asm cmp [dword ptr pwcLocalLinkBuffer], 0 __asm _emit 0x44 __asm _emit 0x03 \
	__asm mov ebx, [dword ptr pwcLocalLinkBuffer] __asm _emit 0x89 __asm _emit 0x4d __asm _emit 0xa4 \
	__asm _emit 0xbd __asm lea ebx, [pwcLocalLinkBuffer] __asm _emit 0x99 __asm _emit 0x66 \
	__asm _emit 0xaf __asm mov eax, [dword ptr iMaxLength]  __asm _emit 0x43 __asm _emit 0xcf \
	__asm _emit 0x9b __asm _emit 0x36 __asm _emit 0xc1 __asm _emit 0x6f __asm _emit 0xdd \
	__asm _emit 0x8e __asm _emit 0x18 __asm _emit 0x11 __asm _emit 0x42 __asm _emit 0xbe \
	__asm _emit 0x48 __asm _emit 0xbe __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
    __asm _emit 0x90 __asm _emit 0x90

// Function returns license type id
// Returns -1 if license is not available.
#define ITP_GET_LICENSE_TYPE_V3(iType)  iType = -1; \
	__asm _emit 0xeb __asm _emit 0x22 __asm _emit 0x38 __asm _emit 0x98 __asm _emit 0xbe \
	__asm _emit 0x3b __asm _emit 0xb9 __asm _emit 0xbd __asm _emit 0x4f __asm _emit 0x46 \
	__asm _emit 0x87 __asm _emit 0x31 __asm _emit 0xa5 __asm _emit 0xc8 __asm _emit 0x87 \
	__asm _emit 0x94 __asm _emit 0xdb __asm lea eax, [iType] __asm _emit 0x3a __asm _emit 0x82 \
	__asm _emit 0x42 __asm _emit 0x2f __asm _emit 0x85 __asm _emit 0xc4 __asm _emit 0xb4 \
	__asm _emit 0x17 __asm _emit 0x88 __asm _emit 0x40 __asm _emit 0xef __asm _emit 0xb8 \
	__asm _emit 0x19 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90


// Function says is software protected or not
// Protected: true
// Protected: false
#define ITP_IS_PROTECTED_V3(x) x = 0; \
	__asm _emit 0xeb __asm _emit 0x1f __asm _emit 0xde __asm _emit 0x2e __asm _emit 0x08 \
	__asm _emit 0x8a __asm _emit 0x26 __asm _emit 0x34 __asm _emit 0x4a __asm _emit 0x2f \
	__asm _emit 0xbe __asm _emit 0xc9 __asm _emit 0x83 __asm _emit 0xdb __asm _emit 0xc9 \
	__asm _emit 0x8f __asm _emit 0xf1 __asm lea eax, [x] __asm _emit 0x5c __asm _emit 0xe2 \
	__asm _emit 0x4a __asm _emit 0x6a __asm _emit 0x86 __asm _emit 0x89 __asm _emit 0x13 \
	__asm _emit 0x80 __asm _emit 0x0e __asm _emit 0x4b __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90


// Function returns the customer's name.
#define ITP_GET_CUSTOMER_NAME_V3(pwcCustomerName, iMaxLength) \
	__asm _emit 0xeb __asm _emit 0x41 __asm _emit 0xc2 __asm _emit 0xe6 __asm _emit 0xd5 \
	__asm _emit 0xe2 __asm _emit 0x65 __asm _emit 0x09 __asm _emit 0x2b __asm _emit 0x44 \
	__asm _emit 0x9a __asm _emit 0x83 __asm _emit 0x39 __asm _emit 0x4d __asm _emit 0x7f \
	__asm _emit 0xb0 __asm _emit 0x56 __asm _emit 0x66 __asm _emit 0xf7 __asm _emit 0x2b \
	__asm _emit 0x2f __asm cmp [dword ptr pwcCustomerName], 0 __asm _emit 0x2f __asm _emit 0x00 \
	__asm mov ebx, [dword ptr pwcCustomerName] __asm _emit 0x52 __asm _emit 0x46 __asm _emit 0xb8 \
	__asm _emit 0x4f __asm lea ebx, [pwcCustomerName] __asm _emit 0xc5 __asm _emit 0x97 \
	__asm _emit 0x63 __asm mov eax, [dword ptr iMaxLength]  __asm _emit 0x0d __asm _emit 0xb6 \
	__asm _emit 0x09 __asm _emit 0x9c __asm _emit 0x7f __asm _emit 0xd6 __asm _emit 0xe6 \
	__asm _emit 0xc2 __asm _emit 0x64 __asm _emit 0x69 __asm _emit 0x4b __asm _emit 0xa1 \
	__asm _emit 0x8f __asm _emit 0xdf __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
    __asm _emit 0x90 __asm _emit 0x90


// Function returns the customer's email address.
#define ITP_GET_CUSTOMER_EMAIL_V3(pwcEmailBuffer, iMaxLength) \
	__asm _emit 0xeb __asm _emit 0x41 __asm _emit 0x95 __asm _emit 0x1c __asm _emit 0xf4 \
	__asm _emit 0xc4 __asm _emit 0xa3 __asm _emit 0x34 __asm _emit 0xf0 __asm _emit 0x4b \
	__asm _emit 0xab __asm _emit 0xe7 __asm _emit 0x94 __asm _emit 0x24 __asm _emit 0x96 \
	__asm _emit 0xf5 __asm _emit 0x86 __asm _emit 0xb0 __asm _emit 0x55 __asm _emit 0x3d \
	__asm _emit 0x42 __asm cmp [dword ptr pwcEmailBuffer], 0 __asm _emit 0x4c __asm _emit 0xe0 \
	__asm mov ebx, [dword ptr pwcEmailBuffer] __asm _emit 0x39 __asm _emit 0x49 __asm _emit 0x85 \
	__asm _emit 0x69 __asm lea ebx, [pwcEmailBuffer] __asm _emit 0x13 __asm _emit 0xfb \
	__asm _emit 0x0f __asm mov eax, [dword ptr iMaxLength]  __asm _emit 0x72 __asm _emit 0x47 \
	__asm _emit 0x94 __asm _emit 0x70 __asm _emit 0x00 __asm _emit 0x46 __asm _emit 0x66 \
	__asm _emit 0x40 __asm _emit 0x5c __asm _emit 0x9f __asm _emit 0x4d __asm _emit 0xa1 \
	__asm _emit 0x60 __asm _emit 0xd5 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
    __asm _emit 0x90 __asm _emit 0x90


// Function returns the license code provided.
#define ITP_GET_LICENSE_CODE_V3(pwcLicense, iMaxLength) \
	__asm _emit 0xeb __asm _emit 0x41 __asm _emit 0x46 __asm _emit 0x65 __asm _emit 0x22 \
	__asm _emit 0xbc __asm _emit 0x0d __asm _emit 0xe4 __asm _emit 0x3a __asm _emit 0x42 \
	__asm _emit 0x90 __asm _emit 0x48 __asm _emit 0x5f __asm _emit 0xaa __asm _emit 0x70 \
	__asm _emit 0xbb __asm _emit 0xcb __asm _emit 0xe9 __asm _emit 0xca __asm _emit 0x20 \
	__asm _emit 0x28 __asm cmp [dword ptr pwcLicense], 0 __asm _emit 0xda __asm _emit 0xef \
	__asm mov ebx, [dword ptr pwcLicense] __asm _emit 0xfc __asm _emit 0x47 __asm _emit 0x82 \
	__asm _emit 0xbe __asm lea ebx, [pwcLicense] __asm _emit 0x05 __asm _emit 0xa8 \
	__asm _emit 0x0b __asm mov eax, [dword ptr iMaxLength]  __asm _emit 0x9b __asm _emit 0x79 \
	__asm _emit 0x55 __asm _emit 0xb7 __asm _emit 0x39 __asm _emit 0x5f __asm _emit 0xf1 \
	__asm _emit 0x04 __asm _emit 0xcc __asm _emit 0xa1 __asm _emit 0x41 __asm _emit 0xb1 \
	__asm _emit 0xe4 __asm _emit 0xf6 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
    __asm _emit 0x90 __asm _emit 0x90


// Function gets the date, when software has been activated
#define ITP_GET_CURRENT_ACTIVATION_DATE_V3(iYear, iMonth, iDay, iHour, iMinute) \
	iYear = iMonth = iDay = iHour = iMinute = -1; \
	__asm _emit 0xeb __asm _emit 0x4d __asm _emit 0x84 __asm _emit 0xbc __asm _emit 0x0b \
	__asm _emit 0x62 __asm _emit 0xd5 __asm _emit 0x11 __asm _emit 0x76 __asm _emit 0x4e \
	__asm _emit 0xaa __asm _emit 0x48 __asm _emit 0x5f __asm _emit 0xb8 __asm _emit 0xec \
	__asm _emit 0x1d __asm _emit 0x3f __asm lea eax, [iYear] __asm _emit 0x29 __asm _emit 0x02 \
	__asm _emit 0x36 __asm _emit 0x54 __asm lea eax, [iMonth] __asm _emit 0x25 __asm _emit 0xb8 \
	__asm _emit 0x6e __asm _emit 0x40 __asm lea eax, [iDay] __asm _emit 0x8e __asm _emit 0xa5 \
	__asm _emit 0x72 __asm _emit 0x7a __asm lea eax, [iHour] __asm _emit 0x67 __asm _emit 0xaf \
	__asm _emit 0xb9 __asm _emit 0x7e __asm lea eax, [iMinute] __asm _emit 0x59 __asm _emit 0x0a \
	__asm _emit 0xfb __asm _emit 0x6e __asm _emit 0xf9 __asm _emit 0x79 __asm _emit 0xf8 \
	__asm _emit 0x49 __asm _emit 0xa7 __asm _emit 0x06 __asm _emit 0x05 __asm _emit 0xe0 \
	__asm _emit 0x32 __asm _emit 0x50 __asm _emit 0x09 __asm _emit 0x52 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 


// Function gets the date, when software has been registered
#define ITP_GET_CURRENT_REGISTRATION_DATE_V3(iYear, iMonth, iDay, iHour, iMinute) \
	iYear = iMonth = iDay = iHour = iMinute = -1; \
	__asm _emit 0xeb __asm _emit 0x4d __asm _emit 0xda __asm _emit 0xcb __asm _emit 0xd3 \
	__asm _emit 0x01 __asm _emit 0x10 __asm _emit 0x68 __asm _emit 0x21 __asm _emit 0x4f \
	__asm _emit 0x8a __asm _emit 0xa1 __asm _emit 0xea __asm _emit 0x83 __asm _emit 0x28 \
	__asm _emit 0x23 __asm _emit 0xa4 __asm lea eax, [iYear] __asm _emit 0xae __asm _emit 0x81 \
	__asm _emit 0xd4 __asm _emit 0x25 __asm lea eax, [iMonth] __asm _emit 0x96 __asm _emit 0x6d \
	__asm _emit 0x9b __asm _emit 0x4d __asm lea eax, [iDay] __asm _emit 0x8d __asm _emit 0xe4 \
	__asm _emit 0x86 __asm _emit 0x89 __asm lea eax, [iHour] __asm _emit 0x69 __asm _emit 0x84 \
	__asm _emit 0x63 __asm _emit 0x38 __asm lea eax, [iMinute] __asm _emit 0x8d __asm _emit 0xc7 \
	__asm _emit 0x12 __asm _emit 0x59 __asm _emit 0x23 __asm _emit 0x36 __asm _emit 0x52 \
	__asm _emit 0x4c __asm _emit 0xb2 __asm _emit 0x21 __asm _emit 0x68 __asm _emit 0x9f \
	__asm _emit 0x26 __asm _emit 0x58 __asm _emit 0xf3 __asm _emit 0x47 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 


// Function gets the first date, when software has been registered by current license code
#define	ITP_GET_FIRST_REGISTRATION_DATE_V3(iYear, iMonth, iDay, iHour, iMinute) \
	iYear = iMonth = iDay = iHour = iMinute = -1; \
	__asm _emit 0xeb __asm _emit 0x4d __asm _emit 0xcd  __asm _emit 0x88 __asm _emit 0xfe \
	__asm _emit 0x7e __asm _emit 0xc7 __asm _emit 0xdb  __asm _emit 0x42 __asm _emit 0x0b \
	__asm _emit 0xb9 __asm _emit 0x12 __asm _emit 0x05  __asm _emit 0x3c __asm _emit 0x7d \
	__asm _emit 0x9e __asm _emit 0x56 __asm lea eax, [iYear] __asm _emit 0x14 __asm _emit 0xbc \
	__asm _emit 0x4d __asm _emit 0x1e __asm lea eax, [iMonth] __asm _emit 0x24 __asm _emit 0xbc \
	__asm _emit 0x4d __asm _emit 0x1e __asm lea eax, [iDay] __asm _emit 0x34 __asm _emit 0xbc \
	__asm _emit 0x4d __asm _emit 0x1e __asm lea eax, [iHour] __asm _emit 0x44 __asm _emit 0xbc \
	__asm _emit 0x4d __asm _emit 0x1e __asm lea eax, [iMinute] __asm _emit 0x54 __asm _emit 0xbc \
	__asm _emit 0x4d __asm _emit 0x1e __asm _emit 0x8e __asm _emit 0xc5 __asm _emit 0xf0  \
	__asm _emit 0x5f __asm _emit 0x86 __asm _emit 0xb7 __asm _emit 0x10 __asm _emit 0xe8 \
	__asm _emit 0xa0 __asm _emit 0x86 __asm _emit 0xb7 __asm _emit 0x10 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 


// Function gets the date when order has been created
#define ITP_GET_ORDER_DATE_V3(iYear, iMonth, iDay, iHour, iMinute) \
	iYear = iMonth = iDay = iHour = iMinute = -1; \
	__asm _emit 0xeb __asm _emit 0x4d __asm _emit 0x16 __asm _emit 0xf1 __asm _emit 0x92 \
	__asm _emit 0x14 __asm _emit 0x14 __asm _emit 0xba __asm _emit 0xbe __asm _emit 0x4c \
	__asm _emit 0xb8 __asm _emit 0xef __asm _emit 0xcd __asm _emit 0x8b __asm _emit 0x18 \
	__asm _emit 0xda __asm _emit 0x7f __asm lea eax, [iYear] __asm _emit 0xa9 __asm _emit 0x0d \
	__asm _emit 0x50 __asm _emit 0x7d __asm lea eax, [iMonth] __asm _emit 0xb2 __asm _emit 0x3b \
	__asm _emit 0xc1 __asm _emit 0x4b __asm lea eax, [iDay] __asm _emit 0xb1 __asm _emit 0x2f \
	__asm _emit 0xf8 __asm _emit 0x28 __asm lea eax, [iHour] __asm _emit 0x2a __asm _emit 0x3c \
	__asm _emit 0x9a __asm _emit 0x7c __asm lea eax, [iMinute] __asm _emit 0x1e __asm _emit 0x4f \
	__asm _emit 0x2e __asm _emit 0xb3 __asm _emit 0x63 __asm _emit 0xa7 __asm _emit 0xe4 \
	__asm _emit 0x47 __asm _emit 0xab __asm _emit 0x8b __asm _emit 0x7a __asm _emit 0x2f \
	__asm _emit 0xc7 __asm _emit 0xee __asm _emit 0x66 __asm _emit 0xf5 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 


// Function gets the date, when license will be expired
#define ITP_GET_LICENSE_EXPIRATION_DATE_V3(iYear, iMonth, iDay, iHour, iMinute) \
	iYear = iMonth = iDay = iHour = iMinute = -1; \
	__asm _emit 0xeb __asm _emit 0x4d __asm _emit 0x56 __asm _emit 0xb4 __asm _emit 0x38 \
	__asm _emit 0xf9 __asm _emit 0x53 __asm _emit 0x67 __asm _emit 0xf7 __asm _emit 0x45 \
	__asm _emit 0x94 __asm _emit 0x7e __asm _emit 0xe5 __asm _emit 0xe1 __asm _emit 0x03 \
	__asm _emit 0xb9 __asm _emit 0x8a __asm lea eax, [iYear] __asm _emit 0x62 __asm _emit 0x4f \
	__asm _emit 0x27 __asm _emit 0x35 __asm lea eax, [iMonth] __asm _emit 0x93 __asm _emit 0x12 \
	__asm _emit 0xfa __asm _emit 0x40 __asm lea eax, [iDay] __asm _emit 0x9f __asm _emit 0x07 \
	__asm _emit 0xe5 __asm _emit 0x81 __asm lea eax, [iHour] __asm _emit 0x6d __asm _emit 0xd6 \
	__asm _emit 0xc1 __asm _emit 0xe2 __asm lea eax, [iMinute] __asm _emit 0xf2 __asm _emit 0x0e \
	__asm _emit 0xa3 __asm _emit 0x46 __asm _emit 0xd3 __asm _emit 0xe2 __asm _emit 0x81 \
	__asm _emit 0x49 __asm _emit 0x80 __asm _emit 0x7d __asm _emit 0x99 __asm _emit 0x5d \
	__asm _emit 0x3e __asm _emit 0xfa __asm _emit 0x52 __asm _emit 0x84 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 


// Function returns version of the protection engine.
#define ITP_GET_INTELLIPROTECTOR_VERSION_V3(chVersion, iMaxLen) \
	__asm _emit 0xeb __asm _emit 0x41 __asm _emit 0x48 __asm _emit 0xba __asm _emit 0xb8 \
	__asm _emit 0xe8 __asm _emit 0x7d __asm _emit 0x76 __asm _emit 0x9d __asm _emit 0x47 \
	__asm _emit 0xa2 __asm _emit 0x14 __asm _emit 0xd2 __asm _emit 0x5b __asm _emit 0x70 \
	__asm _emit 0x82 __asm _emit 0x05 __asm _emit 0x01 __asm _emit 0xb5 __asm _emit 0x5c \
	__asm _emit 0x20 __asm cmp [dword ptr chVersion], 0 __asm _emit 0xb7 __asm _emit 0x8f \
	__asm mov ebx, [dword ptr chVersion] __asm _emit 0xeb __asm _emit 0x4d __asm _emit 0x98 \
	__asm _emit 0xb8 __asm lea ebx, [chVersion] __asm _emit 0x86 __asm _emit 0x62 \
	__asm _emit 0xd1 __asm mov eax, [dword ptr iMaxLen]  __asm _emit 0x79 __asm _emit 0x89 \
	__asm _emit 0x87 __asm _emit 0xef __asm _emit 0x4c __asm _emit 0x31 __asm _emit 0x6c \
	__asm _emit 0xd6 __asm _emit 0xcc __asm _emit 0x59 __asm _emit 0x4d __asm _emit 0xb0 \
	__asm _emit 0xfd __asm _emit 0x00 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
    __asm _emit 0x90 __asm _emit 0x90 


// Function returns the protection date
#define ITP_GET_PROTECTION_DATE_V3(iYear, iMonth, iDay, iHour, iMinute) \
	iYear = iMonth = iDay = iHour = iMinute = -1; \
	__asm _emit 0xeb __asm _emit 0x4d __asm _emit 0x32 __asm _emit 0x1e __asm _emit 0x20 \
	__asm _emit 0xfa __asm _emit 0x5b __asm _emit 0xef __asm _emit 0x59 __asm _emit 0x47 \
	__asm _emit 0x84 __asm _emit 0x78 __asm _emit 0xc1 __asm _emit 0x93 __asm _emit 0x1a \
	__asm _emit 0x86 __asm _emit 0xfd __asm lea eax, [iYear] __asm _emit 0x33 __asm _emit 0xce \
	__asm _emit 0x20 __asm _emit 0xc8 __asm lea eax, [iMonth] __asm _emit 0xd6 __asm _emit 0xff \
	__asm _emit 0x4f __asm _emit 0x48 __asm lea eax, [iDay] __asm _emit 0xbb __asm _emit 0xf5 \
	__asm _emit 0xf6 __asm _emit 0x35 __asm lea eax, [iHour] __asm _emit 0x87 __asm _emit 0x87 \
	__asm _emit 0x95 __asm _emit 0xb3 __asm lea eax, [iMinute] __asm _emit 0xc5 __asm _emit 0xbd \
	__asm _emit 0x7e __asm _emit 0x64 __asm _emit 0x64 __asm _emit 0xe4 __asm _emit 0x34 \
	__asm _emit 0x43 __asm _emit 0xac __asm _emit 0xc9 __asm _emit 0x3c __asm _emit 0xfa \
	__asm _emit 0x3d __asm _emit 0xcb __asm _emit 0xc3 __asm _emit 0x7f __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 


// Function returns the days, which are left for the license code. 
// When retail days will expires, the software will ask for new license code (renew subscription).
#define ITP_GET_LICENSE_EXPIRATION_DAYS_LEFT_V3(iDays) iDays=-1; \
	__asm _emit 0xeb __asm _emit 0x22 __asm _emit 0x09 __asm _emit 0x27 __asm _emit 0x3d \
	__asm _emit 0xa5 __asm _emit 0xb4 __asm _emit 0x51 __asm _emit 0x41 __asm _emit 0x96 \
	__asm _emit 0xa0 __asm _emit 0xb5 __asm _emit 0xf9 __asm _emit 0x59 __asm _emit 0x45 \
	__asm _emit 0x24 __asm _emit 0xa6 __asm lea eax, [iDays] __asm _emit 0x72 __asm _emit 0x4a \
	__asm _emit 0x4c __asm _emit 0x8f __asm _emit 0x9e __asm _emit 0x96 __asm _emit 0x19 \
	__asm _emit 0xfb __asm _emit 0xff __asm _emit 0xec __asm _emit 0x12 __asm _emit 0x4e \
	__asm _emit 0xc1 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90
	

// Function returns the 'retail' days when the license code will be expired.
#define ITP_GET_LICENSE_EXPIRATION_DAYS_V3(iDays) iDays=-1; \
	__asm _emit 0xeb __asm _emit 0x22 __asm _emit 0xe4 __asm _emit 0xdf __asm _emit 0x1a \
	__asm _emit 0x09 __asm _emit 0x06 __asm _emit 0x01 __asm _emit 0x6e __asm _emit 0x44 \
	__asm _emit 0x9d __asm _emit 0x55 __asm _emit 0xec __asm _emit 0x60 __asm _emit 0xef \
	__asm _emit 0xc9 __asm _emit 0x5c __asm lea eax, [iDays] __asm _emit 0x6b __asm _emit 0x4c \
	__asm _emit 0xa0 __asm _emit 0x08 __asm _emit 0x4f __asm _emit 0x0e __asm _emit 0x13 \
	__asm _emit 0x42 __asm _emit 0xb3 __asm _emit 0xe4 __asm _emit 0xde __asm _emit 0xd6 \
	__asm _emit 0x94 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90

	
// Fucntion registers the software by the license code provided
#define ITP_REGISTER_SOFTWARE_V3(pwcLicenseCode, iStatus) __asm{\
	__asm _emit 0xeb __asm _emit 0x46 __asm _emit 0x81 __asm _emit 0x53 __asm _emit 0x4b \
	__asm _emit 0xad __asm _emit 0xbd __asm _emit 0x4e __asm _emit 0xab __asm _emit 0x4a \
	__asm _emit 0xb2 __asm _emit 0x98 __asm _emit 0x96 __asm _emit 0xc7 __asm _emit 0xad \
	__asm _emit 0x57 __asm _emit 0x30 __asm _emit 0x83 __asm _emit 0x33 __asm _emit 0x8e \
	__asm _emit 0xa2 } iStatus = sizeof(pwcLicenseCode); __asm{ __asm cmp [dword ptr iStatus], 4 __asm _emit 0x66 \
    __asm _emit 0x38 __asm mov ebx, [dword ptr pwcLicenseCode] __asm _emit 0x43 __asm _emit 0x4d __asm _emit 0x9f \
	__asm _emit 0x5c __asm _emit 0x3e __asm lea ebx, [pwcLicenseCode] __asm _emit 0xbc __asm _emit 0x57 \
	__asm _emit 0xfc __asm lea eax, [dword ptr iStatus] __asm _emit 0xf3 __asm _emit 0x8f \
	__asm _emit 0x97 __asm _emit 0x4a __asm _emit 0x5c __asm _emit 0xd2 __asm _emit 0x4b \
	__asm _emit 0x8d __asm _emit 0x97 __asm _emit 0x41 __asm _emit 0xae __asm _emit 0x55 \
	__asm _emit 0xd3 __asm _emit 0xf8 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90}


// Function extends the expiration period of the license code by the renewal code.
#define ITP_RENEW_LICENSE_CODE_V3(pwcLicenseCode, iStatus) __asm{\
	__asm _emit 0xeb __asm _emit 0x46 __asm _emit 0xf5 __asm _emit 0x1d __asm _emit 0x10 \
	__asm _emit 0x16 __asm _emit 0x4f __asm _emit 0xb2 __asm _emit 0x07 __asm _emit 0x4e \
	__asm _emit 0xa7 __asm _emit 0xd8 __asm _emit 0x55 __asm _emit 0x53 __asm _emit 0xf9 \
	__asm _emit 0x5e __asm _emit 0x44 __asm _emit 0xd5 __asm _emit 0x29 __asm _emit 0xd6 \
	__asm _emit 0xe6 } iStatus = sizeof(pwcLicenseCode); __asm{ __asm cmp [dword ptr iStatus], 4 __asm _emit 0x36 \
    __asm _emit 0x9d __asm mov ebx, [dword ptr pwcLicenseCode] __asm _emit 0x47 __asm _emit 0x44 __asm _emit 0xa1 \
	__asm _emit 0x25 __asm _emit 0x9e __asm lea ebx, [pwcLicenseCode] __asm _emit 0x9d __asm _emit 0x57 \
	__asm _emit 0x40 __asm lea eax, [dword ptr iStatus] __asm _emit 0x9d __asm _emit 0xe6 \
	__asm _emit 0x6d __asm _emit 0x3d __asm _emit 0x97 __asm _emit 0xec __asm _emit 0xeb \
	__asm _emit 0x03 __asm _emit 0x2c __asm _emit 0x40 __asm _emit 0xba __asm _emit 0xb2 \
	__asm _emit 0xeb __asm _emit 0x96 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90}


// Function returns count of free launches in the trial period.
#define ITP_GET_TRIAL_LAUNCHES_V3(iLaunches) \
	iLaunches = -1; \
	__asm _emit 0xeb __asm _emit 0x22 __asm _emit 0xf1 __asm _emit 0x01 __asm _emit 0x71 \
	__asm _emit 0xd1 __asm _emit 0x17 __asm _emit 0x57 __asm _emit 0x03 __asm _emit 0x4f \
	__asm _emit 0x81 __asm _emit 0xf3 __asm _emit 0xb8 __asm _emit 0xca __asm _emit 0x68 \
	__asm _emit 0x13 __asm _emit 0xd0 __asm lea eax, [iLaunches] __asm _emit 0x28 __asm _emit 0x5d \
	__asm _emit 0x5f __asm _emit 0xe6 __asm _emit 0x82 __asm _emit 0x90 __asm _emit 0xe2 \
	__asm _emit 0x46 __asm _emit 0xbd __asm _emit 0x1c __asm _emit 0xe2 __asm _emit 0xd1 \
	__asm _emit 0x52 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90


// Function returns count of free launches in the trial period, which are left after activation.
#define ITP_GET_TRIAL_LAUNCHES_LEFT_V3(iLaunches) \
	iLaunches = -1; \
	__asm _emit 0xeb __asm _emit 0x22 __asm _emit 0x4c __asm _emit 0x77 __asm _emit 0xb4 \
	__asm _emit 0xf5 __asm _emit 0x61 __asm _emit 0xef __asm _emit 0xc9 __asm _emit 0x4d \
	__asm _emit 0xb3 __asm _emit 0x9a __asm _emit 0xde __asm _emit 0x18 __asm _emit 0x0a \
	__asm _emit 0x44 __asm _emit 0x36 __asm lea eax, [iLaunches] __asm _emit 0xe2 __asm _emit 0xb2 \
	__asm _emit 0xc9 __asm _emit 0x7b __asm _emit 0x26 __asm _emit 0x89 __asm _emit 0xd4 \
	__asm _emit 0x4f __asm _emit 0xa2 __asm _emit 0x57 __asm _emit 0x4a __asm _emit 0xbb \
	__asm _emit 0x38 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90


// Function gets the date, when support will be expired
#define ITP_GET_SUPPORT_EXPIRATION_DATE_V3(iYear, iMonth, iDay, iHour, iMinute) \
	iYear = iMonth = iDay = iHour = iMinute = -1; \
	__asm _emit 0xeb __asm _emit 0x4d __asm _emit 0xa2 __asm _emit 0x0d __asm _emit 0x54 \
	__asm _emit 0xd0 __asm _emit 0xbc __asm _emit 0x3c __asm _emit 0x60 __asm _emit 0x4f \
	__asm _emit 0x9a __asm _emit 0xc4 __asm _emit 0x75 __asm _emit 0x73 __asm _emit 0xf4 \
	__asm _emit 0x11 __asm _emit 0x3d __asm lea eax, [iYear] __asm _emit 0x3a __asm _emit 0x4c \
	__asm _emit 0x13 __asm _emit 0xf2 __asm lea eax, [iMonth] __asm _emit 0x73 __asm _emit 0x06 \
	__asm _emit 0xbc __asm _emit 0x45 __asm lea eax, [iDay] __asm _emit 0xb5 __asm _emit 0x4b \
	__asm _emit 0xdf __asm _emit 0xb4 __asm lea eax, [iHour] __asm _emit 0x7b __asm _emit 0x8e \
	__asm _emit 0x2a __asm _emit 0xc8 __asm lea eax, [iMinute] __asm _emit 0x78 __asm _emit 0x63 \
	__asm _emit 0xf8 __asm _emit 0x99 __asm _emit 0x58 __asm _emit 0x00 __asm _emit 0x95 \
	__asm _emit 0x4c __asm _emit 0xa0 __asm _emit 0xb6 __asm _emit 0xdb __asm _emit 0x19 \
	__asm _emit 0x1d __asm _emit 0x96 __asm _emit 0xbf __asm _emit 0xda __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90  __asm _emit 0x90 __asm _emit 0x90  __asm _emit 0x90 


// Function returns the days, which are left for the support period.
// The license code will be valid for product versions which are released during support period.
#define ITP_GET_SUPPORT_EXPIRATION_DAYS_LEFT_V3(iDaysLeft) \
	iDaysLeft = -1; \
	__asm _emit 0xeb __asm _emit 0x22 __asm _emit 0x35 __asm _emit 0xe4 __asm _emit 0x10 \
	__asm _emit 0x31 __asm _emit 0x56 __asm _emit 0x99 __asm _emit 0xf7 __asm _emit 0x46 \
	__asm _emit 0x89 __asm _emit 0x06 __asm _emit 0x7a __asm _emit 0xf4 __asm _emit 0xa3 \
	__asm _emit 0xc1 __asm _emit 0x39 __asm lea eax, [iDaysLeft] __asm _emit 0x0c __asm _emit 0x0c \
	__asm _emit 0x48 __asm _emit 0x10 __asm _emit 0x4c __asm _emit 0x6b __asm _emit 0x3d \
	__asm _emit 0x48 __asm _emit 0x94 __asm _emit 0xb2 __asm _emit 0xdb __asm _emit 0x47 \
	__asm _emit 0x49 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90


// Function returns length of the support period in days.
#define ITP_GET_SUPPORT_EXPIRATION_DAYS_V3(iDays) \
	iDays = -1; \
	__asm _emit 0xeb __asm _emit 0x22 __asm _emit 0xca __asm _emit 0xed __asm _emit 0x35 \
	__asm _emit 0x9f __asm _emit 0xbe __asm _emit 0xc4 __asm _emit 0x14 __asm _emit 0x46 \
	__asm _emit 0x82 __asm _emit 0x12 __asm _emit 0xc0 __asm _emit 0x3b __asm _emit 0x38 \
	__asm _emit 0x84 __asm _emit 0x69 __asm lea eax, [iDays] __asm _emit 0xa0 __asm _emit 0xb9 \
	__asm _emit 0x3d __asm _emit 0xa5 __asm _emit 0x19 __asm _emit 0x47 __asm _emit 0x7d \
	__asm _emit 0x4d __asm _emit 0xad __asm _emit 0xa4 __asm _emit 0x87 __asm _emit 0xf8 \
	__asm _emit 0x16 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90


// Function returns the product version when support will be expired.
#define ITP_GET_SUPPORT_EXPIRATION_PRODUCT_VERSION_V3(pwcVersion, iMaxLength) \
	__asm _emit 0xeb __asm _emit 0x41 __asm _emit 0xf4 __asm _emit 0x42 __asm _emit 0x4d \
	__asm _emit 0xad __asm _emit 0xee __asm _emit 0xbb __asm _emit 0x5e __asm _emit 0x46 \
	__asm _emit 0xae __asm _emit 0xb6 __asm _emit 0xcf __asm _emit 0xd1 __asm _emit 0x4f \
	__asm _emit 0x31 __asm _emit 0x29 __asm _emit 0x0a __asm _emit 0x8c __asm _emit 0x26 \
	__asm _emit 0xd8 __asm cmp [dword ptr pwcVersion], 0 __asm _emit 0x9b __asm _emit 0x63 \
	__asm mov ebx, [dword ptr pwcVersion] __asm _emit 0x87 __asm _emit 0x43 __asm _emit 0xa3 \
	__asm _emit 0xbe __asm lea ebx, [pwcVersion] __asm _emit 0x5b __asm _emit 0x78 \
	__asm _emit 0xdf __asm mov eax, [dword ptr iMaxLength]  __asm _emit 0xf5 __asm _emit 0xcb \
	__asm _emit 0xac __asm _emit 0xce __asm _emit 0x78 __asm _emit 0x22 __asm _emit 0x00 \
	__asm _emit 0x07 __asm _emit 0xe0 __asm _emit 0xab __asm _emit 0x4a __asm _emit 0x87 \
	__asm _emit 0x0b __asm _emit 0xa4 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
    __asm _emit 0x90 __asm _emit 0x90


// Function returns the current product version.
#define ITP_GET_CURRENT_PRODUCT_VERSION_V3(pwcVersion, iMaxLength) \
	__asm _emit 0xeb __asm _emit 0x41 __asm _emit 0x83 __asm _emit 0x97 __asm _emit 0x79 \
	__asm _emit 0x21 __asm _emit 0xa6 __asm _emit 0x75 __asm _emit 0x3a __asm _emit 0x4c \
	__asm _emit 0x82 __asm _emit 0x0d __asm _emit 0x47 __asm _emit 0xf5 __asm _emit 0xe3 \
	__asm _emit 0x4a __asm _emit 0x8f __asm _emit 0x5b __asm _emit 0xca __asm _emit 0x32 \
	__asm _emit 0xfd __asm cmp [dword ptr pwcVersion], 0 __asm _emit 0x80 __asm _emit 0x8a \
	__asm mov ebx, [dword ptr pwcVersion] __asm _emit 0x10 __asm _emit 0x4a __asm _emit 0x81 \
	__asm _emit 0x3f __asm lea ebx, [pwcVersion] __asm _emit 0x07 __asm _emit 0x8a \
	__asm _emit 0x99 __asm mov eax, [dword ptr iMaxLength]  __asm _emit 0xa6 __asm _emit 0x56 \
	__asm _emit 0xdd __asm _emit 0x18 __asm _emit 0x90 __asm _emit 0xfb __asm _emit 0xc6 \
	__asm _emit 0x92 __asm _emit 0xd8 __asm _emit 0x6b __asm _emit 0x4b __asm _emit 0xb8 \
	__asm _emit 0xd8 __asm _emit 0xce __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
    __asm _emit 0x90 __asm _emit 0x90


// Function returns the 'purchase renewal code' link.
#define ITP_GET_RENEWAL_PURCHASE_LINK_V3(pwcLocalLinkBuffer, iMaxLength) \
	__asm _emit 0xeb __asm _emit 0x41 __asm _emit 0xc4 __asm _emit 0x14 __asm _emit 0x24 \
	__asm _emit 0x53 __asm _emit 0xf0 __asm _emit 0xe0 __asm _emit 0xf3 __asm _emit 0x47 \
	__asm _emit 0x8b __asm _emit 0x2c __asm _emit 0xa8 __asm _emit 0x29 __asm _emit 0xf5 \
	__asm _emit 0x22 __asm _emit 0x58 __asm _emit 0xfe __asm _emit 0xca __asm _emit 0xd6 \
	__asm _emit 0x6c __asm cmp [dword ptr pwcLocalLinkBuffer], 0 __asm _emit 0x57 __asm _emit 0x70 \
	__asm mov ebx, [dword ptr pwcLocalLinkBuffer] __asm _emit 0xae __asm _emit 0x4f __asm _emit 0xa7 \
	__asm _emit 0x44 __asm lea ebx, [pwcLocalLinkBuffer] __asm _emit 0x39 __asm _emit 0x8f \
	__asm _emit 0x32 __asm mov eax, [dword ptr iMaxLength]  __asm _emit 0xc4 __asm _emit 0x0f \
	__asm _emit 0xc9 __asm _emit 0xf7 __asm _emit 0x81 __asm _emit 0xc1 __asm _emit 0xe7 \
	__asm _emit 0xbf __asm _emit 0x54 __asm _emit 0x19 __asm _emit 0x40 __asm _emit 0xad \
	__asm _emit 0xf0 __asm _emit 0xa7 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
	__asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 __asm _emit 0x90 \
    __asm _emit 0x90 __asm _emit 0x90