#ifndef VBNUM_H
#define VBNUM_H

/*
#include <QtGui>
#include <QtWebKit>
#include "global.h"
#include "numwidgets.h"
#include "numer.h"
#include "numerpmatr.h"
#include "numer-comp.h"
#include "numer-chart.h"
#include "lic.h"
#include "layout_painter/reportcreator.h"
#include "appstyle.h"
#include "layout_painter/mathutils.h"
#include "layoutmanagerdlg.h"
*/
#include <QtGui>
#include "global.h"
#include "numwidgets.h"
#include "numer.h"
#include "numerpmatr.h"
#include "numer-comp.h"
#include "numer-chart.h"
#include "lic.h"
#include "layout_painter/reportcreator.h"
#include "appstyle.h"
#include "layout_painter/mathutils.h"
#include "layoutmanagerdlg.h"

#include <myqwebengineview.h>
#include <myqwebenginepage.h>

#include "includes.h"

#include <qtextdocumentwriter.h>
#include "webviewhtmltodocx.h"

//char ckey[]="hd_fv4bv$Vd]]76sf[v)sfcd22";
//char randstart[]="Afh9-lj[[[dsfm9i6jDgfbvBV1";
bool getpString(QString &cr);
void Crypt(char *inp, unsigned int inplen, char* key, unsigned int keylen);
bool getpString( QString &cr );
bool EncryptString( QString &cr );
bool DecryptString( QString &cr );


class CManualVieW : public QDialog
{
    Q_OBJECT

public:
    CAppStyle *appstyle;
    QWebView *webView;
    QPushButton *button;
    QCheckBox *chBox;
    bool *skipStartManual;

    CManualVieW(CAppStyle *style, bool *skVal, QWidget *parent = NULL) :
        QDialog(parent)
    {
#ifdef Q_OS_MAC
        QString qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/manual/manual/");
#else
        QString qsResPath = QCoreApplication::applicationDirPath() + QString("/images/manual/manual/");
#endif
        skipStartManual = skVal;

#ifdef Q_WS_WIN
		this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
#endif
		setWindowTitle("VeBest Numerology");
		setMinimumSize(600,300);
		setMaximumSize(1280,800);
        resize(800, 600);
        appstyle = style;
        webView = new QWebView(this);
		webView->move(10, 10);

        webView->load(QUrl("file:///" + qsResPath + "start.html"));

        button = new QPushButton(this);
        button->resize(100, 20);
        button->setText("Ok");

        appstyle->setWidgetStyle(button, WTButton);

        connect(button, SIGNAL(clicked()), this, SLOT(OnOk()));

        chBox = new QCheckBox(this);
        chBox->resize(200, 20);
        chBox->setText("Do not show this dialog again");

        appstyle->setWidgetStyle(chBox, WTCheckBox);
    }

private slots:
    void OnOk()
    {
        *skipStartManual = true;
        close();
    }

protected:
    void resizeEvent(QResizeEvent *e)
    {
        chBox->move(10, e->size().height()-30);

        button->move((e->size().width() -  button->width()) / 2, e->size().height()-30);

		webView->resize(e->size().width()-20, e->size().height()-10-40);

        QDialog::resizeEvent(e);
    }
};


class CInfoDetailsView : public QDialog
{
	Q_OBJECT

public:
	CAppStyle *appstyle;
	QWebView *webView;
	QPushButton *button;
	QCheckBox *chBox;

	CInfoDetailsView(CAppStyle *style, QString htmlData, QWidget *parent = NULL) :
	QDialog(parent)
	{
#ifdef Q_OS_MAC
		QString qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/manual/manual/");
#else
		QString qsResPath = QCoreApplication::applicationDirPath() + QString("/images/manual/manual/");
#endif

#ifdef Q_WS_WIN
		this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
#endif
		setWindowTitle("VeBest Numerology");
		setMinimumSize(600,300);
		setMaximumSize(1280,800);
		resize(600, 500);
		appstyle = style;
		webView = new QWebView(this);

		webView->setHtml(htmlData);
	}

protected:
	void resizeEvent(QResizeEvent *e)
	{
		webView->resize(e->size().width(), e->size().height());

		QDialog::resizeEvent(e);
	}

};

class CPainterBoard : public QWidget
{
    Q_OBJECT

public:
    CPainterBoard(QWidget *parent = NULL) :
        QWidget(parent)
    {

    }

    void setImage(QImage image)
    {
        img = image;
        repaint();
    }

protected:
    QImage img;
    void paintEvent(QPaintEvent *)
    {
        if (img.isNull())
            return;
        QPainter painter(this);
#ifdef Q_OS_MAC
        painter.setRenderHint(QPainter::SmoothPixmapTransform, true);
#endif
        QSize imgSize = img.size();
        imgSize.scale(size(), Qt::KeepAspectRatio);

        int x = ((width() - imgSize.width()) / 2);
        int y = ((height() - imgSize.height()) / 2);

        painter.drawImage(QRect(x - 1, y - 1, imgSize.width() + 2, imgSize.height() + 2), img);
    }

};

class CVBNum : public QMainWindow
{
	Q_OBJECT

public:
    const static int C_CTRL_BUT_W = 165, C_CTRL_BUT_H = 80, C_CTRL_BUT_H_STEP = 80;

    const static int C_MODE_NUMEROLOGY=0, C_MODE_PMATR=1, C_MODE_COMP=2, C_MODE_CEL=3, C_MODE_PET=4, C_MODE_CHART=5, C_MODE_ANALIZE = 6, C_MODE_FORECAST=7, C_MODE_GUIDE=8;
    const static int C_WIDGID_NAME=0, C_WIDGID_PE=1, C_WIDGID_MATR=2, C_WIDGID_PNAME=3, C_WIDGID_NAMESEL=4, C_WIDGID_ANALIZE = 5, C_WIDGID_CAL = 6, C_WIDGID_TES = 7;

    bool blockUI;
    int cmode;
    QString m_serialNumber;
    bool skipStartManual;
    quint32 styleAt;
    bool inpDateFormat;
    int mmcalcTo99;
    int registermode;
    CSecLic *lic;
    QDateTime lastUpdate;

    bool isLoaded;


    void loadData();
    void embedImagesToHtml(QString &src, QTextStream &dst, bool addAutoPrint=false);
    void embedImagesToHtml(QString &src, QString &dst, bool addAutoPrint);
    QNetworkAccessManager *nwmanager;
    QString m_publisherID;
    QString m_activationID;
    QPushButton *m_pPurchasePRO;
	QString manualStartPage;
	QString hpimagepath;
#ifdef VER_PREM
    QLabel *m_pReportLabel;
    QComboBox *m_pReportBox;
    QPushButton *m_pReportBut;
#endif
    bool firstStart;

    void saveData();

    struct SRes
    {
        QIcon fileSaveAs;
        QIcon helpAbout;
        QIcon helpHelp;
        QIcon filePrint;
        QIcon fileExportPDF;
		QIcon fileSettings;

        QImage titleBar;
        QImage titleBar2;
        //QImage titleBar3;

		QImage todayWidg;

        QImage topRes;
        QImage topResOpt;
        QImage botRes;
        QImage inpRes;

        QImage topResOpt2;
        QImage botRes2;
        QImage inpRes2;

		QImage logo;
        QImage logoh;
    } m_Res;

    struct SUserInfo
    {
        QString fullName;
        QString shortName;
        QDate birthDate;
        bool psex;
        QString partnerFullName;
        QString partnerShortName;
        QDate partnerBirthDate;

        QDate petBirthDate;
        QString petName;

        QString entryText;

        QVector<int> fnvw, snvw;
        QVector<int> pfnvw, psnvw;

        bool uCNC;
		int PMode;
    };

    struct SReports
    {
        bool rep[10];
    } m_repList;

    static bool rmDir(const QString &dirPath);
    static bool cpDir(const QString &srcPath, const QString &dstPath);
    static bool removeDir(const QString &dirName);

public:
    CVBNum(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~CVBNum();

    QString m_folder;
    int m_iCMode;
    bool m_bNameSel;
    CAppStyle *appstyle;

    CExpandWidget *expandPanel;

    QList<SUserInfo> users;

    QWidget *m_pGView;
    QWidget *m_pCtrlWidget;
    QScrollArea *m_pCtrlScrollArea;
    QLabel *m_pCtrlScrollAreaLine;
    QLabel *m_pCtrlSelLabel;
    QWidget *m_pSelPersWidg, *m_pSelPersWidgI;

    QWebView *m_pText;
    QWebView *m_pTextPMatr;
    QWebView *m_pTextComp;
    QWebView *m_pTextCel;
    QWebView *m_pTextPet;
    QWebView *m_pTextAn;
	QWebView *m_pTextGuide;
    CPainterBoard *topLevelLabel;

    QPushButton *m_pCtrlButNumer;
    QPushButton *m_pCtrlButPMatr;
    QPushButton *m_pCtrlButForecast;
    QPushButton *m_pCtrlButComp;
    QPushButton *m_pCtrlButCel;
    QPushButton *m_pCtrlButPet;
    QPushButton *m_pCtrlButChart;
    QPushButton *m_pSelPersBut;
	QPushButton *m_pGuideBut;

    QWidget *m_pStartUpScreen;

    //IC
    QPushButton *m_pCtrlButAnalize;
    QComboBox *comboChart;
    QLabel *labelChart;
    QPushButton *chartEditorBut;
    QPushButton *chartAddBut;
    QPushButton *chartDellBut;
    //*IC

    QTimeLine persButAnum;

    QPropertyAnimation *m_pAnimCtrlSel;
    QPropertyAnimation *m_pAnimNWGeom;
    QParallelAnimationGroup *m_pAnimGrpSetNumer, *m_pAnimGrpResNumer;
    QPropertyAnimation *m_pAnimGrpSetNumerName, *m_pAnimGrpResNumerName;
    QPropertyAnimation *m_pAnimGrpSetNumerPE, *m_pAnimGrpResNumerPE;
    QPropertyAnimation *m_pAnimGrpSetNumerE, *m_pAnimGrpResNumerE;
    QPropertyAnimation *m_pAnimGrpSetNumerCal, *m_pAnimGrpResNumerCal;
    QPropertyAnimation *m_pAnimGrpSetNumerWV, *m_pAnimGrpResNumerWV;
    QPropertyAnimation *m_pAnimGrpSetMatr, *m_pAnimGrpResMatr;
    QPropertyAnimation *m_pAnimGrpSetMatrWV, *m_pAnimGrpResMatrWV;
    QParallelAnimationGroup *m_pAnimGrpSetComp, *m_pAnimGrpResComp;
    QPropertyAnimation *m_pAnimGrpSetCompPM, *m_pAnimGrpResCompPM;
    QPropertyAnimation *m_pAnimGrpSetCompWV, *m_pAnimGrpResCompWV;
    QPropertyAnimation *m_pAnimGrpSetCel, *m_pAnimGrpResCel;
    QPropertyAnimation *m_pAnimGrpSetCelWV, *m_pAnimGrpResCelWV;
    QParallelAnimationGroup *m_pAnimGrpSetPet, *m_pAnimGrpResPet;
    QPropertyAnimation *m_pAnimGrpSetPetName, *m_pAnimGrpResPetName;
    QPropertyAnimation *m_pAnimGrpSetPetWV, *m_pAnimGrpResPetWV;
    QPropertyAnimation *m_pAnimNameSel;

    //IC
    QParallelAnimationGroup *m_pAnimGrpSetAnalize, *m_pAnimGrpResAnalize;
    QPropertyAnimation *m_pAnimGrpSetAnalizeName, *m_pAnimGrpResAnalizeName;
    QPropertyAnimation *m_pAnimGrpSetAnalizeNCalc, *m_pAnimGrpResAnalizeNCalc;
    QPropertyAnimation *m_pAnimGrpSetAnalizeButton, *m_pAnimGrpResAnalizeButton;
    CInfoTodayWidget *personalNumCalendar;
    //*IC

    CInfoWidget *m_pInfoMF;
    CInfoWidget *m_pInfoLC;
    CInfoWidget *m_pInfoAF;
    CInfoWidget *m_pInfoBR;
    CInfoNameWidget *m_pInfoName;
    CInfoExprPlanWidget *m_pInfoPE;
    CInfoEssenceWidget *m_pInfoE;
    CInfoCalendarWidget *m_pInfoCal;

    CInfoMatrWidget *m_pInfoPM;
    CInfoCompMatrWidget *m_pInfoCompPM;
    CInfoWidget *m_pInfoCMF;
    CInfoWidget *m_pInfoCFS;
    CInfoWidget *m_pInfoCLP;
    CInfoWidget *m_pInfoMFPet;

    //IC
    CInfoWidget *m_pInfoAnMF;
    CInfoNameWidget *m_pInfoAnName;
    QPushButton *m_pAnCalcButton;
    //*IC

    CInfoNameWidget *m_pInfoNamePet;
    QPushButton *m_pPetCalcButton;

    CInfoWidget *m_pInfoMFNS;
    CInfoNameWidget *m_pInfoNameNS;
    CInfoWidget *m_pInfoPMFNS;
    CInfoNameWidget *m_pInfoNamePNS;
    QPushButton *m_pSaveFileButton;
    QPushButton *m_pOpenFileButton;
	QPushButton *m_pNewFileButton;
    QPushButton *m_pClearRecent;
    static const int cRecentNum=8;
    QList<QPushButton*> m_pRecentList;
    QVector<SUserInfo> m_pRecentEntries;

    QString buttonRun;

    QLineEdit *m_pFullNameEdit;
    QLineEdit *m_pShortNameEdit;
    QDateEdit *m_pBDate;
    QDateEdit *m_pBDateF;
    QComboBox *m_pSex;
    QVector<int> m_viFnvw, m_viSnvw;
    QVector<int> m_viFnvw_l, m_viSnvw_l;

    QCheckBox *m_uCNC;
	QComboBox *m_PMode;
	QLabel *m_PModeLabel;

    QLineEdit *m_pPFullNameEdit;
    QLineEdit *m_pPShortNameEdit;
    QDateEdit *m_pPBDate;
    QVector<int> m_viPFnvw, m_viPSnvw;
    QVector<int> m_viPFnvw_l, m_viPSnvw_l;

    QLineEdit *m_pPetName;
    QDateEdit *m_pPetBDate;
    QWidget *m_pInpP;

    //IC
    QWidget *m_pInpAnalizer;
    QLineEdit *m_pTitleEdit;
    QDateEdit *m_pAnDate;
    QWidget *bkg1, *bkg2;
    QWidget *m_pSelPersWidgI_;


    //*IC

    CNumerology *m_pNumer;
    CNumerologyPMatrix *m_pNumerPMatr;
    CNumerology *m_pNumerPet;

    CNumerology *m_pNumerComp;
    CNumerologyPMatrix *m_pNumerPMatrComp;
    CCompNumerology *m_pCompNumerology;
    CChartNumerology *m_pChartNumerology;
    CNumerology *m_pNumerAn;
    CNumerology *m_pNumerCel;


    QMenu m_Menu;
    QAction *m_ActionHelp;
    QAction *m_ActionSupport;
    QAction *m_ActionAbout;
    QAction *m_ActionSettings;
    QAction *actionPrint;
    QAction *actionPrintAll;
    QAction *actionSaveRTF;
    QAction *actionExportPDF;
    QAction *actionExportPDFAll;
    QAction *actionRegister;

    QMenu *m_pStyleSubMenu;
    QActionGroup *styleMenuGroup;

    void updateRecentList();

    bool askReportsList();

    void setMode(int mode);
    int getSelIdx(int mode=-1);


    void loadRes(QString folder);
    bool resLoadImage(QString name, QImage &img);
    bool resLoadIcon(QString name, QIcon &icon);
    void loadDB();

    QRect calcNWGeometry(int mode=-1);
    QRect calcBWGeometry(int widgId, bool outOfScreen=false);

    bool getStatus();// { return true; }

	void checkForUpdates();

	void setFreeManual();
	void setFullManual();

    void setDateFormat(bool mode);

	void showHelper(int numerHelperID, QString imgName=QString());
public slots:

    void OnPersButAnim(int val);
    void OnPersButAnimFinish();

    void OnCtrlButNumer() { setMode(C_MODE_NUMEROLOGY); }
    void OnCtrlButPMatr() { setMode(C_MODE_PMATR); }
    void OnCtrlButForecast() { setMode(C_MODE_FORECAST); }
    void OnCtrlButComp() { setMode(C_MODE_COMP); }
    void OnCtrlButCel() { setMode(C_MODE_CEL); }
    void OnCtrlButPet() { setMode(C_MODE_PET); }
    void OnCtrlButChart() { setMode(C_MODE_CHART); }
    void OnCtrlButAnalize() { setMode(C_MODE_ANALIZE); }
	void OnCtrlButGuide() { setMode(C_MODE_GUIDE); }

    void OnUpdateListReport();

    void OnFinishedSU();
    void slotGChart(int value);
    void slotResizeChart();

    void OnMenu();
#ifdef VER_PREM
    void OnReportManager();
#endif
    void OnPrintPreview();
    void OnPrintPreviewAll();
    void printPreview(QPrinter *printer);
    void printPreviewAll(QPrinter *printer);
    void OnExportRTF();
    void OnExportPDF();
    void OnExportPDFAll();
    void OnRegister();
    void replyFinished(QNetworkReply *reply);
    void OnPurchasePRO();

    void slotPNC(QDate date);
    void OnEditChart();
    void OnAddChart();
    void OnDeleteChart();
    //*IC

    void OnNameSel();
    void OnNameVWChanged(int, int);
    void OnNamePVWChanged(int, int);
    void OnPreCalcNum();
    void OnPreCalcNumP();
    void OnTextChanged(const QString &);
	void OnParsChanged(int);
    void OnPTextChanged(const QString &);
	void OnDateChanged(const QDate &);

    void OnResData();
	void OnResDataI();
    void OnCalcNum();
    void OnCalcPetNum();
    void OnSaveFile(bool closeProg = false);
    void OnOpenFile(bool openProg = false);
    void OnRecentClicked();
    void OnClearRecent();
    void OnCalcAnNum();

    void OnSupport();
    void OnHelp();
    void OnAbout();
    void OnSettings();

    void initStyle();

    void OnStyleChange();
    void OnApplyStyle(int a);

    void OnSelReport(int);

	void MFitemCliked(int);
	void AFitemCliked(int);
	void LCitemCliked(int);
	//void BRitemCliked(int);
	void PMitemCliked(int);



protected:
    void resizeEvent(QResizeEvent *);

private:


    void startButAnim();
    void repaintChart();
    QImage getChart(QSize size = QSize(0, 0));
    QString qsResPath;
    //QPixmap *texture;

    QPushButton *m_pMenuButton;
    QPushButton *m_pClearAll;
    QPushButton *m_pAbout;
    QPushButton *m_pSave;
    QPushButton *m_pPrint;

    RepSettings settingReport;

    void PixMapResize(unsigned int toWidth, unsigned int toHeight, unsigned int &fromtWidth, unsigned int &fromHeight);

    unsigned int xWP;
    unsigned int yHP;

    int numTexture;

    bool RecentClicked;
    bool checkCalculateData;

signals:
    void updateStyle();
};

#endif // VBNUM_H
