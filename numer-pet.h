#ifndef PETNUMEROLOGY_H
#define PETNUMEROLOGY_H

#include <QtGui>

class CPetNumerology : public QObject
{
	Q_OBJECT

public:
	int m_BaseNums[64];

	int m_LifePath;
	int m_cNum1, m_cNum2, m_cNum3, m_cNum4;
	int m_cyear, m_PersYear;
	int m_PELet[64];
	int m_Let[64];
	int m_CalcMode[33];

	struct SNumerDetails
	{
		QString lifePath;
		QString birthDay;
		QString expression;
		QString mexpression;
		QString heartDesire;
		QString mheartDesire;
		QString personality;
		QString mpersonality;
		QString maturity;
		QString rationalThought;
		QString karmicDebts;
		QString karmicLessons;
		QString challenges;
		QString pinnacles;
		QString brLPE;
		QString brHDP;
		QString balance;
		QString persYear;
		QString hiddenPassion;
		QString subconsciousConfidence;
		QString ageSpan;

		QString PE[20];
		QString PENum[20];
		QString PECnt[20];
		QString PERep[20];



		QString fnameN1;
		QString fnameN2;
		QString fnameR;
		QString fnameR1;
		QString fnameR2;

		QString snameN1;
		QString snameN2;
		QString snameR;
		QString snameR1;
		QString snameR2;
	} numDet;


	QTextDocument m_TextDoc;


	static int downOnce(int num);
	int downToBase(int num);
	static int downToDigit(int num);
	static int statDownToBase(int num);

	static bool isBase(int num);
	static bool isCarmic(int num);

	int letterToNumber(QChar ch);
	int wordToNumber(QString str);
	void calcNamesNumbers(QString str, QList<int> &res);
	void calcNamesNumbersP(QString str, QList<int> &res, bool Vow);

	static int isVow(QChar sym);

	static int getPEIndex(QChar ch);

public:
	CPetNumerology(QObject *parent);
	~CPetNumerology();

	void setCalcMode(int mode);

	void draw(QPainter *pPntr);

	bool calculate(QString fullName, QString shortName, QDate bdate, QDate cdate, bool trial);

	void clear();
	void resetData();
};

#endif // PETNUMEROLOGY_H
