//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//		Copyright 2010. Intelliprotector.com
//		Web site: http://intelliprotector.com
//		Class: CIntelliProtector
//		Version: v3.1
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include <Windows.h>

namespace IntelliProtectorService
{
	// unit dimensions
	enum enUnitDimension
	{
		eudMinutes,
		eudHours,
		eudDays,
		eudWeeks,
		eudMonths
	};

	class CIntelliProtector
	{
	public:
		CIntelliProtector(void);
	public:
		virtual ~CIntelliProtector(void);

		static BOOL IsSoftwareProtected();
		static BOOL IsSoftwareRegistered();
		
		static int GetTrialDaysCount();
		static int GetTrialUnitsCount(IN int iDimension);

		static int GetTrialDaysLeftCount();
		static int GetTrialUnitsLeftCount(IN int iDimension);

		static int GetTrialLaunchesCount();
		static int GetTrialLaunchesLeftCount();

		static void ShowRegistrationWindow();
		static BOOL IsTrialElapsed();
		static void GetRenewalPurchaseLinkW(OUT wchar_t* pchLinkBuffer, int iMaxLength);
		static void GetRenewalPurchaseLinkA(OUT char* pchLinkBuffer, int iMaxLength);
		static void GetBuyNowLinkW(OUT wchar_t* pchLinkBuffer, IN int iMaxLength);
		static void GetBuyNowLinkA(OUT char* pchLinkBuffer, IN int iMaxLength);
		static int GetLicenseType();
		
		static int GetLicenseExpirationDaysCount();
		static int GetLicenseExpirationDaysLeftCount();

		static int GetSupportExpirationDaysCount();
		static int GetSupportExpirationDaysLeftCount();

		static void GetCurrentProductVersionW(OUT wchar_t* pchVersion, IN int iMaxLength);
		static void GetCurrentProductVersionA(OUT char* pchVersion, IN int iMaxLength);
		static void GetCustomerNameW(OUT wchar_t* pchCustomerName, IN int iMaxLength);
		static void GetCustomerNameA(OUT char* pchCustomerName, IN int iMaxLength);
		static void GetCustomerEMailW(OUT wchar_t* pchCustomerEMail, IN int iMaxLength);
		static void GetCustomerEMailA(OUT char* pchCustomerEMail, IN int iMaxLength);
		static void GetLicenseCodeW(OUT wchar_t* pchLicense, IN int iMaxLength);
		static void GetLicenseCodeA(OUT char* pchLicense, IN int iMaxLength);
		static BOOL RegisterSoftwareW(IN wchar_t* pchLicense);
		static BOOL RegisterSoftwareA(IN char* pchLicense);
		static BOOL RenewLicenseCodeW(IN wchar_t* pchRenewalCode);
		static BOOL RenewLicenseCodeA(IN char* pchRenewalCode);
		
		static void GetCurrentActivationDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute);
		static void GetCurrentRegistrationDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute);
		static void GetFirstRegistrationDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute);
		static void GetOrderDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute);
		static void GetLicenseExpirationDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute);
		static void GetProtectionDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute);
		static void GetSupportExpirationDate(OUT int& iYear, OUT int& iMonth, OUT int& iDay, OUT int& iHour, OUT int& iMinute);

		static void GetCurrentActivationDate(OUT SYSTEMTIME* psysTyme);
		static void GetCurrentRegistrationDate(OUT SYSTEMTIME* psysTyme);
		static void GetFirstRegistrationDate(OUT SYSTEMTIME* psysTyme);
		static void GetOrderDate(OUT SYSTEMTIME* psysTyme);
		static void GetLicenseExpirationDate(OUT SYSTEMTIME* psysTyme);
		static void GetSupportExpirationDate(OUT SYSTEMTIME* psysTyme);
		static void GetProtectionDate(OUT SYSTEMTIME* psysTyme);

		static void GetSupportExpirationProductVersionW(OUT wchar_t* pchVersion, IN int iMaxLength);
		static void GetSupportExpirationProductVersionA(OUT char* pchVersion, IN int iMaxLength);
		static void GetIntelliProtectorVersionW(OUT wchar_t* pchVersion, IN int iMaxLength);
		static void GetIntelliProtectorVersionA(OUT char* pchVersion, IN int iMaxLength);


private:
		static void FillSystemTime(OUT SYSTEMTIME* psysTyme, int iYear, int iMonth, int iDay, int iHour, int iMinute);

	};

}
