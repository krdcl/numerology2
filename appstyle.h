#ifndef APPSTYLE_H
#define APPSTYLE_H

#include "global.h"

#include <QtGui>
#include <QRegion>
#ifdef Q_WS_MAC
#include <QMacStyle>
#endif

void ASCheckScreenBoundary(QWidget *winwidg);

enum WidgetType {WTDlg = 0, WTButton = 1, WTCtrlButStyle = 2, WTTree = 3, WTIconList = 4, WTQsPersSelBut = 5, WTQsSelLab = 6, WTQsPersBkg = 7, WTQsSelPersIWidg = 8,
                 WTLabel = 9, WTEdit = 10, WTCheckBox = 11, WTButRect = 12, WTCombo = 14, WTMainList=16, WTBkWidg = 21, WTIOkButton = 23, WTICancelButton = 24, WTCustomTree = 25};

class CAppStyle
{
public:
    struct SPage
    {
        QImage pageLeft;
        QImage pageRight;
        QImage pageTop;
        QImage pageBottom;

        QImage pageLT;
        QImage pageRT;
        QImage pageLB;
        QImage pageRB;

        QColor canvasLT;
        QColor canvasRT;
        QColor canvasLB;
        QColor canvasRB;

        QColor pageColor;

        double spacing;

        QColor trackerColor;

        QImage iconScaleS;
        QImage iconRotateS;
        QImage iconMoveS;

        QImage iconScaleI;
        QImage iconScaleResI;

        QImage iconDataField;
        QImage iconDataFieldTitle;
        QImage imageDataField;
    } spagerep;

	struct sStyleInfo {
        QString ss, dlgss, labss, mainwss, okbut, canbut, repss;
        QString cBox;
        QString qsCtrlButStyle;
        QString qsPersSelButStyle;
        QString qsSelLabStyle;
        QString qsPersBkgStyle;
        QString qsSelPersIWidgStyle;
        QString qsButRect;

		QColor gs;
		int strength;
		bool genuine;

		sStyleInfo()
		{
			ss = "";
			dlgss = "";
			labss = "";
			mainwss = "";
            repss = "";
            okbut = "";
            canbut = "";
			gs = QColor::fromRgb(255,255,255);
			strength = 0;
			genuine = false;
		}
	};

public:
    CAppStyle(QString resPath);

    QString dataPath;
    void setWidgetStyle(QWidget *widget, WidgetType wt);

	QColor updateColor(QColor c);
	QString updateColor(QString c);
    QImage updateImage(QImage image);

	QString defss;
	QString defdlgss;
	QString deflabss;
	QString defmainwss;
    QString defokbut, defcanbut;
    QString defrepss;
    QString prigressBar;
    QString defQsCtrlButStyle;
    QString defQsPersSelButStyle;
    QString defQsSelLabStyle;
    QString defQsPersBkgStyle;
    QString defQsSelPersIWidgStyle;
    QString defButRect;

	QList<sStyleInfo> styleInfo;
    QIcon icnSearchOn, icnSearchOff;
    QIcon icnRows;

    QIcon icnTextBold, icnTextItalic, icnTextUnder;
    QPixmap lmcont, lmcont100, lmcont160, lmcont190, lmcont300, lmitem, lmitemBig;

    QPixmap lmitemIcon;
    QImage titleBar1, modifTitleBar1;
    QImage expandwidget;

	int cstyle;
	void setGlobalStyle(int id);
	QStringList stylesNames;

    QString genStyle(QString style, QColor gcol, int strength);
    bool loadImage(QString name, QImage &img);
    static QImage drawRectToImage(QImage img, int x, int y, int w, int h, int size, QColor color);

    static QString genStyleToItem(QFont font, QColor color, qreal scale = 1.0);
    static void genStyleToItem(QWidget *widget, QWidget *widget2, QMap<QString, QString> map, qreal scale = 1.0);

};

#endif // APPSTYLE_H
