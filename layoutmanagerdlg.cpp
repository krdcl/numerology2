#include "layoutmanagerdlg.h"
#include "layout_painter/textdlg.h"
#include "layout_painter/ccolordialog.h"
#include "infodlg.h"
#include "texteditor.h"





CWCLItem::CWCLItem(CAppStyle *style, QWidget *parent)
: QWidget(parent)
{
    name = "";
    sel = false;
    px = 0, py = 0;
    freeMove = false;
    appstyle = style;
    copyOnly = false;
    drop = false;

	contentColor = QColor::fromRgb(0,0,0,255);
	headerColor = QColor::fromRgb(0,0,0,255);
    headerFont = QFont("Arial", 18, QFont::Bold);
    headerFont.setPixelSize(18);
    contentFont = QFont("Arial", 16, QFont::Normal);
    contentFont.setPixelSize(16);

//    content.reserve(RESERVE_TEXTS);
//    imgList.reserve(RESERVE_TEXTS);
    for (int i = 0; i < RESERVE_TEXTS; i++)
    {
        content.append(QString());
        imgList.append(QImage());
        isDefList.append(true);
        headerName.append(QString());
    }


    isDefault = true;

    tmrid = startTimer(30);
}
//----------------------------------------------------------
CWCLItem::~CWCLItem()
{
    killTimer(tmrid);
}
//----------------------------------------------------------
void CWCLItem::timerEvent(QTimerEvent *)
{
    if (freeMove == false && drop == false)
    {
        int cx = this->x(), cy = this->y();

        int nx, ny;
        if (px>cx)
            nx = cx + (px-cx+1)/2;
        else
            nx = cx + (px-cx-1)/2;
        if (py>cy)
            ny = cy + (py-cy+1)/2;
        else
            ny = cy + (py-cy-1)/2;

        if ((cx!=nx) || (cy!=ny))
#ifdef Q_WS_MAC
            move(px, py);
#else
            move(nx, ny);
#endif
        else
            if (copyOnly) drop = true;
    }
}
//----------------------------------------------------------
void CWCLItem::drawItem(QPainter *painter, bool drawBackground)
{
    if (sel)
        painter->setPen(QPen(QColor(255, 255, 255)));
    else
        painter->setPen(QPen(QColor(237, 219, 178)));

    if (drawBackground)
    {
        painter->drawImage(0, 0, appstyle->updateImage(appstyle->lmitem.toImage()));

    }
    painter->drawText(1, 1, this->width()-2, this->height()-2-3, Qt::AlignHCenter | Qt::AlignVCenter, name);
}
//----------------------------------------------------------
void CWCLItem::drawItem(QPixmap *pixmap)
{
    (*pixmap) = QPixmap::fromImage(appstyle->updateImage(appstyle->lmitem.toImage()));
    QPainter painter(pixmap);

    drawItem(&painter, false);
}
//----------------------------------------------------------
void CWCLItem::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    drawItem(&painter);
}
//----------------------------------------------------------
//-------------                                -------------
//----------------------------------------------------------
CWCLContainerWidget::CWCLContainerWidget(CAppStyle *style, bool isDestination, QWidget *parent)
    : QWidget(parent), isDest(isDestination)
{
    appstyle = style;
    itemhs = 24;
    itemws = 180;
    itemdw = 150;
    itemdh = 22;
    sel = false;

    setMouseTracking(true);
    setAcceptDrops(true);
}

CWCLContainerWidget::~CWCLContainerWidget()
{
}
//----------------------------------------------------------
void CWCLContainerWidget::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("int/x-lm-item"))
        event->accept();
    else
       event->ignore();
}
//----------------------------------------------------------
void CWCLContainerWidget::dragMoveEvent(QDragMoveEvent *event)
{
    if (event->mimeData()->hasFormat("int/x-lm-item")) {
        event->setDropAction(Qt::MoveAction);
        event->accept();
        prepareItemPlace(event->pos().x(), event->pos().y());
        updateContainer();
    } else
        event->ignore();
}
//----------------------------------------------------------
void CWCLContainerWidget::dragLeaveEvent(QDragLeaveEvent *)
{
    prepareItemPlace(-1000,-1000);
    updateContainer();
}
//----------------------------------------------------------
void CWCLContainerWidget::dropEvent(QDropEvent *event)
{
    int del = 0;
    if (event->mimeData()->hasFormat("int/x-lm-item")) {
        CWCLContainerWidget *source =
            qobject_cast<CWCLContainerWidget *>(event->source());
        if (source!=NULL)
        {
            QByteArray pieceData = event->mimeData()->data("int/x-lm-item");
            QDataStream dataStream(&pieceData, QIODevice::ReadOnly);

            CWCLItem *item = new CWCLItem(appstyle, this);
            dataStream >> item->id >> item->name >> item->type >> item->headerFont >> item->contentFont
                    >> item->headerColor >> item->contentColor >> item->isDefault >> item->content >> item->imgList >> item->isDefList >> item->headerName >> item->extendedSettings;


            item->resize(itemdw, itemdh);
            item->show();
            QObject::connect(item, SIGNAL(itemSel(void *)), this, SLOT(OnItemSel(void *)));

            prepareItemPlace(event->pos().x(), event->pos().y());
            int idx = lmitems.indexOf(NULL);
            del = idx;
            lmitems.insert(idx, item);
            prepareItemPlace(-1000,-1000);
            updateContainer();
            item->move(item->px, item->py);

            event->setDropAction(Qt::MoveAction);
            event->accept();

            if (isDest == false)
            {
                delete lmitems.at(del);
                lmitems.removeAt(del);
                updateContainer();
                OnItemSel(NULL);
            }
            else
            {
                OnItemSel(lmitems.at(del));
            }


        } else
            event->ignore();
    } else
        event->ignore();

}
//----------------------------------------------------------
void CWCLContainerWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
        dragStartPosition = event->pos();
}
//----------------------------------------------------------
void CWCLContainerWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (!(event->buttons() & Qt::LeftButton))
        return;
    if ((event->pos() - dragStartPosition).manhattanLength()
        < QApplication::startDragDistance())
        return;

    startDrag();
}
//----------------------------------------------------------
void CWCLContainerWidget::startDrag()
{
    CWCLItem *item = (CWCLItem *)childAt(dragStartPosition);
    if (item != NULL)
    {
        QByteArray itemData;
        QDataStream dataStream(&itemData, QIODevice::WriteOnly);
        dataStream << item->id << item->name << item->type << item->headerFont << item->contentFont
                   << item->headerColor << item->contentColor << item->isDefault << item->content << item->imgList << item->isDefList << item->headerName << item->extendedSettings;

        QMimeData *mimeData = new QMimeData;
        mimeData->setData("int/x-lm-item", itemData);

        QPixmap pixmap;
        item->drawItem(&pixmap);

        QDrag *drag = new QDrag(this);
        drag->setMimeData(mimeData);
        drag->setHotSpot(QPoint(pixmap.width()/2, pixmap.height()/2));
        drag->setPixmap(pixmap);
        if (item->copyOnly == false)
            item->hide();
        int idx = lmitems.indexOf(item);
        lmitems.removeAt(idx);
        if (drag->exec(Qt::MoveAction) == Qt::MoveAction)
        {
            if (item->copyOnly == false)
            {
                delete item;
            }
            else
                lmitems.insert(idx, item);
        } else
        {
            item->show();
            lmitems.insert(idx, item);
        }
        prepareItemPlace(-1000,-1000);
        updateContainer();
    }
}
//----------------------------------------------------------
void CWCLContainerWidget::updateContainer()
{
    int h;
    if (lmitems.size()>0)
        h = (lmitems.size()+1)*itemhs;
    else
        h = this->height();

    if (h<parentWidget()->height())
        h = parentWidget()->height();

    if ((width()!=itemdw+2) || (height()!=h))
        resize(itemdw+8, h);

    for (int a=0; a<lmitems.size(); a++)
    {
        if (lmitems.at(a) != NULL)
        {
            calcItemPos(lmitems.at(a), a);
        }
    }
}
//----------------------------------------------------------
void CWCLContainerWidget::prepareItemPlace(int x, int y)
{
    lmitems.removeAll(NULL);

    QPoint pnt = QPoint(x, y);
    if ((pnt.x()>0) && (pnt.x()<width())
                        && (pnt.y()>0) && (pnt.y()<height()))
    {
        int ipx = pnt.y()/itemhs;
        lmitems.insert(ipx, NULL);
    }
}
//----------------------------------------------------------
void CWCLContainerWidget::calcItemPos(CWCLItem *item, int idx)
{
    item->px = 6;
    item->py = idx*itemhs+4;
#ifdef Q_WS_MAC
    item->move(item->px,item->py);
#endif
}
//----------------------------------------------------------
//-------------                                -------------
//----------------------------------------------------------
CWCLContainer::CWCLContainer(CAppStyle *style, QWidget *parent, bool isDestination)
: QScrollArea(parent)
{
    widg = new CWCLContainerWidget(style, isDestination);
    setWidget(widg);
    setBackgroundRole(QPalette::Dark);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    connect(widg, SIGNAL(signalRel()), this, SIGNAL(signalPress()));
}
//----------------------------------------------------------
CWCLContainer::~CWCLContainer()
{
}
//----------------------------------------------------------
void CWCLContainer::resizeEvent(QResizeEvent *)
{
    widg->updateContainer();
}
//----------------------------------------------------------
//-------------                                -------------
//----------------------------------------------------------
CLayoutManagerDlg::CLayoutManagerDlg(CAppStyle *style, QString selSetting, QWidget *parent)
    : QWidget(parent)
{
    index = -1;

    m_gridX = 10;
    m_gridY = 10;

    imgID = 0;

    m_gridXm = 7.5;
    m_gridYm = 7.5;

    appstyle = style;

    res = -1;
    citem = NULL;
    cursitem = NULL;
    ddstarted = false;
    ddprepare = false;

    okBut = new QPushButton("Ok", this);
    okBut->resize(80, 20);
    QObject::connect(okBut, SIGNAL(clicked()), this, SLOT(OnOk()));

    cancelBut = new QPushButton("Cancel", this);
    cancelBut->resize(80, 20);
    QObject::connect(cancelBut, SIGNAL(clicked()), this, SLOT(OnExit()));


    int w1 = 180, h2 = 545;
    int lx=400, ly=40+19, sy=46, cw1=170, ch1=25;

    repNameCB = new QComboBox(this);
    repNameCB->resize(cw1, ch1);
    repNameCB->move(lx, ly+sy*0-35);
    repNameCB->addItem("Numerology");
    repNameCB->addItem("Psycho Matrix");
    repNameCB->addItem("Love Compatibility");
    repNameCB->addItem("Celebrities");
    repNameCB->addItem("Pet");
    connect(repNameCB, SIGNAL(currentIndexChanged(int)), this, SLOT(OnRepName(int)));


    QStringList tmAddname;
    tmAddname << "Header" << "Life Path" << "Brithday" << "Expression" << "Minor Expression" << "Heart Desire" << "Minor Heart Desire"
    << "Personality" << "Maturity" << "Rational Thought" << "Karmic Lessons" << "Balance" << "Hidden Passion"
    << "Subconscious Confid." << "Pinnacles" << "Challenges" << "Personal Year" << "Personal Month" << "Personal Day"
    << "Transits" << "Essence" << "Minor Personality" << "Karmic Debts" << "Karmic Debt" << "Compatible" << "Compound"
    << "Planes of Expression" << "PE Physical" << "PE Mental" << "PE Emotional" << "PE Intuitive" << "Bridge LP-E" << "Bridge LP+E" << "Bridge HD-P" << "Bridge HD+P" << "Custom Text";

    nameList.append(tmAddname);
    tmAddname.clear();

    tmAddname <<  "Header" << "Character" << "Energy" << "Interest" << "Health" << "Intuition" << "Labour" << "Luck" << "Duty"
    << "Memory" << "Purposeful" << "Family Formation" << "Stability" << "Self Appraisal" << "Finance" << "Talent" << "Spirituality"
    << "Sexual Life" << "Custom Text";

    nameList.append(tmAddname);
    tmAddname.clear();

    tmAddname <<  "Header" << "Life Path" << "Birthday" << "Name" << "Psycho-matrix" << "Family" << "Carnal Interests" << "Detailed Birthday" << "Character" << "Energy" << "Final" << "Custom Text";

    nameList.append(tmAddname);
    tmAddname.clear();

    tmAddname <<  "Header" << "Life Path" << "Birthday" << "Expression" << "Custom Text";

    nameList.append(tmAddname);
    tmAddname.clear();

    tmAddname <<  "Header" << "Pet" << "Day Forecast" << "Custom Text";

    nameList.append(tmAddname);
    tmAddname.clear();

    headerNameBut = new QPushButton("Name", this);
    headerNameBut->resize(cw1, ch1);
    headerNameBut->move(lx, (ly+sy*2) - 12);
    connect(headerNameBut, SIGNAL(clicked()), this, SLOT(OnSetName()));

    headerFontBut = new QPushButton("Font", this);
    headerFontBut->resize(cw1, ch1);
    headerFontBut->move(lx, (ly+sy*3) - 24);
    connect(headerFontBut, SIGNAL(clicked()), this, SLOT(OnHeaderFont()));

    headerColorBut = new QPushButton("Color", this);
    headerColorBut->resize(cw1, ch1);
    headerColorBut->move(lx, (ly+sy*4) - 36);
    connect(headerColorBut, SIGNAL(clicked()), this, SLOT(OnHeaderColor()));

    contentFontBut = new QPushButton("Font", this);
    contentFontBut->resize(cw1, ch1);
    contentFontBut->move(lx, (ly+sy*4) + 46);
    connect(contentFontBut, SIGNAL(clicked()), this, SLOT(OnContentFont()));

    contentColorBut = new QPushButton("Color", this);
    contentColorBut->resize(cw1, ch1);
    contentColorBut->move(lx, (ly+sy*5) + 34);
    connect(contentColorBut, SIGNAL(clicked()), this, SLOT(OnContentColor()));



    hideAllImagesCB = new QCheckBox("Hide All Images", this);
    hideAllImagesCB->resize(cw1, ch1);
    hideAllImagesCB->move(lx, (ly+sy*1 -15-40));
    connect(hideAllImagesCB, SIGNAL(clicked()), this, SLOT(OnCheckAllImages()));

    hideCurImageCB = new QCheckBox("Hide Image", this);
    hideCurImageCB->resize(cw1, ch1);
    hideCurImageCB->move(lx, (ly+sy*1 + 10));
    connect(hideCurImageCB, SIGNAL(clicked()), this, SLOT(OnCheckCurImage()));



    defCB = new QCheckBox("Default", this);
    defCB->resize(cw1, ch1);
    defCB->move(lx, (ly+sy*1 - 30));
    connect(defCB, SIGNAL(clicked()), this, SLOT(OnCheckDef()));

    contentEditBut = new QPushButton("Edit", this);
    contentEditBut->resize(cw1, ch1);
    contentEditBut->move(lx, (ly+sy*3) + 58);
    connect(contentEditBut, SIGNAL(clicked()), this, SLOT(OnTextEdit()));


    settingList = new QListWidget(this);
    settingList->resize(170, 132);
    settingList->move(lx, (ly+sy*7) - 6);

    loadReportSettings(&settings);

    int curSet = -1;

    for (int i = 0; i < settings.size(); i++)
    {
        if (selSetting == settings.at(i).name)
            curSet = i;
        settingList->addItem(settings.at(i).name);
    }

    connect(settingList, SIGNAL(currentRowChanged(int)), this, SLOT(OnSelectSetting(int)));


    pNewSetting = new QPushButton("New", this);
    pNewSetting->move(lx, (ly+sy*8) - 18 + 111);
    pNewSetting->resize(50, 20);
    connect(pNewSetting, SIGNAL(clicked()), this, SLOT(OnNewSetting()));

    pCopySetting = new QPushButton("Copy", this);
    pCopySetting->move(lx + 60, (ly+sy*8) - 18 + 111);
    pCopySetting->resize(50, 20);
    connect(pCopySetting, SIGNAL(clicked()), this, SLOT(OnAddSetting()));

    pDelSetting = new QPushButton("Delete", this);
    pDelSetting->move(lx + 120, (ly+sy*8) - 18 + 111);
    pDelSetting->resize(50, 20);
    connect(pDelSetting, SIGNAL(clicked()), this, SLOT(OnDelSetting()));

    pRenameSetting = new QPushButton("Rename", this);
    pRenameSetting->move(lx, (ly+sy*8) - 18 + 111 + 30);
    pRenameSetting->resize(50, 20);
    connect(pRenameSetting, SIGNAL(clicked()), this, SLOT(OnRenameSetting()));

    pFolderSetting = new QPushButton("Folder", this);
    pFolderSetting->move(lx + 60, (ly+sy*8) - 18 + 111 + 30);
    pFolderSetting->resize(50, 20);
    connect(pFolderSetting, SIGNAL(clicked()), this, SLOT(OnFolderSetting()));

    CWCLContainer *fieldOrderActive = new CWCLContainer(appstyle, this);
    fieldOrderActive->resize(w1, h2);
    fieldOrderActive->move(15, 25);

    CWCLContainer *fieldOrderAccessed = new CWCLContainer(appstyle, this, false);
    fieldOrderAccessed->resize(w1, h2);
    fieldOrderAccessed->move(205, 25);

    lmcs << fieldOrderActive << fieldOrderAccessed;

    for (int a=0; a<lmcs.size(); a++)
    {
        if (a == 0 || a == 2)
        {
            QObject::connect(lmcs.at(a)->widg, SIGNAL(itemSel(void *)), this, SLOT(OnItemSelected(void *)));
        }
        else
        {
            QObject::connect(lmcs.at(a)->widg, SIGNAL(itemSel(void *)), this, SLOT(OnItemSelectedAlt(void*)));
        }

        QLabel *bkw;
        bkw = new QLabel(this);
        bkw->lower();
        bkw->setGeometry(lmcs.at(a)->geometry());
        bkw->setStyleSheet("QWidget, QLabel { background: transparent; border: none; }");
        bkw->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

        lmcs.at(a)->setGeometry(lmcs.at(a)->geometry().adjusted(1,1,-1,-1));
    }


    OnRepName(repNameCB->currentIndex());

    sz = QSizeF(8.5 * 4 * 25.4, 11 * 4 * 25.4);
    rname = "Untitled";

    initStyle();

    int h = this->height();
    int w = this->width();

#ifdef Q_WS_WIN
    okBut->move(w/2-90, h-30);
    cancelBut->move(w/2+10, h-30);
#else
    okBut->move(w/2+10, h-30);
    cancelBut->move(w/2-90, h-30);
#endif


    if (curSet >= 0)
    {
        settingList->setCurrentRow(curSet);
    }
    else
    {
        if (settings.size() > 0)
            settingList->setCurrentRow(0);
    }

    parentWidget()->setWindowTitle("Report Manager");

    OnRepName(repNameCB->currentIndex());
    updateFields();
}
//----------------------------------------------------------
CLayoutManagerDlg::~CLayoutManagerDlg()
{
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnHeaderFont()
{
    if (citem == NULL)
        return;
    CTextDlgWin *dlg = new CTextDlgWin(appstyle, this);
	if (citem->headerFont.pixelSize() <= 0)
		citem->headerFont.setPixelSize(16);
    dlg->mainWidget->SetFont(citem->headerFont);
    dlg->mainWidget->SetText("Example Text");
    dlg->exec();
	if (dlg->mainWidget->res == 0) {
        citem->headerFont = dlg->mainWidget->GetFont();
	}
    delete dlg;

    itemsToSet(repNameCB->currentIndex(), settingList->currentRow());
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnHeaderColor()
{
    if (citem == NULL)
        return;
    CColorDialog *dlg = new CColorDialog(true);
    dlg->slotSetColor(citem->headerColor);
    dlg->slotUpdate();
    dlg->exec();
    if (dlg->ground.drawMode != CANCEL_DLG)
    {
        citem->headerColor = dlg->ground.color;
    }
    delete dlg;
    itemsToSet(repNameCB->currentIndex(), settingList->currentRow());
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnContentFont()
{
    if (citem == NULL)
        return;
    CTextDlgWin *dlg = new CTextDlgWin(appstyle, this, true);
    if (citem->contentFont.pixelSize() <= 0)
        citem->contentFont.setPixelSize(14);
    dlg->mainWidget->SetFont(citem->contentFont);
    dlg->mainWidget->SetText("Example Text");
    dlg->exec();
	if (dlg->mainWidget->res == 0) {
        citem->contentFont = dlg->mainWidget->GetFont();
	}
    delete dlg;

    itemsToSet(repNameCB->currentIndex(), settingList->currentRow());
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnContentColor()
{
    if (citem == NULL)
        return;
    CColorDialog *dlg = new CColorDialog(true);
    dlg->slotSetColor(citem->contentColor);
    dlg->slotUpdate();
    dlg->exec();
    if (dlg->ground.drawMode != CANCEL_DLG)
    {
        citem->contentColor = dlg->ground.color;
    }
    delete dlg;
    itemsToSet(repNameCB->currentIndex(), settingList->currentRow());
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnTextEdit()
{
    if (citem == NULL)
        return;

    QString *p = NULL;
    getMassText(citem->type, citem->id, &p);

    if (p == NULL)
        return;

    CTextEditor *dlg = new CTextEditor(appstyle, &citem->content, &citem->imgList, &citem->isDefList, p, citem->type, citem->id, this);
    dlg->exec();
    delete dlg;

    itemsToSet(repNameCB->currentIndex(), settingList->currentRow());
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnSetName()
{
    if (citem == NULL)
        return;

    QString winTitle = "New Name";
    QString text = "Enter new setting name";

    CInfoDlgWin *dlg = new CInfoDlgWin(appstyle, this, 1);
    dlg->setWindowTitle(winTitle);
    dlg->mainWidget->winText->setText(text);

    dlg->mainWidget->winText->move(dlg->mainWidget->winText->x(), 50);
    dlg->mainWidget->winText->resize(380, 40);
    QLineEdit *edit = new QLineEdit(dlg->mainWidget);
    edit->setText(citem->headerName);
    edit->move(10, 90);
    edit->resize(380, 22);
    dlg->mainWidget->appstyle->setWidgetStyle(edit, WTEdit);
    dlg->exec();

    if (dlg->mainWidget->res == 0)
        citem->headerName = edit->text();


    itemsToSet(repNameCB->currentIndex(), settingList->currentRow());
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnRepName(int val)
{
    for (int i = 0; i < lmcs.at(1)->widg->lmitems.size(); i++)
    {
        delete lmcs.at(1)->widg->lmitems.at(i);
    }
    lmcs.at(1)->widg->lmitems.clear();

    int crow = settingList->currentRow();

    QString hikey = "Unknown";
    if (val == RTNumerology)
    {
        hikey = "RTNumerology-hide-images";

        addItem(RTNumerology, NTHeader, "Numerology Chart for");
        addItem(RTNumerology, NTLifePath, "Life Path Number");
        addItem(RTNumerology, NTBrithday, "Birthday Number");
        //addItem(RTNumerology, NTCompatible, "Compatible Number");
        addItem(RTNumerology, NTExpression, "Expression Number");
        addItem(RTNumerology, NTMinorExpression, "Minor Expression Number");
        addItem(RTNumerology, NTHeartDesire, "Heart Desire Number");
        addItem(RTNumerology, NTMinorHeartDesire, "Minor Heart Desire Number");
        addItem(RTNumerology, NTCompound, "Compound Number");
        addItem(RTNumerology, NTPersonality, "Personality Number");
        addItem(RTNumerology, NTMinorPersonality, "Minor Personality Number");
        addItem(RTNumerology, NTKarmicDebts, "Karmic Debts Numbers");
        //addItem(RTNumerology, NTKarmicDebt, "Karmic Debt Number:");
        addItem(RTNumerology, NTMaturity, "Maturity Number");
        addItem(RTNumerology, NTRationalThought, "Rational Thought Number");
        addItem(RTNumerology, NTKarmicLessons, "Karmic Lessons Number");
        addItem(RTNumerology, NTBalanceNumber, "Balance Number");
        addItem(RTNumerology, NTHiddenPassion, "Hidden Passion Number");
        addItem(RTNumerology, NTSubconsciousConfidence, "Subconscious Confidence Number");
        addItem(RTNumerology, NTPlanesExpression, "Planes of Expression");
        addItem(RTNumerology, NTPlanesExpressionM, "Planes of Expression Mental Number");
        addItem(RTNumerology, NTPlanesExpressionP, "Planes of Expression Physical Number");
        addItem(RTNumerology, NTPlanesExpressionE, "Planes of Expression Emotional Number");
        addItem(RTNumerology, NTPlanesExpressionI, "Planes of Expression Intuitive Number");
        addItem(RTNumerology, NTPinnacles, "Pinnacles Numbers");
        addItem(RTNumerology, NTChallenges, "Challenges Numbers");
        addItem(RTNumerology, NTBridgeLPE, "Bridge Life Path - Expression");
        addItem(RTNumerology, NTBridgeLPEDet, "");
        addItem(RTNumerology, NTBridgeHDP, "Bridge Heart's Desire - Personality");
        addItem(RTNumerology, NTBridgeHDPDet, "");
        addItem(RTNumerology, NTPersonalYear, "Personal Year Number:");
        addItem(RTNumerology, NTPersonalMonth, "Personal Month Number:");
        addItem(RTNumerology, NTPersonalDay, "Personal Day Number:");
        addItem(RTNumerology, NTTransits, "Transits for age");
        addItem(RTNumerology, NTEssence, "Essence for age");
        addItem(RTNumerology, NTCustomText, "Custom Text");
    }
    else
    if (val == RTPsychoMatrix)
    {
        hikey = "RTPsychoMatrix-hide-images";

        addItem(RTPsychoMatrix, PMTHeader, "Numerology Psychomatrix Chart for");
        addItem(RTPsychoMatrix, PMTCharacter, "Character (1)");
        addItem(RTPsychoMatrix, PMTEnergy, "Energy (2)");
        addItem(RTPsychoMatrix, PMTInterest, "Interest (3)");
        addItem(RTPsychoMatrix, PMTHealth, "Health (4)");
        addItem(RTPsychoMatrix, PMTIntuition, "Intuition (5)");
        addItem(RTPsychoMatrix, PMTLabour, "Labour (6)");
        addItem(RTPsychoMatrix, PMTLuck, "Luck (7)");
        addItem(RTPsychoMatrix, PMTDuty, "Duty (8)");
        addItem(RTPsychoMatrix, PMTMemory, "Memory (9)");
        addItem(RTPsychoMatrix, PMTPurposeful, "Purposeful (1-4-7)");
        addItem(RTPsychoMatrix, PMTFamilyFormation, "Family formation (2-5-8)");
        addItem(RTPsychoMatrix, PMTStability, "Stability (3-6-9)");
        addItem(RTPsychoMatrix, PMTSelfAppraisal, "Self appraisal (1-2-3)");
        addItem(RTPsychoMatrix, PMTFinance, "Finance (4-5-6)");
        addItem(RTPsychoMatrix, PMTTalent, "Talent (7-8-9)");
        addItem(RTPsychoMatrix, PMTSpirituality, "Spirituality (1-5-9)");
        addItem(RTPsychoMatrix, PMTSexualLife, "Sexual life (3-5-7)");
        addItem(RTPsychoMatrix, PMTCustomText, "Custom Text");
    }
    else
    if (val == RTLoveCompatibility)
    {
        hikey = "RTLoveCompatibility-hide-images";

        addItem(RTLoveCompatibility, LCTHeader, "Numerology Compatibility Report");
        addItem(RTLoveCompatibility, LCTLifePath, "Life Path Compatibility");
        addItem(RTLoveCompatibility, LCTBirthday, "Birthday Compatibility");
        addItem(RTLoveCompatibility, LCTDetailedBirthday);
        addItem(RTLoveCompatibility, LCTName, "Name Compatibility");
        addItem(RTLoveCompatibility, LCTPsychoMatrix, "Psycho-matrix");
        addItem(RTLoveCompatibility, LCTFamily, "Family");
        addItem(RTLoveCompatibility, LCTCharacter, "PM Character");
        addItem(RTLoveCompatibility, LCTEnergy, "PM Energy");
        addItem(RTLoveCompatibility, LCTCarnalInterests, "Carnal Interests");
        addItem(RTLoveCompatibility, LCTFinal);
        addItem(RTLoveCompatibility, LCTCustomText, "Custom Text");
    }
    else
    if (val == RTCelebrities)
    {
        hikey = "RTCelebrities-hide-images";

        addItem(RTCelebrities, CTHeader, "Celebrities Report for");
        addItem(RTCelebrities, CTLifePath, "Celebrities with same Life Path");
        addItem(RTCelebrities, CTBrithday, "Celebrities with same Birth day");
        addItem(RTCelebrities, CTExpression, "Celebrities with same Expression number");
        addItem(RTCelebrities, CTCustomText, "Custom Text");
    }
    else
    if (val == RTPet)
    {
        hikey = "RTPet-hide-images";

        addItem(RTPet, PTHeader, "Pet Numerology Chart");
        addItem(RTPet, PTPet, "Pet number");
        addItem(RTPet, PTDayForecast, "Day Forecast for");
        addItem(RTPet, PTCustomText, "Custom Text");
    }

    if ((crow>=0) && (crow<settings.size())) {
        if (settings[crow].extendedSettings.value(hikey,"FALSE")!="TRUE")
            hideAllImagesCB->setChecked(false);
        else
            hideAllImagesCB->setChecked(true);
    }

    setToItems(val, settingList->currentRow());

    lmcs.at(0)->widg->updateContainer();
    lmcs.at(1)->widg->updateContainer();
}
//----------------------------------------------------------
QString CLayoutManagerDlg::getItemName(int type, int id)
{
    if (nameList.size() <= type)
        return QString();

    if (nameList.at(type).size() <= id)
        return QString();

    return nameList.at(type).at(id);
}
//----------------------------------------------------------
void CLayoutManagerDlg::addItem(int type, int id, QString headerNamr)
{
    CWCLItem *item = new CWCLItem(appstyle, lmcs.at(1)->widg);
    item->type = type;
    item->id = id;
    item->name = getItemName(type, id);
    item->resize(lmcs.at(1)->widg->itemdw, lmcs.at(1)->widg->itemdh);
    item->copyOnly = true;
    item->headerName = headerNamr;

    item->headerFont.setPointSize(16);
    item->contentFont.setPointSize(14);

    item->show();
    lmcs.at(1)->widg->lmitems.append(item);
}
//----------------------------------------------------------
void CLayoutManagerDlg::clearSel()
{
    for (int a=0; a<lmcs.size(); a++)
    {
        for (int n=0; n<lmcs.at(a)->widg->lmitems.size(); n++)
        {
            if (lmcs.at(a)->widg->lmitems.at(n)->sel)
            {
                lmcs.at(a)->widg->lmitems.at(n)->sel = false;
                lmcs.at(a)->widg->lmitems.at(n)->repaint();
            }
        }
    }
    citem = NULL;
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnItemSelectedAlt(void *)
{
    OnItemSelected(NULL);
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnItemSelected(void *ip)
{
    clearSel();

    for (int a=0; a<lmcs.size(); a++)
    {
        for (int n=0; n<lmcs.at(a)->widg->lmitems.size(); n++)
        {
            if (lmcs.at(a)->widg->lmitems.at(n) == ip)
            {
                citem = (CWCLItem *)ip;
                citem->sel = true;
                citem->repaint();
                break;
            }
        }
    }

    updateFields();
}
//----------------------------------------------------------
void CLayoutManagerDlg::checkCurItem()
{
    bool passed = false;
    for (int a=0; a<lmcs.size(); a++)
    {
        for (int n=0; n<lmcs.at(a)->widg->lmitems.size(); n++)
        {
            if (lmcs.at(a)->widg->lmitems.at(n) == citem)
            {
                passed = true;
                break;
            }
        }
    }

    if (!passed)
    {
        citem = NULL;
        updateFields();
    }
}
//----------------------------------------------------------
void CLayoutManagerDlg::updateFields()
{
    disconnect(defCB, SIGNAL(clicked()), this, SLOT(OnCheckDef()));
    itemsToSet(repNameCB->currentIndex(), settingList->currentRow());

    int crow = settingList->currentRow();

    if (crow < 0)
    {
        lmcs.at(0)->setEnabled(false);
        lmcs.at(1)->setEnabled(false);
        repNameCB->setEnabled(false);
    }
    else
    {
        lmcs.at(0)->setEnabled(true);
        lmcs.at(1)->setEnabled(true);
        repNameCB->setEnabled(true);
    }

    if (citem == NULL)
    {
        headerFontBut->setEnabled(false);
        headerNameBut->setEnabled(false);
        hideCurImageCB->setEnabled(false);
        hideCurImageCB->setChecked(false);
        defCB->setEnabled(false);
        defCB->setChecked(true);
        headerColorBut->setEnabled(false);
        contentFontBut->setEnabled(false);
        contentColorBut->setEnabled(false);
        contentEditBut->setEnabled(false);

        connect(defCB, SIGNAL(clicked()), this, SLOT(OnCheckDef()));
        return;
    }

    headerFontBut->setEnabled(true);
    headerNameBut->setEnabled(true);
    hideCurImageCB->setEnabled(true);
    defCB->setEnabled(true);
    headerColorBut->setEnabled(true);
    contentFontBut->setEnabled(true);
    contentColorBut->setEnabled(true);
    contentEditBut->setEnabled(true);

    if (!citem->isDefault)
    {
        defCB->setChecked(false);
        headerFontBut->setEnabled(true);
        headerNameBut->setEnabled(true);
        hideCurImageCB->setEnabled(true);
        headerColorBut->setEnabled(true);
        contentFontBut->setEnabled(true);
        contentColorBut->setEnabled(true);
        contentEditBut->setEnabled(true);
    }
    else
    {
        defCB->setChecked(true);
        headerFontBut->setEnabled(false);
        headerNameBut->setEnabled(false);
        hideCurImageCB->setEnabled(false);
        headerColorBut->setEnabled(false);
        contentFontBut->setEnabled(false);
        contentColorBut->setEnabled(false);
        contentEditBut->setEnabled(false);
    }

    if ((citem->id == 0 && citem->type != RTLoveCompatibility) || citem->isDefault || (citem->id == LCTPsychoMatrix && citem->type == RTLoveCompatibility))
        contentEditBut->setEnabled(false);
    else
        contentEditBut->setEnabled(true);

    if (/*citem->id == 0 || */citem->isDefault)
        headerNameBut->setEnabled(false);
    else
        headerNameBut->setEnabled(true);

    if (citem->extendedSettings.value("hide-image","FALSE")!="TRUE")
        hideCurImageCB->setChecked(false);
    else
        hideCurImageCB->setChecked(true);

    connect(defCB, SIGNAL(clicked()), this, SLOT(OnCheckDef()));
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnCheckDef()
{
    if (citem == NULL)
        return;

    citem->isDefault = defCB->isChecked();

    itemsToSet(repNameCB->currentIndex(), settingList->currentRow());
    updateFields();
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnCheckCurImage() {
    if (citem == NULL)
        return;

    if (hideCurImageCB->isChecked())
        citem->extendedSettings["hide-image"] = "TRUE";
    else
        citem->extendedSettings["hide-image"] = "FALSE";

    itemsToSet(repNameCB->currentIndex(), settingList->currentRow());
    updateFields();
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnCheckAllImages() {
    int val = repNameCB->currentIndex();
    int crow = settingList->currentRow();
    QString hikey = "";
    if (val == RTNumerology)
        hikey = "RTNumerology-hide-images";
    else if (val == RTPsychoMatrix)
        hikey = "RTPsychoMatrix-hide-images";
    else if (val == RTLoveCompatibility)
        hikey = "RTLoveCompatibility-hide-images";
    else if (val == RTCelebrities)
        hikey = "RTCelebrities-hide-images";
    else if (val == RTPet)
        hikey = "RTPet-hide-images";

    if ((hikey.size()>0)&&(crow>=0) && (crow<settings.size())) {
        if (hideAllImagesCB->isChecked())
            settings[crow].extendedSettings[hikey] = "TRUE";
        else
            settings[crow].extendedSettings[hikey] = "FALSE";
    }

    itemsToSet(val, crow);
    updateFields();
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnOk(bool noClose)
{
    saveReportSettings(&settings);
    QListWidgetItem *item = NULL;
    item = settingList->currentItem();
    if (item != NULL)
        curSetting_ = item->text();
    res = 0;
    if (!noClose)
        parentWidget()->close();
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnExit()
{
    parentWidget()->close();
}
//----------------------------------------------------------
void CLayoutManagerDlg::itemsToSet(int repType, int setId)
{
    if (setId < 0)
        return;

    if (setId >= settings.size())
        return;

    QList<repItem> *rep = NULL;
    if (repType == 0)
        rep = &settings[setId].Numerology;
    else
    if (repType == 1)
        rep = &settings[setId].PsychoMatrix;
    else
    if (repType == 2)
        rep = &settings[setId].LoveCompatibility;
    else
    if (repType == 3)
        rep = &settings[setId].Celebrities;
    else
    if (repType == 4)
        rep = &settings[setId].Pet;

    if (rep == NULL)
        return;

    rep->clear();

    for (int i = 0; i < lmcs.at(0)->widg->lmitems.size(); i++)
    {
        repItem item;
        CWCLItem *itW = lmcs.at(0)->widg->lmitems.at(i);
        item.id = itW->id;
        item.name = itW->name;
        item.type = itW->type;
        item.headerFont = itW->headerFont;
        item.contentFont = itW->contentFont;
        item.headerColor = itW->headerColor;
        item.contentColor = itW->contentColor;
        item.isContentDefault = itW->isDefault;

        for (int i = itW->content.size(); i < RESERVE_TEXTS; i++)
            itW->content.append(QString());
        for (int i = itW->imgList.size(); i < RESERVE_TEXTS; i++)
            itW->imgList.append(QImage());
        for (int i = itW->isDefList.size(); i < RESERVE_TEXTS; i++)
            itW->isDefList.append(true);
        for (int i = itW->headerName.size(); i < RESERVE_TEXTS; i++)
            itW->headerName.append(QString());

        item.content = itW->content;
        item.imgList = itW->imgList;
        item.isDefList = itW->isDefList;
        item.headerName = itW->headerName;
        item.extendedSettings = itW->extendedSettings;

        rep->append(item);
    }
}
//----------------------------------------------------------
void CLayoutManagerDlg::setToItems(int repType, int setId)
{
    if (setId < 0)
        return;

    if (setId >= settings.size())
        return;

    QList<repItem> *rep = NULL;
    if (repType == 0)
        rep = &settings[setId].Numerology;
    else
    if (repType == 1)
        rep = &settings[setId].PsychoMatrix;
    else
    if (repType == 2)
        rep = &settings[setId].LoveCompatibility;
    else
    if (repType == 3)
        rep = &settings[setId].Celebrities;
    else
    if (repType == 4)
        rep = &settings[setId].Pet;

    if (rep == NULL)
        return;

    for (int i = 0; i < lmcs.at(0)->widg->lmitems.size(); i++)
    {
        delete lmcs.at(0)->widg->lmitems.at(i);
    }
    lmcs.at(0)->widg->lmitems.clear();

    for (int i = 0; i < rep->size(); i++)
    {
        repItem item = rep->at(i);

        CWCLItem *itW = new CWCLItem(appstyle, lmcs.at(0)->widg);

        itW->id = item.id;
        itW->name = getItemName(item.type, item.id);//item.name;
        itW->type = item.type;
        itW->headerFont = item.headerFont;
        itW->contentFont = item.contentFont;
        itW->headerColor = item.headerColor;
        itW->contentColor = item.contentColor;
        itW->isDefault = item.isContentDefault;

        for (int i = item.content.size(); i < RESERVE_TEXTS; i++)
            item.content.append(QString());
        for (int i = item.imgList.size(); i < RESERVE_TEXTS; i++)
            item.imgList.append(QImage());
        for (int i = item.isDefList.size(); i < RESERVE_TEXTS; i++)
            item.isDefList.append(true);
        for (int i = item.headerName.size(); i < RESERVE_TEXTS; i++)
            item.headerName.append(QString());


        itW->content = item.content;
        itW->imgList = item.imgList;
        itW->isDefList = item.isDefList;
        itW->headerName = item.headerName;
        itW->extendedSettings = item.extendedSettings;
        itW->resize(lmcs.at(0)->widg->itemdw, lmcs.at(0)->widg->itemdh);

        connect(itW, SIGNAL(itemSel(void *)), lmcs.at(0)->widg, SLOT(OnItemSel(void *)));

        itW->show();
        lmcs.at(0)->widg->lmitems.append(itW);
    }

    citem = NULL;
    updateFields();
    lmcs.at(0)->widg->updateContainer();
}
//----------------------------------------------------------
//void CLayoutManagerDlg::addFieldGrid(int x, int y, int idx, CREPage *page, CReportPE *report, QSize objSz, int imgNum)
//{
//    int xPix = gridToPix(x) + 10;
//    int yPix = gridToPix(y) + 10;

//    OnAddField(QPoint(xPix, yPix), idx, page, report, objSz, imgNum);
//}
////----------------------------------------------------------
//void CLayoutManagerDlg::OnAddField(QPoint pos, int idx, CREPage *page, CReportPE *report, QSize objSz, int imgNum)
//{
//    CREPage *pPage = page;

//    QTransform Matrix = report->calcDocViewTransform();
//    QPointF m_placePoint = Matrix.inverted().map(QPointF(pos.x(), pos.y()));


//    CREPageItem *obj = new CREPageItem(data);
//    CREPageItem *slot = new CREPageItem(data);
//    slot->m_pContainer = obj;
//    slot->setType(OT_SLOT);
//    slot->m_bSolidContainer = true;
//    obj->setType(OT_DATA_FIELD);
//    obj->m_pOwner = slot;

//    obj->setText(data->doc.docHeader.at(idx).dname);

//    QPainterPath defShape;
//    defShape.addRect(0,0, objSz.width(), objSz.height());
//    slot->setShape(defShape);
//    slot->setBoundSize(defShape.boundingRect().size());
//    obj->setBoundSize(defShape.boundingRect().size());

//    if (m_placePoint.x()+objSz.width()+1 > pPage->getPageSize().width() )
//        m_placePoint.setX(pPage->getPageSize().width() - objSz.width()-1);
//    if (m_placePoint.y()+objSz.height()+1 > pPage->getPageSize().height())
//        m_placePoint.setY(pPage->getPageSize().height() - objSz.height()-1);

//    slot->move(m_placePoint.x(), m_placePoint.y());
//    page->moveToPage(slot);
//    pPage->addObject(slot);


//    if (data->doc.docHeader.at(idx).dtype == CDataFile::ETImageList)
//    {
//        obj->id = data->doc.docHeader.at(idx).idname;
//        obj->isLabel = false;
//        obj->setType(OT_DATA_IMAGE);
//        obj->m_Idx = imgNum;
//        obj->m_Align = Qt::AlignHCenter;
//    } else
//    {
//        obj->id = data->doc.docHeader.at(idx).idname;
//        obj->isLabel = false;
//    }
//}
//----------------------------------------------------------
void CLayoutManagerDlg::initStyle()
{
    appstyle->setWidgetStyle(this, WTDlg);

    appstyle->setWidgetStyle(okBut, WTIOkButton);
    appstyle->setWidgetStyle(cancelBut, WTICancelButton);

    appstyle->setWidgetStyle(headerFontBut, WTButton);
    appstyle->setWidgetStyle(headerNameBut, WTButton);
    appstyle->setWidgetStyle(headerColorBut, WTButton);

    appstyle->setWidgetStyle(contentFontBut, WTButton);
    appstyle->setWidgetStyle(contentColorBut, WTButton);

    appstyle->setWidgetStyle(repNameCB, WTCombo);

    appstyle->setWidgetStyle(contentEditBut, WTButton);
    appstyle->setWidgetStyle(defCB, WTDlg);

    appstyle->setWidgetStyle(pNewSetting, WTButton);
    appstyle->setWidgetStyle(pCopySetting, WTButton);
    appstyle->setWidgetStyle(pDelSetting, WTButton);
    appstyle->setWidgetStyle(pRenameSetting, WTButton);
    appstyle->setWidgetStyle(pFolderSetting, WTButton);

    appstyle->setWidgetStyle(settingList, WTMainList);

    QString dlgTitle;

    parentWidget()->setWindowTitle(dlgTitle);

    int wszx=580, wszy=625;

    resize(wszx, wszy);
    parentWidget()->resize(wszx, wszy);
    parentWidget()->setFixedSize(wszx, wszy);
    parentWidget()->setWindowFlags(Qt::Dialog | Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::CustomizeWindowHint);

}
//----------------------------------------------------------
void CLayoutManagerDlg::saveReportSettings(RepSettings *setList)
{
    QString path = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + "/VeBest Numerology/";
    QDir().mkpath(path);
    QDir dir(path);
    QStringList repList = dir.entryList(QStringList("*.vbnr"));

    QDir().mkpath(path);

    for (int i = 0; i < setList->size(); i++)
    {
        int infOf = repList.indexOf(setList->at(i).name + ".vbnr");
        if (infOf >= 0)
            repList.removeAt(infOf);

        QFile file(path + setList->at(i).name + ".vbnr");
        if (!file.open(QFile::WriteOnly))
            continue;

        QDataStream stream(&file);

        stream << (quint32)TYPE_FILE;
        stream << (quint32)CUR_VER_FILE_REP;
        saveSet(&stream, &setList->at(i).Numerology);
        saveSet(&stream, &setList->at(i).PsychoMatrix);
        saveSet(&stream, &setList->at(i).LoveCompatibility);
        saveSet(&stream, &setList->at(i).Celebrities);
        saveSet(&stream, &setList->at(i).Pet);
		stream << setList->at(i).extendedSettings;

        file.close();
    }

    for (int i = 0; i < repList.size(); i++)
    {
        QFile::remove(path + repList.at(i));
    }
}
//----------------------------------------------------------
void CLayoutManagerDlg::saveSet(QDataStream *stream, const QList<repItem> *rep)
{
    *stream << (quint32)rep->size();
    for (int i = 0; i < rep->size(); i++)
    {
        *stream << rep->at(i).id;
        *stream << rep->at(i).name;
        *stream << rep->at(i).type;
        *stream << rep->at(i).headerFont;
        *stream << rep->at(i).contentFont;
        *stream << rep->at(i).headerColor;
        *stream << rep->at(i).contentColor;
        *stream << rep->at(i).isContentDefault;
        *stream << rep->at(i).content;
        *stream << rep->at(i).imgList;
        *stream << rep->at(i).isDefList;
        *stream << rep->at(i).headerName;
        *stream << rep->at(i).extendedSettings;
    }
}
//----------------------------------------------------------
void CLayoutManagerDlg::loadReportSettings(RepSettings *setList)
{
    setList->clear();
    QString path = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + "/VeBest Numerology/";
    QDir().mkpath(path);
    QDir dir(path);
    QStringList repList = dir.entryList(QStringList("*.vbnr"));
    for (int i = 0; i < repList.size(); i++)
    {
        QFile file(path + repList.at(i));
        if (!file.open(QFile::ReadOnly))
            continue;

        QDataStream stream(&file);
        quint32 uint32;
        stream >> uint32;
        if (uint32 != TYPE_FILE)
            continue;

        quint32 ver;
        stream >> ver;
        if (ver > CUR_VER_FILE_REP)
            continue;

        reportSetting lSet;
        lSet.name = repList.at(i);
        lSet.name.chop(5);

        loadSet(&stream, &lSet.Numerology, ver);
        loadSet(&stream, &lSet.PsychoMatrix, ver);
        loadSet(&stream, &lSet.LoveCompatibility, ver);
        loadSet(&stream, &lSet.Celebrities, ver);
        loadSet(&stream, &lSet.Pet, ver);
		if (!file.atEnd())
			stream >> lSet.extendedSettings;

		if (lSet.extendedSettings["UPDATE1"].size()==0) {
			lSet.extendedSettings["UPDATE1"] = "TRUE";

			bool skip=false;
			for (int a=0; a<lSet.Numerology.size(); a++) {
				if ((lSet.Numerology.at(a).id == NTPlanesExpression)
					|| (lSet.Numerology.at(a).id == NTPlanesExpressionM)
					|| (lSet.Numerology.at(a).id == NTPlanesExpressionP)
					|| (lSet.Numerology.at(a).id == NTPlanesExpressionE)
					|| (lSet.Numerology.at(a).id == NTPlanesExpressionI)) {
						skip = true;
						break;
				}
			}

			if (!skip) {
				for (int a=0; a<lSet.Numerology.size(); a++) {
					if (lSet.Numerology.at(a).id == NTSubconsciousConfidence) {
						repItem pe, pemental, pephisical, peemotional, peintuitive;

						addItemSet(pe, RTNumerology, NTPlanesExpression, "Planes of Expression");
						addItemSet(pemental, RTNumerology, NTPlanesExpressionM, "Planes of Expression Mental Number");
						addItemSet(pephisical, RTNumerology, NTPlanesExpressionP, "Planes of Expression Physical Number");
						addItemSet(peemotional, RTNumerology, NTPlanesExpressionE, "Planes of Expression Emotional Number");
						addItemSet(peintuitive, RTNumerology, NTPlanesExpressionI, "Planes of Expression Intuitive Number");

						lSet.Numerology.insert(a+1,pe);
						lSet.Numerology.insert(a+2,pemental);
						lSet.Numerology.insert(a+3,pephisical);
						lSet.Numerology.insert(a+4,peemotional);
						lSet.Numerology.insert(a+5,peintuitive);
						break;
					}
				}

				for (int a=0; a<lSet.LoveCompatibility.size(); a++) {
					if (lSet.LoveCompatibility.at(a).id == LCTFamily) {
						repItem lcchar, lcenergy, lcfinal;

						addItemSet(lcchar, RTLoveCompatibility, LCTCharacter, "Character");
						addItemSet(lcenergy, RTLoveCompatibility, LCTEnergy, "Energy");
						addItemSet(lcfinal, RTLoveCompatibility, LCTFinal, "Final");


						lSet.LoveCompatibility.insert(a+1,lcchar);
						lSet.LoveCompatibility.insert(a+2,lcenergy);

						lSet.LoveCompatibility.insert(lSet.LoveCompatibility.size(),lcfinal);
						break;
					}
				}
			}
		}

        if (lSet.extendedSettings["UPDATE2"].size()==0) {
            lSet.extendedSettings["UPDATE2"] = "TRUE";

            bool skip=false;
            for (int a=0; a<lSet.Numerology.size(); a++) {
                if ((lSet.Numerology.at(a).id == NTBridgeLPE)
                    || (lSet.Numerology.at(a).id == NTBridgeLPEDet)
                    || (lSet.Numerology.at(a).id == NTBridgeHDP)
                    || (lSet.Numerology.at(a).id == NTBridgeHDPDet)) {
                        skip = true;
                        break;
                }
            }

            if (!skip) {
                for (int a=0; a<lSet.Numerology.size(); a++) {
                    if (lSet.Numerology.at(a).id == NTChallenges) {
                        repItem brLPE, brLPED, brHDP, brHDPD;

                        addItemSet(brLPE, RTNumerology, NTBridgeLPE, "Bridge Life Path - Expression");
                        addItemSet(brLPED, RTNumerology, NTBridgeLPEDet, "");
                        addItemSet(brHDP, RTNumerology, NTBridgeHDP, "Bridge Heart's Desire - Personality");
                        addItemSet(brHDPD, RTNumerology, NTBridgeHDPDet, "");

                        lSet.Numerology.insert(a+1,brLPE);
                        lSet.Numerology.insert(a+2,brLPED);
                        lSet.Numerology.insert(a+3,brHDP);
                        lSet.Numerology.insert(a+4,brHDPD);
                        break;
                    }
                }
            }
        }

        setList->append(lSet);
    }
}
//----------------------------------------------------------
void CLayoutManagerDlg::loadSet(QDataStream *stream, QList<repItem> *rep, quint32 ver)
{
    quint32 uint32;
    *stream >> uint32;
    for (quint32 i = 0; i < uint32; i++)
    {
        repItem item;

        *stream >> item.id;
        *stream >> item.name;
        *stream >> item.type;
        *stream >> item.headerFont;
        *stream >> item.contentFont;
        *stream >> item.headerColor;
        *stream >> item.contentColor;
        *stream >> item.isContentDefault;
        *stream >> item.content;
        *stream >> item.imgList;
        *stream >> item.isDefList;
        *stream >> item.headerName;
        if (ver>=VER_FILE_REP_2)
            *stream >> item.extendedSettings;

        rep->append(item);
    }
}
//----------------------------------------------------------
void CLayoutManagerDlg::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    painter.setPen(QColor(237, 219, 178));

    {
        int w1 = 180, sy=55, h=20;
        painter.drawText(QRectF(20, 60-sy, w1-20, h), Qt::AlignHCenter | Qt::AlignVCenter, "Field order");
        painter.drawText(QRectF(205, 60-sy, w1-20, h), Qt::AlignHCenter | Qt::AlignVCenter, "Available fields");
    }

    int lx=400, ly=5+35, sy=46, h=20;

    painter.drawText(QRectF(lx, ly+sy*0-35, 150, h), Qt::AlignLeft | Qt::AlignVCenter, "Report Name");
    painter.drawText(QRectF(lx, ly+sy*2 - 35, 150, h), Qt::AlignLeft | Qt::AlignVCenter, "Header");
    painter.drawText(QRectF(lx, (ly+sy*5) - 36, 150, h), Qt::AlignLeft | Qt::AlignVCenter, "Content");
    painter.drawText(QRectF(lx, (ly+sy*7) - 10, 150, h), Qt::AlignLeft | Qt::AlignVCenter, "Numerologist");
}

//----------------------------------------------------------
void CLayoutManagerDlg::resizeEvent(QResizeEvent *)
{

}
//----------------------------------------------------------
void CLayoutManagerDlg::OnNewSetting()
{
    OnAddSetting(INSNew);
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnAddSetting(INSERTType valT)
{
    QString winTitle = "New setting";
    QString text = "Enter new setting name";

    CInfoDlgWin *dlg = new CInfoDlgWin(appstyle, this, 1);
    dlg->setWindowTitle(winTitle);
    dlg->mainWidget->winText->setText(text);

    dlg->mainWidget->winText->move(dlg->mainWidget->winText->x(), 50);
    dlg->mainWidget->winText->resize(380, 40);
    QLineEdit *edit = new QLineEdit(dlg->mainWidget);
    edit->move(10, 90);
    edit->resize(380, 22);
    dlg->mainWidget->appstyle->setWidgetStyle(edit, WTEdit);

    while (true)
    {
        dlg->exec();
        if (dlg->mainWidget->res == 0)
        {
            if ((edit->text().isEmpty() || edit->text().isNull()))
            {
                CInfoDlgWin::showInfoMessageBox(appstyle, "Error", "Wrong name!", this);
            }
            else
            if (edit->text().contains("/") || edit->text().contains("?") || edit->text().contains(">") || edit->text().contains("<") || edit->text().contains("\"") || edit->text().contains(":") || edit->text().contains("*") || edit->text().contains("|") || edit->text().contains("\\") || edit->text().contains("default", Qt::CaseInsensitive))
            {
                CInfoDlgWin::showInfoMessageBox(appstyle, "Error", "The characters \"/, ?, |, >, <, \", :, \\, *\" and \"default\" are forbidden!", this);
            }
            else
            {
                bool check = false;
                for (int i = 0; i < settings.size(); i++)
                {
                    if (edit->text().compare(settings.at(i).name, Qt::CaseInsensitive) == 0)
                    {
                        check = true;
                        break;
                    }
                }

                if (check == true)
                {
                    CInfoDlgWin::showInfoMessageBox(appstyle, "Error", "Name \"" + edit->text() + "\" is already in use.", this);
                }
                else
                {
                    reportSetting nemRep;
                    if (valT == INSCopy)
                    {
						if (settingList->count() <= 0) {
                            initialDefSetting(&nemRep);
						} else
                        if (settingList->currentRow() >= 0 && settingList->currentRow() < settings.size())
                            nemRep = settings.at(settingList->currentRow());
                    }
                    else
                    if (valT == INSNew)
                    {
                        initialDefSetting(&nemRep);
                    }
                    nemRep.name = edit->text();
                    settings.append(nemRep);
                    settingList->addItem(nemRep.name);
                    settingList->setCurrentRow(settingList->count() - 1);
                    break;
                }
            }
        }
        else
        {
            break;
        }
    }
    delete dlg;
}
//----------------------------------------------------------
void CLayoutManagerDlg::addItemSet(repItem &item, int retType, int itemId, QString name)
{
	item.headerName = name;
	item.type = retType;
	item.id = itemId;
}

void CLayoutManagerDlg::addItemSet(QList<repItem> *retset, int retType, int itemId, QString name)
{
    repItem item;
	addItemSet(item, retType, itemId, name);

    retset->append(item);
}
//----------------------------------------------------------
void CLayoutManagerDlg::initialDefSetting(reportSetting *setting)
{
    QList<repItem> *repType = &setting->Numerology;

    addItemSet(repType, RTNumerology, NTHeader, "Numerology Chart for");
    addItemSet(repType, RTNumerology, NTLifePath, "Life Path Number");
    addItemSet(repType, RTNumerology, NTBrithday, "Birthday Number");
    //addItemSet(repType, RTNumerology, NTCompatible, "Compatible Number");
    addItemSet(repType, RTNumerology, NTExpression, "Expression Number");
    addItemSet(repType, RTNumerology, NTMinorExpression, "Minor Expression Number");
    addItemSet(repType, RTNumerology, NTHeartDesire, "Heart Desire Number");
    addItemSet(repType, RTNumerology, NTMinorHeartDesire, "Minor Heart Desire Number");
    addItemSet(repType, RTNumerology, NTCompound, "Compound Number");
    addItemSet(repType, RTNumerology, NTPersonality, "Personality Number");
    addItemSet(repType, RTNumerology, NTMinorPersonality, "Minor Personality Number");
    addItemSet(repType, RTNumerology, NTKarmicDebts, "Karmic Debts Numbers");
    //addItemSet(repType, RTNumerology, NTKarmicDebt, "Karmic Debt Number:");
    addItemSet(repType, RTNumerology, NTMaturity, "Maturity Number");
    addItemSet(repType, RTNumerology, NTRationalThought, "Rational Thought Number");
    addItemSet(repType, RTNumerology, NTKarmicLessons, "Karmic Lessons Number");
    addItemSet(repType, RTNumerology, NTBalanceNumber, "Balance Number");
    addItemSet(repType, RTNumerology, NTHiddenPassion, "Hidden Passion Number");
    addItemSet(repType, RTNumerology, NTSubconsciousConfidence, "Subconscious Confidence Number");
	addItemSet(repType, RTNumerology, NTPlanesExpression, "Planes of Expression");
	addItemSet(repType, RTNumerology, NTPlanesExpressionM, "Planes of Expression Mental Number");
	addItemSet(repType, RTNumerology, NTPlanesExpressionP, "Planes of Expression Physical Number");
	addItemSet(repType, RTNumerology, NTPlanesExpressionE, "Planes of Expression Emotional Number");
	addItemSet(repType, RTNumerology, NTPlanesExpressionI, "Planes of Expression Intuitive Number");
    addItemSet(repType, RTNumerology, NTPinnacles, "Pinnacles Numbers");
    addItemSet(repType, RTNumerology, NTChallenges, "Challenges Numbers");
    addItemSet(repType,RTNumerology, NTBridgeLPE, "Bridge Life Path - Expression");
    addItemSet(repType,RTNumerology, NTBridgeLPEDet, "");
    addItemSet(repType,RTNumerology, NTBridgeHDP, "Bridge Heart's Desire - Personality");
    addItemSet(repType,RTNumerology, NTBridgeHDPDet, "");
    addItemSet(repType, RTNumerology, NTPersonalYear, "Personal Year Number:");
    addItemSet(repType, RTNumerology, NTPersonalMonth, "Personal Month Number:");
    addItemSet(repType, RTNumerology, NTPersonalDay, "Personal Day Number:");
    addItemSet(repType, RTNumerology, NTTransits, "Transits for age");
    addItemSet(repType, RTNumerology, NTEssence, "Essence for age");

    repType = &setting->PsychoMatrix;

    addItemSet(repType, RTPsychoMatrix, PMTHeader, "Numerology Psychomatrix Chart for");
    addItemSet(repType, RTPsychoMatrix, PMTCharacter, "Character (1)");
    addItemSet(repType, RTPsychoMatrix, PMTEnergy, "Energy (2)");
    addItemSet(repType, RTPsychoMatrix, PMTInterest, "Interest (3)");
    addItemSet(repType, RTPsychoMatrix, PMTHealth, "Health (4)");
    addItemSet(repType, RTPsychoMatrix, PMTIntuition, "Intuition (5)");
    addItemSet(repType, RTPsychoMatrix, PMTLabour, "Labour (6)");
    addItemSet(repType, RTPsychoMatrix, PMTLuck, "Luck (7)");
    addItemSet(repType, RTPsychoMatrix, PMTDuty, "Duty (8)");
    addItemSet(repType, RTPsychoMatrix, PMTMemory, "Memory (9)");
    addItemSet(repType, RTPsychoMatrix, PMTPurposeful, "Purposeful (1-4-7)");
    addItemSet(repType, RTPsychoMatrix, PMTFamilyFormation, "Family formation (2-5-8)");
    addItemSet(repType, RTPsychoMatrix, PMTStability, "Stability (3-6-9)");
    addItemSet(repType, RTPsychoMatrix, PMTSelfAppraisal, "Self appraisal (1-2-3)");
    addItemSet(repType, RTPsychoMatrix, PMTFinance, "Finance (4-5-6)");
    addItemSet(repType, RTPsychoMatrix, PMTTalent, "Talent (7-8-9)");
    addItemSet(repType, RTPsychoMatrix, PMTSpirituality, "Spirituality (1-5-9)");
    addItemSet(repType, RTPsychoMatrix, PMTSexualLife, "Sexual life (3-5-7)");

    repType = &setting->LoveCompatibility;

    addItemSet(repType, RTLoveCompatibility, LCTHeader, "Numerology Compatibility Report");
    addItemSet(repType, RTLoveCompatibility, LCTLifePath, "Life Path Compatibility");
    addItemSet(repType, RTLoveCompatibility, LCTBirthday, "Birthday Compatibility");
    addItemSet(repType, RTLoveCompatibility, LCTDetailedBirthday, QString());
    addItemSet(repType, RTLoveCompatibility, LCTName, "Name Compatibility");
    addItemSet(repType, RTLoveCompatibility, LCTPsychoMatrix, "Psycho-matrix");
    addItemSet(repType, RTLoveCompatibility, LCTFamily, "Family");
    addItemSet(repType, RTLoveCompatibility, LCTCharacter, "Character");
    addItemSet(repType, RTLoveCompatibility, LCTEnergy, "Energy");
	addItemSet(repType, RTLoveCompatibility, LCTCarnalInterests, "Carnal Interests");
    addItemSet(repType, RTLoveCompatibility, LCTFinal, "Final");

    repType = &setting->Celebrities;

    addItemSet(repType, RTCelebrities, CTHeader, "Celebrities Report for");
    addItemSet(repType, RTCelebrities, CTLifePath, "Celebrities with same Life Path");
    addItemSet(repType, RTCelebrities, CTBrithday, "Celebrities with same Birth day");
    addItemSet(repType, RTCelebrities, CTExpression, "Celebrities with same Expression number");

    repType = &setting->Pet;

    addItemSet(repType, RTPet, PTHeader, "Pet Numerology Chart");
    addItemSet(repType, RTPet, PTPet, "Pet number");
    addItemSet(repType, RTPet, PTDayForecast, "Day Forecast for");

	setting->extendedSettings["UPDATE1"] = "TRUE";
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnDelSetting()
{
    int curRow = settingList->currentRow();
    if (curRow >= 0 && settings.size() > curRow)
    {
        if (CInfoDlgWin::showAskMessageBox(appstyle, "Question", "Do you want remove \"" + settingList->currentItem()->text() + "\"?", this) != 0)
            return;

        settings.removeAt(curRow);
        settingList->clear();
        for (int i = 0; i < settings.size(); i++)
            settingList->addItem(settings.at(i).name);

        if (curRow > 0)
        {
            settingList->setCurrentRow(curRow - 1);
        }
        else
        {
            for (int i = 0; i < lmcs.at(0)->widg->lmitems.size(); i++)
            {
                delete lmcs.at(0)->widg->lmitems.at(i);
            }
            lmcs.at(0)->widg->lmitems.clear();
            settingList->setCurrentRow(0);
        }

        updateFields();
    }
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnRenameSetting()
{
    QString winTitle = "Rename setting";
    QString text = "Enter new setting name";

    QListWidgetItem *item = NULL;
    item = settingList->currentItem();

    CInfoDlgWin *dlg = new CInfoDlgWin(appstyle, this, 1);
    dlg->setWindowTitle(winTitle);
    dlg->mainWidget->winText->setText(text);

    dlg->mainWidget->winText->move(dlg->mainWidget->winText->x(), 50);
    dlg->mainWidget->winText->resize(380, 40);
    QLineEdit *edit = new QLineEdit(dlg->mainWidget);
    edit->move(10, 90);
    edit->resize(380, 22);
    dlg->mainWidget->appstyle->setWidgetStyle(edit, WTEdit);

    if (item != NULL)
        edit->setText(item->text());

    while (true)
    {
        dlg->exec();
        if (dlg->mainWidget->res == 0)
        {
            if ((edit->text().isEmpty() || edit->text().isNull()))
            {
                CInfoDlgWin::showInfoMessageBox(appstyle, "Error", "Wrong name!", this);
            }
            else
            if (edit->text().contains("/") || edit->text().contains("?") || edit->text().contains(">") || edit->text().contains("<") || edit->text().contains("\"") || edit->text().contains(":") || edit->text().contains("*") || edit->text().contains("|") || edit->text().contains("\\") || edit->text().contains("default", Qt::CaseInsensitive))
            {
                CInfoDlgWin::showInfoMessageBox(appstyle, "Error", "The characters \"/, ?, |, >, <, \", :, \\, *\" and \"default\" are forbidden!", this);
            }
            else
            {
                bool check = false;
                for (int i = 0; i < settings.size(); i++)
                {
                    if (edit->text().compare(settings.at(i).name, Qt::CaseInsensitive) == 0)
                    {
                        check = true;
                        break;
                    }
                }

                if (check == true)
                {
                    CInfoDlgWin::showInfoMessageBox(appstyle, "Error", "Name \"" + edit->text() + "\" is already in use.", this);
                }
                else
                {
                    settingList->currentItem()->setText(edit->text());
                    settings[settingList->currentRow()].name = edit->text();
                    break;
                }
            }
        }
        else
        {
            break;
        }
    }
    delete dlg;

    item = NULL;
    item = settingList->currentItem();
    if (item != NULL)
        curSetting_ = item->text();
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnFolderSetting()
{
    OnOk(true);
    QString path = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + "/VeBest Numerology/";
    QDesktopServices::openUrl(QUrl(QUrl::fromLocalFile(path)));
}
//----------------------------------------------------------
void CLayoutManagerDlg::OnSelectSetting(int)
{
    citem = NULL;
    setToItems(repNameCB->currentIndex(), settingList->currentRow());
}
//----------------------------------------------------------
void CLayoutManagerDlg::getMassText(int type, int id, QString **p)
{
    if (type == RTNumerology)
    {
        if (id == NTLifePath)
            *p = numerLifePath;
        else
        if (id == NTBrithday)
            *p = numerBirthDate;
        else
        if (id == NTExpression)
            *p = numerExpr;
        else
        if (id == NTMinorExpression)
            *p = numerExprM;
        else
        if (id == NTHeartDesire)
            *p = numerHeartDesire;
        else
        if (id == NTMinorHeartDesire)
            *p = numerMinorHeartDesire;
        else
        if (id == NTCompound)
            *p = numerCompoundNumbers;
        else
        if (id == NTPersonality || id == NTMinorPersonality)
            *p = numerPersonality;
        else
        if (id == NTMaturity)
            *p = numerMaturity;
        else
        if (id == NTRationalThought)
            *p = numerRationalThoughts;
        else
        if (id == NTKarmicLessons)
            *p = numerKarmicLessons;
        else
        if (id == NTBalanceNumber)
            *p = numerBalance;
        else
        if (id == NTHiddenPassion)
            *p = numerHiddenPassion;
        else
        if (id == NTSubconsciousConfidence)
            *p = numerSubconsConfid;
        else
        if (id == NTPinnacles)
            *p = numerPinnacles;
        else
        if (id == NTChallenges)
            *p = numerChallenges;
        else
        if (id == NTPersonalYear)
            *p = numerPersYear;
        else
        if (id == NTPersonalMonth || id == NTPersonalDay)
            *p = numerPersDate;
        else
        if (id == NTTransits)
            *p = numerTransit;
        else
        if (id == NTEssence)
            *p = numerEssence;
        else
        if (id == NTKarmicDebts || id == NTKarmicDebt)
            *p = numerKarmDebts;
		else
		if (id == NTPlanesExpression)
			*p = numerPlanesOfExpression;
		else
		if (id == NTPlanesExpressionM)
			*p = numerPlanesOfExpressionM;
		else
		if (id == NTPlanesExpressionP)
			*p = numerPlanesOfExpressionP;
		else
		if (id == NTPlanesExpressionE)
			*p = numerPlanesOfExpressionE;
		else
		if (id == NTPlanesExpressionI)
			*p = numerPlanesOfExpressionI;
        else
        if (id == NTBridgeLPE)
            *p = numerBridgeLPE;
        else
        if (id == NTBridgeLPEDet)
            *p = numerBridgeLPEDet;
        else
        if (id == NTBridgeHDP)
            *p = numerBridgeHDP;
        else
        if (id == NTBridgeHDPDet)
            *p = numerBridgeHDPDet;
        else
        if (id == NTCustomText)
            *p = numerCustomText;

//        else
//        if (id == NTCompatible)
//            *p = numerCompNumbers;
    }
    else
    if (type == RTPsychoMatrix)
    {
        if (id == PMTCharacter)
            *p = numer1;
        else
        if (id == PMTEnergy)
            *p = numer2;
        else
        if (id == PMTInterest)
            *p = numer3;
        else
        if (id == PMTHealth)
            *p = numer4;
        else
        if (id == PMTIntuition)
            *p = numer5;
        else
        if (id == PMTLabour)
            *p = numer6;
        else
        if (id == PMTLuck)
            *p = numer7;
        else
        if (id == PMTDuty)
            *p = numer8;
        else
        if (id == PMTMemory)
            *p = numer9;
        else
        if (id == PMTPurposeful)
            *p = numerPurposeful;
        else
        if (id == PMTFamilyFormation)
            *p = numerFamilyFormation;
        else
        if (id == PMTStability)
            *p = numerStability;
        else
        if (id == PMTSelfAppraisal)
            *p = numerSelfAppraisal;
        else
        if (id == PMTFinance)
            *p = numerFinance;
        else
        if (id == PMTTalent)
            *p = numerTalent;
        else
        if (id == PMTSpirituality)
            *p = numerSpirituality;
        else
        if (id == PMTSexualLife)
            *p = numerSexualLife;
        else
        if (id == PMTCustomText)
            *p = numerCustomText;
    }
    else
    if (type == RTLoveCompatibility)
    {
        if (id == LCTHeader)
            *p = numerDetComp; //??? heading
        else
        if (id == LCTLifePath)
            *p = numerLPComp;
        else
        if (id == LCTBirthday)
            *p = numerSumComp; //??? numerDetComp
        else
        if (id == LCTName)
            *p = numerNameComp;
        else
        if (id == LCTPsychoMatrix)
            *p = NULL;
        else
        if (id == LCTFamily) //???
            *p = numerDetComp;
        else
        if (id == LCTCarnalInterests) //???
            *p = numerDetComp;
        else
        if (id == LCTDetailedBirthday)
            *p = numerDetComp;
        else
        if (id == LCTFinal)
            *p = numerDetComp;
        else
        if (id == LCTCharacter)
            *p = numerPMCharacterComp;
        else
        if (id == LCTEnergy)
            *p = numerPMEnergyComp;
        else
        if (id == LCTCustomText)
            *p = numerCustomText;

    }
    else
    if (type == RTCelebrities)
    {
        if (id == CTLifePath)
            *p = numerCelLP;
        else
        if (id == CTBrithday)
            *p = numerCelBD;
        else
        if (id == CTExpression)
            *p = numerCelExpr;
        else
        if (id == CTCustomText)
            *p = numerCustomText;
    }
    else
    if (type == RTPet)
    {
        if (id == PTPet)
            *p = numerPetC;
        else
        if (id == PTDayForecast)
            *p = numerPetDForecast;
        else
        if (id == PTCustomText)
            *p = numerCustomText;
    }
}
//----------------------------------------------------------
//-------------                               --------------
//----------------------------------------------------------
CLayoutManagerDlgWin::CLayoutManagerDlgWin(CAppStyle *style, QString selSetting, QWidget *parent)
: QDialog(parent)
{
    mainWidget = new CLayoutManagerDlg(style, selSetting, this);
}
//----------------------------------------------------------
CLayoutManagerDlgWin::~CLayoutManagerDlgWin()
{}
//----------------------------------------------------------
