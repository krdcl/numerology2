#ifndef CHARTNUMEROLOGY_H
#define CHARTNUMEROLOGY_H

#include <QtGui>
#include "numer.h"
#include "numerpmatr.h"
#include "global.h"

class CChartNumerology : public QObject
{
	Q_OBJECT

private:
    struct palitra
    {
        QColor pen1;
        QColor pen2;
        QColor pen3;
        QColor pen4;
        QColor pen5;
		QColor pen6;
    };

public:
	int m_BaseNums[64];

	QImage numChart;

	QString resPath;

    QImage texture;


	static int downOnce(int num);
	int downToBase(int num);
	static int downToDigit(int num);
	static int statDownToBase(int num);

	static bool isBase(int num);
	static bool isCarmic(int num);


public:
	CChartNumerology(QObject *parent);
	~CChartNumerology();

    void preloadRes(QImage texture_);

	void clear();
	void resetData();

};

#endif // CHARTNUMEROLOGY_H
