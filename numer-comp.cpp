#include "numer-comp.h"

QString numerSumComp[128];
QString numerDetComp[128];
QString numerNameComp[128];
QString numerLPComp[128];
QString numerPMCharacterComp[128];
QString numerPMEnergyComp[128];


CCompNumerology::CCompNumerology(QObject *parent)
: CReportTools(parent)
{
	resetData();
}


CCompNumerology::~CCompNumerology()
{
}


void CCompNumerology::resetData()
{
	clear();
}


void CCompNumerology::clear()
{
	for (int a=0; a<64; a++)
		m_BaseNums[a]=0;

	bsm=0; bsf=0;
	dsm=0; dsf=0;
	bss=0; dss=0; oss=0;

	m_TextDoc.clear();
}


bool CCompNumerology::isBase(int num)
{
	if ( ((num>=0) && (num<=9)) || (num==11) || (num==22) || (num==33))
		return true;
	else
		return false;
}


bool CCompNumerology::isCarmic(int num)
{
	if ((num==13) || (num==14) || (num==16) || (num==19))
		return true;
	else
		return false;
}


int CCompNumerology::downOnce(int num)
{
	if (isBase(num))
		return num;

	int res = num % 10 + ((num/10) % 10) + ((num/100) % 10) + ((num/1000) % 10);
	return res;
}


int CCompNumerology::downToBase(int num)
{
	int rnum = num;

	if (isCarmic(rnum) || isBase(rnum))
		m_BaseNums[rnum]++;

	while(!isBase(rnum))
	{
		rnum = downOnce(rnum);
		if (isCarmic(rnum) || isBase(rnum))
			m_BaseNums[rnum]++;
	}
	return rnum;
}


int CCompNumerology::statDownToBase(int num)
{
	int rnum = num;

	while(!isBase(rnum))
	{
		rnum = downOnce(rnum);
		if (rnum<=0)
			return 0;
	}
	return rnum;
}


int CCompNumerology::downToDigit(int num)
{
	int rnum = num;

	while(!isBase(rnum))
	{
		rnum = downOnce(rnum);
		if (rnum<=0)
			return 0;
	}

	if (rnum>9)
		rnum = rnum % 10 + ((rnum/10) % 10);

	return rnum;
}


bool CCompNumerology::calculateDynamic(RepSettings *repList, int curRep, QString str, QString str2, CNumerology *numer, CNumerology *pnumer, CNumerologyPMatrix *matr, CNumerologyPMatrix *pmatr,
                                       int advlpcomp, int advbcomp, int bcomp, int advecomp, bool trial)
{
    const QList<repItem> *LoveCompatibility = &repList->at(curRep).LoveCompatibility;
    startDoc("Numerology Compatibility Report");

    bool showImages=true;
    if (repList->at(curRep).extendedSettings.value("RTLoveCompatibility-hide-images", "FALSE")=="TRUE")
        showImages=false;


    for (int i = 0; i < LoveCompatibility->size(); i++)
    {
        const repItem *item = &LoveCompatibility->at(i);

        if (trial && (item->id != LCTHeader))
        {
            continue;
        }

        if (item->id == LCTHeader)
        {
            if (item->isContentDefault)
            {
                startChap();
                addHead("Numerology Compatibility Report");
                endChap();

                addPar(str);
                if (!trial)
                {
                    startChap();
                    addPar(numerDetComp[0]);
                    endChap();
                }
            }
            else
            {
                startChap();
                addHead(item->headerName, item->headerColor, item->headerFont);
                endChap();

                addPar(str, item->contentColor, item->contentFont);
                if (!trial)
                {
                    startChap();
                    addPar(numerDetComp[0], item, 0);
					endChap();
                }
            }

            if (trial)
            {
                startChap();
#ifndef VER_FREE
                addHead("Upgrade Numerology Calculator to get readings and explanation for your compatibility chart!");
#endif
                endChap();

                break;
            }
        }
        else
        if (item->id == LCTLifePath)
        {
            QString str = "Life path compatibility = " + numDet.LPScore;
            str += "<br/>\r\n";
            str += "Life path - " + numer->numDet.lifePath + " / " + pnumer->numDet.lifePath;
            str += "<br/>\r\n";
            str += "Life path compatibility code - " + QString::number(numLPCode);
            str += "<br/><br/>\r\n\r\n";
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res35.jpg");
            addStandItem("Life Path Compatibility", QString(), str, numerLPComp[advlpcomp], advlpcomp, item, !showImages);
            //endChap();
        }
        else
        if (item->id == LCTBirthday)
        {
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res37.jpg");
            //addHead("Birthday Compatibility");
            QString str = "Birthday compatibility = " + numDet.BScore;
            str += "<br/>\r\n";
            str += "Birthday - " + numer->numDet.birthDay + " / " + pnumer->numDet.birthDay;
            str += "<br/>\r\n";
            str += "Birthday code - " + QString::number(bcomp);
            str += "<br/><br/>\r\n\r\n";

            addStandItem("Birthday Compatibility", QString(), str, numerSumComp[bcomp], bcomp, item, !showImages);
            //addPar(str);
            //addPar(numerSumComp[bcomp]);

            //addPar(numerDetComp[advbcomp]);
            //endChap();
        }
        else
        if (item->id == LCTDetailedBirthday)
        {
            //startChap();
            addStandItem(QString(), QString(), QString(), numerDetComp[advbcomp], advbcomp, item, !showImages);
            //endChap();
        }
        else
        if (item->id == LCTName)
        {
            QString str = "Name compatibility = " + numDet.nameScore;
            str += "<br/>\r\n";
            str += "Name numbers (expression) - " + numer->numDet.expression + " / " + pnumer->numDet.expression;
            str += "<br/>\r\n";
            str += "Name compatibility code - " + numDet.nameComp;
            str += "<br/><br/>\r\n\r\n";
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res34.jpg");
            addStandItem("Name Compatibility", QString(), str, numerNameComp[advecomp], advecomp, item, !showImages);
            //endChap();
        }
        else
        if (item->id == LCTPsychoMatrix)
        {
            //startChap();
            addStandItem("Psycho-matrix", QString(), str2, QString(), -1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == LCTFamily)
        {
            QString str = "Family Compatibility = " + numDet.famScore;
            str += "<br/>\r\n";
            str += "Home stability " + numDet.bss;
            str += "<br/>\r\n";
            str += "Spiritual stability " + numDet.dss;
            str += "<br/>\r\n\r\n";
            str += "Marriage stability term " + numDet.oss;
            str += "<br/><br/>\r\n";
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res36.jpg");
            addStandItem("Family", QString(), str, numerDetComp[1], 1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == LCTCarnalInterests)
        {
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res33.jpg");
            QString str = "Compatibility = " + numDet.ciScore;
            str += "<br/>\r\n";
            str += "Values (3-5-7) - " + QString::number(matr->m_SumHV[1]) + " / " + QString::number(pmatr->m_SumHV[1]);
            str += "<br/><br/><br/><br/>\r\n";
            addStandItem("Carnal Interests", QString(), str, numerDetComp[2], 2, item, !showImages);
            //endChap();
        }
        else
        if (item->id == LCTCharacter)
        {
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res16.jpg");
            QString str = "Character Compatibility (1) - " + QString::number(matr->m_ResMatr[1]) + " / " + QString::number(pmatr->m_ResMatr[1]);
            str += "<br/><br/><br/><br/><br/>\r\n";
            addStandItem("Character", QString(), str + numerPMCharacterComp[0], numerPMCharacterComp[pmCharacterComp], pmCharacterComp, item, !showImages);
            //endChap();
        }
        else
        if (item->id == LCTEnergy)
        {
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res17.jpg");
            QString str = "Energy Compatibility (2) - " + QString::number(matr->m_ResMatr[2]) + " / " + QString::number(pmatr->m_ResMatr[2]);
            str += "<br/><br/><br/><br/><br/>\r\n";
            addStandItem("Energy", QString(), str, numerPMEnergyComp[pmEnergyComp], pmEnergyComp, item, !showImages);
            //endChap();
        }
        else
        if (item->id == LCTFinal)
        {
            //startChap();
            addStandItem(QString(), QString(), QString(), numerDetComp[3], 3, item, !showImages);
            //endChap();
        }
        else
        if (item->id == LCTCustomText)
        {
            addStandItem("Custom Text", QString(), numerCustomText[0], QString(), -1, item, !showImages);
        }
    }
    endDoc();

    return true;
}

bool CCompNumerology::calculate(CNumerology *numer, CNumerology *pnumer, CNumerologyPMatrix *matr, CNumerologyPMatrix *pmatr, bool fsex, bool trial, RepSettings *repList, int curRep)
{
    int select = curRep - 1;

    if (select >= 0)
    {
        if (repList->size() > 0)
        {
            if (repList->size() <= select)
                select = -1;
        }
        else
        {
            select = -1;
        }
    }



    const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	Q_UNUSED(fsex);
	int numBDScoreTable[9] = { 80, 75, 70, 50, 60, 95, 70, 65, 50 };
	
							//	1   2   3   4   5   6   7   8   9
	int numNameScoreTable[99] = { 55, 90, 85, 70, 90, 85, 70, 60, 95,
								00, 40, 70, 95, 70, 85, 70, 75, 65,
								00, 00, 65, 65, 60, 95, 50, 50, 70,
	                            00, 00, 00, 70, 75, 70, 60, 70, 90,
	                            00, 00, 00, 00, 95, 70, 70, 60, 75,
	                            00, 00, 00, 00, 00, 60, 55, 75, 70,
	                            00, 00, 00, 00, 00, 00, 55, 50, 70,
	                            00, 00, 00, 00, 00, 00, 00, 60, 75,
	                            00, 00, 00, 00, 00, 00, 00, 00, 60 };

	int lpcompscore[4] = { 90, 75, 50, 35 };
	QStringList lpcomptable;
	lpcomptable << "1, 5, 7"	<<"3, 9"	 <<"8"	 <<"2, 4, 6";
	lpcomptable << "2, 4, 8"	<<"3, 6"	 <<"9"	 <<"1, 5, 7";
	lpcomptable << "3, 6, 9"	<<"1, 2, 5"	 <<""	 <<"4, 7, 8";
	lpcomptable << "2, 4, 8"	<<"6, 7"	 <<""	 <<"1, 3, 5, 9";
	lpcomptable << "1, 5, 7"	<<"3, 9"	 <<"8"	 <<"2, 4, 6";
	lpcomptable << "3, 6, 9"	<<"2, 4, 8"	 <<"" 	 <<"1, 5, 7";
	lpcomptable << "1, 5, 7"	<<"4"		 <<"9"	 <<"2, 3, 6, 8";
	lpcomptable << "2, 4, 8"	<<"6"		 <<"1,5" <<"3, 7, 9";
	lpcomptable << "3, 6, 9"	<<"1, 5"	 <<"2,7" <<"4, 8";


	resetData();

	//////
    numLP1 = downToDigit(numer->m_LifePath);
	numLP2 = downToDigit(pnumer->m_LifePath);
	numLPCode = downToDigit(numLP1+numLP2);

	numLPScore = 50;
	for (int a=0; a<4; a++)
	{
		if (lpcomptable.at((numLP1-1)*4+a).contains(QString::number(numLP2)))
		{
			numLPScore = lpcompscore[a];
			break;
		}
	}

	numDet.LP = QString::number(numer->m_LifePath) + "+" + QString::number(pnumer->m_LifePath) + "=" + QString::number(numLPCode);
	numDet.LPScore = QString::number(numLPScore) + "%";
	int lpmin = numLP1;
	if (numLP2 < lpmin)
		lpmin = numLP2;
	int lpmax = numLP1;
	if (numLP2 > lpmax)
		lpmax = numLP2;
	int advlpcomp = lpmin*10 + lpmax;


	///////
	int bdate1 = downToDigit(numer->m_bdate.day());
	int bdate2 = downToDigit(pnumer->m_bdate.day());
	int bcomp = downToDigit(bdate1+bdate2);
	int bscore = numBDScoreTable[bcomp-1];
	numDet.BCode = QString::number(numer->m_bdate.day()) + "+" + QString::number(pnumer->m_bdate.day()) + "=" + QString::number(bcomp);
	numDet.BScore = QString::number(bscore) + "%";

	int bmin = bdate1;
	if (bdate2 < bmin)
		bmin = bdate2;
	int bmax = bdate1;
	if (bdate2 > bmax)
		bmax = bdate2;
	int advbcomp = bmin*10 + bmax;



	///////
	// http://www.fantasiya.net/publ/numerologija/numerologija_deneg/znachenie_strok_kvadrata_pifagora_sovmestimost_partnerov_po_kvadratu_pifagora/96-1-0-1290
	int h1m = matr->m_SumHV[1], h1f = pmatr->m_SumHV[1]; //3, 5, 7
	int h2m = matr->m_SumHV[0], h2f = pmatr->m_SumHV[0]; //1, 5, 9
	int c1m = matr->m_SumH[0], c1f = pmatr->m_SumH[0]; //1, 2, 3
	int r1m = matr->m_SumV[0], r1f = pmatr->m_SumV[0]; //1-4-7
	int r2m = matr->m_SumV[1], r2f = pmatr->m_SumV[1]; //2-5-8
	int r3m = matr->m_SumV[2], r3f = pmatr->m_SumV[2]; //3-6-9

	r1score = 100 - abs(2 - abs(r1m-r1f))*20;
	if (r1score<0)
		r1score=0;


	//http://www.fantasiya.net/publ/numerologija/numerologija_deneg/raschet_kvadrata_pifagora_v_21_veke/96-1-0-1291
	bsm = h1m * r2m * r3m;
	bsf = h1f * r2f * r3f;

	dsm = h2m * c1m * r1m;
	dsf = h2f * c1f * r1f;

	bss = bsm * bsf;
	dss = dsm * dsf;
	oss = bss + dss;

	bss = (bss+180)/365;
	dss = (dss+180)/365;
	oss = bss + dss;
	if (bss==dss)
		oss = bss;

	numDet.bsm = QString::number(bsm);
	numDet.bsf = QString::number(bsf);
	numDet.dsm = QString::number(dsm);
	numDet.dsf = QString::number(dsf);
	numDet.bss = QString::number(bss);
	numDet.dss = QString::number(dss);
	numDet.oss = QString::number(oss);

	int fscore = oss*111/35;
	if (fscore>100)
		fscore = 100;
	numDet.famScore = QString::number(fscore) + "%";


	int civ1 = matr->m_SumHV[1], civ2 = pmatr->m_SumHV[1];
	if (civ1>5) civ1=5;
	if (civ2>5) civ2=5;
	int civ = abs(civ1-civ2);
	if (civ==0)
		iciScore = 100;
	else if (civ==1)
		iciScore = 90;
	else if (civ==2)
		iciScore = 75;
	else if (civ==3)
		iciScore = 50;
	else if (civ==4)
		iciScore = 40;
	else
		iciScore = 30;
	numDet.ciScore = QString::number(iciScore) + "%";

    /////
    int pmc1 = (matr->m_ResMatr[1]>6) ? 6 : matr->m_ResMatr[1];
    int pmc2 = (pmatr->m_ResMatr[1]>6) ? 6 : pmatr->m_ResMatr[1];
    if (pmc2<pmc1) std::swap(pmc1, pmc2);

    pmCharacterComp = (pmc1+1)*10 + (pmc2+1);

    int pme1 = (matr->m_ResMatr[2]>6) ? 6 : matr->m_ResMatr[2];
    int pme2 = (pmatr->m_ResMatr[2]>6) ? 6 : pmatr->m_ResMatr[2];
    if (pme2<pme1) std::swap(pme1, pme2);

    pmEnergyComp = (pme1+1)*10 + (pme2+1);


	//////
	int expr1 = downToDigit(numer->m_expression);
	int expr2 = downToDigit(pnumer->m_expression);

	int emin = expr1;
	if (expr2 < emin)
		emin = expr2;
	int emax = expr1;
	if (expr2 > emax)
		emax = expr2;
	int advecomp = emin*10 + emax;

	nameC = downToDigit(expr1+expr2);
	nameCScore = numNameScoreTable[(emin-1)*9+emax-1];

	numDet.nameComp = QString::number(numer->m_expression) + "+" + QString::number(pnumer->m_expression) + "=" + QString::number(nameC);
	numDet.nameScore = QString::number(nameCScore) + "%";

	//////
	int sumComp = (nameCScore + numLPScore*2 + bscore + fscore*2 + iciScore)/7;
	numDet.sumComp = QString::number(sumComp) + "%";


	//////////////////////////////////////////////////////////////////////////
	/*
	str += "Overall Compatibility = " + numDet.sumComp;
	str+="<br />\r\n";
	str += "by Life Path " + numDet.LPScore;
	str+="<br />\r\n";
	str += "by Birthday " + numDet.BScore;
	str+="<br />\r\n";
	str += "by Family " + numDet.famScore;
	str+="<br />\r\n";
	str += "by Name " + numDet.nameScore;
	str+="<br /><br />\r\n";

	str += "Psycho-matrix Compatibility details";
	str+="<br />\r\n";
	str += "Years together " + numDet.oss;
	str+="<br />\r\n";
	str += "Home " + numDet.bsm;
	str+="<br />\r\n";
	str += "Spiritual " + numDet.dsm;
	str+="<br />\r\n";
	str += "Partner home " + numDet.bsf;
	str+="<br />\r\n";
	str += "Partner spiritual " + numDet.dsf;
	str+="<br />\r\n";
	str += "Home stability " + numDet.bss;
	str+="<br />\r\n";
	str += "Spiritual stability " + numDet.dss;
	str+="<br /><br />\r\n\r\n";


	str += "Psycho-matrix values";
	str+="<br />\r\n";
	str += "Character (1) - " + QString::number(matr->m_ResMatr[1]) + " / " + QString::number(pmatr->m_ResMatr[1]);
	str+="<br />\r\n";
	str += "Energy (2) - " + QString::number(matr->m_ResMatr[2]) + " / " + QString::number(pmatr->m_ResMatr[2]);
	str+="<br />\r\n";
	str += "Interest (3) - " + QString::number(matr->m_ResMatr[3]) + " / " + QString::number(pmatr->m_ResMatr[3]);
	str+="<br />\r\n";
	str += "Health (4) - " + QString::number(matr->m_ResMatr[4]) + " / " + QString::number(pmatr->m_ResMatr[4]);
	str+="<br />\r\n";
	str += "Intuition (5) - " + QString::number(matr->m_ResMatr[5]) + " / " + QString::number(pmatr->m_ResMatr[5]);
	str+="<br />\r\n";
	str += "Labour (6) - " + QString::number(matr->m_ResMatr[6]) + " / " + QString::number(pmatr->m_ResMatr[6]);
	str+="<br />\r\n";
	str += "Luck (7) - " + QString::number(matr->m_ResMatr[7]) + " / " + QString::number(pmatr->m_ResMatr[7]);
	str+="<br />\r\n";
	str += "Duty (8) - " + QString::number(matr->m_ResMatr[8]) + " / " + QString::number(pmatr->m_ResMatr[8]);
	str+="<br />\r\n";
	str += "Memory (9) - " + QString::number(matr->m_ResMatr[9]) + " / " + QString::number(pmatr->m_ResMatr[9]);
	str+="<br />\r\n";
	str += "Purposeful (1-4-7) - " + QString::number(matr->m_SumV[0]) + " / " + QString::number(pmatr->m_SumV[0]);
	str+="<br />\r\n";
	str += "Family formation (2-5-8) - " + QString::number(matr->m_SumV[1]) + " / " + QString::number(pmatr->m_SumV[1]);
	str+="<br />\r\n";
	str += "Stability (3-6-9) - " + QString::number(matr->m_SumV[2]) + " / " + QString::number(pmatr->m_SumV[2]);
	str+="<br />\r\n";
	str += "Self appraisal (1-2-3) - " + QString::number(matr->m_SumH[0]) + " / " + QString::number(pmatr->m_SumH[0]);
	str+="<br />\r\n";
	str += "Finance (4-5-6) - " + QString::number(matr->m_SumH[1]) + " / " + QString::number(pmatr->m_SumH[1]);
	str+="<br />\r\n";
	str += "Talent (7-8-9) - " + QString::number(matr->m_SumH[2]) + " / " + QString::number(pmatr->m_SumH[2]);
	str+="<br />\r\n";
	str += "Spirituality (1-5-9) - " + QString::number(matr->m_SumHV[0]) + " / " + QString::number(pmatr->m_SumHV[0]);
	str+="<br />\r\n";
	str += "Sexual life (3-5-7) - " + QString::number(matr->m_SumHV[1]) + " / " + QString::number(pmatr->m_SumHV[1]);
	str+="<br /><br />\r\n\r\n";


	str += "Life path compatibility = " + numDet.LPScore;
	str+="<br />\r\n";
	str += "Life path - " + numer->numDet.lifePath + " / " + pnumer->numDet.lifePath;
	str+="<br />\r\n";
	str += "Life path compatibility code - " + QString::number(numLPCode);
	str+="<br /><br />\r\n\r\n";

	str += "Birthday compatibility = " + numDet.BScore;
	str+="<br />\r\n";
	str += "Birthday - " + numer->numDet.birthDay + " / " + pnumer->numDet.birthDay;
	str+="<br />\r\n";
	str += "Birthday code - " + QString::number(bcomp);
	str+="<br /><br />\r\n\r\n";

	str += "Name compatibility = " + numDet.nameScore;
	str+="<br />\r\n";
	str += "Name numbers (expression) - " + numer->numDet.expression + " / " + pnumer->numDet.expression;
	str+="<br />\r\n";
	str += "Name compatibility code - " + numDet.nameComp;
	str+="<br /><br />\r\n\r\n";*/

	QString tdstyle = "<td style=\"padding: 5px 20px 5px 20px; font: large bold; color:#6f2a0b;\">";
	QString tdstyle1 = "<td style=\"padding: 5px 20px 5px 20px;\">";
	QString tdstylec2 = "<td colspan=\"2\" style=\"padding: 15px 20px 5px 20px; font-weight: bold; color: #117711;\">";
	QString str = "";
    str += "<table class=maindoc width=\"500\" style=\"border-style: solid; color:#000000; border: 0px; border-collapse:collapse;\">\r\n";
    str += "<tr class=\"tab2\" style = \"border-bottom: 2px solid #117711\">" + tdstyle;
	str += numer->fullName + ", "
		+ QString(mname[numer->m_bdate.month()-1]) + " " + QString::number(numer->m_bdate.day()) + " " + QString::number(numer->m_bdate.year());
	str += "<br />and<br />";
	str += pnumer->fullName + ", "
		+ QString(mname[pnumer->m_bdate.month()-1]) + " " + QString::number(pnumer->m_bdate.day()) + " " + QString::number(pnumer->m_bdate.year());
    str += "</td><td style=\"color:#6f2a0b; font: large bold;\">" + numDet.sumComp + "</td></tr>";


    str += "<tr class=\"tab2\" style = \"border-bottom: 2px solid #117711\">" + tdstylec2;
	str += "Core numbers Compatibility";
	str+="</td></tr>";

    str += "<tr class=\"tab1\" style = \"border-bottom: 1px solid  #aaaaaa\">"+tdstyle1;
	str += "Life path";
	str+="</td><td>" + numDet.LPScore + "</td></tr>";

    str += "<tr class=\"tab1\" style = \"border-bottom: 1px solid  #aaaaaa\">"+tdstyle1;
	str += "Birthday";
	str+="</td><td>" + numDet.BScore + "</td></tr>";

    str += "<tr class=\"tab1\" style = \"border-bottom: 1px solid  #aaaaaa\">"+tdstyle1;
	str += "Name";
	str+="</td><td>" + numDet.nameScore + "</td></tr>";


    str += "<tr class=\"tab2\" style = \"border-bottom: 2px solid #117711\">" + tdstylec2;
	str += "Psycho-matrix Compatibility";
	str+="</td></tr>";

    str += "<tr class=\"tab1\" style = \"border-bottom: 1px solid  #aaaaaa\"> "+tdstyle1;
	str += "Marriage stability term";
	str+="</td><td>" + numDet.oss + " years</td></tr>";

    str += "<tr class=\"tab1\" style = \"border-bottom: 1px solid  #aaaaaa\">"+tdstyle1;
	str += "Family";
	str+="</td><td>" + numDet.famScore + "</td></tr>";

    str += "<tr class=\"tab1\" style = \"border-bottom: 1px solid  #aaaaaa\">"+tdstyle1;
	str += "Carnal interests";
	str+="</td><td>" + numDet.ciScore + "</td></tr>";


	str+="</table>\r\n";

    QString str2 = "Character (1) - " + QString::number(matr->m_ResMatr[1]) + " / " + QString::number(pmatr->m_ResMatr[1]);
    str2+="<br/>\r\n";
    str2 += "Energy (2) - " + QString::number(matr->m_ResMatr[2]) + " / " + QString::number(pmatr->m_ResMatr[2]);
    str2+="<br/>\r\n";
    str2 += "Interest (3) - " + QString::number(matr->m_ResMatr[3]) + " / " + QString::number(pmatr->m_ResMatr[3]);
    str2+="<br/>\r\n";
    str2 += "Health (4) - " + QString::number(matr->m_ResMatr[4]) + " / " + QString::number(pmatr->m_ResMatr[4]);
    str2+="<br/>\r\n";
    str2 += "Intuition (5) - " + QString::number(matr->m_ResMatr[5]) + " / " + QString::number(pmatr->m_ResMatr[5]);
    str2+="<br/>\r\n";
    str2 += "Labour (6) - " + QString::number(matr->m_ResMatr[6]) + " / " + QString::number(pmatr->m_ResMatr[6]);
    str2+="<br/>\r\n";
    str2 += "Luck (7) - " + QString::number(matr->m_ResMatr[7]) + " / " + QString::number(pmatr->m_ResMatr[7]);
    str2+="<br/>\r\n";
    str2 += "Duty (8) - " + QString::number(matr->m_ResMatr[8]) + " / " + QString::number(pmatr->m_ResMatr[8]);
    str2+="<br/>\r\n";
    str2 += "Memory (9) - " + QString::number(matr->m_ResMatr[9]) + " / " + QString::number(pmatr->m_ResMatr[9]);
    str2+="<br/>\r\n";
    str2 += "Purposeful (1-4-7) - " + QString::number(matr->m_SumV[0]) + " / " + QString::number(pmatr->m_SumV[0]);
    str2+="<br/>\r\n";
    str2 += "Family formation (2-5-8) - " + QString::number(matr->m_SumV[1]) + " / " + QString::number(pmatr->m_SumV[1]);
    str2+="<br/>\r\n";
    str2 += "Stability (3-6-9) - " + QString::number(matr->m_SumV[2]) + " / " + QString::number(pmatr->m_SumV[2]);
    str2+="<br/>\r\n";
    str2 += "Self appraisal (1-2-3) - " + QString::number(matr->m_SumH[0]) + " / " + QString::number(pmatr->m_SumH[0]);
    str2+="<br/>\r\n";
    str2 += "Finance (4-5-6) - " + QString::number(matr->m_SumH[1]) + " / " + QString::number(pmatr->m_SumH[1]);
    str2+="<br/>\r\n";
    str2 += "Talent (7-8-9) - " + QString::number(matr->m_SumH[2]) + " / " + QString::number(pmatr->m_SumH[2]);
    str2+="<br/>\r\n";
    str2 += "Spirituality (1-5-9) - " + QString::number(matr->m_SumHV[0]) + " / " + QString::number(pmatr->m_SumHV[0]);
    str2+="<br/>\r\n";
    str2 += "Sexual life (3-5-7) - " + QString::number(matr->m_SumHV[1]) + " / " + QString::number(pmatr->m_SumHV[1]);



    startDoc("Numerology Compatibility Report");


    if (select >= 0)
        return calculateDynamic(repList, select, str, str2, numer, pnumer, matr, pmatr, advlpcomp, advbcomp, bcomp, advecomp, trial);

	startChap();
	addHead("Numerology Compatibility Report");
	addPar("");
	endChap();
	
	htmlDoc += str;
	startChap();
	addPar("");
	endChap();

	if (trial)
	{
		startChap();
#ifndef VER_FREE
        addHead("Upgrade Numerology Calculator to get readings and explanation for your compatibility chart!");
#endif
		endChap();
    }
    else
	{
		startChap();
		addPar(numerDetComp[0]);
		endChap();

		// Details
		startChap();
		addImage("res35.jpg");
		addHead("Life Path Compatibility");
		str = "Life path compatibility = " + numDet.LPScore;
		str+="<br />\r\n";
		str += "Life path - " + numer->numDet.lifePath + " / " + pnumer->numDet.lifePath;
		str+="<br />\r\n";
		str += "Life path compatibility code - " + QString::number(numLPCode);
		str+="<br /><br />\r\n\r\n";
		addPar(str);
		addPar(numerLPComp[advlpcomp]);
		endChap();

		startChap();
        addImage("res37.jpg");
		addHead("Birthday Compatibility");
		str = "Birthday compatibility = " + numDet.BScore;
		str+="<br />\r\n";
		str += "Birthday - " + numer->numDet.birthDay + " / " + pnumer->numDet.birthDay;
		str+="<br />\r\n";
		str += "Birthday code - " + QString::number(bcomp);
		str+="<br /><br />\r\n\r\n";
		addPar(str);
		addPar(numerSumComp[bcomp]);
		addPar(numerDetComp[advbcomp]);
		endChap();

		startChap();
        addImage("res34.jpg");
		addHead("Name Compatibility");
		str = "Name compatibility = " + numDet.nameScore;
		str+="<br />\r\n";
		str += "Name numbers (expression) - " + numer->numDet.expression + " / " + pnumer->numDet.expression;
		str+="<br />\r\n";
		str += "Name compatibility code - " + numDet.nameComp;
		str+="<br /><br />\r\n\r\n";
		addPar(str);
		addPar(numerNameComp[advecomp]);
		endChap();

		startChap();
		addHead("Psycho-matrix");
        addPar(str2);
		endChap();

		startChap();
        addImage("res36.jpg");
		addHead("Family");
		str = "Family Compatibility = " + numDet.famScore;
		str+="<br />\r\n";
		str += "Home stability " + numDet.bss;
		str+="<br />\r\n";
		str += "Spiritual stability " + numDet.dss;
		str+="<br />\r\n\r\n";
		str += "Marriage stability term " + numDet.oss;
		str+="<br /><br />\r\n";
		addPar(str);
		addPar(numerDetComp[1]);
		endChap();

        startChap();
        addImage("res38.jpg");
        addHead("Character");
        str = "Character Compatibility (1) - " + QString::number(matr->m_ResMatr[1]) + " / " + QString::number(pmatr->m_ResMatr[1]);
        str += "<br/><br/><br/><br/><br/>\r\n";
        addPar(str);
        addPar(numerPMCharacterComp[0]);
        addPar(numerPMCharacterComp[pmCharacterComp]);
        endChap();

        startChap();
        addImage("res17.jpg");
        addHead("Energy");
        str = "Energy Compatibility (2) - " + QString::number(matr->m_ResMatr[2]) + " / " + QString::number(pmatr->m_ResMatr[2]);
        str += "<br/><br/><br/><br/><br/>\r\n";
        addPar(str);
        addPar(numerPMEnergyComp[pmEnergyComp]);
        endChap();


		startChap();
		addImage("res33.jpg");
		addHead("Carnal Interests");
		str = "Compatibility = " + numDet.ciScore;
		str+="<br />\r\n";
		str += "Values (3-5-7) - " + QString::number(matr->m_SumHV[1]) + " / " + QString::number(pmatr->m_SumHV[1]);
		str+="<br /><br /><br /><br />\r\n";
		addPar(str);
		addPar(numerDetComp[2]);
		endChap();

        startChap();
        addHead("");
        addPar(numerDetComp[3]);
        endChap();
	}

	endDoc();

	return true;
}


