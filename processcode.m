#import "processcode.h"

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <Security/CMSDecoder.h>
#import <Security/SecAsn1Coder.h>
#import <Security/SecAsn1Templates.h>
#import <Security/SecRequirement.h>
#import <IOKit/IOKitLib.h>

@interface ProcessCode ()

@end


static NSString *s1 = @"com.vebest.numerology";
static NSString *s2 = @"7.3.33";




typedef struct {
    size_t          length;
    unsigned char   *data;
} ASN1_Data;

typedef struct {
    ASN1_Data type;     // INTEGER
    ASN1_Data version;  // INTEGER
    ASN1_Data value;    // OCTET STRING
} PCAttribute;

typedef struct {
    PCAttribute **attrs;
} PCPayload;

// ASN.1 receipt attribute template
static const SecAsn1Template kReceiptAttributeTemplate[] = {
    { SEC_ASN1_SEQUENCE, 0, NULL, sizeof(PCAttribute) },
    { SEC_ASN1_INTEGER, offsetof(PCAttribute, type), NULL, 0 },
    { SEC_ASN1_INTEGER, offsetof(PCAttribute, version), NULL, 0 },
    { SEC_ASN1_OCTET_STRING, offsetof(PCAttribute, value), NULL, 0 },
    { 0, 0, NULL, 0 }
};

// ASN.1 receipt template set
static const SecAsn1Template kSetOfReceiptAttributeTemplate[] = {
    { SEC_ASN1_SET_OF, 0, kReceiptAttributeTemplate, sizeof(PCPayload) },
    { 0, 0, NULL, 0 }
};


enum {
    kPCAttributeTypeBundleID                = 2,
    kPCAttributeTypeApplicationVersion      = 3,
    kPCAttributeTypeOpaqueValue             = 4,
    kPCAttributeTypeSHA1Hash                = 5,
    kPCAttributeTypeInAppPurchaseReceipt    = 17,
    
    kPCAttributeTypeInAppQuantity               = 1701,
    kPCAttributeTypeInAppProductID              = 1702,
    kPCAttributeTypeInAppTransactionID          = 1703,
    kPCAttributeTypeInAppPurchaseDate           = 1704,
    kPCAttributeTypeInAppOriginalTransactionID  = 1705,
    kPCAttributeTypeInAppOriginalPurchaseDate   = 1706,
};


static NSString *kPCInfoKeyBundleID                     = @"Bundle ID";
static NSString *kPCInfoKeyBundleIDData                 = @"Bundle ID Data";
static NSString *kPCInfoKeyApplicationVersion           = @"Application Version";
static NSString *kPCInfoKeyApplicationVersionData       = @"Application Version Data";
static NSString *kPCInfoKeyOpaqueValue                  = @"Opaque Value";
static NSString *kPCInfoKeySHA1Hash                     = @"SHA-1 Hash";
static NSString *kPCInfoKeyInAppPurchaseReceipt         = @"In App Purchase Receipt";

static NSString *kPCInfoKeyInAppProductID               = @"In App Product ID";
static NSString *kPCInfoKeyInAppTransactionID           = @"In App Transaction ID";
static NSString *kPCInfoKeyInAppOriginalTransactionID   = @"In App Original Transaction ID";
static NSString *kPCInfoKeyInAppPurchaseDate            = @"In App Purchase Date";
static NSString *kPCInfoKeyInAppOriginalPurchaseDate    = @"In App Original Purchase Date";
static NSString *kPCInfoKeyInAppQuantity                = @"In App Quantity";





inline static NSData *PCASN1RawData(ASN1_Data asn1Data)
{
    return [NSData dataWithBytes:asn1Data.data length:asn1Data.length];
}

inline static int PCIntValueFromASN1Data(const ASN1_Data *asn1Data)
{
    int ret = 0;
    for (int i = 0; i < asn1Data->length; i++) {
        ret = (ret << 8) | asn1Data->data[i];
    }
    return ret;
}

inline static NSNumber *PCDecodeIntNumberFromASN1Data(SecAsn1CoderRef decoder, ASN1_Data srcData)
{
    ASN1_Data asn1Data;
    OSStatus status = SecAsn1Decode(decoder, srcData.data, srcData.length, kSecAsn1IntegerTemplate, &asn1Data);
    if (status) {
        [NSException raise:@"Validation Error" format:@"Failed to get receipt information: Decode integer value", nil];
    }
    return [NSNumber numberWithInt:PCIntValueFromASN1Data(&asn1Data)];
}

inline static NSString *PCDecodeUTF8StringFromASN1Data(SecAsn1CoderRef decoder, ASN1_Data srcData)
{
    ASN1_Data asn1Data;
    OSStatus status = SecAsn1Decode(decoder, srcData.data, srcData.length, kSecAsn1UTF8StringTemplate, &asn1Data);
    if (status) {
        [NSException raise:@"Validation Error" format:@"Failed to get receipt information: Decode UTF-8 string", nil];
    }
    return [[NSString alloc] initWithBytes:asn1Data.data length:asn1Data.length encoding:NSUTF8StringEncoding];
}

inline static NSDate *PCDecodeDateFromASN1Data(SecAsn1CoderRef decoder, ASN1_Data srcData)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-ddTHH:mm:ssZ"];
    
    ASN1_Data asn1Data;
    OSStatus status = SecAsn1Decode(decoder, srcData.data, srcData.length, kSecAsn1IA5StringTemplate, &asn1Data);
    if (status) {
        [NSException raise:@"Validation Error" format:@"Failed to get receipt information: Decode date (IA5 string)", nil];
    }
    
    NSString *dateStr = [[NSString alloc] initWithBytes:asn1Data.data length:asn1Data.length encoding:NSASCIIStringEncoding];
    return [dateFormatter dateFromString:dateStr];
}

inline static NSDictionary *PCReceiptPayload(NSData *payloadData)
{
    SecAsn1CoderRef asn1Decoder = NULL;
    
    @try {
        NSMutableDictionary *ret = [NSMutableDictionary dictionary];
        
        // Create the ASN.1 parser
        OSStatus status = SecAsn1CoderCreate(&asn1Decoder);
        if (status) {
            [NSException raise:@"Validation Error" format:@"Failed to get receipt information: Create ASN.1 decoder", nil];
        }
        
        // Decode the receipt payload
        PCPayload payload = { NULL };
        status = SecAsn1Decode(asn1Decoder, payloadData.bytes, payloadData.length, kSetOfReceiptAttributeTemplate, &payload);
        if (status) {
            [NSException raise:@"Validation Error" format:@"Failed to get receipt information: Decode payload", nil];
        }
        
        // Fetch all attributes
        PCAttribute *anAttr;
        for (int i = 0; (anAttr = payload.attrs[i]); i++) {
            int type = PCIntValueFromASN1Data(&anAttr->type);
            switch (type) {
                    // UTF-8 String
                case kPCAttributeTypeBundleID:
                    [ret setValue:PCDecodeUTF8StringFromASN1Data(asn1Decoder, anAttr->value) forKey:kPCInfoKeyBundleID];
                    [ret setValue:PCASN1RawData(anAttr->value) forKey:kPCInfoKeyBundleIDData];
                    break;
                case kPCAttributeTypeApplicationVersion:
                    [ret setValue:PCDecodeUTF8StringFromASN1Data(asn1Decoder, anAttr->value) forKey:kPCInfoKeyApplicationVersion];
                    [ret setValue:PCASN1RawData(anAttr->value) forKey:kPCInfoKeyApplicationVersionData];
                    break;
                case kPCAttributeTypeInAppProductID:
                    [ret setValue:PCDecodeUTF8StringFromASN1Data(asn1Decoder, anAttr->value) forKey:kPCInfoKeyInAppProductID];
                    break;
                case kPCAttributeTypeInAppTransactionID:
                    [ret setValue:PCDecodeUTF8StringFromASN1Data(asn1Decoder, anAttr->value) forKey:kPCInfoKeyInAppTransactionID];
                    break;
                case kPCAttributeTypeInAppOriginalTransactionID:
                    [ret setValue:PCDecodeUTF8StringFromASN1Data(asn1Decoder, anAttr->value) forKey:kPCInfoKeyInAppOriginalTransactionID];
                    break;
                    
                    // Purchase Date (As IA5 String (almost identical to the ASCII String))
                case kPCAttributeTypeInAppPurchaseDate:
                    [ret setValue:PCDecodeDateFromASN1Data(asn1Decoder, anAttr->value) forKey:kPCInfoKeyInAppPurchaseDate];
                    break;
                case kPCAttributeTypeInAppOriginalPurchaseDate:
                    [ret setValue:PCDecodeDateFromASN1Data(asn1Decoder, anAttr->value) forKey:kPCInfoKeyInAppOriginalPurchaseDate];
                    break;
                    
                    // Quantity (Integer Value)
                case kPCAttributeTypeInAppQuantity:
                    [ret setValue:PCDecodeIntNumberFromASN1Data(asn1Decoder, anAttr->value)
                           forKey:kPCInfoKeyInAppQuantity];
                    break;
                    
                    // Opaque Value (Octet Data)
                case kPCAttributeTypeOpaqueValue:
                    [ret setValue:PCASN1RawData(anAttr->value) forKey:kPCInfoKeyOpaqueValue];
                    break;
                    
                    // SHA-1 Hash (Octet Data)
                case kPCAttributeTypeSHA1Hash:
                    [ret setValue:PCASN1RawData(anAttr->value) forKey:kPCInfoKeySHA1Hash];
                    break;
                    
                    // In App Purchases Receipt
                case kPCAttributeTypeInAppPurchaseReceipt: {
                    NSMutableArray *inAppPurchases = [ret valueForKey:kPCInfoKeyInAppPurchaseReceipt];
                    if (!inAppPurchases) {
                        inAppPurchases = [NSMutableArray array];
                        [ret setValue:inAppPurchases forKey:kPCInfoKeyInAppPurchaseReceipt];
                    }
                    NSData *inAppData = [NSData dataWithBytes:anAttr->value.data length:anAttr->value.length];
                    NSDictionary *inAppInfo = PCReceiptPayload(inAppData);
                    [inAppPurchases addObject:inAppInfo];
                    break;
                }
                    
                    // Otherwise
                default:
                    break;
            }
        }
        return ret;
    } @catch (NSException *e) {
        @throw e;
    } @finally {
        if (asn1Decoder) SecAsn1CoderRelease(asn1Decoder);
    }
}

inline static NSData *GetMacAddress(void)
{
    mach_port_t masterPort;
    kern_return_t result = IOMasterPort(MACH_PORT_NULL, &masterPort);
    if (result != KERN_SUCCESS) {
        return nil;
    }
    
    CFMutableDictionaryRef matchingDict = IOBSDNameMatching(masterPort, 0, "en0");
    if (!matchingDict) {
        return nil;
    }
    
    io_iterator_t iterator;
    result = IOServiceGetMatchingServices(masterPort, matchingDict, &iterator);
    if (result != KERN_SUCCESS) {
        return nil;
    }
    
    CFDataRef macAddressDataRef = nil;
    io_object_t aService;
    while ((aService = IOIteratorNext(iterator)) != 0) {
        io_object_t parentService;
        result = IORegistryEntryGetParentEntry(aService, kIOServicePlane, &parentService);
        if (result == KERN_SUCCESS) {
            if (macAddressDataRef) CFRelease(macAddressDataRef);
            macAddressDataRef = (CFDataRef)IORegistryEntryCreateCFProperty(parentService, (CFStringRef)@"IOMACAddress", kCFAllocatorDefault, 0);
            IOObjectRelease(parentService);
        }
        IOObjectRelease(aService);
    }
    IOObjectRelease(iterator);
    
    NSData *ret = nil;
    if (macAddressDataRef) {
        ret = [NSData dataWithData:(__bridge NSData *)macAddressDataRef];
        CFRelease(macAddressDataRef);
    }
    return ret;
}


@implementation ProcessCode

+ (void)load {
#ifndef APPSTORE
    return;
#endif
#ifdef VER_FREE
    return;
#endif

    @try {
        // ID & Version
        NSDictionary *bundleInfo = [[NSBundle mainBundle] infoDictionary];
        NSURL *bundleURL = [[NSBundle mainBundle] bundleURL];
        
        NSString *bundleID = [bundleInfo valueForKey:@"CFBundleIdentifier"];
        if (![bundleID isEqualToString:s1]) {
            [NSException raise:@"MAS Validation Error" format:@"Failed to check bundle ID.", nil];
        }
        
        NSString *bundleVersion = [bundleInfo valueForKey:@"CFBundleShortVersionString"];
        if (![bundleVersion isEqualToString:s2]) {
            [NSException raise:@"MAS Validation Error" format:@"Failed to check bundle Version.", nil];
        }

        
        // Signature
        SecStaticCodeRef staticCode = NULL;
        OSStatus status = SecStaticCodeCreateWithPath((__bridge CFURLRef)bundleURL, kSecCSDefaultFlags, &staticCode);
        if (status != errSecSuccess) {
            [NSException raise:@"Validation Error" format:@"Failed to validate bundle signature: Create a static code", nil];
        }
        
        NSString *requirementText = @"anchor apple generic";   // For code signed by Apple
        
        SecRequirementRef requirement = NULL;
        status = SecRequirementCreateWithString((__bridge CFStringRef)requirementText, kSecCSDefaultFlags, &requirement);
        if (status != errSecSuccess) {
            if (staticCode) CFRelease(staticCode);
            [NSException raise:@"Validation Error" format:@"Failed to validate bundle signature: Create a requirement", nil];
        }
        
        status = SecStaticCodeCheckValidity(staticCode, kSecCSDefaultFlags, requirement);
        if (status != errSecSuccess) {
            if (staticCode) CFRelease(staticCode);
            if (requirement) CFRelease(requirement);
            [NSException raise:@"Validation Error" format:@"Failed to validate bundle signature: Check the static code validity", nil];
        }
        
        if (staticCode) CFRelease(staticCode);
        if (requirement) CFRelease(requirement);
        
        
        // Receipt
        NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
        NSData *rData = [NSData dataWithContentsOfURL:receiptURL];
        if (!rData) {
            [NSException raise:@"Validation Error" format:@"Failed to fetch the MacAppStore receipt.", nil];
        }
        
        NSData *rDataDec = NULL;
        CMSDecoderRef decoder = NULL;
        SecPolicyRef policyRef = NULL;
        SecTrustRef trustRef = NULL;
        
        @try {
            // Create a decoder
            OSStatus status = CMSDecoderCreate(&decoder);
            if (status) {
                [NSException raise:@"Validation Error" format:@"Failed to decode receipt data: Create a decoder", nil];
            }
            
            // Decrypt the message (1)
            status = CMSDecoderUpdateMessage(decoder, rData.bytes, rData.length);
            if (status) {
                [NSException raise:@"Validation Error" format:@"Failed to decode receipt data: Update message", nil];
            }
            
            // Decrypt the message (2)
            status = CMSDecoderFinalizeMessage(decoder);
            if (status) {
                [NSException raise:@"Validation Error" format:@"Failed to decode receipt data: Finalize message", nil];
            }
            
            // Get the decrypted content
            CFDataRef dataRef = NULL;
            status = CMSDecoderCopyContent(decoder, &dataRef);
            if (status) {
                [NSException raise:@"Validation Error" format:@"Failed to decode receipt data: Get decrypted content", nil];
            }
            rDataDec = [NSData dataWithData:(__bridge NSData *)dataRef];
            CFRelease(dataRef);
            
            // Check the signature
            size_t numSigners;
            status = CMSDecoderGetNumSigners(decoder, &numSigners);
            if (status) {
                [NSException raise:@"Validation Error" format:@"Failed to check receipt signature: Get singer count", nil];
            }
            if (numSigners == 0) {
                [NSException raise:@"Validation Error" format:@"Failed to check receipt signature: No signer found", nil];
            }
            
            policyRef = SecPolicyCreateBasicX509();
            
            CMSSignerStatus signerStatus;
            OSStatus certVerifyResult;
            status = CMSDecoderCopySignerStatus(decoder, 0, policyRef, TRUE, &signerStatus, &trustRef, &certVerifyResult);
            if (status) {
                [NSException raise:@"Validation Error" format:@"Failed to check receipt signature: Get signer status", nil];
            }
            if (signerStatus != kCMSSignerValid) {
                [NSException raise:@"Validation Error" format:@"Failed to check receipt signature: No valid signer", nil];
            }
        } @catch (NSException *e) {
            @throw e;
        } @finally {
            if (policyRef) CFRelease(policyRef);
            if (trustRef) CFRelease(trustRef);
            if (decoder) CFRelease(decoder);
        }
        
        NSDictionary *receiptInfo = PCReceiptPayload(rDataDec);
//        NSLog(@"receiptInfo=%@", receiptInfo);
        
        {
            NSString *bundleID = [receiptInfo valueForKey:kPCInfoKeyBundleID];
            if (![bundleID isEqualToString:s1]) {
                [NSException raise:@"Validation Error" format:@"Failed to check receipt ID.", nil];
            }
            
            NSString *bundleVersion = [receiptInfo objectForKey:kPCInfoKeyApplicationVersion];
            if (![bundleVersion isEqualToString:s2]) {
                [NSException raise:@"Validation Error" format:@"Failed to check receipt version.", nil];
            }
        }
        
        NSData *macAddressData = GetMacAddress();
        if (!macAddressData) {
            [NSException raise:@"Validation Error" format:@"Failed to get the primary MAC Address for checking receipt hash.", nil];
        }
        NSData *data1 = [receiptInfo valueForKey:kPCInfoKeyOpaqueValue];
        NSData *data2 = [receiptInfo valueForKey:kPCInfoKeyBundleIDData];
        
        NSMutableData *digestData = [NSMutableData dataWithData:macAddressData];
        [digestData appendData:data1];
        [digestData appendData:data2];
        
        unsigned char digestBuffer[CC_SHA1_DIGEST_LENGTH];
        CC_SHA1(digestData.bytes, (CC_LONG)digestData.length, digestBuffer);
        
        NSData *hashData = [receiptInfo valueForKey:kPCInfoKeySHA1Hash];
        if (memcmp(digestBuffer, hashData.bytes, CC_SHA1_DIGEST_LENGTH) != 0) {
            [NSException raise:@"Validation Error" format:@"Failed to check receipt hash.", nil];
        }
        
        return;
    } @catch (NSException *e) {
        NSLog(@"%@", e.reason);
        exit(173);
    }
}

@end
    
