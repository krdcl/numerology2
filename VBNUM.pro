#CONFIG += APP_MAS

CONFIG += VER_PREM
#CONFIG += VER_STD
#CONFIG += VER_FREE



TEMPLATE = app
DEPENDPATH += .
INCLUDEPATH += .
# CONFIG += release
# CONFIG += debug
#CONFIG += x86
QT += network widgets
QT += webengine gui printsupport
QT += webenginewidgets concurrent


VER_PREM {
DEFINES += VER_PREM
macx:TARGET = "VeBest Numerology Everywhere"
win32:TARGET = "VBNE"
macx:ICON = vbnume.icns
}

VER_STD {
DEFINES += VER_STD
macx:TARGET = "VeBest Numerology"
win32:TARGET = "VBNC"
macx:ICON = vbnum.icns
}

VER_FREE {
DEFINES += VER_FREE
macx:TARGET = "VeBest Numerology Free"
macx:ICON = vbnumf.icns
}


APP_MAS {
DEFINES += APPSTORE
VER_STD:OBJECTIVE_SOURCES += processcode.m
VER_PREM:OBJECTIVE_SOURCES += processcode.m
LIBS += -framework Security
LIBS += -lcrypto
}


macx {
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.8
    QMAKE_CFLAGS += -mmacosx-version-min=10.8
    QMAKE_CXXFLAGS += -mmacosx-version-min=10.8
    QMAKE_LFLAGS += -mmacosx-version-min=10.8

    QMAKE_CFLAGS += -gdwarf-2
    QMAKE_CXXFLAGS += -gdwarf-2

    #MAC_SDK  = /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk
    macx:QMAKE_MAC_SDK = $$MAC_SDK

    LIBS += -framework CoreFoundation
    LIBS += -framework Foundation
    LIBS += -framework AppKit
    LIBS += -framework IOKit
    LIBS += -L"/usr/local/lib"

    APP_MAS {
        QMAKE_CFLAGS_RELEASE = $$QMAKE_CFLAGS_RELEASE_WITH_DEBUGINFO
        QMAKE_CXXFLAGS_RELEASE = $$QMAKE_CXXFLAGS_RELEASE_WITH_DEBUGINFO
        QMAKE_OBJECTIVE_CFLAGS_RELEASE =  $$QMAKE_OBJECTIVE_CFLAGS_RELEASE_WITH_DEBUGINFO
        QMAKE_LFLAGS_RELEASE = $$QMAKE_LFLAGS_RELEASE_WITH_DEBUGINFO
    }
}
win32 {
#Full debug
#QMAKE_CXXFLAGS_RELEASE -= -O2
#QMAKE_CXXFLAGS_RELEASE += /Od

QMAKE_CXXFLAGS_RELEASE += /Zi
QMAKE_LFLAGS_RELEASE += /DEBUG

HEADERS += IntelliProtector.h \
    esellerate.h
SOURCES += IntelliProtector.cpp

VER_PREM:RC_FILE = rese.rc
VER_STD:RC_FILE = res.rc
#LIBS += esellerateLibrary.lib

}

# Input
HEADERS += vbnum.h \
    global.h \
    numwidgets.h \
    inifile.h \
    numer-chart.h \
    numer-comp.h \
    numer.h \
    numer_interp.h \
    numerpmatr.h \
    regform.h \
    ui_cregform.h \
    ui_settingsdlg.h \
    lic.h \
    aboutdlg.h \
    eula.h \
    settingsdlg.h \
    appstyle.h \
    layout_painter/barcode.h \
    layout_painter/iconlist.h \
    layout_painter/papersizedlg.h \
    layout_painter/propertieswidget.h \
    layout_painter/QR_Encode.h \
    layout_painter/reportcreator.h \
    layout_painter/reportpe.h \
    layout_painter/reportpepages.h \
    layout_painter/thumbailsettings.h \
    layout_painter/textdlg.h \
    layout_painter/ccolordialog.h \
    layout_painter/mathutils.h \
    layoutmanagerdlg.h \
    infodlg.h \
    texteditor.h \
    reporttools.h \
    includes.h

SOURCES += vbnum.cpp \
    main.cpp \
    numwidgets.cpp \
    inifile.cpp \
    numer-chart.cpp \
    numer-comp.cpp \
    numer.cpp \
    numerpmatr.cpp \
    regform.cpp \
    lic.cpp \
    aboutdlg.cpp \
    eula.cpp \
    vbnum-files.cpp \
    vbnum-calc.cpp \
    settingsdlg.cpp \
    appstyle.cpp \
    layout_painter/iconlist.cpp \
    layout_painter/papersizedlg.cpp \
    layout_painter/propertieswidget.cpp \
    layout_painter/QR_Encode.cpp \
    layout_painter/reportcreator.cpp \
    layout_painter/reportpe.cpp \
    layout_painter/reportpepages.cpp \
    layout_painter/thumbailsettings.cpp \
    layout_painter/textdlg.cpp \
    layout_painter/ccolordialog.cpp \
    layout_painter/mathutils.cpp \
    layoutmanagerdlg.cpp \
    infodlg.cpp \
    texteditor.cpp \
    reporttools.cpp

