#include "numwidgets.h"
#include "numer.h"

void drawFText(QPainter &painter, int x, int y, QString str, bool skipMNH=false)
{
	if (skipMNH)
	{
		painter.drawStaticText(x, y, "<font color=\"#ece7e0\">" + str + "</font>");
	} else
	{
		QString rstr;
		if (str.size()>1)
		{
			for (int a=0; a<str.size(); a++)
			{
				if ((a<str.size()-1) && str.at(a).isDigit() && (str.at(a+1) == str.at(a)))
				{
					bool repr=false;
					if (str.size()>a+2)
					{
						if (!str.at(a+2).isDigit())
							repr = true;
					} else
						repr = true;

					bool repl=false;
					if (a>0)
					{
						if (!str.at(a-1).isDigit())
							repl=true;
					} else
						repl = true;

					if (repl && repr)
					{
						rstr += QString("<font color=\"#F06040\">") + str.at(a) + str.at(a) + QString("</font>");
						a++;
					} else
						rstr += str.at(a);
				} else
					rstr += str.at(a);
			}

		} else
		{
			str.replace("11", "<font color=\"#F06040\">11</font>");
			str.replace("22", "<font color=\"#F06040\">22</font>");
			str.replace("33", "<font color=\"#F06040\">33</font>");
            str.replace("44", "<font color=\"#F06040\">44</font>");
            str.replace("55", "<font color=\"#F06040\">55</font>");
            str.replace("66", "<font color=\"#F06040\">66</font>");
            str.replace("77", "<font color=\"#F06040\">77</font>");
            str.replace("88", "<font color=\"#F06040\">88</font>");
            str.replace("99", "<font color=\"#F06040\">99</font>");
            rstr = str;
		}
		painter.drawStaticText(x, y, "<font color=\"#ece7e0\">" + rstr + "</font>");
	}
    //QStaticText st(str);
}


CInfoWidget::CInfoWidget(CAppStyle *style, QImage titleBar, QWidget *parent, Qt::WindowFlags flags)
    : QWidget(parent, flags)
{
    appstyle = style;
	m_Res.titleBar = titleBar;

	sizeWith = 0;
	skipMNH = false;
	hideHover = true;
	enableHover = false;
	mouse_x = -1;
	mouse_y = -1;
	maxHStrings = 100;

	setMouseTracking(true);

#ifdef Q_WS_WIN
	QGraphicsDropShadowEffect *titShadow = new QGraphicsDropShadowEffect(this);
    titShadow->setBlurRadius(3);
    titShadow->setColor(QColor(63, 63, 63, 180));
    titShadow->setOffset(1,1);
#endif

    wtitle = new QLabel(this);
#ifdef Q_WS_WIN
    wtitle->setStyleSheet("QLabel { color:black; font-size: 12px; background:none; border:none; }");
#else
    wtitle->setStyleSheet("QLabel { color:black; font: bold 12px; background:none; border:none; }");
#endif
	wtitle->setGeometry(10,1,titleBar.width()-20,titleBar.height()-2);
#ifdef Q_WS_WIN
    wtitle->setGraphicsEffect(titShadow);
#endif
    resize(200, 100);
}

void CInfoWidget::mouseMoveEvent(QMouseEvent *event)
{
	mouse_x = event->x();
	mouse_y = event->y();
	hideHover = false;
	update();
}

void CInfoWidget::leaveEvent(QEvent *)
{
	hideHover = true;
	update();
}

void CInfoWidget::enterEvent(QEvent *)
{
	hideHover = false;
}

void CInfoWidget::mousePressEvent(QMouseEvent *event)
{
	mouse_x = event->x();
	mouse_y = event->y();

    int wpx=0, wpy=0;
    int wsx=this->width() + sizeWith;
    int vstep=16;
    int tposyoff=3;
    int tposy=m_Res.titleBar.height()+tposyoff;

	int a=(mouse_y-wpy-tposy);
	if (a>=0)
		a /= vstep;
	if ((a>=0) && (a<parList.size()) && (mouse_x>wpx) && (mouse_x<wpx+wsx) && (a<maxHStrings))
		emit itemCliked(a);
}


CInfoWidget::~CInfoWidget()
{}


void CInfoWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    //m_Res.titleBar = m_Res.titleBar.scaled(200 + sizeWith, 20, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    //painter.setRenderHint(QPainter::TextAntialiasing, false);

    //int wpx=20, wpy=90;
    //int wsx=180, wsy=100;
    int wpx=0, wpy=0;
    int wsx=this->width() + sizeWith;
    //int wsy=this->height();
//#ifdef Q_WS_MAC
    QFont font2("Arial");
    font2.setPixelSize(12);
    painter.setFont(font2);
    QFont font3("Arial");
    font3.setPixelSize(10);
    QFont font4("Arial");
    font4.setBold(true);
    font4.setPixelSize(12);
/*#else
    QFont font2("Arial", 10);
    painter.setFont(font2);
    QFont font3("Arial", 8);
    QFont font4("Arial", 10);
    font4.setBold(true);
#endif*/
    QPen pen2(appstyle->updateColor(QColor(236, 231, 224, 255)));
    //QPen pen4(QColor(255, 255, 255, 255));
    pen2.setWidth(1);
    painter.setPen(pen2);


    int vstep=16;
    int sepx=100;
    int tposyoff=3;
    int tposy=m_Res.titleBar.height()+tposyoff;

    painter.fillRect(wpx, wpy+m_Res.titleBar.height(), wsx, parList.size()*vstep+tposyoff*2, appstyle->updateColor(QColor::fromRgb(65,53,46,255)));

    if ( (parList.size()!=0) && (parList.size() == parListVal.size()) )
    {
        for (int a=0; a<parList.size(); a++)
        {
			if (enableHover && (!hideHover) && (a<maxHStrings))
			{
				if ((mouse_y>=wpy+vstep*a+tposy) && (mouse_y<wpy+vstep*(a+1)+tposy))
					painter.fillRect(wpx+2, wpy+vstep*a+tposy, wsx-2, vstep, appstyle->updateColor(QColor(130,106,92, 255)));
			}
            painter.setFont(font2);
            drawFText(painter, wpx+4, wpy+vstep*a+tposy, parList.at(a), skipMNH );
            //painter.drawText( QRectF(wpx+4, wpy+vstep*a+tposy, sepx-4, vstep), Qt::AlignVCenter | Qt::AlignLeft, parList.at(a) );
#ifdef Q_WS_MAC
            if (parListVal.at(a).size() > 15)
            {
                painter.setFont(font3);
                drawFText(painter, wpx+4+sepx, wpy+vstep*a+tposy+3, parListVal.at(a), skipMNH );
            } else
            {
                drawFText(painter, wpx+4+sepx, wpy+vstep*a+tposy, parListVal.at(a), skipMNH );
            }
#else
            if (parListVal.at(a).size() > 12)
            {
                painter.setFont(font3);
                drawFText(painter, wpx+4+sepx, wpy+vstep*a+tposy+3, parListVal.at(a), skipMNH );
            } else
            {
                drawFText(painter, wpx+4+sepx, wpy+vstep*a+tposy, parListVal.at(a), skipMNH );
            }
#endif
            //painter.drawText( QRectF(wpx+4+sepx, wpy+vstep*a+tposy, wsx-8-sepx, vstep), Qt::AlignVCenter | Qt::AlignLeft, parListVal.at(a) );
        }
    }
    QPen pen5(appstyle->updateColor(QColor(93, 84, 78, 255)));
    QPen pen6(appstyle->updateColor(QColor(57, 46, 40, 255)));
	//if (enableHover && (!hideHover))
	//	pen6 = QPen(appstyle->updateColor(QColor(157, 146, 140, 255)));

    painter.setPen(pen5);
    painter.drawRect(wpx, wpy+m_Res.titleBar.height()-1, wsx-1, parList.size()*vstep+tposyoff*2);
	painter.setPen(pen6);
	painter.drawRect(wpx+1, wpy+m_Res.titleBar.height()-1, wsx-3, parList.size()*vstep+tposyoff*2-1);
    painter.drawImage(wpx, wpy, m_Res.titleBar);


    /*painter.setPen(pen4);
    painter.setFont(font4);
    painter.drawText( QRectF(wpx+4, wpy, m_Res.titleBar.width()-8, m_Res.titleBar.height()), Qt::AlignVCenter | Qt::AlignHCenter, this->windowTitle() );*/
}



//////////////////////////////////////////////////////////////////////////
CInfoTodayWidget::CInfoTodayWidget(CAppStyle *style, QImage titleBar, QWidget *parent, Qt::WindowFlags flags)
    : CInfoWidget(style, titleBar, parent, flags)
{
	resize(180, 104);
}

CInfoTodayWidget::~CInfoTodayWidget()
{}


void CInfoTodayWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.drawImage(0, 0, appstyle->updateImage(m_Res.titleBar));

    QFont font1("Arial");
    font1.setPixelSize(32);
	font1.setBold(true);
//#ifdef Q_WS_MAC
    QFont font2("Arial");
    font2.setPixelSize(12);
/*#else
    QFont font2("Arial", 11);
#endif*/
	font2.setBold(true);
    QPen pen2(QColor(237, 219, 178, 255));
    QPen pen1(QColor(156, 182, 45, 255));

	if (parListVal.size()>=3)
	{
		painter.setPen(pen2);
		painter.setFont(font1);

		painter.drawText(40, 60, parListVal.at(2));

		painter.setFont(font2);
		painter.drawText(90, 40, parListVal.at(1));
		painter.drawText(90, 60, parListVal.at(0));

		painter.setPen(pen1);
		painter.drawText(QRect(15, 0, 165, 25), Qt::AlignHCenter | Qt::AlignVCenter, "Today");
	}
}

//////////////////////////////////////////////////////////////////////////

CInfoNameWidget::CInfoNameWidget(CAppStyle *style, QImage titleBar, QWidget *parent, Qt::WindowFlags flags)
: CInfoWidget(style, titleBar, parent, flags)
{
    setMouseTracking(true);
    m_bSymTrackEna = false;
	m_bMNEna = true;
    resize(600, 133);
    m_pMarkLabel = new QLabel(this);
    m_pMarkLabel->hide();
    m_pMarkLabel->setStyleSheet("QLabel { background-color: rgba(156, 182, 45, 21%); border: none; }");
    m_pMarkLabel->resize(9, 48-1);
}

CInfoNameWidget::~CInfoNameWidget()
{}


void CInfoNameWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    //int wpx=20, wpy=90;
    //int wsx=180, wsy=100;
    int wpx=0, wpy=0;
    int wsx=this->width();
    //int wsy=this->height();

//#ifdef Q_WS_MAC
    QFont font2("Arial");
    font2.setPixelSize(12);
    QFont font3("Courier");
    font3.setPixelSize(12);
    QFont font3s("Courier");
    font3s.setPixelSize(10);
    QFont font3b("Courier");
    font3b.setPixelSize(12);
    font3b.setBold(true);
    QFont font4("Arial");
    font4.setPixelSize(12);
    font4.setBold(true);
/*#else
    QFont font2("Arial", 10);
    QFont font3("Courier", 10);
    QFont font3s("Arial", 5);
    QFont font3b("Courier", 10);
    font3b.setBold(true);
    QFont font4("Arial", 10);
    font4.setBold(true);
#endif*/

    painter.setFont(font2);
    QPen pen2(QColor(236, 231, 224, 255));
    //QPen pen4(QColor(255, 255, 255, 255));
    QPen pen5(QColor(0xF0, 0x60, 0x40, 255));
    pen2.setWidth(1);
    painter.setPen(pen2);


    int vstep=16;
    int sepx=80;
    int tposyoff=3;
    int tposy=m_Res.titleBar.height()+tposyoff;

    painter.fillRect(wpx, wpy+m_Res.titleBar.height(), wsx, parList.size()*vstep+tposyoff*2, appstyle->updateColor(QColor::fromRgb(65,53,46,255)));

    QPen pen1(appstyle->updateColor(QColor(93, 84, 78, 255)));
    pen1.setWidth(1);

    for (int a=0; a<parList.size(); a++)
    {
        painter.setPen(pen2);
        for (int i=0; i<parList.at(a).size(); i++)
        {
            QChar sym;
            if (a<3)
                sym = parList.at(1).at(i);
            else
                sym = parList.at(4).at(i);
            sym = sym.toUpper();
            char s = sym.toAscii();

            if (((s == 'K') || (s == 'V')) && m_bMNEna)
                painter.setPen(pen5);
            else
                painter.setPen(pen2);

            //painter.drawStaticText(QPoint(wpx+4+i*9, wpy+vstep*a+tposy), Qt::AlignVCenter | Qt::AlignLeft, parList.at(a).at(i) );
            if ((((s == 'K') || (s == 'V')) && (a!=1) && (a!=4) && (parList.at(a).at(i) != ' ') && !m_GlobSettings.calcNameDD) && m_bMNEna)
            {
                painter.setFont(font3s);
                QString r = "1\n1";
                if (s == 'V')
                    r = "2\n2";
                painter.drawText( QRectF(wpx+4+i*9, wpy+vstep*a+tposy, 9, vstep), Qt::AlignVCenter | Qt::AlignLeft, r );
            } else
            {
                painter.setFont(font3);
                painter.drawText( QRectF(wpx+4+i*9, wpy+vstep*a+tposy, 9, vstep), Qt::AlignVCenter | Qt::AlignLeft, parList.at(a).at(i) );
            }
        }
        painter.setPen(pen2);
        sepx = parList.at(a).size()*9+9;
        //if (parListVal.at(a).size()>(this->width()-sepx-9)/9)
        //    painter.setFont(font2s);
        //else
            painter.setFont(font2);
        //painter.drawText( QRectF(wpx+4+sepx, wpy+vstep*a+tposy, wsx-8-sepx, vstep), Qt::AlignVCenter | Qt::AlignLeft, parListVal.at(a) );
        drawFText(painter, wpx+4+sepx, wpy+vstep*a+tposy, parListVal.at(a));

        if ((a%3 == 0) && (a>0))
        {
            painter.setPen(pen1);
            painter.drawLine(wpx,wpy+vstep*a+tposy, wpx+wsx-1, wpy+vstep*a+tposy);
        }
    }

    QPen pen6(appstyle->updateColor(QColor(93, 84, 78, 255)));
    QPen pen7(appstyle->updateColor(QColor(57, 46, 40, 255)));
	painter.setPen(pen6);
	painter.drawRect(wpx, wpy+m_Res.titleBar.height()-1, wsx-1, parList.size()*vstep+tposyoff*2);
	painter.setPen(pen7);
	painter.drawRect(wpx+1, wpy+m_Res.titleBar.height()-1, wsx-3, parList.size()*vstep+tposyoff*2-1);
	painter.drawImage(wpx, wpy, m_Res.titleBar);


    /*painter.setPen(pen1);
    painter.drawRect(wpx, wpy+m_Res.titleBar.height()-1, wsx-1, parList.size()*vstep+tposyoff*2);
    painter.drawImage(wpx, wpy, m_Res.titleBar);

    painter.setPen(pen4);
    painter.setFont(font4);
    painter.drawText( QRectF(wpx+4, wpy, m_Res.titleBar.width()-8, m_Res.titleBar.height()), Qt::AlignVCenter | Qt::AlignHCenter, this->windowTitle() );*/
}


void CInfoNameWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (m_bSymTrackEna)
    {
        int vstep=16*3+3, xstep=9;
        int yoff=m_Res.titleBar.height()+2, xoff=3;

        int x = event->x() - xoff, y = event->y() - yoff;
        int ix = x/xstep, iy = y/(vstep);
        if ((ix>=0) && (iy>=0) && (iy<2) && (ix<parList.at(1+iy*3).size()))
        {
            m_pMarkLabel->move(ix*xstep+xoff, iy*vstep+yoff);
            m_pMarkLabel->show();
        } else
        {
            m_pMarkLabel->hide();
        }
    }
}


void CInfoNameWidget::leaveEvent(QEvent *)
{
    if (m_bSymTrackEna)
        m_pMarkLabel->hide();
}


void CInfoNameWidget::mousePressEvent(QMouseEvent *event)
{
    if (m_bSymTrackEna)
    {
        if ((event->buttons()&Qt::LeftButton)!=0)
        {
            int vstep=16*3+3, xstep=9;
            int yoff=m_Res.titleBar.height()+2, xoff=3;

            int x = event->x() - xoff, y = event->y() - yoff;
            int ix = x/xstep, iy = y/(vstep);
            if ((ix>=0) && (iy>=0) && (iy<2) && (ix<parList.at(1+iy*3).size()))
            {
                emit symbolCliked(iy, ix);
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////

CInfoCalendarWidget::CInfoCalendarWidget(CAppStyle *style, QImage titleBar, QWidget *parent, Qt::WindowFlags flags)
: CInfoWidget(style, titleBar, parent, flags)
{
    resize(600, 90);
}

CInfoCalendarWidget::~CInfoCalendarWidget()
{}


void CInfoCalendarWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    //int wpx=20, wpy=90;
    //int wsx=180, wsy=100;
    int wpx=0, wpy=0;
    int wsx=this->width();
    //int wsy=this->height();

//#ifdef Q_WS_MAC
    QFont font2("Arial");
    font2.setPixelSize(10);
    QFont font4("Arial");
    font4.setPixelSize(11);
    font4.setBold(true);
/*#else
    QFont font2("Arial", 10);
    QFont font4("Arial", 10);
    font4.setBold(true);
#endif*/

    painter.setFont(font2);
    QPen pen2(QColor(236, 231, 224, 255));
    pen2.setWidth(1);
    QPen pen3(QColor(236, 231, 224, 255));
    pen3.setWidth(1);

    painter.setPen(pen2);


    int vstep=16, hstep=46;
    int sepx=40;
    int tposyoff=3;
    int tposy=m_Res.titleBar.height()+tposyoff;

    painter.fillRect(wpx, wpy+m_Res.titleBar.height(), wsx, parList.size()*vstep+tposyoff*2, appstyle->updateColor(QColor::fromRgb(65,53,46,255)));

    int a=0;
    painter.setFont(font2);

    const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

    if ( m_PersYear > 0 )
    {

        int m_cyear=cdate.year();
        for (int i=0; i<12; i++)
        {
            int y = m_cyear+i;
            int PersYear = CNumerology::downToDigit(m_PersYear + CNumerology::downToDigit(y));

            if (y == cdate.year())
                painter.setPen(pen3);
            else
                painter.setPen(pen2);

            painter.drawText( QRectF(wpx+4+i*hstep+sepx, wpy+vstep*a+tposy, hstep, vstep), Qt::AlignVCenter | Qt::AlignHCenter, QString::number(y)+" ("+QString::number(PersYear)+")" );
        }
        a++;

        for (int i=0; i<12; i++)
        {
            QDate cd;
            cd = cdate;
            cd = cd.addMonths(i);
			int m_CPersYear = CNumerology::downToDigit(m_PersYear + CNumerology::downToDigit(cd.year()));
            int c = CNumerology::downToDigit(m_CPersYear + CNumerology::downToDigit(cd.month()));

            if (cd.month() == cdate.month())
                painter.setPen(pen3);
            else
                painter.setPen(pen2);

            QString ms = QString(mname[cd.month()-1]) +" ("+QString::number(c)+")";
            painter.drawText(QRectF(wpx+4+i*hstep+sepx, wpy+vstep*a+tposy, hstep, vstep), Qt::AlignVCenter | Qt::AlignHCenter, ms);
        }
        a++;

        //int m_CPersDate = CNumerology::downToDigit(m_CPersMonth + CNumerology::downToDigit(cdate.day()));
        for (int i=0; i<12; i++)
        {
            QDate cd;
            cd = cdate;
            cd = cd.addDays(i);
			int m_CPersYear = CNumerology::downToDigit(m_PersYear + CNumerology::downToDigit(cd.year()));
            int m_CPersMonth = CNumerology::downToDigit(m_CPersYear + CNumerology::downToDigit(cd.month()));
            int c = CNumerology::downToDigit(m_CPersMonth + CNumerology::downToDigit(cd.day()));

            if (cd.day() == cdate.day())
                painter.setPen(pen3);
            else
                painter.setPen(pen2);

            QString ms = QString::number(cd.day()) +" ("+QString::number(c)+")";
            painter.drawText(QRectF(wpx+4+i*hstep+sepx, wpy+vstep*a+tposy, hstep, vstep), Qt::AlignVCenter | Qt::AlignHCenter, ms);
        }
    }

    painter.setFont(font2);
    for (int a=0; a<parList.size(); a++)
        painter.drawText( QRectF(wpx+4, wpy+vstep*a+tposy, sepx-4, vstep), Qt::AlignVCenter | Qt::AlignLeft, parList.at(a) );

    QPen pen6(appstyle->updateColor(QColor(93, 84, 78, 255)));
    QPen pen7(appstyle->updateColor(QColor(57, 46, 40, 255)));
    painter.setPen(pen6);
    painter.drawRect(wpx, wpy+m_Res.titleBar.height()-1, wsx-1, parList.size()*vstep+tposyoff*2);
    painter.setPen(pen7);
    painter.drawRect(wpx+1, wpy+m_Res.titleBar.height()-1, wsx-3, parList.size()*vstep+tposyoff*2-1);
    painter.drawImage(wpx, wpy, m_Res.titleBar);
}


//////////////////////////////////////////////////////////////////////////

CInfoEssenceWidget::CInfoEssenceWidget(CAppStyle *style, QImage titleBar, QWidget *parent, Qt::WindowFlags flags)
: CInfoWidget(style, titleBar, parent, flags)
{
	m_byear=0;
    startAge=0;
    for (int a=0; a<128; a++)
    {
        m_Essence[a]=0;
        m_EssenceS[a]=0;
    }
    cImg = QPixmap(1800,120);
    cImg.fill(QColor::fromRgb(65,53,46,255));
    resize(600, 148);

    m_pSWButton = new QPushButton("< Age >",this);
    m_pSWButton->setStyleSheet("QPushButton { color: #101010; background-color: none; border: none; }");
    m_pSWButton->resize(64, titleBar.height());
    m_pSWButton->move(600-64-8, 0);
    QObject::connect(m_pSWButton, SIGNAL(clicked()), this, SLOT(OnSwitchAge()));
}

CInfoEssenceWidget::~CInfoEssenceWidget()
{}


void CInfoEssenceWidget::OnSwitchAge()
{
    startAge = (startAge+1)%3;

    update();
}


void CInfoEssenceWidget::updateData()
{
    startAge=0;

    QPainter painter(&cImg);

    int wpx=0, wpy=0;
    int wsx=cImg.width();
//#ifdef Q_WS_MAC
    QFont font2("Arial");
    font2.setPixelSize(12);
    QFont font3("Courier");
    font3.setPixelSize(12);
    QFont font2s("Arial");
    font2s.setPixelSize(10);
/*#else
    QFont font2("Arial", 10);
    QFont font3("Courier", 10);
    QFont font2s("Arial", 9);
#endif*/

    QPen pen2(appstyle->updateColor(QColor(236, 231, 224, 255)));
    pen2.setWidth(1);
    painter.setPen(pen2);


    int vstep=16, hstep=8*2;
    int tposyoff=3;
    int tposy=tposyoff;

    painter.fillRect(wpx, wpy, wsx, parList.size()*vstep+tposyoff*2, appstyle->updateColor(QColor::fromRgb(65,53,46,255)));
    for (int i=1; i<101; i+=2)
    {
        painter.fillRect(wpx+i*hstep, wpy+vstep*0, hstep, parList.size()*vstep+tposyoff*2, appstyle->updateColor(QColor::fromRgb(78, 64, 55,255)));
    }

    QPen pen1(appstyle->updateColor(QColor(93*1.5, 84*1.5, 78*1.5, 255)));
    pen1.setWidth(1);

    painter.setFont(font2s);
    for (int i=0; i<101; i++)
    {
        painter.drawText( QRectF(wpx+i*hstep, wpy+vstep*(0)+tposy, hstep, vstep), Qt::AlignVCenter | Qt::AlignHCenter, QString::number(i) );
		painter.drawText( QRectF(wpx+i*hstep, wpy+vstep*(1)+tposy, hstep, vstep), Qt::AlignVCenter | Qt::AlignHCenter, QString::number((i+m_byear)%100));
    }
    if ((parList.size()>0) && (parList.at(0).size()>0))
    {
        for (int i=0; i<101; i++)
        {
            painter.drawText( QRectF(wpx+i*hstep, wpy+vstep*(5)+tposy, hstep, vstep), Qt::AlignVCenter | Qt::AlignHCenter, QString::number(m_EssenceS[i]) );
            painter.drawText( QRectF(wpx+i*hstep, wpy+vstep*(6)+tposy, hstep, vstep), Qt::AlignVCenter | Qt::AlignHCenter, QString::number(m_Essence[i]) );
        }

        painter.setFont(font2);
        for (int a=0; a<parList.size(); a++)
        {
            painter.setFont(font3);
            for (int i=0; i<parList.at(a).size(); i++)
            {
                painter.drawText( QRectF(wpx+i*hstep, wpy+vstep*(a+2)+tposy, hstep, vstep), Qt::AlignVCenter | Qt::AlignHCenter, parList.at(a).at(i) );
            }
        }
    }
    for (int a=0; a<7; a++)
    {
        if ((a==2)||(a==5))
        {
            painter.setPen(pen1);
            painter.drawLine(wpx,wpy+vstep*a+tposy, wpx+wsx-1, wpy+vstep*a+tposy);
        }
    }

}


void CInfoEssenceWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    int wpx=0, wpy=0;
    int wsx=this->width();
    int vstep=16;
    int tposyoff=3;

    painter.fillRect(wpx, wpy+m_Res.titleBar.height(), wsx, parList.size()*vstep+tposyoff*2, appstyle->updateColor(QColor::fromRgb(65,53,46,255)));
    painter.drawPixmap(4,m_Res.titleBar.height(),cImg,startAge*512,0,wsx-4, parList.size()*vstep+tposyoff*2);

    QPen pen6(appstyle->updateColor(QColor(93, 84, 78, 255)));
    QPen pen7(appstyle->updateColor(QColor(57, 46, 40, 255)));
    painter.setPen(pen6);
    painter.drawRect(wpx, wpy+m_Res.titleBar.height()-1, wsx-1, parList.size()*vstep+tposyoff*2);
    painter.setPen(pen7);
    painter.drawRect(wpx+1, wpy+m_Res.titleBar.height()-1, wsx-3, parList.size()*vstep+tposyoff*2-1);
    painter.drawImage(wpx, wpy, m_Res.titleBar);
}

//////////////////////////////////////////////////////////////////////////

CInfoExprPlanWidget::CInfoExprPlanWidget(CAppStyle *style, QImage titleBar, QWidget *parent, Qt::WindowFlags flags)
: CInfoWidget(style, titleBar, parent, flags)
{
    resize(600, 100);
}

CInfoExprPlanWidget::~CInfoExprPlanWidget()
{}


void CInfoExprPlanWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    int wpx=0, wpy=0;
    int wsx=this->width();

    QFont font2("Arial");
    font2.setPixelSize(12);
    QFont font4("Arial");
    font4.setPixelSize(12);
    font4.setBold(true);

    painter.setFont(font2);
    QPen pen2(QColor(236, 231, 224, 255));
    pen2.setWidth(1);
    QPen pen3(QColor(236, 231, 224, 255));
    pen3.setWidth(1);

    pen2.setWidth(1);
    painter.setPen(pen2);


    int vstep=16, hstep=120;
    int sepx=117;
    int tposyoff=3;
    int tposy=m_Res.titleBar.height()+tposyoff;

    painter.fillRect(wpx, wpy+m_Res.titleBar.height(), wsx, parList.size()*vstep+tposyoff*2, appstyle->updateColor(QColor::fromRgb(65,53,46,255)));

    int a=0;

    QStringList hlist;
    hlist.append("Mental");
    hlist.append("Physical");
    hlist.append("Emotional");
    hlist.append("Intuitive");
    painter.setFont(font2);
    for (int i=0; i<4; i++)
        painter.drawText( QRectF(wpx+4+i*hstep+sepx, wpy+vstep*a+tposy, hstep, vstep), Qt::AlignVCenter | Qt::AlignHCenter, hlist.at(i) + " " + parListVal.at(3*5+i) );
    a++;

    for (int y=0; y<3; y++)
    {
        for (int x=0; x<4; x++)
        {
            painter.drawText( QRectF(wpx+4+x*hstep+sepx, wpy+vstep*(y+1)+tposy, hstep, vstep), Qt::AlignVCenter | Qt::AlignHCenter, parListVal.at(y*5+x) );
        }
    }

    painter.setFont(font2);
    for (int a=1; a<parList.size(); a++)
        painter.drawText( QRectF(wpx+4, wpy+vstep*a+tposy, sepx-4, vstep), Qt::AlignVCenter | Qt::AlignLeft, parList.at(a)+ " " + parListVal.at((a-1)*5+4) );

    QPen pen6(appstyle->updateColor(QColor(93, 84, 78, 255)));
    QPen pen7(appstyle->updateColor(QColor(57, 46, 40, 255)));
	painter.setPen(pen6);
	painter.drawRect(wpx, wpy+m_Res.titleBar.height()-1, wsx-1, parList.size()*vstep+tposyoff*2);
	painter.setPen(pen7);
	painter.drawRect(wpx+1, wpy+m_Res.titleBar.height()-1, wsx-3, parList.size()*vstep+tposyoff*2-1);
	painter.drawImage(wpx, wpy, m_Res.titleBar);
}


//////////////////////////////////////////////////////////////////////////

CInfoMatrWidget::CInfoMatrWidget(CAppStyle *style, QImage titleBar, QWidget *parent, Qt::WFlags flags)
    : QWidget(parent, flags)
{
    appstyle = style;

    for (int a=0; a<20; a++)
        pmatr[a] = 7;

    m_Res.titleBar = titleBar;

	hideHover = true;
	enableHover = false;
	mouse_x = -1;
	mouse_y = -1;

	setMouseTracking(true);

#ifdef Q_WS_WIN
    QGraphicsDropShadowEffect *titShadow = new QGraphicsDropShadowEffect(this);
    titShadow->setBlurRadius(3);
    titShadow->setColor(QColor(63, 63, 63, 180));
    titShadow->setOffset(1,1);
#endif

    wtitle = new QLabel(this);
#ifdef Q_WS_WIN
    wtitle->setStyleSheet("QLabel { color: black; font-size: 12px; background:none; border:none; }");
#else
    wtitle->setStyleSheet("QLabel { color: black; font: bold 12px; background:none; border:none; }");
#endif
    wtitle->setGeometry(10,1,titleBar.width()-20,28);
#ifdef Q_WS_WIN
    wtitle->setGraphicsEffect(titShadow);
#endif

    resize(600,291);
}

CInfoMatrWidget::~CInfoMatrWidget()
{}


void CInfoMatrWidget::mouseMoveEvent(QMouseEvent *event)
{
	mouse_x = event->x();
	mouse_y = event->y();
	hideHover = false;
	update();
}

void CInfoMatrWidget::leaveEvent(QEvent *)
{
	hideHover = true;
	update();
}

void CInfoMatrWidget::enterEvent(QEvent *)
{
	hideHover = false;
}

void CInfoMatrWidget::mousePressEvent(QMouseEvent *event)
{
	mouse_x = event->x();
	mouse_y = event->y();

	int sx=118, sy=63;
	int ox=12, oy=35;
	for (int cy=0; cy<4; cy++)
	{
		for (int cx=0; cx<5; cx++)
		{
			if (enableHover && (!hideHover) && (((cy>=3) && (cx!=4)) || (cx>0)))
			{
				if ((mouse_x>=ox+sx*cx-3) && (mouse_x<ox+sx*(cx+1)-12)
					&& (mouse_y>=oy+sy*cy+3) && (mouse_y<oy+sy*(cy+1)-4))
				{
					emit itemCliked(cy*5+cx);
					return;
				}
			}
		}
	}
}


void CInfoMatrWidget::drawSq(QPainter &painter, int x, int y, QString str1, QString str2, int val)
{
    QString s1;
    if (str1 == "*")
    {
        if (val>0)
            s1 = QString::number(val);
    } else
    {
        for (int a=0; a<val; a++)
            s1 += str1;
    }
    if (s1.size()==0)
        s1 = "-";

//#ifdef Q_WS_MAC
    QFont font2("Tahoma");
    font2.setPixelSize(14);
/*#else
    QFont font2("Tahoma", 12);
#endif*/
    font2.setBold(true);
    painter.setFont(font2);
    QPen pen2(QColor(156, 182, 45, 255));
    pen2.setWidth(1);
    painter.setPen(pen2);
    painter.drawText(QRectF(x, y+10, 105, 65-8), Qt::AlignHCenter | Qt::AlignVCenter, s1);


//#ifdef Q_WS_MAC
    QFont font3("Tahoma");
    font3.setPixelSize(11);
/*#else
    QFont font3("Tahoma", 9);
#endif*/
    //font3.setBold(true);
    painter.setFont(font3);
    QPen pen3(QColor(237, 219, 178, 255));
    pen3.setWidth(1);
    painter.setPen(pen3);
    painter.drawText(QRectF(x, y+5, 105, 18), Qt::AlignHCenter | Qt::AlignVCenter, str2);

    // Points
    /*painter.setPen(QColor::fromRgb(255,30,30,255));
    int ox=8, oy=35, sx=12, pw=7;

    for (int a=0; a<val; a++)
    {
        QPainterPath path1;
        path1.addEllipse(x+ox+sx*a, y+oy, pw, pw);
        //path1.addRoundedRect(x+ox+sx*a, y+oy, pw, pw-2, 4, 4);
        painter.fillPath(path1, QColor::fromRgb(255,200,200,255));
        painter.drawEllipse(x+ox+sx*a, y+oy, pw, pw);
    }*/
}


void CInfoMatrWidget::paintEvent(QPaintEvent *)
{
    //qDebug() << "paint";
    QPainter painter(this);
    painter.drawImage(0, 0, appstyle->modifTitleBar1);
    painter.drawImage(0, 0, m_Res.titleBar);
    //painter.fillRect(0, 0, m_Res.titleBar.width(), m_Res.titleBar.height(), QColor(44, 36, 32));

    QStringList vnames;

    vnames << "Character"; //1
    vnames << "Health"; //4
    vnames << "Luck"; //7
    vnames << "Purposeful";

    vnames << "Energy"; //2
    vnames << "Intuition"; //5
    vnames << "Duty"; //8
    vnames << "Family forming";


    vnames << "Interest"; //3
    vnames << "Labour"; //6
    vnames << "Memory"; //9
    vnames << "Stability";


    vnames << "Self appraisal";
    vnames << "Finance";
    vnames << "Talent";
    vnames << "Spirituality";
    vnames << "Sexual life";



    int sx=118, sy=63;
	int ox=12, oy=35;
	for (int cy=0; cy<4; cy++)
	{
		for (int cx=0; cx<5; cx++)
		{
			if (enableHover && (!hideHover) && (((cy>=3) && (cx!=4)) || (cx>0)))
			{
				if ((mouse_x>=ox+sx*cx-3) && (mouse_x<ox+sx*(cx+1)-12)
					&& (mouse_y>=oy+sy*cy+3) && (mouse_y<oy+sy*(cy+1)-4))
					painter.fillRect(ox+sx*cx-2, oy+sy*cy+3, sx-10, sy-8, appstyle->updateColor(QColor(130,106,92, 50)));
			}
		}
	}

    for (int cy=0; cy<3; cy++)
    {
        for (int cx=0; cx<3; cx++)
        {
            drawSq(painter, ox+sx*(cx+1), oy+sy*cy, QString::number(cx*3+cy+1), vnames.at(cx+cy*4), pmatr[cx+cy*4]);
        }
    }
    for (int cx=0; cx<3; cx++)
        drawSq(painter, ox+sx*(cx+1), oy+sy*3, "*", vnames.at(cx+3*4), pmatr[cx+3*4]);
    drawSq(painter, ox, oy+sy*3, "*", vnames.at(4+3*4), pmatr[4+3*4]);

    for (int cy=0; cy<3; cy++)
        drawSq(painter, ox+4*sx, oy+sy*cy, "*", vnames.at(3+cy*4), pmatr[3+cy*4]);

    drawSq(painter, ox+4*sx, oy+sy*3, "*", vnames.at(3+3*4), pmatr[3+3*4]);
}


//////////////////////////////////////////////////////////////////////////

CInfoCompMatrWidget::CInfoCompMatrWidget(CAppStyle *style, QImage titleBar, QWidget *parent, Qt::WFlags flags)
    : QWidget(parent, flags)
{
    appstyle = style;

    for (int a=0; a<20; a++)
    {
        pmatr[a] = 7;
        pmatr2[a] = 3;
    }

    m_Res.titleBar = titleBar;

#ifdef Q_WS_WIN
    QGraphicsDropShadowEffect *titShadow = new QGraphicsDropShadowEffect(this);
    titShadow->setBlurRadius(3);
    titShadow->setColor(QColor(63, 63, 63, 180));
    titShadow->setOffset(1,1);
#endif

    wtitle = new QLabel(this);
#ifdef Q_WS_WIN
    wtitle->setStyleSheet("QLabel { color: black; font-size: 12px; background:none; border:none; }");
#else
    wtitle->setStyleSheet("QLabel { color: black; font: bold 12px; background:none; border:none; }");
#endif
    wtitle->setGeometry(10,1,titleBar.width()-20,28);
#ifdef Q_WS_WIN
    wtitle->setGraphicsEffect(titShadow);
#endif

    resize(600,291);
}

CInfoCompMatrWidget::~CInfoCompMatrWidget()
{}


void CInfoCompMatrWidget::drawSq(QPainter &painter, int x, int y, QString, QString str2, int val, int val2)
{
    QString s1, s2;
    if (val>0)
        s1 = QString::number(val);
    else
        s1 = "-";
    if (val2>0)
        s2 = QString::number(val2);
    else
        s2 = "-";


//#ifdef Q_WS_MAC
    QFont font2("Tahoma");
    font2.setPixelSize(14);
/*#else
    QFont font2("Tahoma", 12);
#endif*/
	font2.setBold(true);
	painter.setFont(font2);
	QPen pen2(QColor(156, 182, 45, 255));
	pen2.setWidth(1);
	painter.setPen(pen2);
    painter.drawText(QRectF(x, y+10, 105, 65-8), Qt::AlignHCenter | Qt::AlignVCenter, s1+"/"+s2 );


//#ifdef Q_WS_MAC
    QFont font3("Tahoma");
    font3.setPixelSize(11);
/*#else
    QFont font3("Tahoma", 9);
#endif*/
	//font3.setBold(true);
	painter.setFont(font3);
	QPen pen3(QColor(237, 219, 178, 255));
	pen3.setWidth(1);
	painter.setPen(pen3);
    painter.drawText(QRectF(x, y+5, 105, 18), Qt::AlignHCenter | Qt::AlignVCenter, str2);
}


void CInfoCompMatrWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.drawImage(0, 0, appstyle->modifTitleBar1);
    painter.drawImage(0, 0, m_Res.titleBar);

    QStringList vnames;

    vnames << "Character"; //1
    vnames << "Health"; //4
    vnames << "Luck"; //7
    vnames << "Purposeful";

    vnames << "Energy"; //2
    vnames << "Intuition"; //5
    vnames << "Duty"; //8
    vnames << "Family forming";


    vnames << "Interest"; //3
    vnames << "Labour"; //6
    vnames << "Memory"; //9
    vnames << "Stability";


    vnames << "Self appraisal";
    vnames << "Money";
    vnames << "Talent";
    vnames << "Spirituality";
    vnames << "Sexual life";



    int sx=119, sy=63;
	int ox=12, oy=35;
    for (int cy=0; cy<3; cy++)
    {
        for (int cx=0; cx<3; cx++)
        {
            drawSq(painter, ox+sx*(cx+1), oy+sy*cy, QString::number(cx*3+cy+1), vnames.at(cx+cy*4), pmatr[cx+cy*4], pmatr2[cx+cy*4]);
        }
    }
    for (int cx=0; cx<3; cx++)
        drawSq(painter, ox+sx*(cx+1), oy+sy*3, "*", vnames.at(cx+3*4), pmatr[cx+3*4], pmatr2[cx+3*4]);
    drawSq(painter, ox, oy+sy*3, "*", vnames.at(4+3*4), pmatr[4+3*4], pmatr2[4+3*4]);

    for (int cy=0; cy<3; cy++)
        drawSq(painter, ox+4*sx, oy+sy*cy, "*", vnames.at(3+cy*4), pmatr[3+cy*4], pmatr2[3+cy*4]);

    drawSq(painter, ox+4*sx, oy+sy*3, "*", vnames.at(3+3*4), pmatr[3+3*4], pmatr2[3+3*4]);
}


//////////////////////////////////////////////////////////////////////////
CInputWidget::CInputWidget(QWidget *parent)
    : QWidget(parent)
{
}

CInputWidget::~CInputWidget()
{}


//--------------------------------------------------------------------
CExpandWidget::CExpandWidget(CAppStyle *style, QWidget *parent, int width, int timeMsAnimation)
    : QWidget(parent), globalWidth(width), animTime(timeMsAnimation)
{
    appstyle = style;

    this->resize(13, 98);
	bkg = new QWidget(this);
	bkg->resize(600, 98);
	bkg->move(13,0);


    butLab = new QLabel(this);
    butLab->resize(15, this->height());
    button = new QPushButton(this);
    button->resize(15, this->height());

    button->setStyleSheet("border: transparent; background: transparent;");

    infoLabel = new QLabel(this);
    infoLabel->move(25, 12);
    infoLabel->resize(globalWidth-50, this->height()-20);
	infoLabel->setStyleSheet("QLabel { background:none; border:none; color: #ffffff; font: 12px; }");
	infoLabel->setWordWrap(true);

    animation = new QPropertyAnimation(this, "geometry");

	QObject::connect(button, SIGNAL(clicked()), this, SLOT(slotExpand()));

    slotUpdate();
}
//--------------------------------------------------------------------
CExpandWidget::~CExpandWidget()
{

}
//--------------------------------------------------------------------
void CExpandWidget::slotExpand()
{
	animation->stop();
    animation->setDuration(animTime);
    if (this->width() < 30)
    {
        animation->setStartValue(QRect(this->pos().x(), this->pos().y(), this->width(), this->height()));
        animation->setEndValue(QRect(startPos.x() - globalWidth, startPos.y(), 13 + globalWidth, 98));
    }
    else
    {
        animation->setStartValue(QRect(this->pos().x(), this->pos().y(), this->width(), this->height()));
        animation->setEndValue(QRect(startPos.x(), startPos.y(), 13, 98));
    }
    animation->start();
}
//--------------------------------------------------------------------
void CExpandWidget::paintEvent(QPaintEvent *)
{
}
//--------------------------------------------------------------------
void CExpandWidget::slotUpdate()
{
    QString bsckground("QWidget { background: ");
    bsckground += appstyle->updateColor(QString("#352821"));
    bsckground += "; border: none; }";
    bkg->setStyleSheet(bsckground);
    butLab->setPixmap(QPixmap::fromImage(appstyle->updateImage(appstyle->expandwidget)));
}
//--------------------------------------------------------------------
