#ifndef EULADLG_H
#define EULADLG_H

#include <QtGui>
#include "global.h"

class CEulaDlg : public QDialog
{
	Q_OBJECT

public:
	CEulaDlg(QWidget *parent=NULL);
	~CEulaDlg();

	QPushButton *m_pOk;
	QPushButton *m_pCancel;
	QPlainTextEdit *edit;

	int res;
private:
public slots:
	void OnOk();
	void OnCancel();
};


#endif // EULADLG_H
