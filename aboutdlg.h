
#ifndef ABOUTDLG_H
#define ABOUTDLG_H

#include <QtGui>
#include "global.h"

class CAboutDlg : public QDialog
{
	Q_OBJECT

public:
	CAboutDlg(QImage *bkg, QImage *shp, QWidget *parent = 0);
	~CAboutDlg();

	QImage *pBackground;
	QImage *pShape;

	QPushButton *visitBut;
	QPushButton *okBut;

private:
	void paintEvent(QPaintEvent * event);
	void mouseReleaseEvent(QMouseEvent *pEvent);

public slots:
	void OnVisit();
	void OnOk();
};

#endif // ABOUTDLG_H
