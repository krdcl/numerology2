#include "numer-pet.h"


QString numerPetC[128];
QString numerPetDForecast[128];


CPetNumerology::CPetNumerology(QObject *parent)
: QObject(parent)
{
	resetData();
}


CPetNumerology::~CPetNumerology()
{
}


void CPetNumerology::resetData()
{
	clear();
	m_LifePath = -1;
	m_TextDoc.clear();
	for (int a=0; a<64; a++)
	{
		m_PELet[a]=0;
		m_Let[a]=0;
	}
	for (int a=0; a<20; a++)
	{
		numDet.PE[a] = "";
		numDet.PENum[a] = "";
		numDet.PECnt[a] = "";
		numDet.PERep[a] = "";
	}
}


void CPetNumerology::clear()
{
	for (int a=0; a<64; a++)
		m_BaseNums[a]=0;
}


bool CPetNumerology::isBase(int num)
{
	if ( (num>=0) && (num<=9) || (num==11) || (num==22) || (num==33))
		return true;
	else
		return false;
}


bool CPetNumerology::isCarmic(int num)
{
	if ((num==13) || (num==14) || (num==16) || (num==19))
		return true;
	else
		return false;
}


int CPetNumerology::downOnce(int num)
{
	if (isBase(num))
		return num;

	int res = num % 10 + ((num/10) % 10) + ((num/100) % 10) + ((num/1000) % 10);
	return res;
}


int CPetNumerology::downToBase(int num)
{
	int rnum = num;

	if (isCarmic(rnum) || isBase(rnum))
		m_BaseNums[rnum]++;

	while(!isBase(rnum))
	{
		rnum = downOnce(rnum);
		if (isCarmic(rnum) || isBase(rnum))
			m_BaseNums[rnum]++;
	}
	return rnum;
}


int CPetNumerology::statDownToBase(int num)
{
	int rnum = num;

	while(!isBase(rnum))
	{
		rnum = downOnce(rnum);
		if (rnum<=0)
			return 0;
	}
	return rnum;
}


int CPetNumerology::downToDigit(int num)
{
	int rnum = num;

	while(!isBase(rnum))
	{
		rnum = downOnce(rnum);
		if (rnum<=0)
			return 0;
	}

	if (rnum>9)
		rnum = rnum % 10 + ((rnum/10) % 10);

	return rnum;
}


int CPetNumerology::letterToNumber(QChar ch)
{
	int r = ch.toAscii()-'A'+1;

	if ((r>=1) && (r<=26))
		return downToDigit(r);
	else
		return 0;
}


int CPetNumerology::wordToNumber(QString str)
{
	int r = 0;
	for (int a=0; a<str.size(); a++)
		r += letterToNumber(str[a]);

	//r = statDownToBase(r);

	return r;
}



void CPetNumerology::setCalcMode(int mode)
{
	if (mode==0)
	{
		//				 ABCDEFGHIJKLMNOPQRSTVUWXYZ
		//char lconst[] = "12345835112345781224666611";

		for (int a=0; a<33; a++)
			m_CalcMode[a] = downToDigit(a);
	} else if (mode==2)
	{
		//				 ABCDEFGHIJKLMNOPQRSTVUWXYZ
		//char lconst[] = "                          ";

	} else
	{
		for (int a=0; a<33; a++)
			m_CalcMode[a] = downToDigit(a);
	}

}


void CPetNumerology::calcNamesNumbers(QString str, QList<int> &res)
{
	QStringList nlist = str.split(" ");

	for (int a=0; a<nlist.size(); a++)
		res.push_back(wordToNumber(nlist[a]));
}


int CPetNumerology::isVow(QChar sym)
{
	sym = sym.toUpper();
	char s = sym.toAscii();

	if (s == 'B'
		|| s == 'C'
		|| s == 'D'
		|| s == 'F'
		|| s == 'G'
		|| s == 'H'
		|| s == 'J'
		|| s == 'K'
		|| s == 'L'
		|| s == 'M'
		|| s == 'N'
		|| s == 'P'
		|| s == 'Q'
		|| s == 'R'
		|| s == 'S'
		|| s == 'T'
		|| s == 'V'
		|| s == 'X'
		|| s == 'Z')
		return -1;

	if (sym == 'A'
		|| s == 'E'
		|| s == 'I'
		|| s == 'O'
		|| s == 'U'
		|| s == 'W'
		|| s == 'Y')
		return 1;

	return 0;
}


int CPetNumerology::getPEIndex(QChar ch)
{
	ch = ch.toUpper();
	char s = ch.toAscii();

	if (s == 'A')
		return 0;
	else if (s == 'E')
		return 1;
	else if (s == 'O')
		return 2;
	else if (s == 'R')
		return 2;
	else if (s == 'I')
		return 2;
	else if (s == 'Z')
		return 2;
	else if (s == 'K')
		return 3;
	else if (s == 'H')
		return 4;
	else if (s == 'J')
		return 4;
	else if (s == 'N')
		return 4;
	else if (s == 'P')
		return 4;
	else if (s == 'W')
		return 5;
	else if (s == 'B')
		return 6;
	else if (s == 'S')
		return 6;
	else if (s == 'T')
		return 6;
	else if (s == 'X')
		return 6;
	else if (s == 'F')
		return 7;
	else if (s == 'Q')
		return 7;
	else if (s == 'U')
		return 7;
	else if (s == 'Y')
		return 7;
	else if (s == 'G')
		return 8;
	else if (s == 'L')
		return 8;
	else if (s == 'D')
		return 9;
	else if (s == 'M')
		return 9;
	else if (s == 'C')
		return 11;
	else if (s == 'V')
		return 11;

	return -1;
}


void CPetNumerology::calcNamesNumbersP(QString str, QList<int> &res, bool Vow)
{
	QStringList nlist = str.split(" ");

	for (int a=0; a<nlist.size(); a++)
	{
		if (Vow)
		{
			nlist[a].remove("B");
			nlist[a].remove("C");
			nlist[a].remove("D");
			nlist[a].remove("F");
			nlist[a].remove("G");
			nlist[a].remove("H");
			nlist[a].remove("J");
			nlist[a].remove("K");
			nlist[a].remove("L");
			nlist[a].remove("M");
			nlist[a].remove("N");
			nlist[a].remove("P");
			nlist[a].remove("Q");
			nlist[a].remove("R");
			nlist[a].remove("S");
			nlist[a].remove("T");
			nlist[a].remove("V");
			nlist[a].remove("X");
			nlist[a].remove("Z");
		} else
		{
			nlist[a].remove("A");
			nlist[a].remove("E");
			nlist[a].remove("I");
			nlist[a].remove("O");
			nlist[a].remove("U");
			nlist[a].remove("W");
			nlist[a].remove("Y");
		}
		res.push_back(wordToNumber(nlist[a]));
	}
}


bool CPetNumerology::calculate(QString fullName, QString shortName, QDate bdate, QDate cdate, bool trial)
{
	Q_UNUSED(shortName); Q_UNUSED(trial);
	fullName = fullName.toUpper();
	//shortName = shortName.toUpper();
	resetData();


	int bnum = downToDigit(downToDigit(bdate.day()) + downToDigit(bdate.month()) + downToDigit(bdate.year()));
	int cnum = downToDigit(downToDigit(cdate.day()) + downToDigit(cdate.month()) + downToDigit(cdate.year()));


	numDet.birthDay = QString::number(bnum);


	int m_expression;
	clear();
	QList<int> nnums;
	calcNamesNumbers(fullName, nnums);
	//int fnnum = nnums[0];
	int sum=0;
	for(int a=0; a<nnums.size(); a++)
	{
		sum += downToBase(nnums[a]);
	}
	m_expression = downToDigit(sum);

	numDet.expression = QString::number(m_expression);
	if (sum != m_expression)
		numDet.expression += "/" + QString::number(sum);


	numDet.fnameR = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR += "+";

		int v = nnums[a];
		int vb = downToBase(nnums[a]);
		numDet.fnameR += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR += '/' + QString::number(v);
	}
	numDet.fnameR += "=" + QString::number(sum);
	if (sum != m_expression)
		numDet.fnameR += "=" + QString::number(m_expression);


	nnums.clear();
	calcNamesNumbersP(fullName, nnums, true);
	sum=0;
	for(int a=0; a<nnums.size(); a++)
		sum += downToBase(nnums[a]);
	int m_hearDesire = downToDigit(sum);

	numDet.fnameR1 = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR1 += "+";

		int v = nnums[a];
		int vb = downToBase(nnums[a]);
		numDet.fnameR1 += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR1 += '/' + QString::number(v);
	}
	numDet.fnameR1 += "=" + QString::number(sum);
	if (sum != m_hearDesire)
		numDet.fnameR1 += "=" + QString::number(m_hearDesire);

	numDet.heartDesire = QString::number(m_hearDesire);
	if (sum != m_hearDesire)
		numDet.heartDesire += "/" + QString::number(sum);



	nnums.clear();
	calcNamesNumbersP(fullName, nnums, false);
	sum=0;
	for(int a=0; a<nnums.size(); a++)
		sum += downToBase(nnums[a]);
	int m_personality = downToDigit(sum);

	numDet.fnameR2 = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR2 += "+";

		int v = nnums[a];
		int vb = downToBase(nnums[a]);
		numDet.fnameR2 += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR2 += '/' + QString::number(v);
	}
	numDet.fnameR2 += "=" + QString::number(sum);
	if (sum != m_personality)
		numDet.fnameR2 += "=" + QString::number(m_personality);

	numDet.personality = QString::number(m_personality);
	if (sum != m_personality)
		numDet.personality += "/" + QString::number(sum);


	numDet.fnameN1 = "";
	numDet.fnameN2 = "";
	for (int a=0; a<fullName.size(); a++)
	{
		int v = isVow(fullName.at(a));
		if (v>0)
		{
			numDet.fnameN1 += QString::number(letterToNumber(fullName[a]));
			numDet.fnameN2 += " ";
		} else if (v<0)
		{
			numDet.fnameN2 += QString::number(letterToNumber(fullName[a]));
			numDet.fnameN1 += " ";
		} else
		{
			numDet.fnameN1 += " ";
			numDet.fnameN2 += " ";
		}
	}

	if ((m_expression==0))
	{
		QString Msg = tr("Error! Incorrect pet name!");
		QMessageBox::StandardButton Res;
		Res = QMessageBox::critical(NULL, tr(""), Msg, QMessageBox::Ok);

		return false;
	}

	int fdcode = downToDigit(m_expression+bnum+cnum);
	numDet.persYear = QString::number(fdcode);



	m_TextDoc.setTextWidth(590);

	QTextCharFormat h1;
	h1.setFontFamily("Arial");
	h1.setFontPointSize(16);
	//h1.setForeground(QBrush(QColor(60,60,30,230), Qt::SolidPattern));
	h1.setForeground(QBrush(QColor(0,0,0,255), Qt::SolidPattern));
	QTextBlockFormat h1p;
	h1p.setLeftMargin(10);

	QTextCharFormat h2;
	h2.setFontFamily("Arial");
	h2.setFontPointSize(14);
	h2.setForeground(QBrush(QColor(0,0,0,255), Qt::SolidPattern));
	QTextBlockFormat h2p;
	h2p.setLeftMargin(10);

	QTextCharFormat h3;
	h3.setFontFamily("Arial");
	h3.setFontPointSize(12);
	h3.setForeground(QBrush(QColor(0,0,0,255), Qt::SolidPattern));
	QTextBlockFormat h3p;
	h3p.setLeftMargin(10);

	QTextCursor cursor(&m_TextDoc);
	QString str;

	// Summary
	str = "Pet number " + numDet.expression;
	str+="\r\n";

	str += "Pet Birthday Code " + numDet.birthDay;
	str +="\r\n";

	str += "Forecast Day Code " + numDet.persYear;
	str +="\r\n";


	cursor.beginEditBlock();
	cursor.setBlockFormat(h1p);
	cursor.setCharFormat(h1);
	cursor.insertText("Pet Numerology Chart");
	cursor.insertText("\r\n");
	cursor.endEditBlock();

	cursor.beginEditBlock();
	cursor.setBlockFormat(h3p);
	cursor.setCharFormat(h3);
	cursor.insertText(str);
	cursor.insertText("\r\n\r\n");
	cursor.endEditBlock();



	// Details
	cursor.beginEditBlock();
	cursor.setBlockFormat(h1p);
	cursor.setCharFormat(h1);
	cursor.insertText("Pet number " + numDet.expression);
	cursor.insertText("\r\n");
	cursor.endEditBlock();

	cursor.beginEditBlock();
	cursor.setBlockFormat(h3p);
	cursor.setCharFormat(h3);
	cursor.insertText(numerPetC[0]);
	cursor.insertText("\r\n");
	cursor.insertText(numerPetC[m_expression]);
	cursor.insertText("\r\n");
	cursor.endEditBlock();


	cursor.beginEditBlock();
	cursor.setBlockFormat(h1p);
	cursor.setCharFormat(h1);
	cursor.insertText("Day Forecast for " + cdate.toString());
	cursor.insertText("\r\n");
	cursor.endEditBlock();

	cursor.beginEditBlock();
	cursor.setBlockFormat(h3p);
	cursor.setCharFormat(h3);
	cursor.insertText(numerPetDForecast[fdcode]);
	cursor.insertText("\r\n");
	cursor.endEditBlock();


	return true;
}


void CPetNumerology::draw(QPainter *pPntr)
{
	if (m_LifePath <= 0)
		return;

	m_TextDoc.drawContents(pPntr);
}
