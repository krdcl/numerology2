#include "vbnum.h"


QString unaccent(const QString s)
{
    QString s2 = s.normalized(QString::NormalizationForm_D);
    QString out1, out;
    for (int i=0,j=s2.length(); i<j; i++)
    {
        // strip diacritic marks
        if (s2.at(i).category()!=QChar::Mark_NonSpacing &&
                s2.at(i).category()!=QChar::Mark_SpacingCombining)
        {
            out1.append(s2.at(i));
        }
    }
    for (int a=0; a<out1.size(); a++)
    {
        QChar c = out1.at(a);
        if (((c>='a') && (c<='z')) || ((c>='A') && (c<='Z')) || ((c>='0') && (c<='9'))) {
            out.append(c);
        } else if (c=='\'') {
        } else {
            out.append(' ');
        }
    }
    return out;
}


void CVBNum::OnResData()
{
    m_pFullNameEdit->setText("");
    m_pShortNameEdit->setText("");

    m_pBDate->setDate(QDate(2000,1,1));
    m_pSex->setCurrentIndex(0);
    m_uCNC->setChecked(false);
    m_pPFullNameEdit->setText("");
    m_pPShortNameEdit->setText("");
    m_pPBDate->setDate(QDate(2000,1,1));


    m_pInfoName->parList.clear();
    for (int a=0; a<6; a++)
        m_pInfoName->parList.append("");
    m_pInfoName->parListVal.clear();
    for (int a=0; a<6; a++)
        m_pInfoName->parListVal.append("");
    m_pInfoNameNS->parList.clear();
    for (int a=0; a<6; a++)
        m_pInfoNameNS->parList.append("");
    m_pInfoNameNS->parListVal.clear();
    for (int a=0; a<6; a++)
        m_pInfoNameNS->parListVal.append("");

    m_pInfoE->parList.clear();
    for (int a=0; a<7; a++)
        m_pInfoE->parList.append("");
    m_pInfoE->parListVal.clear();
    for (int a=0; a<7; a++)
        m_pInfoE->parListVal.append("");
    m_pInfoE->updateData();

    for (int a=0; a<3; a++)
        m_pInfoCal->parListVal.append("");
    m_pInfoCal->m_PersYear = 0;


    m_pInfoNamePNS->parList.clear();
    for (int a=0; a<6; a++)
        m_pInfoNamePNS->parList.append("");
    m_pInfoNamePNS->parListVal.clear();
    for (int a=0; a<6; a++)
        m_pInfoNamePNS->parListVal.append("");


    m_pInfoBR->parListVal.clear();
    for (int a=0; a<2; a++)
        m_pInfoBR->parListVal.append("");

    m_pInfoAF->parListVal.clear();
    for (int a=0; a<6; a++)
        m_pInfoAF->parListVal.append("");

    m_pInfoLC->parListVal.clear();
    for (int a=0; a<6; a++)
        m_pInfoLC->parListVal.append("");

    m_pInfoMF->parListVal.clear();
    for (int a=0; a<6; a++)
        m_pInfoMF->parListVal.append("");
    m_pInfoMFNS->parListVal.clear();
    for (int a=0; a<6; a++)
        m_pInfoMFNS->parListVal.append("");
    m_pInfoPMFNS->parListVal.clear();
    for (int a=0; a<6; a++)
        m_pInfoPMFNS->parListVal.append("");

    m_pInfoPE->parListVal.clear();
    for (int a=0; a<20; a++)
        m_pInfoPE->parListVal.append("");

    for (int a=0; a<17; a++)
    {
        m_pInfoPM->pmatr[a] = 0;
        m_pInfoCompPM->pmatr[a] = 0;
        m_pInfoCompPM->pmatr2[a] = 0;
    }

    m_pInfoCMF->parListVal.clear();
    for (int a=0; a<5; a++)
        m_pInfoCMF->parListVal.append("");

    personalNumCalendar->parListVal.clear();
    for (int a=0; a<3; a++)
        personalNumCalendar->parListVal.append("");

    m_pInfoCFS->parListVal.clear();
    for (int a=0; a<7; a++)
        m_pInfoCFS->parListVal.append("");

    m_pInfoCLP->parListVal.clear();
    for (int a=0; a<3; a++)
        m_pInfoCLP->parListVal.append("");

    m_pInfoNamePet->parList.clear();
    for (int a=0; a<3; a++)
        m_pInfoNamePet->parList.append("");
    m_pInfoNamePet->parListVal.clear();
    for (int a=0; a<3; a++)
        m_pInfoNamePet->parListVal.append("");

    m_pInfoMFPet->parListVal.clear();
    for (int a=0; a<3; a++)
        m_pInfoMFPet->parListVal.append("");

    //IC
    m_pInfoAnMF->parListVal.clear();
    for (int a = 0; a < 5; a++)
        m_pInfoAnMF->parListVal.append("");

    m_pInfoAnName->parList.clear();
    for (int a = 0; a < 3; a++)
        m_pInfoAnName->parList.append("");
    m_pInfoAnName->parListVal.clear();
    for (int a = 0; a < 3; a++)
        m_pInfoAnName->parListVal.append("");
    //*IC

    m_pNumerPet->m_TextDoc.clear();
    m_pNumerPMatrComp->m_TextDoc.clear();
    m_pNumerPMatr->m_TextDoc.clear();
    m_pCompNumerology->resetData();
    m_pChartNumerology->resetData();
    m_pNumerAn->m_TextDoc.clear();

//    unsigned int xW = topLevelLabel->width();
//    unsigned int yH = topLevelLabel->height();
//    topLevelLabel->setPixmap(QPixmap::fromImage(m_pChartNumerology->numChart).scaled(xW, yH, Qt::KeepAspectRatio, Qt::SmoothTransformation));

    //buttonRun = "ENTER YOUR NAME";
    m_pText->setHtml("<html><body><p>&nbsp;</p></body></html>", QUrl());
    m_pTextCel->setHtml("<html><body><p>&nbsp;</p></body></html>", QUrl());
    m_pTextPet->setHtml("<html><body><p>&nbsp;</p></body></html>", QUrl());
    m_pTextComp->setHtml("<html><body><p>&nbsp;</p></body></html>", QUrl());
    m_pTextPMatr->setHtml("<html><body><p>&nbsp;</p></body></html>", QUrl());
    m_pTextAn->setHtml("<html><body><p>&nbsp;</p></body></html>", QUrl());
	m_pTextGuide->setHtml("<html><body><p>&nbsp;</p></body></html>", QUrl());

    m_pPetName->setText("");
    m_pPetBDate->setDate(QDate(2000,1,1));

    m_pTitleEdit->setText("");
    m_pAnDate->setDate(QDate(2000,1,1));

    buttonRun = "ENTER YOUR NAME";
    m_pSelPersBut->setText(buttonRun);

    m_pBDateF->setDate(QDate::currentDate());
    expandPanel->infoLabel->setText("");
    slotPNC(m_pBDateF->date());
    repaint();
}


void CVBNum::OnResDataI()
{
	OnResData();
	buttonRun = "APPLY";
	m_pSelPersBut->setText(buttonRun);
}


void CVBNum::OnPreCalcNum()
{
    SUserInfo inf;
    bool res;

	inf.PMode = m_PMode->currentIndex();
	if ((inf.PMode<0) || (inf.PMode>1))
		inf.PMode=NUM_VOW_MODE_ENGLISH;
	inf.uCNC = m_uCNC->isChecked();
	m_pInfoName->m_bMNEna = !inf.uCNC;
	m_pInfoNameNS->m_bMNEna = m_pInfoName->m_bMNEna;
    inf.birthDate = m_pBDate->date();
    inf.fullName = m_pFullNameEdit->text();
    inf.shortName = m_pShortNameEdit->text();
    if (m_pSex->currentIndex()==0)
        inf.psex = true;
    else
        inf.psex = false;

	inf.fullName.truncate(300);
	inf.shortName.truncate(300);
    inf.fullName = unaccent(inf.fullName);
    inf.shortName = unaccent(inf.shortName);
    inf.fullName.remove(",");
    inf.shortName.remove(",");

    /*inf.partnerBirthDate = m_pPBDate->date();
    inf.partnerFullName = m_pPFullNameEdit->text();
    inf.partnerShortName = m_pPShortNameEdit->text();

    inf.petBirthDate = m_pPetBDate->date();
    inf.petName = m_pPetName->text();
    */

    {
        QStringList flist = inf.fullName.split(" ");
        QStringList slist = inf.shortName.split(" ");

        inf.fullName = "";
        for (int a=0; a<flist.size(); a++)
        {
            if (flist.at(a).size() > 0)
            {
                if (inf.fullName.size()>0)
                    inf.fullName += " ";
                inf.fullName += flist.at(a);
            }
        }

        inf.shortName = "";
        for (int a=0; a<slist.size(); a++)
        {
            if (slist.at(a).size() > 0)
            {
                if (inf.shortName.size()>0)
                    inf.shortName += " ";
                inf.shortName += slist.at(a);
            }
        }
        if (inf.shortName.size()==0)
        {
            flist = inf.fullName.split(" ");
            if (flist.size()>2)
                inf.shortName = flist.at(0) + " " + flist.at(flist.size()-1);
            else
                inf.shortName = inf.fullName;
        }
    }
    m_pNumer->setMasterNumbersMode(mmcalcTo99);

#ifdef VER_PREM
    res = m_pNumer->calculate(inf.fullName, inf.shortName, m_viFnvw, m_viSnvw, inf.uCNC, inf.birthDate, m_pBDateF->date(), true, &settingReport, m_pReportBox->currentIndex(), inf.PMode, true);
#else
    res = m_pNumer->calculate(inf.fullName, inf.shortName, m_viFnvw, m_viSnvw, inf.uCNC, inf.birthDate, m_pBDateF->date(), true, &settingReport, 0, inf.PMode, true);
#endif
    m_pInfoMFNS->parListVal[0] = m_pNumer->numDet.lifePath;
    m_pInfoMFNS->parListVal[1] = m_pNumer->numDet.karmicDebts;
    m_pInfoMFNS->parListVal[2] = m_pNumer->numDet.expression + " (" + m_pNumer->numDet.mexpression +")";
    m_pInfoMFNS->parListVal[3] = m_pNumer->numDet.heartDesire + " (" + m_pNumer->numDet.mheartDesire +")";
    m_pInfoMFNS->parListVal[4] = m_pNumer->numDet.personality + " (" + m_pNumer->numDet.mpersonality +")";
    m_pInfoMFNS->parListVal[5] = m_pNumer->numDet.karmicLessons;
	m_pInfoMFNS->parListVal[5].remove(' ');

    m_pInfoNameNS->parListVal[1] = m_pNumer->numDet.fnameR;
    m_pInfoNameNS->parListVal[0] = m_pNumer->numDet.fnameR1;
    m_pInfoNameNS->parListVal[2] = m_pNumer->numDet.fnameR2;
    m_pInfoNameNS->parList[1] = inf.fullName;
    m_pInfoNameNS->parList[0] = m_pNumer->numDet.fnameN1;
    m_pInfoNameNS->parList[2] = m_pNumer->numDet.fnameN2;

    m_pInfoNameNS->parListVal[4] = m_pNumer->numDet.snameR;
    m_pInfoNameNS->parListVal[3] = m_pNumer->numDet.snameR1;
    m_pInfoNameNS->parListVal[5] = m_pNumer->numDet.snameR2;
    m_pInfoNameNS->parList[4] = inf.shortName;
    m_pInfoNameNS->parList[3] = m_pNumer->numDet.snameN1;
    m_pInfoNameNS->parList[5] = m_pNumer->numDet.snameN2;

    m_pInfoMFNS->repaint();
    m_pInfoNameNS->repaint();
}



void CVBNum::OnPreCalcNumP()
{
    SUserInfo inf;
    bool res;

	inf.PMode = m_PMode->currentIndex();
	if ((inf.PMode<0) || (inf.PMode>1))
		inf.PMode=NUM_VOW_MODE_ENGLISH;
	inf.uCNC = m_uCNC->isChecked();
	m_pInfoName->m_bMNEna = !inf.uCNC;
	m_pInfoNameNS->m_bMNEna = m_pInfoName->m_bMNEna;
    inf.birthDate = m_pPBDate->date();
    inf.fullName = m_pPFullNameEdit->text();
    inf.shortName = m_pPShortNameEdit->text();
    if (m_pSex->currentIndex()==0)
        inf.psex = false;
    else
        inf.psex = true;

	inf.fullName.truncate(300);
	inf.shortName.truncate(300);
    inf.fullName = unaccent(inf.fullName);
    inf.shortName = unaccent(inf.shortName);
    inf.fullName.remove(",");
    inf.shortName.remove(",");

    /*inf.partnerBirthDate = m_pPBDate->date();
    inf.partnerFullName = m_pPFullNameEdit->text();
    inf.partnerShortName = m_pPShortNameEdit->text();

    inf.petBirthDate = m_pPetBDate->date();
    inf.petName = m_pPetName->text();
    */

    {
        QStringList flist = inf.fullName.split(" ");
        QStringList slist = inf.shortName.split(" ");

        inf.fullName = "";
        for (int a=0; a<flist.size(); a++)
        {
            if (flist.at(a).size() > 0)
            {
                if (inf.fullName.size()>0)
                    inf.fullName += " ";
                inf.fullName += flist.at(a);
            }
        }

        inf.shortName = "";
        for (int a=0; a<slist.size(); a++)
        {
            if (slist.at(a).size() > 0)
            {
                if (inf.shortName.size()>0)
                    inf.shortName += " ";
                inf.shortName += slist.at(a);
            }
        }
        if (inf.shortName.size()==0)
        {
            flist = inf.fullName.split(" ");
            if (flist.size()>2)
                inf.shortName = flist.at(0) + " " + flist.at(flist.size()-1);
            else
                inf.shortName = inf.fullName;
        }
    }
    m_pNumerComp->setMasterNumbersMode(mmcalcTo99);
#ifdef VER_PREM
    res = m_pNumerComp->calculate(inf.fullName, inf.shortName, m_viPFnvw, m_viPSnvw, inf.uCNC, inf.birthDate, m_pBDateF->date(), true, &settingReport, m_pReportBox->currentIndex(), inf.PMode, true);
#else
    res = m_pNumerComp->calculate(inf.fullName, inf.shortName, m_viPFnvw, m_viPSnvw, inf.uCNC, inf.birthDate, m_pBDateF->date(), true, &settingReport, 0, inf.PMode, true);
#endif
//    QMessageBox msgBox;
//    msgBox.setText("Hello Here");
//    msgBox.exec();

    m_pInfoPMFNS->parListVal[0] = m_pNumerComp->numDet.lifePath;
    m_pInfoPMFNS->parListVal[1] = m_pNumerComp->numDet.karmicDebts;
    m_pInfoPMFNS->parListVal[2] = m_pNumerComp->numDet.expression + " (" + m_pNumerComp->numDet.mexpression +")";
    m_pInfoPMFNS->parListVal[3] = m_pNumerComp->numDet.heartDesire + " (" + m_pNumerComp->numDet.mheartDesire +")";
    m_pInfoPMFNS->parListVal[4] = m_pNumerComp->numDet.personality + " (" + m_pNumerComp->numDet.mpersonality +")";
    m_pInfoPMFNS->parListVal[5] = m_pNumerComp->numDet.karmicLessons;
	m_pInfoPMFNS->parListVal[5].remove(' ');

    m_pInfoNamePNS->parListVal[1] = m_pNumerComp->numDet.fnameR;
    m_pInfoNamePNS->parListVal[0] = m_pNumerComp->numDet.fnameR1;
    m_pInfoNamePNS->parListVal[2] = m_pNumerComp->numDet.fnameR2;
    m_pInfoNamePNS->parList[1] = inf.fullName;
    m_pInfoNamePNS->parList[0] = m_pNumerComp->numDet.fnameN1;
    m_pInfoNamePNS->parList[2] = m_pNumerComp->numDet.fnameN2;

    m_pInfoNamePNS->parListVal[4] = m_pNumerComp->numDet.snameR;
    m_pInfoNamePNS->parListVal[3] = m_pNumerComp->numDet.snameR1;
    m_pInfoNamePNS->parListVal[5] = m_pNumerComp->numDet.snameR2;
    m_pInfoNamePNS->parList[4] = inf.shortName;
    m_pInfoNamePNS->parList[3] = m_pNumerComp->numDet.snameN1;
    m_pInfoNamePNS->parList[5] = m_pNumerComp->numDet.snameN2;

    m_pInfoPMFNS->repaint();
    m_pInfoNamePNS->repaint();
}



void CVBNum::OnCalcNum()
{
    checkCalculateData = false;
    SUserInfo inf;
    /*QString str = m_pNameCombo->currentText();
    for (int a=0; a<users.size(); a++)
    {
        if (users[a].entryText == str)
        {
            inf = users[a];
            break;
        }
    }*/

	inf.PMode = m_PMode->currentIndex();
	if ((inf.PMode<0) || (inf.PMode>1))
		inf.PMode=NUM_VOW_MODE_ENGLISH;
    inf.uCNC = m_uCNC->isChecked();
	m_pInfoName->m_bMNEna = !inf.uCNC;
	m_pInfoNameNS->m_bMNEna = m_pInfoName->m_bMNEna;
    inf.birthDate = m_pBDate->date();
    inf.fullName = m_pFullNameEdit->text();
    inf.shortName = m_pShortNameEdit->text();
    if (m_pSex->currentIndex()==0)
        inf.psex = true;
    else
        inf.psex = false;

    inf.partnerBirthDate = m_pPBDate->date();
    inf.partnerFullName = m_pPFullNameEdit->text();
    inf.partnerShortName = m_pPShortNameEdit->text();

    inf.petBirthDate = m_pPetBDate->date();
    inf.petName = m_pPetName->text();


	inf.fullName.truncate(300);
	inf.shortName.truncate(300);
	inf.partnerFullName.truncate(300);
	inf.partnerShortName.truncate(300);
	inf.petName.truncate(300);
    inf.fullName = unaccent(inf.fullName);
    inf.shortName = unaccent(inf.shortName);
    inf.partnerFullName = unaccent(inf.partnerFullName);
    inf.partnerShortName = unaccent(inf.partnerShortName);
    inf.petName = unaccent(inf.petName);
    inf.fullName.remove(",");
    inf.shortName.remove(",");
    inf.partnerFullName.remove(",");
    inf.partnerShortName.remove(",");
    inf.petName.remove(",");

    {
        QStringList flist = inf.fullName.split(" ");
        QStringList slist = inf.shortName.split(" ");

        inf.fullName = "";
        for (int a=0; a<flist.size(); a++)
        {
            if (flist.at(a).size() > 0)
            {
                if (inf.fullName.size()>0)
                    inf.fullName += " ";
                inf.fullName += flist.at(a);
            }
        }

        inf.shortName = "";
        for (int a=0; a<slist.size(); a++)
        {
            if (slist.at(a).size() > 0)
            {
                if (inf.shortName.size()>0)
                    inf.shortName += " ";
                inf.shortName += slist.at(a);
            }
        }
        if (inf.shortName.size()==0)
        {
            flist = inf.fullName.split(" ");
            if (flist.size()>2)
                inf.shortName = flist.at(0) + " " + flist.at(flist.size()-1);
            else
                inf.shortName = inf.fullName;
        }
    }


    {
        QStringList flist = inf.partnerFullName.split(" ");
        QStringList slist = inf.partnerShortName.split(" ");

        inf.partnerFullName = "";
        for (int a=0; a<flist.size(); a++)
        {
            if (flist.at(a).size() > 0)
            {
                if (inf.partnerFullName.size()>0)
                    inf.partnerFullName += " ";
                inf.partnerFullName += flist.at(a);
            }
        }

        inf.partnerShortName = "";
        for (int a=0; a<slist.size(); a++)
        {
            if (slist.at(a).size() > 0)
            {
                if (inf.partnerShortName.size()>0)
                    inf.partnerShortName += " ";
                inf.partnerShortName += slist.at(a);
            }
        }
        if (inf.partnerShortName.size()==0)
        {
            flist = inf.partnerFullName.split(" ");
            if (flist.size()>2)
                inf.partnerShortName = flist.at(0) + " " + flist.at(flist.size()-1);
            else
                inf.partnerShortName = inf.partnerFullName;
        }
    }


    /*if (users.size()==0)
    {
        QString Msg = tr("Error! Use Add User Button or Menu Command before calculation!");
        QMessageBox::StandardButton Res;
        Res = QMessageBox::critical(this, tr(""), Msg, QMessageBox::Ok);

        OnAddUser();
        return;
    }*/

    if ((inf.fullName.size()==0) || (inf.shortName.size()==0))
    {
        //QString Msg = tr("Enter your data before chart calculation!");
        //QMessageBox::StandardButton Res;
        //Res = QMessageBox::critical(this, tr(""), Msg, QMessageBox::Ok);

        //OnResData();
        return;
    }

    /*CCalcForm dlg(&m_Res.formbackground, &m_Res.formbackshape, this);
    dlg.setResources(&m_Res.butOk, &m_Res.butCan);

    int x = (this->width()-dlg.width())/2 + this->x();
    if (x<0)
        x=0;

    int y = (this->height()-dlg.height())/2 + this->y();
    if (y<0)
        y=0;

    dlg.move(x, y);

    dlg.m_pBDate->setDate(QDate::currentDate());*/

	bool res = false, res2 = false, res3 = false, res4 = false;

    {
        m_pNumerPet->resetData();
        m_pNumerPMatrComp->resetData();
        m_pNumerPMatr->resetData();
        m_pCompNumerology->resetData();
        m_pChartNumerology->resetData();
        m_pNumerCel->resetData();

        m_pNumer->setMasterNumbersMode(mmcalcTo99);
        m_pNumerComp->setMasterNumbersMode(mmcalcTo99);

        //if (IntelliProtectorService::CIntelliProtector::GetLicenseType()==-1)
        if (!getStatus())
        {
            /*cnum++;

            if ((cnum%4) == 0)
            {
                inf.fullName = "Albert Einstein";
                inf.shortName = "Einstein";
                inf.birthDate = QDate(1879, 3, 14);
            } else if ((cnum%4) == 1)
            {
                inf.fullName = "Napoleon Bonaparte";
                inf.shortName = "Napoleon";
                inf.birthDate = QDate(1769, 8, 15);
            } else if ((cnum%4) == 2)
            {
                inf.fullName = "Alfred Bernhard Nobel";
                inf.shortName = "Alfred Nobel";
                inf.birthDate = QDate(1833, 10, 21);
            } else
            {
                inf.fullName = "Benjamin Franklin";
                inf.shortName = "Franklin";
                inf.birthDate = QDate(1706, 1, 17);
            }

            res = m_pNumer->calculate(inf.fullName, inf.shortName, inf.birthDate, m_pBDateF->date(), true);*/
#ifdef VER_PREM
            res = m_pNumer->calculate(inf.fullName, inf.shortName, m_viFnvw, m_viSnvw, inf.uCNC, inf.birthDate, m_pBDateF->date(), true, &settingReport, m_pReportBox->currentIndex(), inf.PMode);
#else
            res = m_pNumer->calculate(inf.fullName, inf.shortName, m_viFnvw, m_viSnvw, inf.uCNC, inf.birthDate, m_pBDateF->date(), true, &settingReport, 0, inf.PMode);
#endif
            if (res)
            {
#ifdef VER_PREM
                m_pNumerCel->calculateCel(inf.fullName, inf.birthDate, inf.uCNC, true, &settingReport, m_pReportBox->currentIndex(), inf.PMode);
#else
                m_pNumerCel->calculateCel(inf.fullName, inf.birthDate, inf.uCNC, true, &settingReport, 0, inf.PMode);
#endif
            }
#ifdef VER_PREM
            res2 = m_pNumerPMatr->calculate(inf.fullName, inf.birthDate, true, &settingReport, m_pReportBox->currentIndex());
#else
            res2 = m_pNumerPMatr->calculate(inf.fullName, inf.birthDate, true, &settingReport, 0);
#endif
            if (inf.petName.size() != 0)
            {
#ifdef VER_PREM
                res3 = m_pNumerPet->calculatePet(inf.petName, "", inf.petBirthDate, m_pBDateF->date(), true, &settingReport, m_pReportBox->currentIndex());
#else
                res3 = m_pNumerPet->calculatePet(inf.petName, "", inf.petBirthDate, m_pBDateF->date(), true, &settingReport, 0);
#endif
            }
            else
            {
                m_pNumerPet->resetData();
            }

            if ((inf.partnerFullName.size()>0) && (inf.partnerShortName.size()>0) && res2)
            {
#ifdef VER_PREM
                bool cres1 = m_pNumerComp->calculate(inf.partnerFullName, inf.partnerShortName, m_viPFnvw, m_viPSnvw, inf.uCNC, inf.partnerBirthDate, m_pBDateF->date(), true, &settingReport, m_pReportBox->currentIndex(), inf.PMode);
                bool cres2 = m_pNumerPMatrComp->calculate(inf.partnerFullName, inf.partnerBirthDate, true, &settingReport, m_pReportBox->currentIndex());
#else
                bool cres1 = m_pNumerComp->calculate(inf.partnerFullName, inf.partnerShortName, m_viPFnvw, m_viPSnvw, inf.uCNC, inf.partnerBirthDate, m_pBDateF->date(), true, &settingReport, 0, inf.PMode);
                bool cres2 = m_pNumerPMatrComp->calculate(inf.partnerFullName, inf.partnerBirthDate, true, &settingReport, 0);
#endif
				//qDebug() << "1:" << cres1 << ", 2:" << cres2;
                if (cres1 && cres2)
                {
#ifdef VER_PREM
                    res4 = m_pCompNumerology->calculate(m_pNumer, m_pNumerComp, m_pNumerPMatr, m_pNumerPMatrComp, inf.psex, true, &settingReport, m_pReportBox->currentIndex());
#else
                    res4 = m_pCompNumerology->calculate(m_pNumer, m_pNumerComp, m_pNumerPMatr, m_pNumerPMatrComp, inf.psex, true, &settingReport, 0);
#endif
					//qDebug() << "4:" << res4;
                }
            } else
            {
                m_pCompNumerology->resetData();
            }

            if (res && res2)
            {
                checkCalculateData = true;
            }
            else
                m_pChartNumerology->resetData();
        }
        else
        {
#ifdef VER_PREM
            res = m_pNumer->calculate(inf.fullName, inf.shortName, m_viFnvw, m_viSnvw, inf.uCNC, inf.birthDate, m_pBDateF->date(), false, &settingReport, m_pReportBox->currentIndex(), inf.PMode);
#else
            res = m_pNumer->calculate(inf.fullName, inf.shortName, m_viFnvw, m_viSnvw, inf.uCNC, inf.birthDate, m_pBDateF->date(), false, &settingReport, 0, inf.PMode);
#endif
            if (res)
            {
#ifdef VER_PREM
                m_pNumerCel->calculateCel(inf.fullName, inf.birthDate, inf.uCNC, false, &settingReport, m_pReportBox->currentIndex(), inf.PMode);
#else
                m_pNumerCel->calculateCel(inf.fullName, inf.birthDate, inf.uCNC, false, &settingReport, 0, inf.PMode);
#endif
            }
#ifdef VER_PREM
            res2 = m_pNumerPMatr->calculate(inf.fullName, inf.birthDate, false, &settingReport, m_pReportBox->currentIndex());
#else
            res2 = m_pNumerPMatr->calculate(inf.fullName, inf.birthDate, false, &settingReport, 0);
#endif
            if (inf.petName.size() != 0)
            {
#ifdef VER_PREM
                res3 = m_pNumerPet->calculatePet(inf.petName, "", inf.petBirthDate, m_pBDateF->date(), false, &settingReport, m_pReportBox->currentIndex());
#else
                res3 = m_pNumerPet->calculatePet(inf.petName, "", inf.petBirthDate, m_pBDateF->date(), false, &settingReport, 0);
#endif
            }
            else
                m_pNumerPet->resetData();

            if ((inf.partnerFullName.size()>0) && (inf.partnerShortName.size()>0) && res2)
            {
#ifdef VER_PREM
                bool cres1 = m_pNumerComp->calculate(inf.partnerFullName, inf.partnerShortName, m_viPFnvw, m_viPSnvw, inf.uCNC, inf.partnerBirthDate, m_pBDateF->date(), false, &settingReport, m_pReportBox->currentIndex(), inf.PMode);
                bool cres2 = m_pNumerPMatrComp->calculate(inf.partnerFullName, inf.partnerBirthDate, false, &settingReport, m_pReportBox->currentIndex());

                if (cres1 && cres2)
                    res4 = m_pCompNumerology->calculate(m_pNumer, m_pNumerComp, m_pNumerPMatr, m_pNumerPMatrComp, inf.psex, false, &settingReport, m_pReportBox->currentIndex());
#else
                bool cres1 = m_pNumerComp->calculate(inf.partnerFullName, inf.partnerShortName, m_viPFnvw, m_viPSnvw, inf.uCNC, inf.partnerBirthDate, m_pBDateF->date(), false, &settingReport, 0, inf.PMode);
                bool cres2 = m_pNumerPMatrComp->calculate(inf.partnerFullName, inf.partnerBirthDate, false, &settingReport, 0);

                if (cres1 && cres2)
                    res4 = m_pCompNumerology->calculate(m_pNumer, m_pNumerComp, m_pNumerPMatr, m_pNumerPMatrComp, inf.psex, false, &settingReport, 0);
#endif
            } else
            {
                m_pCompNumerology->resetData();
            }

            if (res && res2)
            {
                checkCalculateData = true;
            }
            else
                m_pChartNumerology->resetData();
        }



//        if (res && res2)
//            m_pChartNumerology->calculate(m_pNumer, m_pNumerComp, m_pNumerPMatr, m_pNumerPMatrComp, inf.psex, true);
//        //else
//            //m_pChartNumerology->resetData();
        if (res && res2)
        {

            //OnResData();

            //m_pInfoCal->m_PersYear = m_pNumer->m_PersYear; !!!!!!!!!!!!!!!!!!!!!!!!!!!
            //m_pInfoCal->cdate = m_pNumer->m_cdate;!!!!!!!!!!!!!!!!!!!!!!!!!

            m_pInfoMF->parListVal[0] = m_pNumer->numDet.lifePath;
            m_pInfoMF->parListVal[1] = m_pNumer->numDet.birthDay;
            m_pInfoMF->parListVal[2] = m_pNumer->numDet.expression + " (" + m_pNumer->numDet.mexpression +")";
            m_pInfoMF->parListVal[3] = m_pNumer->numDet.heartDesire + " (" + m_pNumer->numDet.mheartDesire +")";
            m_pInfoMF->parListVal[4] = m_pNumer->numDet.personality + " (" + m_pNumer->numDet.mpersonality +")";
            m_pInfoMF->parListVal[5] = m_pNumer->numDet.karmicDebts;

            m_pInfoAF->parListVal[0] = m_pNumer->numDet.maturity;
            m_pInfoAF->parListVal[1] = m_pNumer->numDet.rationalThought;
            m_pInfoAF->parListVal[2] = m_pNumer->numDet.karmicLessons;
			m_pInfoAF->parListVal[2].remove(' ');
            m_pInfoAF->parListVal[3] = m_pNumer->numDet.balance;
            m_pInfoAF->parListVal[4] = m_pNumer->numDet.hiddenPassion;
            m_pInfoAF->parListVal[5] = m_pNumer->numDet.subconsciousConfidence;

            m_pInfoLC->parListVal[0] = m_pNumer->numDet.ageSpan;
            m_pInfoLC->parListVal[1] = m_pNumer->numDet.challenges;
            m_pInfoLC->parListVal[2] = m_pNumer->numDet.pinnacles;
            m_pInfoLC->parListVal[3] = m_pNumer->numDet.persYear;
            m_pInfoLC->parListVal[4] = m_pNumer->numDet.lpCycles;
            m_pInfoLC->parListVal[5] = m_pNumer->numDet.lpAgeSpan;

            m_pInfoBR->parListVal[0] = m_pNumer->numDet.brLPE;
            m_pInfoBR->parListVal[1] = m_pNumer->numDet.brHDP;


            m_pInfoName->parListVal[1] = m_pNumer->numDet.fnameR;
            m_pInfoName->parListVal[0] = m_pNumer->numDet.fnameR1;
            m_pInfoName->parListVal[2] = m_pNumer->numDet.fnameR2;
            m_pInfoName->parList[1] = inf.fullName;
            m_pInfoName->parList[0] = m_pNumer->numDet.fnameN1;
            m_pInfoName->parList[2] = m_pNumer->numDet.fnameN2;

            m_pInfoName->parListVal[4] = m_pNumer->numDet.snameR;
            m_pInfoName->parListVal[3] = m_pNumer->numDet.snameR1;
            m_pInfoName->parListVal[5] = m_pNumer->numDet.snameR2;
            m_pInfoName->parList[4] = inf.shortName;
            m_pInfoName->parList[3] = m_pNumer->numDet.snameN1;
            m_pInfoName->parList[5] = m_pNumer->numDet.snameN2;

            for (int a=0; a<20; a++)
                m_pInfoPE->parListVal[a] = m_pNumer->numDet.PERep[a];

            m_pInfoE->parList[0] = m_pNumer->m_sTransit1;
            m_pInfoE->parList[1] = m_pNumer->m_sTransit2;
            m_pInfoE->parList[2] = m_pNumer->m_sTransit3;
            for (int a=0; a<128; a++)
            {
                m_pInfoE->m_Essence[a] = m_pNumer->m_Essence[a];
                m_pInfoE->m_EssenceS[a] = m_pNumer->m_EssenceS[a];
            }
			m_pInfoE->m_byear = m_pNumer->m_bdate.year();
            m_pInfoE->updateData();

            m_pInfoCal->m_PersYear = m_pNumer->m_PersYear;
            m_pInfoCal->cdate = m_pNumer->m_cdate;
            slotPNC(m_pNumer->m_cdate);


            // PMatrix
            //m_pNumerPMatr
            m_pInfoPM->pmatr[0] = m_pNumerPMatr->m_ResMatr[1];
            m_pInfoPM->pmatr[1] = m_pNumerPMatr->m_ResMatr[4];
            m_pInfoPM->pmatr[2] = m_pNumerPMatr->m_ResMatr[7];
            m_pInfoPM->pmatr[3] = m_pNumerPMatr->m_SumV[0];

            m_pInfoPM->pmatr[4] = m_pNumerPMatr->m_ResMatr[2];
            m_pInfoPM->pmatr[5] = m_pNumerPMatr->m_ResMatr[5];
            m_pInfoPM->pmatr[6] = m_pNumerPMatr->m_ResMatr[8];
            m_pInfoPM->pmatr[7] = m_pNumerPMatr->m_SumV[1];

            m_pInfoPM->pmatr[8] = m_pNumerPMatr->m_ResMatr[3];
            m_pInfoPM->pmatr[9] = m_pNumerPMatr->m_ResMatr[6];
            m_pInfoPM->pmatr[10] = m_pNumerPMatr->m_ResMatr[9];
            m_pInfoPM->pmatr[11] = m_pNumerPMatr->m_SumV[2];

            m_pInfoPM->pmatr[12] = m_pNumerPMatr->m_SumH[0];
            m_pInfoPM->pmatr[13] = m_pNumerPMatr->m_SumH[1];
            m_pInfoPM->pmatr[14] = m_pNumerPMatr->m_SumH[2];

            m_pInfoPM->pmatr[15] = m_pNumerPMatr->m_SumHV[0];
            m_pInfoPM->pmatr[16] = m_pNumerPMatr->m_SumHV[1];

            // Pet
            if (res3)
            {
                m_pInfoMFPet->parListVal[0] = m_pNumerPet->numDet.expression;
                m_pInfoMFPet->parListVal[1] = m_pNumerPet->numDet.birthDay;
                m_pInfoMFPet->parListVal[2] = m_pNumerPet->numDet.persYear;

                m_pInfoNamePet->parListVal[1] = m_pNumerPet->numDet.fnameR;
                m_pInfoNamePet->parListVal[0] = m_pNumerPet->numDet.fnameR1;
                m_pInfoNamePet->parListVal[2] = m_pNumerPet->numDet.fnameR2;
                m_pInfoNamePet->parList[1] = inf.petName;
                m_pInfoNamePet->parList[0] = m_pNumerPet->numDet.fnameN1;
                m_pInfoNamePet->parList[2] = m_pNumerPet->numDet.fnameN2;
            }

            // Comp
            if (res4)
            {
                m_pInfoCFS->parListVal[0] = m_pCompNumerology->numDet.oss;
                m_pInfoCFS->parListVal[1] = m_pCompNumerology->numDet.bsm;
                m_pInfoCFS->parListVal[2] = m_pCompNumerology->numDet.dsm;
                m_pInfoCFS->parListVal[3] = m_pCompNumerology->numDet.bsf;
                m_pInfoCFS->parListVal[4] = m_pCompNumerology->numDet.dsf;
                m_pInfoCFS->parListVal[5] = m_pCompNumerology->numDet.bss;
                m_pInfoCFS->parListVal[6] = m_pCompNumerology->numDet.dss;


                m_pInfoCompPM->pmatr[0] = m_pNumerPMatr->m_ResMatr[1];
                m_pInfoCompPM->pmatr[1] = m_pNumerPMatr->m_ResMatr[4];
                m_pInfoCompPM->pmatr[2] = m_pNumerPMatr->m_ResMatr[7];
                m_pInfoCompPM->pmatr[3] = m_pNumerPMatr->m_SumV[0];

                m_pInfoCompPM->pmatr[4] = m_pNumerPMatr->m_ResMatr[2];
                m_pInfoCompPM->pmatr[5] = m_pNumerPMatr->m_ResMatr[5];
                m_pInfoCompPM->pmatr[6] = m_pNumerPMatr->m_ResMatr[8];
                m_pInfoCompPM->pmatr[7] = m_pNumerPMatr->m_SumV[1];

                m_pInfoCompPM->pmatr[8] = m_pNumerPMatr->m_ResMatr[3];
                m_pInfoCompPM->pmatr[9] = m_pNumerPMatr->m_ResMatr[6];
                m_pInfoCompPM->pmatr[10] = m_pNumerPMatr->m_ResMatr[9];
                m_pInfoCompPM->pmatr[11] = m_pNumerPMatr->m_SumV[2];

                m_pInfoCompPM->pmatr[12] = m_pNumerPMatr->m_SumH[0];
                m_pInfoCompPM->pmatr[13] = m_pNumerPMatr->m_SumH[1];
                m_pInfoCompPM->pmatr[14] = m_pNumerPMatr->m_SumH[2];

                m_pInfoCompPM->pmatr[15] = m_pNumerPMatr->m_SumHV[0];
                m_pInfoCompPM->pmatr[16] = m_pNumerPMatr->m_SumHV[1];


                m_pInfoCompPM->pmatr2[0] = m_pNumerPMatrComp->m_ResMatr[1];
                m_pInfoCompPM->pmatr2[1] = m_pNumerPMatrComp->m_ResMatr[4];
                m_pInfoCompPM->pmatr2[2] = m_pNumerPMatrComp->m_ResMatr[7];
                m_pInfoCompPM->pmatr2[3] = m_pNumerPMatrComp->m_SumV[0];

                m_pInfoCompPM->pmatr2[4] = m_pNumerPMatrComp->m_ResMatr[2];
                m_pInfoCompPM->pmatr2[5] = m_pNumerPMatrComp->m_ResMatr[5];
                m_pInfoCompPM->pmatr2[6] = m_pNumerPMatrComp->m_ResMatr[8];
                m_pInfoCompPM->pmatr2[7] = m_pNumerPMatrComp->m_SumV[1];

                m_pInfoCompPM->pmatr2[8] = m_pNumerPMatrComp->m_ResMatr[3];
                m_pInfoCompPM->pmatr2[9] = m_pNumerPMatrComp->m_ResMatr[6];
                m_pInfoCompPM->pmatr2[10] = m_pNumerPMatrComp->m_ResMatr[9];
                m_pInfoCompPM->pmatr2[11] = m_pNumerPMatrComp->m_SumV[2];

                m_pInfoCompPM->pmatr2[12] = m_pNumerPMatrComp->m_SumH[0];
                m_pInfoCompPM->pmatr2[13] = m_pNumerPMatrComp->m_SumH[1];
                m_pInfoCompPM->pmatr2[14] = m_pNumerPMatrComp->m_SumH[2];

                m_pInfoCompPM->pmatr2[15] = m_pNumerPMatrComp->m_SumHV[0];
                m_pInfoCompPM->pmatr2[16] = m_pNumerPMatrComp->m_SumHV[1];


                m_pInfoCLP->parListVal[0] = m_pCompNumerology->numDet.LP;
                m_pInfoCLP->parListVal[1] = m_pCompNumerology->numDet.BCode;
                m_pInfoCLP->parListVal[2] = m_pCompNumerology->numDet.nameComp;


                m_pInfoCMF->parListVal[0] = m_pCompNumerology->numDet.sumComp;
                m_pInfoCMF->parListVal[1] = m_pCompNumerology->numDet.LPScore;
                m_pInfoCMF->parListVal[2] = m_pCompNumerology->numDet.BScore;
                m_pInfoCMF->parListVal[3] = m_pCompNumerology->numDet.famScore;
                m_pInfoCMF->parListVal[4] = m_pCompNumerology->numDet.nameScore;
            }
            //m_pChartLab->setPixmap(QPixmap::fromImage(m_pChartNumerology->numChart)); !!!!!!!!!!!!!!!!!!!!!!!!!!!

            if (res && res2)
            {
                inf.fnvw = m_viFnvw;
                inf.snvw = m_viSnvw;
                inf.pfnvw = m_viPFnvw;
                inf.psnvw = m_viPSnvw;
                bool newent = true;
                for (int a=0; a<m_pRecentEntries.size(); a++)
                {
                    SUserInfo re = m_pRecentEntries.at(a);
                    if ((re.fullName == inf.fullName) && (re.shortName == inf.shortName) && (re.birthDate == inf.birthDate)
                            && (re.partnerFullName == inf.partnerFullName) && (re.partnerShortName == inf.partnerShortName) && (re.partnerBirthDate == inf.partnerBirthDate))
                    {
                        // Updating recent order
                        //SUserInfo re = m_pRecentEntries.at(a);
                        m_pRecentEntries.remove(a);
                        m_pRecentEntries.insert(0, inf);

                        newent = false;
                        break;
                    }
                }

                if (newent)
                {
                    if (m_pRecentEntries.size()>=cRecentNum)
                        m_pRecentEntries.pop_back();
                    m_pRecentEntries.insert(0, inf);
                   // m_pSelPersBut->setText("ENTER YOUR NAME");

                }
                buttonRun = inf.shortName + "\n" + inf.birthDate.toString("MM/dd/yyyy");
                updateRecentList();
                //[self saveSettings];
            }

        } else
        {
            m_pChartNumerology->resetData();
            OnResData();
        }
    }
    /*else
    {
        OnResData();
    }*/

	if (res)
		m_pText->setHtml(m_pNumer->htmlDoc, QUrl("file://localhost/"));
	else
		m_pText->setHtml("<html><body><p>&nbsp;</p></body></html>", QUrl());

    if (res2)
        m_pTextPMatr->setHtml(m_pNumerPMatr->htmlDoc, QUrl("file://localhost/"));
    else
		m_pTextPMatr->setHtml("<html><body><p>&nbsp;</p></body></html>", QUrl());
	if (res)
	    m_pTextCel->setHtml(m_pNumerCel->htmlDoc, QUrl("file://localhost/"));
	else
		m_pTextCel->setHtml("<html><body><p>&nbsp;</p></body></html>", QUrl());

	if (res3)
	    m_pTextPet->setHtml(m_pNumerPet->htmlDoc, QUrl("file://localhost/"));
	else
		m_pTextPet->setHtml("<html><body><p>&nbsp;</p></body></html>", QUrl());

	if (res4)
		m_pTextComp->setHtml(m_pCompNumerology->htmlDoc, QUrl("file://localhost/"));
	else
		m_pTextComp->setHtml("<html><body><p>&nbsp;</p></body></html>", QUrl());

	m_pText->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal, Qt::ScrollBarAlwaysOff);
	m_pTextCel->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal, Qt::ScrollBarAlwaysOff);
	m_pTextPet->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal, Qt::ScrollBarAlwaysOff);
	m_pTextPMatr->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal, Qt::ScrollBarAlwaysOff);
	m_pTextComp->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal, Qt::ScrollBarAlwaysOff);
	m_pTextGuide->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal, Qt::ScrollBarAlwaysOff);
    //m_pText->verticalScrollBar()->setSliderPosition(0);

    //m_pNumer->m_TextDoc.setTextWidth(this->width()-240);

//    unsigned int xW = topLevelLabel->width();
//    unsigned int yH = topLevelLabel->height();
//    PixMapResize(texture->size().width(), texture->size().height(), xW, yH);


//    topLevelLabel->setPixmap(QPixmap::fromImage(m_pChartNumerology->numChart).scaled(xW, yH, Qt::KeepAspectRatio, Qt::SmoothTransformation));

    update();
}


void CVBNum::OnCalcPetNum()
{
    bool res3 = false;

    SUserInfo inf;

    inf.petBirthDate = m_pPetBDate->date();
    inf.petName = m_pPetName->text();
    inf.petName = unaccent(inf.petName);

    if (!getStatus())
    {
        if (inf.petName.size() != 0)
        {
#ifdef VER_PREM
            res3 = m_pNumerPet->calculatePet(inf.petName, "", inf.petBirthDate, m_pBDateF->date(), true, &settingReport, m_pReportBox->currentIndex());
#else
            res3 = m_pNumerPet->calculatePet(inf.petName, "", inf.petBirthDate, m_pBDateF->date(), true, &settingReport, 0);
#endif
        }
        else
        {
            m_pNumerPet->resetData();
        }
    } else
    {
        if (inf.petName.size() != 0)
        {
#ifdef VER_PREM
            res3 = m_pNumerPet->calculatePet(inf.petName, "", inf.petBirthDate, m_pBDateF->date(), false, &settingReport, m_pReportBox->currentIndex());
#else
            res3 = m_pNumerPet->calculatePet(inf.petName, "", inf.petBirthDate, m_pBDateF->date(), false, &settingReport, 0);
#endif
        }
        else
        {
            m_pNumerPet->resetData();
        }
    }

    if (res3)
    {
        m_pInfoMFPet->parListVal[0] = m_pNumerPet->numDet.expression;
        m_pInfoMFPet->parListVal[1] = m_pNumerPet->numDet.birthDay;
        m_pInfoMFPet->parListVal[2] = m_pNumerPet->numDet.persYear;

        m_pInfoNamePet->parListVal[1] = m_pNumerPet->numDet.fnameR;
        m_pInfoNamePet->parListVal[0] = m_pNumerPet->numDet.fnameR1;
        m_pInfoNamePet->parListVal[2] = m_pNumerPet->numDet.fnameR2;
        m_pInfoNamePet->parList[1] = inf.petName;
        m_pInfoNamePet->parList[0] = m_pNumerPet->numDet.fnameN1;
        m_pInfoNamePet->parList[2] = m_pNumerPet->numDet.fnameN2;
    }
    m_pTextPet->setHtml(m_pNumerPet->htmlDoc, QUrl("file://localhost/"));
    update();
}


//----------------------------------------------------------
void CVBNum::OnCalcAnNum()
{
    SUserInfo inf;

    inf.birthDate = m_pAnDate->date();
    inf.fullName = m_pTitleEdit->text();
    inf.fullName.truncate(300);
    inf.fullName.remove(",");
    inf.fullName = unaccent(inf.fullName);

    {
        QStringList flist = inf.fullName.split(" ");

        inf.fullName = "";
        for (int a=0; a<flist.size(); a++)
        {
            if (flist.at(a).size() > 0)
            {
                if (inf.fullName.size()>0)
                    inf.fullName += " ";
                inf.fullName += flist.at(a);
            }
        }
    }


    if (inf.fullName.size()==0)
    {
        QString Msg = tr("Enter your data before calculation!");
        QMessageBox::StandardButton Res;
        Res = QMessageBox::critical(this, tr(""), Msg, QMessageBox::Ok);

        //OnResData();
        return;
    }

    {
        bool res = false;

        m_pNumerAn->resetData();

        if (!getStatus())
        {
            res = m_pNumerAn->calculateAn(inf.fullName, inf.birthDate, true);

        }
        else
        {
            res = m_pNumerAn->calculateAn(inf.fullName, inf.birthDate, false);
        }

        if (res)
        {
            //OnResData();
            m_pInfoAnMF->parListVal[0] = m_pNumerAn->numDet.lifePath;
            m_pInfoAnMF->parListVal[1] = m_pNumerAn->numDet.expression;
            m_pInfoAnMF->parListVal[2] = m_pNumerAn->numDet.heartDesire;
            m_pInfoAnMF->parListVal[3] = m_pNumerAn->numDet.personality;
            m_pInfoAnMF->parListVal[4] = m_pNumerAn->numDet.karmicLessons;

            m_pInfoAnName->parListVal[1] = m_pNumerAn->numDet.fnameR;
            m_pInfoAnName->parListVal[0] = m_pNumerAn->numDet.fnameR1;
            m_pInfoAnName->parListVal[2] = m_pNumerAn->numDet.fnameR2;
            m_pInfoAnName->parList[1] = inf.fullName;
            m_pInfoAnName->parList[0] = m_pNumerAn->numDet.fnameN1;
            m_pInfoAnName->parList[2] = m_pNumerAn->numDet.fnameN2;
        } else
        {
            //OnResData(); !!!!
        }
    }
    /*else
    {
        OnResData();
    }*/

    m_pTextAn->setHtml(m_pNumerAn->htmlDoc);

    //recalcScrollBar();
    update();

}


void CVBNum::slotPNC(QDate date)
{
	const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    if (m_pInfoCal->m_PersYear>0)
    {
        int m_PersYear = CNumerology::downToDigit(CNumerology::downToDigit(m_pBDate->date().day()) + CNumerology::downToDigit(m_pBDate->date().month()));
        int PersYear = CNumerology::downToDigit(m_PersYear + CNumerology::downToDigit(date.year()));

        personalNumCalendar->parListVal[0] = QString::number(date.year()) + " (" + QString::number(PersYear) + ")";

        PersYear = CNumerology::downToDigit((m_PersYear + CNumerology::downToDigit(date.year())) + CNumerology::downToDigit(date.month()));
        personalNumCalendar->parListVal[1] = QString(mname[date.month()-1]) + " (" + QString::number(PersYear) + ")";


        PersYear = CNumerology::downToDigit((m_PersYear + CNumerology::downToDigit(date.year()) + CNumerology::downToDigit(date.month())) + CNumerology::downToDigit(date.day()));
        //personalNumCalendar->parListVal[2] = QString::number(date.day()) + " (" + QString::number(PersYear) + ")";
        personalNumCalendar->parListVal[2] = QString::number(PersYear);

        expandPanel->infoLabel->setText(numerPersDate[PersYear]);

        personalNumCalendar->update();

        m_pInfoCal->cdate = date;
        m_pInfoCal->update();
    }
}
