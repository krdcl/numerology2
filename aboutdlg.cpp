#include "aboutdlg.h"
#include "lic.h"

extern QString qsGlobalDlgStyle;

CAboutDlg::CAboutDlg(QImage *bkg, QImage *shp, QWidget *parent)
	: QDialog(parent)
{
	pBackground = bkg;
	pShape = shp;

	setStyleSheet(qsGlobalDlgStyle);

	//setAutoFillBackground(false);
	//setAttribute(Qt::WA_OpaquePaintEvent);
	//setAttribute(Qt::WA_NoSystemBackground);
	resize(400, 220);
	setMinimumSize(400, 220);
	setMaximumSize(400, 220);
	setWindowTitle("About");

	//setMask(QBitmap::fromImage(*pShape));

	//setWindowOpacity(0.93);

	visitBut = new QPushButton("Visit VeBest", this);
	visitBut->resize(120, 30);
	visitBut->move(this->width()/2-135, this->height()-40);
	QObject::connect(visitBut, SIGNAL(clicked()), this, SLOT(OnVisit()));

	okBut = new QPushButton("Close", this);
	okBut->resize(120, 30);
	okBut->move(this->width()/2+15, this->height()-40);
	QObject::connect(okBut, SIGNAL(clicked()), this, SLOT(OnOk()));
}


CAboutDlg::~CAboutDlg()
{

}


void CAboutDlg::paintEvent(QPaintEvent *)
{
	QPainter painter(this);

	painter.drawImage((this->width()-pBackground->width())/2, 30, *pBackground);

	QFont font("Arial", 11);
	painter.setFont(font);
	QPen pen1(QColor(156, 182, 45, 255));
	pen1.setWidth(2);
	painter.setPen(pen1);

	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.drawText( QRectF(0, this->height()-100, this->width(), 24), Qt::AlignCenter, QString("Version ") + QString::number((VBNUMVER>>16)&0xff) + "." + QString::number((VBNUMVER>>8)&0xff) + "." + QString::number(VBNUMVER&0xff) );

	QFont font2("Arial", 16);
	painter.setFont(font2);

#ifdef VER_FREE
	painter.drawText( QRectF(0, this->height()-130, this->width(), 30), Qt::AlignCenter, QString("VeBest Numerology Free") );
#else
#ifdef VER_PREM
	painter.drawText( QRectF(0, this->height()-130, this->width(), 30), Qt::AlignCenter, QString("VeBest Numerology Everywhere") );
#else
	painter.drawText( QRectF(0, this->height()-130, this->width(), 30), Qt::AlignCenter, QString("VeBest Numerology") );
#endif
#endif
    
	/*if (IntelliProtectorService::CIntelliProtector::GetLicenseType()==-1)
	{
		painter.drawText( QRectF(0, 8, this->width(), 24), Qt::AlignCenter, QString("Trial period") );
	}
	else
	{
		const int ciBufferLen = 256;
		char chBuffer[ciBufferLen] = {0, };
		IntelliProtectorService::CIntelliProtector::GetCustomerNameA(chBuffer, ciBufferLen);

		painter.drawText( QRectF(0, 8, this->width(), 24), Qt::AlignCenter, QString("Registered: ") +  QString(chBuffer));
	}*/
}


void CAboutDlg::mouseReleaseEvent(QMouseEvent *)
{
	//accept();
}


void CAboutDlg::OnOk()
{
	reject();
}

void CAboutDlg::OnVisit()
{
	accept();
}
