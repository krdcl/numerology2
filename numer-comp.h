#ifndef COMPNUMEROLOGY_H
#define COMPNUMEROLOGY_H

#include <QtGui>
#include "numer.h"
#include "numerpmatr.h"
#include "global.h"
#include "reporttools.h"

class CCompNumerology : public CReportTools
{
	Q_OBJECT

public:
	int m_BaseNums[64];

	int numLPScore;
	int numLP1, numLP2;
	int numLPCode;
	int r1score;

	int bsm, bsf;
	int dsm, dsf;
	int bss, dss, oss;

	int nameC;
	int nameCScore;
	int iciScore;

	int sumComp;

    int pmCharacterComp;
    int pmEnergyComp;




	struct SNumerDetails
	{
		QString bsm, bsf;
		QString dsm, dsf;
		QString bss, dss, oss;

		QString LP, LPScore;
		QString BCode;
		QString BScore;

		QString famScore;
		QString ciScore;
		
		QString nameComp;
		QString nameScore;

		QString sumComp;
	} numDet;


	QTextDocument m_TextDoc;


	static int downOnce(int num);
	int downToBase(int num);
	static int downToDigit(int num);
	static int statDownToBase(int num);

	static bool isBase(int num);
	static bool isCarmic(int num);

public:
	CCompNumerology(QObject *parent);
	~CCompNumerology();

    bool calculateDynamic(RepSettings *repList, int curRep, QString str, QString str2, CNumerology *numer, CNumerology *pnumer, CNumerologyPMatrix *matr, CNumerologyPMatrix *pmatr,
                          int advlpcomp, int advbcomp, int bcomp, int advecomp, bool trial);
    bool calculate(CNumerology *numer, CNumerology *pnumer, CNumerologyPMatrix *matr, CNumerologyPMatrix *pmatr, bool fsex, bool trial, RepSettings *repList, int curRep);

	void clear();
	void resetData();

};

#endif // COMPNUMEROLOGY_H
