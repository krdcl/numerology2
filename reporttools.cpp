#include "reporttools.h"
#include "numer.h"

/*
#ifdef Q_WS_WIN
const QString qsWKStyle = "::-webkit-scrollbar { width: 17px; height: 17px; }\r\n\
        ::-webkit-scrollbar:vertical { background: -webkit-gradient(linear, left top, right top, color-stop(0%, #EEEEEE), color-stop(50%, #FFFFFF), color-stop(100%, #F2F2F2)); border: 1px solid #DDDDDD; } \r\n\
        ::-webkit-scrollbar-thumb:vertical { height: 50px; background: -webkit-gradient(linear, left top, right top, color-stop(0%, #EEEEEE), color-stop(20%, #EEEEEE), color-stop(21%, #AAAAAA), color-stop(79%, #AAAAAA), color-stop(80%, #F2F2F2), color-stop(100%, #F2F2F2)); border-radius: 12px; }\r\n\
        ::-webkit-scrollbar-thumb:vertical:hover { height: 50px; background: -webkit-gradient(linear, left top, right top, color-stop(0%, #EEEEEE), color-stop(20%, #EEEEEE), color-stop(21%, #A0A0A0), color-stop(79%, #A0A0A0), color-stop(80%, #F2F2F2), color-stop(100%, #F2F2F2)); border-radius: 12px; }\r\n";
#else
const QString qsWKStyle = "";
#endif
*/

extern const QString qsWKStyle;


//------------------------------------------
CReportTools::CReportTools(QObject *parent) :
    QObject(parent)
{

}
//------------------------------------------
void CReportTools::startDoc(QString tit)
{
    htmlDoc = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n";
    htmlDoc += "<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n";
    htmlDoc += "<head>\r\n";
    htmlDoc += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n";

    htmlDoc += "<style type=\"text/css\">\r\n";
    htmlDoc += "body { font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 1.6; color: #111111; }\r\n";
    htmlDoc += "h2 { font-size: 18px; line-height: 1.4; color: #222222; margin-left: 4px; }\r\n";
    htmlDoc += qsWKStyle;
    htmlDoc += "</style>\r\n";

    htmlDoc += "<title>" + tit + "</title>";
    htmlDoc += "</head><body style=\"background-color:#FFFFFF\">\r\n";
}
//------------------------------------------
void CReportTools::addHead(QString text, QColor color, QFont font, QImage image)
{
    if (!image.isNull())
    {
        QByteArray ba;
        QBuffer buffer(&ba);
        buffer.open(QIODevice::WriteOnly);
        image.save(&buffer, "PNG"); // writes image into ba in PNG format
        buffer.close();
        parImageCode = "<img src = \"data:image/png;base64,";
        parImageCode += QString(ba.toBase64());
        parImageCode += "\" style=\"margin-left: 15px;\" align=\"right\" />";
    }

    QString txt;
    if (font.underline())
        txt += "<u>";
    if (font.italic())
        txt += "<i>";

    txt += "<font style=\"";
    int fsz = font.pixelSize();
    //if (fsz<=0) fsz = font.pointSize();
    if (fsz>0)
        txt += "font-size: " + QString::number(fsz) + "px;";

    if (font.family().size()>0)
        txt += " face:" + font.family() + ";";
    txt += " color:" + color.name() + ";\">" + text + "</font>";

    if (font.italic())
        txt += "</i>";
    if (font.underline())
        txt += "</u>";

    htmlDoc += "<h2>" + parImageCode + txt + "</h2>\r\n";
    parImageCode = "";
}
//------------------------------------------
void CReportTools::addPlain(QString text)
{
    htmlDoc += text;
}
//------------------------------------------
void CReportTools::addPar(QString text, QColor color, QFont font, QImage image)
{
    if (!image.isNull())
    {
        QByteArray ba;
        QBuffer buffer(&ba);
        buffer.open(QIODevice::WriteOnly);
        image.save(&buffer, "PNG"); // writes image into ba in PNG format
        buffer.close();
        parImageCode = "<img src = \"data:image/png;base64,";
        parImageCode += QString(ba.toBase64());
        parImageCode += "\" style=\"margin-left: 15px;\" align=\"right\" />";
    }

    QString txt;
    if (font.underline())
        txt += "<u>";
    if (font.italic())
        txt += "<i>";

    //txt += "<font style=\"font-size: " + QString::number(font.pixelSize()) + "px\" face=\"" + font.family() + "\" color=\"" + color.name() + "\">" + text + "</font>";
    txt += "<font style=\"";
    int fsz = font.pixelSize();
    //if (fsz<=0) fsz = font.pointSize();
    if (fsz>0)
        txt += "font-size: " + QString::number(fsz) + "px;";

    if (font.family().size()>0)
        txt += " face:" + font.family() + ";";
    txt += " color:" + color.name() + ";\">" + text + "</font>";


    if (font.italic())
        txt += "</i>";
    if (font.underline())
        txt += "</u>";

    htmlDoc += "<p>" + parImageCode + txt + "</p>\r\n";

    parImageCode = "";
}
//------------------------------------------
void CReportTools::addPar(QString text, QImage image)
{
    if (!image.isNull())
    {
        QByteArray ba;
        QBuffer buffer(&ba);
        buffer.open(QIODevice::WriteOnly);
        image.save(&buffer, "PNG"); // writes image into ba in PNG format
        buffer.close();

        if (!ba.isEmpty())
        {
            parImageCode = "<img src = \"data:image/png;base64,";
            parImageCode += QString(ba.toBase64());
            parImageCode += "\" style=\"margin-left: 15px;\" align=\"right\" />";
        }
    }

    if (!(parImageCode.isEmpty() && text.isEmpty()))
        htmlDoc += "<p>" + parImageCode + text + "</p>\r\n";



    parImageCode = "";
}
//------------------------------------------
void CReportTools::addPar(QString defText, const repItem *item, int id)
{
    if (item->imgList.size() > id)
    {
        if (item->isDefList.at(id) == false)
        {
            defText = delHtmlHeaderTags(item->content.at(id));
        }
    }

    QString txt;

    if (item->isDefList.at(id) == true)
    {
        if (item->contentFont.bold())
            txt += "<b>";
        if (item->contentFont.underline())
            txt += "<u>";
        if (item->contentFont.italic())
            txt += "<i>";

        QFont font = item->contentFont;
        txt += "<font style=\"";
        int fsz = font.pixelSize();
        //if (fsz<=0) fsz = font.pointSize();
        if (fsz>0)
            txt += "font-size: " + QString::number(fsz) + "px;";

        if (font.family().size()>0)
            txt += " face:" + font.family() + ";";
        txt += " color:" + item->contentColor.name() + ";\">";

        //txt += "<font style=\"font-size: " + QString::number(item->contentFont.pixelSize()) + "px\" face=\"" + item->contentFont.family() + "\" color=\"" + item->contentColor.name() + "\">";
    }

    //txt += defText;

    QString txt2;
    if (item->isDefList.at(id) == true)
    {
        txt2 += "</font>";
        if (item->contentFont.italic())
            txt2 += "</i>";
        if (item->contentFont.underline())
            txt2 += "</u>";
        if (item->contentFont.bold())
            txt2 += "</b>";
    }

    if (defText.isEmpty())
    {
        endChap();
        return;
    }

    defText.replace("<br />", "<br>");

    addPar(addSplitParagraph(defText, txt, txt2));
}
//------------------------------------------
void CReportTools::addTabPar(QString text)
{
    htmlDoc += "<span class=maindoc><table class=maindoc border=\"0\" cellspacing=\"0\"><tr>";
    htmlDoc += "<td valign=\"top\"><p>" + parImageCode + "</p></td>\r\n";
    htmlDoc += "<td valign=\"top\"><p>" + text + "</p></td>\r\n";
    htmlDoc += "</tr></table></span>";
    parImageCode = "";
}
//------------------------------------------
void CReportTools::addImage(QString img)
{
	parImageCode = "<img src=\"file:///" + resPath + img + "\" style=\"margin-left: 15px;\" align=\"right\" />";
}
//------------------------------------------
void CReportTools::startChap(bool clearImg)
{
    htmlDoc += "<table class=maindoc border=\"0\" width=100%><tr><td>\r\n";
    if (clearImg)
        parImageCode = "";
}
//------------------------------------------
void CReportTools::endChap()
{
    htmlDoc += "</td></tr></table>\r\n";
    parImageCode = "";
}
//------------------------------------------
void CReportTools::endDoc()
{
    htmlDoc += "</body></html>\r\n";
    parImageCode = "";
    htmlDoc = htmlDoc.replace("font-weight:600", "font-weight:bold");
}
//------------------------------------------
void CReportTools::addStandItem(QString header, QString headerAdd, QString str, QString strAdd, int id, const repItem *item, bool noImg)
{
    str.replace("<br />", "<br>");
    strAdd.replace("<br />", "<br>");
    if (item->isContentDefault)
    {
        startChap(false);
        addHead(header + " " + headerAdd);
        if (strAdd.isEmpty())
        {
            QString sps = addSplitParagraph(str);
            if (sps.isEmpty())
                endChap();
            else
                addPar(sps);
        }
        else
        {
            if (str.isEmpty())
            {
                QString sps = addSplitParagraph(strAdd);
                if (sps.isEmpty())
                    endChap();
                else
                    addPar(sps);
            }
            else
            {
                QString sps = addSplitParagraph(str + "<br>" + strAdd);
                if (sps.isEmpty())
                    endChap();
                else
                    addPar(sps);
            }
        }
        endChap();
    }
    else
    {
        startChap(false);
        if (item->imgList.size() > 0 && (noImg == false) && (item->extendedSettings.value("hide-image","FALSE")!="TRUE"))
            addHead(item->headerName + " " + headerAdd, item->headerColor, item->headerFont, item->imgList.at(0));
        else
            addHead(item->headerName + " " + headerAdd, item->headerColor, item->headerFont);

        if (!str.isEmpty())
        {
            //QString sps = addSplitParagraph(str);
//            addPar(str, item, 0);
            addPar(str, item, 0);
            endChap();
        }


        if (id != -1)
        {
//            if (!str.isEmpty())
//                addPar("<br>");

            int offset = 0;

            if (item->type == RTNumerology &&
                    ((item->id == NTChallenges)
                     || (item->id == NTBridgeLPE)
                     || (item->id == NTBridgeHDP)))
                offset = 1;

            if (!str.isEmpty())
                startChap();
            //QString sps = addSplitParagraph(strAdd);
            addPar(strAdd, item, id + offset);
            endChap();
        }
    }
}
//------------------------------------------
QString CReportTools::addSplitParagraph(QString str, QString tagsOpen, QString tagClose)
{
    if (str.isEmpty())
        return QString();

    QString sps;

    str = str.replace("<p>","");
    str = str.replace("</p>","<br>");
    str = str.replace("<br/>","<br>");
    str = str.replace("<br />","<br>");
    QStringList strList = str.split("<br>");
    //strList.append(strAdd.split("<br>"));

    QString finalSpan = "";
    QString startSpan = "";
    for (int i = 0; i < strList.size(); i++)
    {
        signed int lstart = strList.at(i).lastIndexOf("<span");
        signed int lend = strList.at(i).lastIndexOf("</span>");
        signed int fend = strList.at(i).indexOf("</span>");
        if ((lstart>lend)
                || ((startSpan.size()>0) && (fend<0)) ) {
            finalSpan = "</span>";
        } else {
            finalSpan = "";
        }

        /* else if ((lstart<0) && (lend>lstart)){
            finalSpan = "";
        }*/
        if (i == 0)
        {
            sps += tagsOpen;
            sps += strList.at(i);
            sps += finalSpan;
            sps += tagClose;
            sps += "</td></tr></table>\r\n";  // endChap()
        }
        else
        {
            sps += "<table class=maindoc border=\"0\"><tr><td>\r\n"; // startChap
            sps += tagsOpen;
            sps += startSpan;
            sps += strList.at(i);
            sps += finalSpan;
            sps += tagClose;
            sps += "</td></tr></table>\r\n";  // endChap()
        }

        if (lstart>lend) {
            int ti = strList.at(i).indexOf(">", lstart);
            startSpan = strList.at(i).mid(lstart, ti-lstart+1);
        }
        if ((lend>0) && (lend>lstart))
            startSpan = "";
    }

    //qDebug() << sps;
    return sps;
}
//------------------------------------------
QString CReportTools::delHtmlHeaderTags(QString text)
{
    if (text.isEmpty())
        return QString();

    if (!(text.contains("<body") && text.contains("</body>")))
        return text;

    int indexF = text.indexOf("<body");
    if (text.contains("<!-- md --><br>"))
    {
        indexF = text.indexOf("<!-- md --><br>", indexF);
        indexF += 15;
    }
    else
    {
        indexF = text.indexOf(">", indexF);
        indexF++;
    }
    text = text.right(text.size() - indexF);
    indexF = text.indexOf("</body>");
    text = text.left(indexF);

    return text;
}
//------------------------------------------
void CReportTools::preloadRes(QString rpath)
{
    resPath = rpath;
}
//------------------------------------------
