#ifndef NUMEROLOGYPMATR_H
#define NUMEROLOGYPMATR_H

#include <QtGui>
#ifndef VER_MOBILE
#include "global.h"
#include "layoutmanagerdlg.h"
#endif
#include "reporttools.h"

class CNumerologyPMatrix : public CReportTools
{
	Q_OBJECT

public:
	int m_BaseNums[64];

	int m_LifePath;
	int m_cNum1, m_cNum2, m_cNum3, m_cNum4;
	int m_cyear, m_PersYear;
	int m_PELet[64];
	int m_Let[64];
	int m_CalcMode[33];

	int m_ResMatr[10];
	int m_SumH[3];
	int m_SumV[3];
	int m_SumHV[2];

	struct SNumerDetails
	{
		QString lifePath;
		QString birthDay;
		QString expression;
		QString mexpression;
		QString heartDesire;
		QString mheartDesire;
		QString personality;
		QString mpersonality;
		QString maturity;
		QString rationalThought;
		QString karmicDebts;
		QString karmicLessons;
		QString challenges;
		QString pinnacles;
		QString brLPE;
		QString brHDP;
		QString balance;
		QString persYear;
		QString hiddenPassion;
		QString subconsciousConfidence;
		QString ageSpan;

		QString PE[20];
		QString PENum[20];
		QString PECnt[20];
		QString PERep[20];



		QString fnameN1;
		QString fnameN2;
		QString fnameR;
		QString fnameR1;
		QString fnameR2;

		QString snameN1;
		QString snameN2;
		QString snameR;
		QString snameR1;
		QString snameR2;
	} numDet;


	QTextDocument m_TextDoc;

	static void addNumToList(QList<int> &cl, int num);

public:
	CNumerologyPMatrix(QObject *parent);
	~CNumerologyPMatrix();

    bool calculate(QString fullName, QDate bdate, bool trial, RepSettings *repList, int curRep);
    bool calculateDynamic(QString fullName, RepSettings *repList, int curRep, QString str, QDate bdate, bool trial);

	void clear();
	void resetData();

};

#endif // NUMEROLOGYPMATR_H
