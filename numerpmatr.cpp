#include "numerpmatr.h"
#include "numer.h"
//#include "numer_interp.h"

extern const QString qsWKStyle;

// code 201XX
QString numerPurposeful[128];
// code 202XX
QString numerFamilyFormation[128];
// code 203XX
QString numerStability[128];
// code 204XX
QString numer1[128];
// code 205XX
QString numer2[128];
// code 206XX
QString numer3[128];
// code 207XX
QString numer4[128];
// code 208XX
QString numer5[128];
// code 209XX
QString numer6[128];
// code 210XX
QString numer7[128];
// code 211XX
QString numer8[128];
// code 212XX
QString numer9[128];

// code 213XX
QString numerSelfAppraisal[128];
// code 214XX
QString numerFinance[128];
// code 215XX
QString numerTalent[128];
// code 216XX
QString numerSpirituality[128];
// code 217XX
QString numerSexualLife[128];

//new block
QString numerCompNumbers[128];
QString numerCompoundNumbers[128];



CNumerologyPMatrix::CNumerologyPMatrix(QObject *parent)
: CReportTools(parent)
{
	resetData();
}


CNumerologyPMatrix::~CNumerologyPMatrix()
{
}


void CNumerologyPMatrix::resetData()
{
	clear();
}


void CNumerologyPMatrix::clear()
{
	for (int a=0; a<10; a++)
		m_ResMatr[a]=0;
	for (int a=0; a<3; a++)
	{
		m_SumH[a]=0;
		m_SumV[a]=0;
	}
	m_SumHV[0] = 0;
	m_SumHV[1] = 0;

	m_TextDoc.clear();
}



void CNumerologyPMatrix::addNumToList(QList<int> &cl, int num)
{
	num = num % 10000;
	bool addz = false;

	int d = (num % 10000)/1000;
	if ((d > 0)|| addz)
	{
		cl.append(d);
		addz = true;
	}
	d = (num % 1000)/100;
	if ((d > 0)|| addz)
	{
		cl.append(d);
		addz = true;
	}
	d = (num % 100)/10;
	if ((d > 0)|| addz)
	{
		cl.append(d);
		addz = true;
	}
	d = num % 10;
	cl.append(d);
}

bool CNumerologyPMatrix::calculateDynamic(QString fullName, RepSettings *repList, int curRep, QString str, QDate bdate, bool trial)
{
    const QList<repItem> *PsychoMatrix = &repList->at(curRep).PsychoMatrix;
    const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    startDoc("Numerology Report");

    bool showImages=true;
    if (repList->at(curRep).extendedSettings.value("RTPsychoMatrix-hide-images", "FALSE")=="TRUE")
        showImages=false;

    for (int i = 0; i < PsychoMatrix->size(); i++)
    {
        if (trial == true && i > 4)
        {
            startChap();
#ifndef VER_FREE
            addPar("<br />" + numerInfo[9] + "<br />");
#endif
            endChap();
            break;
        }

        const repItem *item = &PsychoMatrix->at(i);
        if (item->id == PMTHeader)
        {
            startChap();
            if (item->isContentDefault)
            {
                startChap();
                addHead("Numerology Psychomatrix Chart for");
                endChap();

                addHead(fullName + ", " + QString(mname[bdate.month()-1]) + " " + QString::number(bdate.day()) + " " + QString::number(bdate.year()));
                addPar(str);
            }
            else
            {
                startChap();
                addHead(item->headerName, item->headerColor, item->headerFont);
                endChap();

                addHead(fullName + ", " + QString(mname[bdate.month()-1]) + " " + QString::number(bdate.day()) + " " + QString::number(bdate.year()), item->contentColor, item->contentFont);
                addPar(str, item->contentColor, item->contentFont);
            }
            addPar("");
            endChap();
        }
        else
        if (item->id == PMTCharacter)
        {
            int ctype1 = m_ResMatr[1]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res16.jpg");
            addStandItem("Character (1)", QString::number(m_ResMatr[1]), numer1[0], numer1[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTEnergy)
        {
            int ctype1 = m_ResMatr[2]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res17.jpg");
            addStandItem("Energy (2)", QString::number(m_ResMatr[2]), numer2[0], numer2[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTInterest)
        {
            int ctype1 = m_ResMatr[3]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res18.jpg");
            addStandItem("Interest (3)", QString::number(m_ResMatr[3]), numer3[0], numer3[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTHealth)
        {
            int ctype1 = m_ResMatr[4]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res19.jpg");
            addStandItem("Health (4)", QString::number(m_ResMatr[4]), numer4[0], numer4[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTIntuition)
        {
            int ctype1 = m_ResMatr[5]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res20.jpg");
            addStandItem("Intuition (5)", QString::number(m_ResMatr[5]), numer5[0], numer5[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTLabour)
        {
            int ctype1 = m_ResMatr[6]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res21.jpg");
            addStandItem("Labour (6)", QString::number(m_ResMatr[6]), numer6[0], numer6[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTLuck)
        {
            int ctype1 = m_ResMatr[7]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res22.jpg");
            QString st = numer7[0];

            addStandItem("Luck (7)", QString::number(m_ResMatr[7]), st, numer7[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTDuty)
        {
            int ctype1 = m_ResMatr[8]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res23.jpg");
            addStandItem("Duty (8)", QString::number(m_ResMatr[8]), numer8[0], numer8[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTMemory)
        {
            int ctype1 = m_ResMatr[9]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res24.jpg");
            addStandItem("Memory (9)", QString::number(m_ResMatr[9]), numer9[0], numer9[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTPurposeful)
        {
            int ctype1 = m_SumV[0]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res25.jpg");
            addStandItem("Purposeful (1-4-7)", QString::number(m_SumV[0]), numerPurposeful[0], numerPurposeful[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTFamilyFormation)
        {
            int ctype1 = m_SumV[1]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res26.jpg");
            addStandItem("Family formation (2-5-8)", QString::number(m_SumV[1]), numerFamilyFormation[0], numerFamilyFormation[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTStability)
        {
            int ctype1 = m_SumV[2]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res27.jpg");
            addStandItem("Stability (3-6-9)", QString::number(m_SumV[2]), numerStability[0], numerStability[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTSelfAppraisal)
        {
            int ctype1 = m_SumH[0]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res28.jpg");
            addStandItem("Self appraisal (1-2-3)", QString::number(m_SumH[0]), numerSelfAppraisal[0], numerSelfAppraisal[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTFinance)
        {
            int ctype1 = m_SumH[1]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res29.jpg");
            addStandItem("Finance (4-5-6)", QString::number(m_SumH[1]), numerFinance[0], numerFinance[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTTalent)
        {
            int ctype1 = m_SumH[2]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res30.jpg");
            addStandItem("Talent (7-8-9)", QString::number(m_SumH[2]), numerTalent[0], numerTalent[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTSpirituality)
        {
            int ctype1 = m_SumHV[0]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res31.jpg");
            addStandItem("Spirituality (1-5-9)", QString::number(m_SumHV[0]), numerSpirituality[0], numerSpirituality[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTSexualLife)
        {
            int ctype1 = m_SumHV[1]+1;
            if (ctype1>7) ctype1 = 7;
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res32.jpg");
            addStandItem("Sexual life (3-5-7)", QString::number(m_SumHV[1]), numerSexualLife[0], numerSexualLife[ctype1], ctype1, item, !showImages);
            //endChap();
        }
        else
        if (item->id == PMTCustomText)
        {
            addStandItem("Custom Text", QString(), numerCustomText[0], QString(), -1, item, !showImages);
        }
    }
    endDoc();
    return true;
}

bool CNumerologyPMatrix::calculate(QString fullName, QDate bdate, bool trial, RepSettings *repList, int curRep)
{
    int select = curRep - 1;

    if (select >= 0)
    {
        if (repList->size() > 0)
        {
            if (repList->size() <= select)
                select = -1;
        }
        else
        {
            select = -1;
        }
    }


    const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	resetData();

	QList<int> cl1, cl2, cl4, clr;

	addNumToList(cl1, bdate.day());
	addNumToList(cl1, bdate.month());
	addNumToList(cl1, bdate.year());

	int num1=0;
	for (int a=0; a<cl1.size(); a++)
		num1 += cl1.at(a);

	addNumToList(cl2, num1);

	int num2=0;
	for (int a=0; a<cl2.size(); a++)
		num2 += cl2.at(a);

	int num3 = num1 - 2*cl1.at(0);


	addNumToList(cl4, num3);
	int num4=0;
	for (int a=0; a<cl4.size(); a++)
		num4 += cl4.at(a);

	addNumToList(clr, num1);
	addNumToList(clr, num2);
	addNumToList(clr, num3);
	addNumToList(clr, num4);


	for (int a=0; a<cl1.size(); a++)
		m_ResMatr[cl1.at(a)]++;
	for (int a=0; a<clr.size(); a++)
		m_ResMatr[clr.at(a)]++;

	for (int a=0; a<3; a++)
	{
		m_SumH[a] = 0;
		for (int b=0; b<3; b++)
			m_SumH[a] += m_ResMatr[a*3+b+1];
	}

	for (int a=0; a<3; a++)
	{
		m_SumV[a] = 0;
		for (int b=0; b<3; b++)
			m_SumV[a] += m_ResMatr[a+b*3+1];
	}

	m_SumHV[0] = 0;
	m_SumHV[1] = 0;
	for (int a=0; a<3; a++)
	{
		m_SumHV[0] += m_ResMatr[a*3+a+1];
		m_SumHV[1] += m_ResMatr[a*3+(2-a)+1];
	}



    QList<int> allIds;
    if (select >= 0) {
        for (int i = 0; i < repList->at(select).PsychoMatrix.size(); i++)
            allIds.append(repList->at(select).PsychoMatrix.at(i).id);
    } else {
        for (int i = 0; i < PMT_END; i++)
            allIds.append(i);
    }

	QString str;
    if (allIds.contains(PMTCharacter)) {
        str = "Character (1) - " + QString::number(m_ResMatr[1]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTEnergy)) {
        str += "Energy (2) - " + QString::number(m_ResMatr[2]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTInterest)) {
        str += "Interest (3) - " + QString::number(m_ResMatr[3]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTHealth)) {
        str += "Health (4) - " + QString::number(m_ResMatr[4]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTIntuition)) {
        str += "Intuition (5) - " + QString::number(m_ResMatr[5]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTLabour)) {
        str += "Labour (6) - " + QString::number(m_ResMatr[6]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTLuck)) {
        str += "Luck (7) - " + QString::number(m_ResMatr[7]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTDuty)) {
        str += "Duty (8) - " + QString::number(m_ResMatr[8]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTMemory)) {
        str += "Memory (9) - " + QString::number(m_ResMatr[9]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTPurposeful)) {
        str += "Purposeful (1-4-7) - " + QString::number(m_SumV[0]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTFamilyFormation)) {
        str += "Family formation (2-5-8) - " + QString::number(m_SumV[1]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTStability)) {
        str += "Stability (3-6-9) - " + QString::number(m_SumV[2]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTSelfAppraisal)) {
        str += "Self appraisal (1-2-3) - " + QString::number(m_SumH[0]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTFinance)) {
        str += "Finance (4-5-6) - " + QString::number(m_SumH[1]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTTalent)) {
        str += "Talent (7-8-9) - " + QString::number(m_SumH[2]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTSpirituality)) {
        str += "Spirituality (1-5-9) - " + QString::number(m_SumHV[0]);
        str+="<br />\r\n";
    }
    if (allIds.contains(PMTSexualLife)) {
        str += "Sexual life (3-5-7) - " + QString::number(m_SumHV[1]);
        str+="<br />\r\n";
    }

    if (select >= 0)
        return calculateDynamic(fullName, repList, select, str, bdate, trial);

	startDoc("VeBest Numerology Report");
	startChap();
	addHead("Numerology Psychomatrix Chart for");
	addHead(fullName + ", " + QString(mname[bdate.month()-1]) + " " + QString::number(bdate.day()) + " " + QString::number(bdate.year()));
#ifndef VER_MOBILE
	addPar(str);
	addPar("");
#endif
	endChap();

	// Details
	startChap();
    addImage("res16.jpg");
	addHead("Character (1) - " + QString::number(m_ResMatr[1]));
	int ctype1 = m_ResMatr[1]+1;
	if (ctype1>7) ctype1 = 7;
	addPar(numer1[0]);
	endChap();

	startChap();
	addPar(numer1[ctype1]);
	endChap();


    if (!trial) {
        startChap();
        addImage("res17.jpg");
        addHead("Energy (2) - " + QString::number(m_ResMatr[2]));
        ctype1 = m_ResMatr[2]+1;
        if (ctype1>7) ctype1 = 7;
        addPar(numer2[0]);
        endChap();

        startChap();
        addPar(numer2[ctype1]);
        endChap();


        startChap();
        addImage("res18.jpg");
        addHead("Interest (3) - " + QString::number(m_ResMatr[3]));
        ctype1 = m_ResMatr[3]+1;
        if (ctype1>7) ctype1 = 7;
        addPar(numer3[0]);
        endChap();

        startChap();
        addPar(numer3[ctype1]);
        endChap();
    }


	startChap();
    addImage("res19.jpg");
	addHead("Health (4) - " + QString::number(m_ResMatr[4]));
	ctype1 = m_ResMatr[4]+1;
	if (ctype1>7) ctype1 = 7;
	addPar(numer4[0]);
	endChap();

	startChap();
	addPar(numer4[ctype1]);
	endChap();



	if (trial)
	{
		startChap();
#ifndef VER_FREE
        addPar("<br />" + numerInfo[9] + "<br />");
#endif
		addPar("");
		endChap();
	} else
	{
		startChap();
        addImage("res20.jpg");
		addHead("Intuition (5) - " + QString::number(m_ResMatr[5]));
		ctype1 = m_ResMatr[5]+1;
		if (ctype1>7) ctype1 = 7;
		//addPar(numer5[0] + "<br /><br />" + numer5[ctype1]);
		addPar(numer5[0]);
		endChap();

		startChap();
		addPar(numer5[ctype1]);
		endChap();


		startChap();
        addImage("res21.jpg");
		addHead("Labour (6) - " + QString::number(m_ResMatr[6]));
		ctype1 = m_ResMatr[6]+1;
		if (ctype1>7) ctype1 = 7;
		addPar(numer6[0]);
		endChap();

		startChap();
		addPar(numer6[ctype1]);
		endChap();


        startChap();
        addImage("res22.jpg");
		addHead("Luck (7) - " + QString::number(m_ResMatr[7]));
		ctype1 = m_ResMatr[7]+1;
		if (ctype1>7) ctype1 = 7;
		QStringList spl = numer7[0].split("<br />");
        addPar(spl.at(0));
        endChap();

        for (int a=1; a<spl.size(); a++)
        {
            startChap();
            addPar(spl.at(a));
            endChap();
        }

        startChap();
        addPar(numer7[ctype1]);
        endChap();


		startChap();
        addImage("res23.jpg");
		addHead("Duty (8) - " + QString::number(m_ResMatr[8]));
		ctype1 = m_ResMatr[8]+1;
		if (ctype1>7) ctype1 = 7;
		addPar(numer8[0]);
		endChap();

		startChap();
		addPar(numer8[ctype1]);
		endChap();


		startChap();
        addImage("res24.jpg");
		addHead("Memory (9) - " + QString::number(m_ResMatr[9]));
		ctype1 = m_ResMatr[9]+1;
		if (ctype1>7) ctype1 = 7;
		addPar(numer9[0] + "<br /><br />" + numer9[ctype1]);
		endChap();

		startChap();
        addImage("res25.jpg");
		addHead("Purposeful (1-4-7) - " + QString::number(m_SumV[0]));
		ctype1 = m_SumV[0]+1;
		if (ctype1>7) ctype1 = 7;
		addPar(numerPurposeful[0] + "<br /><br />" + numerPurposeful[ctype1]);
		endChap();

		startChap();
        addImage("res26.jpg");
		addHead("Family formation (2-5-8) - " + QString::number(m_SumV[1]));
		ctype1 = m_SumV[1]+1;
		if (ctype1>7) ctype1 = 7;
		addPar(numerFamilyFormation[0] + "<br /><br />" + numerFamilyFormation[ctype1]);
		endChap();

		startChap();
        addImage("res27.jpg");
		addHead("Stability (3-6-9) - " + QString::number(m_SumV[2]));
		ctype1 = m_SumV[2]+1;
		if (ctype1>7) ctype1 = 7;
		addPar(numerStability[0] + "<br /><br />" + numerStability[ctype1]);
		endChap();

		startChap();
        addImage("res28.jpg");
		addHead("Self appraisal (1-2-3) - " + QString::number(m_SumH[0]));
		ctype1 = m_SumH[0]+1;
		if (ctype1>7) ctype1 = 7;
		addPar(numerSelfAppraisal[0] + "<br /><br />" + numerSelfAppraisal[ctype1]);
		endChap();

		startChap();
        addImage("res29.jpg");
		addHead("Finance (4-5-6) - " + QString::number(m_SumH[1]));
		ctype1 = m_SumH[1]+1;
		if (ctype1>7) ctype1 = 7;
		addPar(numerFinance[0] + "<br /><br />" + numerFinance[ctype1]);
		endChap();

		startChap();
        addImage("res30.jpg");
		addHead("Talent (7-8-9) - " + QString::number(m_SumH[2]));
		ctype1 = m_SumH[2]+1;
		if (ctype1>7) ctype1 = 7;
		addPar(numerTalent[0] + "<br /><br />" + numerTalent[ctype1]);
		endChap();

		startChap();
        addImage("res31.jpg");
		addHead("Spirituality (1-5-9) - " + QString::number(m_SumHV[0]));
		ctype1 = m_SumHV[0]+1;
		if (ctype1>7) ctype1 = 7;
		addPar(numerSpirituality[0] + "<br /><br />" + numerSpirituality[ctype1]);
		endChap();

		startChap();
        addImage("res32.jpg");
		addHead("Sexual life (3-5-7) - " + QString::number(m_SumHV[1]));
		ctype1 = m_SumHV[1]+1;
		if (ctype1>7) ctype1 = 7;
		addPar(numerSexualLife[0] + "<br /><br />" + numerSexualLife[ctype1]);
		endChap();
	}

	endDoc();

	return true;
}
