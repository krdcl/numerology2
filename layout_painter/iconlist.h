#ifndef ICONLIST_H
#define ICONLIST_H

#include <QtGui>
//#include "datafile.h"
#include "reportpe.h"
//#include "appsettings.h"


class CILItem : public QWidget
{
    Q_OBJECT

public:
    CILItem(/*CAppSettings *csettings, */QWidget *parent=NULL);
    ~CILItem();

    int px, py;
    QString name;
    QString addonStr;
    QImage *iticon;
    bool sel;
    CREPage *page;

    int id;
    void *pData;
    int vflags;
    int lpr;

    int tmrid;
    bool freeMove;

    //CAppSettings *settings;

protected:
    void paintEvent(QPaintEvent *);
    void timerEvent(QTimerEvent *);
    void mousePressEvent(QMouseEvent *event) { emit startDD(this); QWidget::mousePressEvent(event); }
signals:
    void startDD(void *);
};


class CILContainerWidget : public QWidget
{
    Q_OBJECT

public:
    CILContainerWidget(QWidget *parent=NULL);
    ~CILContainerWidget();
};

class CILContainer : public QScrollArea
{
    Q_OBJECT

public:
    CILContainer(QWidget *parent=NULL);
    ~CILContainer();

    QWidget *widg;
    QList<CILItem *> lmitems;
    bool sel;
    int itemhs, itemws;
    int itemdw, itemdh;

    void updateContainer();
    void prepareItemPlace(int x, int y);
    void calcItemPos(CILItem *item, int idx);

protected:
    void resizeEvent(QResizeEvent *);
};



class CIconList : public QWidget
{
	Q_OBJECT

public:
    CIconList(/*CDataFile *cdata, CAppSettings *csettings, */QWidget *parent=NULL);
    ~CIconList();

    QList<CILContainer*> lmcs;
    CILItem *citem;
    CILItem *cursitem;
    QPoint spnt;
    bool ddstarted, ddprepare;
    CILContainer *slmc;
    int slmcpos;
    QString qsResPath;

    QImage iticon;


//    CAppSettings *settings;
//    CTranslate *q;
//    CDataFile *data;

    int tmrid;

    CILItem* addItem(QString name, int id, CREPage *page = NULL);

    void clearSel();
    void clear();
    void setCurrentItem(CILItem *item, bool sel=true);
    void setCurrentItemByPData(void *pData, bool sel=true);
    CILItem* getSelectedItem();
    void updateFields();
    void updateList();
    void setItemPicture(QImage *img);

    void processItems(QWidget *parW);
    QImage ilcont;
    QImage ilitem;

protected:
    void paintEvent(QPaintEvent *);
    void resizeEvent(QResizeEvent *);
public slots:
    void OnStartDD(void *);

signals:
    void itemSelected(CILItem *);
    void itemDropped(CILItem *, QPoint);
    void signalItemDrop();
};

#endif // ICONLIST_H
