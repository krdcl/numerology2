#ifndef PAPERSIZEDLG_H
#define PAPERSIZEDLG_H

#include <QtGui>
//#include "appsettings.h"
#include "reportcreator.h"

class CPaperSizeDlg : public QWidget
{
	Q_OBJECT

public:
    CPaperSizeDlg(CAppStyle *pAppstyle, QWidget *parent, bool isBlock=false, int mode = THUMB_MODE);
    ~CPaperSizeDlg();

    QString dlgTitle;
    CAppStyle *appstyle;

	void initStyle();

    int mod;

	QPushButton *m_pOk;
	QPushButton *m_pCancel;

    QLineEdit *m_pReportName;
    QComboBox *m_pPaperSizes;
    QComboBox *m_pOrientation;
    QLineEdit *m_pWidth;
    QLineEdit *m_pHeight;
    QComboBox *m_pUnits;
    QLineEdit *m_pMarginLeft;
    QLineEdit *m_pMarginRight;
    QLineEdit *m_pMarginTop;
    QLineEdit *m_pMarginBottom;
    QLineEdit *m_pMargin;
    QComboBox *m_pPageSeq;

    QLabel *m_pL1, *m_pL2, *m_pL3, *m_pL4, *m_pL5, *m_pL6, *m_pL7;

//	CAppSettings *settings;
//	CTranslate *q;

	int res;

    int m_units;

    struct SPageSizes
    {
        QString name;
        double w, h;
        int units;
        bool skip;
        SPageSizes()
        {
            name = "";
            w=0, h=0;
            units=0;
            skip=true;
        }
        SPageSizes(QString psName, double psW, double psH, int psUnits, bool psSkip)
        {
            name = psName;
            w=psW, h=psH;
            units=psUnits;
            skip=psSkip;
        }
    };

    QVector<SPageSizes> pageSizes;

    void connectUI();
    void disconnectUI();
    void selPaperCombo();
    void addSeqCombo();


    double rw, rh;
    int rseq;

protected:
	void paintEvent(QPaintEvent *);
	void resizeEvent(QResizeEvent *event);

public slots:
	void OnExit();
	void OnOk();
	void OnCancel();

    void OnPSCombo(int);
    void OnOrientationCombo(int);
    void OnUnitsCombo(int);
    void OnTextChanged();
};


class CPaperSizeDlgWin : public QDialog
{
	Q_OBJECT

public:
    CPaperSizeDlgWin(CAppStyle *pAppstyle, QWidget *parent, bool isBlock = false, int mode = -5);
    ~CPaperSizeDlgWin();

    CPaperSizeDlg *mainWidget;

protected:
	void keyPressEvent(QKeyEvent *e)
	{
		if ((e->key()==Qt::Key_Enter) || (e->key()==Qt::Key_Return) || (e->key()==Qt::Key_Escape)) {}
		else { QDialog::keyPressEvent(e); return; }
	}
};


#endif // PAPERSIZEDLG_H
