#ifndef QR_CODE_H
#define QR_CODE_H

#include <math.h>
#include <string.h>



//ËÄÖÖ¾À´íµÈ¼¶
#define QR_LEVEL_L      0
#define QR_LEVEL_M      1
#define QR_LEVEL_Q      2
#define QR_LEVEL_H      3

// ±àÂëÄ£Ê½
#define QR_MODE_NUMERAL         0
#define QR_MODE_ALPHABET        1
#define QR_MODE_8BIT            2
#define QR_MODE_KANJI           3

//°æ±¾×é
#define QR_VRESION_S    0 // 1~9
#define QR_VRESION_M    1 // 10~26
#define QR_VRESION_L            2 // 27~40

#define MAX_ALLCODEWORD  3706 // Âë×Ö×ÜÊý×î´óÖµ
#define MAX_DATACODEWORD 2956 // Êý¾ÝÂë×Ö×î´óÖµ£¨°æ±¾40-L£©
#define MAX_CODEBLOCK     153 // Ä£¿éÊý¾ÝÂë×ÖÊý×î´óÖµ£¨°üÀ¨RSÂë×Ö£©
#define MAX_MODULESIZE    177 //  Ä£¿éÊý×î´óÖµ177*177

#define QR_MARGIN       4
#define min(a,b) (((a) < (b)) ? (a) : (b))

/////////////////////////////////////////////////////////////////////////////

typedef struct tagRS_BLOCKINFO
{
        int ncRSBlock;          // ¾À´íÂë×ÖÊä
        int ncAllCodeWord;      // ×ÜÂë×ÖÊý
        int ncDataCodeWord;     // Êý¾ÝÂë×ÖÊý(×ÜÂë×ÖÊý-¾À´íÂë×ÖÊý)
} RS_BLOCKINFO, *LPRS_BLOCKINFO;


/////////////////////////////////////////////////////////////////////////////
//QRcode°æ±¾ÐÅÏ¢
typedef struct tagQR_VERSIONINFO
{
        int nVersionNo;    // °æ±¾ºÅ1-40
        int ncAllCodeWord; // ×ÜÂë×ÖÊý
        int ncDataCodeWord[4];  // Êý¾ÝÂë×Ö£¨×ÜÂë×Ö-RSÂë×Ö£©

        int ncAlignPoint;       // Ð£ÕýµãÊý
        int nAlignPoint[6];     // Ð£ÕýÍ¼ÐÎÖÐÐÄ×ø±ê

        RS_BLOCKINFO RS_BlockInfo1[4]; // ¾À´í¿éÐÅÏ¢
        RS_BLOCKINFO RS_BlockInfo2[4]; // ¾À´í¿éÐÅÏ¢

} QR_VERSIONINFO, *LPQR_VERSIONINFO;


/////////////////////////////////////////////////////////////////////////////
// CQR_Encode Àà

class CQR_Encode
{
// ¹¹Ôìº¯Êý
public:
        CQR_Encode();
        ~CQR_Encode();

public:
        int m_nLevel;           // ¾À´íË®Æ½
        int m_nVersion;         // °æ±¾

        int m_nMaskingNo;       // ÑÚÄ¤·½Ê½
public:
        int m_nSymbleSize;
        unsigned char m_byModuleData[MAX_MODULESIZE][MAX_MODULESIZE];

private:
        int m_ncDataCodeWordBit; // Êý¾ÝÂë×ÖÎ»
        unsigned char m_byDataCodeWord[MAX_DATACODEWORD]; //Êý¾ÝÂë×Ö
        int m_ncDataBlock;
        unsigned char m_byBlockMode[MAX_DATACODEWORD];
        int m_nBlockLength[MAX_DATACODEWORD];

        int m_ncAllCodeWord; // ×ÜÂë×Ö
        unsigned char m_byAllCodeWord[MAX_ALLCODEWORD];
        unsigned char m_byRSWork[MAX_CODEBLOCK];


public:
        int EncodeData(int nLevel, int nVersion, int bAutoExtent, int nMaskingNo, const char * lpsSource, int ncSource = 0);

private:
        int GetEncodeVersion(int nVersion, const char * lpsSource, int ncLength);//»ñµÃ±àÂë°æ±¾
        int EncodeSourceData(const char * lpsSource, int ncLength, int nVerGroup);//±àÂë

        int GetBitLength(unsigned char nMode, int ncData, int nVerGroup);//»ñµÃÎ»Á÷³¤¶È

        int SetBitStream(int nIndex, unsigned short wData, int ncData);

        int IsNumeralData(unsigned char c);//ÊÇ·ñÊÇÊý×Ö
        int IsAlphabetData(unsigned char c);//ÊÇ·ñÊÇ×ÖÄ¸
        int IsHanjiData(unsigned char c1, unsigned char c2);//ÊÇ·ñºº×Ö

        unsigned char AlphabetToBinaly(unsigned char c);//×Ö·û-->Êý×Ö
        unsigned short KanjiToBinaly(unsigned short wc);
        void GetRSCodeWord(unsigned char *lpbyRSWork, int ncDataCodeWord, int ncRSCodeWord);

private:
        void FormatModule();
        void SetFunctionModule();
        void SetFinderPattern(int x, int y);
        void SetAlignmentPattern(int x, int y);
        void SetVersionPattern();
        void SetCodeWordPattern();
        void SetMaskingPattern(int nPatternNo);
        void SetFormatInfoPattern(int nPatternNo);
        int CountPenalty();
};
#endif //QR_CODE_H
