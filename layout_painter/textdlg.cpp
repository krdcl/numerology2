#include "textdlg.h"


CTextDlg::CTextDlg(CAppStyle *style, QWidget *parent, bool disableB)
    : QWidget(parent)
{
//    data = cdata;
//    settings = csettings;
//    q = settings->q;
    appstyle = style;
#ifdef Q_OS_MAC
    qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/");
#else
    qsResPath = QCoreApplication::applicationDirPath() + QString("/images/");
#endif

    res = -1;

    m_pOk = new QPushButton("Ok", this );
    m_pOk->resize(80, 20);
    QObject::connect(m_pOk, SIGNAL(clicked()), this, SLOT(OnOk()));

    m_pCancel = new QPushButton("Cancel", this );
    m_pCancel->resize(80, 20);
    QObject::connect(m_pCancel, SIGNAL(clicked()), this, SLOT(OnCancel()));


    m_Align = Qt::AlignCenter;

    QSize WidSz(350, 300);

    int sy=10;

    m_pFontCombo = new QFontComboBox(this);
    m_pFontCombo->setGeometry(10, sy+5, 240, 25);
    m_pFontCombo->setFontFilters(QFontComboBox::ScalableFonts);
    m_pFontCombo->setCurrentFont(QFont("Arial"));
    connect(m_pFontCombo, SIGNAL(activated(const QString &)), this, SLOT(OnTextFamily(const QString &)));

    m_pSizeCombo = new QComboBox(this);
    m_pSizeCombo->setGeometry(260, sy+5, 80, 25);
    m_pSizeCombo->setEditable(true);

    QFontDatabase db;
    foreach(int size, db.standardSizes())
        m_pSizeCombo->addItem(QString::number(size));
    connect(m_pSizeCombo, SIGNAL(activated(const QString &)), this, SLOT(OnTextSize(const QString &)));

    m_pTextEdit = new QTextEdit(this);
    m_pTextEdit->setGeometry(10,sy+78,WidSz.width()-20,WidSz.height()-118-sy);
    m_pTextEdit->setFontPointSize(10);
    m_pTextEdit->setReadOnly(true);

    //m_pFontToolBar = new QToolBar(this);
    //m_pFontToolBar->move(10, sy+28);



    int asy=45;
    actionTextBold = new QToolButton(this);
    actionTextBold->setToolTip("Bold");
    actionTextBold->resize(30, 30);
    actionTextBold->move(10, asy);
    QObject::connect(actionTextBold, SIGNAL(clicked()), this, SLOT(OnTextBold()));

    if (disableB == true)
        actionTextBold->hide();

    actionTextBold->setIcon(QIcon(qsResPath+"text-bold.png"));
    actionTextBold->setIconSize(QSize(24,24));
    actionTextBold->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    actionTextBold->setCheckable(true);

    actionTextItalic = new QToolButton(this);
    actionTextItalic->setToolTip("Italic");
    actionTextItalic->resize(30, 30);
    if (disableB == true)
        actionTextItalic->move(10, asy);
    else
        actionTextItalic->move(45, asy);
    QObject::connect(actionTextItalic, SIGNAL(clicked()), this, SLOT(OnTextItalic()));
    actionTextItalic->setIcon(QIcon(qsResPath+"text-italic.png"));
    actionTextItalic->setIconSize(QSize(24,24));
    actionTextItalic->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    actionTextItalic->setCheckable(true);

    actionTextUnderline = new QToolButton(this);
    actionTextUnderline->setToolTip("Underline");
    actionTextUnderline->resize(30, 30);
    if (disableB == true)
        actionTextUnderline->move(45, asy);
    else
        actionTextUnderline->move(80, asy);
    QObject::connect(actionTextUnderline, SIGNAL(clicked()), this, SLOT(OnTextUnderline()));
    actionTextUnderline->setIcon(QIcon(qsResPath+"text-under.png"));
    actionTextUnderline->setIconSize(QSize(24,24));
    actionTextUnderline->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    actionTextUnderline->setCheckable(true);

    /*QActionGroup *grp = new QActionGroup(this);
    connect(grp, SIGNAL(triggered(QAction *)), this, SLOT(OnTextAlign(QAction *)));

    actionAlignLeft = new QAction(settings->appstyle->icnAlignLeft, "Left", grp);
    actionAlignLeft->setCheckable(true);
    actionAlignCenter = new QAction(settings->appstyle->icnAlignCenter, "Center", grp);
    actionAlignCenter->setCheckable(true);
    actionAlignRight = new QAction(settings->appstyle->icnAlignRight, "Right", grp);
    actionAlignRight->setCheckable(true);

    actionTextColor = new QAction(settings->appstyle->icnSearchOn, tr("&Color..."), this);
    connect(actionTextColor, SIGNAL(triggered()), this, SLOT(OnTextColor()));*/


    m_pTextEdit->setFocus();


    appstyle->setWidgetStyle(this, WTDlg);

    appstyle->setWidgetStyle(m_pOk, WTButton);
    appstyle->setWidgetStyle(m_pCancel, WTButton);

    appstyle->setWidgetStyle(m_pTextEdit, WTEdit);
    appstyle->setWidgetStyle(m_pSizeCombo, WTCombo);
    appstyle->setWidgetStyle(m_pFontCombo, WTCombo);

    appstyle->setWidgetStyle(actionTextBold, WTButton);
    appstyle->setWidgetStyle(actionTextItalic, WTButton);
    appstyle->setWidgetStyle(actionTextUnderline, WTButton);

    parentWidget()->setWindowTitle("Style");

    resize(WidSz);
    parentWidget()->resize(WidSz);
    parentWidget()->setFixedSize(WidSz);
    parentWidget()->setWindowFlags(Qt::Dialog | Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::CustomizeWindowHint);

    int px=0, py=0;
    if (parentWidget()->parentWidget()!=NULL)
    {
        px = parentWidget()->parentWidget()->parentWidget()->x() + (parentWidget()->parentWidget()->parentWidget()->width()-this->width())/2;
        py = parentWidget()->parentWidget()->parentWidget()->y() + (parentWidget()->parentWidget()->parentWidget()->height()-this->height())/2;
    } else
    {
        QDesktopWidget desktopWid;
        px = (desktopWid.width()-this->width())/2;
        py = (desktopWid.height()-this->height())/2;
    }
    if (px<0) px=0; if (py<0) py=0;
    parentWidget()->move(px, py);
}


CTextDlg::~CTextDlg()
{
}


void CTextDlg::SetText(QString Text)
{
    m_Text = Text;
    m_pTextEdit->setText(m_Text);
}


QString CTextDlg::GetText()
{
    m_Text = m_pTextEdit->document()->toPlainText();
    return m_Text;
}


void CTextDlg::SetFont(QFont Font)
{
    if (Font.family().isEmpty())
    {
        Font.setFamily("Arial");
        Font.setPixelSize(11);
    }

    if (Font.pixelSize() <= 0)
        Font.setPixelSize(12);

    m_Font = Font;

    m_pTextEdit->selectAll();
    m_pTextEdit->setFont(m_Font);

    m_pFontCombo->setCurrentIndex(m_pFontCombo->findText(QFontInfo(Font).family()));
    int CurIndex = m_pSizeCombo->findText(QString::number(Font.pixelSize()));
    if( CurIndex >= 0 )
        m_pSizeCombo->setCurrentIndex(CurIndex);
    m_pSizeCombo->setEditText(QString::number(Font.pixelSize()));

    actionTextBold->setChecked((Font.weight()==QFont::Bold)?true:false);
    actionTextItalic->setChecked(Font.italic());
    actionTextUnderline->setChecked(Font.underline());
}


QFont CTextDlg::GetFont()
{
    return m_Font;
}


void CTextDlg::SetAlign(int Align)
{
    m_Align = Align;
    m_pTextEdit->selectAll();
    m_pTextEdit->setAlignment((Qt::Alignment)Align);

    /*actionAlignLeft->setChecked(Align == Qt::AlignLeft);
    actionAlignCenter->setChecked(Align == Qt::AlignHCenter);
    actionAlignRight->setChecked(Align == Qt::AlignRight);*/
}


int CTextDlg::GetAlign()
{
    return m_Align;
}


void CTextDlg::SetColor(QColor Clr)
{
    m_Color = Clr;

    m_pTextEdit->selectAll();
    m_pTextEdit->setTextColor(Clr);

    /*QPixmap pix(16, 16);
    pix.fill(Clr);
    actionTextColor->setIcon(pix);*/
}


QColor CTextDlg::GetColor()
{
    return m_Color;
}


void CTextDlg::OnTextFamily(const QString &Family)
{
    m_pTextEdit->selectAll();
    m_pTextEdit->setFontFamily(Family);
    m_Font.setFamily(Family);
    m_pTextEdit->setFocus();
}

void CTextDlg::OnTextSize(const QString &SizeStr)
{
    m_pTextEdit->selectAll();
    m_Font.setPixelSize(SizeStr.toInt());
    m_pTextEdit->setFont(m_Font);
    m_pTextEdit->setFocus();
}


void CTextDlg::OnTextBold()
{
    m_pTextEdit->selectAll();
    m_pTextEdit->setFontWeight(actionTextBold->isChecked() ? QFont::Bold : QFont::Normal);
    m_Font.setBold(actionTextBold->isChecked());
}


void CTextDlg::OnTextItalic()
{
    m_pTextEdit->selectAll();
    m_pTextEdit->setFontItalic(actionTextItalic->isChecked());
    m_Font.setItalic(actionTextItalic->isChecked());
}


void CTextDlg::OnTextUnderline()
{
    m_pTextEdit->selectAll();
    m_pTextEdit->setFontUnderline(actionTextUnderline->isChecked());
    m_Font.setUnderline(actionTextUnderline->isChecked());
}


void CTextDlg::OnTextAlign(QAction *)
{
    int Align = Qt::AlignLeft;
    m_pTextEdit->selectAll();
/*    if (a == actionAlignLeft)
        Align = Qt::AlignLeft;
    else if (a == actionAlignCenter)
        Align = Qt::AlignHCenter;
    else if (a == actionAlignRight)
        Align = Qt::AlignRight;*/

    m_pTextEdit->setAlignment((Qt::Alignment)Align);
    m_Align = Align;
}


void CTextDlg::OnTextColor()
{
    QColor Clr = QColorDialog::getColor(m_pTextEdit->textColor(), this);
    if( !Clr.isValid() )
        return;

    m_pTextEdit->selectAll();
    m_pTextEdit->setTextColor(Clr);

    /*QPixmap pix(16, 16);
    pix.fill(Clr);
    actionTextColor->setIcon(pix);*/

    m_Color = Clr;
}


void CTextDlg::OnCancel()
{
    res = -1;
    parentWidget()->close();
}


void CTextDlg::OnOk()
{
    QString SizeStr = m_pSizeCombo->lineEdit()->text();
    int Size = SizeStr.toInt();
    if(!(Size <= 0))
        m_Font.setPixelSize(Size);

    res = 0;
    parentWidget()->close();
}


void CTextDlg::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    //painter.setRenderHints( QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform );

    QStyleOption opt;
    opt.init(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);
}


void CTextDlg::resizeEvent(QResizeEvent *event)
{
//	ASProcessResizeEvent(this, event);

    int w = event->size().width();
    int h = event->size().height();

#ifdef Q_WS_WIN
    m_pOk->move(w/2-90, h-30);
    m_pCancel->move(w/2+10, h-30);
#else
    m_pOk->move(w/2+10, h-30);
    m_pCancel->move(w/2-90, h-30);
#endif

}


CTextDlgWin::CTextDlgWin(CAppStyle *style, QWidget *parent, bool disableB)
: QDialog(parent)
{
    //ASCreateWindow(this);

    mainWidget = new CTextDlg(style, this, disableB);
}


CTextDlgWin::~CTextDlgWin()
{ }
