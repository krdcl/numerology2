#include "thumbailsettings.h"


QImage CManagerThumbnailDlg::slotPrintReport(int id, CReportCreatorWin *dlg, QSize scale)
{
    QImage imageOut(QSize(10, 10), QImage::Format_ARGB32);
    imageOut.fill(Qt::white);

    if (dlg->mainWidget->pageArea->page != NULL )
    {
        QSize sizeP;
        sizeP.setWidth((dlg->mainWidget->pageArea->page->getPageSize().width() / PIX_MM));
        sizeP.setHeight((dlg->mainWidget->pageArea->page->getPageSize().height() / PIX_MM));
#if defined(Q_OS_IOS) || defined(Q_OS_MAC)
        QImage img(sizeP * 2, QImage::Format_ARGB32);
#else
        QImage img(sizeP, QImage::Format_ARGB32);
#endif

        if (scale != QSize(0, 0))
        {
            img = img.scaled(scale, Qt::KeepAspectRatio);
            sizeP = img.size();
        }
        else
        {
            img = img.scaled(img.size() * 4, Qt::KeepAspectRatio);
            sizeP = img.size();
        }

        CDrawMode drawMode;
        drawMode.finalRender = true;

        // Printing
        QList<int> ilist;
        ilist.append(id);

        if (ilist.size()>0)
        {
            int iidx=0;
            int siidx=iidx;
            int pageNum=1;
            // All reports

            for (int rn=0; rn<dlg->mainWidget->pageArea->report.size(); rn++)
            {

                // All page templates in report
                for (int rp=0; rp<dlg->mainWidget->pageArea->report.at(rn)->pages.size(); rp++)
                {
                    CREPage *cpage = dlg->mainWidget->pageArea->report.at(rn)->pages.at(rp);
                    int templItNum = cpage->m_PageSeq;
                    if (templItNum==0)
                        templItNum = 1000000;

                    // Print template
                    for (int tn=0; tn<templItNum; tn++)
                    {

                        siidx=iidx;

                        if (iidx>=ilist.size())
                            break;
                        // Preparing Template
                        cpage->m_itemid = ilist.at(iidx);
                        cpage->m_pnum = pageNum;
                        cpage->prefillData();
                        bool hasDataItems = preparePrintReportBlock(cpage);
                        if (hasDataItems)
                            iidx++;

                        // Preparing Blocks int template
                        QList<CREPageItem*> blks; // List of blocks with data on page
                        for (int a=0; a<cpage->m_PageObjects.size(); a++)
                        {
                            CREPageItem *pitem = cpage->m_PageObjects.at(a);
                            if (pitem->m_pContainer!=NULL)
                            {

                                if (pitem->m_pContainer->m_Type == OT_BLOCK)
                                {
                                    CREPage *block = (CREPage*)pitem->m_pContainer->m_pData;
                                    if (preparePrintReportBlock(block))
                                    {
                                        int blkpos=0;
                                        for (blkpos=0; blkpos<blks.size(); blkpos++)
                                        {
                                            if (blks.at(blkpos)->m_iopt > pitem->m_pContainer->m_iopt)
                                                break;
                                        }
                                        blks.insert(blkpos, pitem->m_pContainer);
                                    } else
                                    {
                                        pitem->m_itemid = ilist.at(0);
                                        pitem->m_pnum = pageNum;
                                        pitem->m_pContainer->m_Idx = 0;
                                    }
                                }
                            }
                        }

                        img.fill(Qt::white);

                        QPainter painter;
                        painter.begin(&img);
#ifdef Q_OS_IOS
                        painter.scale(2, 2);
#endif

                        cpage->draw(&painter, QRectF(0,0,img.width(), img.height()), &drawMode, dlg->mainWidget->pageArea->style);

                        imageOut = img;

                        pageNum++;

                        if (siidx==iidx)
                            break;
                    }
                    if (iidx>=ilist.size())
                        break;
                }
                if (iidx>=ilist.size())
                    break;
            }
        }

    }

    return imageOut;

}

bool CManagerThumbnailDlg::preparePrintReportBlock(CREPage *block)
{
    bool hasDataField=false;
    for (int i=0; i<block->m_PageObjects.size(); i++)
    {
        CREPageItem *item = block->m_PageObjects.at(i);
        if (item->m_pContainer!=NULL)
        {
            item = item->m_pContainer;
            if (item->m_Type == OT_DATA_FIELD)
            {
                hasDataField = true;
            } else if (item->m_Type == OT_DATA_FIELD_NAME)
            {
            } else if (item->m_Type == OT_DATA_IMAGE)
            {
                hasDataField = true;
            }
            else if (item->m_Type == OT_BAR_CODE)
            {
               hasDataField = true;
            }
        }
    }
    return hasDataField;
}
