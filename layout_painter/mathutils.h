#ifndef MathUtilsH
#define MathUtilsH

#include <math.h>
#include <QPointF>
#include <QSizeF>


#define MATH_PI 3.14159265358979323846264338327950288419717
#define MATH_2PI (2.0*MATH_PI)

#define __MIN(a,b) (((a) < (b)) ? (a) : (b))
#define __MAX(a,b) (((a) > (b)) ? (a) : (b))
#define __MIN3(a, b, c) __MIN( __MIN(a, b), c )
#define __MAX3(a, b, c) __MAX( __MAX(a, b), c )

/*
struct BPADoublePoint
{
public:
	double m_x;
	double m_y;

	BPADoublePoint()
	{
		m_x = 0;
		m_y = 0;
	}

	BPADoublePoint(double x, double y)
	{
		m_x = x;
		m_y = y;
	}
};

typedef BPADoublePoint clDoublePoint;*/

double cround(double Argument, int Precision);

double findAngleBetweenVectors(QPointF *Centre, QPointF *V1_P, QPointF *V2_P);

double calcFitScale(int srcW, int srcH, int DstW, int DstH, int &fitOffX, int &fitOffY );


/*int mm_to_px(double mm);
int in_to_px(double mm);
double px_to_mm(int px);
double px_to_mm(double px);
double px_to_in(int px);
double px_to_in(double px);

BPADoublePoint RotatePoint(BPADoublePoint *pCentre, BPADoublePoint *pPoint, double Angle);
double FindDistanceBetweenPoints(BPADoublePoint *pP1, BPADoublePoint *pP2);
BPADoublePoint FindPointOnLine(BPADoublePoint *pSP, BPADoublePoint *pEP, BPADoublePoint *pPt, double d);
bool FindPointPerpendToLine(BPADoublePoint *pSP, BPADoublePoint *pEP, BPADoublePoint *pPt, double d, BPADoublePoint *pResPt);
bool FindEqualDistPointsOnLine(BPADoublePoint *pSP, BPADoublePoint *pEP, BPADoublePoint *pPt, BPADoublePoint *pP1, BPADoublePoint *pP2, double *pDist);
double FindAngleBetweenLines(BPADoublePoint *pL1_P1, BPADoublePoint *pL1_P2, BPADoublePoint *pL2_P1, BPADoublePoint *pL2_P2);
bool CompareLinePoints(BPADoublePoint *pSP, BPADoublePoint *pEP, BPADoublePoint *pP0, BPADoublePoint *pP1);
bool IsPointsOnLine(BPADoublePoint *pP1, BPADoublePoint *pP2, BPADoublePoint *pP3, BPADoublePoint *pP4);
QSizeF FitImgSizeToOwnerSize(QSizeF ImgSz, QSizeF OwnerSz, double EnlargePerc = 1.02);
QSizeF FitImgSizeToPageSize(QSizeF ImgSz, QSizeF PageSz);*/

#endif
