#ifndef REPORTCREATOR_H
#define REPORTCREATOR_H

#define REPORT_MODE 0
#define THUMB_MODE 1

#include <QtGui>
#include <QDialog>
#include "reportpe.h"
#ifndef VER_MOBILE
#include "propertieswidget.h"
#include "reportpepages.h"
#include "papersizedlg.h"





class CReportCreatorW1 : public QWidget
{
    Q_OBJECT

public:
    CReportCreatorW1(QWidget *parent=NULL) : QWidget(parent)
    { }

protected:
    void resizeEvent(QResizeEvent *)
    { emit updateWidgetGeometry(); }

signals:
    void updateWidgetGeometry();
};

class CReportCreatorW2 : public QWidget
{
    Q_OBJECT

public:
    CReportCreatorW2(QWidget *parent=NULL) : QWidget(parent)
    { }

protected:
    void resizeEvent(QResizeEvent *)
    { emit updateWidgetGeometry(); }

signals:
    void updateWidgetGeometry();
};

class CReportCreatorW3 : public QWidget
{
    Q_OBJECT

public:
    CReportCreatorW3(QWidget *parent=NULL) : QWidget(parent)
    { }

protected:
    void resizeEvent(QResizeEvent *)
    { emit updateWidgetGeometry(); }

signals:
    void updateWidgetGeometry();
};

#endif


class CReportCreator : public QWidget
{
	Q_OBJECT
#ifndef VER_MOBILE
protected:

    void resizeEvent(QResizeEvent *event);
    void keyPressEvent(QKeyEvent *ev)
    {
        if (ev->key() == Qt::Key_Alt)
            isAddTitle = true;

        if (ev->key() == Qt::Key_Shift)
            fieldsList->setDragEnabled(false);
    }

    void keyReleaseEvent(QKeyEvent *ev)
    {
        if (ev->key() == Qt::Key_Alt)
            isAddTitle = false;

        if (ev->key() == Qt::Key_Shift)
            fieldsList->setDragEnabled(true);
    }
#endif

public:
    CReportCreator(calcNumContainer *pNumContainer, CAppStyle *style, /*CDataFile *cdata, CAppSettings *csettings, */QWidget *parent = 0, int mode = REPORT_MODE, int *trial = NULL);
    ~CReportCreator();

    int m_mode;
    int *isTrial;

    background groundTmp;
    CAppStyle *appstyle;

    QString qsResPath;

    int editItem;

	int res;
	int mode;

    calcNumContainer *numContainer;

    QString path;

//	CDataFile *data;
//	CAppSettings *settings;
//	CTranslate *q;
#ifndef VER_MOBILE
    QPushButton *m_pOk;
    QPushButton *m_pCancel;
    QPushButton *m_pZoomIn;
    QPushButton *m_pZoomOut;
    QPushButton *m_pZoomNormal;
    QPushButton *m_pProperties;
    QPushButton *m_pDelete;
    QPushButton *m_pGrid;
    QPushButton *m_pSnap;
    QPushButton *m_pBoundaries;
    QPushButton *m_pMoveToFront;
    QPushButton *m_pMoveToBack;
    QSplitter *m_pSplitter;
    CReportCreatorW1 *m_pW1;
    CReportCreatorW2 *m_pW2;
    CReportCreatorW3 *m_pW3;

    QTreeWidget *fieldsList;
    QLabel *l1, *l2, *l3, *l4, *l5;
#endif
    CReportPE *pageArea;
#ifndef VER_MOBILE
    CPropertiesWidget *propList;
    CReportPEPages *repPages;

    QList<QTreeWidgetItem*> flitems;


    void updatePagesList();
	void applyItems();
	void updateFields();
	void updateFieldsSel();
	void updateStates();

    void setBlockOrderFromBlocksList();
    void setPageOrderFromPagesList();

	void init();

public slots:
	void OnCancel();
	void OnOk();

    void OnProperties();
    void slotOnBackground();
    void OnDelete();

    void OnAddField(QPoint pos);
    void OnBlockItemDropped(CILItem *, QPoint);
    void OnPageSelectionChanged();
    void OnPropertyChanged();

    void OnUpdateWidgetGeometry();

    void OnAddBlock();
    void OnDelBlock();
    void OnBlockProp();
    void OnAddPage();
    void OnDelPage();
    void OnPageProp();
    void OnListDrop();
    void OnBlockDrop();


    void OnPageSelected(CILItem *);

private:

    void itemConverter(CREPageItem *item, CPropertiesWidget *propList, int propertiesId, bool *isUpdate);
    QStringList idFieldsListForBarC;
    QStringList idFieldsList;
#endif
    bool isAddTitle;

signals:
    void rigister();


};


class CReportCreatorWin : public QDialog
{
	Q_OBJECT

public:
    CReportCreatorWin(calcNumContainer *pNumContainer, CAppStyle *style, QWidget *parent=NULL, int mode = REPORT_MODE, int *trial = NULL);
    ~CReportCreatorWin();

	CReportCreator *mainWidget;
private:
#ifndef VER_MOBILE
protected:
	void keyPressEvent(QKeyEvent *e)
	{
		if ((e->key()==Qt::Key_Enter) || (e->key()==Qt::Key_Return) || (e->key()==Qt::Key_Escape)) {}
		else { QDialog::keyPressEvent(e); return; }
	}
    void resizeEvent(QResizeEvent *event)
    { mainWidget->resize(event->size()); }
#endif
};

#endif // REPORTCREATOR_H
