#include "propertieswidget.h"
#include "textdlg.h"
#include "ccolordialog.h"

CPropertiesWidget::CPropertiesWidget(CAppStyle *style, /*CDataFile *cdata, CAppSettings *csettings, */QWidget *parent) :
    QWidget(parent)
{
    //data = cdata;
    //settings = csettings;
    //q = settings->q;

#ifdef Q_OS_MAC
    qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/");
#else
    qsResPath = QCoreApplication::applicationDirPath() + QString("/images/");
#endif



    appstyle = style;

    tablelist = new QTableWidget(this);
    tablelist->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    tablelist->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    tablelist->setShowGrid(false);

    tablelist->setRowCount(0);
    tablelist->setColumnCount(2);
    tablelist->verticalHeader()->hide();
    tablelist->setSelectionMode(QAbstractItemView::SingleSelection);

    lableFixScroll = new QLabel(this);
    lableFixScroll->setStyleSheet("border: 0px;");

    clear();
}


CPropertiesWidget::~CPropertiesWidget()
{
    clear();
}


void CPropertiesWidget::resizeEvent(QResizeEvent *)
{
    int w=this->width(), h=this->height();

    tablelist->resize(w-5, h-4);
    tablelist->move(3, 2);
    tablelist->setColumnWidth(0,w*0.4-5);
    tablelist->setColumnWidth(1,(w*0.6-5) - 13);

    updateFixScroll();
}


void CPropertiesWidget::clear()
{
    disconnect(tablelist, SIGNAL(itemChanged(QTableWidgetItem *)), this, SLOT(OnItemsChanged(QTableWidgetItem*)));
    tablelist->clear();

    for (int a=0; a<m_items.size(); a++)
        delete m_items.at(a);
    m_items.clear();

    QTableWidgetItem *item = new QTableWidgetItem("Property");
    tablelist->setHorizontalHeaderItem(0, item);
    item = new QTableWidgetItem("Value");
    tablelist->setHorizontalHeaderItem(1, item);
    tablelist->setRowCount(0);
}


void CPropertiesWidget::setList(QVector<CPWItem*> items)
{
    clear();
    m_items = items;

    tablelist->setRowCount(m_items.size());
    for (int a=0; a<m_items.size(); a++)
    {
        tablelist->setRowHeight(a, 22);

        QTableWidgetItem *item = new QTableWidgetItem(m_items.at(a)->m_name);
        tablelist->setItem(a, 0, item);
        item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

        if (m_items.at(a)->but!=NULL)
            delete m_items.at(a)->but;
        if (m_items.at(a)->combo!=NULL)
            delete m_items.at(a)->combo;

        if (m_items.at(a)->m_type == PWIT_STRING)
        {
            item = new QTableWidgetItem(m_items.at(a)->m_str);
            item->setFlags(Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable);
            tablelist->setItem(a, 1, item);
        }
        if (m_items.at(a)->m_type == PWIT_COLOR)
        {
            m_items.at(a)->but = new QPushButton("...");
            m_items.at(a)->but->resize(15, 15);
            connect(m_items.at(a)->but, SIGNAL(clicked()), m_items.at(a), SLOT(OnItemButton()));
            connect(m_items.at(a), SIGNAL(itemButtonClicked(CPWItem*)), this, SLOT(OnItemsActions(CPWItem*)));
            tablelist->setCellWidget(a, 1, m_items.at(a)->but);
        }
        if (m_items.at(a)->m_type == PWIT_FONT)
        {
            m_items.at(a)->but = new QPushButton("...");
            connect(m_items.at(a)->but, SIGNAL(clicked()), m_items.at(a), SLOT(OnItemButton()));
            connect(m_items.at(a), SIGNAL(itemButtonClicked(CPWItem*)), this, SLOT(OnItemsActions(CPWItem*)));
            tablelist->setCellWidget(a, 1, m_items.at(a)->but);
        }
        if (m_items.at(a)->m_type == PWIT_ALIGN)
        {
            m_items.at(a)->combo = new QComboBox(this);
            m_items.at(a)->combo->addItem("Left");
            m_items.at(a)->combo->addItem("Center");
            m_items.at(a)->combo->addItem("Right");

            int alm = m_items.at(a)->m_alignement;
            int idx=0;
            if (alm == Qt::AlignLeft)
                idx=0;
            else if (alm == Qt::AlignHCenter)
                idx=1;
            else if (alm == Qt::AlignRight)
                idx=2;

            //settings->appstyle->setWidgetStyle(m_items.at(a)->combo, DIDEntry, WTCombo);
            m_items.at(a)->combo->setCurrentIndex(idx);
            connect(m_items.at(a)->combo, SIGNAL(currentIndexChanged(int)), m_items.at(a), SLOT(OnItemCombo(int)));
            connect(m_items.at(a), SIGNAL(itemButtonClicked(CPWItem*)), this, SLOT(OnItemsActions(CPWItem*)));
            tablelist->setCellWidget(a, 1, m_items.at(a)->combo);
        }
        if ((m_items.at(a)->m_type == PWIT_COMBO) || (m_items.at(a)->m_type == PWIT_ECOMBO))
        {
            m_items.at(a)->combo = new QComboBox(this);
            m_items.at(a)->combo->addItems(m_items.at(a)->m_comboVals);
            if ((m_items.at(a)->m_comboIdx>=0) && (m_items.at(a)->m_comboIdx<m_items.at(a)->m_comboVals.size()))
                m_items.at(a)->combo->setCurrentIndex(m_items.at(a)->m_comboIdx);
            else
                m_items.at(a)->combo->setCurrentIndex(0);

            if (m_items.at(a)->m_type == PWIT_ECOMBO)
            {
                m_items.at(a)->combo->setEditable(true);
                m_items.at(a)->combo->setEditText(m_items.at(a)->m_str);
            }

            //settings->appstyle->setWidgetStyle(m_items.at(a)->combo, DIDEntry, WTCombo);
            connect(m_items.at(a)->combo, SIGNAL(currentIndexChanged(int)), m_items.at(a), SLOT(OnItemCombo(int)));
            connect(m_items.at(a), SIGNAL(itemButtonClicked(CPWItem*)), this, SLOT(OnItemsActions(CPWItem*)));
            tablelist->setCellWidget(a, 1, m_items.at(a)->combo);
        }
    }

    connect(tablelist, SIGNAL(itemChanged(QTableWidgetItem *)), this, SLOT(OnItemsChanged(QTableWidgetItem*)));
}


void CPropertiesWidget::OnItemsActions(CPWItem*item)
{
    for (int a=0; a<m_items.size(); a++)
    {
        if (item==m_items.at(a))
        {
            if (m_items.at(a)->m_type == PWIT_COLOR)
            {
//                QColorDialog *dlg = new QColorDialog(m_items.at(a)->m_col);
//                if (dlg->exec()==QDialog::Accepted)
//                {
//                    m_items.at(a)->m_col = dlg->currentColor();
//                }
                CColorDialog *dlg = new CColorDialog(/*settings, */true);
                dlg->slotSetColor(m_items.at(a)->m_col);
                dlg->slotUpdate();
                dlg->setWindowModality(Qt::ApplicationModal);
                dlg->exec();

               if (dlg->ground.drawMode != CANCEL_DLG)
               {
                   m_items.at(a)->m_col = dlg->ground.color;
               }

                dlg->hide();
                delete dlg;
            }
            if (m_items.at(a)->m_type == PWIT_FONT)
            {
                CTextDlgWin *dlg = new CTextDlgWin(appstyle, parentWidget()->parentWidget()->parentWidget());
                dlg->mainWidget->SetFont(m_items.at(a)->m_font);
                if (m_items.at(0)->m_str.size()>0)
                    dlg->mainWidget->SetText(m_items.at(a)->m_str);
                else
                    dlg->mainWidget->SetText("Text");
                dlg->exec();
                int res = dlg->mainWidget->res;
                if (res==0)
                {
                    m_items.at(a)->m_font = dlg->mainWidget->GetFont();
                    //m_items.at(0)->m_str = dlg->mainWidget->GetText();
                }
                delete dlg;
            }
            if (m_items.at(a)->m_type == PWIT_ALIGN)
            {
                int idx = m_items.at(a)->combo->currentIndex();
                int alm = Qt::AlignLeft;
                if (idx==0)
                    alm = Qt::AlignLeft;
                else if (idx==1)
                    alm = Qt::AlignHCenter;
                else if (idx==2)
                    alm = Qt::AlignRight;

                m_items.at(a)->m_alignement = alm;
            }
            if (m_items.at(a)->m_type == PWIT_COMBO)
            {
                int idx = m_items.at(a)->combo->currentIndex();
                if ((idx<0) || (idx>m_items.at(a)->m_comboVals.size()))
                    idx = 0;
                m_items.at(a)->m_comboIdx = idx;
            }
            if (m_items.at(a)->m_type == PWIT_ECOMBO)
            {
                m_items.at(a)->m_str = m_items.at(a)->combo->currentText();
            }
            break;
        }
    }

    OnItemsChanged(NULL);
}


void CPropertiesWidget::OnItemsChanged(QTableWidgetItem*)
{
    for (int a=0; a<m_items.size(); a++)
    {
        if (m_items.at(a)->m_type == PWIT_STRING)
        {
            QTableWidgetItem *item = tablelist->item(a, 1);
            if (item != NULL)
            {
                m_items.at(a)->m_str = item->text();
            }
        }
    }
    emit dataChanged();
}

void CPropertiesWidget::updateFixScroll()
{

    lableFixScroll->resize(35, 1);
    QImage hline(qsResPath + "hline.png");
    hline = appstyle->updateImage(hline);
    lableFixScroll->setPixmap(QPixmap::fromImage(hline));
    lableFixScroll->move(width() - 35 - 3, height() - 3);

}

//void CPropertiesWidget::paintEvent(QPaintEvent *)
//{

//    QStyleOption opt;
//    opt.init(this);
//    QPainter painter(this);
//    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);
//}
