#include "ccolordialog.h"
#include "QMouseEvent"
#include <qfiledialog.h>
//#include "appsettings.h"
//#include "infodlg.h"


//----------------------------------------------------------
CModLabel::CModLabel(QWidget *parent)
    : QLabel(parent)
{
    isPressed = false;
}
//----------------------------------------------------------
void CModLabel::mousePressEvent(QMouseEvent *event)
{
    isPressed = true;
    emit signalClick(event->pos());
    QColor col(color);

    emit signalClick(col);
}
//----------------------------------------------------------
void CModLabel::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
    isPressed = false;
}
//----------------------------------------------------------
void CModLabel::mouseMoveEvent(QMouseEvent *event)
{
    if (isPressed == true)
        emit signalClick(event->pos());
}
//----------------------------------------------------------
//----------------------------------------------------------
//--------                                          --------
//----------------------------------------------------------
CColorDialog::CColorDialog(bool colorDiag, QWidget *parent)
    : QDialog(parent)//, q(set->q), settings(set)
{
    cd = colorDiag;
    resize(465, 325);
    setMaximumSize(size());
    setMinimumSize(size());
    setWindowFlags(Qt::Tool);

    if (cd == false)
        setWindowTitle("Background");
    else
        setWindowTitle("Select Color");


    txtBasicCol = new QLabel("Basic colors", this);
    txtBasicCol->resize(160, 20);
    txtBasicCol->move(10, 2);

    CModLabel *item;
    item = new CModLabel(this);
    item->color = qRgb(255, 128, 128);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(255, 0, 0);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(128, 64, 64);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(128, 0, 0);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(64, 0, 0);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 0, 0);
    colorItems << item;

    item = new CModLabel(this);
    item->color = qRgb(255, 255, 128);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(255, 255, 0);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(255, 128, 64);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(255, 128, 0);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(128, 64, 0);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(128, 128, 0);
    colorItems << item;

    item = new CModLabel(this);
    item->color = qRgb(128, 255, 128);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(128, 255, 0);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 255, 0);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 128, 0);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 64, 0);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(128, 128, 64);
    colorItems << item;

    item = new CModLabel(this);
    item->color = qRgb(0, 255, 128);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 255, 64);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 128, 128);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 128, 64);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 64, 64);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(128, 128, 128);
    colorItems << item;

    item = new CModLabel(this);
    item->color = qRgb(128, 255, 255);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 255, 255);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 64, 128);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 0, 255);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 0, 128);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(64, 128, 128);
    colorItems << item;

    item = new CModLabel(this);
    item->color = qRgb(0, 128, 255);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 128, 192);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(128, 128, 255);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 0, 160);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(0, 0, 64);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(192, 192, 192);
    colorItems << item;

    item = new CModLabel(this);
    item->color = qRgb(255, 128, 192);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(128, 128, 192);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(128, 0, 64);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(128, 0, 128);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(64, 0, 64);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(64, 0, 64);
    colorItems << item;

    item = new CModLabel(this);
    item->color = qRgb(255, 128, 255);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(255, 0, 255);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(255, 0, 128);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(128, 0, 255);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(64, 0, 128);
    colorItems << item;
    item = new CModLabel(this);
    item->color = qRgb(255, 255, 255);
    colorItems << item;

    int i = 0;
    for (int x = 0; x < 8; x++)
    {
        for (int y = 0; y < 6; y++)
        {
            colorItems.at(i)->resize(19, 16);
            colorItems.at(i)->move(9 + (26 * x), 25 + (22 * y));
            int r = qRed(colorItems.at(i)->color);
            int g = qGreen(colorItems.at(i)->color);
            int b = qBlue(colorItems.at(i)->color);
            colorItems.at(i)->setStyleSheet("background-color : rgb(" + QString::number(r) + ", " + QString::number(g) + ", " + QString::number(b) + "); border-width: 1px; border-style: solid; border-color: #000000;");
            connect(colorItems.at(i), SIGNAL(signalClick(QColor&)), this, SLOT(slotSelBasicColor(QColor&)));
            i++;
        }
    }

    hue = 179;
    sat = 127;
    val = 127;

    colorSpector = new CModLabel(this);
    colorSpector->resize(190, 187);
    colorSpector->move(229, 8);

    colorSpectorLine = new CModLabel(this);
    colorSpectorLine->resize(22, 187);
    colorSpectorLine->move(435, 8);

    outColor = new QLabel(this);
    outColor->resize(28, 83);
    outColor->move(229, 202);
    outColor->setStyleSheet("border-width: 1px; border-style: solid; border-color: #000000;");

    txtHue = new QLabel("Hue:", this);
    txtHue->resize(50, 25);
    txtHue->move(240, 207 + (32 * 0));
    txtHue->setAlignment(Qt::AlignRight);

    txtSat = new QLabel("Sat:", this);
    txtSat->resize(50, 25);
    txtSat->move(240, 205 + (32 * 1));
    txtSat->setAlignment(Qt::AlignRight);

    txtVal = new QLabel("Val:", this);
    txtVal->resize(50, 25);
    txtVal->move(240, 205 + (32 * 2));
    txtVal->setAlignment(Qt::AlignRight);

    spinHue = new QSpinBox(this);
    spinHue->resize(57, 25);
    spinHue->move(295, 202 + (30 * 0));
    spinHue->setMinimum(0);
    spinHue->setMaximum(359);

    spinSat = new QSpinBox(this);
    spinSat->resize(57, 25);
    spinSat->move(295, 202 + (30 * 1));
    spinSat->setMinimum(0);
    spinSat->setMaximum(255);

    spinVal = new QSpinBox(this);
    spinVal->resize(57, 25);
    spinVal->move(295, 202 + (30 * 2));
    spinVal->setMinimum(0);
    spinVal->setMaximum(255);

    txtRed = new QLabel("Red:", this);
    txtRed->resize(50, 25);
    txtRed->move(347, 207 + (32 * 0));
    txtRed->setAlignment(Qt::AlignRight);

    txtGreen = new QLabel("Green:", this);
    txtGreen->resize(50, 25);
    txtGreen->move(347, 205 + (32 * 1));
    txtGreen->setAlignment(Qt::AlignRight);

    txtBlue = new QLabel("Blue:", this);
    txtBlue->resize(50, 25);
    txtBlue->move(347, 205 + (32 * 2));
    txtBlue->setAlignment(Qt::AlignRight);

    spinRed = new QSpinBox(this);
    spinRed->resize(57, 25);
    spinRed->move(402, 202 + (30 * 0));
    spinRed->setMinimum(0);
    spinRed->setMaximum(359);

    spinGreen = new QSpinBox(this);
    spinGreen->resize(57, 25);
    spinGreen->move(402, 202 + (30 * 1));
    spinGreen->setMinimum(0);
    spinGreen->setMaximum(255);

    spinBlue = new QSpinBox(this);
    spinBlue->resize(57, 25);
    spinBlue->move(402, 202 + (30 * 2));
    spinBlue->setMinimum(0);
    spinBlue->setMaximum(255);

    buttonOk = new QPushButton("OK", this);
    buttonOk->resize(74, 21);
    buttonOk->move(290, 295);

    buttonCancel = new QPushButton("Cancel", this);
    buttonCancel->resize(74, 21);
    buttonCancel->move(374, 295);

    connect(buttonOk, SIGNAL(clicked()), this, SLOT(slotOk()));
    connect(buttonCancel, SIGNAL(clicked()), this, SLOT(slotCancel()));

    spectorImg = new QImage(colorSpector->size(), QImage::Format_RGB32);
    spectorImgLine = new QImage(10, 187, QImage::Format_ARGB32);
    linePointer = new QImage(10, 20, QImage::Format_ARGB32);
    spectorPointer = new QImage(19, 19, QImage::Format_ARGB32);

    linePointer->fill(qRgba(0,0,0,0));
    spectorPointer->fill(qRgba(0,0,0,0));

    for (int x = 0; x < linePointer->width(); x++)
    {
        for (int y = 0; y < linePointer->height(); y++)
        {
            int diap = (linePointer->height() / 2) - x;
            if ((y > diap) && (y < (linePointer->height() - diap)))
            {
                linePointer->setPixel(x, y, qRgba(0,0,0,255));
            }
        }
    }

    for (int x = 0; x < spectorPointer->width(); x++)
    {
        for (int y = 0; y < spectorPointer->height(); y++)
        {
            if (((y > ((spectorPointer->width() / 2) - 2)) && (y < (spectorPointer->width() - ((spectorPointer->width() / 2) - 1))))
               || ((x > ((spectorPointer->height() / 2) - 2)) && (x < (spectorPointer->height() - ((spectorPointer->height() / 2) - 1)))))
            {
                if (!((y > ((spectorPointer->width() / 2) - 5)) && (y < (spectorPointer->width() - ((spectorPointer->width() / 2) - 4))))
                   || !((x > ((spectorPointer->height() / 2) - 5)) && (x < (spectorPointer->height() - ((spectorPointer->height() / 2) - 4)))))
                {
                    spectorPointer->setPixel(x, y, qRgba(0,0,0,255));
                }
            }
        }
    }

    stepX = 360.0 / 190.0;
    stepY = 256.0 / 187.0;

    QColor hsv(QColor::Hsv);
    for (int h = 0; h < 360; h++)
    {
        for (int s = 0; s < 256 ; s++)
        {
            hsv.setHsv(h, 255 - s, 255 - (0.5* s));
            spectorImg->setPixel((double)h / stepX, ((double)s / stepY), hsv.rgb());
        }
    }

    ground.image = QImage();

    txtPreview = new QLabel("Preview", this);
    txtPreview->move(10, 160);
    txtPreview->resize(100, 110);
    txtPreview->setAlignment(Qt::AlignCenter);

    imagePreview = new QLabel(this);
    imagePreview->setStyleSheet("border-width: 1px; border-style: solid; border-color: #000000;");
    imagePreview->move(10, 160);
    imagePreview->resize(100, 110);

    isImage = new QCheckBox("Add Image" ,this);
    isImage->resize(110, 20);
    isImage->move(120, 170);
    connect(isImage, SIGNAL(clicked(bool)), this, SLOT(slotAddImage(bool)));



    openImage = new QPushButton("Open Image", this);
    openImage->resize(100, 21);
    openImage->move(120, 195);
    connect(openImage, SIGNAL(clicked()), this, SLOT(slotLoadImage()));

    oneInOne = new QRadioButton("1:1", this);
    oneInOne->resize(110, 20);
    oneInOne->move(120, 215 + 8);


    fitSize = new QRadioButton("Fit Size", this);
    fitSize->resize(110, 20);
    fitSize->move(120, 240 + 8);
    fitSize->setChecked(true);

    if (cd == true)
    {
        isImage->setVisible(false);
        txtPreview->setVisible(false);
        imagePreview->setVisible(false);
        openImage->setVisible(false);
        oneInOne->setVisible(false);
        fitSize->setVisible(false);
    }

    connect(oneInOne, SIGNAL(clicked()), this, SLOT(slotSetImage()));
    connect(fitSize, SIGNAL(clicked()), this, SLOT(slotSetImage()));

    slotAddImage(false);
    slotUpdate();
}
//----------------------------------------------------------
CColorDialog::~CColorDialog()
{
    delete spectorImg;
}
//----------------------------------------------------------
void CColorDialog::slotLoadImage()
{
    QString fileName_DATA = QFileDialog::getOpenFileName(this, "Open File", startPath, tr("Image Files (*.png *.jpg *.jpeg *.tiff *.tif *.bmp)"));
    if (fileName_DATA.isEmpty() || fileName_DATA.isNull())
        return;

    QImage imageTmp;
    imageTmp.load(fileName_DATA);


    if (imageTmp.width() > 1024 || imageTmp.height() > 1024)
    {
//        CInfoDlgWin::showInfoMessageBox(q->t("Warning"), q->t("Image must be within (8x8 - 1024x1024)"), settings, this);
//        return;
        imageTmp = imageTmp.scaled(1024, 1024, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    }
    else
    if (imageTmp.width() < 8 || imageTmp.height() < 8)
    {
        imageTmp = imageTmp.scaled(8, 8, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    }

    ground.image = imageTmp;
    slotSetImage();
}
//----------------------------------------------------------
void CColorDialog::slotSetImage()
{
    if (ground.image.isNull())
        return;

    if (oneInOne->isChecked())
    {
        imagePreview->setPixmap(QPixmap::fromImage(ground.image));
    }
    else
    if (fitSize->isChecked())
    {
        imagePreview->setPixmap(QPixmap::fromImage(ground.image.scaled(imagePreview->width(), imagePreview->height(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation)));
    }
}
//----------------------------------------------------------
void CColorDialog::slotAddImage(bool isChecked)
{
    if (isChecked)
    {
        txtPreview->setEnabled(true);
        imagePreview->setEnabled(true);
        openImage->setEnabled(true);
        oneInOne->setEnabled(true);
        fitSize->setEnabled(true);
    }
    else
    {
        txtPreview->setEnabled(false);
        imagePreview->setEnabled(false);
        openImage->setEnabled(false);
        oneInOne->setEnabled(false);
        fitSize->setEnabled(false);
    }
}
//----------------------------------------------------------
void CColorDialog::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
}
//----------------------------------------------------------
void CColorDialog::draw()
{
    QPixmap mapSpector(colorSpector->size());
    QImage lineSpector(colorSpectorLine->size(), QImage::Format_ARGB32);
    QPixmap colorOut(outColor->size());
    lineSpector.fill(qRgba(0, 0, 0, 0));

    QPainter painter(&mapSpector);
    painter.drawImage(0, 0, *spectorImg);
    painter.drawImage((hue / stepX) - 9, ((255 - sat) / stepY) - 9, *spectorPointer);

    QPainter painter2(&lineSpector);
    painter2.drawImage(12, ((255 - val) / stepY) - 10, *linePointer);

    QColor hsv(QColor::Hsv);
    for (int yVal = 0; yVal < 256; yVal++)
    {
        hsv.setHsv(hue, sat, (255 - yVal));
        painter2.setPen(hsv);
        painter2.drawLine(0, (yVal / stepY), 10, (yVal / stepY));
    }

    QPainter painter3(&colorOut);
    hsv.setHsv(hue, sat, val);
    painter3.fillRect(colorOut.rect(), hsv);

    colorSpector->setPixmap(mapSpector);
    colorSpectorLine->setPixmap(QPixmap::fromImage(lineSpector));
    outColor->setPixmap(colorOut);
}
//----------------------------------------------------------
void CColorDialog::slotClickSpector(QPoint pos)
{
    alotSetHue(pos.x() * stepX);
    slotSetSat(255 - (pos.y() * stepY));
}
//----------------------------------------------------------
void CColorDialog::slotClickSpectorLine(QPoint pos)
{
    slotSetVal(255 - (pos.y() * stepY));
}
//----------------------------------------------------------
void CColorDialog::alotSetHue(int val)
{
    if (val < 0)
        val = 0;

    if (val > 359)
        val = 359;

    hue = val;
    slotUpdate();
    draw();
}
//----------------------------------------------------------
void CColorDialog::slotSetSat(int val)
{
    if (val < 0)
        val = 0;

    if (val > 255)
        val = 255;

    sat = val;
    slotUpdate();
    draw();
}
//----------------------------------------------------------
void CColorDialog::slotSetVal(int value)
{
    if (value < 0)
        value = 0;

    if (value > 255)
        value = 255;

    val = value;
    slotUpdate();
    draw();
}
//----------------------------------------------------------
void CColorDialog::slotSetColor(QColor color)
{
    color.getHsv(&hue, &sat, &val);
    draw();
}
//----------------------------------------------------------
void CColorDialog::slotSetGround(background &gro)
{
    ground = gro;
    slotSetColor(ground.color);
    if (!ground.image.isNull())
    {
        if (ground.drawMode == GROUND_ONE_IN_ONE || ground.drawMode == GROUND_FIT_SIZE )
        {
            isImage->setChecked(true);
            slotAddImage(true);
        }
        if (ground.drawMode == GROUND_ONE_IN_ONE || ground.drawMode == GROUND_ONE_IN_ONE_COLOR)
        {
            oneInOne->setChecked(true);
        }
        else
        if (ground.drawMode == GROUND_FIT_SIZE || ground.drawMode == GROUND_FIT_SIZE_COLOR)
        {
            fitSize->setChecked(true);
        }
        slotSetImage();
    }
    slotUpdate();
}
//----------------------------------------------------------
void CColorDialog::slotSelBasicColor(QColor &color)
{
    color.getHsv(&hue, &sat, &val);
    slotUpdate();
    draw();
}
//----------------------------------------------------------
void CColorDialog::slotStRGB()
{
    QColor rgb;
    rgb.setRgb(spinRed->value(), spinGreen->value(), spinBlue->value());
    slotSetColor(rgb);

    disconnectAll();

    QColor color(QColor::Hsv);
    color.setHsv(hue, sat, val);

    int h;
    int s;
    int v;

    color.getHsv(&h, &s, &v);

    spinHue->setValue(h);
    spinSat->setValue(s);
    spinVal->setValue(v);

    connectAll();
    draw();
}
//----------------------------------------------------------
void CColorDialog::slotStHSV()
{
    QColor hsv;
    hsv.setHsv(spinHue->value(), spinSat->value(), spinVal->value());
    slotSetColor(hsv);

    disconnectAll();

    QColor color(QColor::Hsv);
    color.setHsv(hue, sat, val);

    int r;
    int g;
    int b;

    color.getRgb(&r, &g, &b);

    spinRed->setValue(r);
    spinGreen->setValue(g);
    spinBlue->setValue(b);

    connectAll();
    draw();
}
//----------------------------------------------------------
void CColorDialog::connectAll()
{
    connect(colorSpector, SIGNAL(signalClick(QPoint)), this, SLOT(slotClickSpector(QPoint)));
    connect(colorSpectorLine, SIGNAL(signalClick(QPoint)), this, SLOT(slotClickSpectorLine(QPoint)));

    connect(spinHue, SIGNAL(valueChanged(int)), this, SLOT(slotStHSV()));
    connect(spinSat, SIGNAL(valueChanged(int)), this, SLOT(slotStHSV()));
    connect(spinVal, SIGNAL(valueChanged(int)), this, SLOT(slotStHSV()));

    connect(spinRed, SIGNAL(valueChanged(int)), this, SLOT(slotStRGB()));
    connect(spinGreen, SIGNAL(valueChanged(int)), this, SLOT(slotStRGB()));
    connect(spinBlue, SIGNAL(valueChanged(int)), this, SLOT(slotStRGB()));
}
//----------------------------------------------------------
void CColorDialog::disconnectAll()
{
    disconnect(colorSpector, SIGNAL(signalClick(QPoint)), this, SLOT(slotClickSpector(QPoint)));
    disconnect(colorSpectorLine, SIGNAL(signalClick(QPoint)), this, SLOT(slotClickSpectorLine(QPoint)));

    disconnect(spinHue, SIGNAL(valueChanged(int)), this, SLOT(slotStHSV()));
    disconnect(spinSat, SIGNAL(valueChanged(int)), this, SLOT(slotStHSV()));
    disconnect(spinVal, SIGNAL(valueChanged(int)), this, SLOT(slotStHSV()));

    disconnect(spinRed, SIGNAL(valueChanged(int)), this, SLOT(slotStRGB()));
    disconnect(spinGreen, SIGNAL(valueChanged(int)), this, SLOT(slotStRGB()));
    disconnect(spinBlue, SIGNAL(valueChanged(int)), this, SLOT(slotStRGB()));
}
//----------------------------------------------------------
void CColorDialog::slotUpdate()
{
    disconnectAll();

    QColor color(QColor::Hsv);
    color.setHsv(hue, sat, val);

    int r;
    int g;
    int b;

    color.getRgb(&r, &g, &b);

    spinRed->setValue(r);
    spinGreen->setValue(g);
    spinBlue->setValue(b);

    int h;
    int s;
    int v;

    color.getHsv(&h, &s, &v);

    spinHue->setValue(h);
    spinSat->setValue(s);
    spinVal->setValue(v);

    connectAll();
    draw();
}
//----------------------------------------------------------
void CColorDialog::slotOk()
{
    ground.drawMode = GROUND_DRAW_COLOR;
    ground.color.setHsv(hue, sat, val);
    if (isImage->isChecked())
    {
        if (ground.image.isNull())
        {
            //CInfoDlgWin::showInfoMessageBox(q->t("Warning"), q->t("Image no selected"), settings, this);
            QMessageBox::information(this, "Warning", "Image no selected");
            return;
        }

        if (oneInOne->isChecked())
        {
            ground.drawMode = GROUND_ONE_IN_ONE;
        }
        else
        if (fitSize->isChecked())
        {
            ground.drawMode = GROUND_FIT_SIZE;
        }
    }
    else
    {
        if (oneInOne->isChecked())
        {
            ground.drawMode = GROUND_ONE_IN_ONE_COLOR;
        }
        else
        if (fitSize->isChecked())
        {
            ground.drawMode = GROUND_FIT_SIZE_COLOR;
        }
    }
    close();
}
//----------------------------------------------------------
void CColorDialog::slotCancel()
{
    ground.drawMode = CANCEL_DLG;
    close();
}
//----------------------------------------------------------
