#include "papersizedlg.h"

CPaperSizeDlg::CPaperSizeDlg(CAppStyle *pAppstyle, QWidget *parent, bool isBlock, int mode)
	: QWidget(parent)
{
//	settings = csettings;
//	q = settings->q;

    appstyle = pAppstyle;

	res = -1;
    m_pPageSeq = NULL;
    rseq = 0;

    mod = mode;

    m_pOk = new QPushButton("Ok", this);
	m_pOk->resize(80, 20);
	QObject::connect(m_pOk, SIGNAL(clicked()), this, SLOT(OnOk()));

    m_pCancel = new QPushButton("Cancel", this);
	m_pCancel->resize(80, 20);
	QObject::connect(m_pCancel, SIGNAL(clicked()), this, SLOT(OnCancel()));

    pageSizes.append(SPageSizes("Custom", 0, 0, 0, true));
    pageSizes.append(SPageSizes("", 0, 0, 0, true));
    pageSizes.append(SPageSizes("Letter (8.5\" x 11\")", 8.5, 11, 0, false));
    pageSizes.append(SPageSizes("Legal (8.5\" x 14\")", 8.5, 14, 0, false));
    pageSizes.append(SPageSizes("Junior Legal (8\" x 5\")", 8, 5, 0, false));
    pageSizes.append(SPageSizes("Ledger (17\" x 11\")", 17, 11, 0, false));
    pageSizes.append(SPageSizes("Tabloid (11\" x 17\")", 11, 17, 0, false));
    pageSizes.append(SPageSizes("", 0, 0, 0, true));
    pageSizes.append(SPageSizes("A3 (297mm x 420mm)", 297, 420, 1, false));
    pageSizes.append(SPageSizes("A4 (210mm x 297mm)", 210, 297, 1, false));
    pageSizes.append(SPageSizes("A5 (148.5mm x 210mm)", 148.5, 210, 1, false));
    pageSizes.append(SPageSizes("A6 (105mm x 148.5mm)", 105, 148.5, 1, false));



    m_pL1 = new QLabel("Title", this);
    m_pL1->resize(100, 20);
    if (isBlock)
        m_pL2 = new QLabel("Size", this);
    else
        m_pL2 = new QLabel("Paper size", this);



    m_pL2->resize(100, 20);
    m_pL3 = new QLabel("X", this);
    m_pL3->setAlignment(Qt::AlignHCenter);
    m_pL3->resize(20, 20);


    m_pReportName = new QLineEdit("Untitled", this);
    //QObject::connect(m_pReportName, SIGNAL(textChanged(const QString &)), this, SLOT(OnTextChanged(const QString &)));

    if (isBlock)
    {
        m_pWidth = new QLineEdit("3", this);
        m_pHeight = new QLineEdit("2", this);
        rw=3, rh=2;
    } else
    {
        m_pWidth = new QLineEdit("8.5", this);
        m_pHeight = new QLineEdit("11", this);
        rw=8.5, rh=11;
    }

    m_pPaperSizes = new QComboBox(this);
    /*QStringList unitsFormat;
    unitsFormat << "\"" << "mm";
    for (int a=0; a<pageSizes.size(); a++)
    {
        QString s = pageSizes.at(a).name;
        if (!pageSizes.at(a).skip)
        {
            s += " (" + QString::number(pageSizes.at(a).w, 'f', 2) + unitsFormat.at(pageSizes.at(a).units)
                    + " x "
                    + QString::number(pageSizes.at(a).h, 'f', 2) + unitsFormat.at(pageSizes.at(a).units) + ")";
        }
        m_pPaperSizes->addItem(s);
    }*/
    for (int a=0; a<pageSizes.size(); a++)
        m_pPaperSizes->addItem(pageSizes.at(a).name);
    m_pPaperSizes->setCurrentIndex(0);

    if (mod == THUMB_MODE)
    {
        m_pL2->setText("Card Size");
        m_pPaperSizes->hide();
    }

    m_pOrientation = new QComboBox(this);

    m_pOrientation->addItem("Vertical");
    m_pOrientation->addItem("Horizontal");
    m_pOrientation->setCurrentIndex(0);

    if (mod == THUMB_MODE)
    {
        m_pOrientation->setCurrentIndex(1);
        m_pOrientation->hide();
    }

    m_pUnits = new QComboBox(this);
    m_pUnits->addItem("inches");
    m_pUnits->addItem("mm");
    m_pUnits->setCurrentIndex(0);


    m_units = 0;

    if (isBlock)
    {
        m_pPaperSizes->hide();
        m_pOrientation->hide();
    }

    m_pL1->move(10, 20);
    m_pReportName->move(10, 40);
    m_pReportName->resize(380, 25);

    m_pL2->move(10, 70);
    m_pPaperSizes->move(10, 90);
    m_pPaperSizes->resize(250, 25);
    m_pOrientation->move(265, 90);
    m_pOrientation->resize(125, 25);

    if (isBlock)
    {
        m_pWidth->move(10, 90);
        m_pWidth->resize(80, 25);
        m_pL3->move(90, 95);
        m_pHeight->move(110, 90);
        m_pHeight->resize(80, 25);
        m_pUnits->move(200, 90);
        m_pUnits->resize(120, 25);
    } else
    {
        m_pWidth->move(10, 125);
        m_pWidth->resize(80, 25);
        m_pL3->move(90, 130);
        m_pHeight->move(110, 125);
        m_pHeight->resize(80, 25);
        m_pUnits->move(200, 125);
        m_pUnits->resize(120, 25);
    }

    if (mod == THUMB_MODE)
    {
        m_pWidth->move(10, 90);
        m_pHeight->move(110, 90);
        m_pL3->move(90, 95);
        m_pUnits->hide();
        m_pUnits->setCurrentIndex(1);
    }

    connectUI();

    if (isBlock)
        dlgTitle = "Properties";
    else
        dlgTitle = "Report Properties";

    if (mod == THUMB_MODE)
    {
        dlgTitle = "Properties";
    }

    initStyle();

	int w = this->width();
	int h = this->height();


#ifdef Q_WS_WIN
	m_pOk->move(w/2-90, h-30);
	m_pCancel->move(w/2+10, h-30);
#else
	m_pOk->move(w/2+10, h-30);
	m_pCancel->move(w/2-90, h-30);
#endif
}


CPaperSizeDlg::~CPaperSizeDlg()
{

}

void CPaperSizeDlg::initStyle()
{
    appstyle->setWidgetStyle(this, WTDlg);

    appstyle->setWidgetStyle(m_pOk, WTButton);
    appstyle->setWidgetStyle(m_pCancel, WTButton);

    appstyle->setWidgetStyle(m_pReportName, WTEdit);
    appstyle->setWidgetStyle(m_pWidth, WTEdit);
    appstyle->setWidgetStyle(m_pHeight, WTEdit);

    appstyle->setWidgetStyle(m_pPaperSizes, WTCombo);
    appstyle->setWidgetStyle(m_pOrientation, WTCombo);
    appstyle->setWidgetStyle(m_pUnits, WTCombo);

    appstyle->setWidgetStyle(m_pL1, WTLabel);
    appstyle->setWidgetStyle(m_pL2, WTLabel);
    appstyle->setWidgetStyle(m_pL3, WTLabel);


    parentWidget()->setWindowTitle(dlgTitle);



    int wszx=400, wszy=230;
    if (mod == THUMB_MODE)
    {
        wszy = 200;
    }
    resize(wszx, wszy);

    parentWidget()->setFixedSize(wszx, wszy);
    parentWidget()->setWindowFlags(Qt::Dialog | Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::CustomizeWindowHint);

    int px=0, py=0;
    if (parentWidget()->parentWidget()->parentWidget()!=NULL)
    {
        px = parentWidget()->parentWidget()->parentWidget()->x() + (parentWidget()->parentWidget()->parentWidget()->width()-this->width())/2;
        py = parentWidget()->parentWidget()->parentWidget()->y() + (parentWidget()->parentWidget()->parentWidget()->height()-this->height())/2;
    } else
    {
        QDesktopWidget desktopWid;
        px = (desktopWid.width()-this->width())/2;
        py = (desktopWid.height()-this->height())/2;
    }
    if (px<0) px=0; if (py<0) py=0;
    parentWidget()->move(px, py);
}


void CPaperSizeDlg::addSeqCombo()
{
    m_pPageSeq = new QComboBox(this);
    QStringList pslist;
    pslist << "All" << "1" << "2" << "3" << "4" << "5" << "6" << "7" << "8" << "9" << "10";
    m_pPageSeq->addItems(pslist);
    m_pPageSeq->setEditable(true);

//    DialogsID did = DIDInfo;
//    settings->appstyle->setWidgetStyle(m_pPageSeq, did, WTCombo);

    m_pPageSeq->resize(200, 25);
    m_pPageSeq->move(10, 90);
    m_pL2->setText("Pages");

    m_pWidth->hide();
    m_pHeight->hide();
    m_pUnits->hide();
    m_pL3->hide();

    if (rseq<=0)
        m_pPageSeq->setCurrentIndex(0);
    else
        m_pPageSeq->setEditText(QString::number(rseq));
}


void CPaperSizeDlg::paintEvent(QPaintEvent *)
{
    //QPainter painter(this);
    //settings->appstyle->drawMainDlg(painter, this, DIDInfo);
}


void CPaperSizeDlg::OnExit()
{
	parentWidget()->close();
}


void CPaperSizeDlg::OnOk()
{
    m_units = m_pUnits->currentIndex();
    rw = m_pWidth->text().toDouble();
    rh = m_pHeight->text().toDouble();


    if (m_units==1)
    {
        rw = rw/25.4;
        rh = rh/25.4;
    }


    if (rw<0.1) rw=0.1;
    if (rw>50) rw=50;
    if (rh<0.1) rh=0.1;
    if (rh>50) rh=50;

    if (m_pPageSeq!=NULL)
    {
        rseq = m_pPageSeq->currentText().toInt();
        if (rseq > 100000)
            rseq = 100000;
        if (rseq<=0)
            rseq = 0;
    }

	res = 0;
	parentWidget()->close();
}


void CPaperSizeDlg::OnCancel()
{
	parentWidget()->close();
}


void CPaperSizeDlg::OnPSCombo(int)
{
    if (mod == THUMB_MODE)
        return;
    disconnectUI();
    int idx = m_pPaperSizes->currentIndex();
    if ((idx>=0) && (idx<pageSizes.size()))
    {
        if (!pageSizes.at(idx).skip)
        {
            m_units = pageSizes.at(idx).units;
            m_pUnits->setCurrentIndex(m_units);
            m_pWidth->setText(QString::number(pageSizes.at(idx).w, 'f', 2));
            m_pHeight->setText(QString::number(pageSizes.at(idx).h, 'f', 2));
            if (pageSizes.at(idx).w>pageSizes.at(idx).h)
                m_pOrientation->setCurrentIndex(1);
            else
                m_pOrientation->setCurrentIndex(0);
        } else
        {
            m_pPaperSizes->setCurrentIndex(0);
            idx = 0;
        }
    }
    if (idx==0)
    {
        m_pWidth->setDisabled(false);
        m_pHeight->setDisabled(false);
        m_pUnits->setDisabled(false);
    } else
    {
        m_pWidth->setDisabled(true);
        m_pHeight->setDisabled(true);
        m_pUnits->setDisabled(true);
    }
    OnOrientationCombo(0);
}


void CPaperSizeDlg::OnOrientationCombo(int)
{
    if (mod == THUMB_MODE)
        return;
    disconnectUI();
    double w = m_pWidth->text().toDouble();
    double h = m_pHeight->text().toDouble();
    QString str;

	if (m_pOrientation->isVisible())
	{
		if (m_pOrientation->currentIndex()==0)
		{
			if (h<w)
			{
				str = m_pWidth->text();
				m_pWidth->setText(m_pHeight->text());
				m_pHeight->setText(str);
			}
		} else
		{
			if (w<h)
			{
				str = m_pWidth->text();
				m_pWidth->setText(m_pHeight->text());
				m_pHeight->setText(str);
			}
		}
	}
    connectUI();
}


void CPaperSizeDlg::OnUnitsCombo(int)
{
    if (mod == THUMB_MODE)
        return;
    if (m_pUnits->currentIndex()!=m_units)
    {
        disconnectUI();
        m_units = m_pUnits->currentIndex();
        double w = m_pWidth->text().toDouble();
        double h = m_pHeight->text().toDouble();

        if (m_units==0)
        {
            w = w/25.4;
            h = h/25.4;
            m_pWidth->setText(QString::number(w, 'f', 2));
            m_pHeight->setText(QString::number(h, 'f', 2));
        } else
        {
            w = w*25.4;
            h = h*25.4;
            m_pWidth->setText(QString::number(w, 'f', 2));
            m_pHeight->setText(QString::number(h, 'f', 2));
        }
        connectUI();
    }
}


void CPaperSizeDlg::OnTextChanged()
{
    OnOrientationCombo(0);
}


void CPaperSizeDlg::selPaperCombo()
{
    m_units = m_pUnits->currentIndex();
    double w = m_pWidth->text().toDouble();
    double h = m_pHeight->text().toDouble();
    if (m_units==1)
    {
        w = w/25.4;
        h = h/25.4;
    }

    int idx = -1;
    for (int a=0; a<pageSizes.size(); a++)
    {
        if (!pageSizes.at(a).skip)
        {
            double w1 = pageSizes.at(a).w;
            double h1 = pageSizes.at(a).h;
            if (pageSizes.at(a).units==1)
            {
                w1 = w1/25.4;
                h1 = h1/25.4;
            }
            if ((fabs(w-w1)<0.02) && (fabs(h-h1)<0.02))
            {
                idx = a;
                break;
            }
        }
    }
    if (idx>0)
    {
        m_pPaperSizes->setCurrentIndex(idx);
        m_pOrientation->setCurrentIndex(0);
    }
    else
    {
        idx = -1;
        for (int a=0; a<pageSizes.size(); a++)
        {
            if (!pageSizes.at(a).skip)
            {
                double w1 = pageSizes.at(a).w;
                double h1 = pageSizes.at(a).h;
                if (pageSizes.at(a).units==1)
                {
                    w1 = w1/25.4;
                    h1 = h1/25.4;
                }
                if ((fabs(h-w1)<0.02) && (fabs(w-h1)<0.02))
                {
                    idx = a;
                    break;
                }
            }
        }
        if (idx>0)
        {
            m_pPaperSizes->setCurrentIndex(idx);
            m_pOrientation->setCurrentIndex(1);
        }
    }
}


void CPaperSizeDlg::connectUI()
{
    QObject::connect(m_pPaperSizes, SIGNAL(currentIndexChanged(int)), this, SLOT(OnPSCombo(int)));
    QObject::connect(m_pWidth, SIGNAL(editingFinished()), this, SLOT(OnTextChanged()));
    QObject::connect(m_pHeight, SIGNAL(editingFinished()), this, SLOT(OnTextChanged()));
    QObject::connect(m_pOrientation, SIGNAL(currentIndexChanged(int)), this, SLOT(OnOrientationCombo(int)));
    QObject::connect(m_pUnits, SIGNAL(currentIndexChanged(int)), this, SLOT(OnUnitsCombo(int)));
}


void CPaperSizeDlg::disconnectUI()
{
    QObject::disconnect(m_pPaperSizes, SIGNAL(currentIndexChanged(int)), this, SLOT(OnPSCombo(int)));
    QObject::disconnect(m_pWidth, SIGNAL(editingFinished()), this, SLOT(OnTextChanged()));
    QObject::disconnect(m_pHeight, SIGNAL(editingFinished()), this, SLOT(OnTextChanged()));
    QObject::disconnect(m_pOrientation, SIGNAL(currentIndexChanged(int)), this, SLOT(OnOrientationCombo(int)));
    QObject::disconnect(m_pUnits, SIGNAL(currentIndexChanged(int)), this, SLOT(OnUnitsCombo(int)));
}


void CPaperSizeDlg::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event)
    //ASProcessResizeEvent(this, event);
}


CPaperSizeDlgWin::CPaperSizeDlgWin(CAppStyle *pAppstyle, QWidget *parent, bool isBlock, int mode)
: QDialog(parent)
{
    //ASCreateWindow(this);

    mainWidget = new CPaperSizeDlg(pAppstyle, this, isBlock, mode);
}

CPaperSizeDlgWin::~CPaperSizeDlgWin()
{

}

