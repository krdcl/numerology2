#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <qdialog.h>
#include <qlabel.h>
#include <qpainter.h>
#include <qspinbox.h>
#include <qcheckbox.h>
#include <qradiobutton.h>
#include <qpushbutton.h>

#include "reportpe.h"

class CModLabel : public QLabel
{
    Q_OBJECT

public:

    CModLabel(QWidget *parent = 0);
    QRgb color;

signals:

    void signalClick(QPoint pos);
    void signalClick(QColor &color);

protected:

    bool isPressed;

    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

};


class CColorDialog : public QDialog
{
    Q_OBJECT
    
public:

    CColorDialog(/*CAppSettings *set, */bool colorDiag = false, QWidget *parent = 0);
    ~CColorDialog();

    background ground;
    QString startPath;

private:

//    CTranslate *q;
//    CAppSettings *settings;
    bool cd;

    QList <CModLabel *> colorItems;

    CModLabel *colorSpector;
    CModLabel *colorSpectorLine;
    QLabel *outColor;
    QImage *spectorImg;
    QImage *spectorImgLine;
    QImage *linePointer;
    QImage *spectorPointer;

    QLabel *txtHue;
    QLabel *txtSat;
    QLabel *txtVal;
    QLabel *txtRed;
    QLabel *txtGreen;
    QLabel *txtBlue;

    QLabel *txtBasicCol;

    QSpinBox *spinHue;
    QSpinBox *spinSat;
    QSpinBox *spinVal;
    QSpinBox *spinRed;
    QSpinBox *spinGreen;
    QSpinBox *spinBlue;

    QLabel *imagePreview;
    QLabel *txtPreview;

    QCheckBox *isImage;
    QPushButton *openImage;
    QRadioButton *oneInOne;
    QRadioButton *fitSize;

    int hue;
    int sat;
    int val;

    double stepX;
    double stepY;

    void connectAll();
    void disconnectAll();

    QPushButton *buttonOk;
    QPushButton *buttonCancel;


protected:

    void paintEvent(QPaintEvent *event);
    void draw();

private slots:

    void slotClickSpector(QPoint pos);
    void slotClickSpectorLine(QPoint pos);
    void slotSelBasicColor(QColor &color);

    void slotStRGB();
    void slotStHSV();

    void slotAddImage(bool isChecked);
    void slotLoadImage();
    void slotSetImage();

    void slotOk();
    void slotCancel();

public slots:

    void alotSetHue(int val);
    void slotSetSat(int val);
    void slotSetVal(int value);

    void slotSetColor(QColor color);
    void slotSetGround(background &gro);

    void slotUpdate();

};

#endif // MAINWINDOW_H
