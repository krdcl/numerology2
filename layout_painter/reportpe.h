#ifndef REPORTPE_H
#define REPORTPE_H

#include <QtGui>
#include <QAbstractScrollArea>
#include "appstyle.h"
//#include "datafile.h"
//#include "appsettings.h"

#include "numerpmatr.h"
#include "numer.h"


#define DEFREPFILEHEADER "MultiCollectorReportFile->"
#define REPFILE_VERSION 0x0107
#define REPFILE_VERSION100 0x0100
#define REPFILE_VERSION101 0x0101
#define REPFILE_VERSION102 0x0102
#define REPFILE_VERSION103 0x0103
#define REPFILE_VERSION104 0x0104
#define REPFILE_VERSION105 0x0105
#define REPFILE_VERSION106 0x0106
#define REPFILE_VERSION107 0x0107

#define CANCEL_DLG -1
#define GROUND_DRAW_COLOR 0
#define GROUND_ONE_IN_ONE 1
#define GROUND_FIT_SIZE 2
#define GROUND_ONE_IN_ONE_COLOR 3
#define GROUND_FIT_SIZE_COLOR 4

struct calcNumContainer
{
    CNumerology *m_pNumer;
    CNumerology *m_pNumerComp;

    CNumerologyPMatrix *m_pNumerPMatr;
    CNumerologyPMatrix *m_pNumerPMatrComp;

    calcNumContainer()
    {
        m_pNumer = NULL;
        m_pNumerComp = NULL;

        m_pNumerPMatr = NULL;
        m_pNumerPMatrComp = NULL;
    }
};

struct background
{
    QColor color;
    QImage image;
    qint8 drawMode;
};

class CStyle
{
public:
    struct SPage
    {
        QColor trackerColor;

        QImage iconScaleS;
        QImage iconRotateS;
        QImage iconMoveS;

        QImage iconScaleI;
        QImage iconScaleResI;

        QImage iconDataField;
        QImage iconDataFieldTitle;
        QImage imageDataField;
    } m_page;

    CStyle()
    {
        m_page.trackerColor = QColor::fromRgb(180,180,180,255);
    }
    ~CStyle() {}
};

class CDrawMode
{
public:
    bool thumb;
    bool finalRender;
    bool isDrawBorder;
    double grx, gry;
    //CDataFile *data;
    CDrawMode(/*CDataFile *cdata*/) { thumb = false; finalRender = false; /*data = cdata;*/ grx=0; gry=0; isDrawBorder = true; }
    ~CDrawMode() {}
};

enum OBJECT_TYPE
{
    OT_NONE = 0,
    OT_SLOT = 1,
    OT_IMAGE = 2,
    OT_TEXT = 3,
    OT_VECTOR_CLIP = 4,
    OT_DATA_FIELD = 5,
    OT_DATA_FIELD_NAME = 6,
    OT_DATA_IMAGE = 7,
    OT_BLOCK = 8,
    OT_ICON = 9,
    OT_BAR_CODE = 10,
    OT_CALC_BLOCK_T1 = 11,
    OT_CALC_BLOCK_T2 = 12,
    OT_CALC_BLOCK_T3 = 13,
    OT_CALC_BLOCK_T4 = 14,
    OT_CALC_BLOCK_T5 = 15
};

enum EDIT_ACTIONS { ACT_NONE=0, ACT_MOVE_CONTAINER=1, ACT_MOVE_OBJECT=2, ACT_ROTATE_OBJECT=3, ACT_RESIZE_OBJECT=4, ACT_UPDATE_SIZE=12, ACT_ROTATE_IOBJECT=13,
    ACT_ADD_OBJECT=5,
    ACT_DELETE_PAGE=6, ACT_INS_PAGE=7, ACT_MOVE_PAGE=8,
    ACT_CHANGE_TEXT=9, ACT_CHANGE_SHAPE=10, ACT_CHANGE_CLIPART=11};

enum TRACKER_INDEX { TRACKER_SIZE_LT=0, TRACKER_SIZE_RT=1, TRACKER_SIZE_LB=2, TRACKER_SIZE_RB=3,
    TRACKER_SIZE_L=4, TRACKER_SIZE_R=5, TRACKER_SIZE_B=6,
    TRACKER_MOVE=7, TRACKER_ROTATE=8,
    TRACKER_CONT_SCALE=9, TRACKER_CONT_POS_RES=10 };

enum DRAW_MODE { DM_NORMAL=0, DM_TEMPLATES=1 };

static const int TrackersNum = 11;

struct STracker
{
    QRectF rect;
};


class CREPageItem
{

public:
    CREPageItem(calcNumContainer *pNumContainer);
    ~CREPageItem();

    struct palitra
    {
        QColor pen1;
        QColor pen2;
        QColor pen3;
        QColor pen4;
        QColor pen5;
        QColor pen6;
    };

    //CDataFile *data;

    //CTranslate *q;

    static const int OBJ_MIN_WIDTH = 15;
    static const int OBJ_MIN_HEIGHT = 15;

    calcNumContainer *numContainer;

    int m_Type;
	bool isLabel;
    bool isTextBox;
	QString name;
	QString id;
    QTransform m_Matrix;
    double m_Angle;
    QSizeF m_BoundSize;
    bool m_bSelected;
    QImage *m_pImg;
    void *m_pData;

    QPainterPath m_Shape;
    QColor m_ShapeFillColor;
    int m_imgID;
    QString m_fileLink;

    QSizeF m_imgSize;
    QImage m_img;

    int m_itemid, m_pnum;
    QList <QImage> *icon;

    int widthIcon;

    int m_iopt;

    bool m_bCanResize;
    bool m_bSolidContainer;

    QString m_Text;
    QFont m_Font;
    int m_Align;
    int m_LineType;
    double m_LineWidth;
    QColor m_LineColor;
    int m_Idx;
    int m_Frame;
    QColor m_FrameColor;
    int barCodes;
    int m_Fit;
    int iconNum;
	QColor m_BackgroundColor;

    CREPageItem *m_pContainer;
    CREPageItem *m_pOwner;

	bool sel;

    void resTransformation();
    void rotate(double angle, bool calcAngle = true);
    void move(double dx, double dy);
    void scale(double sx, double sy);
    void setMatrix(const QTransform &matrix) { m_Matrix = matrix; }
    void translate(double dx, double dy);
    QTransform getMatrix() { return m_Matrix; }
    double getAngle() { return m_Angle; }
    QPointF getPos() {return QPointF(m_Matrix.dx(), m_Matrix.dy()); }

    int objectType() { return m_Type; }
    void setType(int type) { m_Type = type; }

    void setBoundSize(QSizeF sz);
    QSizeF getBoundSize();
    QRectF getBoundRect();

    void setShape(QPainterPath shape);
    QPainterPath getShape() { return m_Shape; }
    QRectF getShapeBoundRect();
    void setShapeColor(QColor col) { m_ShapeFillColor = col; }
    QColor getShapeColor() { return m_ShapeFillColor; }

    void setText(const QString text) { m_Text = text; }
    QString getText() { return m_Text; }

    void setFont(const QFont font) { m_Font=font; }

    void fitToRect(QRectF rect, bool skipScale=false);
    static QRectF fitToPageP(QRectF obj, QSizeF page);
    static QRectF fitToPageR(QRectF obj, QSizeF page, int trackerID, double k=0);


    void setSelected(bool selected) { m_bSelected = selected; }
    bool getSelected() { return m_bSelected; }

    void draw(QPainter *pPntr, CDrawMode *drawMode, CStyle *style);
    QImage getQrCode(QString valStr, QColor color);
    QImage drawBarcode(QString valStr, int type, QColor color);
    void drawTracker(QPainter *pPntr, CDrawMode *drawMode, CStyle *style, bool drawBorderOnly = false);

    void genTrackers(STracker trackers[TrackersNum], QRectF boundRect, QTransform mat, CStyle *style);

    QString getNumRun(int val, QString num);

    void load(QDataStream &stream, int ProjectVersion);
    void save(QDataStream &stream);

    void drawFTextS(QPainter &painter, int x, int y, QString str);

    void paintInfo(QPainter *painter, QStringList parList, QStringList parListVal, palitra pens);
    void paintExprInfo(QPainter *painter, QStringList parList, QStringList parListVal, palitra pens);
    void paintNameInfo(QPainter *painter, QStringList parList, QStringList parListVal, palitra pens);
    void paintMatrInfo(QPainter *painter, QStringList parList, QStringList parListVal, palitra pens);
    void paintCalendarInfo(QPainter *painter, QDate cdate, int m_PersYear, QStringList parList, palitra pens);
};


class CREPage
{
public:
    CREPage(calcNumContainer *pNumContainer, CStyle *style/*, CDataFile *cdata*/);
    ~CREPage();

    QString qsResPath;
    calcNumContainer *numContainer;

    //CDataFile *datafile;
    QVector<CREPageItem*> m_PageObjects;
    QImage *m_thumbnail;

    QSizeF m_PageSize;
    QString m_PageName;
    int m_PageSeq;
    int m_PageGenOptions;

    background ground;

    int m_Ind;

    int m_itemid, m_pnum;

    QList <QImage> defIcon;

    void draw(QPainter *pPntr, CDrawMode *drawMode, CStyle *style, bool drawPaper=true);
    void draw(QPainter *pPntr, QRectF rect, CDrawMode *drawMode, CStyle *style, bool drawPaper=true);
    void drawTrackers(QPainter *pPntr, CDrawMode *drawMode, CStyle *style, QList<CREPageItem*> &list);


    QSizeF getPageSize(){ return m_PageSize; }
    void setPageSize(QSizeF Sz);
    void addObject(CREPageItem* object) { m_PageObjects.push_back(object); }
    /*void clear();*/

    bool getObjectsList(QPointF Pt, QList<CREPageItem*> &list);
    bool getObjectsList(double x, double y, QList<CREPageItem*> &list);

    void unselectAll();
    void getSelected(QList<CREPageItem*> &list, bool ignoreDelected = false);

    void moveToPage(CREPageItem*item);

    void load(QDataStream &stream, int ProjectVersion);
    void save(QDataStream &stream);

    void generateThumbnail(bool drawFrame=false);
    QImage* getThumbnail() { return m_thumbnail; }

    void prefillData();

    /*void saveTemplate(QString fileName, int side);
    static void loadTemplate(QString fileName, SItem *item, QImage &m_thumb);*/


    CStyle *m_pStyle;
};


class CReportPE : public QAbstractScrollArea
{
    Q_OBJECT


private:
    struct STrackerAnim
    {
        int cnt;
        int etime;
        bool toVisible;
        CREPageItem *obj;
        STrackerAnim()
        {
            cnt = 0;
            etime = 0;
            obj = NULL;
            toVisible = 0;
        }
    };
    struct SAnimStruct
    {
        QList<STrackerAnim> trackerAnim;

        SAnimStruct()
        {
            trackerAnim = QList<STrackerAnim>();
        }
    } m_anim;

    QTimer animTmr;

    QPointF m_placePoint;
    CREPageItem *m_pEditObject;

public:
    //CDataFile *data;
    //CAppSettings *settings;
    //CTranslate *q;
    calcNumContainer *numContainer;

    QString reportName;
    QString m_FileName;
    CStyle *style;
    CDrawMode *drawMode;

    int m_grid;
    int m_snap;
    double m_gridX, m_gridY;

    double m_Spacing;
    double m_Scale;

    CREPageItem *m_pSelObject;
    QTransform m_selObjMatrix;
    double m_selObjAngle;
    QPainterPath m_selObjShape;

    int m_editAction;

    QPointF m_dragStartPt;
    QSizeF m_dragStartSz;
    QRectF m_dragStartRc;
    int m_dragTrackerID;
    QPointF lastPt;

    CREPage *page;

    struct SPReport
    {
        QVector<CREPage*> pages;
    };

    QVector<SPReport*> report;
    QVector<CREPage*> pblocks;

    CReportPE(calcNumContainer *pNumContainer, CAppStyle *style_,/*CDataFile *cdata, CAppSettings *csettings, */QWidget *parent=NULL);
    ~CReportPE();

    CAppStyle *appstyle;
    QString qsResPath;

    CREPageItem* addTextItem(QString text, QPoint pos);
    CREPageItem* addDataItem(QString text, QPoint pos, QSize objSz=QSize(120,25));
    CREPageItem* addImageItem(QImage &img, QPoint pos);
    CREPageItem *addBlockItem(CREPage *block, QPoint pos);

    QPointF calcDocViewTranslate();
    QTransform calcDocViewTransform();
    QPointF snapPoint(QPointF Pt, int snap=1);
#ifndef VER_MOBILE
    void mouseProcessEvent(QMouseEvent *pEvent, bool release);
    void deleteSelectedItems();
#endif
    int load(QDataStream &stream);
    int save(QDataStream &stream);
    void newReport();
    void clear();
    CREPage* addBlock(QSizeF sz, QString name);
    CREPage* addPage(QSizeF sz, QString name);
    void updateView();

protected:
    bool isCards;

    QLabel *lableFixScroll;

    void paintEvent(QPaintEvent *);
    void resizeEvent(QResizeEvent *);
    //void wheelEvent(QWheelEvent *);
#ifndef VER_MOBILE
    void mousePressEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void keyPressEvent (QKeyEvent *);
    void leaveEvent(QEvent *);

    void dropEvent(QDropEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event)
       { event->acceptProposedAction(); }
    void dragLeaveEvent(QDragLeaveEvent *event) { event->accept(); }

    void updateFixScroll();

signals:
    void itemDropped(QPoint pos);
    void selectionChanged();

public slots:
    void OnZoomIn();
    void OnZoomOut();

    void OnGrid();
    void OnSnap();
    void OnBoundaries();
    void moveToFrontSelItems();
    void moveToBackSelItems();
#endif

public slots:
    void OnZoomNormal();
};


#endif // REPORTPE_H
