

#include "reportcreator.h"
#ifndef VER_MOBILE
#include "infodlg.h"
#include "ccolordialog.h"
#endif
#include <math.h>
#include "thumbailsettings.h"

CReportCreator::CReportCreator(calcNumContainer *pNumContainer, CAppStyle *style, /*CDataFile *cdata, CAppSettings *csettings, */QWidget *parent, int mode, int *trial)
    : QWidget(parent), m_mode(mode)
{
    isTrial = trial;
    appstyle = style;
    numContainer = pNumContainer;

#ifdef Q_OS_MAC
    qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/");
#else
    qsResPath = QCoreApplication::applicationDirPath() + QString("/images/");
#endif

    parentWidget()->setWindowFlags(Qt::Dialog | Qt::WindowCloseButtonHint | Qt::WindowMaximizeButtonHint);

//    data = cdata;
//    settings = csettings;
//    q = settings->q;

    res = -1;

    isAddTitle = false;

    groundTmp.drawMode = CANCEL_DLG;
    groundTmp.color = qRgb(255, 255, 255);
    groundTmp.image = QImage();

    editItem = -1;

#ifndef VER_MOBILE
    m_pSplitter = new QSplitter(this);
    m_pSplitter->move(5, 10);

    QList<int> sizes;

    if (mode == REPORT_MODE)
    {
        m_pW1 = new CReportCreatorW1(m_pSplitter);
        QObject::connect(m_pW1, SIGNAL(updateWidgetGeometry()), this, SLOT(OnUpdateWidgetGeometry()));
        sizes << 100 << 700 << 200;
        m_pSplitter->addWidget(m_pW1);

    }
    else
    if (mode == THUMB_MODE)
    {
        m_pW1 = new CReportCreatorW1(this);
        m_pW1->hide();
        sizes << 700 << 200;
    }

    m_pW2 = new CReportCreatorW2(m_pSplitter);
    QObject::connect(m_pW2, SIGNAL(updateWidgetGeometry()), this, SLOT(OnUpdateWidgetGeometry()));
    m_pW3 = new CReportCreatorW3(m_pSplitter);
    QObject::connect(m_pW3, SIGNAL(updateWidgetGeometry()), this, SLOT(OnUpdateWidgetGeometry()));



    m_pSplitter->addWidget(m_pW2);
    m_pSplitter->addWidget(m_pW3);
    m_pW1->setMinimumWidth(100);
    m_pW2->setMinimumWidth(300);
    m_pW3->setMinimumWidth(100);
    m_pW1->setMaximumWidth(100);
    //m_pW2->setMaximumWidth();
    m_pW3->setMaximumWidth(300);

    m_pSplitter->setSizes(sizes);


    m_pOk = new QPushButton("Save", m_pW2 );
    m_pOk->resize(80, 20);
    QObject::connect(m_pOk, SIGNAL(clicked()), this, SLOT(OnOk()));

    m_pCancel = new QPushButton("Close", m_pW2 );
    m_pCancel->resize(80, 20);
    QObject::connect(m_pCancel, SIGNAL(clicked()), this, SLOT(OnCancel()));
#else

    pageArea = new CReportPE(data, settings, this);

#endif

#ifndef VER_MOBILE
    pageArea = new CReportPE(numContainer, appstyle, m_pW2);
    //pageArea->setStyleSheet("background: #1e1917;");

    pageArea->resize(550, 500);
    QObject::connect(pageArea, SIGNAL(itemDropped(QPoint)), this, SLOT(OnAddField(QPoint)));
    QObject::connect(pageArea, SIGNAL(selectionChanged()), this, SLOT(OnPageSelectionChanged()));

    propList = new CPropertiesWidget(appstyle, m_pW3);
    QObject::connect(propList, SIGNAL(dataChanged()), this, SLOT(OnPropertyChanged()));


    m_pZoomNormal = new QPushButton("", m_pW2);
    m_pZoomNormal->resize(35, 26);
    m_pZoomNormal->setIcon(QIcon(qsResPath+"bzoomn.png"));
    m_pZoomNormal->setIconSize(QSize(20,20));
    m_pZoomNormal->setToolTip("100%");
    QObject::connect(m_pZoomNormal, SIGNAL(clicked()), pageArea, SLOT(OnZoomNormal()));

    m_pZoomIn = new QPushButton("", m_pW2);
    m_pZoomIn->resize(35, 26);
    m_pZoomIn->setIcon(QIcon(qsResPath+"bzoomi.png"));
    m_pZoomIn->setIconSize(QSize(20,20));
    m_pZoomIn->setToolTip("Zoom In");
    QObject::connect(m_pZoomIn, SIGNAL(clicked()), pageArea, SLOT(OnZoomIn()));

    m_pZoomOut = new QPushButton("", m_pW2);
    m_pZoomOut->resize(35, 26);
    m_pZoomOut->setIcon(QIcon(qsResPath+"bzoomo.png"));
    m_pZoomOut->setIconSize(QSize(20,20));
    m_pZoomOut->setToolTip("Zoom Out");
    QObject::connect(m_pZoomOut, SIGNAL(clicked()), pageArea, SLOT(OnZoomOut()));

    m_pProperties = new QPushButton("", m_pW2);
    m_pProperties->resize(35, 26);
    m_pProperties->setIcon(QIcon(qsResPath+"bprops.png"));
    m_pProperties->setIconSize(QSize(20,20));
 //   if (mode == THUMB_MODE)
        m_pProperties->setToolTip("Graphics Charts Properties");
//    else
//        m_pProperties->setToolTip("Report Properties");
    QObject::connect(m_pProperties, SIGNAL(clicked()), this, SLOT(OnProperties()));

    m_pDelete = new QPushButton("", m_pW2);
    m_pDelete->resize(35, 26);
    m_pDelete->setIcon(QIcon(qsResPath+"bdelete.png"));
    m_pDelete->setIconSize(QSize(20,20));
    m_pDelete->setToolTip("Delete");
    QObject::connect(m_pDelete, SIGNAL(clicked()), this, SLOT(OnDelete()));

    m_pGrid = new QPushButton("", m_pW2);
    m_pGrid->resize(35, 26);
    m_pGrid->setIcon(QIcon(qsResPath+"bgrid.png"));
    m_pGrid->setIconSize(QSize(20,20));
    m_pGrid->setToolTip("Show Grid");
    QObject::connect(m_pGrid, SIGNAL(clicked()), pageArea, SLOT(OnGrid()));

    m_pSnap = new QPushButton("", m_pW2);
    m_pSnap->resize(35, 26);
    m_pSnap->setIcon(QIcon(qsResPath+"bsnap.png"));
    m_pSnap->setIconSize(QSize(20,20));
    m_pSnap->setToolTip("Snap to Grid");
    QObject::connect(m_pSnap, SIGNAL(clicked()), pageArea, SLOT(OnSnap()));

    m_pBoundaries = new QPushButton("", m_pW2);
    m_pBoundaries->resize(35, 26);
    m_pBoundaries->setIcon(QIcon(qsResPath+"bbound.png"));
    m_pBoundaries->setIconSize(QSize(20,20));
    m_pBoundaries->setToolTip("Boundaries");
    connect(m_pBoundaries, SIGNAL(clicked()), pageArea, SLOT(OnBoundaries()));

    m_pMoveToFront = new QPushButton("", m_pW2);
    m_pMoveToFront->resize(35, 26);
    m_pMoveToFront->setIcon(QIcon(qsResPath+"bbringfront.png"));
    m_pMoveToFront->setIconSize(QSize(20,20));
    m_pMoveToFront->setToolTip("Bring to Front");
    QObject::connect(m_pMoveToFront, SIGNAL(clicked()), pageArea, SLOT(moveToFrontSelItems()));

    m_pMoveToBack = new QPushButton("", m_pW2);
    m_pMoveToBack->resize(35, 26);
    m_pMoveToBack->setIcon(QIcon(qsResPath+"bbringback.png"));
    m_pMoveToBack->setIconSize(QSize(20,20));
    m_pMoveToBack->setToolTip("Send to Back");
    QObject::connect(m_pMoveToBack, SIGNAL(clicked()), pageArea, SLOT(moveToBackSelItems()));

/*	pagesList = new QListWidget(this);
    pagesList->resize(150, 140);
    pagesList->setMovement(QListView::Static);
    pagesList->setFlow(QListView::TopToBottom);
    pagesList->setViewMode(QListView::IconMode);
    pagesList->setIconSize(QSize(128, 64));
    pagesList->setSelectionMode(QAbstractItemView::SingleSelection);
    QObject::connect(pagesList, SIGNAL(itemSelectionChanged()), this, SLOT(OnPageChanged()));*/


    fieldsList = new QTreeWidget(m_pW3);
    fieldsList->header()->hide();
    fieldsList->resize(150, 500);
    fieldsList->setSelectionMode(QAbstractItemView::SingleSelection);
    fieldsList->clear();
    fieldsList->setColumnCount(1);
    fieldsList->setDragEnabled(true);
    fieldsList->setDropIndicatorShown(true);

    QTreeWidgetItem *itemCat = new QTreeWidgetItem(QStringList("Calculated data"));
    itemCat->setIcon(0, QIcon(qsResPath+"datafield-i.png"));
    idFieldsListForBarC.clear();
    idFieldsList.clear();

    idFieldsList.append("Name");
    QTreeWidgetItem *item = new QTreeWidgetItem(itemCat, QStringList(idFieldsList.at(idFieldsList.size() - 1)));
    item->setData(0, Qt::UserRole, 1);
    flitems.append(item);

    idFieldsList.append("Planes of Expression");
    item = new QTreeWidgetItem(itemCat, QStringList(idFieldsList.at(idFieldsList.size() - 1)));
    item->setData(0, Qt::UserRole, 1);
    flitems.append(item);

    idFieldsList.append("Psychomatrix");
    item = new QTreeWidgetItem(itemCat, QStringList(idFieldsList.at(idFieldsList.size() - 1)));
    item->setData(0, Qt::UserRole, 1);
    flitems.append(item);

    idFieldsList.append("Personal numbers calendar");
    item = new QTreeWidgetItem(itemCat, QStringList(idFieldsList.at(idFieldsList.size() - 1)));
    item->setData(0, Qt::UserRole, 1);
    flitems.append(item);

    idFieldsList.append("Main Features");
    item = new QTreeWidgetItem(itemCat, QStringList(idFieldsList.at(idFieldsList.size() - 1)));
    item->setData(0, Qt::UserRole, 1);
    flitems.append(item);

    idFieldsList.append("Additional Features");
    item = new QTreeWidgetItem(itemCat, QStringList(idFieldsList.at(idFieldsList.size() - 1)));
    item->setData(0, Qt::UserRole, 1);
    flitems.append(item);

    idFieldsList.append("Life Cycles");
    item = new QTreeWidgetItem(itemCat, QStringList(idFieldsList.at(idFieldsList.size() - 1)));
    item->setData(0, Qt::UserRole, 1);
    flitems.append(item);

    idFieldsList.append("Bridges");
    item = new QTreeWidgetItem(itemCat, QStringList(idFieldsList.at(idFieldsList.size() - 1)));
    item->setData(0, Qt::UserRole, 1);
    flitems.append(item);


//    for (int a=0; a<data->doc.docHeader.size(); a++)
//    {
//        CDataFile::EntryTypes type = data->doc.docHeader.at(a).dtype;
//        if ((type==CDataFile::ETString)
//            || (type==CDataFile::ETNumber)
//            || (type==CDataFile::ETInteger)
//            || (type==CDataFile::ETDate)
//            || (type==CDataFile::ETText)
//            || (type==CDataFile::ETCategory)
//            || (type==CDataFile::ETFixedCategory)
//            || (type==CDataFile::ETCategorySet)
//            || (type==CDataFile::ETRating)
//            || ((type==CDataFile::ETImageList) && (mode == REPORT_MODE))
//            || ((type==CDataFile::ETIcon) && (mode == THUMB_MODE))
//            || (type==CDataFile::ETFormula)
//            || (type==CDataFile::ETTime)
//#ifdef APP_BMC
//            || (type==CDataFile::ETStringList)
//#endif
//            || (type==CDataFile::ETLocation))
//        {
//            if (!((type == CDataFile::ETRating) || (type == CDataFile::ETImageList) || (type == CDataFile::ETIcon)))
//                idFieldsListForBarC.append(data->doc.docHeader.at(a).idname);

//            if (!((type == CDataFile::ETImageList) || (type == CDataFile::ETIcon)))
//                idFieldsList.append(data->doc.docHeader.at(a).idname);
//            QTreeWidgetItem *item = new QTreeWidgetItem(itemCat, QStringList(data->doc.docHeader.at(a).dname));
//            item->setData(0, Qt::UserRole, a+1);
//            flitems.append(item);
//        }
//    }
    fieldsList->addTopLevelItem(itemCat);
    //QObject::connect(fieldsList, SIGNAL(itemSelectionChanged()), this, SLOT(OnFieldChanged()));

    itemCat = new QTreeWidgetItem(QStringList("Variables"));
    itemCat->setIcon(0, QIcon(qsResPath+"datafieldtitle-i.png"));
    QStringList pageFieldsV;

    pageFieldsV << "Full Name" << "Short Name" << "Birthdate" << "Print date" << "Numerologist";


    for (int a=0; a<pageFieldsV.size(); a++)
    {
        //if (!(mode == THUMB_MODE && a == 0))
        {
            QTreeWidgetItem *item = new QTreeWidgetItem(itemCat, QStringList(pageFieldsV.at(a)));
            item->setData(0, Qt::UserRole, 1000+a);
            flitems.append(item);
        }
    }
    fieldsList->addTopLevelItem(itemCat);


    itemCat = new QTreeWidgetItem(QStringList("Objects"));
    itemCat->setIcon(0, QIcon(qsResPath+"thumb.png"));
    QStringList pageFieldsO;
    pageFieldsO << "Text" << "Image" << "Bar Codes";

    for (int a=0; a<pageFieldsO.size(); a++)
    {
        QTreeWidgetItem *item = new QTreeWidgetItem(itemCat, QStringList(pageFieldsO.at(a)));
        item->setData(0, Qt::UserRole, 2000+a);
        flitems.append(item);
    }
    fieldsList->addTopLevelItem(itemCat);

    repPages = new CReportPEPages(/*data, settings, */m_pW1);
    QObject::connect(repPages->m_pAddBlock, SIGNAL(clicked()), this, SLOT(OnAddBlock()));
    QObject::connect(repPages->m_pDelBlock, SIGNAL(clicked()), this, SLOT(OnDelBlock()));
    QObject::connect(repPages->m_pBlockProp, SIGNAL(clicked()), this, SLOT(OnBlockProp()));
    QObject::connect(repPages->m_pAddPage, SIGNAL(clicked()), this, SLOT(OnAddPage()));
    QObject::connect(repPages->m_pDelPage, SIGNAL(clicked()), this, SLOT(OnDelPage()));
    QObject::connect(repPages->m_pPageProp, SIGNAL(clicked()), this, SLOT(OnPageProp()));
    QObject::connect(repPages->pagesList, SIGNAL(itemSelected(CILItem *)), this, SLOT(OnPageSelected(CILItem *)));
    QObject::connect(repPages->pagesList, SIGNAL(signalItemDrop()), this, SLOT(OnListDrop()));
    QObject::connect(repPages->blocksList, SIGNAL(itemSelected(CILItem *)), this, SLOT(OnPageSelected(CILItem *)));
    QObject::connect(repPages->blocksList, SIGNAL(itemDropped(CILItem *, QPoint)), this, SLOT(OnBlockItemDropped(CILItem *, QPoint)));
    QObject::connect(repPages->blocksList, SIGNAL(signalItemDrop()), this, SLOT(OnBlockDrop()));


    appstyle->setWidgetStyle(this->parentWidget(), WTDlg);
    appstyle->setWidgetStyle(m_pOk, WTButton);
    appstyle->setWidgetStyle(m_pCancel, WTButton);
    appstyle->setWidgetStyle(m_pSplitter, WTMainList);
    appstyle->setWidgetStyle(fieldsList, WTCustomTree);
    appstyle->setWidgetStyle(pageArea, WTIconList);
    appstyle->setWidgetStyle(propList, WTBkWidg);
    appstyle->setWidgetStyle(propList->tablelist, WTTree);
    appstyle->setWidgetStyle(m_pZoomNormal, WTButton);
    appstyle->setWidgetStyle(m_pZoomIn, WTButton);
    appstyle->setWidgetStyle(m_pZoomOut, WTButton);
    appstyle->setWidgetStyle(m_pProperties, WTButton);
    appstyle->setWidgetStyle(m_pDelete, WTButton);
    appstyle->setWidgetStyle(m_pMoveToFront, WTButton);
    appstyle->setWidgetStyle(m_pMoveToBack, WTButton);
    appstyle->setWidgetStyle(m_pGrid, WTButton);
    appstyle->setWidgetStyle(m_pSnap, WTButton);
    appstyle->setWidgetStyle(m_pBoundaries, WTButton);


//    if (mode == THUMB_MODE)
        parentWidget()->setWindowTitle("Graphics Charts Creator");
//    else
//        parentWidget()->setWindowTitle("Report Creator");



    int wszx=1000, wszy=640;
    parentWidget()->setMinimumSize(800, 500);
    parentWidget()->resize(wszx, wszy);
    resize(wszx, wszy);

    int px=0, py=0;
    if (parentWidget()->parentWidget()!=NULL)
    {
        px = parentWidget()->parentWidget()->x() + (parentWidget()->parentWidget()->width()-this->width())/2;
        py = parentWidget()->parentWidget()->y() + (parentWidget()->parentWidget()->height()-this->height())/2;
    } else
    {
        QDesktopWidget desktopWid;
        px = (desktopWid.width()-this->width())/2;
        py = (desktopWid.height()-this->height())/2;
    }
    if (px<0) px=0; if (py<0) py=0;
    parentWidget()->move(px, py);
    ASCheckScreenBoundary(this->parentWidget());

    l1 = new QLabel("Fields:", m_pW3);
    l1->resize(150, 20);
    l2 = new QLabel("Properties", m_pW3);
    l2->resize(150, 20);

//    settings->appstyle->setWidgetStyle(l1, did, WTLabel);
//    settings->appstyle->setWidgetStyle(l2, did, WTLabel);
    //settings->appstyle->setWidgetStyle(l3, did, WTLabel);
    //settings->appstyle->setWidgetStyle(l4, did, WTLabel);
    //settings->appstyle->setWidgetStyle(l5, did, WTLabel);

    updateStates();
#endif
}

#ifndef VER_MOBILE
void CReportCreator::init()
{
    updatePagesList();
    repPages->pagesList->setCurrentItemByPData(pageArea->page);
    repPages->blocksList->setCurrentItemByPData(pageArea->page);
    updateStates();
}


void CReportCreator::OnCancel()
{
    if (res == 0)
    {
        parentWidget()->close();
    }
    else
    {
        //int cres = CInfoDlgWin::showAskMessageBox(q->t("Warning"), q->t("Save changes?"), settings, this);
        //;
        //int cres = QMessageBox::question(0, "Warning", "Save changes?", tr("&Yes"), tr("&No"), QString::null, 0, 1 );
        if (!CInfoDlgWin::showAskMessageBox(appstyle, "Warning", "Save changes?", this))
        {
            OnOk();
            parentWidget()->close();
        } else
        {
            parentWidget()->close();
        }
    }
}


void CReportCreator::OnOk()
{
    if (isTrial == NULL || *isTrial != 1)
    {
        emit rigister();
        return;
    }

    QString repfn = QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + "/VeBest Numerology/" + pageArea->reportName;

    if (!path.isEmpty())
    {
        QFile::remove(path);
    }

    for (int i = 0; i < 10000; i++)
    {
        if (i == 0)
        {
            QFileInfo inf(repfn + ".vnr");
            if (!inf.exists())
            {
                repfn = repfn + ".vnr";
                break;
            }
        }

        QFileInfo inf(repfn + QString::number(i) + ".vnr");
        if (!inf.exists())
        {
            repfn = repfn + QString::number(i) + ".vnr";
            break;
        }
    }


    QFile File(repfn+".000");
    if( !File.open(QIODevice::WriteOnly) )
    {
        //CInfoDlgWin::showInfoMessageBox(q->t("Error"), q->t("Can't save report!"), settings, this);
        QMessageBox::information(0, "Error", "Can't save report!");
        return;
    }
    QDataStream stream(&File);
    pageArea->save(stream);
    File.close();

    path = repfn;

    QFile::remove(repfn);
    QFile::rename(repfn+".000", repfn);

    res = 0;
    /*res = 0;
    parentWidget()->close();*/
}


void CReportCreator::OnProperties()
{
    CPaperSizeDlgWin *repdlg = 0;
    double w = 0;
    double h = 0;

    if (m_mode == THUMB_MODE)
    {
        repdlg = new CPaperSizeDlgWin(appstyle, this, false, THUMB_MODE);
        repdlg->mainWidget->m_pWidth->setText(QString::number((int)((double)pageArea->page->m_PageSize.width() / PIX_MM_V3)));
        repdlg->mainWidget->m_pHeight->setText(QString::number((int)((double)pageArea->page->m_PageSize.height() / PIX_MM_V3)));
        repdlg->mainWidget->m_pReportName->setText(pageArea->reportName);
    }
    else
    {
        repdlg = new CPaperSizeDlgWin(appstyle, this);
        w = pageArea->page->m_PageSize.width()/(4*25.4);
        h = pageArea->page->m_PageSize.height()/(4*25.4);
        repdlg->mainWidget->m_pWidth->setText(QString::number(w, 'f', 2));
        repdlg->mainWidget->m_pHeight->setText(QString::number(h, 'f', 2));
        repdlg->mainWidget->selPaperCombo();
        repdlg->mainWidget->m_pReportName->setText(pageArea->reportName);
    }


    QPushButton *onGroundDialog = NULL;
    if (m_mode == THUMB_MODE)
    {
        //DialogsID did = DIDEntry;
        onGroundDialog = new QPushButton("Set Background", repdlg);
        appstyle->setWidgetStyle(onGroundDialog, WTButton);
        onGroundDialog->resize(120, 20);
        onGroundDialog->move(10, 130);
        connect(onGroundDialog, SIGNAL(clicked()), this, SLOT(slotOnBackground()));
    }

    repdlg->exec();



    if (repdlg->mainWidget->res==0)
    {       
        if (groundTmp.drawMode != CANCEL_DLG)
            pageArea->page->ground = groundTmp;

        pageArea->reportName = repdlg->mainWidget->m_pReportName->text();
        if (m_mode == THUMB_MODE)
        {
            pageArea->page->setPageSize(QSizeF(repdlg->mainWidget->m_pWidth->text().toDouble() * PIX_MM_V3, repdlg->mainWidget->m_pHeight->text().toDouble() * PIX_MM_V3));
        }
        else
        {
            pageArea->page->setPageSize(QSizeF(repdlg->mainWidget->rw*4*25.4, repdlg->mainWidget->rh*4*25.4));
        }

        pageArea->OnZoomNormal();
    }

    delete repdlg;
}

void CReportCreator::slotOnBackground()
{
    CColorDialog *dlg = new CColorDialog/*(settings)*/;
    dlg->slotSetGround(pageArea->page->ground);
    dlg->slotUpdate();
    dlg->setWindowModality(Qt::ApplicationModal);
    //dlg->startPath = settings->dataPath + "/cards/";
    dlg->exec();

    groundTmp = dlg->ground;

    dlg->hide();
    delete dlg;
}

void CReportCreator::OnDelete()
{
    pageArea->deleteSelectedItems();
}


void CReportCreator::resizeEvent(QResizeEvent *event)
{
	int w = event->size().width();
	int h = event->size().height();

    m_pSplitter->resize(w-10, h-10);
}


void CReportCreator::OnUpdateWidgetGeometry()
{
    int w, h;
    // Widget 1
    w = m_pW1->width(), h = m_pW1->height();
    repPages->move(0, 0);
    repPages->resize(w, h);

    // Widget 2
    w = m_pW2->width(), h = m_pW2->height();
    pageArea->resize(w, h-75);
    pageArea->move(0, 30);

    m_pZoomNormal->move(0, 0);
    m_pZoomIn->move(45, 0);
    m_pZoomOut->move(45+40, 0);
    m_pGrid->move(45+40*3, 0);
    m_pSnap->move(45+40*4, 0);
    m_pBoundaries->move(45+40*5, 0);
    m_pProperties->move(45+40*7, 0);
    m_pDelete->move(45+40*8, 0);
    m_pMoveToFront->move(45+40*10, 0);
    m_pMoveToBack->move(45+40*11, 0);

#ifdef Q_WS_WIN
    m_pOk->move((w-10)/2-90, h-30);
    m_pCancel->move((w-10)/2+10, h-30);
#else
    m_pOk->move((w-10)/2+10, h-30);
    m_pCancel->move((w-10)/2-90, h-30);
#endif

    // Widget 3
    w = m_pW3->width(), h = m_pW3->height();
    int plh = 85+h/8;
    l2->move(0, h-plh-35);
    propList->move(0, h-plh-15);
    propList->resize(w, plh);

    l1->move(0, 0);
    fieldsList->move(0, 20);
    fieldsList->resize(w, h-plh-20-35);
}


void CReportCreator::updatePagesList()
{
    repPages->pagesList->clear();
    repPages->blocksList->clear();
    for (int a=0; a<pageArea->report.at(0)->pages.size(); a++)
    {
        //CILItem *item = repPages->pagesList->addItem(q->t("Pages") + " " + QString::number(a+1),0);
        CILItem *item = repPages->pagesList->addItem(pageArea->report.at(0)->pages.at(a)->m_PageName, 0, pageArea->report.at(0)->pages.at(a));
        item->pData = pageArea->report.at(0)->pages.at(a);
        if (pageArea->report.at(0)->pages.at(a)->m_PageSeq == 0)
            item->addonStr = QChar(0x221E);
        else
            item->addonStr = QString::number(pageArea->report.at(0)->pages.at(a)->m_PageSeq);
    }

    for (int a=0; a<pageArea->pblocks.size(); a++)
    {
        //CILItem *item = repPages->blocksList->addItem(q->t("Block") + " " + QString::number(a+1),0);
        CILItem *item = repPages->blocksList->addItem(pageArea->pblocks.at(a)->m_PageName,0);
        item->pData = pageArea->pblocks.at(a);
    }
    repPages->pagesList->update();
    repPages->blocksList->update();
}


void CReportCreator::applyItems()
{
}


void CReportCreator::updateFields()
{
}


void CReportCreator::updateFieldsSel()
{
}


void CReportCreator::OnAddField(QPoint pos)
{
    int fontSize = 11;
//    if (m_mode == REPORT_MODE)
//        fontSize = 16;
    if (fieldsList->selectedItems().size()>0)
    {
        for (int a=0; a<flitems.size(); a++)
        {
            if (fieldsList->selectedItems().at(0) == flitems.at(a))
            {
                bool cok;
                int idx = flitems.at(a)->data(0, Qt::UserRole).toInt(&cok);
                if (cok && (idx>0) && (idx<1000))
                {
                    // Data field
                    idx--;

//                    if (data->doc.docHeader.at(idx).dtype == CDataFile::ETImageList)
//                    {
//                        CREPageItem *item = pageArea->addDataItem(data->doc.docHeader.at(idx).dname, pos, QSize(120,120));
//                        item->m_Font.setPointSize(fontSize);
//                        item->id = data->doc.docHeader.at(idx).idname;
//                        item->isLabel = false;
//                        item->setType(OT_DATA_IMAGE);
//                    }
//                    else
//                    if (data->doc.docHeader.at(idx).dtype == CDataFile::ETIcon)
//                    {
//                        CREPageItem *item = pageArea->addDataItem(data->doc.docHeader.at(idx).dname, pos, QSize(120,120));
//                        item->m_Font.setPointSize(fontSize);
//                        item->id = data->doc.docHeader.at(idx).idname;
//                        item->isLabel = false;
//                        item->setType(OT_ICON);
//                    }
//                    else
//                    {
//                        CREPageItem *item = pageArea->addDataItem(data->doc.docHeader.at(idx).dname, pos, QSize(150,25));
//                        item->m_Font.setPointSize(fontSize);
//                        item->id = data->doc.docHeader.at(idx).idname;
//                        item->isLabel = false;
//                    }
                    if (flitems.at(a)->text(0) == "Name")
                    {
                        CREPageItem *item = pageArea->addDataItem(flitems.at(a)->text(0), pos, QSize(650, 100));
                        item->m_Font.setPixelSize(fontSize);
                        item->id = flitems.at(a)->text(0);
                        item->isLabel = false;
                        item->setType(OT_CALC_BLOCK_T2);
                    }
                    else
                    if (flitems.at(a)->text(0) == "Psychomatrix" || flitems.at(a)->text(0) == "Planes of Expression")
                    {
                        CREPageItem *item = pageArea->addDataItem(flitems.at(a)->text(0), pos, QSize(650, 68));
                        item->m_Font.setPixelSize(fontSize);
                        item->id = flitems.at(a)->text(0);
                        item->isLabel = false;
                        item->setType(OT_CALC_BLOCK_T1);
                    }
                    else
                    if (flitems.at(a)->text(0) == "Personal numbers calendar")
                    {
                        CREPageItem *item = pageArea->addDataItem(flitems.at(a)->text(0), pos, QSize(650, 52));
                        item->m_Font.setPixelSize(fontSize);
                        item->id = flitems.at(a)->text(0);
                        item->isLabel = false;
                        item->setType(OT_CALC_BLOCK_T3);
                    }
                    else
                    if (flitems.at(a)->text(0) == "Main Features" || flitems.at(a)->text(0) == "Additional Features" || flitems.at(a)->text(0) == "Life Cycles")
                    {
                        CREPageItem *item = pageArea->addDataItem(flitems.at(a)->text(0), pos, QSize(200, 100));
                        item->m_Font.setPixelSize(fontSize);
                        item->id = flitems.at(a)->text(0);
                        item->isLabel = false;
                        item->setType(OT_CALC_BLOCK_T4);
                    }
                    else
                    if (flitems.at(a)->text(0) == "Bridges")
                    {
                        CREPageItem *item = pageArea->addDataItem(flitems.at(a)->text(0), pos, QSize(200, 36));
                        item->m_Font.setPixelSize(fontSize);
                        item->id = flitems.at(a)->text(0);
                        item->isLabel = false;
                        item->setType(OT_CALC_BLOCK_T5);
                    }

                    if (isAddTitle == true)
                    {
                        CREPageItem *itemTit = pageArea->addDataItem(flitems.at(a)->text(0), pos, QSize(150,25));
                        itemTit->m_Font.setPixelSize(fontSize);
                        itemTit->id = flitems.at(a)->text(0);
                        itemTit->isLabel = true;
                        itemTit->setType(OT_DATA_FIELD_NAME);
                        itemTit->m_pOwner->move(0, -25);
                        pageArea->page->moveToPage(itemTit->m_pOwner);
                    }

                    pageArea->viewport()->update();
                } else if (cok && (idx>=1000) && (idx<2000))
                {
                    // Variables
                    if (idx==1000)
                    {
                        CREPageItem *itemTit = pageArea->addDataItem("FULL NAME", pos);
                        itemTit->m_Font.setPixelSize(fontSize);
                        itemTit->id = "#full_name";
                        itemTit->isLabel = true;
                        itemTit->setType(OT_DATA_FIELD_NAME);
                        pageArea->viewport()->update();
                    } else if (idx==1001)
                    {
                        CREPageItem *itemTit = pageArea->addDataItem("SHORT NAME", pos);
                        itemTit->m_Font.setPixelSize(fontSize);
                        itemTit->id = "#short_name";
                        itemTit->isLabel = true;
                        itemTit->setType(OT_DATA_FIELD_NAME);
                        pageArea->viewport()->update();
                    } else if (idx==1002)
                    {
                        CREPageItem *itemTit = pageArea->addDataItem("BIRTHDATE", pos);
                        itemTit->m_Font.setPixelSize(fontSize);
                        itemTit->id = "#birthdate";
                        itemTit->isLabel = true;
                        itemTit->setType(OT_DATA_FIELD_NAME);
                        pageArea->viewport()->update();
                    } else if (idx==1003)
                    {
                        CREPageItem *itemTit = pageArea->addDataItem("PRINT DATE", pos);
                        itemTit->m_Font.setPixelSize(fontSize);
                        itemTit->id = "#print_date";
                        itemTit->isLabel = true;
                        itemTit->setType(OT_DATA_FIELD_NAME);
                        pageArea->viewport()->update();
                    } else if (idx==1004)
                    {
                        CREPageItem *itemTit = pageArea->addDataItem("NUMEROLOGIST", pos);
                        itemTit->m_Font.setPixelSize(fontSize);
                        itemTit->id = "#numerologist";
                        itemTit->isLabel = true;
                        itemTit->setType(OT_DATA_FIELD_NAME);
                        pageArea->viewport()->update();
                    }
                } else if (cok && (idx>=2000) && (idx<3000))
                {
                    // Objects
                    // Text
                    if (idx==2000)
                    {
                        //CREPageItem *item = pageArea->addTextItem("Text", pos);
                        CREPageItem *item = pageArea->addDataItem("Text", pos);
                        item->m_Font.setPixelSize(fontSize);
                        item->id = "#text";
                        item->isTextBox = true;
                        item->isLabel = false;
                        item->setType(OT_DATA_FIELD_NAME);
                        pageArea->viewport()->update();
                    } else if (idx==2001)
                    {
                        QString fileName = QFileDialog::getOpenFileName(this, "Open Image", "", "Image Files (*.png *.jpg *.jpeg *.gif *.bmp)");

                        if (fileName.size()>4)
                        {
                            QImage img(fileName);
                            if (!img.isNull())
                            {
                                CREPageItem *item = pageArea->addImageItem(img, pos);
                                item->m_Font.setPixelSize(fontSize);
                                item->id = "#image";
                                pageArea->viewport()->update();
                            }
                        }
                    }
                    else if (idx==2002)
                    {
                        CREPageItem *item = pageArea->addDataItem("Bar Codes", pos);
                        item->m_Font.setPixelSize(fontSize);
                        item->id = "";
                        item->isTextBox = true;
                        item->isLabel = false;
                        item->setType(OT_BAR_CODE);
                        pageArea->viewport()->update();
                    }
                }
                break;
            }
        }
    }
}


void CReportCreator::OnBlockItemDropped(CILItem *item, QPoint pos)
{
    if (item==NULL)
        return;
    QPoint pnt = pageArea->mapFromGlobal(pos);

    CREPage *cpage = (CREPage *)item->pData;
    if ((pnt.x()>0) && (pnt.y()>0) && (pnt.x()<pageArea->width()) && (pnt.y()<pageArea->height()))
    {
        // Not allowed to drop into blocks
        for (int a=0; a<pageArea->pblocks.size(); a++)
        {
            if (pageArea->page == pageArea->pblocks.at(a))
                return;
        }


        // Not allowed to drop pages
        for (int a=0; a<pageArea->report.at(0)->pages.size(); a++)
        {
            if (cpage == pageArea->report.at(0)->pages.at(a))
                return;
        }

        cpage->generateThumbnail();
        CREPageItem *item = pageArea->addBlockItem(cpage, pnt);
        item->id = QString::number((qulonglong)cpage);
        item->m_pImg = cpage->getThumbnail();
        pageArea->viewport()->update();
    }
}


void CReportCreator::updateStates()
{
    QList<CREPageItem*> items;
    pageArea->page->getSelected(items);

    if (items.size()>0)
    {
        m_pDelete->setDisabled(false);
        m_pMoveToFront->setDisabled(false);
        m_pMoveToBack->setDisabled(false);
    } else
    {
        m_pDelete->setDisabled(true);
        m_pMoveToFront->setDisabled(true);
        m_pMoveToBack->setDisabled(true);
    }

    if (repPages->pagesList->getSelectedItem()==NULL)
    {
        repPages->m_pDelPage->setDisabled(true);
        repPages->m_pPageProp->setDisabled(true);
    }
    else if (pageArea->report.at(0)->pages.size()<2)
    {
        repPages->m_pDelPage->setDisabled(true);
        repPages->m_pPageProp->setDisabled(false);
    }
    else
    {
        repPages->m_pDelPage->setDisabled(false);
        repPages->m_pPageProp->setDisabled(false);
    }

    if (repPages->blocksList->getSelectedItem()==NULL)
    {
        repPages->m_pDelBlock->setDisabled(true);
        repPages->m_pBlockProp->setDisabled(true);
    }
    else
    {
        repPages->m_pDelBlock->setDisabled(false);
        repPages->m_pBlockProp->setDisabled(false);
    }
}


void CReportCreator::OnPageSelectionChanged()
{
    QList<CREPageItem*> items;
    pageArea->page->getSelected(items);

    if (items.size()==1)
    {
        if (items.at(0)->m_pContainer!=NULL)
        {
            CREPageItem *item = items.at(0)->m_pContainer;
            if (item->id.startsWith("#text"))
            {
                CPWItem *item1 = new CPWItem("Text", item->m_Text);
                CPWItem *item2 = new CPWItem("Color", item->m_ShapeFillColor);
                CPWItem *item3 = new CPWItem("Font", item->m_Font);
                item3->m_str = item->m_Text;
                CPWItem *item4 = new CPWItem("Frame", PWIT_COMBO);
                item4->m_comboIdx = item->m_Frame;
                item4->m_comboVals << "None" << "Solid";
                CPWItem *item5 = new CPWItem("Frame Color", item->m_FrameColor);

                QVector<CPWItem*> pitems;
                pitems << item1 << item2 << item3 << item4 << item5;// << item6;

                propList->setList(pitems);
            } else if (item->m_Type == OT_DATA_IMAGE)
            {
                CPWItem *item1 = new CPWItem("Image #", PWIT_COMBO);
                item1->m_comboIdx = item->m_Idx;
                for (int k=1; k<30; k++)
                    item1->m_comboVals << QString::number(k);
                CPWItem *item2 = new CPWItem("Alignement", PWIT_ALIGN);
                item2->m_alignement = item->m_Align;
                CPWItem *item3 = new CPWItem("Fit to", PWIT_COMBO);
                item3->m_comboIdx = item->m_Fit;
                item3->m_comboVals << "None" << "Width";

                QVector<CPWItem*> pitems;
                pitems << item1 << item2 << item3;

                propList->setList(pitems);
            } else if (item->m_Type == OT_ICON)
            {
                CPWItem *item1 = new CPWItem("Alignement", PWIT_ALIGN);
                item1->m_alignement = item->m_Align;
                CPWItem *item2 = new CPWItem("Image #", PWIT_COMBO);
                item2->m_comboIdx = item->iconNum;
                item2->m_comboVals << "All" << "1" << "2" << "3";


                QVector<CPWItem*> pitems;
                pitems << item1 << item2;

                propList->setList(pitems);
            } else if (item->m_Type == OT_IMAGE)
            {
                propList->clear();
            } else if (item->m_Type == OT_BLOCK)
            {
                // Re-Indexing Orders
                CREPage *cpage = pageArea->page;
                QList<CREPageItem*> blks; // List of blocks
                for (int a=0; a<cpage->m_PageObjects.size(); a++)
                {
                    CREPageItem *pitem = cpage->m_PageObjects.at(a);
                    if (pitem->m_pContainer!=NULL)
                    {
                        if (pitem->m_pContainer->m_Type == OT_BLOCK)
                        {
                            int blkpos=0;
                            for (blkpos=0; blkpos<blks.size(); blkpos++)
                            {
                                if (blks.at(blkpos)->m_iopt > pitem->m_pContainer->m_iopt)
                                    break;
                            }
                            blks.insert(blkpos, pitem->m_pContainer);
                        }
                    }
                }

                for (int blkpos=0; blkpos<blks.size(); blkpos++)
                    blks.at(blkpos)->m_iopt=blkpos;


                CPWItem *item1 = new CPWItem("Order #", PWIT_COMBO);
                item1->m_comboIdx = item->m_iopt;
                for (int k=1; k<blks.size()+1; k++)
                    item1->m_comboVals << QString::number(k);

                CPWItem *item2 = new CPWItem("Frame", PWIT_COMBO);
                item2->m_comboIdx = item->m_Frame;
                item2->m_comboVals << "None" << "Solid";
                CPWItem *item3 = new CPWItem("Frame Color", item->m_FrameColor);

                QVector<CPWItem*> pitems;
                pitems << item1 << item2 << item3;

                propList->setList(pitems);
            }
            else
            if (item->m_Type == OT_BAR_CODE)
            {
                CPWItem *item0 = new CPWItem("Field", PWIT_COMBO);
                int numCombRow = 0;
                item0->m_comboVals << "some text";
//                for (int i = 0; i < idFieldsListForBarC.size(); i++)
//                {
//                    if (item->id == idFieldsListForBarC.at(i))
//                       numCombRow = i + 1;
//                    item0->m_comboVals << data->doc.docHeader.at(data->indexByFieldID(idFieldsListForBarC.at(i))).dname;
//                }
                item0->m_comboIdx = numCombRow;

                CPWItem *item1 = new CPWItem("Bar code", PWIT_COMBO);
                item1->m_comboVals << "QR Code" << "Codabar" << "Code 128" << "Code 39" << "Code 93" << "EAN 13";
                item1->m_comboVals << "EAN 8" << "Interleaved 2 of 5" << "MSI" << "Postnet" << "UPC";
                item1->m_comboIdx = item->barCodes;

                CPWItem *item2 = new CPWItem("Text", item->m_Text);

                CPWItem *item3 = new CPWItem("Color", item->m_ShapeFillColor);

                CPWItem *item4 = new CPWItem("Alignement", PWIT_ALIGN);
                item4->m_alignement = item->m_Align;

                QVector<CPWItem*> pitems;
                pitems << item0 << item1 << item2 << item3 << item4;

                propList->setList(pitems);
            }
            else
            if (item->m_Type == OT_CALC_BLOCK_T1 || item->m_Type == OT_CALC_BLOCK_T2 || item->m_Type == OT_CALC_BLOCK_T3
                    || item->m_Type == OT_CALC_BLOCK_T4 || item->m_Type == OT_CALC_BLOCK_T5)
            {
                CPWItem *item1 = new CPWItem("Color", item->m_ShapeFillColor);
                CPWItem *item2 = new CPWItem("Font", item->m_Font);
                CPWItem *item3 = new CPWItem("Alignement", PWIT_ALIGN);
                item3->m_alignement = item->m_Align;
                CPWItem *item4 = new CPWItem("Frame", PWIT_COMBO);
                item4->m_comboIdx = item->m_Frame;
                item4->m_comboVals << "None" << "Solid";
                CPWItem *item5 = new CPWItem("Frame Color", item->m_FrameColor);
				CPWItem *item6 = new CPWItem("Background", item->m_BackgroundColor);
				CPWItem *item7 = new CPWItem("Opacity", PWIT_COMBO);
				item7->m_comboIdx = (int)(((double)item->m_BackgroundColor.alpha())/25.5);
				for (int o=0;o<=100; o+=10)
					item7->m_comboVals << QString::number(o)+"%";

                QVector<CPWItem*> pitems;
                pitems << item1 << item2 << item3 << item4 << item5 << item6 << item7;

                propList->setList(pitems);
            }
            else
            {
                CPWItem *item1 = new CPWItem("Color", item->m_ShapeFillColor);
                CPWItem *item2 = new CPWItem("Font", item->m_Font);
                CPWItem *item3 = new CPWItem("Alignement", PWIT_ALIGN);
                item3->m_alignement = item->m_Align;
                CPWItem *item4 = new CPWItem("Frame", PWIT_COMBO);
                item4->m_comboIdx = item->m_Frame;
                item4->m_comboVals << "None" << "Solid";
                CPWItem *item5 = new CPWItem("Frame Color", item->m_FrameColor);

                CPWItem *item6;
                if (item->isLabel == false)
                {
                    item6 = new CPWItem("Field", PWIT_COMBO);
                    int numCombRow = -1;
//                    for (int i = 0; i < idFieldsList.size(); i++)
//                    {
//                        if (item->id == idFieldsList.at(i))
//                           numCombRow = i;
//                        item6->m_comboVals << data->doc.docHeader.at(data->indexByFieldID(idFieldsList.at(i))).dname;
//                    }
                    //item6->m_comboVals << q->t("Text") << q->t("Page number") << q->t("Date") << q->t("DB Name");

                    if (numCombRow == -1)
                    {
                        if (item->id.startsWith("#text"))
                            numCombRow = idFieldsList.size();
                        else
                        if (item->id.startsWith("#pagenumber"))
                            numCombRow = idFieldsList.size() + 1;
                        else
                        if (item->id.startsWith("#date"))
                            numCombRow = idFieldsList.size() + 2;
                        else
                        if (item->id.startsWith("#dbname"))
                            numCombRow = idFieldsList.size() + 3;
                        else
                            numCombRow = 0;
                    }

                    item6->m_comboIdx = numCombRow;
                }

                QVector<CPWItem*> pitems;
                pitems << item1 << item2 << item3 << item4 << item5;

                if (item->isLabel == false)
                   pitems << item6;

                propList->setList(pitems);
            }
            //
        }

    } else
    {
        propList->clear();
    }
    updateStates();
}


void CReportCreator::OnPropertyChanged()
{
    QList<CREPageItem*> items;
    pageArea->page->getSelected(items);

    bool isUpdate = false;

    if (items.size()==1)
    {
        if (items.at(0)->m_pContainer!=NULL)
        {
            CREPageItem *item = items.at(0)->m_pContainer;
            if (item->id.startsWith("#text"))
            {
                if (propList->m_items.size() >= 5)
                {
                    item->setText(propList->m_items.at(0)->m_str);
                    item->setShapeColor(propList->m_items.at(1)->m_col);
                    item->setFont(propList->m_items.at(2)->m_font);
                    item->m_Frame = propList->m_items.at(3)->m_comboIdx;
                    item->m_FrameColor = propList->m_items.at(4)->m_col;
                    if (propList->m_items.size() >= 6)
                        itemConverter(item, propList, 5, &isUpdate);
                }
            } else if (item->m_Type == OT_DATA_IMAGE)
            {
                if (propList->m_items.size() == 3)
                {
                    item->m_Idx = propList->m_items.at(0)->m_comboIdx;
                    item->m_Align = propList->m_items.at(1)->m_alignement;
                    item->m_Fit = propList->m_items.at(2)->m_comboIdx;
                }
            } else if (item->m_Type == OT_ICON)
            {
                if (propList->m_items.size() == 2)
                {
                    item->m_Align = propList->m_items.at(0)->m_alignement;
                    item->iconNum = propList->m_items.at(1)->m_comboIdx;
                }
            } else if (item->m_Type == OT_IMAGE)
			{
            } else if (item->m_Type == OT_BLOCK)
            {
                if (propList->m_items.size() == 3)
                {
                    int pord = item->m_iopt;
                    int nord = propList->m_items.at(0)->m_comboIdx;
                    // Updating Orders
                    CREPage *cpage = pageArea->page;
                    int blkcnt=0;
                    for (int a=0; a<cpage->m_PageObjects.size(); a++)
                    {
                        CREPageItem *pitem = cpage->m_PageObjects.at(a);
                        if (pitem->m_pContainer!=NULL)
                        {
                            if (pitem->m_pContainer->m_Type == OT_BLOCK)
                            {
                                if (pitem->m_pContainer->m_iopt > pord)
                                    pitem->m_pContainer->m_iopt--;
                                if (pitem->m_pContainer->m_iopt >= nord)
                                    pitem->m_pContainer->m_iopt++;
                                blkcnt++;
                            }
                        }
                    }

                    if (nord>blkcnt-1)
                        nord=blkcnt-1;
                    item->m_iopt = nord;

                    item->m_Frame = propList->m_items.at(1)->m_comboIdx;
                    item->m_FrameColor = propList->m_items.at(2)->m_col;
                }
            } else if (item->m_Type == OT_BAR_CODE)
            {
                if (propList->m_items.at(0)->m_comboIdx > 0)
                    item->id = idFieldsListForBarC.at(propList->m_items.at(0)->m_comboIdx - 1);
                else
                    item->id = "";

                item->barCodes = propList->m_items.at(1)->m_comboIdx;
                item->m_Text = propList->m_items.at(2)->m_str;
                item->m_ShapeFillColor = propList->m_items.at(3)->m_col;
                item->m_Align = propList->m_items.at(4)->m_alignement;
            }
			else if (item->m_Type == OT_CALC_BLOCK_T1 || item->m_Type == OT_CALC_BLOCK_T2 || item->m_Type == OT_CALC_BLOCK_T3
				|| item->m_Type == OT_CALC_BLOCK_T4 || item->m_Type == OT_CALC_BLOCK_T5)
			{
				if (propList->m_items.size() >= 7)
				{
					item->setShapeColor(propList->m_items.at(0)->m_col);
					item->setFont(propList->m_items.at(1)->m_font);
					item->m_Align = propList->m_items.at(2)->m_alignement;
					item->m_Frame = propList->m_items.at(3)->m_comboIdx;
					item->m_FrameColor = propList->m_items.at(4)->m_col;

					int alphaidx = propList->m_items.at(6)->m_comboIdx;
					item->m_BackgroundColor = propList->m_items.at(5)->m_col;
					item->m_BackgroundColor.setAlpha((int)((double)alphaidx*25.5));
				}

			} else
			{
                if (propList->m_items.size() >= 5)
                {
                    item->setShapeColor(propList->m_items.at(0)->m_col);
                    item->setFont(propList->m_items.at(1)->m_font);
                    item->m_Align = propList->m_items.at(2)->m_alignement;
                    item->m_Frame = propList->m_items.at(3)->m_comboIdx;
                    item->m_FrameColor = propList->m_items.at(4)->m_col;
                    if (propList->m_items.size() >= 6)
                        itemConverter(item, propList, 5, &isUpdate);
                }

            }
            if (isUpdate == true)
                OnPageSelectionChanged();
			pageArea->viewport()->update();
        }  
    }
}

void CReportCreator::itemConverter(CREPageItem *item, CPropertiesWidget *propList, int propertiesId, bool *isUpdate)
{
    QString olId = item->id;
    if (propList->m_items.at(propertiesId)->m_comboIdx >= idFieldsList.size())
    {
        int type = propList->m_items.at(propertiesId)->m_comboIdx - idFieldsList.size();
        switch (type)
        {
            case 0:
                item->id = "#text";
                item->m_Align = 0;
                item->m_Type = OT_DATA_FIELD_NAME;
                item->isTextBox = true;
                item->isLabel = false;
                break;
            case 1:
                item->id = "#pagenumber";
                item->m_Align = 0;
                item->m_Type = OT_DATA_FIELD_NAME;
                item->isLabel = true;
                item->isTextBox = false;
                item->m_Text = "PAGE #";
                break;
            case 2:
                item->id = "#date";
                item->m_Align = 0;
                item->m_Type = OT_DATA_FIELD_NAME;
                item->isLabel = true;
                item->isTextBox = false;
                item->m_Text = "DATE";
                break;
            case 3:
                item->id = "#dbname";
                item->m_Align = 0;
                item->m_Type = OT_DATA_FIELD_NAME;
                item->isLabel = true;
                item->isTextBox = false;
                item->m_Text = "DB NAME";
                break;
            default:
                break;
        }

    }
    else
    {
        item->id = idFieldsList.at(propList->m_items.at(propertiesId)->m_comboIdx);
        //item->m_Text = data->doc.docHeader.at(data->indexByFieldID(item->id)).dname;
        item->isLabel = false;
        item->isTextBox = false;
    }
    if (olId != item->id)
    {
        *isUpdate = true;
    }
}


void CReportCreator::OnPageSelected(CILItem *item)
{
    if (item==NULL)
        return;

    CREPage *cpage = (CREPage *)item->pData;
    if (cpage==NULL)
        return;

    repPages->pagesList->setCurrentItemByPData(cpage);
    repPages->blocksList->setCurrentItemByPData(cpage);
    if (pageArea->page != cpage)
    {
        // Update page thumb
        if  (pageArea->page != NULL)
        {
            QString pid = QString::number((qulonglong)pageArea->page);
            pageArea->page->generateThumbnail();
            QImage *img = pageArea->page->getThumbnail();
            for (int r=0; r<pageArea->report.at(0)->pages.size(); r++)
            {
                for (int a=0; a<pageArea->report.at(0)->pages.at(r)->m_PageObjects.size(); a++)
                {
                    CREPageItem *pitem = pageArea->report.at(0)->pages.at(r)->m_PageObjects.at(a);
                    if (pitem->m_pContainer!=NULL)
                        if (pitem->m_pContainer->m_Type == OT_BLOCK)
                            if (pitem->m_pContainer->id == pid)
                                pitem->m_pContainer->m_pImg = img;
                }
            }
        }

        pageArea->page = cpage;
        pageArea->updateView();
    }
    updateStates();
}


void CReportCreator::OnBlockProp()
{
    setBlockOrderFromBlocksList();

    CILItem *item = repPages->blocksList->getSelectedItem();
    if (item==NULL)
        return;

    CREPage *cblock = (CREPage*)item->pData;
    if (cblock==NULL)
        return;

    CPaperSizeDlgWin *repdlg = new CPaperSizeDlgWin(appstyle, this, true);

    double w = pageArea->page->m_PageSize.width()/(4*25.4);
    double h = pageArea->page->m_PageSize.height()/(4*25.4);
    repdlg->mainWidget->m_pWidth->setText(QString::number(w, 'f', 2));
    repdlg->mainWidget->m_pHeight->setText(QString::number(h, 'f', 2));
    repdlg->mainWidget->selPaperCombo();
    repdlg->mainWidget->m_pReportName->setText(pageArea->page->m_PageName);


    repdlg->exec();

    if (repdlg->mainWidget->res==0)
    {
        pageArea->page->m_PageName = repdlg->mainWidget->m_pReportName->text();
        pageArea->page->setPageSize(QSizeF(repdlg->mainWidget->rw*4*25.4, repdlg->mainWidget->rh*4*25.4));
        pageArea->OnZoomNormal();

        item->name = pageArea->page->m_PageName;
        item->update();
    }

    delete repdlg;
}

void CReportCreator::OnListDrop()
{
    pageArea->report.at(0)->pages.clear();
    for (int i = 0; i < repPages->pagesList->lmcs.at(0)->lmitems.size(); i++)
    {
        pageArea->report.at(0)->pages.append(repPages->pagesList->lmcs.at(0)->lmitems.at(i)->page);
    }
}

void CReportCreator::OnBlockDrop()
{
    pageArea->pblocks.clear();
    for (int it=0; it<repPages->blocksList->lmcs.at(0)->lmitems.size(); it++)
    {
        CREPage *p1 = (CREPage*)(repPages->blocksList->lmcs.at(0)->lmitems.at(it)->pData);
        pageArea->pblocks.append(p1);
    }
}

void CReportCreator::OnPageProp()
{
    setPageOrderFromPagesList();

    CILItem *item = repPages->pagesList->getSelectedItem();
    if (item==NULL)
        return;

    CREPage *cblock = (CREPage*)item->pData;
    if (cblock==NULL)
        return;

    CPaperSizeDlgWin *repdlg = new CPaperSizeDlgWin(appstyle, this, true);

    double w = pageArea->page->m_PageSize.width()/(4*25.4);
    double h = pageArea->page->m_PageSize.height()/(4*25.4);
    repdlg->mainWidget->m_pWidth->setText(QString::number(w, 'f', 2));
    repdlg->mainWidget->m_pHeight->setText(QString::number(h, 'f', 2));
    repdlg->mainWidget->selPaperCombo();
    repdlg->mainWidget->m_pReportName->setText(pageArea->page->m_PageName);
    repdlg->mainWidget->rseq = pageArea->page->m_PageSeq;
    repdlg->mainWidget->addSeqCombo();

    repdlg->exec();

    if (repdlg->mainWidget->res==0)
    {
        pageArea->page->m_PageName = repdlg->mainWidget->m_pReportName->text();
        pageArea->page->setPageSize(QSizeF(repdlg->mainWidget->rw*4*25.4, repdlg->mainWidget->rh*4*25.4));
        pageArea->page->m_PageSeq = repdlg->mainWidget->rseq;
        pageArea->OnZoomNormal();

        item->name = pageArea->page->m_PageName;
        if (pageArea->page->m_PageSeq == 0)
            item->addonStr = QChar(0x221E);
        else
            item->addonStr = QString::number(pageArea->page->m_PageSeq);
        item->update();
    }

    delete repdlg;
}


void CReportCreator::OnAddBlock()
{
    QSizeF sz;
    QString rname;

    CPaperSizeDlgWin *repdlg = new CPaperSizeDlgWin(appstyle, this, true);
    repdlg->mainWidget->selPaperCombo();
    repdlg->exec();

    if (repdlg->mainWidget->res==0)
    {
        rname = repdlg->mainWidget->m_pReportName->text();
        sz = QSizeF(repdlg->mainWidget->rw*4*25.4, repdlg->mainWidget->rh*4*25.4);
    } else
    {
        delete repdlg;
        return;
    }
    delete repdlg;

    CREPage *cpage = pageArea->addBlock(sz, rname);
    updatePagesList();
    repPages->pagesList->clearSel();
    repPages->blocksList->setCurrentItemByPData(cpage);
    pageArea->page = cpage;
    pageArea->updateView();
    updateStates();
}


void CReportCreator::OnDelBlock()
{
    setBlockOrderFromBlocksList();

    CILItem *item = repPages->blocksList->getSelectedItem();
    if (item==NULL)
        return;

    CREPage *cblock = (CREPage*)item->pData;
    if (cblock==NULL)
        return;

    //int res = CInfoDlgWin::showAskMessageBox(q->t("Question"), q->t("Are You sure?") + "\r\n" + q->t("Do You want to delete selected item?"), settings, this);
    int res = CInfoDlgWin::showAskMessageBox(appstyle, "Question", "Are You sure?\r\nDo You want to delete selected item?", this);
    //int res = QMessageBox::question(0, "Question", "Are You sure?\r\nDo You want to delete selected item?\r\n", tr("&Yes"), tr("&No"), QString::null, 0, 1 );
    if (res!=0)
        return;

    // Removing all blocks from pages
    QString pid = QString::number((qulonglong)cblock);
    for (int r=0; r<pageArea->report.at(0)->pages.size(); r++)
    {
        bool done;
        do
        {
            done=true;
            for (int a=0; a<pageArea->report.at(0)->pages.at(r)->m_PageObjects.size(); a++)
            {
                CREPageItem *pitem = pageArea->report.at(0)->pages.at(r)->m_PageObjects.at(a);
                if (pitem->m_pContainer!=NULL)
                    if (pitem->m_pContainer->m_Type == OT_BLOCK)
                        if (pitem->m_pContainer->id == pid)
                        {
                            pageArea->report.at(0)->pages.at(r)->m_PageObjects.remove(a);
                            done=false;
                            break;
                        }
            }
        } while (!done);
    }

    // Deleting block from list
    for (int r=0; r<pageArea->pblocks.size(); r++)
    {
        if (pageArea->pblocks.at(r) == cblock)
        {
            pageArea->pblocks.remove(r);
            break;
        }
    }

    // Setting new current page
    pageArea->page = pageArea->report.at(0)->pages.at(0);
    updatePagesList();
    repPages->pagesList->setCurrentItemByPData(pageArea->page);
    pageArea->update();
    updateStates();

    // Deleting block
    delete cblock;
}


void CReportCreator::OnAddPage()
{
    QSizeF sz = pageArea->report.at(0)->pages.at(0)->m_PageSize;
    QString rname = "Pages " + QString::number(pageArea->report.at(0)->pages.size()+1);

    CREPage *cpage = pageArea->addPage(sz, rname);
    updatePagesList();
    repPages->blocksList->clearSel();
    repPages->pagesList->setCurrentItemByPData(cpage);
    pageArea->page = cpage;
    pageArea->updateView();
    updateStates();
}


void CReportCreator::OnDelPage()
{
    setPageOrderFromPagesList();

    if (pageArea->report.at(0)->pages.size()<2)
        return;

    CILItem *item = repPages->pagesList->getSelectedItem();
    if (item==NULL)
        return;

    CREPage *cblock = (CREPage*)item->pData;
    if (cblock==NULL)
        return;

    //int res = CInfoDlgWin::showAskMessageBox(q->t("Question"), q->t("Are You sure?") + "\r\n" + q->t("Do You want to delete selected item?"), settings, this);
    int res = CInfoDlgWin::showAskMessageBox(appstyle, "Question", "Are You sure?\r\nDo You want to delete selected item?", this);
    //int res = QMessageBox::question(0, "Question", "Are You sure?\r\nDo You want to delete selected item?\r\n", tr("&Yes"), tr("&No"), QString::null, 0, 1 );
    if (res!=0)
        return;

    // Deleting page from list
    for (int r=0; r<pageArea->report.at(0)->pages.size(); r++)
    {
        if (pageArea->report.at(0)->pages.at(r) == cblock)
        {
            pageArea->report.at(0)->pages.remove(r);
            break;
        }
    }

    // Setting new current page
    pageArea->page = pageArea->report.at(0)->pages.at(0);
    updatePagesList();
    repPages->pagesList->setCurrentItemByPData(pageArea->page);
    pageArea->update();
    updateStates();

    // Deleting block
    delete cblock;
}


void CReportCreator::setBlockOrderFromBlocksList()
{
    pageArea->pblocks.clear();
    for (int it=0; it<repPages->blocksList->lmcs.at(0)->lmitems.size(); it++)
    {
        CREPage *p1 = (CREPage*)(repPages->blocksList->lmcs.at(0)->lmitems.at(it)->pData);
        pageArea->pblocks.append(p1);
    }
}


void CReportCreator::setPageOrderFromPagesList()
{
    QVector<CREPage*> respages;

    for (int it=0; it<repPages->pagesList->lmcs.at(0)->lmitems.size(); it++)
    {
        CREPage *p1 = (CREPage*)(repPages->pagesList->lmcs.at(0)->lmitems.at(it)->pData);
        respages.append(p1);
    }
    pageArea->report[0]->pages = respages;
}



#endif
CReportCreator::~CReportCreator()
{
}

CReportCreatorWin::CReportCreatorWin(calcNumContainer *pNumContainer, CAppStyle *style, QWidget *parent, int mode, int *trial)
: QDialog(parent)
{
    mainWidget = new CReportCreator(pNumContainer, style, this, mode, trial);
}


CReportCreatorWin::~CReportCreatorWin()
{
}
