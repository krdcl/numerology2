#ifndef PROPERTIESWIDGET_H
#define PROPERTIESWIDGET_H

#include <QWidget>
#include <QtGui>
#include "appstyle.h"
//#include "datafile.h"


enum PWItemType { PWIT_STRING=0, PWIT_COLOR=1, PWIT_FONT=2, PWIT_ALIGN=3, PWIT_IMAGE=4, PWIT_COMBO=5, PWIT_ECOMBO=6 };

class CPWItem : public QObject
{
    Q_OBJECT
public:
    QColor m_col;
    QString m_str;
    QFont m_font;
    int m_alignement;

    int m_type;
    QString m_name;

    int m_comboIdx;
    QStringList m_comboVals;

    QPushButton *but;
    QLineEdit *edit;
    QComboBox *combo;
    QImage *image;

    CPWItem()
    {
        m_type = PWIT_STRING;
        m_name = "";
        m_str = "";
        but = NULL;
        edit = NULL;
        combo = NULL;
        image = NULL;
        m_comboIdx = 0;
    }

    CPWItem(QString name, int type)
    {
        m_type = type;
        m_name = name;
        but = NULL;
        edit = NULL;
        combo = NULL;
        image = NULL;
        m_comboIdx = 0;
    }

    CPWItem(QString name, QString text)
    {
        m_type = PWIT_STRING;
        m_name = name;
        m_str = text;
        but = NULL;
        edit = NULL;
        combo = NULL;
        image = NULL;
        m_comboIdx = 0;
    }

    CPWItem(QString name, QColor color)
    {
        m_type = PWIT_COLOR;
        m_name = name;
        m_col = color;
        but = NULL;
        edit = NULL;
        combo = NULL;
        image = NULL;
        m_comboIdx = 0;
    }

    CPWItem(QString name, QFont font)
    {
        m_type = PWIT_FONT;
        m_name = name;
        m_font = font;
        but = NULL;
        edit = NULL;
        combo = NULL;
        image = NULL;
        m_comboIdx = 0;
    }

    ~CPWItem()
    {
        //if (but != NULL)
        //    delete but;
        //if (edit != NULL)
        //    delete edit;
        //if (combo != NULL)
        //    delete combo;
        if (image != NULL)
            delete image;
    }

signals:
    void itemButtonClicked(CPWItem*);

public slots:
    void OnItemButton()
    {
        emit itemButtonClicked(this);
    }
    void OnItemCombo(int)
    {
        emit itemButtonClicked(this);
    }
};


class CPropertiesWidget : public QWidget
{
    Q_OBJECT
public:
    //CDataFile *data;
    //CAppSettings *settings;
    //CTranslate *q;

    QVector<CPWItem*> m_items;
    void clear();
    void setList(QVector<CPWItem*> items);

    QTableWidget *tablelist;

    CPropertiesWidget(CAppStyle *style, QWidget *parent);
    ~CPropertiesWidget();

    QString qsResPath;
    QLabel *lableFixScroll;

    CAppStyle *appstyle;
    
protected:
    void resizeEvent(QResizeEvent *);
    //void paintEvent(QPaintEvent *);
    void updateFixScroll();

signals:
    void dataChanged();

public slots:
    void OnItemsActions(CPWItem*);
    void OnItemsChanged(QTableWidgetItem*);
    
};

#endif // PROPERTIESWIDGET_H
