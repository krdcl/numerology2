#include "iconlist.h"
//#include "infodlg.h"


CIconList::CIconList(/*CDataFile *cdata, CAppSettings *csettings, */QWidget *parent)
    : QWidget(parent)
{
//    data = cdata;
//    settings = csettings;
//    q = settings->q;
#ifdef Q_OS_MAC
    qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/");
#else
    qsResPath = QCoreApplication::applicationDirPath() + QString("/images/");
#endif

    setMouseTracking(true);
    citem = NULL;
    cursitem = NULL;
    ddstarted = false;
    ddprepare = false;
    ilcont.load(qsResPath+"ilcont.png");
    ilitem.load(qsResPath+"ilitem.png");


    int w1 = 100, h1 = 250;

    CILContainer *lmc1 = new CILContainer(this);
    lmc1->resize(w1, h1);
    lmc1->move(0, 0);

    lmcs << lmc1;

    setItemPicture(&ilitem);

    /*for (int a=0; a<lmcs.size(); a++)
    {
        QLabel *bkw;
        bkw = new QLabel(this);
        bkw->lower();
        bkw->setGeometry(lmcs.at(a)->geometry());
        bkw->setStyleSheet("QWidget, QLabel { background: transparent; border: none; }");
        bkw->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        if (lmcs.at(a)->height()==160)
            bkw->setPixmap(QPixmap(settings->appstyle->lmcont160));
        else if (lmcs.at(a)->height()==190)
            bkw->setPixmap(QPixmap(settings->appstyle->lmcont190));
        else
            bkw->setPixmap(QPixmap(settings->appstyle->lmcont));

        lmcs.at(a)->setGeometry(lmcs.at(a)->geometry().adjusted(1,1,-1,-1));
    }*/

    /*for (int a=0; a<data->doc.docHeader.size(); a++)
    {
        if ((data->doc.docHeader.at(a).dtype != CDataFile::ETDBEntryIndex) && (data->doc.docHeader.at(a).dtype != CDataFile::ETIcon))
        {
            int lid = data->doc.docHeader.at(a).layoutID;
            if ((lid<0) || (lid>=lmcs.size()))
                continue;
            CLMItem *item = new CLMItem(settings, lmcs.at(lid)->widg);
            item->id = a;
            item->name = q->t(data->doc.docHeader.at(a).dname);
            item->resize(lmc1->itemdw,lmc1->itemdh);
            item->vflags = data->doc.docHeader.at(a).vflags;
            item->lpr = data->doc.docHeader.at(a).layoutPr;
            QObject::connect(item, SIGNAL(startDD(void *)), this, SLOT(OnStartDD(void *)));

            int p=lmcs.at(lid)->lmitems.size();
            for (int n=0; n<lmcs.at(lid)->lmitems.size(); n++)
            {
                if (item->lpr<lmcs.at(lid)->lmitems.at(n)->lpr)
                {
                    p = n;
                    break;
                }
            }
            lmcs.at(lid)->lmitems.insert(p, item);
        }
    }*/


//    tmrid = startTimer(30);
}


CIconList::~CIconList()
{
 //   killTimer(tmrid);
}


void CIconList::updateList()
{
    for (int a=0; a<lmcs.size(); a++)
        lmcs.at(a)->updateContainer();
}


CILItem* CIconList::addItem(QString name, int id, CREPage *page)
{
    int lmidx=0;
    CILItem *item = new CILItem(/*settings, */lmcs.at(lmidx)->widg);
    item->iticon = &iticon;
    item->id = id;
    item->page = page;
    item->name = name;
    item->resize(lmcs.at(lmidx)->itemdw, lmcs.at(lmidx)->itemdh);
    //item->vflags = data->doc.docHeader.at(a).vflags;
    //item->lpr = data->doc.docHeader.at(a).layoutPr;
    QObject::connect(item, SIGNAL(startDD(void *)), this, SLOT(OnStartDD(void *)));
    lmcs.at(lmidx)->lmitems.push_back(item);
    item->show();
    lmcs.at(lmidx)->updateContainer();
    return item;
}


void CIconList::clearSel()
{
    for (int a=0; a<lmcs.size(); a++)
    {
        for (int n=0; n<lmcs.at(a)->lmitems.size(); n++)
        {
            if (lmcs.at(a)->lmitems.at(n)->sel)
            {
                lmcs.at(a)->lmitems.at(n)->sel = false;
                lmcs.at(a)->lmitems.at(n)->repaint();
            }
        }
    }
    citem = NULL;
}

void CIconList::clear()
{
    citem = NULL;

    for (int a=0; a<lmcs.size(); a++)
    {
        for (int n=0; n<lmcs.at(a)->lmitems.size(); n++)
        {
            delete lmcs.at(a)->lmitems.at(n);
        }
        lmcs.at(a)->lmitems.clear();
    }
}


void CIconList::setCurrentItem(CILItem *item, bool sel)
{
    if (sel)
        clearSel();
    if (item==NULL)
        return;
    for (int a=0; a<lmcs.size(); a++)
    {
        for (int n=0; n<lmcs.at(a)->lmitems.size(); n++)
        {
            if (lmcs.at(a)->lmitems.at(n) == item)
            {
                citem = lmcs.at(a)->lmitems.at(n);
                if (sel)
                    citem->sel = true;
                citem->repaint();
                break;
            }
        }
    }
}


void CIconList::setCurrentItemByPData(void *pData, bool sel)
{
    if (sel)
        clearSel();
    if (pData == NULL)
        return;
    for (int a=0; a<lmcs.size(); a++)
    {
        for (int n=0; n<lmcs.at(a)->lmitems.size(); n++)
        {
            if (lmcs.at(a)->lmitems.at(n)->pData == pData)
            {
                citem = lmcs.at(a)->lmitems.at(n);
                if (sel)
                    citem->sel = true;
                citem->repaint();
                break;
            }
        }
    }
}


CILItem* CIconList::getSelectedItem()
{
    for (int a=0; a<lmcs.size(); a++)
    {
        for (int n=0; n<lmcs.at(a)->lmitems.size(); n++)
        {
            if (lmcs.at(a)->lmitems.at(n)->sel)
            {
                return lmcs.at(a)->lmitems.at(n);
            }
        }
    }
    return NULL;
}


void CIconList::OnStartDD(void *ip)
{
    setCurrentItem((CILItem *)ip, false);

    spnt = QCursor::pos();
    ddprepare = true;
    updateFields();
    //emit itemSelected(citem);
}


void CIconList::updateFields()
{
    if (citem!=NULL)
    {
        // Field selected
    }
}


void CIconList::processItems(QWidget *parW)
{
    if (citem == NULL)
    {
        //Disable states
    } else
    {
        //Enable states
    }
    {
        QPoint globPnt = QCursor::pos();
        if (citem!=NULL)
        {
            QPoint cpnt = QCursor::pos();
            Qt::MouseButtons mbs = QApplication::mouseButtons();
            if (mbs.testFlag(Qt::LeftButton))
            {
                if ((!ddstarted) && ddprepare && (abs(spnt.x()-cpnt.x()) + abs(spnt.y()-cpnt.y())>10))
                {
                    bool ok = false;
                    for (int a=0; a<lmcs.size(); a++)
                    {
                        for (int n=0; n<lmcs.at(a)->lmitems.size(); n++)
                        {
                            if (lmcs.at(a)->lmitems.at(n) == citem)
                            {
                                slmc = lmcs.at(a);
                                slmc->lmitems.removeAt(n);
                                slmcpos = n;
                                ddstarted = true;
                                citem->freeMove = true;
                                citem->move(0,-100);

                                cursitem = new CILItem(/*settings, */parW);
                                cursitem->iticon = &iticon;
                                cursitem->freeMove = true;
                                cursitem->name = citem->name;
                                cursitem->vflags = citem->vflags;
                                cursitem->id = citem->id;
                                cursitem->pData = citem->pData;
                                cursitem->sel = citem->sel;
                                cursitem->resize(citem->size());
                                cursitem->show();

                                ok = true;
                                break;
                            }
                        }
                        if (ok)
                            break;
                    }
                }
            } else
            {
                if (ddprepare && (!ddstarted) && (citem!=NULL))
                {
                    citem->sel = true;
                    emit itemSelected(citem);
                }
                ddprepare = false;
            }
            if (ddstarted)
            {
                if (!mbs.testFlag(Qt::LeftButton))
                {
                    CILContainer *lmc = NULL;
                    QPoint lcpnt = mapFromGlobal(cpnt);
                    for (int a=0; a<lmcs.size(); a++)
                    {
                        if (lmcs.at(a)->geometry().contains(lcpnt.x(), lcpnt.y()))
                        {
                            lmc = lmcs.at(a);
                            break;
                        }
                    }
                    if (lmc==NULL)
                    {
                        citem->setParent(slmc->widg);
                        slmc->lmitems.insert(slmcpos, citem);
                        lmc = slmc;
                        emit itemDropped(citem, globPnt);
                    } else
                    {
                        citem->setParent(lmc->widg);
                        int idx = lmc->lmitems.indexOf(NULL);
                        if (idx>=0)
                            lmc->lmitems.insert(idx, citem);
                        else
                            lmc->lmitems.append(citem);
                        lmc->calcItemPos(citem, idx);
                        citem->move(citem->px, citem->py);
                    }
                    citem->freeMove = false;
                    citem->show();

                    ddstarted = false;
                    ddprepare = false;
                    //citem->sel = false;
                    citem = NULL;
                    delete cursitem;
                    cursitem = NULL;

                    for (int a=0; a<lmcs.size(); a++)
                    {
                        lmcs.at(a)->prepareItemPlace(-1000, -1000);
                        lmcs.at(a)->updateContainer();
                    }

                    emit signalItemDrop();
                } else
                {
                    QPoint cwpnt = parW->mapFromGlobal(cpnt);
                    int csx=cwpnt.x()-citem->width()/2, csy=cwpnt.y()-citem->height()/2;
                    if (csx<0)
                        csx = 0;
                    if (csx>parW->width()-cursitem->width())
                        csx = parW->width()-cursitem->width();
                    if (csy<0)
                        csy = 0;
                    if (csy > parW->height())
                        csy = parW->height();
                    cursitem->move(csx, csy);
                    QPoint locPnt = this->mapFromGlobal(globPnt);
                    for (int a=0; a<lmcs.size(); a++)
                    {
                        lmcs.at(a)->prepareItemPlace(locPnt.x(), locPnt.y());
                        lmcs.at(a)->updateContainer();
                    }
                }
            }
        }
    }
}


void CIconList::setItemPicture(QImage *img)
{
/*    int lmidx=0;
    int w = lmcs.at(lmidx)->itemdw;
    int h = lmcs.at(lmidx)->itemdh;

    iticon = QImage(w,h,QImage::Format_ARGB32);
    iticon.fill(Qt::transparent);

    QPainter painter(&iticon);
    painter.setCompositionMode(QPainter::CompositionMode_SourceOver);

    int pw = img->width()/2-1;
    int ph = img->height()/2-1;
    painter.drawImage(0,0, *img, 0,0, pw,ph);
    painter.drawImage(w-pw,0, *img, pw+1,0, pw,ph);
    painter.drawImage(0,h-ph, *img, 0,ph+1, pw,ph);
    painter.drawImage(w-pw,h-ph, *img, pw+1,ph+1, pw,ph);

    painter.drawImage(QRect(pw,0, w-pw*2, ph), *img, QRect(pw,0, 1,ph));
    painter.drawImage(QRect(pw,h-ph, w-pw*2, ph), *img, QRect(pw,ph+1, 1,ph));*/

    iticon = *img;
}


void CIconList::paintEvent(QPaintEvent *)
{
    QImage *img = &ilcont;

    int pw = img->width();
    int ph = img->height()/2-1;
    //int w = this->width();
    int h = this->height();

    QPainter painter(this);
    painter.drawImage(0,0, *img, 0,0, pw,ph);
    painter.drawImage(QRect(0,ph, pw, h-ph*2-1), *img, QRect(0,ph, pw,1));
    painter.drawImage(0,h-ph-1, *img, 0,ph+2, pw,ph);
}


void CIconList::resizeEvent(QResizeEvent *)
{
    if (lmcs.size()>0)
    {
        lmcs.at(0)->move(4,4);
        lmcs.at(0)->resize(this->width()-8, this->height()-8);
    }
}



CILItem::CILItem(/*CAppSettings *csettings, */QWidget *parent)
: QWidget(parent)
{
    name = "";
    sel = false;
    px = 0, py = 0;
    id = -1;
    pData = NULL;
    freeMove = false;
    //settings = csettings;
    tmrid = startTimer(30);
    iticon = NULL;
}


CILItem::~CILItem()
{
    killTimer(tmrid);
}


void CILItem::timerEvent(QTimerEvent *)
{
    if (!freeMove)
    {
        int cx = this->x(), cy = this->y();

        int nx, ny;
        if (px>cx)
            nx = cx + (px-cx+1)/2;
        else
            nx = cx + (px-cx-1)/2;
        if (py>cy)
            ny = cy + (py-cy+1)/2;
        else
            ny = cy + (py-cy-1)/2;

        if ((cx!=nx) || (cy!=ny))
            move(nx, ny);
    }
}


void CILItem::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    //painter.setRenderHints(QPainter::TextAntialiasing);

    if (iticon!=NULL)
        painter.drawImage(0,0,*iticon);

    if (sel)
        painter.setPen(QPen(QColor(255, 50, 50)));
    else
        painter.setPen(QPen(QColor(0, 0x5d, 0xb3)));

    QFont font1("Arial", 10);
    painter.setFont(font1);
    if (addonStr.size()>0)
    {
        painter.drawText(4, 4, this->width()-8, this->height()-20, Qt::AlignHCenter | Qt::AlignVCenter | Qt::TextWordWrap, name);
        painter.drawText(20, this->height()-20, this->width()-20-8, 14, Qt::AlignRight | Qt::AlignVCenter, addonStr);
    } else
    {
        painter.drawText(4, 4, this->width()-8, this->height()-12, Qt::AlignHCenter | Qt::AlignVCenter | Qt::TextWordWrap, name);
    }
}


CILContainerWidget::CILContainerWidget(QWidget *parent)
: QWidget(parent)
{
    //setMouseTracking(true);
}

CILContainerWidget::~CILContainerWidget()
{
}

CILContainer::CILContainer(QWidget *parent)
: QScrollArea(parent)
{
    itemhs = 64;
    itemws = 80;
    itemdw = 70;
    itemdh = 60;
    sel = false;
    widg = new CILContainerWidget(this);
    setWidget(widg);
    setBackgroundRole(QPalette::Dark);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
}

CILContainer::~CILContainer()
{
}


void CILContainer::resizeEvent(QResizeEvent *)
{
    updateContainer();
}


void CILContainer::updateContainer()
{
    int h;
    if (lmitems.size()>0)
        h = (lmitems.size()+1)*itemhs;
    else
        h = this->height();

    if (h<this->height())
        h = this->height();

    if ((widg->width()!=itemdw+2) || (widg->height()!=h))
        widg->resize(itemdw+8, h);

    for (int a=0; a<lmitems.size(); a++)
    {
        if (lmitems.at(a) != NULL)
        {
            calcItemPos(lmitems.at(a), a);
        }
    }
}


void CILContainer::prepareItemPlace(int x, int y)
{
    lmitems.removeAll(NULL);

    QPoint pnt = widg->mapFrom(this, this->mapFromParent(QPoint(x, y)));

    if ((pnt.x()>0) && (pnt.x()<widg->width())
                        && (pnt.y()>0) && (pnt.y()<widg->height()))
    {
        int ipx = pnt.y()/itemhs;
        lmitems.insert(ipx, NULL);
    }
}

void CILContainer::calcItemPos(CILItem *item, int idx)
{
    item->px = 3;
    item->py = idx*itemhs+4;
}
