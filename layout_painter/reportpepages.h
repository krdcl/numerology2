#ifndef REPORTPEPAGES_H
#define REPORTPEPAGES_H

#include <QtGui>
//#include "datafile.h"
//#include "appsettings.h"
#include "iconlist.h"


class CReportPEPages : public QWidget
{
	Q_OBJECT

public:
    CReportPEPages(/*CDataFile *cdata, CAppSettings *csettings, */QWidget *parent=NULL);
    ~CReportPEPages();

    //CDataFile *data;
   //CAppSettings *settings;
    //CTranslate *q;

    QPushButton *m_pAddBlock;
    QPushButton *m_pDelBlock;
    QPushButton *m_pBlockProp;
    QPushButton *m_pAddPage;
    QPushButton *m_pDelPage;
    QPushButton *m_pPageProp;

    QSplitter *splitter;
    CIconList *pagesList;
    CIconList *blocksList;

    QWidget *iaWidg;
    void updateLists();

    int tmrid;

protected:
    void paintEvent(QPaintEvent *);
    void timerEvent(QTimerEvent *);
    void resizeEvent(QResizeEvent *);
};

#endif // REPORTPEPAGES_H
