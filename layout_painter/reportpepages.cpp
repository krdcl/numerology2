#include "reportpepages.h"
//#include "infodlg.h"


CReportPEPages::CReportPEPages(/*CDataFile *cdata, CAppSettings *csettings, */QWidget *parent)
    : QWidget(parent)
{
    //data = cdata;
   // settings = csettings;
    //q = settings->q;

    //splitter = new QSplitter(Qt::Vertical, this);

    pagesList = new CIconList(/*data, settings, */this);
    pagesList->resize(100, 300);
    blocksList = new CIconList(/*data, settings, */this);
    blocksList->resize(100, 300);


    m_pAddBlock = new QPushButton("+", this);
    m_pAddBlock->resize(20, 20);
    m_pAddBlock->setToolTip("Add Block");

    m_pDelBlock = new QPushButton("x", this);
    m_pDelBlock->resize(20, 20);
    m_pDelBlock->setToolTip("Delete Block");

    m_pBlockProp = new QPushButton(QChar(0x2699), this);
    m_pBlockProp->resize(20, 20);
    m_pBlockProp->setToolTip("Block Properties");

    m_pAddPage = new QPushButton("+", this);
    m_pAddPage->resize(20, 20);
    m_pAddPage->setToolTip("Add Pages");

    m_pDelPage = new QPushButton("x", this);
    m_pDelPage->resize(20, 20);
    m_pDelPage->setToolTip("Delete Pages");

    m_pPageProp = new QPushButton(QChar(0x2699), this);
    m_pPageProp->resize(20, 20);
    m_pPageProp->setToolTip("Pages Properties");

//    DialogsID did = DIDEntry;
//    settings->appstyle->setWidgetStyle(m_pAddBlock, did, WTButton);
//    settings->appstyle->setWidgetStyle(m_pDelBlock, did, WTButton);
//    settings->appstyle->setWidgetStyle(m_pBlockProp, did, WTButton);
//    settings->appstyle->setWidgetStyle(m_pAddPage, did, WTButton);
//    settings->appstyle->setWidgetStyle(m_pDelPage, did, WTButton);
//    settings->appstyle->setWidgetStyle(m_pPageProp, did, WTButton);

    //settings->appstyle->setWidgetStyle(splitter, did, WTMainList);

    iaWidg = parentWidget()->parentWidget()->parentWidget();

#ifndef VER_PREM
    m_pAddPage->hide();
    m_pDelPage->hide();
    m_pPageProp->hide();
#endif

    tmrid = startTimer(30);
}


CReportPEPages::~CReportPEPages()
{
    killTimer(tmrid);
}


void CReportPEPages::paintEvent(QPaintEvent *)
{
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);
}


void CReportPEPages::updateLists()
{

}


void CReportPEPages::timerEvent(QTimerEvent *)
{
    pagesList->processItems(iaWidg);
    blocksList->processItems(iaWidg);
}


void CReportPEPages::resizeEvent(QResizeEvent *)
{
    int h=this->height(), w=this->width();

#ifdef VER_PREM
    int h1 = h/2-35, h2=h/2-35;
#else
    int h1 = h/4-35, h2=3*h/4-35;
#endif
    pagesList->move(0, 0);
    pagesList->resize(w, h1);
    m_pAddPage->move(0, h1+5);
    m_pDelPage->move(25, h1+5);
    m_pPageProp->move(2*25, h1+5);

    blocksList->move(0, h1+35);
    blocksList->resize(w, h2);
    m_pAddBlock->move(0, h1+35+h2+5);
    m_pDelBlock->move(25, h1+35+h2+5);
    m_pBlockProp->move(2*25, h1+35+h2+5);
}
