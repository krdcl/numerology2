#include "reportpe.h"
#ifndef VER_MOBILE
#include "infodlg.h"
#endif
#include <QScrollBar>

#include <math.h>
#include "mathutils.h"
#ifndef VER_MOBILE
#include "textdlg.h"
#endif
#include "barcode.h"
#include "QR_Encode.h"



CReportPE::CReportPE(calcNumContainer *pNumContainer, CAppStyle *style_, QWidget *parent)
    : QAbstractScrollArea(parent)
{
    //data = cdata;
    //settings = csettings;
    //q = settings->q;

#ifdef Q_OS_MAC
    qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/");
#else
    qsResPath = QCoreApplication::applicationDirPath() + QString("/images/");
#endif

    lableFixScroll = new QLabel(this);
    lableFixScroll->setStyleSheet("border: 0px;");
    numContainer = pNumContainer;
    appstyle = style_;
    page = NULL;

    drawMode = new CDrawMode(/*data*/);
    drawMode->isDrawBorder = 1;

    style = new CStyle;
#ifndef VER_MOBILE
    style->m_page.iconMoveS = appstyle->spagerep.iconMoveS;
    style->m_page.iconRotateS = appstyle->spagerep.iconRotateS;
    style->m_page.iconScaleI = appstyle->spagerep.iconScaleI;
    style->m_page.iconScaleResI = appstyle->spagerep.iconScaleResI;
    style->m_page.iconScaleS = appstyle->spagerep.iconScaleS;
    style->m_page.iconDataField = appstyle->spagerep.iconDataField;
    style->m_page.iconDataFieldTitle = appstyle->spagerep.iconDataFieldTitle;
    style->m_page.imageDataField = appstyle->spagerep.imageDataField;
#endif

    viewport()->setAutoFillBackground(false);

    viewport()->setAttribute(Qt::WA_OpaquePaintEvent);
    viewport()->setAttribute(Qt::WA_NoSystemBackground);

#ifndef VER_MOBILE
    setMouseTracking(true);
    setAcceptDrops(true);
#endif

    m_Spacing = 10;
    m_Scale = 0.6;

    m_pSelObject = NULL;
    m_editAction = ACT_NONE;

    newReport();

    animTmr.setSingleShot(true);
    connect(&animTmr, SIGNAL(timeout()), viewport(), SLOT(update()));

}


CReportPE::~CReportPE()
{
    clear();
    delete style;
}

void CReportPE::updateFixScroll()
{
    if (verticalScrollBar()->isVisible() && horizontalScrollBar()->isVisible())
    {
#ifdef Q_OS_MAC
        lableFixScroll->setAlignment(Qt::AlignBottom | Qt::AlignRight);
        lableFixScroll->resize(16, 16);
#else
        lableFixScroll->resize(18, 18);
#endif
        QImage hline(qsResPath + "scrbox.png");
        hline = appstyle->updateImage(hline);
        lableFixScroll->setPixmap(QPixmap::fromImage(hline));
#ifdef Q_OS_MAC
        lableFixScroll->move(width() - 16, height() - 16);
#else
        lableFixScroll->move(width() - 18, height() - 18);
#endif
    }
    else
    {
        lableFixScroll->resize(35, 1);
        QImage hline(qsResPath + "hline.png");
        hline = appstyle->updateImage(hline);
        lableFixScroll->setPixmap(QPixmap::fromImage(hline));
        lableFixScroll->move(width() - 35, height() - 1);
    }
}

CREPageItem::CREPageItem(calcNumContainer *pNumContainer)
{
    //data = dat;
    numContainer = pNumContainer;
    m_Type = OT_NONE;
    m_pContainer = NULL;
    m_pOwner = NULL;
    barCodes = 0;

    widthIcon = /*dat->doc.imageOfPreview * */80;
    //q = dat->settings->q;

    //m_effects.resize(SEFF_NUM);
    m_ShapeFillColor = QColor(0,0,0,255);
    m_imgID = -1;
    m_imgSize = QSizeF(0,0);
    m_Font = QFont("Arial", 10, QFont::Normal, false);
    m_Font.setPixelSize(11);
    m_Align = Qt::AlignLeft;
    m_Fit = 1;
    iconNum = 0;

    m_LineType = Qt::SolidLine;
    m_LineWidth = 1;
    m_LineColor = QColor::fromRgb(0,0,0,255);

    m_Frame = 0;
    m_FrameColor = QColor::fromRgb(0,0,0,255);

	m_BackgroundColor = QColor::fromRgb(255,255,255,222);

    m_fileLink = "";

    m_bSelected = false;
    m_bCanResize = true;
    m_bSolidContainer = false;

    m_Matrix.reset();
    m_Angle = 0;
    //m_ScaleX = 1;
    //m_ScaleY = 1;
    m_BoundSize = QSizeF(0,0);

	name = "";
	id = "";
	sel = false;
	isLabel = false;
    isTextBox = false;
    m_Idx = 0;
    m_pImg = NULL;
    m_iopt = 0;
    m_itemid = -1;
    icon = NULL;
}


CREPageItem::~CREPageItem()
{
    if (m_pContainer!=NULL)
        delete m_pContainer;
    m_pContainer = NULL;
}


void CREPageItem::resTransformation()
{
    m_Matrix.reset();
    m_Angle = 0;
    if (m_pContainer != NULL)
        m_pContainer->resTransformation();
}

void CREPageItem::rotate(double angle, bool calcAngle)
{
    if ( m_Type == OT_SLOT )
    {
        QTransform mat;
        double cx = m_Shape.boundingRect().center().x();
        double cy = m_Shape.boundingRect().center().y();
        mat.translate(cx, cy);
        mat.rotate(angle);
        mat.translate(-cx, -cy);
        m_Shape = mat.map(m_Shape);
    } else
    {
        m_Matrix.rotate(angle);
    }
    if ( calcAngle )
    {
        m_Angle += angle;
        if( m_Angle < 0 ) m_Angle += 360;
        if( m_Angle > 360 ) m_Angle -= 360;

        if ( m_pContainer != NULL )
        {
            m_pContainer->m_Angle = m_Angle;
        }
    }
}


void CREPageItem::move(double dx, double dy)
{
    if ( m_Type == OT_SLOT )
    {
        m_Matrix.translate(dx/abs(m_Matrix.m11()),dy/abs(m_Matrix.m22()));
    } else
    {
        m_Matrix.rotate(-m_Angle);
        m_Matrix.translate(dx/abs(m_Matrix.m11()),dy/abs(m_Matrix.m22()));
        m_Matrix.rotate(m_Angle);
    }
}

void CREPageItem::translate(double dx, double dy)
{
    m_Matrix.translate(dx,dy);
}

void CREPageItem::scale(double sx, double sy)
{
    m_Matrix.scale(sx,sy);
}


QRectF CREPageItem::getBoundRect()
{
    QRectF bound(0, 0, m_BoundSize.width(), m_BoundSize.height());
    bound = m_Matrix.mapRect(bound);
    return bound;
}

void CREPageItem::setBoundSize(QSizeF sz)
{
    m_BoundSize = sz;
    if (m_BoundSize.width() == 0)
        m_BoundSize.setWidth(m_BoundSize.height());
    if (m_BoundSize.height() == 0)
        m_BoundSize.setHeight(m_BoundSize.width());
}

QSizeF CREPageItem::getBoundSize()
{
    if (m_BoundSize.width() == 0)
        m_BoundSize.setWidth(m_BoundSize.height());
    if (m_BoundSize.height() == 0)
        m_BoundSize.setHeight(m_BoundSize.width());
    return m_BoundSize;
}


QRectF CREPageItem::getShapeBoundRect()
{
    return m_Matrix.map(m_Shape).boundingRect();
}


void CREPageItem::setShape(QPainterPath shape)
{
    m_Shape = shape;
}


void CREPageItem::drawTracker(QPainter *pPntr, CDrawMode *drawMode, CStyle *style, bool drawBorderOnly)
{
    if ((drawBorderOnly == true) && (drawMode->isDrawBorder == false))
        return;
    pPntr->save();

    QTransform srcMatrix = pPntr->transform();
    QTransform resMatrix = m_Matrix*srcMatrix;
    pPntr->setTransform(resMatrix);
    pPntr->setRenderHint(QPainter::HighQualityAntialiasing, true);
    pPntr->setPen(QPen(QBrush(style->m_page.trackerColor),1));
    pPntr->setBrush(Qt::NoBrush);
    pPntr->setRenderHint(QPainter::Antialiasing, false);
    pPntr->setRenderHint(QPainter::HighQualityAntialiasing, false);
    pPntr->resetTransform();

    QRectF rect = srcMatrix.mapRect(getShapeBoundRect());
    const int ext = 2;
    rect.adjust(-ext, -ext, ext-1, ext-1);

    if (drawBorderOnly == true)
    {
        pPntr->drawRect(rect);
        pPntr->restore();
        return;
    }

    const int tsz = 8;

    if ( (rect.width() > style->m_page.iconMoveS.width()*3) && (rect.height() > style->m_page.iconMoveS.height()*2))
        pPntr->drawImage(rect.left()+rect.width()/2-style->m_page.iconMoveS.width()/2, rect.top()-style->m_page.iconMoveS.height()/2-1, style->m_page.iconMoveS);
    {
        if ( (rect.width() > style->m_page.iconScaleS.width()*3) && (rect.height() > style->m_page.iconScaleS.height()*2))
            pPntr->drawImage(rect.right()-style->m_page.iconScaleS.width()+2, rect.bottom()-style->m_page.iconScaleS.height()+1, style->m_page.iconScaleS);
    }
    pPntr->drawImage(rect.right()+style->m_page.iconRotateS.width()/4, rect.bottom()-style->m_page.iconRotateS.height()+1, style->m_page.iconRotateS);

    if ( m_pContainer!=NULL )
    {
        if ( (rect.width() > style->m_page.iconScaleResI.width()*3) && (rect.height() > style->m_page.iconScaleResI.height()*2)
             && (!m_bSolidContainer))
        {
            pPntr->drawImage(QPointF(rect.center().x()-rect.width()/4-style->m_page.iconScaleResI.width()/2, rect.center().y()-style->m_page.iconScaleResI.height()/2), style->m_page.iconScaleResI);
            pPntr->drawImage(QPointF(rect.center().x()+rect.width()/4-style->m_page.iconScaleResI.width()/2, rect.center().y()-style->m_page.iconScaleI.height()/2), style->m_page.iconScaleI);
        }
    }


    pPntr->drawRect(rect);
    pPntr->drawRect(QRectF(rect.left(), rect.bottom()-tsz, tsz, tsz));
    pPntr->drawRect(QRectF(rect.left(), rect.top(), tsz, tsz));
    pPntr->drawRect(QRectF(rect.right()-tsz, rect.top(), tsz, tsz));
    if (!((rect.width() > style->m_page.iconScaleS.width()*3) && (rect.height() > style->m_page.iconScaleS.height()*2)))
        pPntr->drawRect(QRectF(rect.right()-tsz, rect.bottom()-tsz, tsz, tsz));

    const int tsz2x = 6;
    const int tsz2y = 8;
    if ( (rect.height() > tsz*2+tsz2y+3))
    {
        pPntr->drawEllipse(QRectF(rect.left()-tsz2x/2, rect.top()+rect.height()/2-tsz2y/2, tsz2x, tsz2y));
        pPntr->drawEllipse(QRectF(rect.right()-tsz2x/2, rect.top()+rect.height()/2-tsz2y/2, tsz2x, tsz2y));
    }
    if ( (rect.width() > tsz*2+tsz2y+3))
        pPntr->drawEllipse(QRectF(rect.left()+rect.width()/2-tsz2y/2, rect.bottom()-tsz2x/2, tsz2y, tsz2x));

    pPntr->drawLine(QPointF(rect.topLeft().x(),rect.topLeft().y()-2),
                    QPointF(rect.topRight().x(),rect.topRight().y()-2));

    pPntr->restore();
}


QImage CREPageItem::getQrCode(QString valStr, QColor color)
{
    int m_bDataEncoded;
    int nLevel = 0;
    int nVersion = 0;
    int bAutoExtent = 1;
    int nMaskingNo   =  -1;

    CQR_Encode* pQR_Encode = new CQR_Encode;
    m_bDataEncoded = pQR_Encode->EncodeData(nLevel, nVersion, bAutoExtent, nMaskingNo, valStr.toLocal8Bit().data());

    if (m_bDataEncoded != 1)
        return QImage();

    QImage imageCode(pQR_Encode->m_nSymbleSize, pQR_Encode->m_nSymbleSize, QImage::Format_RGB32);
    imageCode.fill(Qt::white);

    for (int x = 0; x < pQR_Encode->m_nSymbleSize; x++)
    {
        for (int y = 0; y < pQR_Encode->m_nSymbleSize; y++)
        {
            if (pQR_Encode->m_byModuleData[x][y] == (unsigned char)1)
                imageCode.setPixel(x, y, color.rgb());
        }
    }

    delete pQR_Encode;
    return imageCode.scaled(imageCode.width() * 10, imageCode.height() * 10);
}

QImage CREPageItem::drawBarcode(QString valStr, int type, QColor color)
{
    barCodeColor = color;
    char *returnData = 0;
    long sizeOut = 0;
    QImage img;
    long ret = 0;
    switch (type)
    {
        case 0: //QR Code
            return getQrCode(valStr, color);
        case 1: //Codabar
            ret = Codabar(valStr.toLocal8Bit().data(), &returnData, sizeOut);
            if (ret != 0)
                return QImage();
            img =  barCodeToQimageCodabar(returnData, sizeOut);
            delete [] returnData;
            return img;
        case 2: //Code 128
            ret = Code128b(valStr.toLocal8Bit().data(), &returnData, sizeOut);
            if (ret != 0)
                return QImage();
            img =  barCodeToQimageCode128(returnData, sizeOut);
            delete [] returnData;
            return img;
        case 3: //Code 39
            ret = Code39(valStr.toLocal8Bit().data(), &returnData, sizeOut);
            if (ret != 0)
                return QImage();
            img =  barCodeToQimageCode39(returnData, sizeOut);
            delete [] returnData;
            return img;
        case 4: //Code 93
            ret = Code93(valStr.toLocal8Bit().data(), &returnData, sizeOut);
            if (ret != 0)
                return QImage();
            img =  barCodeToQimageCode93(returnData, sizeOut);
            delete [] returnData;
            return img;
        case 5: //EAN 13
            ret = EAN13(valStr.toLocal8Bit().data(), &returnData, sizeOut);
            if (ret != 0)
                return QImage();
            img =  barEnaCodeToQimageEAN(returnData, sizeOut);
            delete [] returnData;
            return img;
        case 6: //EAN 8
            ret = EAN8(valStr.toLocal8Bit().data(), &returnData, sizeOut);
            if (ret != 0)
                return QImage();
            img =  barEnaCodeToQimageEAN(returnData, sizeOut);
            delete [] returnData;
            return img;
        case 7: //Interleaved 2 of 5
            ret = Interleaved2of5(valStr.toLocal8Bit().data(), &returnData, sizeOut);
            if (ret != 0)
                return QImage();
            img =  barCodeToQimageInterleaved2of5(returnData, sizeOut, valStr.toLocal8Bit().data());
            delete [] returnData;
            return img;
        case 8: //MSI
            ret = MSI(valStr.toLocal8Bit().data(), &returnData, sizeOut);
            if (ret != 0)
                return QImage();
            img =  barEnaCodeToQimageUNA(returnData, sizeOut);
            delete [] returnData;
            return img;
        case 9: //Postnet
            ret = Postnet(valStr.toLocal8Bit().data(), &returnData, sizeOut);
            if (ret != 0)
                return QImage();
            img =  barEnaCodeToQimagePostNet(returnData, sizeOut);
            delete [] returnData;
            return img;
        case 10: //UPC
            ret = UPCa(valStr.toLocal8Bit().data(), &returnData, sizeOut);
            if (ret != 0)
                return QImage();
            img =  barEnaCodeToQimageEAN(returnData, sizeOut);
            delete [] returnData;
            return img;
        default:
            return QImage();
    }
    return QImage();
}

void CREPageItem::draw(QPainter *pPntr, CDrawMode *drawMode, CStyle *style)
{

    pPntr->save();
    QTransform srcMatrix = pPntr->transform();
    pPntr->setTransform(m_Matrix*srcMatrix);

    m_Font.setStyleStrategy(QFont::PreferAntialias);


    m_Text = m_Text;

    if (m_Type == OT_SLOT)
    {
        if (m_pContainer->objectType() != OT_VECTOR_CLIP)
            pPntr->setClipPath(m_Shape, Qt::IntersectClip);

        m_pContainer->draw(pPntr, drawMode, style);

        if (m_pContainer->m_Frame!=0 || (drawMode->finalRender == false && drawMode->isDrawBorder == true))
        {
            QPen fpen;
            fpen.setWidth(1);
            fpen.setStyle(Qt::SolidLine);
            if (m_pContainer->m_Frame!=0)

                fpen.setColor(m_pContainer->m_FrameColor);
            else
                fpen.setColor(style->m_page.trackerColor);

            pPntr->setClipping(false);
            pPntr->strokePath(m_Shape, fpen);
        }
    } else if (m_Type == OT_IMAGE)
    {
        if (!m_img.isNull())
        {
            pPntr->drawImage(QRectF(0, 0, m_BoundSize.width(), m_BoundSize.height()), m_img);
        }
    } else if (m_Type == OT_TEXT)
    {
        pPntr->setRenderHint(QPainter::TextAntialiasing, true);
        pPntr->setFont(m_Font);

        QPen pen;
        pen.setStyle(Qt::SolidLine);
        pen.setWidth(1);
        pen.setColor(m_ShapeFillColor);
        pPntr->setPen(pen);
        QRectF bndRect(0,0,m_BoundSize.width(), m_BoundSize.height());
        QPointF rcnt = bndRect.center();

        QRectF rres;
        pPntr->drawText(QRectF(0,0,0,0), m_Align, m_Text, &rres);
        QRectF rect(rcnt.x()-rres.width()/2-1, rcnt.y()-rres.height()/2-1, rres.width()+2, rres.height()+2);
        pPntr->drawText(rect, m_Align, m_Text);

    } else if (m_Type == OT_DATA_FIELD)
    {
        pPntr->setRenderHint(QPainter::TextAntialiasing, true);
        pPntr->setFont(m_Font);
        QPen pen;
        pen.setStyle(Qt::SolidLine);
        pen.setWidth(1);
        pen.setColor(m_ShapeFillColor);
        pPntr->setPen(pen);
        QRectF bndRect(0,0,m_BoundSize.width(), m_BoundSize.height());

        if (drawMode->thumb | drawMode->finalRender)
        {
            QRectF rect(1, 0, bndRect.width()-1, bndRect.height());
            pPntr->drawText(rect, m_Align | Qt::TextWordWrap, m_Text);
        } else
        {
            pPntr->drawImage(0,0,style->m_page.iconDataField);
            QRectF rect(16+2, 0, bndRect.width()-16-2, bndRect.height());
            pPntr->drawText(rect, m_Align | Qt::TextWordWrap, m_Text);
        }

    } else if (m_Type == OT_DATA_FIELD_NAME)
    {
        pPntr->setRenderHint(QPainter::TextAntialiasing, true);
        pPntr->setFont(m_Font);
        QPen pen;
        pen.setStyle(Qt::SolidLine);
        pen.setWidth(1);
        pen.setColor(m_ShapeFillColor);
        pPntr->setPen(pen);
        QRectF bndRect(0,0,m_BoundSize.width(), m_BoundSize.height());

        CNumerology *numer = numContainer->m_pNumer;

        if (id == "#full_name")
        {
            QRectF rect(1, 0, bndRect.width()-2, bndRect.height());
            pPntr->drawText(rect, m_Align | Qt::TextWordWrap, numer->fullName);
        }
        else
        if (id == "#short_name")
        {
            QRectF rect(1, 0, bndRect.width()-2, bndRect.height());
            pPntr->drawText(rect, m_Align | Qt::TextWordWrap, numer->shortName);
        }
        else
        if (id == "#birthdate")
        {
            const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
            QRectF rect(1, 0, bndRect.width()-2, bndRect.height());
            pPntr->drawText(rect, m_Align | Qt::TextWordWrap, QString(mname[numer->m_bdate.month()-1]) + " " + QString::number(numer->m_bdate.day()) + " " + QString::number(numer->m_bdate.year()));
        }
        else
        if (id == "#print_date")
        {
            const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
            QRectF rect(1, 0, bndRect.width()-2, bndRect.height());
            QDate dat = QDate::currentDate();
            pPntr->drawText(rect, m_Align | Qt::TextWordWrap, QString(mname[dat.month()-1]) + " " + QString::number(dat.day()) + " " + QString::number(dat.year()));
        }
        else
        if (id == "#numerologist")
        {
            QRectF rect(1, 0, bndRect.width()-2, bndRect.height());
            pPntr->drawText(rect, m_Align | Qt::TextWordWrap, numer->shortName);
        }
        else
        if (isLabel)
        {
            QRectF rect(2, 2, bndRect.width()-3, bndRect.height()-3);
            pPntr->drawText(rect, m_Align | Qt::TextWordWrap, m_Text);
        }
        if (isTextBox)
        {
            QRectF rect(2, 2, bndRect.width()-3, bndRect.height()-3);
            pPntr->drawText(rect, m_Align | Qt::TextWordWrap, m_Text);
        }

//        if (isTextBox)
//        {
//            QRectF rect(2, 2, bndRect.width()-3, bndRect.height()-3);
//            pPntr->drawText(rect, m_Align | Qt::TextWordWrap, m_Text);
//        } else
//        {
//            if (drawMode->thumb || drawMode->finalRender)
//            {
//                QRectF rect(1, 0, bndRect.width()-2, bndRect.height());
//                pPntr->drawText(rect, m_Align | Qt::TextWordWrap, m_Text);
//            } else
//            {
//                pPntr->drawImage(0,0,style->m_page.iconDataFieldTitle);
//                QRectF rect(16+2, 0, bndRect.width()-16-2, bndRect.height());
//                pPntr->drawText(rect, m_Align | Qt::TextWordWrap, m_Text);
//            }
//        }
    } else if (m_Type == OT_VECTOR_CLIP)
    {
        pPntr->fillPath(m_Shape, QBrush(m_ShapeFillColor));

        if ( m_LineWidth > 0 )
        {
            QPen pen;
            pen.setStyle((Qt::PenStyle)m_LineType);
            pen.setWidth(m_LineWidth);
            pen.setColor(m_LineColor);
            pPntr->setPen(pen);
            pPntr->drawPath(m_Shape);
        }
    } else if (m_Type == OT_DATA_IMAGE)
    {
        pPntr->setFont(QFont("Arial", 36));
        QPen pen;
        pen.setStyle(Qt::SolidLine);
        pen.setWidth(1);
        pen.setColor(QColor(50, 50, 50, 255));
        pPntr->setPen(pen);
        QRectF bndRect(0,0,m_BoundSize.width(), m_BoundSize.height());

        if (m_Fit==0)
        {
            QImage *img = &style->m_page.imageDataField;

            if (drawMode->finalRender)
            {
//                if (m_imgID>=0)
//                   img = drawMode->data->m_pStorage->getImage(m_imgID);
//                else
//                   img = NULL;
            }

            if (img != NULL)
                pPntr->drawImage(0,0,*img);
        } else if (m_Fit==1)
        {
            QImage *img = &style->m_page.imageDataField;

            if (drawMode->finalRender)
            {
                if (m_imgID>=0)
                {
                   //img = drawMode->data->m_pStorage->getImage(m_imgID);
                }
                else
                   img = NULL;
            }

            if (img != NULL)
            {
                QSizeF isz = img->size();
                int ox, oy;
                double scale = calcFitScale(isz.width(), isz.height(), bndRect.width(), bndRect.height(), ox, oy);
                int szx = isz.width()*scale;
                int szy = isz.height()*scale;
                if (szx<1)
                    szx=1;
                if (szy<1)
                    szy=1;
                //QSizeF objSz(szx, szy);

                if (m_Align == Qt::AlignLeft)
                    pPntr->drawImage(QRect(0,0, szx, szy), *img, QRect(0,0,img->width(), img->height()));
                else if (m_Align == Qt::AlignHCenter)
                    pPntr->drawImage(QRect(ox,0, szx, szy), *img, QRect(0,0,img->width(), img->height()));
                else if (m_Align == Qt::AlignRight)
                    pPntr->drawImage(QRect(m_BoundSize.width()-szx,0, szx, szy), *img, QRect(0,0,img->width(), img->height()));
            }
        }
        if (!drawMode->finalRender)
            pPntr->drawText(bndRect, QString::number(m_Idx+1));
    } else if (m_Type == OT_ICON)
    {

//        QImage tmImg = style->m_page.imageDataField;
//        pPntr->drawImage(0, 0, tmImg);

        pPntr->setFont(QFont("Arial", 36));
        QPen pen;
        pen.setStyle(Qt::SolidLine);
        pen.setWidth(1);
        pen.setColor(QColor(50, 50, 50, 255));
        pPntr->setPen(pen);
        QRectF bndRect(0,0,m_BoundSize.width(), m_BoundSize.height());

        QImage tmImg;


        if (drawMode->finalRender == false)
        {
            tmImg = style->m_page.imageDataField;
        }
        else
        {
            if (!(m_itemid == -1 || icon->isEmpty()))
            {
                int imgId = 0;

                if (iconNum > icon->size())
                    imgId = icon->size() - 1;
                else
                    imgId = iconNum - 1;

                if (iconNum == 0)
                {
                    int width = 0;
                    int height = 0;

                    for (int i = 0; i < icon->size(); i++)
                    {
                        width += icon->at(i).width();
                        if (height < icon->at(i).height())
                            height = icon->at(i).height();
                    }

                    tmImg = QImage(width + 2 + icon->size(), height + 2, QImage::Format_ARGB32);
                    tmImg.fill(0);
                    QPainter painter(&tmImg);
                    int moveX = 1;
                    for (int i = 0; i < icon->size(); i++)
                    {
                        int moveY = ((double)height / 2.0) - ((double)icon->at(i).height() / 2.0) + 1;
                        painter.drawImage(QRect(moveX, moveY, icon->at(i).width(), icon->at(i).height()), icon->at(i));
                        moveX += icon->at(i).width() + 1;
                    }
                }
                else
                {
                    tmImg = icon->at(imgId);
                }
            }
        }

        if (m_Fit==0)
        {
            if (!tmImg.isNull())
                pPntr->drawImage(0, 0, tmImg);
        }
        else
        if (m_Fit==1)
        {
            if (drawMode->finalRender == false)
            {
                tmImg = style->m_page.imageDataField;
            }
            if (!tmImg.isNull())
            {
               // int szx = 0;

                QSizeF isz = tmImg.size();
              //  szx = isz.width();


                int ox, oy;
                double scale = calcFitScale(isz.width(), isz.height(), bndRect.width(), bndRect.height(), ox, oy);

                int szx = isz.width()*scale;
                int szy = isz.height()*scale;
                if (szx<1)
                    szx=1;
                if (szy<1)
                    szy=1;

                if (m_Align == Qt::AlignLeft)
                    pPntr->drawImage(QRect(0,0, szx, szy), tmImg);
                else if (m_Align == Qt::AlignHCenter)
                    pPntr->drawImage(QRect(ox,0, szx, szy), tmImg);
                else if (m_Align == Qt::AlignRight)
                    pPntr->drawImage(QRect(m_BoundSize.width()-szx,0, szx, szy), tmImg);
            }
        }

    } else if (m_Type == OT_BLOCK)
    {
        if (drawMode->finalRender)
        {
            if (m_pData !=NULL)
            {
                if (m_Idx>=0)
                {
                    CREPage *ipage = (CREPage*)m_pData;
                    ipage->m_itemid = m_itemid;
                    ipage->m_pnum = m_pnum;
                    ipage->prefillData();
                    ipage->draw(pPntr, drawMode, style, false);
                }
            }
        } else
        {
            QPen pen;
            pen.setStyle(Qt::SolidLine);
            pen.setWidth(0);
            pen.setColor(QColor(50, 50, 50, 255));
            pPntr->setPen(pen);
            QRectF rectf(0, 0, m_BoundSize.width(), m_BoundSize.height());
            if (m_pImg != NULL)
            {
                pPntr->setOpacity(0.8);
                pPntr->drawImage(rectf, *m_pImg);
                pPntr->setOpacity(1.0);
            }
            pPntr->setFont(QFont("Arial", 24));
            pPntr->drawText(rectf, "#" + QString::number(m_iopt+1));

            if (m_Frame==0)
                pPntr->drawRect(rectf);
        }
    }
    else if (m_Type == OT_BAR_CODE)
    {
        pPntr->setRenderHint(QPainter::SmoothPixmapTransform, true);
        QRectF bndRect(0,0,m_BoundSize.width(), m_BoundSize.height());
        QImage tmImg = drawBarcode(m_Text, barCodes, m_ShapeFillColor);
        QSizeF isz = tmImg.size();

        int ox, oy;
        double scale = calcFitScale(isz.width(), isz.height(), bndRect.width(), bndRect.height(), ox, oy);
        int szx = isz.width()*scale;
        int szy = isz.height()*scale;

        if (szx<1)
            szx=1;
        if (szy<1)
            szy=1;

        if (m_Align == Qt::AlignLeft)
            pPntr->drawImage(QRect(0,0, szx, szy), tmImg);
        else if (m_Align == Qt::AlignHCenter)
            pPntr->drawImage(QRect(ox,0, szx, szy), tmImg);
        else if (m_Align == Qt::AlignRight)
            pPntr->drawImage(QRect(m_BoundSize.width()-szx,0, szx, szy), tmImg);
    }
    else if (m_Type == OT_CALC_BLOCK_T1 || m_Type == OT_CALC_BLOCK_T2 || m_Type == OT_CALC_BLOCK_T3
             || m_Type == OT_CALC_BLOCK_T4 || m_Type == OT_CALC_BLOCK_T5)
    {
        palitra pens;

        pens.pen1 = QColor(180, 180, 180, 255);
        pens.pen2 = QColor(0, 0, 0, 255);
        pens.pen3 = QColor(0, 0, 0, 255);
        pens.pen4 = m_BackgroundColor;
        pens.pen5 = m_BackgroundColor;
        pens.pen6 = QColor(0,0,0,255);
        if (id == "Name")
        {
            CNumerology *numer = numContainer->m_pNumer;
            QStringList m_pInfoNameparListVal;
            QStringList m_pInfoNameparList;
            m_pInfoNameparListVal << numer->numDet.fnameR1;
            m_pInfoNameparListVal << numer->numDet.fnameR;
            m_pInfoNameparListVal << numer->numDet.fnameR2;
            m_pInfoNameparList << numer->numDet.fnameN1;
            m_pInfoNameparList << numer->fullName;
            m_pInfoNameparList << numer->numDet.fnameN2;

            m_pInfoNameparListVal << numer->numDet.snameR1;
            m_pInfoNameparListVal << numer->numDet.snameR;
            m_pInfoNameparListVal << numer->numDet.snameR2;
            m_pInfoNameparList << numer->numDet.snameN1;
            m_pInfoNameparList << numer->shortName;
            m_pInfoNameparList << numer->numDet.snameN2;

            paintNameInfo(pPntr, m_pInfoNameparList, m_pInfoNameparListVal, pens);
        }
        else
        if (id == "Psychomatrix")
        {
            QStringList m_pInfoMTparList;
            m_pInfoMTparList << "" << "" << "" << "";

            CNumerologyPMatrix *matr = numContainer->m_pNumerPMatr;

            QStringList m_pInfoMTparListVal;
            m_pInfoMTparListVal << "";
            m_pInfoMTparListVal << "Character " + getNumRun(matr->m_ResMatr[1],"1");
            m_pInfoMTparListVal << "Health " + getNumRun(matr->m_ResMatr[4],"4");
            m_pInfoMTparListVal << "Luck " + getNumRun(matr->m_ResMatr[7],"7");
            m_pInfoMTparListVal << "Purposeful - " + QString::number(matr->m_SumV[0]);

            m_pInfoMTparListVal << "";
            m_pInfoMTparListVal << "Energy " + getNumRun(matr->m_ResMatr[2],"2");
            m_pInfoMTparListVal << "Intuition " + getNumRun(matr->m_ResMatr[5],"5");
            m_pInfoMTparListVal << "Duty " + getNumRun(matr->m_ResMatr[8],"8");
            m_pInfoMTparListVal << "Family formation - " + QString::number(matr->m_SumV[1]);

            m_pInfoMTparListVal << "";
            m_pInfoMTparListVal << "Interest " + getNumRun(matr->m_ResMatr[3],"3");
            m_pInfoMTparListVal << "Labour " + getNumRun(matr->m_ResMatr[6],"6");
            m_pInfoMTparListVal << "Memory " + getNumRun(matr->m_ResMatr[9],"9");
            m_pInfoMTparListVal << "Stability - " + QString::number(matr->m_SumV[2]);

            m_pInfoMTparListVal << "Sexual life - " + QString::number(matr->m_SumHV[1]);
            m_pInfoMTparListVal << "Self appraisal - " + QString::number(matr->m_SumH[0]);
            m_pInfoMTparListVal << "Finance - " + QString::number(matr->m_SumH[1]);
            m_pInfoMTparListVal << "Talent - " + QString::number(matr->m_SumH[2]);
            m_pInfoMTparListVal << "Spirituality - " + QString::number(matr->m_SumHV[0]);

            paintMatrInfo(pPntr, m_pInfoMTparList, m_pInfoMTparListVal, pens);
        }
        else
        if (id == "Planes of Expression")
        {
            CNumerology *numer = numContainer->m_pNumer;

            QStringList m_pInfoPEparListVal;
            for (int a=0; a<20; a++)
                m_pInfoPEparListVal << numer->numDet.PERep[a];

            QStringList m_pInfoPEparList;
            m_pInfoPEparList.append("");
            m_pInfoPEparList.append("Creative");
            m_pInfoPEparList.append("Dual");
            m_pInfoPEparList.append("Grounded");

            paintExprInfo(pPntr, m_pInfoPEparList, m_pInfoPEparListVal, pens);
        }
        else
        if (id == "Personal numbers calendar")
        {
            CNumerology *numer = numContainer->m_pNumer;

            QStringList m_pInfoCalparList;
            m_pInfoCalparList.append("Years ");
            m_pInfoCalparList.append("Months");
            m_pInfoCalparList.append("Days");

            paintCalendarInfo(pPntr, numer->m_cdate, numer->m_PersYear, m_pInfoCalparList, pens);
        }
        else
        if (id == "Main Features")
        {
            CNumerology *numer = numContainer->m_pNumer;

            QStringList m_pInfoMFparListVal;
            m_pInfoMFparListVal << numer->numDet.lifePath;
            m_pInfoMFparListVal << numer->numDet.birthDay;
            m_pInfoMFparListVal << numer->numDet.expression + " (" + numer->numDet.mexpression +")";
            m_pInfoMFparListVal << numer->numDet.heartDesire + " (" + numer->numDet.mheartDesire +")";
            m_pInfoMFparListVal << numer->numDet.personality + " (" + numer->numDet.mpersonality +")";
            m_pInfoMFparListVal << numer->numDet.karmicDebts;

            QStringList m_pInfoMFparList;
            m_pInfoMFparList.append("Life Path");
            m_pInfoMFparList.append("Birthday");
            m_pInfoMFparList.append("Expression (M)");
            m_pInfoMFparList.append("Heart Desire (M)");
            m_pInfoMFparList.append("Personality (M)");
            m_pInfoMFparList.append("Karmic Debts");

            paintInfo(pPntr, m_pInfoMFparList, m_pInfoMFparListVal, pens);
        }
        else
        if (id == "Additional Features")
        {
            CNumerology *numer = numContainer->m_pNumer;

            QStringList m_pInfoAFparListVal;
            m_pInfoAFparListVal << numer->numDet.maturity;
            m_pInfoAFparListVal << numer->numDet.rationalThought;
            QString skl = numer->numDet.karmicLessons;
            skl.remove(' ');
            m_pInfoAFparListVal << skl;
            m_pInfoAFparListVal << numer->numDet.balance;
            m_pInfoAFparListVal << numer->numDet.hiddenPassion;
            m_pInfoAFparListVal << numer->numDet.subconsciousConfidence;

            QStringList m_pInfoAFparList;
            m_pInfoAFparList.append("Maturity");
            m_pInfoAFparList.append("Rational Thought");
            m_pInfoAFparList.append("Karmic Lessons");
            m_pInfoAFparList.append("Balance");
            m_pInfoAFparList.append("Hidden Passion");
            m_pInfoAFparList.append("Subcons.Confid.");

            paintInfo(pPntr, m_pInfoAFparList, m_pInfoAFparListVal, pens);
        }
        else
        if (id == "Life Cycles")
        {
            CNumerology *numer = numContainer->m_pNumer;

            QStringList m_pInfoLCparListVal;
            m_pInfoLCparListVal << numer->numDet.ageSpan;
            m_pInfoLCparListVal << numer->numDet.challenges;
            m_pInfoLCparListVal << numer->numDet.pinnacles;
            m_pInfoLCparListVal << numer->numDet.persYear;
            m_pInfoLCparListVal << numer->numDet.lpCycles;
            m_pInfoLCparListVal << numer->numDet.lpAgeSpan;

            QStringList m_pInfoLCparList;
            m_pInfoLCparList.append("Age span");
            m_pInfoLCparList.append("Challenges");
            m_pInfoLCparList.append("Pinnacles");
            m_pInfoLCparList.append("Personal Year");
            m_pInfoLCparList.append("LP Periods");
            m_pInfoLCparList.append("LP Age Span");

            paintInfo(pPntr, m_pInfoLCparList, m_pInfoLCparListVal, pens);
        }
        else
        if (id == "Bridges")
        {
            CNumerology *numer = numContainer->m_pNumer;

            QStringList m_pInfoBRparListVal;
            m_pInfoBRparListVal << numer->numDet.brLPE;
            m_pInfoBRparListVal << numer->numDet.brHDP;

            QStringList m_pInfoBRparList;
            m_pInfoBRparList.append("Life path - Expr.");
            m_pInfoBRparList.append("Heart D. - Pers.");

            paintInfo(pPntr, m_pInfoBRparList, m_pInfoBRparListVal, pens);
        }
    }
    pPntr->restore();
}

QString CREPageItem::getNumRun(int val, QString num)
{
    QString res = "";

    if (val==0)
        res = "-";
    else
    {
        for (int a=0; a<val; a++)
            res += num;
    }

    return res;
}

void CREPageItem::genTrackers(STracker trackers[TrackersNum], QRectF boundRect, QTransform mat, CStyle *style)
{
    double ss=8;
    double ssx=ss/mat.m11(), ssy=ss/mat.m22();
    double rpos=3/mat.m11(), rsz=15;
    double rszx=rsz/mat.m11(), rszy=rsz/mat.m22();

    const int ext = 2;
    boundRect.adjust(-ext/mat.m11(), -ext/mat.m22(), (ext-1)/mat.m11(), (ext-1)/mat.m22());

    trackers[TRACKER_SIZE_LT].rect = QRectF(boundRect.left()-ssx, boundRect.top()-ssy, ssx*2, ssy*2);
    trackers[TRACKER_SIZE_RT].rect = QRectF(boundRect.right()-ssx, boundRect.top()-ssy, ssx*2, ssy*2);
    trackers[TRACKER_SIZE_LB].rect = QRectF(boundRect.left()-ssx, boundRect.bottom()-ssy, ssx*2, ssy*2);
    trackers[TRACKER_SIZE_RB].rect = QRectF(boundRect.right()-ssx, boundRect.bottom()-ssy, ssx*2, ssy*2);

    trackers[TRACKER_SIZE_L].rect = QRectF(boundRect.left()-ssx, boundRect.center().y()-ssy, ssx*2, ssy*2);
    trackers[TRACKER_SIZE_R].rect = QRectF(boundRect.right()-ssx, boundRect.center().y()-ssy, ssx*2, ssy*2);
    trackers[TRACKER_SIZE_B].rect = QRectF(boundRect.center().x()-ssx, boundRect.bottom()-ssy, ssx*2, ssy*2);

    //trackers[TRACKER_MOVE].rect = QRectF(boundRect.left()+ssx, boundRect.top()-ssy*2, boundRect.width()-ssx*2, ssy*3);
	trackers[TRACKER_MOVE].rect = QRectF(boundRect.left()+ssx, boundRect.top(), boundRect.width()-ssx*2, ssy*3);

    trackers[TRACKER_ROTATE].rect = QRectF(boundRect.right()+rpos, boundRect.bottom()-rszx, rszx, rszy);


    if ( (boundRect.width()*mat.m11() > style->m_page.iconScaleResI.width()*3)
         && (boundRect.height()*mat.m22() > style->m_page.iconScaleResI.height()*2)
         && (!m_bSolidContainer))
    {
        trackers[TRACKER_CONT_SCALE].rect = QRectF(boundRect.center().x()+boundRect.width()/4-style->m_page.iconScaleResI.width()/(2*mat.m11()),
                                                   boundRect.center().y()-style->m_page.iconScaleI.height()/(2*mat.m22()),
                                                   style->m_page.iconScaleI.width()/mat.m11(),
                                                   style->m_page.iconScaleI.height()/mat.m22());

        trackers[TRACKER_CONT_POS_RES].rect = QRectF(boundRect.center().x()-boundRect.width()/4-style->m_page.iconScaleResI.width()/(2*mat.m11()),
                                                     boundRect.center().y()-style->m_page.iconScaleResI.height()/(2*mat.m22()),
                                                     style->m_page.iconScaleResI.width()/mat.m11(),
                                                     style->m_page.iconScaleResI.height()/mat.m22());
    } else
    {
        trackers[TRACKER_CONT_SCALE].rect = QRectF(0,0,0,0);
        trackers[TRACKER_CONT_POS_RES].rect = QRectF(0,0,0,0);
    }
}


void CREPageItem::fitToRect(QRectF rect, bool skipScale)
{
    if ( m_Type == OT_SLOT )
    {
        QRectF rotRect = m_Shape.boundingRect();
        if (skipScale)
        {
            /*m_BoundSize = QSizeF(rect.width(), rect.height());
            QPainterPath defShape;
            defShape.addRect(0,0, m_BoundSize.width(), m_BoundSize.height());
            setShape(defShape);

            if (m_pContainer!=NULL)
            {
                QPointF Centre(m_Shape.boundingRect().center().x(),m_Shape.boundingRect().center().y());
                QTransform matr;
                matr.translate(Centre.x(), Centre.y());
                matr.rotate(m_Angle);
                matr.translate(-Centre.x(), -Centre.y());
                m_pContainer->setMatrix(matr);
            }*/

            /*// Calculating scale
            double sscx = rect.width()/rotRect.width();
            double sscy = rect.height()/rotRect.height();

            double rangle = m_Angle*M_PI/180.0;
            double scx = sscx * fabs(cos(rangle)) + sscy * fabs(sin(rangle));
            double scy = sscy * fabs(cos(rangle)) + sscx * fabs(sin(rangle));

            // Translating
            //m_Matrix.reset();
            QRectF brect = getBoundRect();
            //QRectF brect = rotRect;
            m_Matrix.translate(rect.center().x()-brect.center().x(), rect.center().y()-brect.center().y());

            // New bound size & shape
            m_BoundSize = QSizeF(m_BoundSize.width()*scx, m_BoundSize.height()*scy);
            QPainterPath defShape;
            defShape.addRect(0,0, m_BoundSize.width(), m_BoundSize.height());
            setShape(defShape);
            rotate(m_Angle, false); // Rotate only shape

            if (m_pContainer!=NULL)
            {
                QPointF Centre(m_Shape.boundingRect().center().x(),m_Shape.boundingRect().center().y());
                QTransform matr;
                matr.translate(Centre.x(), Centre.y());
                matr.rotate(m_Angle);
                matr.translate(-Centre.x(), -Centre.y());
                m_pContainer->setMatrix(matr);
            }*/

            //qDebug() << sscx << sscy << m_BoundSize;
            //qDebug() << m_Angle << cos(rangle) << scx << scy;
            //qDebug() << m_BoundSize << defShape.boundingRect();

            /*QRectF brect = m_Matrix.mapRect(rect);

            double scx = rect.width()/brect.width();
            double scy = rect.height()/brect.height();

            m_Matrix.reset();
            m_Matrix.translate(rect.left(), rect.top());
            m_Matrix.scale(scx, scy);
            m_Matrix.translate(-rotRect.left(), -rotRect.top());*/

            bool inv=false;
            int asect=0;
            if (m_Angle > 315)
                m_Angle = 0;
            else if (m_Angle > 225)
            {
                inv = true;
                m_Angle = 270;
                asect=3;
            }
            else if (m_Angle > 135)
            {
                m_Angle = 180;
                asect=2;
            }
            else if (m_Angle > 45)
            {
                inv = true;
                m_Angle = 90;
                asect=1;
            }
            else
                m_Angle = 0;

            m_Matrix.reset();
            m_Matrix.translate(rect.left(), rect.top());

            QPainterPath defShape;
            if (inv)
                defShape.addRect(0,0, rect.height(), rect.width());
            else
                defShape.addRect(0,0, rect.width(), rect.height());
            setShape(defShape);
            m_BoundSize = defShape.boundingRect().size();
            rotate(m_Angle, false);
            //if (inv)
            //    m_BoundSize.transpose();

            //setBoundSize(m_Shape.boundingRect().size());

            //rotate(m_Angle, false);
            //m_Matrix.rotate(m_Angle);
            /*if (m_pContainer!=NULL)
            {
                m_pContainer->m_Angle = 0;
                m_pContainer->m_Matrix.reset();
                m_pContainer->setBoundSize(defShape.boundingRect().size());
            }*/
            if (m_pContainer!=NULL)
            {
                QPointF Centre(m_Shape.boundingRect().center().x(),m_Shape.boundingRect().center().y());
                QTransform matr;
                matr.translate(Centre.x(), Centre.y());
                matr.rotate(m_Angle);
                matr.translate(-Centre.x(), -Centre.y());
                m_pContainer->setMatrix(matr);


                /*//if (inv)
                //    m_pContainer->setBoundSize(QSizeF(rect.height(), rect.width()));
                //else
                //    m_pContainer->setBoundSize(QSizeF(rect.width(), rect.height()));
                m_pContainer->m_BoundSize = m_BoundSize;
                //QPointF Centre(m_pContainer->m_BoundSize.width()/2,m_pContainer->m_BoundSize.height()/2);
                QTransform matr;
                matr.rotate(m_Angle);
                if (asect==1)
                    matr.translate(0, -rect.width());
                else if (asect==2)
                        matr.translate(-rect.width(), -rect.height());
                else if (asect==3)
                        matr.translate(rect.width(), rect.height());
                m_pContainer->setMatrix(matr);*/
                m_pContainer->m_BoundSize = m_BoundSize;
                m_pContainer->m_Angle = m_Angle;
            }
        } else
        {
            double scx = rect.width()/rotRect.width();
            double scy = rect.height()/rotRect.height();

            m_Matrix.reset();
            m_Matrix.translate(rect.left(), rect.top());
            m_Matrix.scale(scx, scy);
            m_Matrix.translate(-rotRect.left(), -rotRect.top());
        }
    }
}


#define FITTOPAGE

QRectF CREPageItem::fitToPageR(QRectF obj, QSizeF page, int trackerID, double k)
{
    double oxmin, oymin;

    if (k == 0)
        k = obj.width()/obj.height();

    /*if ( k > 1.0 )
    {
        oxmin = OBJ_MIN_WIDTH;
        oymin = oxmin/k;
    } else
    {
        oymin = OBJ_MIN_HEIGHT;
        oxmin = oymin*k;
    }

    if ( (trackerID == TRACKER_SIZE_R)
        || (trackerID == TRACKER_SIZE_L)
        || (trackerID == TRACKER_SIZE_B) )
    {
        oxmin = OBJ_MIN_WIDTH;
        oymin = OBJ_MIN_HEIGHT;
    }*/
    oxmin = OBJ_MIN_WIDTH;
    oymin = OBJ_MIN_HEIGHT;

    QRectF srcRect = obj;

    if (srcRect.left()+oxmin > srcRect.right())
    {
        if ( (trackerID == TRACKER_SIZE_RT)
            || (trackerID == TRACKER_SIZE_RB)
            || (trackerID == TRACKER_SIZE_R) )
            srcRect.setRight(srcRect.left()+oxmin);
        else
            srcRect.setLeft(srcRect.right()-oxmin);
    }

    if (srcRect.top()+oymin > srcRect.bottom())
    {
        if ( (trackerID == TRACKER_SIZE_LT)
            || (trackerID == TRACKER_SIZE_RT))
            srcRect.setTop(srcRect.bottom()-oymin);
        else
            srcRect.setBottom(srcRect.top()+oymin);
    }

    if ( srcRect.width() < oxmin )
        srcRect.setWidth(oxmin);
    if ( srcRect.height() < oymin )
        srcRect.setHeight(oymin);

#ifdef FITTOPAGE
    if ( srcRect.left() < 0 )
    {
        if ( trackerID == TRACKER_SIZE_LT )
            srcRect.adjust(-srcRect.left(), srcRect.left()/k, 0, 0);
        else
            srcRect.adjust(-srcRect.left(), 0, 0, -srcRect.left()/k);
    }
    if ( srcRect.right() > page.width() )
    {
        if ( trackerID == TRACKER_SIZE_RT )
            srcRect.adjust(0, -(page.width()-srcRect.right())/k, page.width()-srcRect.right(), 0);
        else
            srcRect.adjust(0, 0, page.width()-srcRect.right(), (page.width()-srcRect.right())/k);
    }
    if ( srcRect.top() < 0 )
    {
        if ( trackerID == TRACKER_SIZE_LT )
            srcRect.adjust(-srcRect.top()*k, -srcRect.top(), 0, 0);
        else
            srcRect.adjust(0, -srcRect.top(), srcRect.top()*k, 0);
    }
    if ( srcRect.bottom() > page.height() )
    {
        if ( trackerID == TRACKER_SIZE_RB )
            srcRect.adjust(0, 0, k*(page.height()-srcRect.bottom()), page.height()-srcRect.bottom());
        else
            srcRect.adjust(-k*(page.height()-srcRect.bottom()), 0, 0, page.height()-srcRect.bottom());
    }
#else
    if ( srcRect.left() < -obj.width()/2 )
    {
        if ( trackerID == TRACKER_SIZE_LT )
            srcRect.adjust(-srcRect.left()-obj.width()/2, (srcRect.left()-obj.width()/2)/k, 0, 0);
        else
            srcRect.adjust(-srcRect.left()-obj.width()/2, 0, 0, (-srcRect.left()-obj.width()/2)/k);
    }
    if ( srcRect.right() > page.width()+obj.width()/2 )
    {
        if ( trackerID == TRACKER_SIZE_RT )
            srcRect.adjust(0, -(page.width()+obj.width()/2-srcRect.right())/k, page.width()+obj.width()/2-srcRect.right(), 0);
        else
            srcRect.adjust(0, 0, page.width()+obj.width()/2-srcRect.right(), (page.width()+obj.width()/2-srcRect.right())/k);
    }
    if ( srcRect.top() < -obj.height()/2 )
    {
        if ( trackerID == TRACKER_SIZE_LT )
            srcRect.adjust((-srcRect.top()-obj.height()/2)*k, -srcRect.top()-obj.height()/2, 0, 0);
        else
            srcRect.adjust(0, -srcRect.top()-obj.height()/2, (srcRect.top()-obj.height()/2)*k, 0);
    }
    if ( srcRect.bottom() > page.height()+obj.height()/2 )
    {
        if ( trackerID == TRACKER_SIZE_RB )
            srcRect.adjust(0, 0, k*(page.height()+obj.height()/2-srcRect.bottom()), page.height()+obj.height()/2-srcRect.bottom());
        else
            srcRect.adjust(-k*(page.height()+obj.height()/2-srcRect.bottom()), 0, 0, page.height()+obj.height()/2-srcRect.bottom());
    }
#endif
    return srcRect;
}


QRectF CREPageItem::fitToPageP(QRectF obj, QSizeF page)
{
    QRectF srcRect = obj;

#ifdef FITTOPAGE
    if ( obj.left() < 0 )
        srcRect.translate(-obj.left(), 0);
    if ( obj.right() > page.width() )
        srcRect.translate(page.width()-obj.right(), 0);
    if ( obj.top() < 0 )
        srcRect.translate(0, -obj.top());
    if ( obj.bottom() > page.height() )
        srcRect.translate(0, page.height()-obj.bottom());
#else
    if (obj.left() < -obj.width()/2)
        srcRect.translate(-obj.left()-obj.width()/2, 0);
    if (obj.right() > page.width()+obj.width()/2)
        srcRect.translate(page.width()-obj.right()+obj.width()/2, 0);
    if (obj.top() < -obj.height()/2)
        srcRect.translate(0, -obj.top()-obj.height()/2);
    if (obj.bottom() > page.height()+obj.height()/2)
        srcRect.translate(0, page.height()-obj.bottom()+obj.height()/2);
#endif
    return srcRect;
}


CREPage::CREPage(calcNumContainer *pNumContainer, CStyle *style)
{
    //datafile = cdata;
#ifdef Q_OS_MAC
    qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/");
#else
    qsResPath = QCoreApplication::applicationDirPath() + QString("/images/");
#endif

    numContainer = pNumContainer;

    m_pStyle = style;
    m_PageSize = QSizeF(210*4, 297*4);
    m_thumbnail = NULL;
    m_PageSeq = 0;
    m_PageGenOptions = 0;
    //defIcon = QIcon(cdata->settings->imagesPath + "noimage.png");
    defIcon.append(QImage(qsResPath + "noimage.png"));
    ground.color.setRgb(255, 255, 255);
    ground.image = QImage();
    ground.drawMode = CANCEL_DLG;
}

CREPage::~CREPage()
{
    for (int a=0; a<m_PageObjects.size(); a++)
        delete m_PageObjects.at(a);
    m_PageObjects.clear();

    if (m_thumbnail != NULL)
        delete m_thumbnail;
}

void CREPage::moveToPage(CREPageItem* item)
{
    QRectF obj = item->getBoundRect();

    if ( obj.left() < 0 )
        item->move(-obj.left(), 0);
    if ( obj.right() > m_PageSize.width() )
        item->move(m_PageSize.width()-obj.right(), 0);
    if ( obj.top() < 0 )
        item->move(0, -obj.top());
    if ( obj.bottom() > m_PageSize.height() )
        item->move(0, m_PageSize.height()-obj.bottom());
}

void CREPage::setPageSize(QSizeF Sz)
{
    m_PageSize = Sz;
    for (int a=0; a<m_PageObjects.size(); a++)
        moveToPage(m_PageObjects.at(a));
}


void CREPage::getSelected(QList<CREPageItem*> &list, bool ignoreDelected)
{
    list.clear();
    for (int a=0; a<m_PageObjects.size(); a++)
    {
        if ( m_PageObjects[a]->objectType() == OT_SLOT )
        {
            if ( m_PageObjects[a]->getSelected() || ignoreDelected)
            {
                list.append(m_PageObjects[a]);
            }
        }
    }
}


bool CREPage::getObjectsList(double x, double y, QList<CREPageItem*> &list)
{
    return getObjectsList(QPointF(x, y), list);
}


bool CREPage::getObjectsList(QPointF Pt, QList<CREPageItem*> &list)
{
    list.clear();

    for (int a=0; a<m_PageObjects.size(); a++)
    {
        if ( m_PageObjects[a]->objectType() == OT_SLOT )
        {
            QTransform matr = m_PageObjects[a]->getMatrix();
            QPainterPath mapShape = matr.map(m_PageObjects[a]->getShape());
            if ( mapShape.contains(Pt) )
                list.prepend(m_PageObjects[a]);
        }
    }

    if ( list.size() > 0 )
        return true;
    else
        return false;
}


void CREPage::unselectAll()
{
    for (int a=0; a<m_PageObjects.size(); a++)
        m_PageObjects[a]->setSelected(false);
}

void CREPage::draw(QPainter *pPntr, CDrawMode *drawMode, CStyle *style, bool drawPaper)
{
    pPntr->save();


    //draw page paper
    if (drawPaper)
    {
        pPntr->setPen(Qt::NoPen);
        pPntr->setBrush(Qt::white);
        pPntr->drawRect(QRectF(0, 0, m_PageSize.width(), m_PageSize.height()));
    }
    pPntr->setBrush(Qt::NoBrush);

   // pPntr->fillRect(QRectF(0, 0, m_PageSize.width(), m_PageSize.height()), qRgb(255, 0, 0));

    if (ground.drawMode != CANCEL_DLG)
    {
        if (ground.image.isNull() == false)
        {
            if (ground.drawMode == GROUND_ONE_IN_ONE)
            {
                pPntr->drawImage(0, 0, ground.image);
            }
            else
            if (ground.drawMode == GROUND_FIT_SIZE)
            {
                pPntr->drawImage(QRectF(0, 0, m_PageSize.width(), m_PageSize.height()), ground.image);
            }
            else
            {
                pPntr->fillRect(QRectF(0, 0, m_PageSize.width(), m_PageSize.height()), ground.color);
            }
        }
        else
        {
            pPntr->fillRect(QRectF(0, 0, m_PageSize.width(), m_PageSize.height()), ground.color);
        }
    }

    //grid
    if ((drawMode->grx>1) && (drawMode->gry>1))
    {
        QPen pen1(QColor(222, 222, 222));
        pPntr->setPen(pen1);

        for (double py=drawMode->gry; py<m_PageSize.height(); py += drawMode->gry)
            pPntr->drawLine(QPointF(0, py), QPointF(m_PageSize.width(), py));
        for (double px=drawMode->grx; px<m_PageSize.width(); px += drawMode->grx)
            pPntr->drawLine(QPointF(px, 0), QPointF(px, m_PageSize.height()));
    }


    //drawing document
    pPntr->setRenderHint(QPainter::Antialiasing);
    pPntr->setClipRect(0,0, m_PageSize.width(), m_PageSize.height());


    for (int a=0; a<m_PageObjects.size(); a++)
        m_PageObjects[a]->draw(pPntr, drawMode, style);


    pPntr->restore();
}


void CREPage::draw(QPainter *pPntr, QRectF rect, CDrawMode *drawMode, CStyle *style, bool drawPaper)
{
    QTransform Matrix;
    qreal scale_x = rect.width()/(m_PageSize.width());
    qreal scale_y = rect.height()/(m_PageSize.height());
    qreal min_scale = scale_x;
    if( scale_y < scale_x )
        min_scale = scale_y;
    qreal ScaledPageWidth = m_PageSize.width()*min_scale;
    qreal ScaledPageHeight = m_PageSize.height()*min_scale;
    qreal dx = cround((rect.width()-ScaledPageWidth)/2, 5);
    qreal dy = cround((rect.height()-ScaledPageHeight)/2, 5);
    Matrix.translate(rect.left(),rect.top());
    Matrix.translate(dx,dy);
    Matrix.scale(min_scale, min_scale);
    pPntr->setRenderHint(QPainter::SmoothPixmapTransform);

    pPntr->save();
    pPntr->setTransform(Matrix);

    draw(pPntr, drawMode, style, drawPaper);
    pPntr->restore();
}


void CREPage::generateThumbnail(bool drawFrame)
{

    int ox, oy;
    double k = calcFitScale(m_PageSize.width(), m_PageSize.height(), 256, 256, ox, oy);

    QSize tsz = QSize(m_PageSize.width()*k, m_PageSize.height()*k);
    if (m_thumbnail == NULL)
    {
        m_thumbnail = new QImage(tsz, QImage::Format_ARGB32);
    }
    else if (m_thumbnail->size() != tsz)
    {
        delete m_thumbnail;
        m_thumbnail = new QImage(tsz, QImage::Format_ARGB32);
    }

    QPainter *pntr = new QPainter(m_thumbnail);

    pntr->fillRect(0,0,tsz.width(),tsz.height(),
                   QBrush(QColor(255,255,255)));

    CDrawMode drawMode;
    drawMode.thumb = true;

    draw(pntr, QRectF(0, 0, tsz.width(), tsz.height()), &drawMode, m_pStyle);

    if (drawFrame)
    {
        pntr->setPen(QColor(0,0,0,255));
        pntr->drawRect(0,0,tsz.width()-1,tsz.height()-1);
    }

    delete pntr;
}


void CREPage::drawTrackers(QPainter *pPntr, CDrawMode *drawMode, CStyle *style, QList<CREPageItem*> &list)
{
    for (int a=0; a<list.size(); a++)
    {
        list[a]->drawTracker(pPntr, drawMode, style);
    }
}


void CREPage::prefillData()
{


//    if (m_itemid < 0)
//        return;

//    CDataFile::slinedata *ld = datafile->doc.lines.at(m_itemid);

//    for (int i=0; i<m_PageObjects.size(); i++)
//    {
//        CREPageItem *item = m_PageObjects.at(i);
//        if (item->m_pContainer!=NULL)
//        {
//            item = item->m_pContainer;
//            if (item->m_Type == OT_DATA_FIELD)
//            {
//                int idx = datafile->indexByFieldID(item->id);
//                if (idx>=0)
//                {
//                    item->m_Text = datafile->fieldToText(ld, idx, true);
//                }
//            } else if (item->m_Type == OT_BAR_CODE)
//            {
//                int idx = datafile->indexByFieldID(item->id);
//                //qDebug(QString::number(idx).toLocal8Bit());
//                if (idx>=0)
//                {

//                    item->m_Text = datafile->fieldToText(ld, idx, true);
//                }
//                //qDebug(QString::number(idx).toLocal8Bit());
//            }else if (item->m_Type == OT_DATA_FIELD_NAME)
//            {
//                if (item->id=="#pagenumber")
//                {
//                    item->m_Text = QString::number(m_pnum);
//                } else if (item->id=="#date")
//                {
//                    item->m_Text = QDate::currentDate().toString(datafile->doc.docset.dateFormat);
//                } else if (item->id=="#dbname")
//                {
//                    item->m_Text = datafile->doc.docName;
//                } else
//                {
//                    int idx = datafile->indexByFieldID(item->id);
//                    if (idx>=0)
//                        item->m_Text = datafile->doc.docHeader.at(idx).dname;
//                }
//            } else if (item->m_Type == OT_DATA_IMAGE)
//            {
//                int idx = datafile->indexByFieldID(item->id);
//                if ((idx>=0) && (datafile->doc.docHeader.at(idx).dtype == CDataFile::ETImageList))
//                {
//                    if ((item->m_Idx>=0) && (item->m_Idx<(*(CDataFile::SDTImageList *)ld->fields.at(idx)).val.size()))
//                    {
//                        item->m_imgID = (*(CDataFile::SDTImageList *)ld->fields.at(idx)).val.at(item->m_Idx).val;
//                    }
//                    else
//                        item->m_imgID = -1;
//                } else
//                {
//                    item->m_imgID = -1;
//                }
//            } else if (item->m_Type == OT_ICON)
//            {
//                int idx = datafile->indexByFieldID(item->id);
//                if ((idx>=0) && (datafile->doc.docHeader.at(idx).dtype == CDataFile::ETIcon))
//                {
//                    item->m_itemid = m_itemid;
//                    CDataFile::slinedata *ld = datafile->doc.lines.at(m_itemid);
//                    if (!(*(CDataFile::SDTIcon *)ld->fields.at(idx)).icon.isEmpty())
//                    {
//                        item->icon = &(*(CDataFile::SDTIcon *)ld->fields.at(idx)).icon;
//                    }
//                    else
//                    {
//                        item->icon = &defIcon;
//                    }
//                }
//                else
//                {
//                    item->m_itemid = -1;
//                }
//            }
//        }
//    }
}

#ifndef VER_MOBILE
void CReportPE::dropEvent(QDropEvent *event)
{
    event->acceptProposedAction();
    emit itemDropped(event->pos());
}

void CReportPE::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("application/x-qabstractitemmodeldatalist"))
        event->acceptProposedAction();
}
#endif

QTransform CReportPE::calcDocViewTransform()
{
    QTransform transf;
    QPointF TransP = calcDocViewTranslate();
    transf.translate(TransP.x(), TransP.y());
    transf.scale(m_Scale, m_Scale);
    return transf;
}


QPointF CReportPE::calcDocViewTranslate()
{
    QSizeF &psz = page->m_PageSize;
    double spacing = m_Spacing;

    double dx = -horizontalScrollBar()->value(), dy = -verticalScrollBar()->value();
    dx += spacing;
    dy += spacing;

    QSize ViewportSize = viewport()->size();
    QSize DocSize;
    DocSize.setWidth(qRound(psz.width()*m_Scale + 2*spacing));
    DocSize.setHeight(qRound(psz.height()*m_Scale + 2*spacing));
    int sx = (ViewportSize.width()-DocSize.width())/2; if( sx < 0 ) sx = 0;
    int sy = (ViewportSize.height()-DocSize.height())/2; if( sy < 0 ) sy = 0;
    dx += sx;
    dy += sy;

    return QPointF(dx,dy);
}


void CReportPE::paintEvent(QPaintEvent *)
{
#ifndef VER_MOBILE
    QPainter pntr(viewport());

    pntr.save();
    pntr.fillRect(0, 0, width(), height(), appstyle->updateColor(QColor(44, 36, 32)));


    //QSizeF psz = m_pDoc->getPageSize();
    //QSize vsz = viewport()->size();

    QTransform transf = calcDocViewTransform();
    //int pcnt = 2;


    // Draw page contents
    CREPage *pPage = page;
    if (pPage!=NULL)
    {
        pntr.save();
        pntr.setTransform(transf);
        page->draw(&pntr, drawMode, style);
        pntr.restore();
    }

    // Draw page canvas
    QRectF rpage = transf.mapRect(QRectF(0,0,page->m_PageSize.width(),page->m_PageSize.height()));
    //QSize vsz = viewport()->size();

    double ltx = rpage.left(), lty = rpage.top();
    double rtx = rpage.right(), rty = rpage.top();
    double lbx = rpage.left(), lby = rpage.bottom();
    double rbx = rpage.right(), rby = rpage.bottom();


    CAppStyle::SPage *spage = &appstyle->spagerep;//&settings->appstyle->spagerep;

    //pntr.fillRect(QRectF(ltx-1, lty-1, rtx-ltx+2, rby-rty+2), spage->pageColor);

//    pntr.fillRect(
//                QRectF(0,0,
//                       ltx-spage->pageLT.width()+2, lty-spage->pageLT.height()+2),
//                spage->canvasLT);
//    pntr.fillRect(QRectF(rtx+spage->pageLT.width()-1, 0,
//                         vsz.width()-rtx-spage->pageRT.width()+2, rty-spage->pageRT.height()+2),
//                  spage->canvasRT);
//    pntr.fillRect(QRectF(0, lby+spage->pageLB.height()-1,
//                         ltx-spage->pageLB.width()+2, vsz.height()-lby-spage->pageLB.height()+2),
//                  spage->canvasLB);
//    pntr.fillRect(QRectF(rbx+spage->pageRB.width()-1, rby+spage->pageRB.height()-1,
//                         vsz.width()-rbx-spage->pageRB.width()+2, vsz.height()-rby-spage->pageRB.height()+2),
//                  spage->canvasRB);

//    pntr.drawImage(
//                QRectF(0, lty, ltx-spage->pageLeft.width(), lby-lty),
//                spage->pageLeft,
//                QRectF(0, 0, 1, spage->pageLeft.height()));
//    pntr.drawImage(
//                QRectF(rtx+spage->pageRight.width(), rty, vsz.width()-rtx-spage->pageRight.width(), rby-rty),
//                spage->pageRight,
//                QRectF(spage->pageRight.width()-1, 0, 1, spage->pageRight.height()));
//    pntr.drawImage(
//                QRectF(ltx, 0, rtx-ltx, lty-spage->pageTop.height()),
//                spage->pageTop,
//                QRectF(0, 0, spage->pageTop.width(), 1));
//    pntr.drawImage(
//                QRectF(lbx, lby+spage->pageBottom.height(), rbx-lbx, vsz.height()-lby-spage->pageBottom.height()),
//                spage->pageBottom,
//                QRectF(0, spage->pageTop.height()-1, spage->pageTop.width(), 1));

//    pntr.drawImage(
//                QRectF(0, lty-spage->pageLT.height(), ltx-spage->pageLT.width(), spage->pageLT.height()),
//                spage->pageLT,
//                QRectF(0, 0, 1, spage->pageLT.height()));
//    pntr.drawImage(
//                QRectF(0, lby, lbx-spage->pageLB.width(), spage->pageLB.height()),
//                spage->pageLB,
//                QRectF(0, 0, 1, spage->pageLB.height()));
//    pntr.drawImage(
//                QRectF(rtx+spage->pageRT.width(), rty-spage->pageRT.height(), vsz.width()-rtx-spage->pageRT.width(), spage->pageRT.height()),
//                spage->pageRT,
//                QRectF(spage->pageRT.width()-1, 0, 1, spage->pageRT.height()));
//    pntr.drawImage(
//                QRectF(rbx+spage->pageRB.width(), rby, vsz.width()-rbx-spage->pageRB.width(), spage->pageRB.height()),
//                spage->pageRB,
//                QRectF(spage->pageRB.width()-1, 0, 1, spage->pageRB.height()));
//    pntr.drawImage(
//                QRectF(ltx-spage->pageLT.width(), 0, spage->pageLT.width(), lty-spage->pageLT.height()),
//                spage->pageLT,
//                QRectF(0, 0, spage->pageLT.width(), 1));
//    pntr.drawImage(
//                QRectF(rtx, 0, spage->pageRT.width(), rty-spage->pageRT.height()),
//                spage->pageRT,
//                QRectF(0, 0, spage->pageRT.width(), 1));
//    pntr.drawImage(
//                QRectF(lbx-spage->pageLB.width(), lby+spage->pageLB.height(), spage->pageLB.width(), vsz.height()-lby-spage->pageLB.height()),
//                spage->pageLB,
//                QRectF(0, spage->pageLB.height()-1, spage->pageLB.width(), 1));
//    pntr.drawImage(
//                QRectF(rbx, rby+spage->pageRB.height(), spage->pageRB.width(), vsz.height()-rby-spage->pageRB.height()),
//                spage->pageRB,
//                QRectF(0, spage->pageRB.height()-1, spage->pageRB.width(), 1));

    pntr.drawImage(
                QRectF(ltx-spage->pageLeft.width(), lty, spage->pageLeft.width(), lby-lty),
                spage->pageLeft,
                spage->pageLeft.rect());
    pntr.drawImage(
                QRectF(rtx, rty, spage->pageRight.width(), rby-rty),
                spage->pageRight,
                spage->pageRight.rect());
    pntr.drawImage(
                QRectF(ltx, lty-spage->pageTop.height(), rtx-ltx, spage->pageTop.height()),
                spage->pageTop,
                spage->pageTop.rect());
    pntr.drawImage(
                QRectF(lbx, lby, rbx-lbx, spage->pageBottom.height()),
                spage->pageBottom,
                spage->pageBottom.rect());

    pntr.drawImage(
                QRectF(ltx-spage->pageLT.width(), lty-spage->pageLT.height(), spage->pageLT.width(), spage->pageLT.height()),
                spage->pageLT,
                spage->pageLT.rect());
    pntr.drawImage(
                QRectF(rtx, rty-spage->pageRT.height(), spage->pageRT.width(), spage->pageRT.height()),
                spage->pageRT,
                spage->pageRT.rect());
    pntr.drawImage(
                QRectF(lbx-spage->pageLB.width(), lby, spage->pageLB.width(), spage->pageLB.height()),
                spage->pageLB,
                spage->pageLB.rect());
    pntr.drawImage(
                QRectF(rbx, rby, spage->pageRB.width(), spage->pageRB.height()),
                spage->pageRB,
                spage->pageRB.rect());

/*
    // Draw separator
    QPen Pen(QColor(Qt::black), 1, Qt::DashLine);
    pntr.setBrush(Qt::NoBrush);
    pntr.setPen(Pen);
    double xstep = (rtx-ltx)/pcnt;
    for (int a=1; a<pcnt; a++)
        pntr.drawLine(QPointF(ltx+xstep*a, lty), QPointF(lbx+xstep*a, lby));
*/

    if (pPage!=NULL)
    {
//        if (drawMode->isDrawBorder == true)
//        {
//            pntr.setTransform(transf);
//            QList<CREPageItem*> oList;
//            pPage->getSelected(oList, true);
//            for (int a = 0; a < oList.size(); a++)
//            {
//                oList[a]->drawTracker(&pntr, drawMode, style, true);
//            }
//        }
        if ( m_anim.trackerAnim.size() == 0 )
        {
            pntr.save();
            pntr.setTransform(transf);
            QList<CREPageItem*> oList;
            pPage->getSelected(oList);
            pPage->drawTrackers(&pntr, NULL, style, oList);

            pntr.restore();
        } else
        {
            pntr.save();
            pntr.setTransform(transf);

            for (int a=0; a<m_anim.trackerAnim.size(); a++ )
            {
                if ( m_anim.trackerAnim[a].cnt > 0 )
                    m_anim.trackerAnim[a].cnt--;

                double opac;
                if ( m_anim.trackerAnim[a].toVisible )
                    opac = 1.0 - (double)m_anim.trackerAnim[a].cnt/(double)m_anim.trackerAnim[a].etime;
                else
                    opac = (double)m_anim.trackerAnim[a].cnt/(double)m_anim.trackerAnim[a].etime;

                pntr.setOpacity(opac);

                m_anim.trackerAnim[a].obj->drawTracker(&pntr, NULL, style);
            }
            pntr.restore();

            if ( m_anim.trackerAnim.size() != 0 )
            {
                bool finished;
                do
                {
                    finished = true;
                    for (int a=0; a<m_anim.trackerAnim.size(); a++)
                    {
                        if ( m_anim.trackerAnim[a].cnt == 0 )
                        {
                            m_anim.trackerAnim.removeAt(a);

                            finished = false;
                            break;
                        }
                    }
                } while ( !finished );
#ifdef Q_WS_MAC
                animTmr.start(30);
#else
                animTmr.start(30);
#endif
            }
        }
    }

    pntr.restore();
#endif
    updateFixScroll();
}


void CReportPE::resizeEvent(QResizeEvent*)
{
    QSizeF psz = page->m_PageSize;
    QSize ViewportSize = viewport()->size();

    QSize docSize(0,0);
    docSize.setWidth(qRound(psz.width()*m_Scale + 2*m_Spacing));
    docSize.setHeight(qRound(psz.height()*m_Scale + 2*m_Spacing));

    horizontalScrollBar()->setRange(0, docSize.width() - ViewportSize.width());
    horizontalScrollBar()->setPageStep(ViewportSize.width()/2);
    horizontalScrollBar()->setSingleStep(8);

    verticalScrollBar()->setRange(0, docSize.height() - ViewportSize.height());
    verticalScrollBar()->setPageStep(ViewportSize.height()/2);
    verticalScrollBar()->setSingleStep(8);

    if (m_grid==1)
    {
        drawMode->grx = m_gridX*2;
        drawMode->gry = m_gridY*2;
    } else
    {
        drawMode->grx = 0;
        drawMode->gry = 0;
    }

    updateFixScroll();
    //viewport()->resize(docSize);
}

void CReportPE::updateView()
{
    resizeEvent(NULL);
    viewport()->update();
    updateFixScroll();
}

#ifndef VER_MOBILE
void CReportPE::OnZoomIn()
{
    m_Scale *= 1.10;
    if( m_Scale > 2.0 )
        m_Scale = 2.0;
    updateView();

    updateFixScroll();
}

void CReportPE::OnZoomOut()
{
    m_Scale /= 1.10;
    if( m_Scale < 0.2 )
        m_Scale = 0.2;
    updateView();

    updateFixScroll();
}




void CReportPE::OnGrid()
{
    if (m_grid==0)
        m_grid=1;
    else
        m_grid=0;

    updateView();
    updateFixScroll();
}

void CReportPE::OnSnap()
{
    if (m_snap==0)
        m_snap=1;
    else
        m_snap=0;
    updateFixScroll();
}

void CReportPE::OnBoundaries()
{
    if (drawMode->isDrawBorder == 0)
        drawMode->isDrawBorder = 1;
    else
        drawMode->isDrawBorder = 0;
    viewport()->update();
    updateFixScroll();
}




void CReportPE::mousePressEvent(QMouseEvent *pEvent)
{
    QPointF Pt;
    if (pEvent != NULL)
        Pt = QPointF(pEvent->x(), pEvent->y());
    else
    {
        emit selectionChanged();
        return;
    }
        //Pt = QPointF(-1000, -1000);
    QTransform Matrix = calcDocViewTransform();
    Pt = Matrix.inverted().map(Pt);

    CREPage *pPage = page;

    if (pPage!=NULL)
    {
        if (/*(pEvent == NULL) || */((pEvent->buttons() & Qt::LeftButton)!=0))
        {
            QList<CREPageItem*> objList;
            QList<CREPageItem*> poList;
            pPage->getSelected(poList);

            if ( poList.size() > 0 )
            {
                m_pSelObject = poList[0];
                emit selectionChanged();

                int trackIdx = -1;
                STracker trackers[TrackersNum];
                m_pSelObject->genTrackers(trackers, m_pSelObject->getShapeBoundRect(), Matrix, style);
                for (int a=0; a<TrackersNum; a++)
                {
                    if ( trackers[a].rect.contains(Pt) )
                    {
                        trackIdx = a;
                        break;
                    }
                }

                // Move object action
                if ( trackIdx == TRACKER_MOVE )
                {
                    m_editAction = ACT_MOVE_OBJECT;
                    m_selObjMatrix = m_pSelObject->getMatrix();
                    m_selObjAngle = m_pSelObject->getAngle();

                    m_dragStartPt = Pt;
                    m_dragStartRc = m_pSelObject->getShapeBoundRect();
                    m_dragStartSz = m_dragStartRc.size();
                } else if ( trackIdx == TRACKER_ROTATE )
                {
                    if ((m_pSelObject->m_pContainer!=NULL)
                            && ((m_pSelObject->m_pContainer->m_Type == OT_DATA_FIELD) || (m_pSelObject->m_pContainer->m_Type == OT_DATA_FIELD_NAME) || (m_pSelObject->m_pContainer->m_Type == OT_DATA_IMAGE) || (m_pSelObject->m_pContainer->m_Type == OT_ICON) || (m_pSelObject->m_pContainer->m_Type == OT_BAR_CODE)))
                        m_editAction = ACT_ROTATE_IOBJECT;
                    else
                        m_editAction = ACT_ROTATE_OBJECT;
                    m_selObjMatrix = m_pSelObject->getMatrix();
                    m_selObjAngle = m_pSelObject->getAngle();
                    m_selObjShape = m_pSelObject->getShape();

                    m_dragStartPt = Pt;
                    m_dragStartRc = m_pSelObject->getShapeBoundRect();
                    m_dragStartSz = m_dragStartRc.size();
                } else if ( (trackIdx == TRACKER_SIZE_LT)
                    || (trackIdx == TRACKER_SIZE_RT)
                    || (trackIdx == TRACKER_SIZE_LB)
                    || (trackIdx == TRACKER_SIZE_RB)
                    || (trackIdx == TRACKER_SIZE_L)
                    || (trackIdx == TRACKER_SIZE_R)
                    || (trackIdx == TRACKER_SIZE_B) )
                {
                    if ((m_pSelObject->m_pContainer!=NULL)
                            && ((m_pSelObject->m_pContainer->m_Type == OT_DATA_FIELD) || (m_pSelObject->m_pContainer->m_Type == OT_DATA_FIELD_NAME) || (m_pSelObject->m_pContainer->m_Type == OT_DATA_IMAGE) || (m_pSelObject->m_pContainer->m_Type == OT_ICON) || (m_pSelObject->m_pContainer->m_Type == OT_BAR_CODE)))
                        m_editAction = ACT_UPDATE_SIZE;
                    else
                        m_editAction = ACT_RESIZE_OBJECT;

                    m_selObjMatrix = m_pSelObject->getMatrix();
                    m_selObjAngle = m_pSelObject->getAngle();
                    m_selObjShape = m_pSelObject->getShape();

                    m_dragStartPt = Pt;
                    m_dragStartRc = m_pSelObject->getShapeBoundRect();
                    m_dragStartSz = m_dragStartRc.size();
                    m_dragTrackerID = trackIdx;
                }
            }
            if ( m_editAction == ACT_NONE )
            {
                pPage->unselectAll();

                if ( pPage->getObjectsList(Pt, objList) )
                    objList[0]->setSelected(true);
                emit selectionChanged();

                QList<CREPageItem*> oList;
                pPage->getSelected(oList);

                // Select Animation
                {
                    if ( oList.size() > 0 )
                    {
                        STrackerAnim anim;
                        anim.cnt = 5;
                        anim.etime = 5;
                        anim.obj = oList[0];
                        anim.toVisible = true;

                        bool aadd = true;
                        for (int a=0; a<poList.size(); a++)
                            //for (int a=poList.size()-1; a>=0; a--)
                        {
                            if ( oList[0]== poList[a] )
                            {
                                aadd = false;
                            } else
                            {
                                STrackerAnim panim;
                                panim.cnt = 3;
                                panim.obj = poList[a];
                                panim.toVisible = false;
                                panim.etime = 3;
                                bool paadd = true;
                                for (int i=0; i<m_anim.trackerAnim.size(); i++)
                                {
                                    if ( m_anim.trackerAnim[i].obj == panim.obj )
                                    {
                                        paadd = false;
                                        break;
                                    }
                                }
                                if ( paadd )
                                    m_anim.trackerAnim.append(panim);
                            }
                        }

                        for (int i=0; i<m_anim.trackerAnim.size(); i++)
                        {
                            if ( m_anim.trackerAnim[i].obj == anim.obj )
                            {
                                aadd = false;
                                break;
                            }
                        }
                        if ( aadd )
                            m_anim.trackerAnim.append(anim);

                    } else
                    {
                        for (int a=0; a<poList.size(); a++)
                        {
                            STrackerAnim panim;
                            panim.cnt = 3;
                            panim.obj = poList[a];
                            panim.toVisible = false;
                            panim.etime = 3;

                            bool aadd = true;
                            for (int i=0; i<m_anim.trackerAnim.size(); i++)
                            {
                                if ( m_anim.trackerAnim[i].obj == panim.obj )
                                {
                                    aadd = false;
                                    break;
                                }
                            }
                            if ( aadd )
                                m_anim.trackerAnim.append(panim);
                        }
                    }
                }

                if ( oList.size() > 0 )
                {
                    m_pSelObject = oList[0];

                    // Container move action
                    if (m_pSelObject->m_pContainer != NULL)
                    {
                        if ( m_pSelObject->m_bSolidContainer )
                        {
                            m_dragTrackerID = TRACKER_MOVE;
                            m_editAction = ACT_MOVE_OBJECT;
                        } else
                        {
                            m_editAction = ACT_MOVE_CONTAINER;
                        }
                        m_selObjMatrix = m_pSelObject->getMatrix();
                        m_selObjAngle = m_pSelObject->getAngle();

                        m_dragStartPt = Pt;
                        m_dragStartRc = m_pSelObject->getShapeBoundRect();
                        m_dragStartSz = m_dragStartRc.size();
                    }
                }
            }
        } else if(((pEvent->buttons() & Qt::RightButton)!=0) && (m_editAction==ACT_NONE))
        {
            //m_placePoint = Pt;

            QList<CREPageItem*> objList;
            if ( pPage->getObjectsList(Pt, objList) )
            {
                /*m_pEditObject = objList[0];
                if ( (m_pEditObject->objectType() == OT_SLOT) && (!m_pEditObject->m_bSolidContainer))
                {
                    m_PopupMenu.clear();
                    m_PopupMenu.addAction(actionEditSlot);
                    m_PopupMenu.popup(mapToGlobal(pEvent->pos()));
                } else if ((m_pEditObject->objectType() == OT_SLOT) && (m_pEditObject->m_bSolidContainer) && (m_pEditObject->m_pContainer != NULL))
                {
                    if (m_pEditObject->m_pContainer->objectType() == OT_TEXT)
                    {
                        m_PopupMenu.clear();
                        m_PopupMenu.addAction(actionEditText);
                        m_PopupMenu.popup(mapToGlobal(pEvent->pos()));
                    } else if (m_pEditObject->m_pContainer->objectType() == OT_VECTOR_CLIP)
                    {
                        m_PopupMenu.clear();
                        m_PopupMenu.addAction(actionEditClipart);
                        m_PopupMenu.popup(mapToGlobal(pEvent->pos()));
                    }
                }*/
            } else
            {
                /*m_PopupMenu.clear();
                m_PopupMenu.addAction(actionAddText);
                m_PopupMenu.addAction(actionAddSlot);
                m_PopupMenu.addAction(actionAddClipart);
                m_PopupMenu.addSeparator();
                m_PopupMenu.addAction(actionSaveTemplate);
                m_PopupMenu.addAction(actionSaveFullTemplate);
                m_PopupMenu.popup(mapToGlobal(pEvent->pos()));*/
            }
        }
    }

    viewport()->update();
}
#endif

void CReportPE::OnZoomNormal()
{
    m_Scale = 0.75;
    updateView();
}

QPointF CReportPE::snapPoint(QPointF Pt, int snap)
{
    if (snap==1)
    {
        double x = floor(Pt.x()/m_gridX + 0.5) * m_gridX;
        double y = floor(Pt.y()/m_gridY + 0.5) * m_gridY;
        return QPointF(x, y);
    } else
        return Pt;
}

#ifndef VER_MOBILE
void CReportPE::mouseProcessEvent(QMouseEvent *pEvent, bool release)
{
    QPointF Pt;
    QTransform Matrix = calcDocViewTransform();
    if (pEvent!=NULL)
    {
        Pt = QPointF(pEvent->x(), pEvent->y());
        Pt = Matrix.inverted().map(Pt);
        lastPt = Pt;
    } else
    {
        Pt = lastPt;
    }


    if (m_editAction==ACT_NONE)
    {
        CREPage *pPage = page;

        if (pPage!=NULL)
        {
            //QList<CREPageItem*> objList;
            QList<CREPageItem*> poList;
            pPage->getSelected(poList);

            if ( poList.size() > 0 )
            {
                m_pSelObject = poList[0];

                int trackIdx = -1;
                STracker trackers[TrackersNum];
                m_pSelObject->genTrackers(trackers, m_pSelObject->getShapeBoundRect(), Matrix, style);
                for (int a=0; a<TrackersNum; a++)
                {
                    if ( trackers[a].rect.contains(Pt) )
                    {
                        trackIdx = a;
                        break;
                    }
                }
                if ((trackIdx == TRACKER_SIZE_LT) || (trackIdx == TRACKER_SIZE_RB))
                    setCursor(Qt::SizeFDiagCursor);
                else if ((trackIdx == TRACKER_SIZE_LB) || (trackIdx == TRACKER_SIZE_RT))
                    setCursor(Qt::SizeBDiagCursor);
                else if ((trackIdx == TRACKER_SIZE_L) || (trackIdx == TRACKER_SIZE_R))
                    setCursor(Qt::SizeHorCursor);
                else if (trackIdx == TRACKER_SIZE_B)
                    setCursor(Qt::SizeVerCursor);
                else if (trackIdx == TRACKER_MOVE)
                    setCursor(Qt::SizeAllCursor);
                else if (trackIdx == TRACKER_ROTATE)
                    setCursor(Qt::OpenHandCursor);
                else if (trackIdx == TRACKER_CONT_SCALE)
                    setCursor(Qt::OpenHandCursor);
                else if (trackIdx == TRACKER_CONT_POS_RES)
                    setCursor(Qt::OpenHandCursor);
                else
                    setCursor(Qt::ArrowCursor);
            } else
                setCursor(Qt::ArrowCursor);
        } else
            setCursor(Qt::ArrowCursor);
    } else if (m_editAction == ACT_MOVE_OBJECT)
    {
        double dx = Pt.x()-m_dragStartPt.x();
        double dy = Pt.y()-m_dragStartPt.y();

        QRectF bound = m_dragStartRc;
        bound.translate(dx, dy);
        bound.moveTopLeft(snapPoint(bound.topLeft(), m_snap));

        bound = CREPageItem::fitToPageP(bound, page->getPageSize());
        m_pSelObject->fitToRect(bound);

        //if (release)
        //    m_pHistory->add(ACT_MOVE_OBJECT, m_pDoc);

        viewport()->update();
    } else if ((m_editAction == ACT_ROTATE_OBJECT) || (m_editAction == ACT_ROTATE_IOBJECT))
    {
        QRectF bound = m_dragStartRc;
        double dx = bound.center().x();
        double dy = bound.center().y();

        QPointF centre(dx, dy);
        double angle = -findAngleBetweenVectors(&centre, &m_dragStartPt, &Pt);

        //QPointF Centre(m_dragStartSz.width()/2, m_dragStartSz.height()/2);
        QPointF NewCentre = QPointF(m_dragStartSz.width()/2, m_dragStartSz.height()/2);

        m_pSelObject->setMatrix(m_selObjMatrix);
        m_pSelObject->setShape(m_selObjShape);
        if (release)
            m_pSelObject->rotate(angle, true);
        else
            m_pSelObject->rotate(angle, false);

        NewCentre = m_pSelObject->getShapeBoundRect().center();

        double cx = centre.x()-NewCentre.x();
        double cy = centre.y()-NewCentre.y();

        m_pSelObject->translate(cx, cy);
        if ( m_pSelObject->m_pContainer != NULL )
        {
            QPointF Centre(m_selObjShape.boundingRect().center().x(),m_selObjShape.boundingRect().center().y());
            QTransform matr;
            matr.translate(Centre.x(), Centre.y());
            matr.rotate(angle+m_selObjAngle);
            matr.translate(-Centre.x(), -Centre.y());
            m_pSelObject->m_pContainer->setMatrix(matr);
        }

        //if (release)
        //    m_pHistory->add(ACT_ROTATE_OBJECT, m_pDoc);

        viewport()->update();
    } /*else if (m_editAction == ACT_ROTATE_IOBJECT)
    {
        QRectF bound = m_dragStartRc;
        double dx = bound.center().x();
        double dy = bound.center().y();

        QPointF centre(dx, dy);
        double angle = -findAngleBetweenVectors(&centre, &m_dragStartPt, &Pt);

        if ( m_pSelObject->m_pContainer != NULL )
        {
            QPointF Centre(m_pSelObject->m_pContainer->getBoundRect().center().x(),m_pSelObject->m_pContainer->getBoundRect().center().y());
            QTransform matr;
            matr.translate(Centre.x(), Centre.y());
            matr.rotate(angle+m_selObjAngle);
            matr.translate(-Centre.x(), -Centre.y());
            m_pSelObject->m_pContainer->setMatrix(matr);
        }

        //if (release)
        //    m_pHistory->add(ACT_ROTATE_IOBJECT, m_pDoc);

        viewport()->update();
    }*/ else if (m_editAction == ACT_MOVE_CONTAINER)
    {
        double dx = Pt.x()-m_dragStartPt.x();
        double dy = Pt.y()-m_dragStartPt.y();
        QPointF pnt(dx,dy);
        QMatrix mat;
        mat.rotate(-m_pSelObject->getAngle());
        mat.scale(1/m_pSelObject->getMatrix().m11(), 1/m_pSelObject->getMatrix().m22());
        pnt = mat.map(pnt);

        m_pSelObject->m_pContainer->translate(pnt.x(),pnt.y());
        m_dragStartPt = Pt;

        //if (release)
        //    m_pHistory->add(ACT_MOVE_CONTAINER, m_pDoc);

        viewport()->update();
    }  else if ((m_editAction == ACT_RESIZE_OBJECT) || (m_editAction == ACT_UPDATE_SIZE))
    {
//        Pt = snapPoint(Pt, m_snap);
        QSizeF psz = page->getPageSize();
        if (Pt.x() < 0)
            Pt.setX(0);
        if (Pt.x() > psz.width())
            Pt.setX(psz.width());
        if (Pt.y() < 0 )
            Pt.setY(0);
        if (Pt.y() > psz.height())
            Pt.setY(psz.height());

        double dx = Pt.x()-m_dragStartPt.x();
        double dy = Pt.y()-m_dragStartPt.y();

        double k = m_dragStartRc.width()/m_dragStartRc.height();

        QRectF bound = m_dragStartRc;

        switch( m_dragTrackerID )
        {
        case TRACKER_SIZE_LT:
            if (m_editAction == ACT_RESIZE_OBJECT)
                dy = dx/k;
            bound.adjust(dx,dy,0,0);
            break;
        case TRACKER_SIZE_RT:
            if (m_editAction == ACT_RESIZE_OBJECT)
                dy = -dx/k;
            bound.adjust(0,dy,dx,0);
            break;
        case TRACKER_SIZE_LB:
            if (m_editAction == ACT_RESIZE_OBJECT)
                dy = -dx/k;
            bound.adjust(dx,0,0,dy);
            break;
        case TRACKER_SIZE_RB:
            if (m_editAction == ACT_RESIZE_OBJECT)
                dy = dx/k;
            bound.adjust(0,0,dx,dy);
            break;
        case TRACKER_SIZE_L:
            bound.adjust(dx,0,0,0);
            break;
        case TRACKER_SIZE_R:
            bound.adjust(0,0,dx,0);
            break;
        case TRACKER_SIZE_B:
            bound.adjust(0,0,0,dy);
            break;
        }

        if ((m_editAction == ACT_RESIZE_OBJECT) &&
                ((m_dragTrackerID==TRACKER_SIZE_LT) || (m_dragTrackerID==TRACKER_SIZE_RT) || (m_dragTrackerID==TRACKER_SIZE_LB) || (m_dragTrackerID==TRACKER_SIZE_RB)))
        {
            bound.moveLeft(snapPoint(bound.topLeft(), m_snap).x());
            bound.setRight(snapPoint(bound.bottomRight(), m_snap).x());
            double nh = bound.width()/k;
            double dh = nh-bound.height();
            if ((m_dragTrackerID==TRACKER_SIZE_LT) || (m_dragTrackerID==TRACKER_SIZE_RT))
                bound.adjust(0,-dh,0,0);
            else
                bound.adjust(0,0,0,dh);
        } else
        {
            bound.moveTopLeft(snapPoint(bound.topLeft(), m_snap));
            bound.setBottomRight(snapPoint(bound.bottomRight(), m_snap));
        }

        bound = CREPageItem::fitToPageR(bound, page->getPageSize(), m_dragTrackerID);

        bool update = true;
        if ((m_editAction == ACT_RESIZE_OBJECT)
            && ((m_dragTrackerID == TRACKER_SIZE_LT)
            || (m_dragTrackerID == TRACKER_SIZE_RT)
            || (m_dragTrackerID == TRACKER_SIZE_LB)
            || (m_dragTrackerID == TRACKER_SIZE_RB)))
        {
            if (cround(bound.width()/bound.height(),5) != cround(k,5))
                update = false;
        }

        if (update)
        {
            if (m_editAction == ACT_UPDATE_SIZE)
            {
                m_pSelObject->setShape(m_selObjShape);
                m_pSelObject->setMatrix(m_selObjMatrix);
                m_pSelObject->fitToRect(bound, true);

                QRectF shbrect = m_pSelObject->getShapeBoundRect();
                double cx = bound.x()-shbrect.x();
                double cy = bound.y()-shbrect.y();
                m_pSelObject->translate(cx, cy);
            } else
            {
                m_pSelObject->fitToRect(bound);
            }
        }

        //if (release)
        //    m_pHistory->add(ACT_RESIZE_OBJECT, m_pDoc);

        viewport()->update();
    }

    if (release)
    {
        m_editAction = ACT_NONE;
        setCursor(Qt::ArrowCursor);
    }
}


void CReportPE::mouseReleaseEvent(QMouseEvent *pEvent)
{
#ifdef Q_WS_MAC
    mouseProcessEvent(pEvent, true);
#else
    if ((pEvent->buttons() & Qt::LeftButton)==0)
        mouseProcessEvent(pEvent, true);
#endif
}


void CReportPE::mouseMoveEvent(QMouseEvent *pEvent)
{
    if (((pEvent->buttons() & Qt::LeftButton)==0) && (m_editAction != ACT_NONE))
        mouseProcessEvent(pEvent, true);
    else
        mouseProcessEvent(pEvent, false);
}

void CReportPE::leaveEvent(QEvent *)
{
    mouseProcessEvent(NULL, true);
}


void CReportPE::keyPressEvent (QKeyEvent *e)
{
    if ((e->key()==Qt::Key_Delete) || (e->key()==Qt::Key_Backspace))
    {
        if (m_editAction == ACT_NONE)
            deleteSelectedItems();
    }
    QAbstractScrollArea::keyPressEvent(e);
}

void CReportPE::deleteSelectedItems()
{
    if (page!=NULL)
    {
        QList<CREPageItem*> poList;
        page->getSelected(poList);

        if (poList.size() > 0)
        {
            //int res = CInfoDlgWin::showAskMessageBox(q->t("Question"), q->t("Are You sure?") + "\r\n" + q->t("Do You want to delete selected item?"), settings, parentWidget()->parentWidget()->parentWidget());
            //int res = QMessageBox::question(0, "Question", "Are You sure?\r\nDo You want to delete selected item?\r\n", tr("&Yes"), tr("&No"), QString::null, 0, 1 );
            int res = CInfoDlgWin::showAskMessageBox(appstyle, "Question", "Are You sure?\r\nDo You want to delete selected item?");
            if (res==0)
            {
                for (int a=0; a<poList.size(); a++)
                {
                    for (int b=0; b<page->m_PageObjects.size(); b++)
                    {
                        if (page->m_PageObjects.at(b)==poList.at(a))
                        {
                            delete page->m_PageObjects.at(b);
                            page->m_PageObjects.remove(b);

                            // Re-Indexing Orders
                            CREPage *cpage = page;
                            QList<CREPageItem*> blks; // List of blocks
                            for (int a=0; a<cpage->m_PageObjects.size(); a++)
                            {
                                CREPageItem *pitem = cpage->m_PageObjects.at(a);
                                if (pitem->m_pContainer!=NULL)
                                {
                                    if (pitem->m_pContainer->m_Type == OT_BLOCK)
                                    {
                                        int blkpos=0;
                                        for (blkpos=0; blkpos<blks.size(); blkpos++)
                                        {
                                            if (blks.at(blkpos)->m_iopt > pitem->m_pContainer->m_iopt)
                                                break;
                                        }
                                        blks.insert(blkpos, pitem->m_pContainer);
                                    }
                                }
                            }
                            for (int blkpos=0; blkpos<blks.size(); blkpos++)
                            {
                                blks.at(blkpos)->m_iopt=blkpos;
                            }

                            break;
                        }
                    }
                }
                //QMouseEvent *pEvent = new QMouseEvent();

                mousePressEvent(NULL);
                viewport()->update();
            }
        }
    }
}


void CReportPE::moveToFrontSelItems()
{
    if (page!=NULL)
    {
        QList<CREPageItem*> poList;
        page->getSelected(poList);

        if (poList.size() > 0)
        {
            for (int a=0; a<poList.size(); a++)
            {
                for (int b=0; b<page->m_PageObjects.size(); b++)
                {
                    if (page->m_PageObjects.at(b)==poList.at(a))
                    {
                        CREPageItem *item = page->m_PageObjects.at(b);
                        page->m_PageObjects.remove(b);
                        page->m_PageObjects.append(item);
                        break;
                    }
                }
            }
            viewport()->update();
        }
    }
}


void CReportPE::moveToBackSelItems()
{
    if (page!=NULL)
    {
        QList<CREPageItem*> poList;
        page->getSelected(poList);

        if (poList.size() > 0)
        {
            for (int a=0; a<poList.size(); a++)
            {
                for (int b=0; b<page->m_PageObjects.size(); b++)
                {
                    if (page->m_PageObjects.at(b)==poList.at(a))
                    {
                        CREPageItem *item = page->m_PageObjects.at(b);
                        page->m_PageObjects.remove(b);
                        page->m_PageObjects.prepend(item);
                        break;
                    }
                }
            }
            viewport()->update();
        }
    }
}
#endif

CREPageItem* CReportPE::addDataItem(QString text, QPoint pos, QSize objSz)
{
    CREPage *pPage = page;

    QTransform Matrix = calcDocViewTransform();
    m_placePoint = Matrix.inverted().map(QPointF(pos.x(), pos.y()));


    CREPageItem *obj = new CREPageItem(numContainer);
    CREPageItem *slot = new CREPageItem(numContainer);
    slot->m_pContainer = obj;
    slot->setType(OT_SLOT);
    slot->m_bSolidContainer = true;
    obj->setType(OT_DATA_FIELD);
    obj->m_pOwner = slot;

    //CTextDlg dlg(m_pStyle, this);
    //if (dlg.exec() == QDialog::Accepted)
    {
        //obj->setText(dlg.GetText());
        //obj->m_Font = dlg.GetFont();
        //obj->m_Align = dlg.GetAlign();
        //obj->setShapeColor(dlg.GetColor());

        obj->setText(text);

        /*QRectF rres;
        QPainter pntr;
        //pntr.begin(data->m_pStorage->img32);
        QImage *img = new QImage(8,8,QImage::Format_ARGB32);
        pntr.begin();
        pntr.resetTransform();
        pntr.setFont(obj->m_Font);
        pntr.drawText(QRectF(0,0,0,0), obj->m_Align, obj->getText(), &rres);
        pntr.end();*/

        //QSizeF objSz(120,25);
        QPainterPath defShape;
        defShape.addRect(0,0, objSz.width(), objSz.height());
        slot->setShape(defShape);
        slot->setBoundSize(defShape.boundingRect().size());
        obj->setBoundSize(defShape.boundingRect().size());

        if (m_placePoint.x()+objSz.width()+1 > pPage->getPageSize().width() )
            m_placePoint.setX(pPage->getPageSize().width() - objSz.width()-1);
        if (m_placePoint.y()+objSz.height()+1 > pPage->getPageSize().height())
            m_placePoint.setY(pPage->getPageSize().height() - objSz.height()-1);

        slot->move(m_placePoint.x(), m_placePoint.y());
        page->moveToPage(slot);
        pPage->addObject(slot);

        //m_pHistory->add(ACT_CHANGE_TEXT, m_pDoc);
        viewport()->update();
    }
    //else
    //{
    //    delete slot;
    //   obj = NULL;
    //}

    return obj;
}


CREPageItem* CReportPE::addTextItem(QString text, QPoint pos)
{
    CREPage *pPage = page;

    QTransform Matrix = calcDocViewTransform();
    m_placePoint = Matrix.inverted().map(QPointF(pos.x(), pos.y()));


    CREPageItem *obj = new CREPageItem(numContainer);
    CREPageItem *slot = new CREPageItem(numContainer);
    slot->m_pContainer = obj;
    slot->setType(OT_SLOT);
    slot->m_bSolidContainer = true;
    obj->setType(OT_TEXT);

    //CTextDlg dlg(m_pStyle, this);
    //if (dlg.exec() == QDialog::Accepted)
    {
        //obj->setText(dlg.GetText());
        //obj->m_Font = dlg.GetFont();
        //obj->m_Align = dlg.GetAlign();
        //obj->setShapeColor(dlg.GetColor());

        obj->m_Font = QFont("Arial", 48);
        obj->setShapeColor(QColor(88, 88, 88, 255));
        obj->setText(text);

        /*QRectF rres;
        QPainter *pntr = new QPainter();
        //pntr.begin(data->m_pStorage->img32);
        QImage *img = new QImage(8,8,QImage::Format_ARGB32);
        pntr->begin(img);
        pntr->resetTransform();
        pntr->setFont(obj->m_Font);
        pntr->drawText(QRectF(0,0,0,0), obj->m_Align, obj->getText(), &rres);
        pntr->end();
        delete pntr;
        delete img;

        //QSizeF objSz(120,20);
        QSizeF objSz(rres.size());*/

		QFontMetrics fm(obj->m_Font);
		QSizeF objSz(fm.boundingRect(obj->getText()).adjusted(0,0,2,2).size());
        QPainterPath defShape;
        defShape.addRect(0,0, objSz.width(), objSz.height());
        slot->setShape(defShape);
        slot->setBoundSize(defShape.boundingRect().size());
        obj->setBoundSize(defShape.boundingRect().size());

        if (m_placePoint.x()+objSz.width()+1 > pPage->getPageSize().width() )
            m_placePoint.setX(pPage->getPageSize().width() - objSz.width()-1);
        if (m_placePoint.y()+objSz.height()+1 > pPage->getPageSize().height())
            m_placePoint.setY(pPage->getPageSize().height() - objSz.height()-1);

        slot->move(m_placePoint.x(), m_placePoint.y());
        page->moveToPage(slot);
        pPage->addObject(slot);

        //m_pHistory->add(ACT_CHANGE_TEXT, m_pDoc);
        viewport()->update();
    }
    //else
    //{
    //    delete slot;
    //   obj = NULL;
    //}

    return obj;
}



CREPageItem* CReportPE::addImageItem(QImage &img, QPoint pos)
{
    if (img.isNull())
        return NULL;

    int maxres = img.size().width();
    if (img.size().height()>maxres)
        maxres = img.size().height();
    if (maxres>1024)
    {
        double sc = (double)maxres/1024.0;
        int nw = img.size().width()/sc;
        int nh = img.size().height()/sc;
        if (nw<1)
            nw = 1;
        if (nh<1)
            nh=1;
        img = img.scaled(nw, nh, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    }

    CREPage *pPage = page;

    QTransform Matrix = calcDocViewTransform();
    m_placePoint = Matrix.inverted().map(QPointF(pos.x(), pos.y()));


    CREPageItem *obj = new CREPageItem(numContainer);
    CREPageItem *slot = new CREPageItem(numContainer);
    slot->m_pContainer = obj;
    slot->setType(OT_SLOT);
    obj->setType(OT_IMAGE);

    QSizeF isz = img.size();
    int ox, oy;
    double scale = calcFitScale(isz.width(), isz.height(), pPage->getPageSize().width()/4, pPage->getPageSize().height()/4, ox, oy);
    int szx = isz.width()*scale;
    int szy = isz.height()*scale;
    if (szx<1)
        szx=1;
    if (szy<1)
        szy=1;
    QSizeF objSz(szx, szy);

    QPainterPath path;
    path.addRect(0, 0, szx, szy);
    slot->setShape(path);
    slot->setBoundSize(objSz);
    slot->m_bSolidContainer = true;

    obj->setBoundSize(slot->getBoundSize());
    obj->m_img = img;


    if (m_placePoint.x()+objSz.width()+1 > pPage->getPageSize().width() )
        m_placePoint.setX(pPage->getPageSize().width() - objSz.width()-1);
    if (m_placePoint.y()+objSz.height()+1 > pPage->getPageSize().height())
        m_placePoint.setY(pPage->getPageSize().height() - objSz.height()-1);

    slot->move(m_placePoint.x(), m_placePoint.y());
    page->moveToPage(slot);
    pPage->addObject(slot);

    //m_pHistory->add(ACT_ADD_OBJECT, m_pDoc);
    viewport()->update();

    return obj;
}


CREPageItem *CReportPE::addBlockItem(CREPage *block, QPoint pos)
{
    if (block==NULL)
        return NULL;

    CREPage *pPage = page;

    QTransform Matrix = calcDocViewTransform();
    m_placePoint = Matrix.inverted().map(QPointF(pos.x(), pos.y()));


    CREPageItem *obj = new CREPageItem(numContainer);
    CREPageItem *slot = new CREPageItem(numContainer);
    slot->m_pContainer = obj;
    slot->setType(OT_SLOT);
    obj->setType(OT_BLOCK);

    int blkcnt=0;
    for (int a=0; a<pPage->m_PageObjects.size(); a++)
        if (pPage->m_PageObjects.at(a)->m_pContainer!=NULL)
            if (pPage->m_PageObjects.at(a)->m_pContainer->m_Type == OT_BLOCK)
                blkcnt++;
    obj->m_iopt = blkcnt;


    QSizeF isz = block->m_PageSize;
    int szx = isz.width(), szy = isz.height();

    if ((szx>=pPage->getPageSize().width()-1) || (szy>=pPage->getPageSize().height()-1))
    {
        int ox, oy;
        double scale = calcFitScale(isz.width(), isz.height(), pPage->getPageSize().width()-1, pPage->getPageSize().height()-1, ox, oy);
        szx = isz.width()*scale;
        szy = isz.height()*scale;
        if (szx<1)
            szx=1;
        if (szy<1)
            szy=1;
    }
    QSizeF objSz(szx, szy);

    QPainterPath path;
    path.addRect(0, 0, szx, szy);
    slot->setShape(path);
    slot->setBoundSize(objSz);
    slot->m_bSolidContainer = true;

    obj->setBoundSize(slot->getBoundSize());
    for (int a=0; a<pblocks.size(); a++)
        if (pblocks.at(a) == block)
            obj->m_Idx = a;

    if (m_placePoint.x()+objSz.width()+1 > pPage->getPageSize().width() )
        m_placePoint.setX(pPage->getPageSize().width() - objSz.width()-1);
    if (m_placePoint.y()+objSz.height()+1 > pPage->getPageSize().height())
        m_placePoint.setY(pPage->getPageSize().height() - objSz.height()-1);

    slot->move(m_placePoint.x(), m_placePoint.y());
    page->moveToPage(slot);
    pPage->addObject(slot);

    //m_pHistory->add(ACT_ADD_OBJECT, m_pDoc);
    viewport()->update();

    return obj;
}



CREPage* CReportPE::addBlock(QSizeF sz, QString name)
{
    CREPage *block = new CREPage(numContainer, style);

    block->setPageSize(sz);
    block->m_PageName = name;

    pblocks.append(block);

    return block;
}


CREPage* CReportPE::addPage(QSizeF sz, QString name)
{
    CREPage *cpage = new CREPage(numContainer, style);

    cpage->setPageSize(sz);
    cpage->m_PageName = name;

    report.at(0)->pages.append(cpage);

    return cpage;
}

void CReportPE::newReport()
{
    clear();

    page = new CREPage(numContainer, style);

    SPReport *cpages = new SPReport();
    cpages->pages.append(page);

    report.append(cpages);

    reportName = "New report";
    m_FileName = "untitled.mcr";
    page->m_PageName = "Untitled";

    m_grid = 1;
    m_snap = 1;
    drawMode->isDrawBorder = 1;
    m_gridX = 10, m_gridY = 10;

    OnZoomNormal();
}

void CReportPE::clear()
{
    page = NULL;
    for (int i=0; i<report.size(); i++)
    {
        for (int n=0; n<report.at(i)->pages.size(); n++)
        {
            delete report.at(i)->pages.at(n);
        }
        report.at(i)->pages.clear();
        delete report.at(i);
    }
    report.clear();

    for (int i=0; i<pblocks.size(); i++)
    {
        delete pblocks.at(i);
    }
    pblocks.clear();
}


void CREPageItem::save(QDataStream &stream)
{
    stream << (qint32)m_Type;

    if (isLabel)
        stream << (qint8)0;
    else
        stream << (qint8)1;

    if (isTextBox)
        stream << (qint8)0;
    else
        stream << (qint8)1;

    stream << name;
    stream << id;
    stream << m_Matrix;
    stream << m_Angle;
    stream << m_BoundSize;

    stream << m_Shape;
    stream << m_ShapeFillColor;
    //stream << (qint32)m_imgID;
    //stream << m_fileLink;

    stream << m_imgSize;
    stream << m_img;

    if (m_bCanResize)
        stream << (qint8)0;
    else
        stream << (qint8)1;

    if (m_bSolidContainer)
        stream << (qint8)0;
    else
        stream << (qint8)1;

    stream << m_Text;
    stream << m_Font;
    stream << (qint64)m_Align;
    stream << (qint32)m_LineType;
    stream << m_LineWidth;
    stream << m_LineColor;
    stream << (qint32)m_Idx;
    stream << (qint32)m_Frame;
    stream << m_FrameColor;
    stream << (qint32)m_Fit;

    // 102
    stream << (qint32)m_iopt;

    // 104
    stream << (qint32)iconNum;

    // 105
    stream << (qint32)barCodes;

	// 107
	stream << m_BackgroundColor;

}

void CREPageItem::load(QDataStream &stream, int ProjectVersion)
{
    qint32 i32;
    qint64 i64;
    qint8 i8;

    stream >> i32;
    m_Type = i32;

    stream >> i8;
    if (i8==0)
        isLabel = true;
    else
        isLabel = false;

    stream >> i8;
    if (i8==0)
        isTextBox = true;
    else
        isTextBox = false;


    stream >> name;
    stream >> id;
    stream >> m_Matrix;
    stream >> m_Angle;
    stream >> m_BoundSize;

    stream >> m_Shape;
    stream >> m_ShapeFillColor;
    //stream << (qint32)m_imgID;
    //stream << m_fileLink;

    stream >> m_imgSize;
    stream >> m_img;

    stream >> i8;
    if (i8==0)
        m_bCanResize = true;
    else
        m_bCanResize = false;

    stream >> i8;
    if (i8==0)
        m_bSolidContainer = true;
    else
        m_bSolidContainer = false;

    stream >> m_Text;

//    if (!id.isEmpty())
//    {
//        for (int in = 0; in < data->doc.docHeader.size(); in++)
//        {
//            if (data->doc.docHeader.at(in).idname == id)
//            {
//                m_Text = data->doc.docHeader.at(in).dname;
//                break;
//            }
//        }
//    }

    stream >> m_Font;
    stream >> i64;
    m_Align = i64;
    stream >> i32;
    m_LineType = i32;
    stream >> m_LineWidth;
    stream >> m_LineColor;
    stream >> i32;
    m_Idx = i32;
    stream >> i32;
    m_Frame = i32;
    stream >> m_FrameColor;
    stream >> i32;
    m_Fit = i32;

    if (ProjectVersion >= REPFILE_VERSION102)
    {
        stream >> i32;
        m_iopt = i32;
    }

    iconNum = 0;
    if (ProjectVersion >= REPFILE_VERSION104)
    {
        stream >> i32;
        iconNum = i32;
    }

    barCodes = 0;
    if (ProjectVersion >= REPFILE_VERSION105)
    {
        stream >> i32;
        barCodes = i32;
    }

	if (ProjectVersion >= REPFILE_VERSION107)
		stream >> m_BackgroundColor;

	if (m_Font.pixelSize()<=0)
		m_Font.setPixelSize((m_Font.pointSize()*12)/10+1);
}


void CREPageItem::drawFTextS(QPainter &painter, int x, int y, QString str)
{
    QString rstr;
    if (str.size()>1)
    {
        for (int a=0; a<str.size(); a++)
        {
            if ((a<str.size()-1) && str.at(a).isDigit() && (str.at(a+1) == str.at(a)))
            {
                bool repr=false;
                if (str.size()>a+2)
                {
                    if (!str.at(a+2).isDigit())
                        repr = true;
                } else
                    repr = true;

                bool repl=false;
                if (a>0)
                {
                    if (!str.at(a-1).isDigit())
                        repl=true;
                } else
                    repl = true;

                if (repl && repr)
                {
                    rstr += QString("<font color=\"#D05030\">") + str.at(a) + str.at(a) + QString("</font>");
                    a++;
                } else
                    rstr += str.at(a);
            } else
                rstr += str.at(a);
        }

    } else
    {
        str.replace("11", "<font color=\"#D05030\">11</font>");
        str.replace("22", "<font color=\"#D05030\">22</font>");
        str.replace("33", "<font color=\"#D05030\">33</font>");
        rstr = str;
    }
    painter.drawStaticText(x, y, "<font color=\"#000000\">" + rstr + "</font>");
}

void CREPageItem::paintMatrInfo(QPainter *painter, QStringList parList, QStringList parListVal, palitra pens)
{
    int wpx=0, wpy=-22;
    int wsx=650;
    painter->save();

    painter->setFont(m_Font);
    QPen pen2(m_ShapeFillColor);
    pen2.setWidth(1);
    painter->setPen(pen2);

    int vstep=16, hstep=125;
    int sepx=0;
    int tposyoff=3;
    int titheight = 22;
    int tposy=titheight+tposyoff;

    painter->fillRect(wpx, wpy+titheight-1, wsx-1, parList.size()*vstep+tposyoff*2, pens.pen4);

    for (int y=0; y<4; y++)
    {
        for (int x=0; x<5; x++)
        {
            painter->drawText( QRectF(wpx+4+x*hstep+sepx, wpy+vstep*(y)+tposy, hstep, vstep), Qt::AlignVCenter | Qt::AlignHCenter, parListVal.at(y*5+x) );
        }
    }

    painter->restore();
}

void CREPageItem::paintCalendarInfo(QPainter *painter, QDate cdate, int m_PersYear, QStringList parList, palitra pens)
{
    int wpx=0, wpy=-22;
    int wsx=650;

    painter->save();

    painter->setFont(m_Font);
    QPen pen2(m_ShapeFillColor);
    pen2.setWidth(1);
    painter->setPen(pen2);

    int vstep=16, hstep=50;
    int sepx=44;
    int tposyoff=3;
    int titheight = 22;
    int tposy=titheight+tposyoff;

    painter->fillRect(wpx, wpy+titheight-1, wsx-1, parList.size()*vstep+tposyoff*2, pens.pen4);

    const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

    int a=0;

    if ( m_PersYear > 0 )
    {

        int m_cyear=cdate.year();
        for (int i=0; i<12; i++)
        {
            int y = m_cyear+i;
            int PersYear = CNumerology::downToDigit(m_PersYear + CNumerology::downToDigit(y));

            painter->drawText( QRectF(wpx+4+i*hstep+sepx, wpy+vstep*a+tposy, hstep + 5, vstep), Qt::AlignVCenter | Qt::AlignHCenter, QString::number(y)+" ("+QString::number(PersYear)+")" );
        }
        a++;

        for (int i=0; i<12; i++)
        {
            QDate cd;
            cd = cdate;
            cd = cd.addMonths(i);
			int m_CPersYear = CNumerology::downToDigit(m_PersYear + CNumerology::downToDigit(cd.year()));
            int c = CNumerology::downToDigit(m_CPersYear + CNumerology::downToDigit(cd.month()));

            QString ms = QString(mname[cd.month()-1]) +" ("+QString::number(c)+")";
            painter->drawText(QRectF(wpx+4+i*hstep+sepx, wpy+vstep*a+tposy, hstep + 5, vstep), Qt::AlignVCenter | Qt::AlignHCenter, ms);
        }
        a++;

        for (int i=0; i<12; i++)
        {
            QDate cd;
            cd = cdate;
            cd = cd.addDays(i);
			int m_CPersYear = CNumerology::downToDigit(m_PersYear + CNumerology::downToDigit(cd.year()));
            int m_CPersMonth = CNumerology::downToDigit(m_CPersYear + CNumerology::downToDigit(cd.month()));
            int c = CNumerology::downToDigit(m_CPersMonth + CNumerology::downToDigit(cd.day()));

            QString ms = QString::number(cd.day()) +" ("+QString::number(c)+")";
            painter->drawText(QRectF(wpx+4+i*hstep+sepx, wpy+vstep*a+tposy, hstep + 5, vstep), Qt::AlignVCenter | Qt::AlignHCenter, ms);
        }



    }

    for (int a=0; a<parList.size(); a++)
        painter->drawText( QRectF(wpx+4, wpy+vstep*a+tposy, sepx-4 + 5, vstep), Qt::AlignVCenter | Qt::AlignLeft, parList.at(a) );

    painter->restore();
}

void CREPageItem::paintInfo(QPainter *painter, QStringList parList, QStringList parListVal, palitra pens)
{
    int wpx=0, wpy=-22;
    int wsx=200;

    painter->save();

    painter->setFont(m_Font);
    QPen pen2(m_ShapeFillColor);
    pen2.setWidth(1);
    painter->setPen(pen2);

    int vstep=16;
    int sepx=100;
    int tposyoff=3;

    int titheight = 22;
    int tposy=titheight+tposyoff;

    painter->fillRect(wpx, wpy+titheight-1, wsx-1, parList.size()*vstep+tposyoff*2, pens.pen5);

    if ( (parList.size()!=0) && (parList.size() == parListVal.size()) )
    {
        for (int a=0; a<parList.size(); a++)
        {
            painter->drawText( QRectF(wpx+4, wpy+vstep*a+tposy, sepx-4, vstep), Qt::AlignVCenter | Qt::AlignLeft, parList.at(a) );
            painter->drawText( QRectF(wpx+4+sepx, wpy+vstep*a+tposy, wsx-8-sepx, vstep), Qt::AlignVCenter | Qt::AlignLeft, parListVal.at(a) );
        }
    }
    painter->restore();
}

void CREPageItem::paintExprInfo(QPainter *painter, QStringList parList, QStringList parListVal, palitra pens)
{
    int wpx=0, wpy=-22;
    int wsx=650;

    painter->save();

    painter->setFont(m_Font);
    QPen pen2(m_ShapeFillColor);
    pen2.setWidth(1);
    painter->setPen(pen2);

    int vstep=16, hstep=130;
    int sepx=117;
    int tposyoff=3;
    int titheight = 22;
    int tposy=titheight+tposyoff;


    painter->fillRect(wpx, wpy+titheight-1, wsx-1, parList.size()*vstep+tposyoff*2, pens.pen4);

    int a=0;


    QStringList hlist;
    hlist.append("Mental");
    hlist.append("Physical");
    hlist.append("Emotional");
    hlist.append("Intuitive");
    for (int i=0; i<4; i++)
        painter->drawText( QRectF(wpx+4+i*hstep+sepx, wpy+vstep*a+tposy, hstep, vstep), Qt::AlignVCenter | Qt::AlignHCenter, hlist.at(i) + " " + parListVal.at(3*5+i) );
    a++;

    for (int y=0; y<3; y++)
    {
        for (int x=0; x<4; x++)
        {
            painter->drawText( QRectF(wpx+4+x*hstep+sepx, wpy+vstep*(y+1)+tposy, hstep, vstep), Qt::AlignVCenter | Qt::AlignHCenter, parListVal.at(y*5+x) );
        }
    }

    for (int a=1; a<parList.size(); a++)
        painter->drawText( QRectF(wpx+4, wpy+vstep*a+tposy, sepx-4, vstep), Qt::AlignVCenter | Qt::AlignLeft, parList.at(a)+ " " + parListVal.at((a-1)*5+4) );

    painter->restore();
}

void CREPageItem::paintNameInfo(QPainter *painter, QStringList parList, QStringList parListVal, palitra pens)
{
    int wpx=0, wpy=-22;
    int wsx=650;

    painter->save();

    painter->setFont(m_Font);
    QPen pen2(m_ShapeFillColor);

    pen2.setWidth(1);
    painter->setPen(pen2);

    int vstep=16;
    int sepx=80;
    int tposyoff=3;
    int titheight = 22;
    int tposy=titheight+tposyoff;

    painter->fillRect(wpx, wpy+titheight-1, wsx-1, parList.size()*vstep+tposyoff*2, pens.pen4);

    QPen pen1(pens.pen1);
    pen1.setWidth(1);

    for (int a=0; a<parList.size(); a++)
    {
        for (int i=0; i<parList.at(a).size(); i++)
        {
            QChar sym;
            if (a<3)
                sym = parList.at(1).at(i);
            else
                sym = parList.at(4).at(i);
            sym = sym.toUpper();
            char s = sym.toAscii();

            painter->setPen(pen2);
            if (((s == 'K') || (s == 'V')) && (a!=1) && (a!=4) && (parList.at(a).at(i) != ' ') && !m_GlobSettings.calcNameDD)
            {
                QString r = "1\n1";
                if (s == 'V')
                    r = "2\n2";
                painter->drawText( QRectF(wpx+4+i*9, wpy+vstep*a+tposy, 9, vstep), Qt::AlignVCenter | Qt::AlignLeft, r );
            } else
            {
                painter->drawText( QRectF(wpx+4+i*9, wpy+vstep*a+tposy, 9, vstep), Qt::AlignVCenter | Qt::AlignLeft, parList.at(a).at(i) );
            }
        }
        painter->setPen(pen2);
        sepx = parList.at(a).size()*9+9;
        drawFTextS(*painter, wpx+4+sepx, wpy+vstep*a+tposy, parListVal.at(a));

        if ((a%3 == 0) && (a>0))
        {
            painter->setPen(pen1);
            painter->drawLine(wpx,wpy+vstep*a+tposy, wpx+wsx-1, wpy+vstep*a+tposy);
        }
    }
    painter->restore();
}


void CREPage::save(QDataStream &stream)
{
    stream << (double)m_PageSize.width();
    stream << (double)m_PageSize.height();

    //102
    stream << m_PageName;
    stream << (qint32)m_PageSeq;
    stream << (qint32)m_PageGenOptions;

    //103
    stream << ground.color;
    stream << ground.drawMode;
    stream << ground.image;

    qint32 poNum = m_PageObjects.size();
    stream << poNum;
    for (int a=0; a<poNum; a++)
    {
        m_PageObjects.at(a)->save(stream);
        if (m_PageObjects.at(a)->m_pContainer==NULL)
        {
            stream << (qint8)0;
        } else
        {
            stream << (qint8)1;
            m_PageObjects.at(a)->m_pContainer->save(stream);
        }
    }

}

void CREPage::load(QDataStream &stream, int ProjectVersion)
{
    qint32 i32;
    double psx=100.0, psy=100.0;
    stream >> psx;
    stream >> psy;
    m_PageSize = QSizeF(psx, psy);

    if (ProjectVersion >= REPFILE_VERSION102)
    {
        stream >> m_PageName;
        stream >> i32;
        m_PageSeq = i32;
        stream >> i32;
        m_PageGenOptions = i32;
    }

    if (ProjectVersion >= REPFILE_VERSION103)
    {
        stream >> ground.color;
        stream >> ground.drawMode;
        stream >> ground.image;
    }

    qint32 poNum;
    stream >> poNum;
    for (int a=0; a<poNum; a++)
    {
        CREPageItem *item = new CREPageItem(numContainer);
        m_PageObjects.append(item);
        m_PageObjects.at(a)->load(stream, ProjectVersion);
        qint8 i8;
        stream >> i8;
        if (i8==1)
        {
            CREPageItem *itemC = new CREPageItem(numContainer);
            itemC->load(stream, ProjectVersion);
            item->m_pContainer = itemC;
            itemC->m_pOwner = item;
        }
    }
}


int CReportPE::save(QDataStream &stream)
{
    if (report.size()==0)
        return -1;
    if (report.at(0)->pages.size()==0)
        return -1;

    report.at(0)->pages.at(0)->generateThumbnail(true);

    char file_header[] = DEFREPFILEHEADER;
    qint32 ProjectVersion = REPFILE_VERSION;

    stream << file_header;
    stream << ProjectVersion;

    stream << reportName;
    stream << (qint32) 1;
    stream << (*report.at(0)->pages.at(0)->getThumbnail());
    report.at(0)->pages.at(0)->generateThumbnail();

    stream << (qint32) m_grid;
    stream << (qint32) m_snap;
    stream << (qint32) drawMode->isDrawBorder;
    stream << m_gridX;
    stream << m_gridY;

    // Calc block index
    for (int a=0; a<report.size(); a++)
    {
        qint32 pNum = report.at(a)->pages.size();
        for (int n=0; n<pNum; n++)
        {
            for (int i=0; i<report.at(a)->pages.at(n)->m_PageObjects.size(); i++)
            {
                CREPageItem *pitem = report.at(a)->pages.at(n)->m_PageObjects.at(i);
                if (pitem->m_pContainer!=NULL)
                    if (pitem->m_pContainer->m_Type == OT_BLOCK)
                    {
                        for (int pi=0; pi<pblocks.size(); pi++)
                        {
                            if (pitem->m_pContainer->m_pData == pblocks.at(pi))
                            {
                                pitem->m_pContainer->m_Idx = pi;
                                break;
                            }
                        }
                    }
            }
        }
    }

    // 102
    qint32 blkNum = pblocks.size();
    stream << blkNum;
    for (int a=0; a<blkNum; a++)
        pblocks.at(a)->save(stream);


    qint32 repNum = report.size();
    stream << repNum;
    for (int a=0; a<repNum; a++)
    {
        qint32 pNum = report.at(a)->pages.size();
        stream << pNum;
        for (int n=0; n<pNum; n++)
        {
            report.at(a)->pages.at(n)->save(stream);
        }
    }

    return 0;
}


int CReportPE::load(QDataStream &stream)
{

    page = NULL;

    char buf[32];

    char file_header[] = DEFREPFILEHEADER;

    qint32 ProjectVersion = 0;
    qint8 i8;
    qint32 i32;

    stream >> i32;

    for (unsigned int a=0; a<strlen(file_header)+1; a++ )
    {
        stream >> i8;
        buf[a] = i8;
    }
    buf[31] = 0;
    if (strcmp(file_header, buf) != 0)
    {
        return -1;
    }

    stream >> ProjectVersion;
    if (ProjectVersion <= REPFILE_VERSION)
    {

        clear();

        stream >> reportName;

        qint32 previewNum;
        stream >> previewNum;
        QImage prevImg;
        stream >> prevImg;

        if (ProjectVersion>=REPFILE_VERSION101)
        {
            stream >> i32;
            m_grid = i32;
            stream >> i32;
            m_snap = i32;
            if (ProjectVersion >= REPFILE_VERSION106)
            {
                stream >> i32;
                drawMode->isDrawBorder = i32;
            }
            stream >> m_gridX;
            stream >> m_gridY;
        }

        if (ProjectVersion >= REPFILE_VERSION102)
        {
            qint32 blkNum;
            stream >> blkNum;
            for (int a=0; a<blkNum; a++)
            {
                CREPage *cpage = new CREPage(numContainer, style);
                pblocks.append(cpage);
                pblocks.at(a)->load(stream, ProjectVersion);
                pblocks.at(a)->generateThumbnail();
            }
        }

        qint32 repNum;
        stream >> repNum;
        for (int a=0; a<repNum; a++)
        {
            SPReport *cpages = new SPReport();
            report.append(cpages);

            qint32 pNum;
            stream >> pNum;
            for (int n=0; n<pNum; n++)
            {
                CREPage *cpage = new CREPage(numContainer, style);
                report.at(a)->pages.append(cpage);

                report.at(a)->pages.at(n)->load(stream, ProjectVersion);
            }
        }

        // Linking blocks, generating thumbnails
        for (int a=0; a<repNum; a++)
        {

            qint32 pNum = report.at(a)->pages.size();

            for (int n=0; n<pNum; n++)
            {
                for (int i=0; i<report.at(a)->pages.at(n)->m_PageObjects.size(); i++)
                {
                    CREPageItem *pitem = report.at(a)->pages.at(n)->m_PageObjects.at(i);

                    if (pitem->m_pContainer!=NULL)
                        if (pitem->m_pContainer->m_Type == OT_BLOCK)
                        {
                            QString pid = QString::number((qulonglong)pblocks.at(pitem->m_pContainer->m_Idx));
                            pitem->m_pContainer->id = pid;
                            pitem->m_pContainer->m_pData = pblocks.at(pitem->m_pContainer->m_Idx);
                            pitem->m_pContainer->m_pImg = pblocks.at(pitem->m_pContainer->m_Idx)->getThumbnail();
                        }
                }

                report.at(a)->pages.at(n)->generateThumbnail();


            }
        }

        if ((report.size()==0) || (report.at(0)->pages.size()==0))
            newReport();
           else
            page = report.at(0)->pages.at(0);

        if (ProjectVersion < REPFILE_VERSION102)
            page->m_PageName = "Pages";

        OnZoomNormal();
    } else
    {
        newReport();
        return -1;
    }

    return 0;
}

