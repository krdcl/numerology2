#ifndef THUMBAILSETTINGS_H
#define THUMBAILSETTINGS_H

#define PIX_MM 1.334
#define PIX_MM_V2 1.27
#define PIX_MM_V3 1.18
#define ASPECT_RATIO 1.3333

//#include "datafile.h"
#include "reportcreator.h"
#include "appstyle.h"
//#include "dbTypes.h"

#ifndef VER_MOBILE
struct itemView
{
    QListView *pThumbview;
    QStringList bottom;
    QStringList right;
    QImage icon;
    int iconSize;
    bool hidden;
    int *itemSizeH;
    int *itemSizeW;
    double *scale;
    QList <QImage> *listIcon;
    int id;
    int idx;
    //CDataFile *data;
    CReportCreatorWin *dlg;
    bool selected;
    itemView()
    {
        //data = 0;
        dlg = 0;
        hidden = false;
        selected = false;
        pThumbview = 0;
    }
};

Q_DECLARE_METATYPE(itemView *)

struct itemViewParam
{
    QList<quint32> bottomId;
    QList<quint32> rightId;
    quint32 iconSize;
    quint32 itemSize;
};

#endif


struct reourtParamSet
{
    int imgNum;
    Qt::AlignmentFlag align;

    reourtParamSet()
    {
        imgNum = -1;
        align = Qt::AlignLeft;
    }
};

class CManagerThumbnailDlg
{
public:

    static QImage slotPrintReport(int id, CReportCreatorWin *dlg, QSize scale = QSize(0, 0));
    static bool preparePrintReportBlock(CREPage *block);
};

#endif // THUMBAILSETTINGS_H
