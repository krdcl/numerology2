﻿#ifndef BARCODE_1D_H
#define BARCODE_1D_H
/*****************************************************************/
/* ANSI C Functions for IDAutomation Barcode Fonts               */
/* © Copyright 2002- 2007, IDAutomation.com, Inc.                */
/* All rights reserved.                                          */
/* Redistribution and use of this code in source and/or binary   */
/* forms, with or without modification, are permitted provided   */
/* that: (1) all copies of the source code retain the above      */
/* unmodified copyright notice and this entire unmodified        */
/* section of text, (2) You or Your organization owns a valid    */
/* Developer License to this product from IDAutomation.com       */
/* and, (3) when any portion of this code is bundled in any      */
/* form with an application, a valid notice must be provided     */
/* within the user documentation, start-up screen or in the      */
/* help-about section of the application that specifies          */
/* IDAutomation.com as the provider of the Software bundled      */
/* with the application.                                         */
/*****************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <QImage>
#include <QPainter>

static QColor barCodeColor = Qt::black;

struct symbolToEAN
{
    unsigned char symbol;
    unsigned char width;
    unsigned char line_0;
    unsigned char line_1;
    unsigned char line_2;
    unsigned char line_3;
    unsigned char line_4;
    unsigned char line_5;
    unsigned char line_6;
    char numberShow;

    symbolToEAN()
    {
        symbol = 0;
        width = 0;
        line_0 = 0;
        line_1 = 0;
        line_2 = 0;
        line_3 = 0;
        line_4 = 0;
        line_5 = 0;
        line_6 = 0;
        numberShow = -1;
    }
};

struct symbolToUNI
{
    unsigned char symbol;
    unsigned char width;
    unsigned char line_0;
    unsigned char line_1;
    unsigned char line_2;
    unsigned char line_3;
    unsigned char line_4;
    unsigned char line_5;
    unsigned char line_6;
    unsigned char line_7;
    unsigned char line_8;
    unsigned char line_9;
    unsigned char line_10;
    unsigned char line_11;
    char numberShow;

    symbolToUNI()
    {
        symbol = 0;
        width = 0;
        line_0 = 0;
        line_1 = 0;
        line_2 = 0;
        line_3 = 0;
        line_4 = 0;
        line_5 = 0;
        line_6 = 0;
        line_7 = 0;
        line_8 = 0;
        line_9 = 0;
        line_10 = 0;
        line_11 = 0;
        numberShow = -1;
    }
};

struct symbolToPostNet
{
    unsigned char symbol;
    unsigned char width;
    unsigned char line_0;
    unsigned char line_1;
    unsigned char line_2;
    unsigned char line_3;
    unsigned char line_4;
    char numberShow;

    symbolToPostNet()
    {
        symbol = 0;
        width = 0;
        line_0 = 0;
        line_1 = 0;
        line_2 = 0;
        line_3 = 0;
        line_4 = 0;
        numberShow = -1;
    }
};

struct symbolTo128Code
{
    unsigned char symbol;
    unsigned char width;
    unsigned char line_0;
    unsigned char line_1;
    unsigned char line_2;
    unsigned char line_3;
    unsigned char line_4;
    unsigned char line_5;
    unsigned char line_6;
    unsigned char line_7;
    unsigned char line_8;
    unsigned char line_9;
    unsigned char line_10;
    unsigned char line_11;
    unsigned char line_12;
    unsigned char symbolShow;

    symbolTo128Code()
    {
        symbol = 0;
        width = 0;
        line_0 = 0;
        line_1 = 0;
        line_2 = 0;
        line_3 = 0;
        line_4 = 0;
        line_5 = 0;
        line_6 = 0;
        line_7 = 0;
        line_8 = 0;
        line_9 = 0;
        line_10 = 0;
        line_11 = 0;
        line_12 = 0;
        symbolShow = 0;
    }
};

struct symbolTo93Code
{
    unsigned char symbol;
    unsigned char width;
    unsigned char line_0;
    unsigned char line_1;
    unsigned char line_2;
    unsigned char line_3;
    unsigned char line_4;
    unsigned char line_5;
    unsigned char line_6;
    unsigned char line_7;
    unsigned char line_8;
    unsigned char line_9;
    char symbolShow;

    symbolTo93Code()
    {
        symbol = 0;
        width = 0;
        line_0 = 0;
        line_1 = 0;
        line_2 = 0;
        line_3 = 0;
        line_4 = 0;
        line_5 = 0;
        line_6 = 0;
        line_7 = 0;
        line_8 = 0;
        line_9 = 0;
        symbolShow = 0;
    }
};

struct symbolUniversal
{
    unsigned char symbol;
    unsigned char width;
    QList <unsigned char> lines;
    char symbolShow;

    symbolUniversal()
    {
        symbol = 0;
        width = 0;
        lines.clear();
        symbolShow = 0;
    }
};

static QList <symbolToEAN> symbUPCEANList;
static QList <symbolToUNI> symbUNIList;
static QList <symbolToPostNet> symbPostNetList;
static QList <symbolTo128Code> symbolTo128CodeList;
static QList <symbolTo93Code> symbolTo93CodeList;
static QList <symbolUniversal> symbolToCodabarList;
static QList <symbolUniversal> symbolTo39CodeList;


void initializeSymbolMap()
{
    symbUPCEANList.clear();
    symbUNIList.clear();
    symbPostNetList.clear();
    symbolTo128CodeList.clear();
    symbolTo93CodeList.clear();
    symbolToCodabarList.clear();
    symbolTo39CodeList.clear();
    symbolToEAN symbUPCEAN;
    symbolToUNI symbUNI;
    symbolToPostNet symbPostNet;
    symbolTo128Code symb128Code;
    symbolTo93Code symb93Code;
    symbolUniversal symbCodabar;
    symbolUniversal symb39Code;

    symbUPCEAN.symbol = 0x20; //" "
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -1;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x21; //"!"
    symbUPCEAN.width = 2;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -1;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x22; //"\""
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 0;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x28; //"("
    symbUPCEAN.width = 3;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x29; //")"
    symbUPCEAN.width = 6;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -1;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x2A; //"*"
    symbUPCEAN.width = 5;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x2B; //"+"
    symbUPCEAN.width = 4;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 4;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -1;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x30; //"0"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 0;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x31; //"1"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 1;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x32; //"2"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x33; //"3"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 3;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x34; //"4"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 4;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x35; //"5"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 5;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x36; //"6"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 6;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x37; //"7"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 7;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x38; //"8"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 8;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x39; //"9"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 9;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x41; //"A"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 0;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x42; //"B"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 1;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x43; //"C"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x44; //"D"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 3;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x44; //"D"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 3;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x45; //"E"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 4;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x46; //"F"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 5;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x47; //"G"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 6;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x48; //"H"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 7;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x49; //"I"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 8;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x4A; //"J"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = 9;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x4B; //"K"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 0;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x4C; //"L"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 1;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x4D; //"M"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x4E; //"N"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 3;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x4F; //"O"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 4;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x50; //"P"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 5;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x51; //"Q"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 6;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x52; //"R"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 7;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x53; //"S"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 8;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x54; //"T"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 9;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x55; //"U"
    symbUPCEAN.width = 13;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 0;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x56; //"V"
    symbUPCEAN.width = 13;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 1;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x57; //"W"
    symbUPCEAN.width = 13;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x58; //"X"
    symbUPCEAN.width = 13;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 3;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x59; //"Y"
    symbUPCEAN.width = 13;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 4;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x75; //"u"
    symbUPCEAN.width = 13;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 5;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x76; //"v"
    symbUPCEAN.width = 13;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 6;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x77; //"w"
    symbUPCEAN.width = 13;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 7;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x78; //"x"
    symbUPCEAN.width = 13;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 8;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x79; //"y"
    symbUPCEAN.width = 13;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = 9;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x61; //"a"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x62; //"b"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x63; //"c"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x64; //"d"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x65; //"e"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x66; //"f"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x67; //"g"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x68; //"h"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x69; //"i"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x6A; //"j"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 0;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 1;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x6B; //"k"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x6C; //"l"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x6D; //"m"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x6E; //"n"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x6F; //"o"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x70; //"p"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 1;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x71; //"q"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x72; //"r"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x73; //"s"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 0;
    symbUPCEAN.line_2 = 0;
    symbUPCEAN.line_3 = 1;
    symbUPCEAN.line_4 = 0;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    symbUPCEAN.symbol = 0x74; //"t"
    symbUPCEAN.width = 7;
    symbUPCEAN.line_0 = 1;
    symbUPCEAN.line_1 = 1;
    symbUPCEAN.line_2 = 1;
    symbUPCEAN.line_3 = 0;
    symbUPCEAN.line_4 = 1;
    symbUPCEAN.line_5 = 0;
    symbUPCEAN.line_6 = 0;
    symbUPCEAN.numberShow = -2;
    symbUPCEANList.append(symbUPCEAN);

    //---------------UNI INI------------------

    symbUNI.symbol = 0x28; //"("
    symbUNI.width = 3;
    symbUNI.line_0 = 1;
    symbUNI.line_1 = 1;
    symbUNI.line_2 = 0;
    symbUNI.line_3 = 0;
    symbUNI.line_4 = 0;
    symbUNI.line_5 = 0;
    symbUNI.line_6 = 0;
    symbUNI.line_7 = 0;
    symbUNI.line_8 = 0;
    symbUNI.line_9 = 0;
    symbUNI.line_10 = 0;
    symbUNI.line_11 = 0;
    symbUNI.numberShow = -1;
    symbUNIList.append(symbUNI);

    symbUNI.symbol = 0x30; //"0"
    symbUNI.width = 12;
    symbUNI.line_0 = 1;
    symbUNI.line_1 = 0;
    symbUNI.line_2 = 0;
    symbUNI.line_3 = 1;
    symbUNI.line_4 = 0;
    symbUNI.line_5 = 0;
    symbUNI.line_6 = 1;
    symbUNI.line_7 = 0;
    symbUNI.line_8 = 0;
    symbUNI.line_9 = 1;
    symbUNI.line_10 = 0;
    symbUNI.line_11 = 0;
    symbUNI.numberShow = 0;
    symbUNIList.append(symbUNI);

    symbUNI.symbol = 0x31; //"1"
    symbUNI.width = 12;
    symbUNI.line_0 = 1;
    symbUNI.line_1 = 0;
    symbUNI.line_2 = 0;
    symbUNI.line_3 = 1;
    symbUNI.line_4 = 0;
    symbUNI.line_5 = 0;
    symbUNI.line_6 = 1;
    symbUNI.line_7 = 0;
    symbUNI.line_8 = 0;
    symbUNI.line_9 = 1;
    symbUNI.line_10 = 1;
    symbUNI.line_11 = 0;
    symbUNI.numberShow = 1;
    symbUNIList.append(symbUNI);

    symbUNI.symbol = 0x32; //"2"
    symbUNI.width = 12;
    symbUNI.line_0 = 1;
    symbUNI.line_1 = 0;
    symbUNI.line_2 = 0;
    symbUNI.line_3 = 1;
    symbUNI.line_4 = 0;
    symbUNI.line_5 = 0;
    symbUNI.line_6 = 1;
    symbUNI.line_7 = 1;
    symbUNI.line_8 = 0;
    symbUNI.line_9 = 1;
    symbUNI.line_10 = 0;
    symbUNI.line_11 = 0;
    symbUNI.numberShow = 2;
    symbUNIList.append(symbUNI);

    symbUNI.symbol = 0x33; //"3"
    symbUNI.width = 12;
    symbUNI.line_0 = 1;
    symbUNI.line_1 = 0;
    symbUNI.line_2 = 0;
    symbUNI.line_3 = 1;
    symbUNI.line_4 = 0;
    symbUNI.line_5 = 0;
    symbUNI.line_6 = 1;
    symbUNI.line_7 = 1;
    symbUNI.line_8 = 0;
    symbUNI.line_9 = 1;
    symbUNI.line_10 = 1;
    symbUNI.line_11 = 0;
    symbUNI.numberShow = 3;
    symbUNIList.append(symbUNI);

    symbUNI.symbol = 0x34; //"4"
    symbUNI.width = 12;
    symbUNI.line_0 = 1;
    symbUNI.line_1 = 0;
    symbUNI.line_2 = 0;
    symbUNI.line_3 = 1;
    symbUNI.line_4 = 1;
    symbUNI.line_5 = 0;
    symbUNI.line_6 = 1;
    symbUNI.line_7 = 0;
    symbUNI.line_8 = 0;
    symbUNI.line_9 = 1;
    symbUNI.line_10 = 0;
    symbUNI.line_11 = 0;
    symbUNI.numberShow = 4;
    symbUNIList.append(symbUNI);

    symbUNI.symbol = 0x35; //"5"
    symbUNI.width = 12;
    symbUNI.line_0 = 1;
    symbUNI.line_1 = 0;
    symbUNI.line_2 = 0;
    symbUNI.line_3 = 1;
    symbUNI.line_4 = 1;
    symbUNI.line_5 = 0;
    symbUNI.line_6 = 1;
    symbUNI.line_7 = 0;
    symbUNI.line_8 = 0;
    symbUNI.line_9 = 1;
    symbUNI.line_10 = 1;
    symbUNI.line_11 = 0;
    symbUNI.numberShow = 5;
    symbUNIList.append(symbUNI);

    symbUNI.symbol = 0x36; //"6"
    symbUNI.width = 12;
    symbUNI.line_0 = 1;
    symbUNI.line_1 = 0;
    symbUNI.line_2 = 0;
    symbUNI.line_3 = 1;
    symbUNI.line_4 = 1;
    symbUNI.line_5 = 0;
    symbUNI.line_6 = 1;
    symbUNI.line_7 = 1;
    symbUNI.line_8 = 0;
    symbUNI.line_9 = 1;
    symbUNI.line_10 = 0;
    symbUNI.line_11 = 0;
    symbUNI.numberShow = 6;
    symbUNIList.append(symbUNI);

    symbUNI.symbol = 0x37; //"7"
    symbUNI.width = 12;
    symbUNI.line_0 = 1;
    symbUNI.line_1 = 0;
    symbUNI.line_2 = 0;
    symbUNI.line_3 = 1;
    symbUNI.line_4 = 1;
    symbUNI.line_5 = 0;
    symbUNI.line_6 = 1;
    symbUNI.line_7 = 1;
    symbUNI.line_8 = 0;
    symbUNI.line_9 = 1;
    symbUNI.line_10 = 1;
    symbUNI.line_11 = 0;
    symbUNI.numberShow = 7;
    symbUNIList.append(symbUNI);

    symbUNI.symbol = 0x38; //"8"
    symbUNI.width = 12;
    symbUNI.line_0 = 1;
    symbUNI.line_1 = 1;
    symbUNI.line_2 = 0;
    symbUNI.line_3 = 1;
    symbUNI.line_4 = 0;
    symbUNI.line_5 = 0;
    symbUNI.line_6 = 1;
    symbUNI.line_7 = 0;
    symbUNI.line_8 = 0;
    symbUNI.line_9 = 1;
    symbUNI.line_10 = 0;
    symbUNI.line_11 = 0;
    symbUNI.numberShow = 8;
    symbUNIList.append(symbUNI);

    symbUNI.symbol = 0x39; //"9"
    symbUNI.width = 12;
    symbUNI.line_0 = 1;
    symbUNI.line_1 = 1;
    symbUNI.line_2 = 0;
    symbUNI.line_3 = 1;
    symbUNI.line_4 = 0;
    symbUNI.line_5 = 0;
    symbUNI.line_6 = 1;
    symbUNI.line_7 = 0;
    symbUNI.line_8 = 0;
    symbUNI.line_9 = 1;
    symbUNI.line_10 = 1;
    symbUNI.line_11 = 0;
    symbUNI.numberShow = 9;
    symbUNIList.append(symbUNI);

    symbUNI.symbol = 0x29; //")"
    symbUNI.width = 4;
    symbUNI.line_0 = 1;
    symbUNI.line_1 = 0;
    symbUNI.line_2 = 0;
    symbUNI.line_3 = 1;
    symbUNI.line_4 = 0;
    symbUNI.line_5 = 0;
    symbUNI.line_6 = 0;
    symbUNI.line_7 = 0;
    symbUNI.line_8 = 0;
    symbUNI.line_9 = 0;
    symbUNI.line_10 = 0;
    symbUNI.line_11 = 0;
    symbUNI.numberShow = -1;
    symbUNIList.append(symbUNI);

    //---------PostNet--------

    symbPostNet.symbol = 0x28; //"("
    symbPostNet.width = 1;
    symbPostNet.line_0 = 1;
    symbPostNet.line_1 = 0;
    symbPostNet.line_2 = 0;
    symbPostNet.line_3 = 0;
    symbPostNet.line_4 = 0;
    symbPostNet.numberShow = -1;
    symbPostNetList.append(symbPostNet);

    symbPostNet.symbol = 0x30; //"0"
    symbPostNet.width = 5;
    symbPostNet.line_0 = 1;
    symbPostNet.line_1 = 1;
    symbPostNet.line_2 = 0;
    symbPostNet.line_3 = 0;
    symbPostNet.line_4 = 0;
    symbPostNet.numberShow = -1;
    symbPostNetList.append(symbPostNet);

    symbPostNet.symbol = 0x31; //"1"
    symbPostNet.width = 5;
    symbPostNet.line_0 = 0;
    symbPostNet.line_1 = 0;
    symbPostNet.line_2 = 0;
    symbPostNet.line_3 = 1;
    symbPostNet.line_4 = 1;
    symbPostNet.numberShow = -1;
    symbPostNetList.append(symbPostNet);

    symbPostNet.symbol = 0x32; //"2"
    symbPostNet.width = 5;
    symbPostNet.line_0 = 0;
    symbPostNet.line_1 = 0;
    symbPostNet.line_2 = 1;
    symbPostNet.line_3 = 0;
    symbPostNet.line_4 = 1;
    symbPostNet.numberShow = -1;
    symbPostNetList.append(symbPostNet);

    symbPostNet.symbol = 0x33; //"3"
    symbPostNet.width = 5;
    symbPostNet.line_0 = 0;
    symbPostNet.line_1 = 0;
    symbPostNet.line_2 = 1;
    symbPostNet.line_3 = 1;
    symbPostNet.line_4 = 0;
    symbPostNet.numberShow = -1;
    symbPostNetList.append(symbPostNet);

    symbPostNet.symbol = 0x34; //"4"
    symbPostNet.width = 5;
    symbPostNet.line_0 = 0;
    symbPostNet.line_1 = 1;
    symbPostNet.line_2 = 0;
    symbPostNet.line_3 = 0;
    symbPostNet.line_4 = 1;
    symbPostNet.numberShow = -1;
    symbPostNetList.append(symbPostNet);

    symbPostNet.symbol = 0x35; //"5"
    symbPostNet.width = 5;
    symbPostNet.line_0 = 0;
    symbPostNet.line_1 = 1;
    symbPostNet.line_2 = 0;
    symbPostNet.line_3 = 1;
    symbPostNet.line_4 = 0;
    symbPostNet.numberShow = -1;
    symbPostNetList.append(symbPostNet);

    symbPostNet.symbol = 0x36; //"6"
    symbPostNet.width = 5;
    symbPostNet.line_0 = 0;
    symbPostNet.line_1 = 1;
    symbPostNet.line_2 = 1;
    symbPostNet.line_3 = 0;
    symbPostNet.line_4 = 0;
    symbPostNet.numberShow = -1;
    symbPostNetList.append(symbPostNet);

    symbPostNet.symbol = 0x37; //"7"
    symbPostNet.width = 5;
    symbPostNet.line_0 = 1;
    symbPostNet.line_1 = 0;
    symbPostNet.line_2 = 0;
    symbPostNet.line_3 = 0;
    symbPostNet.line_4 = 1;
    symbPostNet.numberShow = -1;
    symbPostNetList.append(symbPostNet);

    symbPostNet.symbol = 0x38; //"8"
    symbPostNet.width = 5;
    symbPostNet.line_0 = 1;
    symbPostNet.line_1 = 0;
    symbPostNet.line_2 = 0;
    symbPostNet.line_3 = 1;
    symbPostNet.line_4 = 0;
    symbPostNet.numberShow = -1;
    symbPostNetList.append(symbPostNet);

    symbPostNet.symbol = 0x39; //"9"
    symbPostNet.width = 5;
    symbPostNet.line_0 = 1;
    symbPostNet.line_1 = 0;
    symbPostNet.line_2 = 1;
    symbPostNet.line_3 = 0;
    symbPostNet.line_4 = 0;
    symbPostNet.numberShow = -1;
    symbPostNetList.append(symbPostNet);

    symbPostNet.symbol = 0x29; //")"
    symbPostNet.width = 1;
    symbPostNet.line_0 = 1;
    symbPostNet.line_1 = 0;
    symbPostNet.line_2 = 0;
    symbPostNet.line_3 = 0;
    symbPostNet.line_4 = 0;
    symbPostNet.numberShow = -1;
    symbPostNetList.append(symbPostNet);

    //---------128 Code-------

    symb128Code.symbol = 0x20; //"Space or Ô / ü"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = ' ';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xD4; //"Space or Ô / ü"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xD4;//'Ô';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xFC; //"Space or Ô / ü"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xFC;//'ü';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xC2; //"Space or Ô / ü"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = ' ';//'ü';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x21; //"!"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '!';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x22; //"""
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '"';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x23; //"#"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '#';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x24; //"$"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '$';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x25; //"%"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '%';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x26; //"&"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '&';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x27; //"'"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '\'';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x28; //"("
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '(';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x29; //")"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = ')';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x2A; //"*"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '*';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x2B; //"+"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '+';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x2C; //","
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = ',';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x2D; //"-"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '-';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x2E; //"."
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '.';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x2F; //"/"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '/';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x30; //"0"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '0';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x31; //"1"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '1';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x32; //"2"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '2';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x33; //"3"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '3';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x34; //"4"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '4';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x35; //"5"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '5';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x36; //"6"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '6';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x37; //"7"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '7';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x38; //"8"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '8';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x39; //"9"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '9';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x3A; //":"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = ':';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x3B; //";"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = ';';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x3C; //"<"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '<';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x3D; //"="
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '=';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x3E; //">"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '>';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x3F; //"?"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '?';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x40; //"@"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '@';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x41; //"A"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'A';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x42; //"B"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'B';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x43; //"C"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'C';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x44; //"D"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'D';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x45; //"E"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'E';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x46; //"F"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'F';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x47; //"G"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'G';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x48; //"H"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'H';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x49; //"I"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'I';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x4A; //"J"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'J';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x4B; //"K"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'K';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x4C; //"L"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'L';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x4D; //"M"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'M';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x4E; //"N"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'N';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x4F; //"O"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'O';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x50; //"P"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'P';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x51; //"Q"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'Q';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x52; //"R"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'R';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x53; //"S"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'S';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x54; //"T"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'T';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x55; //"U"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'U';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x56; //"V"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'V';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x57; //"W"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'W';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x58; //"X"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'X';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x59; //"Y"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'Y';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x5A; //"Z"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'Z';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x5B; //"["
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '[';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x5C; //"\"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '\\';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x5D; //"]"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = ']';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x5E; //"^"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '^';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x5F; //"_"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '_';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x60; //"`"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '`';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x61; //"a"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'a';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x62; //"b"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'b';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x63; //"c"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'c';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x64; //"d"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'd';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x65; //"e"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'e';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x66; //"f"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'f';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x67; //"g"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'g';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x68; //"h"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'h';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x69; //"i"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'i';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x6A; //"j"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'j';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x6B; //"k"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'k';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x6C; //"l"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'l';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x6D; //"m"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'm';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x6E; //"n"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'n';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x6F; //"o"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'o';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x70; //"p"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'p';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x71; //"q"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'q';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x72; //"r"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'r';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x73; //"s"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 's';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x74; //"t"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 't';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x75; //"u"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'u';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x76; //"v"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'v';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x77; //"w"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'w';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x78; //"x"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'x';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x79; //"y"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'y';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x7A; //"z"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 'z';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x7B; //"{"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '{';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x7C; //"|"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '|';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x7D; //"}"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '}';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0x7E; //"~"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = '~';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xC8; //"È"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xC8;//'È';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xC9; //"É"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xC9;//'É';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xCA; //"Ê"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xCA;//'Ê';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xCB; //"Ë"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xCB;//'Ë';
    symbolTo128CodeList.append(symb128Code);

//    symb128Code.symbol = 0xCC; //"Ì"
//    symb128Code.width = 11;
//    symb128Code.line_0 = 1;
//    symb128Code.line_1 = 0;
//    symb128Code.line_2 = 1;
//    symb128Code.line_3 = 1;
//    symb128Code.line_4 = 1;
//    symb128Code.line_5 = 0;
//    symb128Code.line_6 = 1;
//    symb128Code.line_7 = 1;
//    symb128Code.line_8 = 1;
//    symb128Code.line_9 = 1;
//    symb128Code.line_10 = 0;
//    symb128Code.line_11 = 0;
//    symb128Code.line_12 = 0;
//    symb128Code.symbolShow = 'Ì';
//    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xCC; //"Ì"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xCC;//'Ì';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xCD; //"Í"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xCD;//'Í';
    symbolTo128CodeList.append(symb128Code);

//    symb128Code.symbol = 0xCE; //"Î"
//    symb128Code.width = 11;
//    symb128Code.line_0 = 1;
//    symb128Code.line_1 = 1;
//    symb128Code.line_2 = 1;
//    symb128Code.line_3 = 0;
//    symb128Code.line_4 = 1;
//    symb128Code.line_5 = 0;
//    symb128Code.line_6 = 1;
//    symb128Code.line_7 = 1;
//    symb128Code.line_8 = 1;
//    symb128Code.line_9 = 1;
//    symb128Code.line_10 = 0;
//    symb128Code.line_11 = 0;
//    symb128Code.line_12 = 0;
//    symb128Code.symbolShow = 'Î';
//    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xCE; //"Î"
    symb128Code.width = 13;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 1;
    symb128Code.line_12 = 1;
    symb128Code.symbolShow = 0xCE;//'Î';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xCF; //"Ï"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xCF;//'Ï';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xD0; //"Ð"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xD0;//'Ð';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xD1; //"Ñ"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xD1;//'Ñ';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xD2; //"Ò"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xD2;//'Ò';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xD3; //"Ó"
    symb128Code.width = 13;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 1;
    symb128Code.line_12 = 1;
    symb128Code.symbolShow = 0xD3;//'Ó';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xFF; //"reserved"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0;
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xF0; //"ð"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xF0;//'ð';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xF1; //"ñ"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xF1;//'ñ';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xF2; //"ò"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xF2;//'ò';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xF3; //"ó"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xF3;//'ó';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xF4; //"ô"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xF4;//'ô';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xF5; //"õ"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 0;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xF5;//'õ';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xF6; //"ö"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 1;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xF6;//'ö';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xF7; //"÷"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 1;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xF7;//'÷';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xF8; //"ø"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 0;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xF8;//'ø';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xF9; //"ù"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 0;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xF9;//'ù';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xFA; //"ú"
    symb128Code.width = 11;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 1;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 0;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 1;
    symb128Code.line_9 = 0;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 0;
    symb128Code.line_12 = 0;
    symb128Code.symbolShow = 0xFA;//'ú';
    symbolTo128CodeList.append(symb128Code);

    symb128Code.symbol = 0xFB; //"û"
    symb128Code.width = 13;
    symb128Code.line_0 = 1;
    symb128Code.line_1 = 1;
    symb128Code.line_2 = 0;
    symb128Code.line_3 = 0;
    symb128Code.line_4 = 0;
    symb128Code.line_5 = 1;
    symb128Code.line_6 = 1;
    symb128Code.line_7 = 1;
    symb128Code.line_8 = 0;
    symb128Code.line_9 = 1;
    symb128Code.line_10 = 0;
    symb128Code.line_11 = 1;
    symb128Code.line_12 = 1;
    symb128Code.symbolShow = 0xFB;//'û';
    symbolTo128CodeList.append(symb128Code);

    // symbol code 93

    symb93Code.symbol = 0x24; //"$"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '$';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x25; //"%"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '%';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x2B; //"+"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '+';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x2D; //"-"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '-';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x2E; //"."
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '.';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x2F; //"/"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '/';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x30; //"0"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '0';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x31; //"1"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '1';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x32; //"2"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '2';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x33; //"3"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '3';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x34; //"4"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '4';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x35; //"5"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '5';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x36; //"6"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '6';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x37; //"7"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '7';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x38; //"8"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '8';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x39; //"9"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '9';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x3D; //" "
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = ' ';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x20; //" "
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = ' ';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x41; //"A"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'A';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x42; //"B"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'B';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x43; //"C"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'C';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x44; //"D"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'D';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x45; //"E"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'E';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x46; //"F"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'F';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x47; //"G"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'G';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x48; //"H"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'H';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x49; //"I"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'I';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x4A; //"J"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'J';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x4B; //"K"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'K';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x4C; //"L"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'L';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x4D; //"M"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'M';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x4E; //"N"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'N';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x4F; //"O"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'O';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x50; //"P"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'P';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x51; //"Q"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'Q';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x52; //"R"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'R';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x53; //"S"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'S';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x54; //"T"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'T';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x55; //"U"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'U';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x56; //"V"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'V';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x57; //"W"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'W';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x58; //"X"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'X';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x59; //"Y"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'Y';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x5A; //"Z"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 'Z';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x0; //"($)"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '$';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x0; //"(%)"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '%';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x0; //"(/)"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '/';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x0; //"(+)"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = '+';
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x28; //"(Start)"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = -1;
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x29; //"(Stop)"
    symb93Code.width = 10;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 1;
    symb93Code.symbolShow = -1;
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x0; //"Unused"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 0;
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x0; //"Unused"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 0;
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x0; //"Unused"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 0;
    symb93Code.line_4 = 0;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 0;
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x0; //"Unused"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 0;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 0;
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x0; //"Unused"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 0;
    symb93Code.line_2 = 1;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 0;
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x0; //"Unused"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 1;
    symb93Code.line_6 = 0;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 0;
    symbolTo93CodeList.append(symb93Code);

    symb93Code.symbol = 0x0; //"Unused"
    symb93Code.width = 9;
    symb93Code.line_0 = 1;
    symb93Code.line_1 = 1;
    symb93Code.line_2 = 0;
    symb93Code.line_3 = 1;
    symb93Code.line_4 = 1;
    symb93Code.line_5 = 0;
    symb93Code.line_6 = 1;
    symb93Code.line_7 = 1;
    symb93Code.line_8 = 0;
    symb93Code.line_9 = 0;
    symb93Code.symbolShow = 0;
    symbolTo93CodeList.append(symb93Code);

    //-------Codabar-------
    symbCodabar.symbol = 0x30; //"0"
    symbCodabar.width = 14;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '0';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x31; //"1"
    symbCodabar.width = 14;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '1';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x32; //"2"
    symbCodabar.width = 14;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '2';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x33; //"3"
    symbCodabar.width = 14;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '3';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x34; //"4"
    symbCodabar.width = 14;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '4';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x35; //"5"
    symbCodabar.width = 14;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '5';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x36; //"6"
    symbCodabar.width = 14;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '6';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x37; //"7"
    symbCodabar.width = 14;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '7';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x38; //"8"
    symbCodabar.width = 14;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '8';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x39; //"9"
    symbCodabar.width = 14;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '9';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x2D; //"-"
    symbCodabar.width = 14;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '-';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x24; //"$"
    symbCodabar.width = 14;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '$';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x3A; //":"
    symbCodabar.width = 16;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = ':';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x2F; //"/"
    symbCodabar.width = 16;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '/';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x2E; //"."
    symbCodabar.width = 16;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '.';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x2B; //"+"
    symbCodabar.width = 20;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = '+';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x41; //"Start/Stop A"
    symbCodabar.width = 16;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = 'A';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x42; //"Start/Stop B"
    symbCodabar.width = 16;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = 'B';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x43; //"Start/Stop C"
    symbCodabar.width = 16;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = 'C';
    symbolToCodabarList.append(symbCodabar);

    symbCodabar.symbol = 0x44; //"Start/Stop D"
    symbCodabar.width = 16;
    symbCodabar.lines.clear();
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(1);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(0);
    symbCodabar.lines.append(1);
    symbCodabar.symbolShow = 'D';
    symbolToCodabarList.append(symbCodabar);

    //-------Code 93--------

    symb39Code.lines.clear();
    symb39Code.symbol = 0x30; //"0"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '0';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x31; //"1"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '1';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x32; //"2"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '2';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x33; //"3"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '3';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x34; //"4"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '4';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x35; //"5"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '5';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x36; //"6"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '6';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x37; //"7"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '7';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x38; //"8"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '8';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x39; //"9"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '9';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x41; //"A"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'A';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x42; //"B"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'B';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x43; //"C"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'C';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x44; //"D"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'D';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x45; //"E"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'E';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x46; //"F"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'F';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x47; //"G"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'G';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x48; //"H"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'H';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x49; //"I"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'I';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x4A; //"J"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'J';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x4B; //"K"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'K';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x4C; //"L"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'L';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x4D; //"M"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'M';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x4E; //"N"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'N';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x4F; //"O"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'O';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x50; //"P"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'P';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x51; //"Q"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'Q';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x52; //"R"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'R';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x53; //"S"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'S';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x54; //"T"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'T';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x55; //"U"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'U';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x56; //"V"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'V';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x57; //"W"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'W';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x58; //"X"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'X';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x59; //"Y"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'Y';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x5A; //"Z"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = 'Z';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x2D; //"-"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '-';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x2E; //"."
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '.';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x3D; //"SPACE"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = ' ';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x24; //"$"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '$';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x2F; //"/"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '/';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x2B; //"+"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '+';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x25; //"%"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = '%';
    symbolTo39CodeList.append(symb39Code);

    symb39Code.lines.clear();
    symb39Code.symbol = 0x2A; //"Start/End"
    symb39Code.width = 12;
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.lines.append(1);
    symb39Code.lines.append(0);
    symb39Code.lines.append(1);
    symb39Code.symbolShow = ' ';
    symbolTo39CodeList.append(symb39Code);
}

/* ------------------------ */
/*     System Variables     */
/* ------------------------ */

static  int  Str_Cnt;
static  char Str_Function [64][257];

/* ---------------------- */
/*  Standard Prototypes   */
/* ---------------------- */

extern int  asc (char *);
extern char *mid (char *, int, int);
char Find_Mod_10_Digit(char *);
char *Calc_MSI_Check_Digit(char *);

/* ------------------- */
/*  User's Prototypes  */ 
/* ------------------- */

static int TRUE_ = 1;
static int FALSE_ = 0;
  
//long  UPCe(char *pcData_To_Encode, char *szReturnVal, long iSize);
long  Codabar(char *pcData_To_Encode, char **szReturnVal, long &iSize);
long  Code128a(char *pcData_To_Encode, char **szReturnVal, long &iSize);
long  Code128b(char *pcData_To_Encode, char **szReturnVal, long &iSize);
long  Code128c(char *pcData_To_Encode, char **szReturnVal, long &iSize);
long  Interleaved2of5(char *pcData_To_Encode, char **szReturnVal, long &iSize);
long  Interleaved2of5Mod10(char *pcData_To_Encode, char *szReturnVal, long iSize);
long  Code39(char *pcData_To_Encode, char **szReturnVal, long &iSize);
long  Code39Mod43(char *pcData_To_Encode, char *szReturnVal, long iSize);
long  EAN8(char *pcData_To_Encode, char **szReturnVal, long &iSize);
long  EAN13(char *pcData_To_Encode, char **szReturnVal, long &iSize);
long  UPCa(char *pcData_To_Encode, char **szReturnVal, long &iSize);
long  MSI(char *pcData_To_Encode, char **szReturnVal, long &iSize);
long  Postnet(char *pcData_To_Encode, char **szReturnVal, long &iSize);
long  Code128(char *Data_To_Encode, int ApplyTilde, char **szReturnVal, long &iSize);
long  Code128HumanReadable(char *Data_To_Encode, int ApplyTilde, char **szReturnVal, long &iSize);
long  UCC128(char *Data_To_Encode, char **szReturnVal, long &iSize);
long  Code93(char *Data_To_Encode, char **szReturnVal, long &iSize);


/* --------------------- */
/*  Run Time Functions   */
/* --------------------- */

//CMemLeadDetect memLeakDetect;///mem leak 
char* str_upr(char* str)
{
	char returnString[512];
    memset(&returnString, 0, 512);
	
	/*//char *returnString = new char[512] ;*/
    int Print_Buffer=0;
	int x=0;
for (x=1;x<= (int)strlen(str);++x)
{
	int CurrentValue = asc(mid(str, x, 1));
  if (CurrentValue >= 97 && CurrentValue <= 122)
            Print_Buffer += sprintf(returnString + Print_Buffer ,"%c", CurrentValue - 32);
	 else 
            Print_Buffer += sprintf(returnString + Print_Buffer ,"%c", CurrentValue );

}
sprintf(str,"%s",returnString);
return (char*)str;
}
char* str_rev(char* str)
{

if (NULL==str)return str; 
int l=strlen(str)-1;
if (1==l)return str;
int x=0;
for(x=0;x<l;x++,l--)
{
str[x]^=str[l];  
str[l]^=str[x];  
str[x]^=str[l];
}
l=0;
return (char*)str;
}

extern char *mid(char *S, int start, int length)
{
    if(++Str_Cnt==64)
	{
        Str_Cnt = 0;
	}
	
	if (start > (int) strlen(S))
	{ 
        Str_Function[Str_Cnt][0]='\0';
	}
	else
	{
        strncpy (Str_Function[Str_Cnt], &S [start-1], length);
	}

    Str_Function[Str_Cnt][length]='\0';
	
    return Str_Function[Str_Cnt];
}




extern int asc(char *z)
{
static int q;
q = 0;
memmove(&q,z,1);
return q;
}


/* ---------------------- */
/*  User Subs/Functions   */
/* ---------------------- */


long  Code128a(char *Data_To_Encode,  char **output, long &iSize)
{
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;
	
	char DataToPrint[512];
    memset(&DataToPrint, 0, 512);
	//DataToPrint = new char[512];

    char Printable_String[512];
    memset(&Printable_String, 0, 512);
    //Printable_String = new char[512];

	int  C128Start;
	int  C128StartA;
	int  C128StartB;
	int  C128StartC;
	int  C128Stop;
	int  weightedTotal;
	int  I;
	int  CurrentChar;
	int  CurrentValue;
	int  CheckDigitValue;
	int  CheckDigit;
	char C128CheckDigit;
	//char C128CurrentChar;
	int C128CurrentChar;
	int BufferCounter = 0;

	C128StartA=203;
	C128StartB=204;
	C128StartC=205;
	C128Stop=206;

	/* Here we select character set A */
	C128Start=C128StartA;

	/* <<<< Calculate Modulo 103 Check Digit >>>> */
	/* Set WeightedTotal to the value of the start character */
	weightedTotal=C128Start-100;

    for(I=1;I <= (int)strlen(Data_To_Encode);I++)
	{
		/* Get the ASCII value of each character */
        CurrentChar=(asc(mid(Data_To_Encode,I,1)));

		/* Get the Code 128 value of CurrentChar according to chart */
  		if(CurrentChar<135) {CurrentValue=CurrentChar-32;}
  		if(CurrentChar>134) {CurrentValue=CurrentChar-100;}

		/* Multiply by the weighting character */
  		CurrentValue=CurrentValue*I;

		/* Add the values together to get the weighted total*/
  		weightedTotal=weightedTotal+CurrentValue;
	}

	/* divide the WeightedTotal by 103 and get the remainder, this is the CheckDigitValue*/
	CheckDigitValue = (int) (weightedTotal % 103);

	/* Now that we have the CheckDigitValue, find the corresponding ASCII character from the table */
	if(CheckDigitValue < 95 && CheckDigitValue > 0)
	{
		CheckDigit = CheckDigitValue + 32;
	}
	if(CheckDigitValue > 94)
	{ 
		CheckDigit = CheckDigitValue + 100;
	}
	if(CheckDigitValue == 0)
	{
		CheckDigit = 194;
	}
	C128CheckDigit=CheckDigit;
/*	for(I=1;I <= (int)strlen(Data_To_Encode);I++)
	{
        C128CurrentChar = asc((mid(Data_To_Encode,I,1)));
		//if(C128CurrentChar==' ') 
		if(C128CurrentChar==32) 
		{
			//C128CurrentChar=(char)194;
			C128CurrentChar=194;
		}
        //BufferCounter += sprintf(DataToPrint + BufferCounter, "%c", Data_To_Encode[I-1]);
		BufferCounter += sprintf(DataToPrint + BufferCounter, "%c", C128CurrentChar);
		//strcat(DataToPrint,&C128CurrentChar);
	}
*/
/* Check for spaces or "00" and print ASCII 194 instead */
	/* place changes in DataToPrint */
	BufferCounter = 0;
    for(I=1;I <= (int)strlen(Data_To_Encode);I++)
	{
        C128CurrentChar=Data_To_Encode[I-1];
		if(C128CurrentChar==' ') 
		{
			C128CurrentChar = (char)194;
		}
		BufferCounter += sprintf(DataToPrint + BufferCounter, "%c", C128CurrentChar);
	}
    /* Get Printable_String */
    sprintf(Printable_String,"%c%s%c%c",C128Start,DataToPrint,C128CheckDigit,C128Stop);


    iSize = (long)strlen(Printable_String);
    *output = new char [iSize];
    memset(*output, 0, iSize);
    strncpy(*output, Printable_String, strlen(Printable_String));
	return 0;
}



long  Code128b(char *Data_To_Encode, char **output, long &iSize)
{
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;
	
	char DataToPrint[512];
    memset(&DataToPrint, 0, 512);
	//DataToPrint = new char;

    char Printable_String[512];
    memset(&Printable_String, 0, 512);
    //Printable_String = new char[512];

	int BufferCounter = 0;
	int  C128Start;
	int  C128StartA;
	int  C128StartB;
	int  C128StartC;
	int  C128Stop;
	int  weightedTotal;
	int  I;
	int  CurrentChar;
	int  CurrentValue;
	int  CheckDigitValue;
	int  CheckDigit;
	char C128CheckDigit;
	char C128CurrentChar;

	C128StartA=203;
	C128StartB=204;
	C128StartC=205;
	C128Stop=206;

	/* Here we select character set A */
	C128Start=C128StartB;

	/* <<<< Calculate Modulo 103 Check Digit >>>> */
	/* Set WeightedTotal to the value of the start character */
	weightedTotal=C128Start-100;

    for(I=1;I <= (int)strlen(Data_To_Encode);I++)
	{
		/* Get the ASCII value of each character */
        CurrentChar=(asc(mid(Data_To_Encode,I,1)));

		/* Get the Code 128 value of CurrentChar according to chart */
		if(CurrentChar<135) {CurrentValue=CurrentChar-32;}
		if(CurrentChar>134) {CurrentValue=CurrentChar-100;}

		/* Multiply by the weighting character */
		CurrentValue=CurrentValue*I;

		/* Add the values together */
		weightedTotal=weightedTotal+CurrentValue;
	}

	/* divide the WeightedTotal by 103 and get the remainder, this is the CheckDigitValue	*/
	CheckDigitValue = (int) (weightedTotal % 103);

	/* Now that we have the CheckDigitValue, find the corresponding ASCII character
	from the table */
	if(CheckDigitValue<95 && CheckDigitValue>0) {CheckDigit=CheckDigitValue+32;}
	if(CheckDigitValue>94) {CheckDigit=CheckDigitValue+100;}
	if(CheckDigitValue==0) {CheckDigit=194;}
	C128CheckDigit=CheckDigit;

	/* Check for spaces or "00" and print ASCII 194 instead */
	/* place changes in DataToPrint */
	BufferCounter = 0;
    for(I=1;I <= (int)strlen(Data_To_Encode);I++)
	{
        C128CurrentChar=Data_To_Encode[I-1];
		if(C128CurrentChar==' ') 
		{
			C128CurrentChar = (char)194;
		}
		BufferCounter += sprintf(DataToPrint + BufferCounter, "%c", C128CurrentChar);
	}

    /* Get Printable_String */
    sprintf(Printable_String,"%c%s%c%c",C128Start, DataToPrint,C128CheckDigit,C128Stop);

    iSize = (long)strlen(Printable_String);

    *output = new char [iSize];
    memset(*output, 0, iSize);

    strncpy(*output, Printable_String, strlen(Printable_String));
//    FILE *f=fopen("c:/qqq.q","w");
//    fwrite(*output, iSize, 1, f);
//    fclose(f);
	return 0;
} //end Code128B()


long  Code128c(char *pcData_To_Encode, char **output, long &iSize)
{
    if(pcData_To_Encode == NULL)
		return 1;
    if(strlen(pcData_To_Encode) == 0)
		return 1;
	
	char DataToPrint[512];
    memset(&DataToPrint, 0, 512);
	//DataToPrint = new char[512];

    char Printable_String[512];
    memset(&Printable_String, 0, 512);
    //Printable_String = new char[512];

    char Only_Correct_Data[512];
    memset(&Only_Correct_Data, 0, 512);
    //Only_Correct_Data = new char[512];

    char TempOnly_Correct_Data[512];
    memset(&TempOnly_Correct_Data, 0, 512);
    //TempOnly_Correct_Data = new char[512];

	int  C128Start;
	int  C128Stop;
	int  weightedTotal;
	int  WeightValue;
	int  I;
	int  CurrentValue;
	int  CheckDigitValue;
	char C128CheckDigit;
    int Print_Buffer = 0;
	/* Check to make sure data is numeric and remove dashes, etc. */
    for(I=1;I <= (int)strlen(pcData_To_Encode);I++)
	{
        /* Add all numbers to Only_Correct_Data string */
        if(isdigit(asc(mid(pcData_To_Encode,I,1))) != 0)
		{
            Print_Buffer += sprintf(TempOnly_Correct_Data + Print_Buffer, "%s", mid(pcData_To_Encode,I,1));
		}
	}

	

	/* Check for an even number of digits, add 0 if not even */
    int rem = (int) (strlen(TempOnly_Correct_Data) % 2);
	if(rem == 1)	
	{
        sprintf(Only_Correct_Data, "0%s", TempOnly_Correct_Data);
	}
	else
        sprintf(Only_Correct_Data, "%s", TempOnly_Correct_Data);

	/* Assign start & stop codes */
	C128Start=205;
	C128Stop=206;

	/* <<<< Calculate Modulo 103 Check Digit and generate DataToPrint >>>> */
	/* Set WeightedTotal to the Code 128 value of the start character */
	weightedTotal=105;
	WeightValue = 1;
    Print_Buffer = 0;
    for(I=1;I <= (int)strlen(Only_Correct_Data);I = I + 2)
	{
		/* Get the value of each number pair */
        CurrentValue = atoi(mid(Only_Correct_Data, I, 2));

		/* Get the DataToPrint */
		if(CurrentValue < 95 && CurrentValue > 0) 
		{
            Print_Buffer += sprintf(DataToPrint + Print_Buffer,"%c", CurrentValue + 32);
		}
		if(CurrentValue>94) 
		{
            Print_Buffer += sprintf(DataToPrint + Print_Buffer,"%c", CurrentValue + 100);
		}
		if(CurrentValue==0) 
		{
            Print_Buffer += sprintf(DataToPrint + Print_Buffer,"%c", (char)194);
		}

		/* Multiply by the weighting character */
		CurrentValue=CurrentValue * WeightValue;

		/* Add the values together to get the weighted total*/
		weightedTotal = weightedTotal + CurrentValue;
		WeightValue = WeightValue + 1;
	}

	/* Divide the weighted total by 103 and get the remainder, this is the CheckDigitValue
	*/
	CheckDigitValue = (int)(weightedTotal % 103);

	/* Now that we have the CheckDigitValue, find the corresponding ASCII character
	from the table */
	if (CheckDigitValue<95 && CheckDigitValue>0) 
	{
		C128CheckDigit=CheckDigitValue+32;
	}
	if(CheckDigitValue>94)
	{
		C128CheckDigit=CheckDigitValue+100;
	}
	if (CheckDigitValue==0) 
	{
		C128CheckDigit = (char)194;
	}

    /* Get Printable_String */
    sprintf(Printable_String,"%c%s%c%c",C128Start,DataToPrint,C128CheckDigit,C128Stop);

    iSize = (long)strlen(Printable_String);
    *output = new char [iSize];
    memset(*output, 0, iSize);
    strncpy(*output, Printable_String, strlen(Printable_String));
	return 0;
} 


/*****************************************************************/
/* ANSI C Functions for IDAutomation Barcode Fonts               */
/* © Copyright 2002- 2007, IDAutomation.com, Inc.                */
/* All rights reserved.                                          */
/* Redistribution and use of this code in source and/or binary   */
/* forms, with or without modification, are permitted provided   */
/* that: (1) all copies of the source code retain the above      */
/* unmodified copyright notice and this entire unmodified        */
/* section of text, (2) You or Your organization owns a valid    */
/* Developer License to this product from IDAutomation.com       */
/* and, (3) when any portion of this code is bundled in any      */
/* form with an application, a valid notice must be provided     */
/* within the user documentation, start-up screen or in the      */
/* help-about section of the application that specifies          */
/* IDAutomation.com as the provider of the Software bundled      */
/* with the application.                                         */
/*****************************************************************/


long  Codabar(char *Data_To_Encode, char **output, long &iSize)
{
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;

    char Only_Correct_Data[512];
    memset(&Only_Correct_Data, 0, 512);
    //Only_Correct_Data = new char[512];

    char Printable_String[512];
    memset(&Printable_String, 0, 512);
    //Printable_String = new char[512];
	
	int I;
    int Print_Buffer = 0;
	/* Check to make sure data is numeric, $, +, -, /, or :, and remove all others. */
    for(I = 1;I <= (int)strlen(Data_To_Encode);I++)
	{
        if((int) asc(mid(Data_To_Encode, I, 1)) > 0 && isdigit(asc(mid(Data_To_Encode,I,1))))
            Print_Buffer += sprintf(Only_Correct_Data + Print_Buffer, "%s", mid(Data_To_Encode, I, 1));
        else if(Data_To_Encode[I - 1] == '$' || Data_To_Encode[I - 1] == '+' || Data_To_Encode[I - 1] == '-' ||
            Data_To_Encode[I - 1] == '/' || Data_To_Encode[I - 1] == '.' || Data_To_Encode[I - 1] == ':')
            Print_Buffer += sprintf(Only_Correct_Data + Print_Buffer, "%s", mid(Data_To_Encode,I,1));
	}

	/* Get Printable String */
    sprintf(Printable_String,"A%sB", Only_Correct_Data);


    iSize = (long)strlen(Printable_String);
    *output = new char [iSize];
    memset(*output, 0, iSize);
    strncpy(*output, Printable_String, strlen(Printable_String));

	return 0;
}



long  Interleaved2of5(char *Data_To_Encode, char **output, long &iSize)
{
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;
	
//	char Printable_String[512];
//	//Printable_String = new char[512];

	char DataToPrint[512];
    memset(&DataToPrint, 0, 512);
	//DataToPrint = new char[512];

    char Only_Correct_Data[512];
    memset(&Only_Correct_Data, 0, 512);
    //Only_Correct_Data = new char[512];

	char TempString[512];
    memset(&TempString, 0, 512);
	//TempString = new char[512];

	int I;
	int CurrentNumberPair = 0;
	char StartCode;
	char StopCode;
	int BufferCounter = 0;
	
	/* Check to make sure data is numeric and remove dashes, etc. */
    for(I=1;I <= (int)strlen(Data_To_Encode);I++)
	{
        /* Add all numbers to Only_Correct_Data string */
        if(isdigit(asc(mid(Data_To_Encode,I,1))))
		{
            BufferCounter += sprintf(Only_Correct_Data + BufferCounter,"%c", asc(mid(Data_To_Encode,I,1)));
		}
	}	

	BufferCounter = 0;
	
	/* Check for an even number of digits, add 0 if not even */
    if((int)(strlen(Only_Correct_Data) % 2) == 1)
	{
        sprintf(TempString,"%d%s",0, Only_Correct_Data);
	}
	else
        sprintf(TempString,"%s",Only_Correct_Data);

	/* Assign start and stop codes */
	StartCode = (char)203;
	StopCode = (char)204;
	for(I=1;I <= (int)strlen(TempString);I = I + 2)
	{
		/* Get the value of each number pair */
		CurrentNumberPair = atoi(mid(TempString, I, 2));
		/* == Get the ASCII value of CurrentChar according to chart by adding to the value == */
//		if(CurrentNumberPair < 94)
		{
            BufferCounter += sprintf(DataToPrint + BufferCounter,"%c", (char)(CurrentNumberPair));// + 33));
		}
//		if(CurrentNumberPair > 93)
//		{
//            BufferCounter += sprintf(DataToPrint + BufferCounter,"%c", (char)(CurrentNumberPair));// + 103));
//		}
	}

	/* Get Printable String */
    //sprintf(Printable_String,"%c%s%c", StartCode, DataToPrint, StopCode);

    QString str1;
    for (int i = 0; i < BufferCounter; i++)
    {
        if ((unsigned char)DataToPrint[i] < 10)
        {
            str1.append("0");
            str1.append(QString::number((unsigned char)DataToPrint[i]));
        }
        else
        {
            str1.append(QString::number((unsigned char)DataToPrint[i]));
        }
    }

    QMap <QString, QString> mapCode;
    mapCode["1"] = "WNNNW";
    mapCode["2"] = "NWNNW";
    mapCode["3"] = "WWNNN";
    mapCode["4"] = "NNWNW";
    mapCode["5"] = "WNWNN";
    mapCode["6"] = "NWWNN";
    mapCode["7"] = "NNNWW";
    mapCode["8"] = "WNNWN";
    mapCode["9"] = "NWNWN";
    mapCode["0"] = "NNWWN";



    if((int)(str1.size() % 2) == 1)
        return 1;

    QStringList strCombList;
    strCombList.append("1010");
    for (int i = 0; i < str1.size(); i += 2)
    {
        QString str01;
        QString str02;

        if (str01.size() != str02.size())
            return 1;

        str01 = mapCode[str1.at(i)];
        str02 = mapCode[str1.at(i + 1)];

        for (int n = 0; n < str01.size(); n ++)
        {
            str02.insert((n * 2), str01.at(n));
        }


        QString outCode;
        for (int n = 0; n < str02.size(); n ++)
        {
            QString bar = "0";
            if((int)(n % 2) == 1)
            {
                bar = "0";
            }
            else
            {
                bar = "1";
            }

            if (QString(str02.at(n)) == "W")
            {
                outCode += bar;
                outCode += bar;
            }
            else
            {
                outCode += bar;
            }
        }

        strCombList.append(outCode);
    }

    strCombList.append("1101");
//    iSize = 0;
//    for (int i = 0; i < strCombList.size(); i ++)
//    {
//        iSize += strCombList.at(i).size();
//    }
   // iSize = (long)strlen(Printable_String);


    QString outCode = strCombList.join("");
    iSize = outCode.size();
    *output = new char [outCode.size()];
    char *nOut = *output;
    memset(nOut, 0, outCode.size());

    for (int i = 0; i < outCode.size(); i++)
    {

        nOut[i] = (char)(QString(outCode.at(i)).toInt());
    }
    //strncpy(*output, Printable_String, strlen(Printable_String));
	return 0;
} 


/*****************************************************************/
/* ANSI C Functions for IDAutomation Barcode Fonts               */
/* © Copyright 2002- 2007, IDAutomation.com, Inc.                */
/* All rights reserved.                                          */
/* Redistribution and use of this code in source and/or binary   */
/* forms, with or without modification, are permitted provided   */
/* that: (1) all copies of the source code retain the above      */
/* unmodified copyright notice and this entire unmodified        */
/* section of text, (2) You or Your organization owns a valid    */
/* Developer License to this product from IDAutomation.com       */
/* and, (3) when any portion of this code is bundled in any      */
/* form with an application, a valid notice must be provided     */
/* within the user documentation, start-up screen or in the      */
/* help-about section of the application that specifies          */
/* IDAutomation.com as the provider of the Software bundled      */
/* with the application.                                         */
/*****************************************************************/


long  Interleaved2of5Mod10 (char *Data_To_Encode, char *output, long iSize)
{
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;
	
    char Data_To_Encode2[512];
    memset(&Data_To_Encode2, 0, 512);
    //Data_To_Encode2 = new char[512];
	
    char Printable_String[512];
    memset(&Printable_String, 0, 512);
    //Printable_String = new char[512];

    char Only_Correct_Data[512];
    memset(&Only_Correct_Data, 0, 512);
    //Only_Correct_Data = new char[512];

	int BufferCounter = 0;
	char StartCode;
	char StopCode;
	int I = 0;
	int CurrentNumberPair = 0;
	
	/* Check to make sure data is numeric and remove dashes, etc. */
    for(I=1;I <= (int)strlen(Data_To_Encode);I++)
	{
        /* Add all numbers to Only_Correct_Data string */
        if(isdigit(asc(mid(Data_To_Encode,I,1))))
		{
            BufferCounter += sprintf(Only_Correct_Data + BufferCounter, "%s", mid(Data_To_Encode, I, 1));
		}
	}


    BufferCounter += sprintf(Only_Correct_Data + BufferCounter, "%c", Find_Mod_10_Digit(Only_Correct_Data));

	/* Check for an even number of digits, add 0 if not even */
    if((int)(strlen(Only_Correct_Data) % 2) == 1)
        sprintf(Data_To_Encode2, "0%s", Only_Correct_Data);
	else	
        sprintf(Data_To_Encode2, "%s", Only_Correct_Data);
 

	/* Assign start and stop codes */
	StartCode = (char)203;
	StopCode = (char)204;
	BufferCounter = 0;
    BufferCounter += sprintf(Printable_String + BufferCounter, "%c", StartCode);
    for(I = 1;I <= (int)strlen(Data_To_Encode2);I = I + 2)
	{
        CurrentNumberPair = atoi(mid(Data_To_Encode2, I, 2));
		if(CurrentNumberPair < 94)
            BufferCounter += sprintf(Printable_String + BufferCounter, "%c", (char)(CurrentNumberPair + 33));
		else
            BufferCounter += sprintf(Printable_String + BufferCounter, "%c", (char)(CurrentNumberPair + 103));
	}

    BufferCounter += sprintf(Printable_String + BufferCounter, "%c", StopCode);


    iSize = (long)strlen(Printable_String);
    strncpy(output, Printable_String, strlen(Printable_String));
	
	return 0;
} 


/*****************************************************************/
/* ANSI C Functions for IDAutomation Barcode Fonts               */
/* © Copyright 2002- 2007, IDAutomation.com, Inc.                */
/* All rights reserved.                                          */
/* Redistribution and use of this code in source and/or binary   */
/* forms, with or without modification, are permitted provided   */
/* that: (1) all copies of the source code retain the above      */
/* unmodified copyright notice and this entire unmodified        */
/* section of text, (2) You or Your organization owns a valid    */
/* Developer License to this product from IDAutomation.com       */
/* and, (3) when any portion of this code is bundled in any      */
/* form with an application, a valid notice must be provided     */
/* within the user documentation, start-up screen or in the      */
/* help-about section of the application that specifies          */
/* IDAutomation.com as the provider of the Software bundled      */
/* with the application.                                         */
/*****************************************************************/


long  Code39 (char *Data_To_Encode, char **output, long &iSize)
{
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;

	char DataToPrint[512];
    memset(&DataToPrint, 0, 512);
	//DataToPrint = new char[512];
	
    char Printable_String[512];
    memset(&Printable_String, 0, 512);
    //Printable_String = new char[512];

	int I;
	int CurrentChar;
//	int BufferCounter = 0;
	/* Check for spaces in code */
    for(I=1;I <= (int)strlen(Data_To_Encode);I++)
	{
		/* Get each character one at a time */
        CurrentChar = asc((mid(Data_To_Encode,I,1)));
		/* To print the barcode symbol representing a space you will need*/
		/* to type or print "=" (the equal character) instead of a space character.*/
		if(CurrentChar == 32)
		{
			CurrentChar = 61;
		}
		//BufferCounter += sprintf(DataToPrint + BufferCounter,"%c", CurrentChar);
		sprintf(DataToPrint,"%s%c", DataToPrint,CurrentChar);
	}
	
	/* Get Printable String */
    sprintf(Printable_String,"*%s*",str_upr(DataToPrint));
	
    iSize = (long)strlen(Printable_String);
    *output = new char [iSize];
    memset(*output, 0, iSize);
    strncpy(*output, Printable_String, strlen(Printable_String));

	return 0;
}


/*****************************************************************/
/* ANSI C Functions for IDAutomation Barcode Fonts               */
/* © Copyright 2002- 2007, IDAutomation.com, Inc.                */
/* All rights reserved.                                          */
/* Redistribution and use of this code in source and/or binary   */
/* forms, with or without modification, are permitted provided   */
/* that: (1) all copies of the source code retain the above      */
/* unmodified copyright notice and this entire unmodified        */
/* section of text, (2) You or Your organization owns a valid    */
/* Developer License to this product from IDAutomation.com       */
/* and, (3) when any portion of this code is bundled in any      */
/* form with an application, a valid notice must be provided     */
/* within the user documentation, start-up screen or in the      */
/* help-about section of the application that specifies          */
/* IDAutomation.com as the provider of the Software bundled      */
/* with the application.                                         */
/*****************************************************************/


long  Code39Mod43 (char *Data_To_Encode, char *output, long iSize)
{

    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;
	
    //char Data_To_Encode2[512];
    //Data_To_Encode2 = new char[512];

	char DataToPrint[512];
    memset(&DataToPrint, 0, 512);
	//DataToPrint = new char[512];

    char Printable_String[512];
    memset(&Printable_String, 0, 512);
    //Printable_String = new char[512];

	int I;
	int weightedTotal;
	int	CurrentChar;		
	char CurrentValue;
    char CheckDigit = 0;
	char CheckDigitValue;
	int bufferCounter = 0;

    /* Get data from user, this is the Data_To_Encode */
//	sprintf(Data_To_Encode2,"%s",Data_To_Encode);
	weightedTotal = 0;
	/* only pass correct data */
    for(I = 1;I <= (int)strlen(Data_To_Encode);I++)
	{
		// Get each character one at a time 
        CurrentChar = asc(str_upr(mid(Data_To_Encode,I,1)));
		// Get the value of CurrentChar according to MOD43 
		// 0-9 
		if(CurrentChar < 58 && CurrentChar > 47) 
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = CurrentChar - 48;
		}
		else if(CurrentChar < 91 && CurrentChar > 64)   
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = CurrentChar - 55;
		}
		else if(CurrentChar == 32) 		// Space 
		{
			bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", "=");
			CurrentValue = 38;
		}
		else if(CurrentChar == 45) // - 
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = 36;
		}
		else if(CurrentChar == 46) // . 
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = 37;
		}
		else if(CurrentChar == 36) // $ 
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = 39;
		}
		else if(CurrentChar == 47) // / 
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = 40;
		}
		else if(CurrentChar == 43) // + 
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = 41;
		}
		else if(CurrentChar == 37) // % 
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = 42;
		}
		else
		{
			CurrentValue = 0;
		}
		// add the values together 
		weightedTotal = weightedTotal + CurrentValue;
	}
	// divide the WeightedTotal by 43 and get the remainder, this is the CheckDigit	
	CheckDigitValue = (int)((weightedTotal % 43));

	// Assign values to characters 
	if(CheckDigitValue < 10) // 0-9 
	{
		CheckDigit = CheckDigitValue + 48;
	}	
	else if((CheckDigitValue < 36) && (CheckDigitValue > 9)) // A-Z 
	{
		CheckDigit = CheckDigitValue + 55;
	}
	else if(CheckDigitValue == 38) // Space 
	{
		CheckDigit = 61;
	}	
	else if(CheckDigitValue == 36) // - 
	{
		CheckDigit = 45;
	}		
	else if(CheckDigitValue == 37)    // . 
	{
		CheckDigit = 46;
	}	
	else if(CheckDigitValue == 39)	// $ 
	{
		CheckDigit = 36;
	}
	else if(CheckDigitValue == 40)	// / 
	{
		CheckDigit = 47;
	}	
	else if(CheckDigitValue == 41)	// + 
	{
		CheckDigit = 43;
	}	
	else if(CheckDigitValue == 42)	// % 
	{
		CheckDigit = 37;
	}	
	
	// Get Printable String 
    sprintf(Printable_String,"*%s%c*", str_upr(DataToPrint), CheckDigit);
    iSize = (long)strlen(Printable_String);
    strncpy(output, Printable_String, strlen(Printable_String));

	return 0;
}


/*****************************************************************/
/* ANSI C Functions for IDAutomation Barcode Fonts               */
/* © Copyright 2002- 2007, IDAutomation.com, Inc.                */
/* All rights reserved.                                          */
/* Redistribution and use of this code in source and/or binary   */
/* forms, with or without modification, are permitted provided   */
/* that: (1) all copies of the source code retain the above      */
/* unmodified copyright notice and this entire unmodified        */
/* section of text, (2) You or Your organization owns a valid    */
/* Developer License to this product from IDAutomation.com       */
/* and, (3) when any portion of this code is bundled in any      */
/* form with an application, a valid notice must be provided     */
/* within the user documentation, start-up screen or in the      */
/* help-about section of the application that specifies          */
/* IDAutomation.com as the provider of the Software bundled      */
/* with the application.                                         */
/*****************************************************************/


long  Code93(char *Data_To_Encode, char **output, long &iSize)
{
	
	
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;
	
    //char Data_To_Encode2[512];
    //Data_To_Encode2 = new char[512];

	char DataToPrint[512];
    memset(&DataToPrint, 0, 512);
	//DataToPrint = new char[512];

    char Printable_String[512];
    memset(&Printable_String, 0, 512);
    //Printable_String = new char[512];


	
	int  I;
	char CurrentChar;
    char CurrentValue;
    char CheckDigitC;
    char CheckDigitK;
	char CheckDigitValue; 
	
	
	
	int  CW = 1;                           
    int  KW = 2;						        
    int  CWSum =0;                           
    int  KWSum =0;                           
    int bufferCounter = 0;
    


     
	

    /* Get data from user, this is the Data_To_Encode */
    //sprintf(Data_To_Encode2,"%s",str_upr(Data_To_Encode));
	
	/* only pass correct data */
    for(I = 1;I <= (int)strlen(Data_To_Encode);I++)
	{
		/* Get each character one at a time */
        CurrentChar = asc(str_upr(mid(Data_To_Encode,I,1)));
		/* Get the value of CurrentChar according to MOD43 */
		/* 0-9 */
		if(CurrentChar < 58 && CurrentChar > 47) 
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = CurrentChar - 48;
		}
		else if(CurrentChar < 91 && CurrentChar > 64)   
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = CurrentChar - 55;
		}
		else if(CurrentChar == 32) 		/* Space */
		{
			bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", "=");
			CurrentValue = 38;
		}
		else if(CurrentChar == 45) /* - */
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = 36;
		}
		else if(CurrentChar == 46) /* . */
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = 37;
		}
		else if(CurrentChar == 36) /* $ */
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = 39;
		}
		else if(CurrentChar == 47) /* / */
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = 40;
		}
		else if(CurrentChar == 43) /* + */
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = 41;
		}
		else if(CurrentChar == 37) /* % */
		{
            bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(Data_To_Encode,I,1));
			CurrentValue = 42;
		}
		else
		{
			
			CurrentValue = 0;
		}

/* To print the barcode symbol representing a space you will */
		/* to type or print "=" (the equal character) instead of a space character.*/
		if(CurrentChar == 32)
		{
			CurrentChar = 61;
		}

		
	}

  for(I= (int)strlen(Data_To_Encode);I>0;I--)
	{
		/* Get each character one at a time */
        CurrentChar=asc(str_upr(mid(Data_To_Encode,I,1)));
		/* Get the value of CurrentChar according to 93 */
		if((CurrentChar<58)&&(CurrentChar>47))
		/* 0-9 */
		{
			CurrentValue=CurrentChar-48;
		}

		/* A-Z */
		if((CurrentChar<91)&&(CurrentChar>64))
		{
			CurrentValue=CurrentChar-55;
		}
		
		/* - */
		if(CurrentChar==45)
		{
			CurrentValue=36;
		}

		/* . */
		if(CurrentChar==46)
		{
			CurrentValue=37;
		}

	    /* Space */
		if(CurrentChar==32)

		{
		
			CurrentValue=38;
		}

		/* $ */
		if(CurrentChar==36)
		{
			CurrentValue=39;
		}

		/* / */
		if(CurrentChar==47)
		{
			CurrentValue=40;
		}

		/* + */
		if(CurrentChar==43)
		{
			CurrentValue=41;
		}

		/* % */
		if(CurrentChar==37)
		{
			CurrentValue=42;
		}
		
        /* ! */
		if(CurrentChar==33 ) 
		{
			CurrentValue=43;
		}
		
		/* # */
		if(CurrentChar==35 ) 
		{
			CurrentValue=44;
		}


		/* & */
		if(CurrentChar==38 ) 
		{
			CurrentValue=45;
		}


		/* @ */
		if(CurrentChar==64 ) 
		{
			CurrentValue=46;
		}

		CWSum += CurrentValue * CW;        

		KWSum += CurrentValue * KW;        

		if (CW == 20) {                    
			CW = 0;                        
		}

		if (KW == 15) {                    
			KW = 0;
		}

		CW++;    
		KW++;    


	}

	/* divide the WeightedTotal by 47 and get the remainder, this is the CheckDigit for C	*/
	
	CheckDigitValue = ((int)(CWSum % 47));
	
	/* Assign values to characters for Check digit C */
	/* 0-9 */
	if(CheckDigitValue<10)
	{
		CheckDigitC=CheckDigitValue+48;
	 }
	
	/* A-Z */
	if((CheckDigitValue<36)&&(CheckDigitValue>9))
	{
		CheckDigitC=CheckDigitValue+55;
	 }
	
	/* = */
	if(CheckDigitValue==38)
	{
		CheckDigitC=61;
	}


	/* Space */
	if(CheckDigitValue==38)
	{
		CheckDigitC=32;
	}

	/* - */
	if(CheckDigitValue==36)
	{
		CheckDigitC=45;
	}
	
	/* . */
	if(CheckDigitValue==37)
	{
		CheckDigitC=46;
	}
	
	/* $ */
	if(CheckDigitValue==39)
	{
		CheckDigitC=36;
	}

	/* / */
	if(CheckDigitValue==40)
	{
		CheckDigitC=47;
	}
	
	/* + */
	if(CheckDigitValue==41)
	{
		CheckDigitC=43;
	}
	
	/* % */
	if(CheckDigitValue==42)
	{
		CheckDigitC=37;
	  }

	/* ! */
	if(CheckDigitValue == 43 ) 
	{
		CheckDigitC=33;
	}

	/* # */
	if(CheckDigitValue == 44 ) 
	{
		CheckDigitC=35;
	}

	/* & */
	if(CheckDigitValue == 45 ) 
	{
		CheckDigitC=38;
	}

	/* @ */
	if(CheckDigitValue == 46 ) 
	{
		CheckDigitC=64;
	}  

	/* divide the WeightedTotal by 47 and get the remainder, this is the CheckDigit	for K*/
	
	CheckDigitValue = (int)((KWSum+CheckDigitValue) % 47);
	
	/* 0-9 */
	if(CheckDigitValue<10)
	{
		CheckDigitK=CheckDigitValue+48;
	 }
	
	/* A-Z */
	if((CheckDigitValue<36)&&(CheckDigitValue>9))
	{
		CheckDigitK=CheckDigitValue+55;
	 }
	
	/* Space */
	if(CheckDigitValue == 38)
	{
		CheckDigitK=32;
	}

	/* - */
	if(CheckDigitValue == 36)
	{
		CheckDigitK=45;
	}
	
	/* . */
	if(CheckDigitValue == 37)
	{
		CheckDigitK=46;
	}
	
	/* $ */
	if(CheckDigitValue == 39)
	{
		CheckDigitK=36;
	}

	/* / */
	if(CheckDigitValue == 40)
	{
		CheckDigitK=47;
	}
	
	/* + */
	if(CheckDigitValue == 41)
	{
		CheckDigitK=43;
	}
	
	/* % */
	if(CheckDigitValue == 42)
	{
		CheckDigitK=37;
	  }

	/* ! */
	if(CheckDigitValue == 43) 
	{
		CheckDigitK=33;
	}

	/* # */
	if(CheckDigitValue == 44) 
	{
		CheckDigitK=35;
	}

	/* & */
	if(CheckDigitValue == 45) 
	{
		CheckDigitK=38;
	}

	/* @ */
	if(CheckDigitValue == 46 ) 
	{
		CheckDigitK=64;
	}

	/* Get Printable String */
    sprintf(Printable_String,"(%s%c%c)",str_upr(DataToPrint),CheckDigitC,CheckDigitK);

    iSize = (long)strlen(Printable_String);

    *output = new char [iSize];
    memset(*output, 0, iSize);

    strncpy(*output, Printable_String, strlen(Printable_String));



	return 0;
}


/*****************************************************************/
/* ANSI C Functions for IDAutomation Barcode Fonts               */
/* © Copyright 2002- 2007, IDAutomation.com, Inc.                */
/* All rights reserved.                                          */
/* Redistribution and use of this code in source and/or binary   */
/* forms, with or without modification, are permitted provided   */
/* that: (1) all copies of the source code retain the above      */
/* unmodified copyright notice and this entire unmodified        */
/* section of text, (2) You or Your organization owns a valid    */
/* Developer License to this product from IDAutomation.com       */
/* and, (3) when any portion of this code is bundled in any      */
/* form with an application, a valid notice must be provided     */
/* within the user documentation, start-up screen or in the      */
/* help-about section of the application that specifies          */
/* IDAutomation.com as the provider of the Software bundled      */
/* with the application.                                         */
/*****************************************************************/


long  Postnet (char *Data_To_Encode, char **output, long &iSize)
{
	/* Enter all the numbers without dashes */
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;
	
    char Only_Correct_Data[512];
    memset(&Only_Correct_Data, 0, 512);
    //Only_Correct_Data = new char[512];

    char Printable_String[512];
    memset(&Printable_String, 0, 512);
    //Printable_String = new char[512];
	
	int BufferCounter = 0;
	int  I;
	int  weightedTotal;
	int  CheckDigit;

	/* Check to make sure data is numeric and remove dashes, etc. */
    for(I=1;I <= (int)strlen(Data_To_Encode);I++)
	{
        /* Add all numbers to Only_Correct_Data string */
        if(isdigit(asc(mid(Data_To_Encode,I,1))))
            BufferCounter += sprintf(Only_Correct_Data + BufferCounter,"%s", mid(Data_To_Encode,I,1));
	}

	/* <<<< Calculate Check Digit >>>>.  which is just the sum of the numbers */
	weightedTotal=0;

    for(I = 1;I <= (int)strlen(Only_Correct_Data);I++)
	{		
        weightedTotal += atoi(mid(Only_Correct_Data, I, 1));
	}

	/* Find the CheckDigit by finding the number + weightedTotal that = a multiple of 10 */
	/* divide by 10, get the remainder and subtract from 10 */
	I = (int)((weightedTotal % 10));
	if(	I!= 0)
		CheckDigit=(10-I);
	else
		CheckDigit=0;
	
	BufferCounter = 0;
    BufferCounter += sprintf(Printable_String + BufferCounter,"(%s%i)", Only_Correct_Data, CheckDigit);
	
    iSize = (long)strlen(Printable_String);

    *output = new char [iSize];
    memset(*output, 0, iSize);

    strncpy(*output, Printable_String, strlen(Printable_String));
	

	return 0;
} 


/*****************************************************************/
/* ANSI C Functions for IDAutomation Barcode Fonts               */
/* © Copyright 2002- 2007, IDAutomation.com, Inc.                */
/* All rights reserved.                                          */
/* Redistribution and use of this code in source and/or binary   */
/* forms, with or without modification, are permitted provided   */
/* that: (1) all copies of the source code retain the above      */
/* unmodified copyright notice and this entire unmodified        */
/* section of text, (2) You or Your organization owns a valid    */
/* Developer License to this product from IDAutomation.com       */
/* and, (3) when any portion of this code is bundled in any      */
/* form with an application, a valid notice must be provided     */
/* within the user documentation, start-up screen or in the      */
/* help-about section of the application that specifies          */
/* IDAutomation.com as the provider of the Software bundled      */
/* with the application.                                         */
/*****************************************************************/


long  EAN8 (char *Data_To_Encode, char **output, long &iSize)
{
	/* The purpose of this code is to calculate the EAN-8 barcode */
	/* Enter all the numbers without dashes */
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;
	
    char Data_To_Encode2[512];
    memset(&Data_To_Encode2, 0, 512);
    //Data_To_Encode2 = new char[512];

	char DataToPrint[512];
    memset(&DataToPrint, 0, 512);
	//DataToPrint = new char[512];

    char Only_Correct_Data[512];
    memset(&Only_Correct_Data, 0, 512);
    //Only_Correct_Data = new char[512];

    char Printable_String[512];
    memset(&Printable_String, 0, 512);
    //Printable_String = new char[512];

	int  I;
	int  Factor;
	int  weightedTotal;
	char CurrentChar;
	int  CheckDigit;
    int Print_Buffer = 0;
    sprintf(Data_To_Encode2,"%s",Data_To_Encode);

	/* Check to make sure data is numeric and remove dashes, etc. */
    for(I=1;I <= (int)strlen(Data_To_Encode2);I++)
	{
        /* Add all numbers to Only_Correct_Data string */
        if(isdigit(asc(mid(Data_To_Encode2,I,1))))
		{
            Print_Buffer += sprintf(Only_Correct_Data + Print_Buffer,"%c", asc(mid(Data_To_Encode2,I,1)));
		}
	}

    if(strlen(Only_Correct_Data) >= 7)
        sprintf(Data_To_Encode2,"%s",mid(Only_Correct_Data, 1, 7));
	else
        sprintf(Data_To_Encode2,"%s","0005000");
	
	/* <<<< Calculate Check Digit >>>> */
	Factor=3;
	weightedTotal=0;
    for(I = (int)strlen(Data_To_Encode2);I >= 1;I = I +- 1)
	{
		/* Get the value of each number starting at the end */
        CurrentChar=asc(mid(Data_To_Encode2,I,1));
		/* multiply by the weighting factor which is 3,1,3,1... */
		/* and add the sum together */
		weightedTotal=weightedTotal+CurrentChar*Factor;
		/* change factor for next calculation */
		Factor=4-Factor;
	}

	/* Find the CheckDigit by finding the number + weightedTotal that = a multiple of 10 */
	/* divide by 10, get the remainder and subtract from 10 */
	I = (int)(weightedTotal % 10);
	if(I!=0)
		CheckDigit=(10-I);
	else
		CheckDigit=0;

    sprintf(Data_To_Encode2,"%s%i", Data_To_Encode2, CheckDigit);

	/* Now that have the total number including the check digit, determine character to print */
	/* for proper barcoding */
    Print_Buffer = 0;
    for(I = 1;I <= (int)strlen(Data_To_Encode2);I++)
	{

		/* Get the ASCII value of each number */
        CurrentChar = asc(mid(Data_To_Encode2,I,1));

		/* Print different barcodes according to the location of the CurrentChar and CurrentEncoding */
		if(I==1)
		{
			/* For the first character print the normal guard pattern */
			/* and then the barcode without the human readable character */
            Print_Buffer += sprintf(DataToPrint + Print_Buffer,"(%c",CurrentChar);
		}
		if(I==2)
		{
            Print_Buffer += sprintf(DataToPrint + Print_Buffer,"%c", CurrentChar);
		}
		if(I==3)
		{
            Print_Buffer += sprintf(DataToPrint + Print_Buffer,"%c", CurrentChar);
		}
		if(I==4)
		{
			/* Print the center guard pattern after the 6th character */
			
            Print_Buffer += sprintf(DataToPrint + Print_Buffer,"%c*", CurrentChar);
		}
		if(I==5)
		{
            Print_Buffer += sprintf(DataToPrint + Print_Buffer,"%c", CurrentChar+27);
		}
		if(I==6)
		{
            Print_Buffer += sprintf(DataToPrint + Print_Buffer,"%c", CurrentChar+27);
		}
		if(I==7)
		{
            Print_Buffer += sprintf(DataToPrint + Print_Buffer,"%c", CurrentChar+27);
		}
		if(I==8)
		{
			/* Print the check digit as 8th character + normal guard pattern */
            Print_Buffer += sprintf(DataToPrint + Print_Buffer,"%c(", CurrentChar+27);
		}		 
	}


	/* Get Printable String */
    sprintf(Printable_String,"%s",DataToPrint);
	
    iSize = (long)strlen(Printable_String);

    *output = new char [iSize];
    memset(*output, 0, iSize);
    strncpy(*output, Printable_String, strlen(Printable_String));

	return 0;
}


/*****************************************************************/
/* ANSI C Functions for IDAutomation Barcode Fonts               */
/* © Copyright 2002- 2007, IDAutomation.com, Inc.                */
/* All rights reserved.                                          */
/* Redistribution and use of this code in source and/or binary   */
/* forms, with or without modification, are permitted provided   */
/* that: (1) all copies of the source code retain the above      */
/* unmodified copyright notice and this entire unmodified        */
/* section of text, (2) You or Your organization owns a valid    */
/* Developer License to this product from IDAutomation.com       */
/* and, (3) when any portion of this code is bundled in any      */
/* form with an application, a valid notice must be provided     */
/* within the user documentation, start-up screen or in the      */
/* help-about section of the application that specifies          */
/* IDAutomation.com as the provider of the Software bundled      */
/* with the application.                                         */
/*****************************************************************/


long  EAN13 (char *Data_To_Encode, char **output, long &iSize)
{
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;
	
	int StringLength = 0;
	int I = 0;					
	int Factor = 3;				
	int weightedTotal = 0;		
	int CurrentChar = 0;		
    int DataToPrint_Buffer = 0;
	int CheckDigit = 0;			
	int LeadingDigit = 0;		
	int CurrentEncoding = 0;	

    char LPrintable_String[512];
    memset(&LPrintable_String, 0, 512);
    //LPrintable_String = new char[512];

    char ActualData_To_Encode[20];
    memset(&ActualData_To_Encode, 0, 20);
    //ActualData_To_Encode = new char[20];

    char DataToPrint[20];
    memset(&DataToPrint, 0, 20);
	//DataToPrint = new char[20];
	
    char EAN2AddOn[3];
    memset(&EAN2AddOn, 0, 3);
	//EAN2AddOn = new char[3];
	
    char Only_Correct_Data[20];
    memset(&Only_Correct_Data, 0, 20);
    //Only_Correct_Data = new char[20];

  char EAN5AddOn[6];
  memset(&EAN5AddOn, 0, 6);
	//EAN5AddOn = new char[6];

  char EANAddOnToPrint[15];		
  memset(&EANAddOnToPrint, 0, 15);
	//EANAddOnToPrint = new char[15];

    char Encoding[20];
    memset(&Encoding, 0, 20);
	//Encoding = new char[20];

    char Temp[512];
    memset(&Temp, 0, 512);
	//Temp = new char[512];
	
    StringLength = (int)strlen(Data_To_Encode);
	for(I = 0;I < StringLength;I++)
	{		
        if(isdigit((int)Data_To_Encode[I]))
		{
            DataToPrint_Buffer += sprintf(Only_Correct_Data + DataToPrint_Buffer, "%c", Data_To_Encode[I]);
		}
	}

    DataToPrint_Buffer = 0;
    if(strlen(Only_Correct_Data) < 12 || strlen(Only_Correct_Data) == 16 || strlen(Only_Correct_Data) > 18)
	{
        sprintf(Only_Correct_Data, "%s", "0000000000000");
	}
	
	
    if(strlen(Only_Correct_Data) == 12 || strlen(Only_Correct_Data) == 13)
	{
        sprintf(LPrintable_String, "%s", mid(Only_Correct_Data, 1, 12));
	}
    else if(strlen(Only_Correct_Data) == 14)
	{
        sprintf(LPrintable_String,"%s", mid(Only_Correct_Data, 1, 12));
        sprintf(EAN2AddOn,"%s", mid(Only_Correct_Data, 13, 2));
	}
    else if(strlen(Only_Correct_Data) == 15)
	{
        sprintf(LPrintable_String,"%s", mid(Only_Correct_Data, 1, 12));
        sprintf(EAN2AddOn,"%s", mid(Only_Correct_Data, 14, 2));
	}
    else if(strlen(Only_Correct_Data) == 17)
	{
        sprintf(LPrintable_String,"%s", mid(Only_Correct_Data, 1, 12));
        sprintf(EAN5AddOn,"%s", mid(Only_Correct_Data, 13, 5));
	}
    else if(strlen(Only_Correct_Data) == 18)
	{
        sprintf(LPrintable_String,"%s", mid(Only_Correct_Data, 1, 12));
        sprintf(EAN5AddOn,"%s", mid(Only_Correct_Data, 14, 5));
	}
	
	
	
	/* <<<< Calculate Check Digit >>>> */
    for(I = (int)strlen(LPrintable_String);I >= 1;I--)
	{
		/* Get the value of each number starting at the end */
        CurrentChar = asc(mid(LPrintable_String,I,1)) - 48;
		/* multiply by the weighting factor which is 3,1,3,1... */
		/* and add the sum together */
		weightedTotal = weightedTotal + (CurrentChar * Factor);
		/* change factor for next calculation */
		Factor = 4 - Factor;
	}

	/* Find the CheckDigitValue by finding the number + weightedTotal that = a multiple of 10 */
	/* divide by 10, get the remainder and subtract from 10 */
	I = (int)(weightedTotal % 10);
	if(I != 0)
		CheckDigit=(10 - I);
	else
		CheckDigit = 0;

	/* Now we must encode the leading digit into the left half of the EAN-13 symbol */
	/* by using variable parity between character sets A and B */
    LeadingDigit = (((asc(mid(LPrintable_String, 1, 1))))-48);
	
	if(LeadingDigit == 0)	
		sprintf(Encoding, "AAAAAACCCCCC");
	else if(LeadingDigit == 1)	
		sprintf(Encoding, "AABABBCCCCCC");
	else if(LeadingDigit == 2)	
		sprintf(Encoding, "AABBABCCCCCC");
	else if(LeadingDigit == 3)
		sprintf(Encoding, "AABBBACCCCCC");
	else if(LeadingDigit == 4)
		sprintf(Encoding, "ABAABBCCCCCC");
	else if(LeadingDigit == 5)
		sprintf(Encoding, "ABBAABCCCCCC");
	else if(LeadingDigit == 6)
		sprintf(Encoding, "ABBBAACCCCCC");
	else if(LeadingDigit == 7)
		sprintf(Encoding, "ABABABCCCCCC");
	else if(LeadingDigit == 8)
		sprintf(Encoding, "ABABBACCCCCC");
	else if(LeadingDigit == 9)
		sprintf(Encoding, "ABBABACCCCCC");

	/* add the check digit to the end of the barcode & remove the leading digit */
    sprintf(ActualData_To_Encode, "%s%i", mid(LPrintable_String, 2, 11), CheckDigit);
	
	/* Now that we have the total number including the check digit, determine character to print */
	/* for proper barcoding: */
    for(I = 1;I <= (int)strlen(ActualData_To_Encode);I++)
	{
		/* Get the ASCII value of each number excluding the first number because */
		/* it is encoded with variable parity */
        CurrentChar = asc(mid(ActualData_To_Encode, I, 1));
		CurrentEncoding = asc(mid(Encoding, I, 1));

		/* Print different barcodes according to the location of the CurrentChar and CurrentEncoding */
		if(CurrentEncoding == 'A')
            DataToPrint_Buffer += sprintf(DataToPrint + DataToPrint_Buffer, "%c", CurrentChar);
		else if(CurrentEncoding == 'B')
            DataToPrint_Buffer += sprintf(DataToPrint + DataToPrint_Buffer, "%c", CurrentChar + 17);
		else if(CurrentEncoding=='C')
            DataToPrint_Buffer += sprintf(DataToPrint + DataToPrint_Buffer, "%c", CurrentChar + 27);

		/* add in the 1st character along with guard patterns */
		if(I == 1)
		{
            DataToPrint_Buffer = 0;
			/* For the LeadingDigit print the human readable character, */
			/* the normal guard pattern and then the rest of the barcode */
			sprintf(Temp,"%i",LeadingDigit);
			if(LeadingDigit > 4)
			{
				sprintf(Temp,"%c(%s",asc(Temp) + 64, DataToPrint);
                DataToPrint_Buffer += sprintf(DataToPrint + DataToPrint_Buffer, "%s", Temp);
			}
			else if(LeadingDigit < 5)
			{
				sprintf(Temp,"%c(%s",asc(Temp) + 37, DataToPrint);
                DataToPrint_Buffer += sprintf(DataToPrint + DataToPrint_Buffer,"%s",Temp);
			}
		}
		else if(I==6)
		{
			/* Print the center guard pattern after the 6th character */
            DataToPrint_Buffer += sprintf(DataToPrint + DataToPrint_Buffer, "*");
		}
		else if(I==12)
		{
			/* For the last character (12) print the normal guard pattern */
			/* after the barcode */
            DataToPrint_Buffer += sprintf(DataToPrint + DataToPrint_Buffer, "(");
		}
	}

	/* Process 5 digit add on if it exists */
	if(strlen(EAN5AddOn) == 5)
	{
		/* Get check digit for add on */
		Factor = 3;
		weightedTotal=0;
		for(I = (int)strlen(EAN5AddOn);I >= 1;I--)
		{
			/* Get the value of each number starting at the end */
			CurrentChar = asc(mid(EAN5AddOn, I, 1)) - 48;
			/* multiply by the weighting factor which is 3,9,3,9. */
			/* and add the sum together */
			if(Factor == 3)
				weightedTotal = weightedTotal + (CurrentChar * 3);
			else if(Factor == 1)
				weightedTotal = weightedTotal + CurrentChar * 9;

			/* change factor for next calculation */
			Factor = 4 - Factor;
		}
		/* Find the CheckDigit by extracting the right-most number from weightedTotal */
		sprintf(Temp,"%i",weightedTotal);
		CheckDigit = asc(mid(Temp,(int)strlen(Temp), 1)) - 48;

		/* Now we must encode the add-on CheckDigit into the number sets */
		/* by using variable parity between character sets A and B */		
		if(CheckDigit == 0)			
			sprintf(Encoding,"BBAAA");
		else if(CheckDigit == 1)			
			sprintf(Encoding,"BABAA");
		else if(CheckDigit == 2)			
			sprintf(Encoding,"BAABA");
		else if(CheckDigit == 3)			
			sprintf(Encoding,"BAAAB");
		else if(CheckDigit == 4)
			sprintf(Encoding,"ABBAA");
		else if(CheckDigit == 5)			
			sprintf(Encoding,"AABBA");
		else if(CheckDigit == 6)			
			sprintf(Encoding,"AAABB");
		else if(CheckDigit == 7)			
			sprintf(Encoding,"ABABA");
		else if(CheckDigit == 8)			
			sprintf(Encoding,"ABAAB");
		else if(CheckDigit == 9)			
			sprintf(Encoding,"AABAB");

		/* Now that we have the total number including the check digit, determine character to print */
		/* for proper barcoding: */
        DataToPrint_Buffer = 0;
		for(I = 1;I <= (int)strlen(EAN5AddOn);I++)
		{
			/* Get the value of each number.  It is encoded with variable parity */
			CurrentChar = asc(mid(EAN5AddOn,I,1)) - 48;
			CurrentEncoding = asc(mid(Encoding, I, 1));
			if(I == 1)
                DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c%c", 32, 43);

			/* Print different barcodes according to the location of the CurrentChar and CurrentEncoding */
			if(CurrentEncoding == 'A')
			{
				if(CurrentChar == 0)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 34);
				else if(CurrentChar == 1)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 35);
				else if(CurrentChar == 2)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 36);
				else if(CurrentChar == 3)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 37);
				else if(CurrentChar == 4)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 38);
				else if(CurrentChar == 5)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 44);
				else if(CurrentChar == 6)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 46);
				else if(CurrentChar == 7)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 47);
				else if(CurrentChar == 8)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 58);
				else if(CurrentChar == 9)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 59);
			} 

			if(CurrentEncoding == 'B')
			{
				if(CurrentChar == 0)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 122);
				else if(CurrentChar == 1)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 61);
				else if(CurrentChar == 2)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 63);
				else if(CurrentChar == 3)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 64);
				else if(CurrentChar == 4)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 91);
				else if(CurrentChar == 5)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 92);
				else if(CurrentChar == 6)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 93);
				else if(CurrentChar == 7)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 95);
				else if(CurrentChar == 8)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 123);
				else if(CurrentChar == 9)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 125);
			}

			/* add in the space & add-on guard pattern */
			if(I==1)
			{
				/* Now print add-on delineators between each add-on character */
                DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 33);
				
				
			}
			else if(I == 2)
                DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 33);
				
			else if(I == 3)
                DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 33);
			
			else if(I == 4)
                DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 33);
				
			else if(I == 5)
			{
			}
		} 
	} 

	/* Process 2 digit add on if it exists */
	if(strlen(EAN2AddOn)==2)
	{
		int Tempi = atoi(mid(EAN2AddOn, 1, 2));
		
		/* Get encoding for add on */
		for(I = 0;I <= 99;I = I + 4)
		{
			if(Tempi == I)
				sprintf(Encoding, "AA");
			if(Tempi == I + 1)
				sprintf(Encoding, "AB");
			if(Tempi == I + 2)
				sprintf(Encoding, "BA");
			if(Tempi == I + 3)
				sprintf(Encoding, "BB");
		}
		/* Now that we have the total number including the encoding */
		/* determine what to print */
        DataToPrint_Buffer = 0;
		for(I = 1;I <= (int)strlen(EAN2AddOn);I++)
		{
			/* Get the value of each number. It is encoded with variable parity */
			if(I == 1)
                DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, " %c", 43);

			CurrentChar = asc(mid(EAN2AddOn, I, 1)) - 48;
			CurrentEncoding = asc(mid(Encoding, I, 1));
			/* Print different barcodes according to the location of the CurrentChar and CurrentEncoding */
			if(CurrentEncoding == 'A')
			{
				if(CurrentChar == 0)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 34);
				else if(CurrentChar == 1)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 35);
				else if(CurrentChar == 2)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 36);
				else if(CurrentChar == 3)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 37);
				else if(CurrentChar == 4)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 38);
				else if(CurrentChar == 5)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 44);
				else if(CurrentChar == 6)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 46);
				else if(CurrentChar == 7)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 47);
				else if(CurrentChar == 8)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 58);
				else if(CurrentChar == 9)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 59);
			} 
			if(CurrentEncoding == 'B')
			{
				if(CurrentChar == 0)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 122);
				else if(CurrentChar == 1)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 61);
				else if(CurrentChar == 2)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 63);
				else if(CurrentChar == 3)
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 64);
				else if(CurrentChar == 4)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 91);
				else if(CurrentChar == 5)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 92);
				else if(CurrentChar == 6)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 93);
				else if(CurrentChar == 7)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 95);
				else if(CurrentChar == 8)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 123);
				else if(CurrentChar == 9)				
                    DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 125);
			}
			
            /* add in the space & add-on guard pattern */
			if(I==1)
			{
                DataToPrint_Buffer += sprintf(EANAddOnToPrint + DataToPrint_Buffer, "%c", 33);
			}
		}
	} 
	
  //  qDebug(QString::number(DataToPrint_Buffer).toLocal8Bit());
	
    //LPrintable_String[512];
	if(strlen(EAN2AddOn) == 2 || strlen(EAN5AddOn) == 5)
        sprintf(LPrintable_String, "%s%s", DataToPrint, EANAddOnToPrint);
	else
        sprintf(LPrintable_String, "%s", DataToPrint);

    iSize = (long)strlen(LPrintable_String);
    *output = new char [iSize];
    memset(*output, 0, iSize);
    //qDebug(LPrintable_String);
    strncpy(*output, LPrintable_String, strlen(LPrintable_String));

	return 0;
}


/*****************************************************************/
/* ANSI C Functions for IDAutomation Barcode Fonts               */
/* © Copyright 2002- 2007, IDAutomation.com, Inc.                */
/* All rights reserved.                                          */
/* Redistribution and use of this code in source and/or binary   */
/* forms, with or without modification, are permitted provided   */
/* that: (1) all copies of the source code retain the above      */
/* unmodified copyright notice and this entire unmodified        */
/* section of text, (2) You or Your organization owns a valid    */
/* Developer License to this product from IDAutomation.com       */
/* and, (3) when any portion of this code is bundled in any      */
/* form with an application, a valid notice must be provided     */
/* within the user documentation, start-up screen or in the      */
/* help-about section of the application that specifies          */
/* IDAutomation.com as the provider of the Software bundled      */
/* with the application.                                         */
/*****************************************************************/


long  UPCa (char *Data_To_Encode, char **output, long &iSize)
{
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;
	
	char DataToPrint[512];
    memset(&DataToPrint, 0, 512);
    char Data_To_Encode2[512];
    memset(&Data_To_Encode2, 0, 512);
    char Only_Correct_Data[512];
    memset(&Only_Correct_Data, 0, 512);
	char EAN2AddOn[512];
    memset(&EAN2AddOn, 0, 512);
	char EAN5AddOn[512];
    memset(&EAN5AddOn, 0, 512);
	char EANAddOnToPrint[512];
    memset(&EANAddOnToPrint, 0, 512);
	char Encoding[512];
    memset(&Encoding, 0, 512);
	char Temp[512];
    memset(&Temp, 0, 512);
    static char Printable_String[512];
    memset(&Printable_String, 0, 512);
	char CurrentChar;
	char CurrentEncoding;
	int  Factor;
	int  weightedTotal;
	int  CheckDigit;
	int  Tempi;
	int  I;

	
	DataToPrint[0]=(char)0;
    Data_To_Encode2[0]=(char)0;
    Only_Correct_Data[0]=(char)0;
	EAN2AddOn[0]=(char)0;
	EAN5AddOn[0]=(char)0;
	EANAddOnToPrint[0]=(char)0;
	Encoding[0]=(char)0;
	Temp[0]=(char)0;
    Printable_String[0]=(char)0;

    sprintf(Data_To_Encode2,"%s",Data_To_Encode);
	/* Check to make sure data is numeric and remove dashes, etc. */
    for(I=1;I <= (int)strlen(Data_To_Encode2);I++)
	{
        /* Add all numbers to Only_Correct_Data string */
        if(isdigit(asc(mid(Data_To_Encode2,I,1))))
		{
            sprintf(Only_Correct_Data,"%s%s", Only_Correct_Data, mid(Data_To_Encode2,I,1));
		}
	}

	/* Remove check digits if they added one */
    if(strlen(Only_Correct_Data)==12)
	{
        sprintf(Only_Correct_Data,"%s",mid(Only_Correct_Data,1,11));
	}
    if(strlen(Only_Correct_Data)==14)
	{
        sprintf(Only_Correct_Data,"%s%s",mid(Only_Correct_Data,1,11),mid(Only_Correct_Data,13,2));
	}
    if(strlen(Only_Correct_Data)==17)
	{
        sprintf(Only_Correct_Data,"%s%s",mid(Only_Correct_Data,1,11),mid(Only_Correct_Data,13,5));
	}
    if(strlen(Only_Correct_Data)==16)
	{
        sprintf(EAN5AddOn,"%s",mid(Only_Correct_Data,12,5));
	}
    if(strlen(Only_Correct_Data)==13)
	{
        sprintf(EAN2AddOn,"%s",mid(Only_Correct_Data,12,2));
	}

    if(strlen(Only_Correct_Data) != 11 && strlen(Only_Correct_Data) != 12 && strlen(Only_Correct_Data) != 14 &&
        strlen(Only_Correct_Data) != 17 && strlen(Only_Correct_Data) != 16 && strlen(Only_Correct_Data) != 13)
	{
        sprintf(Only_Correct_Data,"%s","00000000000");
	}

	/* split 12 digit number from add-on */
    sprintf(Data_To_Encode2,"%s",mid(Only_Correct_Data,1,11));

	/* <<<< Calculate Check Digit >>>> */
	Factor=3;
	weightedTotal=0;
    for(I = (int)strlen(Data_To_Encode2);I >= 1;I = I + -1)
	{
		/* Get the value of each number starting at the end */
        CurrentChar=(asc(mid(Data_To_Encode2,I,1)))-48;

		/* multiply by the weighting factor which is 3,1,3,1... */
		/* and add the sum together */
		weightedTotal=weightedTotal+CurrentChar*Factor;

		/* change factor for next calculation */
		Factor=4-Factor;
	}

	/* Find the CheckDigit by finding the number + weightedTotal that = a multiple of 10 */
	/* divide by 10, get the remainder and subtract from 10 */
	I = (int)(weightedTotal % 10);
	if(I!=0)
	{
		CheckDigit=(10-I);
	}
	else
	{
		CheckDigit=0;
	}

    sprintf(Data_To_Encode2,"%s%i", Data_To_Encode2, CheckDigit);
	/* Now that have the total number including the check digit, determine character to print */
	/* for proper barcoding */
    for(I=1;I <= (int)strlen(Data_To_Encode2);I++)
	{
		/* Get the ASCII value of each number */
        CurrentChar=asc(mid(Data_To_Encode2,I,1));
		/* Print different barcodes according to the location of the CurrentChar */
		for(;;)
		{
			if(I==1)
			{
				/* For the first character print the human readable character, the normal */
				/* guard pattern and then the barcode without the human readable character */
				if((CurrentChar-48)>4)
				{
					sprintf(DataToPrint,"%c(%c",CurrentChar+64,CurrentChar+49);
				}
				if((CurrentChar-48)<5)
				{
					sprintf(DataToPrint,"%c(%c",CurrentChar+37,CurrentChar+49);
				}
				break;
			}
			if(I==2)
			{
                sprintf(DataToPrint,"%s%c", DataToPrint, CurrentChar);
				break;
			}
			if(I==3)
			{
                sprintf(DataToPrint,"%s%c", DataToPrint, CurrentChar);
				break;
			}
			if(I==4)
			{
                sprintf(DataToPrint,"%s%c",DataToPrint,CurrentChar);
				break;
			}
			if(I==5)
			{
                sprintf(DataToPrint,"%s%c",DataToPrint,CurrentChar);
				break;
			}
			if(I==6)
			{
				/* Print the center guard pattern after the 6th character */
                sprintf(DataToPrint,"%s%c*",DataToPrint,CurrentChar);
				break;
			}
			if(I==7)
			{
				/* Add 27 to the ASII value of characters 6-12 to print from character set+ C */
				/* this is required when printing to the right of the center guard pattern */
                sprintf(DataToPrint,"%s%c",DataToPrint,CurrentChar+27);
				break;
			}
			if(I==8)
			{
                sprintf(DataToPrint,"%s%c",DataToPrint,CurrentChar+27);
				break;
			}
			if(I==9)
			{
                sprintf(DataToPrint,"%s%c",DataToPrint,CurrentChar+27);
				break;
			}
			if(I==10)
			{
                sprintf(DataToPrint,"%s%c",DataToPrint,CurrentChar+27);
				break;
			}
			if(I==11)
			{
                sprintf(DataToPrint,"%s%c",DataToPrint,CurrentChar+27);
				break;
			}
			if(I==12)
			{
				/* For the last character print the barcode without the human readable character, */
				/* the normal guard pattern and then the human readable character. */
				if((CurrentChar-48)>4)
				{
                    sprintf(DataToPrint,"%s%c(%c",DataToPrint,CurrentChar+59,CurrentChar+64);
				}
				if((CurrentChar-48)<5)
				{
                    sprintf(DataToPrint,"%s%c(%c",DataToPrint,CurrentChar+59,CurrentChar+37);
				}
			}
			break;
		} 
	}

	/* Process 5 digit add on if it exists */
	if(strlen(EAN5AddOn)==5)
	{
		/* Get check digit for add on */
		Factor=3;
		weightedTotal=0;
		for(I = (int)strlen(EAN5AddOn);I >= 1;I--)
		{
			/* Get the value of each number starting at the end */
			CurrentChar=asc(mid(EAN5AddOn,I,1))-48;
			/* multiply by the weighting factor which is 3,9,3,9. */
			/* and add the sum together */
			if(Factor==3)
			{
				weightedTotal=weightedTotal+(CurrentChar*3);
			}
			if(Factor==1)
			{
				weightedTotal=weightedTotal+CurrentChar*9;
			}
			/* change factor for next calculation */
			Factor=4-Factor;
		}

		/* Find the CheckDigit by extracting the right-most number from weightedTotal */
		sprintf(Temp,"%i",weightedTotal);
		CheckDigit = asc(mid(Temp, (int)strlen(Temp), 1)) - 48;

		/* Now we must encode the add-on CheckDigit into the number sets */
		/* by using variable parity between character sets A and B */
		for(;;)
		{
			if(CheckDigit==0)
			{
				sprintf(Encoding,"BBAAA");
				break;
			}
			if(CheckDigit==1)
			{
				sprintf(Encoding,"BABAA");
				break;
			}
			if(CheckDigit==2)
			{
				sprintf(Encoding,"BAABA");
				break;
			}
			if(CheckDigit==3)
			{
				sprintf(Encoding,"BAAAB");
				break;
			}
			if(CheckDigit==4)
			{
				sprintf(Encoding,"ABBAA");
				break;
			}
			if(CheckDigit==5)
			{
				sprintf(Encoding,"AABBA");
				break;
			}
			if(CheckDigit==6)
			{
				sprintf(Encoding,"AAABB");
				break;
			}
			if(CheckDigit==7)
			{
				sprintf(Encoding,"ABABA");
				break;
			}
			if(CheckDigit==8)
			{
				sprintf(Encoding,"ABAAB");
				break;
			}
			if(CheckDigit==9)
			{
				sprintf(Encoding,"AABAB");
			}
			break;
		}


		/* Now that we have the total number including the check digit, determine character to print */
		/* for proper barcoding: */
		for(I=1;I <= (int)strlen(EAN5AddOn);I++)
		{
			/* Get the value of each number */
			/* it is encoded with variable parity */
			CurrentChar=asc(mid(EAN5AddOn,I,1))-48;
			CurrentEncoding=asc(mid(Encoding,I,1));

			/* Print different barcodes according to the location of the CurrentChar and CurrentEncoding */
			for(;;)
			{
				if(CurrentEncoding=='A')
				{
					if(CurrentChar==0)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,34);
					}
					if(CurrentChar==1)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,35);
					}
					if(CurrentChar==2)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,36);
					}
					if(CurrentChar==3)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,37);
					}
					if(CurrentChar==4)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,38);
					}
					if(CurrentChar==5)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,44);
					}
					if(CurrentChar==6)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,46);
					}
					if(CurrentChar==7)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,47);
					}
					if(CurrentChar==8)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,58);
					}
					if(CurrentChar==9)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,59);
					}
					break;
				}
				if(CurrentEncoding=='B')
				{
					if(CurrentChar==0)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,122);
					}
					if(CurrentChar==1)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,61);
					}
					if(CurrentChar==2)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,63);
					}
					if(CurrentChar==3)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,64);
					}
					if(CurrentChar==4)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,91);
					}
					if(CurrentChar==5)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,92);
					}
					if(CurrentChar==6)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,93);
					}
					if(CurrentChar==7)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,95);
					}
					if(CurrentChar==8)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,123);
					}
					if(CurrentChar==9)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,125);
					}
				}
				
				break;
			}

			/* add in the space & add-on guard pattern */
			for(;;)
			{
				if(I==1)
				{
                    sprintf(Temp,"%c%s%c",43,EANAddOnToPrint,33);
                    sprintf(EANAddOnToPrint,"%s",Temp);
					/* Now print add-on delineators between each add-on character */
					break;
				}
				if(I==2)
				{
                    sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,33);
					break;
				}
				if(I==3)
				{
                    sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,33);
					break;
				}
				if(I==4)
				{
                    sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,33);
					break;
				}
				if(I==5)
				{
				}
				break;
			}
		}
	} 

	/* Process 2 digit add on if it exists */
	if(strlen(EAN2AddOn)==2)
	{

		Tempi=(((asc(mid(EAN2AddOn,1,1))))-48)*10;
		Tempi=(Tempi + (asc(mid(EAN2AddOn,2,1))))-48;

		/* Get encoding for add on */
		for(I=0;I<=99;I=I+4)
		{
			if(Tempi==I)
			{
				sprintf(Encoding,"AA");
			}
			if(Tempi==I+1)
			{
				sprintf(Encoding,"AB");
			}
			if(Tempi==I+2)
			{
				sprintf(Encoding,"BA");
			}
			if(Tempi==I+3)
			{
				sprintf(Encoding,"BB");
			}
		}

		/* Now that we have the total number including the encoding */
		/* determine what to print */
		for(I=1;I <= (int)strlen(EAN2AddOn);I++)
		{
			/* Get the value of each number */
			/* it is encoded with variable parity */
			CurrentChar=asc(mid(EAN2AddOn,I,1))-48;
			CurrentEncoding=asc(mid(Encoding,I,1));

			/* Print different barcodes according to the location of the CurrentChar and CurrentEncoding */
			for(;;)
			{
				if(CurrentEncoding=='A')
				{
					if(CurrentChar==0)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,34);
					}
					if(CurrentChar==1)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,35);
					}
					if(CurrentChar==2)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,36);
					}
					if(CurrentChar==3)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,37);
					}
					if(CurrentChar==4)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,38);
					}
					if(CurrentChar==5)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,44);
					}
					if(CurrentChar==6)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,46);
					}
					if(CurrentChar==7)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,47);
					}
					if(CurrentChar==8)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,58);
					}
					if(CurrentChar==9)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,59);
					}
					break;
				}
				if(CurrentEncoding=='B')
				{
					if(CurrentChar==0)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,122);
					}
					if(CurrentChar==1)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,61);
					}
					if(CurrentChar==2)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,63);
					}
					if(CurrentChar==3)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,64);
					}
					if(CurrentChar==4)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,91);
					}
					if(CurrentChar==5)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,92);
					}
					if(CurrentChar==6)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,93);
					}
					if(CurrentChar==7)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,95);
					}
					if(CurrentChar==8)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,123);
					}
					if(CurrentChar==9)
					{
                        sprintf(EANAddOnToPrint,"%s%c",EANAddOnToPrint,125);
					}
				}
				break;
			}

			/* add in the space & add-on guard pattern */
			for(;;)
			{
				if(I==1)
				{
                    sprintf(Temp,"%c%s%c",43,EANAddOnToPrint,33);
                    sprintf(EANAddOnToPrint,"%s",Temp);
					/* Now print add-on delineators between each add-on character */
					break;
				}
				if(I==2)
				{

				}
				break;
			}
		}
	}


	/* Get Printable String */
    sprintf(Printable_String,"%s%s", DataToPrint, EANAddOnToPrint);
	
    iSize = (long)strlen(Printable_String);
    *output = new char [iSize];
    memset(*output, 0, iSize);
    strncpy(*output, Printable_String, strlen(Printable_String));
	return 0;
} 


/*****************************************************************/
/* ANSI C Functions for IDAutomation Barcode Fonts               */
/* © Copyright 2002- 2007, IDAutomation.com, Inc.                */
/* All rights reserved.                                          */
/* Redistribution and use of this code in source and/or binary   */
/* forms, with or without modification, are permitted provided   */
/* that: (1) all copies of the source code retain the above      */
/* unmodified copyright notice and this entire unmodified        */
/* section of text, (2) You or Your organization owns a valid    */
/* Developer License to this product from IDAutomation.com       */
/* and, (3) when any portion of this code is bundled in any      */
/* form with an application, a valid notice must be provided     */
/* within the user documentation, start-up screen or in the      */
/* help-about section of the application that specifies          */
/* IDAutomation.com as the provider of the Software bundled      */
/* with the application.                                         */
/*****************************************************************/


long  MSI (char *Data_To_Encode, char **output, long &iSize)
{
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;
	
	char DataToPrint[512];	
    memset(&DataToPrint, 0, 512);
	//DataToPrint = new char[512];

    char Only_Correct_Data[512];
    memset(&Only_Correct_Data, 0, 512);
    //Only_Correct_Data = new char[512];

    int sPrint_Buffer = 0;
	int  I;

	
    for(I = 1;I <= (int)strlen(Data_To_Encode);I++)
	{
		
        if(isdigit(asc(mid(Data_To_Encode, I, 1))))
		{
            sPrint_Buffer += sprintf(Only_Correct_Data + sPrint_Buffer, "%s", mid(Data_To_Encode, I, 1));
		}
	}

	
    sprintf(DataToPrint,"(%s%s)", Only_Correct_Data, Calc_MSI_Check_Digit(Only_Correct_Data));

	iSize = (long)strlen(DataToPrint); 

    *output = new char [iSize];
    memset(*output, 0, iSize);

    strncpy(*output, DataToPrint, strlen(DataToPrint));



	return 0;
} 

char *Calc_MSI_Check_Digit(char Data_To_Encode[])
{
    int StringLength = (int)strlen(Data_To_Encode);
    char OddNumbers[30];
    memset(&OddNumbers, 0, 30);
		char sOddNumberProduct[8];
        memset(&sOddNumberProduct, 0, 8);
    int OddDigit = TRUE_;
   //	int OddDigit = 1;
    int EvenNumberSum = 0;
		int BufferCounterOdd = 0;
        //int BufferCounterEven = 0;
		int OddNumberProduct = 0;
		int OddNumberSum = 0;
		int CD = 0;
		int Idx = 0;
		static char sCD[1];

	for(Idx = StringLength;Idx >= 0;Idx--)
	{
        if(OddDigit == TRUE_)
		{
            BufferCounterOdd += sprintf(OddNumbers + BufferCounterOdd, "%s", mid(Data_To_Encode, Idx, 1));
            //OddDigit = FALSE_;
			OddDigit = 0;
		}
		else
		{
            EvenNumberSum += atoi(mid(Data_To_Encode, Idx, 1));
            //OddDigit = TRUE_;
			OddDigit = 1;
		}
	}
        
   
    OddNumberProduct = atoi(OddNumbers) * 2;

    
	sprintf(sOddNumberProduct, "%d", OddNumberProduct);
	StringLength = (int)strlen(sOddNumberProduct);
    OddNumberSum = 0;

	for(Idx = 1;Idx <= StringLength;Idx++)
	{
		OddNumberSum += atoi(mid(sOddNumberProduct, Idx, 1));
	}
   
    
    
    OddNumberSum = OddNumberSum + EvenNumberSum;
    
    
    
    OddNumberSum = OddNumberSum % 10;
	if(OddNumberSum != 0)
		CD = 10 - OddNumberSum;
	else
		CD = 0;

	sprintf(sCD, "%i", CD);

	return sCD;

}


long  Code128HumanReadable (char *Data_To_Encode, int ApplyTilde, char **output, long &iSize)
{
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;

    char ReadableText[512];
    memset(&ReadableText, 0, 512);
	//ReadableText = new char[512];

	char StringToCheckFiltered[512];	
    memset(&StringToCheckFiltered, 0, 512);
	//StringToCheckFiltered = new char[512];

	char *RevFilteredString;	
	int BufferCounter = 0;		
	int BufferCounter2 = 0;		
	int i = 0;					
	int q = 0;					
	int GoodData = 0;			
	int FNCDone;
	
    for(i = 0;i < (int)strlen(Data_To_Encode);i++)
	{
        FNCDone = FALSE_;
        if((int)asc(mid(Data_To_Encode, i + 1, 1)) != 126 || ApplyTilde == FALSE_ || strlen(Data_To_Encode) < 4)
		{
			
            if(asc(mid(Data_To_Encode, i + 1, 1)) == 212 && i <= (int)strlen(Data_To_Encode) - 3)			{
				
                BufferCounter += sprintf(ReadableText + BufferCounter, "%s%s%s"," (", mid(Data_To_Encode, i + 2, 2), ") ");
				i += 2;
			}
            else if(asc(mid(Data_To_Encode, i + 1, 1)) == 213 && i <= (int)strlen(Data_To_Encode) - 4)
			{
                BufferCounter += sprintf(ReadableText + BufferCounter, "%s%s%s"," (", mid(Data_To_Encode, i + 2, 3), ") ");
				i += 3;
			}
            else if(asc(mid(Data_To_Encode, i + 1, 1)) == 214 && i <= (int)strlen(Data_To_Encode) - 5)
			{
                BufferCounter += sprintf(ReadableText + BufferCounter, "%s%s%s"," (", mid(Data_To_Encode, i + 2, 4), ") ");
				i += 4;
			}
            else if(asc(mid(Data_To_Encode, i + 1, 1)) == 215 && i <= (int)strlen(Data_To_Encode) - 6)
			{
                BufferCounter += sprintf(ReadableText + BufferCounter, "%s%s%s"," (", mid(Data_To_Encode, i + 2, 5), ") ");
				i += 5;
			}
            else if(asc(mid(Data_To_Encode, i + 1, 1)) == 216 && i <= (int)strlen(Data_To_Encode) - 7)
			{
                BufferCounter += sprintf(ReadableText + BufferCounter, "%s%s%s"," (", mid(Data_To_Encode, i + 2, 6), ") ");
				i += 6;
			}
            else if(asc(mid(Data_To_Encode, i + 1, 1)) == 217 && i <= (int)strlen(Data_To_Encode) - 8)
			{
                BufferCounter += sprintf(ReadableText + BufferCounter, "%s%s%s"," (", mid(Data_To_Encode, i + 2, 7), ") ");
				i += 7;
			}
            else if(asc(mid(Data_To_Encode, i + 1, 1)) == 202 && i <= (int)strlen(Data_To_Encode) - 4 &&
                isdigit((int) Data_To_Encode[i + 1]) && isdigit((int) Data_To_Encode[i + 2]))
			{
				
				
                if(atoi(mid(Data_To_Encode, i + 2, 2)) == 80 || atoi(mid(Data_To_Encode, i + 2, 2)) == 81 ||
                    (atoi(mid(Data_To_Encode, i + 2, 2)) <= 34 && atoi(mid(Data_To_Encode, i + 2, 2)) >= 31))
				{
                    BufferCounter += sprintf(ReadableText + BufferCounter, "%s%s%s"," (", mid(Data_To_Encode, i + 2, 4), ") ");
					i += 4;
                    FNCDone = TRUE_;
				}

				
                if(FNCDone == FALSE_)
				{					
                    if((atoi(mid(Data_To_Encode, i + 2, 2)) <= 49 && atoi(mid(Data_To_Encode, i + 2, 2)) >= 40) ||
                        (atoi(mid(Data_To_Encode, i + 2, 2)) <= 25 && atoi(mid(Data_To_Encode, i + 2, 2)) >= 23))
					{
                        BufferCounter += sprintf(ReadableText + BufferCounter, "%s%s%s"," (", mid(Data_To_Encode, i + 2, 3), ") ");
						i += 3;
                        FNCDone = TRUE_;
					}
				}

				
                if(FNCDone == FALSE_)
				{					
                    if((atoi(mid(Data_To_Encode, i + 2, 2)) <= 30 && atoi(mid(Data_To_Encode, i + 2, 2)) >= 0) ||
                        (atoi(mid(Data_To_Encode, i + 2, 2)) <= 99 && atoi(mid(Data_To_Encode, i + 2, 2)) >= 90))
					{
                        BufferCounter += sprintf(ReadableText + BufferCounter, "%s%s%s"," (", mid(Data_To_Encode, i + 2, 2), ") ");
						i += 2;
                        FNCDone = TRUE_;
					}

				}
				
                if(FNCDone == FALSE_)
				{
                    BufferCounter += sprintf(ReadableText + BufferCounter, "%s%s%s"," (", mid(Data_To_Encode, i + 4, 4), ") ");
					i += 4;
                    FNCDone = TRUE_;
				}
			}
            else if(asc(mid(Data_To_Encode, i + 1, 1)) < 32)
				BufferCounter += sprintf(ReadableText + BufferCounter, "%s", " ");
            else if(asc(mid(Data_To_Encode, i + 1, 1)) >= 32 && asc(mid(Data_To_Encode, i + 1, 1)) <= 126)
                BufferCounter += sprintf(ReadableText + BufferCounter, "%s", mid(Data_To_Encode, i + 1, 1));
		}
		else
		{
			
			
            int char0 = (int) asc(mid(Data_To_Encode, i + 2, 1));
            int char1 = (int) asc(mid(Data_To_Encode, i + 3, 1));
            int char2 = (int) asc(mid(Data_To_Encode, i + 4, 1));
            if (i <= (int)strlen(Data_To_Encode) - 3 && isdigit(char0) && isdigit(char1)
				&& isdigit(char2) )
			{
				
				
//				char *tempNum = mid(Data_To_Encode,i + 2, 3);
                int convertedValue = atoi(mid(Data_To_Encode, i + 2, 3));
				if((convertedValue > 0 && convertedValue < 32))
				{
					
					BufferCounter += sprintf(ReadableText + BufferCounter, "%s", " ");
					i = i + 3;
				}
				else if(convertedValue == 197)
					i = i + 3;
				else if(convertedValue != 197) 
                    BufferCounter += sprintf(ReadableText + BufferCounter, "%c", Data_To_Encode[i]);
			} 			
			
            else if(((int)strlen(Data_To_Encode) > 5 && i > 1 && i <= (int)strlen(Data_To_Encode) - 3) && (int) Data_To_Encode[i + 1] == 109)
			{
				
                if(isdigit(Data_To_Encode[i + 2]) && isdigit(Data_To_Encode[i + 3]))
				{
                    int NumCharsToConvert = atoi(mid(Data_To_Encode, i + 3, 2));
					
					if(i - (NumCharsToConvert - 1) >= 0)
					{
						
                        char *StringToCheck = mid(Data_To_Encode, i - NumCharsToConvert + 1, NumCharsToConvert);
						
						int NumCharsFiltered = 0;
						
						GoodData = 0;
						BufferCounter2 = 0;
						
						for(q = (int)strlen(StringToCheck);q > 0 ;q--)
						{						
                            GoodData = FALSE_;
							if((int)StringToCheck[q - 1] > 0 && (int)StringToCheck[q - 1] <= 126  && isdigit((int)StringToCheck[q - 1]))								
                                GoodData = TRUE_;;
							
							if(GoodData)
							{
								if(NumCharsFiltered < NumCharsToConvert )
								{
									BufferCounter2 += sprintf(StringToCheckFiltered + BufferCounter2, "%s", mid(StringToCheck, q, 1));
									NumCharsFiltered++;
								}
								else 
									break;
							}	
						}
						
						
                        if(strcmp(StringToCheckFiltered, "") == 0)
                            GoodData = FALSE_;
						else
						{
                            GoodData = TRUE_;
                            RevFilteredString = str_rev(StringToCheckFiltered);
						}

                        if(GoodData != FALSE_)
						{  
                            BufferCounter += sprintf(ReadableText + BufferCounter, "%c", Find_Mod_10_Digit(RevFilteredString));
							i += 3;  
						}
						else 
                            BufferCounter += sprintf(ReadableText + BufferCounter, "%c", Data_To_Encode[i]);
					} 
					else 
                        BufferCounter += sprintf(ReadableText + BufferCounter, "%c", Data_To_Encode[i]);
				} 
				else 
                    BufferCounter += sprintf(ReadableText + BufferCounter, "%c", Data_To_Encode[i]);
			} 
		} 
	} 
	
	iSize = (long)strlen(ReadableText); 
    *output = new char [iSize];
    memset(*output, 0, iSize);
    strncpy(*output, ReadableText, strlen(ReadableText));
	
	return 0;
}




long  UCC128 (char *Data_To_Encode, char **output, long &iSize)
{
    char StringToPass[512];
    memset(&StringToPass, 0, 512);
	//StringToPass = new char[512];

    int char0 = (int) asc(mid(Data_To_Encode, 1, 1));
	// check for FNC1 character, if one is there; do nothing
	if(char0 == 202 || (char0 > 211 && char0 < 218))
        sprintf(StringToPass, "%s", Data_To_Encode);
		
	else//insert FNC1 in the begining of the string
        sprintf(StringToPass, "%c%s", (char)202, Data_To_Encode);

	int iTilde = 1;
    Code128(StringToPass, iTilde, output, iSize);
	
	return 0;
} 


/*****************************************************************/
/* ANSI C Functions for IDAutomation Barcode Fonts               */
/* © Copyright 2002- 2007, IDAutomation.com, Inc.                */
/* All rights reserved.                                          */
/* Redistribution and use of this code in source and/or binary   */
/* forms, with or without modification, are permitted provided   */
/* that: (1) all copies of the source code retain the above      */
/* unmodified copyright notice and this entire unmodified        */
/* section of text, (2) You or Your organization owns a valid    */
/* Developer License to this product from IDAutomation.com       */
/* and, (3) when any portion of this code is bundled in any      */
/* form with an application, a valid notice must be provided     */
/* within the user documentation, start-up screen or in the      */
/* help-about section of the application that specifies          */
/* IDAutomation.com as the provider of the Software bundled      */
/* with the application.                                         */
/*****************************************************************/


long  Code128 (char *Data_To_Encode, int ApplyTilde, char **output, long &iSize)
{
    if(Data_To_Encode == NULL)
		return 1;
    if(strlen(Data_To_Encode) == 0)
		return 1;

    char Printable_String[512];
    memset(&Printable_String, 0, 512);
    //Printable_String = new char[512];

    char Only_Correct_Data[512];
    memset(&Only_Correct_Data, 0, 512);
    //Only_Correct_Data = new char[512];
	
    //char StringToCheckFiltered[512];
	//StringToCheckFiltered = new char[512];
	
    //char tildeStringToCheck[512];
	//tildeStringToCheck = new char[512];

	
	
	

	
	char CurrentEncoding;			
	char StartChar;
	char StopChar;
    int Data_To_EncodeLength = 0;
	int CurrentValue = 0;		
	int i = 0;	
	int GoodData = 0; 
	int q = 0; 
	char *RevFilteredString;  
	int BufferCounter = 0;
	int L_BufferCounter = 0;
	int WeightedTotal = 0;
	int StringLength = 0;
	int CurrentCharNumber = 0;
	int CheckDigitValue = 0;

	

    Data_To_EncodeLength = (int)strlen(Data_To_Encode);
    Printable_String[0]=(char)0;
	StopChar = (char)206;

	
    if(((int)asc(mid(Data_To_Encode, 1, 1))) < 32)
	{
		StartChar = (char) 203;  
		CurrentEncoding = 'A';

	}


	
    if(((int) asc(mid(Data_To_Encode, 1, 1)) > 31 && (int) asc(mid(Data_To_Encode, 1, 1)) < 127) || ((int) asc(mid(Data_To_Encode, 1, 1)) == 197))
	{
		StartChar = (char) 204;  
		CurrentEncoding = 'B';
	}


	
    if(Data_To_EncodeLength > 3  && isdigit(Data_To_Encode[0]) && isdigit(Data_To_Encode[1]) &&
        isdigit(Data_To_Encode[2]) && isdigit(Data_To_Encode[3]))
	{
		StartChar = (char) 205;  
		CurrentEncoding = 'C';
	}

    if ((int) asc(mid(Data_To_Encode, 1, 1)) == 202 || ((int) asc(mid(Data_To_Encode, 1, 1)) >= 212 && (int) asc(mid(Data_To_Encode, 1, 1)) <= 217))
	{
		StartChar = (char) 205;  
		CurrentEncoding = 'C';
	}


	
    for(i = 0; i <= Data_To_EncodeLength - 1; i++)
	{
        if((Data_To_EncodeLength > 5 && i > 1 && i <= Data_To_EncodeLength - 3) && (int) Data_To_Encode[i] == 126
            && (int) Data_To_Encode[i + 1] == 109 && ApplyTilde)
		{
			
            if(isdigit(Data_To_Encode[i + 2]) && isdigit(Data_To_Encode[i + 3]))
			{

                int NumCharsToConvert = atoi(mid(Data_To_Encode, i + 3, 2));
				
				if(i - (NumCharsToConvert - 1) >= 0)
				{
					
                    char *StringToCheck = mid(Data_To_Encode, i - NumCharsToConvert + 1, NumCharsToConvert);
					char StringToCheckFiltered[40];
					int NumCharsFiltered = 0;
					
					GoodData = 0;
					BufferCounter = 0;
					
					for(q = (int)strlen(StringToCheck);q > 0 ;q--)
					{
						GoodData = isdigit((int)StringToCheck[q - 1]);
						if(GoodData)
						{
							if(NumCharsFiltered < NumCharsToConvert )
							{
								BufferCounter += sprintf(StringToCheckFiltered + BufferCounter, "%s", mid(StringToCheck, q, 1));
								NumCharsFiltered++;
							}
							else 
								break;
						}	
					}
					
					

                    if(strcmp(StringToCheckFiltered, "") == 0)
                        GoodData = FALSE_;
					else
					{
                        GoodData = TRUE_;
                        RevFilteredString = str_rev(StringToCheckFiltered);
					}

                    if(GoodData != FALSE_)
					{  

                        L_BufferCounter += sprintf(Only_Correct_Data + L_BufferCounter,"%c", Find_Mod_10_Digit(RevFilteredString));
						i += 3;  
					}
					else 
                        L_BufferCounter += sprintf(Only_Correct_Data + L_BufferCounter,"%c", Data_To_Encode[i]);
				} 
				else
                    L_BufferCounter += sprintf(Only_Correct_Data + L_BufferCounter,"%c", Data_To_Encode[i]);
			} 
			else 
                L_BufferCounter += sprintf(Only_Correct_Data + L_BufferCounter,"%c", Data_To_Encode[i]);
		} 
			
        else if ((Data_To_EncodeLength >= 4 && i <= Data_To_EncodeLength - 3) && (int) Data_To_Encode[i] == 126
            && isdigit((int) Data_To_Encode[i + 1]) && isdigit((int) Data_To_Encode[i + 2])
            && isdigit((int) Data_To_Encode[i + 3]) && ApplyTilde)
		{
			
            int convertedValue = atoi(mid(Data_To_Encode, i + 2, 3));
			if((convertedValue > 0 && convertedValue < 32) || convertedValue == 197 || convertedValue == 202 ||
				(convertedValue > 211 && convertedValue < 218))
			{
                L_BufferCounter += sprintf(Only_Correct_Data + L_BufferCounter,"%c", (char) (convertedValue));
				i = i + 3;
			}
			else
                L_BufferCounter += sprintf(Only_Correct_Data + L_BufferCounter,"%c", Data_To_Encode[i]);
		} 	
        else if((Data_To_EncodeLength >= 2 && i <= Data_To_EncodeLength - 2) && (int) Data_To_Encode[i] == 126
             && (int) Data_To_Encode[i + 1] == 126 && ApplyTilde)
		{ 
            L_BufferCounter += sprintf(Only_Correct_Data + L_BufferCounter,"%c", Data_To_Encode[i]);
			i = i + 1;
		}
		else
            L_BufferCounter += sprintf(Only_Correct_Data + L_BufferCounter,"%c", Data_To_Encode[i]);
	}

	L_BufferCounter = 0;
	
    L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", StartChar);
    Data_To_EncodeLength = (int)strlen(Only_Correct_Data);
	
    for(i = 0; i <= Data_To_EncodeLength - 1; i++)
	{

			
        if( i < Data_To_EncodeLength - 1 && ((int) asc(mid(Only_Correct_Data, i + 1, 1)) == 202 || (int) asc(mid(Only_Correct_Data, i + 1, 1)) == 212 || (int) asc(mid(Only_Correct_Data, i + 1, 1)) == 213
            || (int) asc(mid(Only_Correct_Data, i + 1, 1)) == 214 || (int) asc(mid(Only_Correct_Data, i + 1, 1)) == 215
            || (int) asc(mid(Only_Correct_Data, i + 1, 1)) == 216 || (int) asc(mid(Only_Correct_Data, i + 1, 1)) == 217 || (int) asc(mid(Only_Correct_Data, i + 1, 1)) == 218))
		{
			
			
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)202);
			
		}
		
		else if 
		
		
            ( (Data_To_EncodeLength > 3 && i <= (Data_To_EncodeLength - 2)) &&
            ((isdigit((int) Only_Correct_Data[i]) && isdigit((int) Only_Correct_Data[i + 1]) &&
            isdigit((int) Only_Correct_Data[i + 2]) && isdigit((int) Only_Correct_Data[i + 3]) ) ||
            (i <= Data_To_EncodeLength - 1 && isdigit((int) Only_Correct_Data[i]) &&
            isdigit((int) Only_Correct_Data[i + 1]) && CurrentEncoding == 'C' )))
		
		{
		

	 if (CurrentEncoding != 'C')

	{

                int j = i;
				int Factor = 3;

                while (j <= Data_To_EncodeLength-1 && isdigit((int)(Only_Correct_Data[j])))
					{

						Factor = (4 - Factor);
						j++;
					}
	
            
            if (Factor == 1)

			{
				
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter ,"%c", Data_To_Encode[i]);
				i++;
            }

				Factor = 0;
	}

		
			if(CurrentEncoding != 'C')
			{
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)199);
				CurrentEncoding = 'C';
			}

			/* Get the value of each number pair */
            CurrentValue = atoi(mid(Only_Correct_Data, i + 1, 2));

			/* Get the DataToPrint */
			if(CurrentValue < 95 && CurrentValue > 0) 
			{
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)(CurrentValue + 32));
			}
			if(CurrentValue > 94)
			{
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)(CurrentValue + 100));
			}
			if(CurrentValue == 0)
			{
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)194);
			}
			
			
			i++;
		}
		
        else if((i <= Data_To_EncodeLength - 1) && (((int) asc(mid(Only_Correct_Data, i + 1, 1)) < 32) || ((CurrentEncoding == 'A') && ((int) asc(mid(Only_Correct_Data, i + 1, 1)) < 96))))
		{	
			if (CurrentEncoding != 'A') 
			{
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)201);
				CurrentEncoding = 'A';
			}
            if ((int) Only_Correct_Data[i] < 32)
			{
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)((int) Only_Correct_Data[i] + 96));
			}
            else if ((int) Only_Correct_Data[i] > 31)
			{
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)Only_Correct_Data[i]);
			}
            else if ((int) Only_Correct_Data[i] == 32)
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)194);
		}
		
        else if((int) asc(mid(Only_Correct_Data, i + 1, 1)) == 197)
		{
			if(CurrentEncoding == 'C')
			{
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)200);
				CurrentEncoding = 'B';
			}
            L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", Only_Correct_Data[i]);
		}
		
        else if ((i <= Data_To_EncodeLength - 1) && ((int) asc(mid(Only_Correct_Data, i + 1, 1)) > 31 && (int) asc(mid(Only_Correct_Data, i + 1, 1)) < 127))
		{
			
			if (CurrentEncoding != 'B') 
			{
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)200);
				CurrentEncoding = 'B';
			}
			
            if ((int) Only_Correct_Data[i] == 32)
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)194);
			else
                L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", Only_Correct_Data[i]);
		}
	}

	
    WeightedTotal = (asc(&StartChar) - 100);
    StringLength = (int)strlen(Printable_String);
	CurrentCharNumber = 0;
	CurrentValue = 0;
	CheckDigitValue = 0;
	for(q = 2;q <= StringLength;q++)  
	{
        CurrentCharNumber = asc(mid(Printable_String, q, 1));
		if(CurrentCharNumber < 135)
			CurrentValue = (CurrentCharNumber - 32);
		else if(CurrentCharNumber > 134  && CurrentCharNumber != 194)
			CurrentValue = (CurrentCharNumber - 100);
		else if(CurrentCharNumber == 194)
			CurrentValue = 0;

		CurrentValue = CurrentValue * (q - 1);
		WeightedTotal = WeightedTotal + CurrentValue;
	}
    
    CheckDigitValue = (WeightedTotal % 103);

    if(CheckDigitValue < 95 && CheckDigitValue > 0)
        L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)(CheckDigitValue + 32));
	else if(CheckDigitValue > 94)
        L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)(CheckDigitValue + 100));
	
	
	else if(CheckDigitValue == 0)
        L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", (char)(194));

	
    L_BufferCounter += sprintf(Printable_String + L_BufferCounter,"%c", StopChar);



    iSize = (long)strlen(Printable_String);
    *output = new char [iSize];
    memset(*output, 0, iSize);
    strncpy(*output, Printable_String, strlen(Printable_String));
	return 0;
} 
	


char Find_Mod_10_Digit(char *input)
{
	int remainder;
	int M10Factor = 0;
	int M10WeightedTotal = 0;
    int M10StringLength = 0;
	int M10I = 0;

	M10Factor = 3;
	M10WeightedTotal = 0;
    M10StringLength = (int)strlen(input);
	for(M10I = M10StringLength;M10I > 0;M10I--)
	{
		
		   M10WeightedTotal = M10WeightedTotal +  (atoi(mid(input, M10I, 1)) * M10Factor);
		     M10Factor = 4 - M10Factor;
	}
	remainder = M10WeightedTotal % 10;
	if (remainder == 0) 
		return (char)48;
	else 
		return (char) ((10 - remainder) + 48);  
} 


void UPCe7to11(char DataToExpand[])
{   
	
    
	if (strlen(DataToExpand) == 6)
	{
        char buf[255];
        memset(&buf, 0, 255);
		int n = 1 + (int)strlen(DataToExpand); 

		int i =0;
		for (i=n-1;i>=0;i--)
		{			
			buf[n] = DataToExpand[i];
			n--;
		}
		buf[0] = '0';

		
		strcpy(DataToExpand,buf);
	}

	
	char D1 = DataToExpand[0];
	char D2 = DataToExpand[1];
	char D3 = DataToExpand[2];
	char D4 = DataToExpand[3];
	char D5 = DataToExpand[4];
	char D6 = DataToExpand[5];
	char D7 = DataToExpand[6];
	
	switch (D7)
	{
	case '0':
		DataToExpand[0] = D1; DataToExpand[1] = D2; DataToExpand[2] = D3;		
		DataToExpand[3] = '0'; DataToExpand[4] = '0'; DataToExpand[5] = '0'; 
		DataToExpand[6] = '0'; DataToExpand[7] = '0'; DataToExpand[8] = D4; 
		DataToExpand[9] = D5; DataToExpand[10] = D6; 
		break;
	case '1':
		DataToExpand[0] = D1; DataToExpand[1] = D2; DataToExpand[2] = D3;		
		DataToExpand[3] = D7; DataToExpand[4] = '0'; DataToExpand[5] = '0'; 
		DataToExpand[6] = '0'; DataToExpand[7] = '0'; DataToExpand[8] = D4; 
		DataToExpand[9] = D5; DataToExpand[10] = D6; 
		break;
	case '2':
		DataToExpand[0] = D1; DataToExpand[1] = D2; DataToExpand[2] = D3;		
		DataToExpand[3] = D7; DataToExpand[4] = '0'; DataToExpand[5] = '0'; 
		DataToExpand[6] = '0'; DataToExpand[7] = '0'; DataToExpand[8] = D4; 
		DataToExpand[9] = D5; DataToExpand[10] = D6; 
		break;
	case '3':
		DataToExpand[0] = D1; DataToExpand[1] = D2; DataToExpand[2] = D3;		
		DataToExpand[3] = D4; DataToExpand[4] = '0'; DataToExpand[5] = '0'; 
		DataToExpand[6] = '0'; DataToExpand[7] = '0'; DataToExpand[8] = '0'; 
		DataToExpand[9] = D5; DataToExpand[10] = D6;
		break;
	case '4':
		DataToExpand[0] = D1; DataToExpand[1] = D2; DataToExpand[2] = D3;		
		DataToExpand[3] = D4; DataToExpand[4] = D5; DataToExpand[5] = '0'; 
		DataToExpand[6] = '0'; DataToExpand[7] = '0'; DataToExpand[8] = '0'; 
		DataToExpand[9] = '0'; DataToExpand[10] = D6;
		break;
	case '5':
		DataToExpand[0] = D1; DataToExpand[1] = D2; DataToExpand[2] = D3;		
		DataToExpand[3] = D4; DataToExpand[4] = D5; DataToExpand[5] = D6; 
		DataToExpand[6] = '0'; DataToExpand[7] = '0'; DataToExpand[8] = '0'; 
		DataToExpand[9] = '0'; DataToExpand[10] = D7;
		break;
	case '6':
		DataToExpand[0] = D1; DataToExpand[1] = D2; DataToExpand[2] = D3;		
		DataToExpand[3] = D4; DataToExpand[4] = D5; DataToExpand[5] = D6; 
		DataToExpand[6] = '0'; DataToExpand[7] = '0'; DataToExpand[8] = '0'; 
		DataToExpand[9] = '0'; DataToExpand[10] = D7;
		break;
	case '7':
		DataToExpand[0] = D1; DataToExpand[1] = D2; DataToExpand[2] = D3;		
		DataToExpand[3] = D4; DataToExpand[4] = D5; DataToExpand[5] = D6; 
		DataToExpand[6] = '0'; DataToExpand[7] = '0'; DataToExpand[8] = '0'; 
		DataToExpand[9] = '0'; DataToExpand[10] = D7;
		break;
	case '8':
		DataToExpand[0] = D1; DataToExpand[1] = D2; DataToExpand[2] = D3;		
		DataToExpand[3] = D4; DataToExpand[4] = D5; DataToExpand[5] = D6; 
		DataToExpand[6] = '0'; DataToExpand[7] = '0'; DataToExpand[8] = '0'; 
		DataToExpand[9] = '0'; DataToExpand[10] = D7;
		break;
	case '9':
		DataToExpand[0] = D1; DataToExpand[1] = D2; DataToExpand[2] = D3;		
		DataToExpand[3] = D4; DataToExpand[4] = D5; DataToExpand[5] = D6; 
		DataToExpand[6] = '0'; DataToExpand[7] = '0'; DataToExpand[8] = '0'; 
		DataToExpand[9] = '0'; DataToExpand[10] = D7;
		break;
	}
	DataToExpand[11] =(int) NULL;
}



//-------------------------------------------------------

//long  UPCe (char *Data_To_Encode, char *output, long iSize)
//{
//    if(Data_To_Encode == NULL)
//		return 1;
//    if(strlen(Data_To_Encode) == 0)
//		return 1;
	
//	int StringLength = 0;
//	int I = 0;

//    char LPrintable_String[512];
//    memset(&LPrintable_String, 0, 512);
//    //LPrintable_String = new char[512];

//    char ActualData_To_Encode[20];
//    memset(&ActualData_To_Encode, 0, 20);
//    //ActualData_To_Encode = new char[20];

//    char DataToPrint[512];
//    memset(&DataToPrint, 0, 512);
//	//DataToPrint = new char[512];
	
//    char EAN2AddOn[3];
//    memset(&EAN2AddOn, 0, 3);
//	//EAN2AddOn = new char[3];
	
//    char Only_Correct_Data[20];
//    memset(&Only_Correct_Data, 0, 20);
//    //Only_Correct_Data = new char[20];

//    char EAN5AddOn[6];
//    memset(&EAN5AddOn, 0, 6);
//	//EAN5AddOn = new char[6];

//    char EANAddOnToPrint[11];
//    memset(&EANAddOnToPrint, 0, 11);
//	//EANAddOnToPrint = new char[11];
	
//	int Factor = 3;
//    int WeightedTotal = 0;
//	int CheckDigit = 0;
//    char Encoding[7];
//    memset(&Encoding, 0, 7);
//	int CurrentNumber;
//	char midEncoding;
//	char *sCheck = 0;
//	int bufferCounter = 0;

	
//	char *D1;
//	char *D2;
//	char *D3;
//	char *D4;
//	char *D5;
//	char *D6;
//	char *D7;
//	char *D8;
//	char *D9;
//	char *D10;
//	char *D11;
//	char *D12;
	
//    StringLength = (int)strlen(Data_To_Encode);
//    LPrintable_String[0]=(char)0;
	
//	if(StringLength > 20)
//		StringLength = 20;

    
//	for(I = 0;I < StringLength;I++)
//	{
//        if(isdigit((int)Data_To_Encode[I]))
//		{
//            bufferCounter += sprintf(Only_Correct_Data + bufferCounter, "%c", Data_To_Encode[I]);
//		}
		
//	}
	
    
//    if(strlen(Only_Correct_Data) == 6 || strlen(Only_Correct_Data) == 7 || strlen(Only_Correct_Data) == 8)
//	{
//        UPCe7to11(Only_Correct_Data);
//	}
	
//    if(strlen(Only_Correct_Data) < 11)
//	{
//        sprintf(LPrintable_String,"%s","00005000000");
//		return 0;
//	}
//    if(strlen(Only_Correct_Data) == 15)
//	{
//        sprintf(LPrintable_String,"%s","00005000000");
//		return 0;
//	}
//    if(strlen(Only_Correct_Data) > 18 && Only_Correct_Data[11] != (int) NULL)
//	{
//        sprintf(LPrintable_String,"%s","00005000000");
//		return 0;
//	}
//    if(strlen(Only_Correct_Data) == 12)
//        sprintf(LPrintable_String,"%s", mid(Only_Correct_Data, 1, 11));
//    if(strlen(Only_Correct_Data) == 14)
//        sprintf(LPrintable_String,"%s%s", mid(Only_Correct_Data, 1, 11), mid(Only_Correct_Data, 13, 2));
//    if(strlen(Only_Correct_Data) == 17)
//        sprintf(LPrintable_String,"%s%s", mid(Only_Correct_Data, 1, 11), mid(Only_Correct_Data, 13, 5));

     
//    if(strlen(Only_Correct_Data) == 16)
//        sprintf(EAN5AddOn,"%s", mid(Only_Correct_Data, 12, 5));
//    else if(strlen(Only_Correct_Data) == 13)
//	{
//        sprintf(EAN2AddOn,"%s", mid(Only_Correct_Data, 12, 2));
//	}
//	else
//	{
//		sprintf(EAN2AddOn,"%s", "");
//		sprintf(EAN5AddOn,"%s", "");
//		sprintf(EANAddOnToPrint,"%s", "");
//	}

     
//    if(strlen(Only_Correct_Data) > 11)
//        sprintf(Only_Correct_Data,"%s", mid(Only_Correct_Data, 1, 11));
     
	
//     Factor = 3;
//     WeightedTotal = 0;
	 
	 
//     for(I = (int)strlen(Only_Correct_Data);I >= 0;I--)
//	 {
		
//        CurrentNumber = atoi(mid(Only_Correct_Data, I, 1));

	    
//        WeightedTotal = WeightedTotal + (CurrentNumber * Factor);
		
//        Factor = 4 - Factor;
//     }

	
//	I = (WeightedTotal % 10);
//	if(I != 0)
//		CheckDigit = (10 - I);
//	else
//		CheckDigit = 0;

//    sprintf(Only_Correct_Data,"%s%i", Only_Correct_Data, CheckDigit);

	

//    D1 = mid(Only_Correct_Data, 1, 1);
//    D2 = mid(Only_Correct_Data, 2, 1);
//    D3 = mid(Only_Correct_Data, 3, 1);
//    D4 = mid(Only_Correct_Data, 4, 1);
//    D5 = mid(Only_Correct_Data, 5, 1);
//    D6 = mid(Only_Correct_Data, 6, 1);
//    D7 = mid(Only_Correct_Data, 7, 1);
//    D8 = mid(Only_Correct_Data, 8, 1);
//    D9 = mid(Only_Correct_Data, 9, 1);
//    D10 = mid(Only_Correct_Data, 10, 1);
//    D11 = mid(Only_Correct_Data, 11, 1);
//    D12 = mid(Only_Correct_Data, 12, 1);

	
//	if((atoi(D11) == 5 || atoi(D11) == 6 || atoi(D11) == 7 || atoi(D11) == 8 || atoi(D11) == 9) &&
//		(atoi(D6) != 0 && atoi(D7) == 0 && atoi(D8) == 0 && atoi(D9) == 0 && atoi(D10) == 0))
//	{
//        sprintf(ActualData_To_Encode, "%s%s%s%s%s%s", D2, D3, D4, D5, D6, D11);
//	}

	
//	else if((atoi(D6) == 0 && atoi(D7) == 0 && atoi(D8) == 0 && atoi(D9) == 0 && atoi(D10) == 0) && atoi(D5) != 0)
//	{
//        sprintf(ActualData_To_Encode, "%s%s%s%s%s%s", D2, D3, D4, D5, D11, "4");
//	}
	
	
//	else if((atoi(D5) == 0 && atoi(D6) == 0 && atoi(D7) == 0 && atoi(D8) == 0)
//		&& (atoi(D4) == 1 || atoi(D4) == 2 || atoi(D4) == 0))
//	{
//        sprintf(ActualData_To_Encode, "%s%s%s%s%s%s", D2, D3, D9, D10, D11, D4);
//	}

	
//	else if((atoi(D5) == 0 && atoi(D6) == 0 && atoi(D7) == 0 && atoi(D8) == 0 && atoi(D9) == 0) &&
//		(atoi(D4) == 3 || atoi(D4) == 4 || atoi(D4) == 5 || atoi(D4) == 6 || atoi(D4) == 7 || atoi(D4) == 8 || atoi(D4) == 9) )
//	{
//        sprintf(ActualData_To_Encode, "%s%s%s%s%s%s", D2, D3, D4, D10, D11, "3");
//	}
//	else

//	{
		
//        sprintf(ActualData_To_Encode, "%s", Only_Correct_Data);
//	}
     
	
//    if(strlen(ActualData_To_Encode) == 6)
//	{
		
//		if(atoi(D1) == 1)
//		{
//			switch(atoi(D12))
//			{
//				case 0:
//                    //Encoding = "AAABBB";
//                    sprintf(Encoding,"%s","AAABBB");
//					break;
//				case 1:
//                    sprintf(Encoding,"%s", "AABABB");
//					break;
//				case 2:
//                    sprintf(Encoding,"%s", "AABBAB");
//					break;
//				case 3:
//                    sprintf(Encoding,"%s", "AABBBA");
//					break;
//				case 4:
//                    sprintf(Encoding,"%s", "ABAABB");
//					break;
//				case 5:
//                    sprintf(Encoding,"%s", "ABBAAB");
//					break;
//				case 6:
//                    sprintf(Encoding,"%s", "ABBBAA");
//					break;
//				case 7:
//                    sprintf(Encoding,"%s", "ABABAB");
//					break;
//				case 8:
//                    sprintf(Encoding,"%s", "ABABBA");
//					break;
//				case 9:
//                    sprintf(Encoding,"%s", "ABBABA");
//					break;
//			}
//		}

		
//		else
//		{
//			switch(atoi(D12))
//			{
//				case 0:
//					Encoding = "BBBAAA";
//					break;
//				case 1:
//					Encoding = "BBABAA";
//					break;
//				case 2:
//					Encoding = "BBAABA";
//					break;
//				case 3:
//					Encoding = "BBAAAB";
//					break;
//				case 4:
//					Encoding = "BABBAA";
//					break;
//				case 5:
//					Encoding = "BAABBA";
//					break;
//				case 6:
//					Encoding = "BAAABB";
//					break;
//				case 7:
//					Encoding = "BABABA";
//					break;
//				case 8:
//					Encoding = "BABAAB";
//					break;
//				case 9:
//					Encoding = "BAABAB";
//					break;
//			}
//		}

//		bufferCounter = 0;
//        for(I = 1;I <= (int)strlen(ActualData_To_Encode);I++)
//		{

//			if(I == 1)
//			{
//                if(asc(mid(Only_Correct_Data, I, 1)) == 48)
//				{
//					bufferCounter += sprintf(DataToPrint, "%c%s", (char)85, "(");
//				}
//				else
//				{
//					bufferCounter += sprintf(DataToPrint, "%c%s", (char)86, "(");
//				}
//			}
//            CurrentNumber = asc(mid(ActualData_To_Encode, I, 1));

			
//			midEncoding = Encoding[I - 1];
//			switch(midEncoding)
//			{
//				case 'A':
//					bufferCounter += sprintf(DataToPrint + bufferCounter, "%c", (char)CurrentNumber);
//					break;
//				case 'B':
//					bufferCounter += sprintf(DataToPrint + bufferCounter, "%c", (char)(CurrentNumber + 17));
//					break;
//			}

            
//			switch(I)
//			{
				
//				case 1:
				
//			/*	if(atoi(D1) == 0)
//				{
//					bufferCounter += sprintf(DataToPrint + bufferCounter, "%c%s", (char)85, "(");
//				}
//				if(atoi(D1) == 1)
//				{
//					bufferCounter += sprintf(DataToPrint + bufferCounter, "%c%s", (char)86, "(");
//				}*/
//					break;
//				case 6:
					
//					if(atoi(D12) > 4)
//						bufferCounter += sprintf(DataToPrint + bufferCounter, "%s%c", ")", (char)(asc(D12) + 64));
//					if(atoi(D12) < 5)
//						bufferCounter += sprintf(DataToPrint + bufferCounter, "%s%c", ")", (char)(asc(D12) + 37));
//					break;
//			}
//		}
//	}
     
	
//	bufferCounter = 0;
//    if(strlen(ActualData_To_Encode) != 6)
//	{
//        for(I = 1;I <= (int)strlen(ActualData_To_Encode);I++)
//		{
//            CurrentNumber = asc(mid(ActualData_To_Encode, I, 1));

			
//			switch(I)
//			{
//				case 1:
					
					
//                    if(atoi(mid(ActualData_To_Encode, I, 1)) > 4)
//						bufferCounter += sprintf(DataToPrint + bufferCounter, "%c%s%c", (char)(CurrentNumber + 64), "(", (char)(CurrentNumber + 49));
//                    if(atoi(mid(ActualData_To_Encode, I, 1)) < 5)
//						bufferCounter += sprintf(DataToPrint + bufferCounter, "%c%s%c", (char)(CurrentNumber + 37), "(", (char)(CurrentNumber + 49));
//					break;
//				case 2:
//                    bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(ActualData_To_Encode, I, 1));
//					break;
//				case 3:
//                    bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(ActualData_To_Encode, I, 1));
//					break;
//				case 4:
//                    bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(ActualData_To_Encode, I, 1));
//					break;
//				case 5:
//                    bufferCounter += sprintf(DataToPrint + bufferCounter, "%s", mid(ActualData_To_Encode, I, 1));
//					break;
//				case 6:
					
//                    bufferCounter += sprintf(DataToPrint + bufferCounter, "%s%s", mid(ActualData_To_Encode, I, 1), "*");
//					break;
//				case 7:
					
					
//					bufferCounter += sprintf(DataToPrint + bufferCounter, "%c", (char)(CurrentNumber + 27));
//					break;
//				case 8:
//					bufferCounter += sprintf(DataToPrint + bufferCounter, "%c", (char)(CurrentNumber + 27));
//					break;
//				case 9:
//					bufferCounter += sprintf(DataToPrint + bufferCounter, "%c", (char)(CurrentNumber + 27));
//					break;
//				case 10:
//					bufferCounter += sprintf(DataToPrint + bufferCounter, "%c", (char)(CurrentNumber + 27));
//					break;
//				case 11:
//					bufferCounter += sprintf(DataToPrint + bufferCounter, "%c", (char)(CurrentNumber + 27));
//					break;
//				case 12:
					
					
//                    if(atoi(mid(ActualData_To_Encode, I, 1)) > 4)
//						bufferCounter += sprintf(DataToPrint + bufferCounter, "%c%s%c", (char)(CurrentNumber + 59), ")", (char)(CurrentNumber + 64));
//                    if(atoi(mid(ActualData_To_Encode, I, 1)) < 5)
//						bufferCounter += sprintf(DataToPrint + bufferCounter, "%c%s%c", (char)(CurrentNumber + 59), ")", (char)(CurrentNumber + 37));
//					break;
//			}
//		}
//	}
     
	
//	if(strlen(EAN5AddOn) == 5)
//	{
		
//        Factor = 3;
//		WeightedTotal = 0;
//		for(I=  (int)strlen(EAN5AddOn);I >= 0;I--)
//		{
			
//            CurrentNumber = atoi(mid(EAN5AddOn, I, 1));
			
//            if(Factor == 3)
//				WeightedTotal = WeightedTotal + (CurrentNumber * 3);
//            if(Factor == 1 )
//				WeightedTotal = WeightedTotal + (CurrentNumber * 9);
        
			
//            Factor = 4 - Factor;
//               // Factor = 4 - Factor;
//		}
		
		
//		sprintf(sCheck,"%i", WeightedTotal);

//		CheckDigit = atoi(mid(sCheck, (int)strlen(sCheck), 1));
		
		
		
//		switch(CheckDigit)
//		{
//			case 0:
//               sprintf(Encoding,"%s", "BBAAA");
//			   break;
//			case 1:
//               sprintf(Encoding,"%s", "BABAA");
//			   break;
//			case 2:
//               sprintf(Encoding,"%s", "BAABA");
//			   break;
//			case 3:
//               sprintf(Encoding,"%s", "BAAAB");
//			   break;
//			case 4:
//               sprintf(Encoding,"%s", "ABBAA");
//			   break;
//			case 5:
//               sprintf(Encoding,"%s", "AABBA");
//			   break;
//			case 6:
//               sprintf(Encoding,"%s", "AAABB");
//			   break;
//			case 7:
//               sprintf(Encoding,"%s", "ABABA");
//			   break;
//			case 8:
//               sprintf(Encoding,"%s", "ABAAB");
//			   break;
//			case 9:
//               sprintf(Encoding,"%s", "AABAB");
//			   break;
//		}
//		//Now that we have the total number including the check digit, determine character to print for proper barcoding:
//		bufferCounter = 0;
//		for(I = 1;I <= (int)strlen(EAN5AddOn);I++)
//		{
//			//Get the value of each number.  It is encoded with variable parity
//            CurrentNumber = atoi(mid(EAN5AddOn, I, 1));
//			//Print different barcodes according to the location of the CurrentChar and CurrentEncoding
//			midEncoding = Encoding[I - 1];
//			switch(midEncoding)
//			{
//				case 'A':
//					if(CurrentNumber == 0)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)34);
//                    if(CurrentNumber == 1)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)35);
//                    if(CurrentNumber == 2)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)36);
//                    if(CurrentNumber == 3)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)37);
//                    if(CurrentNumber == 4)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)38);
//                    if(CurrentNumber == 5)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)44);
//                    if(CurrentNumber == 6)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)46);
//                    if(CurrentNumber == 7)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)47);
//                    if(CurrentNumber == 8)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)58);
//                    if(CurrentNumber == 9)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)59);
//					break;

//				case 'B':
//					if(CurrentNumber == 0)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)122);
//                    if(CurrentNumber == 1)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)61);
//                    if(CurrentNumber == 2)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)63);
//                    if(CurrentNumber == 3)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)64);
//                    if(CurrentNumber == 4)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)91);
//                    if(CurrentNumber == 5)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)92);
//                    if(CurrentNumber == 6)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)93);
//                    if(CurrentNumber == 7)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)95);
//                    if(CurrentNumber == 8)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)123);
//                    if(CurrentNumber == 9)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)125);
//					break;
//			}
			
//			//add in the space & add-on guard pattern
//			switch(I)
//			{
//				case 1:
//					bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c%s%c", (char)43, EANAddOnToPrint, (char)33);
//					break;
//					//Now print add-on delineators between each add-on character
//				case 2:
//					bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)33);
//					break;
//				case 3:
//                    bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)33);
//					break;
//				case 4:
//                    bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)33);
//					break;
//				case 5:
//                    //EANAddOnToPrint = EANAddOnToPrint
//					break;
//			}
//		} //end for loop
//	}	//end 5 digit supplement work
	
//	//Now for the 2 digit add on
//	if(strlen(EAN2AddOn) == 2)
//	{
//		//Get encoding for add on
//		for(I = 0;I <= 99;I++)
//		{
//			if(atoi(EAN2AddOn) == I)
//				Encoding = "AA";
//            if(atoi(EAN2AddOn) == I + 1)
//				Encoding = "AB";
//            if(atoi(EAN2AddOn) == I + 2)
//				Encoding = "BA";
//            if(atoi(EAN2AddOn) == I + 3)
//				Encoding = "BB";
//		}//end for loop
          
//		bufferCounter = 0;
//		//Now that we have the total number including the encoding determine what to print
//		for(I = 1;I <= (int)strlen(EAN2AddOn);I++)
//		{
//			CurrentNumber = atoi(mid(EAN2AddOn, I, 1));
//			midEncoding = Encoding[I - 1];
//			//Print different barcodes according to the location of the CurrentChar and CurrentEncoding
//			switch(midEncoding)
//			{
//				case 'A':
//					if(CurrentNumber == 0)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)34);
//					if(CurrentNumber == 1)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)35);
//					if(CurrentNumber == 2)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)36);
//					if(CurrentNumber == 3)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)37);
//					if(CurrentNumber == 4)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)38);
//					if(CurrentNumber == 5)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)44);
//					if(CurrentNumber == 6)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)46);
//					if(CurrentNumber == 7)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)47);
//					if(CurrentNumber == 8)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)58);
//					if(CurrentNumber == 9)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)59);
//					break;

//				case 'B':
//					if(CurrentNumber == 0)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)122);
//					if(CurrentNumber == 1)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)61);
//					if(CurrentNumber == 2)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)63);
//					if(CurrentNumber == 3)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)64);
//					if(CurrentNumber == 4)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)91);
//					if(CurrentNumber == 5)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)92);
//					if(CurrentNumber == 6)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)93);
//					if(CurrentNumber == 7)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)95);
//					if(CurrentNumber == 8)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)123);
//					if(CurrentNumber == 9)
//						bufferCounter += sprintf(EANAddOnToPrint + bufferCounter, "%c", (char)125);
//					break;
//			} //end switch
//			//add in the space & add-on guard pattern
//			switch(I)
//			{
//				case 1:
//					bufferCounter += sprintf(EANAddOnToPrint, "%c%s%c", (char)43, EANAddOnToPrint, (char)33);
//					break;
//				case 2:
//					//EANAddOnToPrint = EANAddOnToPrint
//					break;
//			}
//		}//end loop thru characers
//	} //end ean 2 digit supp processing
     
//	//Now we have everything together
//    sprintf(LPrintable_String, "%s%s", DataToPrint, EANAddOnToPrint);

//// Allow the string to return to the proper size.
//    iSize = (long)strlen(LPrintable_String);
//    strncpy(output, LPrintable_String, strlen(LPrintable_String));

//	return 0;
//} //end upce

QColor getColor(unsigned char color)
{
    if (color == 1)
        return barCodeColor;

    return Qt::white;
}

QImage barEnaCodeToQimageEAN(char *str, int sizeStr)
{
    if (symbUPCEANList.size() <= 0)
    {
        initializeSymbolMap();
        if (symbUPCEANList.size() <= 0)
            return QImage();
    }

    QImage outImage(5000, 150, QImage::Format_ARGB32);
    outImage.fill(Qt::white);

    QPainter painter(&outImage);
    int widtxImage = 0;
    for (int i = 0; i < sizeStr; i++)
    {
        for (int n = 0; n < symbUPCEANList.size(); n++)
        {
            if (str[i] == symbUPCEANList.at(n).symbol)
            {
                int heigntP = 0;
                if (symbUPCEANList.at(n).numberShow == -2)
                    heigntP = 10;

                if (symbUPCEANList.at(n).width >= 1)
                {
                    painter.setPen(getColor(symbUPCEANList.at(n).line_0));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUPCEANList.at(n).width >= 2)
                {
                    painter.setPen(getColor(symbUPCEANList.at(n).line_1));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUPCEANList.at(n).width >= 3)
                {
                    painter.setPen(getColor(symbUPCEANList.at(n).line_2));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUPCEANList.at(n).width >= 4)
                {
                    painter.setPen(getColor(symbUPCEANList.at(n).line_3));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUPCEANList.at(n).width >= 5)
                {
                    painter.setPen(getColor(symbUPCEANList.at(n).line_4));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUPCEANList.at(n).width >= 6)
                {
                    painter.setPen(getColor(symbUPCEANList.at(n).line_5));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUPCEANList.at(n).width >= 7)
                {
                    painter.setPen(getColor(symbUPCEANList.at(n).line_6));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUPCEANList.at(n).width >= 8)
                    widtxImage += 2;

                if (symbUPCEANList.at(n).width >= 9)
                    widtxImage += 2;

                if (symbUPCEANList.at(n).width >= 10)
                    widtxImage += 2;

                if (symbUPCEANList.at(n).width >= 11)
                    widtxImage += 2;

                if (symbUPCEANList.at(n).width >= 12)
                    widtxImage += 2;

                if (symbUPCEANList.at(n).width >= 13)
                    widtxImage += 2;



                if (symbUPCEANList.at(n).numberShow >= 0)
                {
                    QFont font;
                    font.setPixelSize(18);
                    //font.setBold(true);
                    font.setFamily("Arial");
                    painter.setFont(font);
                    painter.setPen(barCodeColor);
                    painter.drawText((widtxImage - symbUPCEANList.at(n).width - 4), 130, symbUPCEANList.at(n).width + 2, 20, Qt::AlignLeft, QString::number(symbUPCEANList.at(n).numberShow));
                }

                break;
            }
        }
    }

    return outImage.copy(0, 0, widtxImage + 5, outImage.height());
}

QImage barEnaCodeToQimageUNA(char *str, int sizeStr)
{
    if (symbUNIList.size() <= 0)
    {
        initializeSymbolMap();
        if (symbUNIList.size() <= 0)
            return QImage();
    }

    QImage outImage(5000, 150, QImage::Format_ARGB32);
    outImage.fill(Qt::white);

    QPainter painter(&outImage);
    int widtxImage = 0;
    for (int i = 0; i < sizeStr; i++)
    {
        for (int n = 0; n < symbUNIList.size(); n++)
        {
            if (str[i] == symbUNIList.at(n).symbol)
            {
                int heigntP = 0;
                if (symbUNIList.at(n).numberShow == -2)
                    heigntP = 10;

                if (symbUNIList.at(n).width >= 1)
                {
                    painter.setPen(getColor(symbUNIList.at(n).line_0));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUNIList.at(n).width >= 2)
                {
                    painter.setPen(getColor(symbUNIList.at(n).line_1));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUNIList.at(n).width >= 3)
                {
                    painter.setPen(getColor(symbUNIList.at(n).line_2));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUNIList.at(n).width >= 4)
                {
                    painter.setPen(getColor(symbUNIList.at(n).line_3));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUNIList.at(n).width >= 5)
                {
                    painter.setPen(getColor(symbUNIList.at(n).line_4));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUNIList.at(n).width >= 6)
                {
                    painter.setPen(getColor(symbUNIList.at(n).line_5));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUNIList.at(n).width >= 7)
                {
                    painter.setPen(getColor(symbUNIList.at(n).line_6));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUNIList.at(n).width >= 8)
                {
                    painter.setPen(getColor(symbUNIList.at(n).line_7));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUNIList.at(n).width >= 9)
                {
                    painter.setPen(getColor(symbUNIList.at(n).line_8));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUNIList.at(n).width >= 10)
                {
                    painter.setPen(getColor(symbUNIList.at(n).line_9));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUNIList.at(n).width >= 11)
                {
                    painter.setPen(getColor(symbUNIList.at(n).line_10));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }

                if (symbUNIList.at(n).width >= 12)
                {
                    painter.setPen(getColor(symbUNIList.at(n).line_11));
                    painter.drawLine(widtxImage, 0, widtxImage, 130 + heigntP);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130 + heigntP);
                    widtxImage += 2;
                }


                if (symbUNIList.at(n).numberShow >= 0)
                {
                    QFont font;
                    font.setPixelSize(18);
                    //font.setBold(true);
                    font.setFamily("Arial");
                    painter.setFont(font);
                    painter.setPen(barCodeColor);
                    painter.drawText((widtxImage - symbUNIList.at(n).width - 4), 130, symbUNIList.at(n).width + 2, 20, Qt::AlignLeft, QString::number(symbUNIList.at(n).numberShow));
                }

                break;
            }
        }
    }

    return outImage.copy(0, 0, widtxImage + 5, outImage.height());
}

QImage barEnaCodeToQimagePostNet(char *str, int sizeStr)
{
    if (symbPostNetList.size() <= 0)
    {
        initializeSymbolMap();
        if (symbPostNetList.size() <= 0)
            return QImage();
    }

    QImage outImage(5000, 150, QImage::Format_ARGB32);
    outImage.fill(Qt::white);

    QPainter painter(&outImage);

    painter.setPen(barCodeColor);

    int widtxImage = 0;
    for (int i = 0; i < sizeStr; i++)
    {
        for (int n = 0; n < symbPostNetList.size(); n++)
        {
            if (str[i] == symbPostNetList.at(n).symbol)
            {
                if (symbPostNetList.at(n).width >= 1)
                {
                    if (symbPostNetList.at(n).line_0 == 1)
                    {
                        painter.drawLine(widtxImage + 2, 0, widtxImage + 2, 20);
                        painter.drawLine(widtxImage + 3, 0, widtxImage + 3, 20);
                    }
                    else
                    {
                        painter.drawLine(widtxImage + 2, 10, widtxImage + 2, 20);
                        painter.drawLine(widtxImage + 3, 10, widtxImage + 3, 20);
                    }
                    widtxImage += 6;
                }

                if (symbPostNetList.at(n).width >= 2)
                {
                    if (symbPostNetList.at(n).line_1 == 1)
                    {
                        painter.drawLine(widtxImage + 2, 0, widtxImage + 2, 20);
                        painter.drawLine(widtxImage + 3, 0, widtxImage + 3, 20);
                    }
                    else
                    {
                        painter.drawLine(widtxImage + 2, 10, widtxImage + 2, 20);
                        painter.drawLine(widtxImage + 3, 10, widtxImage + 3, 20);
                    }
                    widtxImage += 6;
                }

                if (symbPostNetList.at(n).width >= 3)
                {
                    if (symbPostNetList.at(n).line_2 == 1)
                    {
                        painter.drawLine(widtxImage + 2, 0, widtxImage + 2, 20);
                        painter.drawLine(widtxImage + 3, 0, widtxImage + 3, 20);
                    }
                    else
                    {
                        painter.drawLine(widtxImage + 2, 10, widtxImage + 2, 20);
                        painter.drawLine(widtxImage + 3, 10, widtxImage + 3, 20);
                    }
                    widtxImage += 6;
                }

                if (symbPostNetList.at(n).width >= 4)
                {
                    if (symbPostNetList.at(n).line_3 == 1)
                    {
                        painter.drawLine(widtxImage + 2, 0, widtxImage + 2, 20);
                        painter.drawLine(widtxImage + 3, 0, widtxImage + 3, 20);
                    }
                    else
                    {
                        painter.drawLine(widtxImage + 2, 10, widtxImage + 2, 20);
                        painter.drawLine(widtxImage + 3, 10, widtxImage + 3, 20);
                    }
                    widtxImage += 6;
                }

                if (symbPostNetList.at(n).width >= 5)
                {
                    if (symbPostNetList.at(n).line_4 == 1)
                    {
                        painter.drawLine(widtxImage + 2, 0, widtxImage + 2, 20);
                        painter.drawLine(widtxImage + 3, 0, widtxImage + 3, 20);
                    }
                    else
                    {
                        painter.drawLine(widtxImage + 2, 10, widtxImage + 2, 20);
                        painter.drawLine(widtxImage + 3, 10, widtxImage + 3, 20);
                    }
                    widtxImage += 6;
                }

                break;
            }
        }
    }

    return outImage.copy(0, 0, widtxImage, 20);
}

QImage barCodeToQimageCode128(char *str, int sizeStr)
{
   // qDebug(str);

    if (symbolTo128CodeList.size() <= 0)
    {
        initializeSymbolMap();
        if (symbolTo128CodeList.size() <= 0)
            return QImage();
    }

    QImage outImage(5000, 150, QImage::Format_ARGB32);
    outImage.fill(Qt::white);

    QPainter painter(&outImage);
    int widtxImage = 0;
    for (int i = 0; i < sizeStr; i++)
    {
        for (int n = 0; n < symbolTo128CodeList.size(); n++)
        {
            if ((unsigned char)str[i] == symbolTo128CodeList.at(n).symbol)
            {
                if (symbolTo128CodeList.at(n).width >= 1)
                {
                    painter.setPen(getColor(symbolTo128CodeList.at(n).line_0));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo128CodeList.at(n).width >= 2)
                {
                    painter.setPen(getColor(symbolTo128CodeList.at(n).line_1));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo128CodeList.at(n).width >= 3)
                {
                    painter.setPen(getColor(symbolTo128CodeList.at(n).line_2));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo128CodeList.at(n).width >= 4)
                {
                    painter.setPen(getColor(symbolTo128CodeList.at(n).line_3));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo128CodeList.at(n).width >= 5)
                {
                    painter.setPen(getColor(symbolTo128CodeList.at(n).line_4));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo128CodeList.at(n).width >= 6)
                {
                    painter.setPen(getColor(symbolTo128CodeList.at(n).line_5));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo128CodeList.at(n).width >= 7)
                {
                    painter.setPen(getColor(symbolTo128CodeList.at(n).line_6));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo128CodeList.at(n).width >= 8)
                {
                    painter.setPen(getColor(symbolTo128CodeList.at(n).line_7));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo128CodeList.at(n).width >= 9)
                {
                    painter.setPen(getColor(symbolTo128CodeList.at(n).line_8));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo128CodeList.at(n).width >= 10)
                {
                    painter.setPen(getColor(symbolTo128CodeList.at(n).line_9));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo128CodeList.at(n).width >= 11)
                {
                    painter.setPen(getColor(symbolTo128CodeList.at(n).line_10));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo128CodeList.at(n).width >= 12)
                {
                    painter.setPen(getColor(symbolTo128CodeList.at(n).line_11));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo128CodeList.at(n).width >= 13)
                {
                    painter.setPen(getColor(symbolTo128CodeList.at(n).line_12));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }


                if ((symbolTo128CodeList.at(n).symbolShow != 0) && (i > 0) && (i < (sizeStr - 2)))
                {
                    QFont font;
                    font.setPixelSize(18);
                    //font.setBold(true);
                    font.setFamily("Arial");
                    painter.setFont(font);
                    painter.setPen(barCodeColor);
                    painter.drawText((widtxImage - symbolTo128CodeList.at(n).width - 4), 130, symbolTo128CodeList.at(n).width + 8, 20, Qt::AlignHCenter, QString(symbolTo128CodeList.at(n).symbolShow));
                }

                break;
            }
        }
    }



    return outImage.copy(0, 0, widtxImage + 5, outImage.height());
}

QImage barCodeToQimageCode93(char *str, int sizeStr)
{
   // qDebug(str);

    if (symbolTo93CodeList.size() <= 0)
    {
        initializeSymbolMap();
        if (symbolTo93CodeList.size() <= 0)
            return QImage();
    }

    QImage outImage(5000, 150, QImage::Format_ARGB32);
    outImage.fill(Qt::white);

    QPainter painter(&outImage);
    int widtxImage = 0;
    for (int i = 0; i < sizeStr; i++)
    {
        for (int n = 0; n < symbolTo93CodeList.size(); n++)
        {
            if ((unsigned char)str[i] == symbolTo93CodeList.at(n).symbol)
            {
                if (symbolTo93CodeList.at(n).width >= 1)
                {
                    painter.setPen(getColor(symbolTo93CodeList.at(n).line_0));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;

                }

                if (symbolTo93CodeList.at(n).width >= 2)
                {
                    painter.setPen(getColor(symbolTo93CodeList.at(n).line_1));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo93CodeList.at(n).width >= 3)
                {
                    painter.setPen(getColor(symbolTo93CodeList.at(n).line_2));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo93CodeList.at(n).width >= 4)
                {
                    painter.setPen(getColor(symbolTo93CodeList.at(n).line_3));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo93CodeList.at(n).width >= 5)
                {
                    painter.setPen(getColor(symbolTo93CodeList.at(n).line_4));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo93CodeList.at(n).width >= 6)
                {
                    painter.setPen(getColor(symbolTo93CodeList.at(n).line_5));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo93CodeList.at(n).width >= 7)
                {
                    painter.setPen(getColor(symbolTo93CodeList.at(n).line_6));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo93CodeList.at(n).width >= 8)
                {
                    painter.setPen(getColor(symbolTo93CodeList.at(n).line_7));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo93CodeList.at(n).width >= 9)
                {
                    painter.setPen(getColor(symbolTo93CodeList.at(n).line_8));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                if (symbolTo93CodeList.at(n).width >= 10)
                {
                    painter.setPen(getColor(symbolTo93CodeList.at(n).line_9));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }


                if ((symbolTo93CodeList.at(n).symbolShow != 0) && (i > 0) && (i < (sizeStr - 3)))
                {
                    QFont font;
                    font.setPixelSize(18);
                    //font.setBold(true);
                    font.setFamily("Arial");
                    painter.setFont(font);
                    painter.setPen(barCodeColor);
                    painter.drawText((widtxImage - symbolTo93CodeList.at(n).width - 4), 130, symbolTo93CodeList.at(n).width + 8, 20, Qt::AlignHCenter, QString(symbolTo93CodeList.at(n).symbolShow));
                }

                break;
            }
        }
    }



    return outImage.copy(0, 0, widtxImage + 5, outImage.height());
}

QImage barCodeToQimageCodabar(char *str, int sizeStr)
{
   // qDebug(str);

    if (symbolToCodabarList.size() <= 0)
    {
        initializeSymbolMap();
        if (symbolToCodabarList.size() <= 0)
            return QImage();
    }

    QImage outImage(5000, 150, QImage::Format_ARGB32);
    outImage.fill(Qt::white);

    QPainter painter(&outImage);
    int widtxImage = 0;
    for (int i = 0; i < sizeStr; i++)
    {
        for (int n = 0; n < symbolToCodabarList.size(); n++)
        {
            if ((unsigned char)str[i] == symbolToCodabarList.at(n).symbol)
            {
                for (int l = 0; l < symbolToCodabarList.at(n).lines.size(); l++)
                {
                    painter.setPen(getColor(symbolToCodabarList.at(n).lines.at(l)));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                widtxImage += 4;

                if ((symbolToCodabarList.at(n).symbolShow != 0))
                {
                    QFont font;
                    font.setPixelSize(18);
                    //font.setBold(true);
                    font.setFamily("Arial");
                    painter.setFont(font);
                    painter.setPen(barCodeColor);
                    painter.drawText((widtxImage - symbolToCodabarList.at(n).width - 10), 130, symbolToCodabarList.at(n).width + 8, 20, Qt::AlignHCenter, QString(symbolToCodabarList.at(n).symbolShow));
                }

                break;
            }
        }
    }



    return outImage.copy(0, 0, widtxImage + 5, outImage.height());
}

QImage barCodeToQimageInterleaved2of5(char *str, int sizeStr, QString valueStr)
{
   // qDebug(str);

    QImage outImage((sizeStr + 1) * 2, 80, QImage::Format_ARGB32);
    outImage.fill(Qt::white);

    QPainter painter(&outImage);
    int widtxImage = 0;
    for (int i = 0; i < sizeStr; i++)
    {
        painter.setPen(getColor(str[i]));
        painter.drawLine(widtxImage, 0, widtxImage, 60);
        painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 60);
        widtxImage += 2;
    }

    QFont font;
    font.setPixelSize(18);
    //font.setBold(true);
    font.setFamily("Arial");
    painter.setFont(font);
    painter.setPen(barCodeColor);
    valueStr.remove(QRegExp("[^0-9]"));
    painter.drawText(0, 60, outImage.width(), 20, Qt::AlignHCenter, valueStr);
//    QPainter painter(&outImage);
//    int widtxImage = 0;
//    for (int i = 0; i < sizeStr; i++)
//    {
//        for (int n = 0; n < symbolToCodabarList.size(); n++)
//        {
//            if ((unsigned char)str[i] == symbolToCodabarList.at(n).symbol)
//            {
//                for (int l = 0; l < symbolToCodabarList.at(n).lines.size(); l++)
//                {
//                    painter.setPen(getColor(symbolToCodabarList.at(n).lines.at(l)));
//                    painter.drawLine(widtxImage, 0, widtxImage, 130);
//                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
//                    widtxImage += 2;
//                }

//                widtxImage += 4;

//                if ((symbolToCodabarList.at(n).symbolShow >= 0))
//                {
//                    QFont font;
//                    font.setPixelSize(18);
//                    //font.setBold(true);
//                    font.setFamily("Arial");
//                    painter.setFont(font);
//                    painter.setPen(barCodeColor);
//                    painter.drawText((widtxImage - symbolToCodabarList.at(n).width - 10), 130, symbolToCodabarList.at(n).width + 8, 20, Qt::AlignHCenter, QString(symbolToCodabarList.at(n).symbolShow));
//                }

//                break;
//            }
//        }
//    }



    return outImage;
}


QImage barCodeToQimageCode39(char *str, int sizeStr)
{
   // qDebug(str);

    if (symbolTo39CodeList.size() <= 0)
    {
        initializeSymbolMap();
        if (symbolTo39CodeList.size() <= 0)
            return QImage();
    }

    QImage outImage(5000, 150, QImage::Format_ARGB32);
    outImage.fill(Qt::white);

    QPainter painter(&outImage);
    int widtxImage = 0;
    for (int i = 0; i < sizeStr; i++)
    {
       // qDebug(QString(str[i]).toLocal8Bit());
        for (int n = 0; n < symbolTo39CodeList.size(); n++)
        {
            if ((unsigned char)str[i] == symbolTo39CodeList.at(n).symbol)
            {

                for (int l = 0; l < symbolTo39CodeList.at(n).lines.size(); l++)
                {
                    painter.setPen(getColor(symbolTo39CodeList.at(n).lines.at(l)));
                    painter.drawLine(widtxImage, 0, widtxImage, 130);
                    painter.drawLine(widtxImage + 1, 0, widtxImage + 1, 130);
                    widtxImage += 2;
                }

                widtxImage += 4;

                if ((symbolTo39CodeList.at(n).symbolShow != 0))
                {
                    QFont font;
                    font.setPixelSize(18);
                    //font.setBold(true);
                    font.setFamily("Arial");
                    painter.setFont(font);
                    painter.setPen(barCodeColor);
                    painter.drawText((widtxImage - symbolTo39CodeList.at(n).width - 10), 130, symbolTo39CodeList.at(n).width + 8, 20, Qt::AlignHCenter, QString(symbolTo39CodeList.at(n).symbolShow));
                }

                break;
            }
        }
    }



    return outImage.copy(0, 0, widtxImage + 5, outImage.height());
}
#endif // BARCODE_1D_H
