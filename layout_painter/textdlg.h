#ifndef TEXTDLGH
#define TEXTDLGH

#include <QtGui>
#include "appstyle.h"
//#include "datafile.h"


class CTextDlg : public QWidget
{
    Q_OBJECT

    QPushButton *m_pOk;
    QPushButton *m_pCancel;
//    CDataFile *data;
//    CAppSettings *settings;
//    CTranslate *q;

    QFontComboBox *m_pFontCombo;
    QComboBox *m_pSizeCombo;
    QTextEdit *m_pTextEdit;
    //QToolBar *m_pFontToolBar;


    QString m_Text;
    QFont m_Font;
    int m_Align;
    QColor m_Color;

public:
    CTextDlg(CAppStyle *style, QWidget *parent, bool disableB = false);
    ~CTextDlg();

    CAppStyle *appstyle;

    void SetText(QString Text);
    QString GetText();
    void SetFont(QFont Font);
    QFont GetFont();
    void SetAlign(int Align);
    int GetAlign();
    void SetColor(QColor Clr);
    QColor GetColor();
    QString qsResPath;

    int res;

protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);

public slots:
    void OnTextFamily(const QString &Family);
    void OnTextSize(const QString &SizeStr);
    void OnTextItalic();
    void OnTextBold();
    void OnTextUnderline();
    void OnTextAlign(QAction *a);
    void OnTextColor();
    void OnOk();
    void OnCancel();

private:
    QToolButton *actionTextBold;
    QToolButton *actionTextItalic;
    QToolButton *actionTextUnderline;

    /*QAction *actionAlignLeft;
    QAction *actionAlignCenter;
    QAction *actionAlignRight;
    QAction *actionTextColor;*/
};



class CTextDlgWin : public QDialog
{
    Q_OBJECT

public:
    CTextDlgWin(CAppStyle *style, QWidget *parent, bool disableB = false);
    ~CTextDlgWin();

	CTextDlg *mainWidget;

protected:
	void keyPressEvent(QKeyEvent *e)
	{
		if ((e->key()==Qt::Key_Enter) || (e->key()==Qt::Key_Return) || (e->key()==Qt::Key_Escape)) {}
		else { QDialog::keyPressEvent(e); return; }
	}
};

#endif
