#include "MathUtils.h"


    double cround(double Argument, int Precision)
	{
		double div = 1.0;
		if(Precision >= 0)
			while(Precision--)
				div *= 10.0;
		else
			while(Precision++)
				div /= 10.0;
		return floor(Argument * div + 0.5) / div;
	}


	double findAngleBetweenVectors(QPointF *Centre, QPointF *V1_P, QPointF *V2_P)
	{
		if(((Centre->x() != V1_P->x())||(Centre->y() != V1_P->y()))&&((Centre->x() != V2_P->x())||(Centre->y() != V2_P->y())))
		{
			double Result;
			double Angle1;
			double Angle2;

			Angle1 = atan2(V1_P->x() - Centre->x(), V1_P->y() - Centre->y());
			Angle2 = atan2(V2_P->x() - Centre->x(), V2_P->y() - Centre->y());

			Result = Angle2 - Angle1;

            int a = (int)cround((Result*180)/(MATH_PI), 0);
			if( a < 0 ) a+= 360;
			//Result = (((double)a)*pi)/180; //translate to radian
			Result = a;

			return Result;
		}
		else
			return 0;
	}


	double calcFitScale(int srcW, int srcH, int DstW, int DstH, int &fitOffX, int &fitOffY )
	{
		double scX = (double)DstW / srcW;
		double scY = (double)DstH / srcH;
		double sc;
		if ( scX < scY )
			sc = scX;
		else
			sc = scY;

		fitOffX = (int)((DstW - srcW * sc) / 2);
		fitOffY = (int)((DstH - srcH * sc) / 2);

		return sc;
	}

/*	int mm_to_px(double mm)
	{
		int px = Round(mm*96/25.4, 0);
		return px;
	}

	int in_to_px(double in)
	{
		int px = Round(in*96, 0);
		return px;
	}

	double px_to_mm(int px)
	{
		double mm = Round(((double)px)/96*25.4, 6);
		return mm;
	}

	double px_to_mm(double px)
	{
		double mm = Round(px/96*25.4, 6);
		return mm;
	}

	double px_to_in(int px)
	{
		double in = Round(((double)px)/96, 6);
		return in;
	}

	double px_to_in(double px)
	{
		double in = Round(px/96, 6);
		return in;
	}

	BPADoublePoint RotatePoint(BPADoublePoint *pCentre, BPADoublePoint *pPoint, double Angle)
	{
		BPADoublePoint P;

		pPoint->m_x -= pCentre->m_x;
		pPoint->m_y -= pCentre->m_y;

		P = *pPoint;

		P.m_x = pPoint->m_x*cos(Angle) - pPoint->m_y*sin(Angle);
		P.m_y = pPoint->m_x*sin(Angle) + pPoint->m_y*cos(Angle);

		P.m_x += pCentre->m_x;
		P.m_y += pCentre->m_y;

		return P;
	}

	double FindDistanceBetweenPoints(BPADoublePoint *pP1, BPADoublePoint *pP2)
	{
		double d;

		d = sqrt((pP1->m_x - pP2->m_x)*(pP1->m_x - pP2->m_x) + (pP1->m_y - pP2->m_y)*(pP1->m_y - pP2->m_y));

		return d;
	}

	BPADoublePoint FindPointOnLine(BPADoublePoint *pSP, BPADoublePoint *pEP, BPADoublePoint *pPt, double d)
	{
		BPADoublePoint Result;
		double k;
		double b;
		double x;
		double y;

		if((pSP->m_x != pEP->m_x)||(pSP->m_y != pEP->m_y))
		{
			if(pSP->m_x >= pEP->m_x) d = -d;

			if(pSP->m_y == pEP->m_y)
			{
				Result.m_x = pPt->m_x + d;
				Result.m_y = pSP->m_y;
				return Result;
			}

			if(pSP->m_x == pEP->m_x)
			{
				Result.m_x = pSP->m_x;
				if(pSP->m_y >= pEP->m_y)
					Result.m_y = pPt->m_y + d;
				else
					Result.m_y = pPt->m_y - d;
				return Result;
			}

			k = (pEP->m_y-pSP->m_y)/(pEP->m_x-pSP->m_x);
			b = pSP->m_y - k*pSP->m_x;

			x = pPt->m_x + d/(sqrt(1 + k*k));
			y = k*x + b;

			Result.m_x = x;
			Result.m_y = y;
		}
		else
			Result = *pPt;

		return Result;
	}

	bool FindPointPerpendToLine(BPADoublePoint *pSP, BPADoublePoint *pEP, BPADoublePoint *pPt, double d, BPADoublePoint *pResPt)
	{
		*pResPt = BPADoublePoint();

		if( (pSP->m_x != pEP->m_x)||(pSP->m_y != pEP->m_y) )
		{
			if( (pSP->m_y < pEP->m_y && pSP->m_x < pEP->m_x) ||	(pSP->m_y > pEP->m_y && pSP->m_x < pEP->m_x) ) 
				d = -d;
			else if( pSP->m_x == pEP->m_x && pSP->m_y < pEP->m_y )
				d = -d;
			else if( pSP->m_y == pEP->m_y && pSP->m_x > pEP->m_x )
				d = -d;


			if( pSP->m_x == pEP->m_x )
			{
				pResPt->m_x = pPt->m_x-d;
				pResPt->m_y = pPt->m_y;
				return true;
			}
			if( pSP->m_y == pEP->m_y )
			{
				pResPt->m_x = pPt->m_x;
				pResPt->m_y = pPt->m_y-d;
				return true;
			}

			double dx01 = pSP->m_x-pEP->m_x;
			double dy01 = pSP->m_y-pEP->m_y;
			pResPt->m_y = pPt->m_y + d*fabs(dx01)/sqrt(dx01*dx01 + dy01*dy01);
			pResPt->m_x = pPt->m_x + (pPt->m_y-pResPt->m_y )*dy01/dx01;
		}
		else
			return false;

		return true;
	}

	bool FindEqualDistPointsOnLine(BPADoublePoint *pSP, BPADoublePoint *pEP, BPADoublePoint *Pt, BPADoublePoint *pP1, BPADoublePoint *pP2, double *dist)
	{
		if((pSP->m_x != pEP->m_x)||(pSP->m_y != pEP->m_y))
		{
			double h, delta, dist_2, h_2;

			dist_2 = (*dist)*(*dist);

			if(pSP->m_y == pEP->m_y)
			{
				h = Pt->m_y - pSP->m_y;
				h_2 = h*h;
				if(dist_2 >= h_2)
				{
					delta = sqrt(dist_2 - h_2);
					pP1->m_y = pSP->m_y;
					pP2->m_y = pSP->m_y;
					pP1->m_x = Pt->m_x + delta;
					pP2->m_x = Pt->m_x - delta;
					return true;
				}
				else
				{
					return false;
				}
			}

			if(pSP->m_x == pEP->m_x)
			{
				h = Pt->m_x - pSP->m_x;
				h_2 = h*h;
				if(dist_2 >= h_2)
				{
					delta = sqrt(dist_2 - h_2);
					pP1->m_x = pSP->m_x;
					pP2->m_x = pSP->m_x;
					pP1->m_y = Pt->m_y + delta;
					pP2->m_y = Pt->m_y - delta;
					return true;
				}
				else
				{
					return false;
				}
			}

			double k, b, D, c;

			k = (pEP->m_y - pSP->m_y)/(pEP->m_x - pSP->m_x);
			b = pSP->m_y - k*pSP->m_x;

			c = Pt->m_y - b;

			double k_c, c_c, k_k;
			double v1, v2, v3;

			k_c = k*c;
			c_c = c*c;
			k_k = k*k;

			D = 8*k_c*Pt->m_x + 4*((dist_2- c_c) + k_k*(dist_2 - Pt->m_x*Pt->m_x));

			if(D < 0) return false;

			v1 = 2*(k_c + Pt->m_x);
			v2 = sqrt(D);
			v3 = 2*(1 + k_k);

			pP1->m_x = (v1 + v2)/v3;
			pP2->m_x = (v1 - v2)/v3;
			pP1->m_y = k*pP1->m_x + b;
			pP2->m_y = k*pP2->m_x + b;
			return true;
		}
		else
			return false;
	}

	double FindAngleBetweenLines(BPADoublePoint *pL1_P1, BPADoublePoint *pL1_P2, BPADoublePoint *pL2_P1, BPADoublePoint *pL2_P2)
	{
		if( ((pL1_P1->m_x != pL1_P2->m_x)||(pL1_P1->m_y != pL1_P2->m_y))&&((pL2_P1->m_x != pL2_P2->m_x)||(pL2_P1->m_y != pL2_P2->m_y)) )
		{
			double Result;
			double Angle1;
			double Angle2;

			Angle1 = atan2(pL1_P1->m_x - pL1_P2->m_x, pL1_P1->m_y - pL1_P2->m_y);
			Angle2 = atan2(pL2_P1->m_x - pL2_P2->m_x, pL2_P1->m_y - pL2_P2->m_y);

			Result = Angle2 - Angle1;

			double pi = 3.141592;
			int a = (int)Round((Result*180)/(pi), 0);
			if (a < 0 ) a+= 360;
			//Result = (((double)a)*pi)/180;
			return a;

			return Result;
		}
		else
		{
			return 0;
		}
	}

	bool CompareLinePoints(BPADoublePoint *pSP, BPADoublePoint *pEP, BPADoublePoint *pP0, BPADoublePoint *pP1)
	{
		if((pSP->m_x != pEP->m_x)||(pSP->m_y != pEP->m_y))
		{
			if(pSP->m_y == pEP->m_y)
			{
				if(pSP->m_x < pEP->m_x)
				{
					if(pP1->m_x > pP0->m_x)
						return true;
					else
						return false;
				}
				else
				{
					if(pP1->m_x < pP0->m_x)
						return true;
					else
						return false;
				}
			}

			if(pSP->m_x == pEP->m_x)
			{
				if(pSP->m_y < pEP->m_y)
				{
					if(pP1->m_y > pP0->m_y)
						return true;
					else
						return false;
				}
				else
				{
					if(pP1->m_y < pP0->m_y)
						return true;
					else
						return false;
				}
			}

			if(pSP->m_x < pEP->m_x)
			{
				if(pP1->m_x > pP0->m_x)
					return true;
				else
					return false;
			}
			else
			{
				if(pP1->m_x < pP0->m_x)
					return true;
				else
					return false;
			}
		}
		else
			return false;
	}

	bool IsPointsOnLine(BPADoublePoint *pP1, BPADoublePoint *pP2, BPADoublePoint *pP3, BPADoublePoint *pP4)
	{
		bool Result = false;

		double k;
		double b;
		double x1, x2;
		double y1, y2;

		double sg = 1;

		if((pP1->m_x != pP4->m_x)||(pP1->m_y != pP4->m_y))
		{

			if(pP1->m_y == pP4->m_y)
			{
				if((pP2->m_y == pP3->m_y)&&(pP1->m_y == pP2->m_y))
					return true;
				else
					return false;
			}

			if(pP1->m_x == pP4->m_x)
			{
				if((pP2->m_x == pP3->m_x)&&(pP1->m_x == pP2->m_x))
					return true;
				else
					return false;
			}

			k = (pP4->m_y-pP1->m_y)/(pP4->m_x-pP1->m_x);
			b = pP1->m_y - k*pP1->m_x;

			x1 = pP2->m_x;
			y1 = k*x1 + b;

			x2 = pP3->m_x;
			y2 = k*x2 + b;

			if((fabs(pP2->m_y - y1) <= sg)&&(fabs(pP3->m_y - y2) <= sg))
				return true;
			else
				return false;
		}
		else
			return false;	

		return Result;
	}


	QSizeF FitImgSizeToOwnerSize(QSizeF ImgSz, QSizeF OwnerSz, double EnlargePerc)
	{
		double Perc = EnlargePerc;
		double kw = ImgSz.width()/ImgSz.height();
		double kh = ImgSz.height()/ImgSz.width();
		double NewW = OwnerSz.width()*Perc;
		double NewH = OwnerSz.height()*Perc;

		if( NewW/ImgSz.width() < NewH/ImgSz.height() )
			NewW = kw*NewH;
		else 
			NewH = kh*NewW;

		return QSizeF(NewW, NewH);
	}


	QSizeF FitImgSizeToPageSize(QSizeF ImgSz, QSizeF PageSz)
	{
		double Perc = 0.7;

		if( ImgSz.width() <= PageSz.width()*Perc && ImgSz.height() <= PageSz.height()*Perc )
			return ImgSz;

		double kw = ImgSz.width()/ImgSz.height();
		double kh = ImgSz.height()/ImgSz.width();
		double NewW = PageSz.width()*Perc;
		double NewH = PageSz.height()*Perc;

		if( NewW/ImgSz.width() > NewH/ImgSz.height() )
			NewW = kw*NewH;
		else 
			NewH = kh*NewW;

		return QSizeF(NewW, NewH);
	}*/
