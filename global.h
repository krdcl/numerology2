#ifndef GLOBAL_H
#define GLOBAL_H

#define VBNUMVER 0x070604

#ifdef VER_PREM
#ifdef Q_WS_MAC
#define Q_APP_TARGET "VeBest Numerology Everywhere"
#else
#define Q_APP_TARGET "VBNE"
#endif
#endif

#ifdef VER_STD
#ifdef Q_WS_MAC
#define Q_APP_TARGET "VeBest Numerology"
#else
#define Q_APP_TARGET "VBNC"
#endif
#endif

#ifdef VER_FREE
#ifdef Q_WS_MAC
#define Q_APP_TARGET "VeBest Numerology Lite"
#else
#endif
#endif

struct sNumSettings
{
    bool calcLP1, calcLP2, calcLP3;
    bool calcNameWD, calcNameDD;
    bool skipTransitFS;
    sNumSettings()
    {
        calcLP1 = true;
        calcLP2 = false;
        calcLP3 = false;
        calcNameWD = true;
        calcNameDD = true;
        skipTransitFS = false;
        //vNameCalcMode = -1;
        //cNameCalcMode = -1;
    }
};

extern sNumSettings m_GlobSettings;

#endif // GLOBAL_H
