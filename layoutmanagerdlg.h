#ifndef LAYOUTMANAGERDLG_H
#define LAYOUTMANAGERDLG_H

#include <QtGui>
#include "appstyle.h"

#include "includes.h"

#define TYPE_FILE 0x564e5250 //VNRP
#define VER_FILE_REP_1 0x00000001
#define VER_FILE_REP_2 0x00000002

#define CUR_VER_FILE_REP VER_FILE_REP_2

#define RESERVE_TEXTS 256


struct repItem
{
    qint32 id;
    QString name;
    qint32 type;
    QFont headerFont;
    QFont contentFont;
    QColor headerColor;
    QColor contentColor;
    bool isContentDefault;
    QStringList content;
    QList<QImage> imgList;
    QList<bool> isDefList;
    QString headerName;
    QMap <QString, QString> extendedSettings;

    repItem()
    {
		isContentDefault = true;
		contentColor = QColor::fromRgb(0,0,0,255);
		headerColor = QColor::fromRgb(0,0,0,255);
        headerFont = QFont("Arial", 18, QFont::Bold);
        headerFont.setPixelSize(18);
        contentFont = QFont("Arial", 16, QFont::Normal);
        contentFont.setPixelSize(16);
        for (int i = content.size(); i < RESERVE_TEXTS; i++)
            content.append(QString());
        for (int i = imgList.size(); i < RESERVE_TEXTS; i++)
            imgList.append(QImage());
        for (int i = isDefList.size(); i < RESERVE_TEXTS; i++)
            isDefList.append(true);
    }
};

struct reportSetting
{
    QString name;
    QList<repItem> Numerology;
    QList<repItem> PsychoMatrix;
    QList<repItem> LoveCompatibility;
    QList<repItem> Celebrities;
    QList<repItem> Pet;
	QMap <QString, QString> extendedSettings;
};


typedef QList<reportSetting> RepSettings;


class CWCLItem : public QWidget
{
    Q_OBJECT

public:
    CWCLItem(CAppStyle *style, QWidget *parent=NULL);
    ~CWCLItem();

    int px, py;
    QString name;
    bool sel;

    bool copyOnly;

    qint32 type;
    qint32 id;

    QFont headerFont;
    QFont contentFont;

    QColor headerColor;
    QColor contentColor;

    bool isDefault;

    QStringList content;
    QList<QImage> imgList;
    QList<bool> isDefList;
    QString headerName;
    QMap <QString, QString> extendedSettings;


    int tmrid;
    bool freeMove;
    bool drop;

    CAppStyle *appstyle;

    void drawItem(QPainter*, bool drawBackground=true);
    void drawItem(QPixmap*);

private:


signals:
    void itemSel(void *);

protected:
    void paintEvent(QPaintEvent *);
    void timerEvent(QTimerEvent *);
    void mousePressEvent(QMouseEvent *event) { emit itemSel(this); QWidget::mousePressEvent(event); }
};


class CWCLContainerWidget : public QWidget
{
    Q_OBJECT

public:
    CWCLContainerWidget(CAppStyle *style, bool isDestination = true, QWidget *parent=NULL);
    ~CWCLContainerWidget();

    CAppStyle *appstyle;

    QPoint dragStartPosition;

    QList<CWCLItem *> lmitems;
    bool sel;
    int itemhs, itemws;
    int itemdw, itemdh;

    void updateContainer();
    void prepareItemPlace(int x, int y);
    void calcItemPos(CWCLItem *item, int idx);

private:

    bool isDest;

signals:
    void itemSel(void *);
    void signalRel();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);
    void dropEvent(QDropEvent *event);
    void startDrag();

public slots:

    void OnItemSel(void *v) { emit itemSel(v); }

private slots:


};


class CWCLContainer : public QScrollArea
{
    Q_OBJECT

public:
    CWCLContainer(CAppStyle *style, QWidget *parent=NULL, bool isDestination = true);
    ~CWCLContainer();

    CWCLContainerWidget *widg;

protected:
    void resizeEvent(QResizeEvent *);

signals:

    void signalPress();
};

class CLayoutManagerDlg : public QWidget
{
    Q_OBJECT

public:
    CLayoutManagerDlg(CAppStyle *style, QString selSetting, QWidget *parent=NULL);
    ~CLayoutManagerDlg();

    QString curSetting_;

    void initStyle();

//    void OnAddField(QPoint pos, int idx, CREPage *page, CReportPE *report, QSize objSz=QSize(180,25), int imgNum = -1);

//    void addFieldGrid(int x, int y, int idx, CREPage *page, CReportPE *report, QSize objSz=QSize(180,25), int imgNum = -1);
    static void saveReportSettings(RepSettings *setList);
    static void saveSet(QDataStream *stream, const QList<repItem> *rep);

    static void loadReportSettings(RepSettings *setList);
    static void loadSet(QDataStream *stream, QList<repItem> *rep, quint32 ver);

    QPushButton *okBut;
    QPushButton *cancelBut;

    QComboBox *repNameCB;
    QPushButton *headerNameBut;
    QPushButton *headerFontBut;
    QPushButton *headerColorBut;
    QPushButton *contentFontBut;
    QPushButton *contentColorBut;
    QCheckBox *defCB;
    QPushButton *contentEditBut;

    QListWidget *settingList;
    QPushButton *pNewSetting;
    QPushButton *pCopySetting;
    QPushButton *pDelSetting;
    QPushButton *pRenameSetting;
    QPushButton *pFolderSetting;

    QCheckBox *hideAllImagesCB;
    QCheckBox *hideCurImageCB;


    QList<CWCLContainer*> lmcs;
    CWCLItem *citem;
    CWCLItem *cursitem;
    QPoint spnt;
    bool ddstarted, ddprepare;
    CWCLContainer *slmc;
    int slmcpos;

    CAppStyle *appstyle;

    int res;
    QString filePathSave;

    int imgID;

    void clearSel();
    void updateFields();
    void checkCurItem();

    int m_gridX, m_gridY;
    double m_gridXm, m_gridYm;

    int widgetHandleAdjustX(int x) { return ((int)((double)x/m_gridXm)) * m_gridXm; }
    int widgetHandleAdjustY(int y) { return ((int)((double)y/m_gridYm)) * m_gridYm; }
    int gridToPix(int grid) { return grid * m_gridYm; }
    int pixToGrig(int pix) { return pix / m_gridYm; }

    static void getMassText(int type, int id, QString **p);

private:

    QList <QStringList> nameList;
    int index;

    QSizeF sz;
    QString rname;
    void addItem(int type, int id, QString headerNamr = QString());
    QString getItemName(int type, int id);
    RepSettings settings;
    void initialDefSetting(reportSetting *setting);
	static void addItemSet(repItem &item, int retType, int itemId, QString name);
	static void addItemSet(QList<repItem> *retset, int retType, int itemId, QString name);

    void itemsToSet(int repType, int setId);
    void setToItems(int repType, int setId);

    enum INSERTType {INSNew, INSCopy};

protected:
    void paintEvent(QPaintEvent *);
    void resizeEvent(QResizeEvent *);

public slots:
    void OnOk(bool noClose = false);
    void OnExit();
    void OnItemSelected(void *);
    void OnItemSelectedAlt(void *);
    void OnHeaderFont();
    void OnHeaderColor();
    void OnContentFont();
    void OnContentColor();
    void OnRepName(int val);
    void OnCheckDef();
    void OnCheckAllImages();
    void OnCheckCurImage();
    void OnNewSetting();
    void OnAddSetting(INSERTType valT = INSCopy);
    void OnDelSetting();
    void OnRenameSetting();
    void OnFolderSetting();
    void OnSelectSetting(int);
    void OnTextEdit();
    void OnSetName();

signals:

    void signalPress();


};


class CLayoutManagerDlgWin : public QDialog
{
    Q_OBJECT

public:
    CLayoutManagerDlgWin(CAppStyle *style, QString selSetting, QWidget *parent = 0);
    ~CLayoutManagerDlgWin();

    CLayoutManagerDlg *mainWidget;

protected:
    void keyPressEvent(QKeyEvent *e)
    {
        if ((e->key()==Qt::Key_Enter) || (e->key()==Qt::Key_Return) || (e->key()==Qt::Key_Escape)) {}
        else { QDialog::keyPressEvent(e); return; }
    }
};


#endif // LAYOUTMANAGERDLG_H
