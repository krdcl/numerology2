#ifndef INFODLG_H
#define INFODLG_H

#include <QtGui>
#include "appstyle.h"

class CInfoDlg : public QWidget
{
	Q_OBJECT

public:
    CInfoDlg(CAppStyle *style, QWidget *parent, qreal sc = 0, int wi = 400);
	~CInfoDlg();

	void initStyle();

	QPushButton *getLicenseButton;

	QPushButton *m_pOk;
	QPushButton *m_pCancel;

    CAppStyle *appstyle;

	QLabel *winText;
	int res;
    qreal scale;
    int qWi;

protected:

public slots:
	void OnOk();
	void OnCancel();
    void OnMidle();
    void OnDownload();
    void OnWhatNes();
    static void OnGetLicense();
};


class CInfoDlgWin : public QDialog
{
	Q_OBJECT

public:
    CInfoDlgWin(CAppStyle *style, QWidget *parent, qreal sc = 0, int wi = 400);
	~CInfoDlgWin();

	CInfoDlg *mainWidget;

    static int showAskMessageBox(CAppStyle *style, QString winTitle, QString text, QWidget *parent = NULL);
    static int showInfoMessageBox(CAppStyle *style, QString winTitle, QString text, QWidget *parent);
    static int showEnterNameDlg(CAppStyle *style, QString &name, QWidget *parent);

private:

protected:
	void keyPressEvent(QKeyEvent *e)
	{
		if ((e->key()==Qt::Key_Enter) || (e->key()==Qt::Key_Return) || (e->key()==Qt::Key_Escape)) {}
		else { QDialog::keyPressEvent(e); return; }
	}
};


#endif // INFODLG_H
