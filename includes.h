#ifndef INCLUDES_H
#define INCLUDES_H


#include <QPushButton>
#include <QLabel>
#include <QApplication>
#include <QDialog>
#include <QLineEdit>
#include <QFontComboBox>
#include <QComboBox>
#include <QToolButton>
#include <QTextEdit>
#include <QCheckBox>
#include <QFileDialog>
#include <QWebEngineView>
#include <QTimer>
#include <QScrollArea>
#include <QCheckBox>
#include <QWebEngineView>
#include <QMainWindow>
#include <QDateEdit>
#include <QMenu>
#include <QPrinter>
#include <QDesktopWidget>
#include <QRect>
#include <QMessageBox>
#include <QPrintDialog>
#include <QFileDialog>
#include <QWebEngineSettings>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QWebEngineProfile>
#include <QWebEngineDownloadItem>
#include <QProgressDialog>
#include <QFile>
#include "QtConcurrent"

#endif // INCLUDES_H
