#pragma once

#include <QString>

#ifndef VER_MOBILE
#include "global.h"
#endif

extern const QString qsWKStyle;

//#10100
extern QString numerLifePath[128];
//#10200
extern QString numerBirthDate[128];
//#10300
extern QString numerKarmDebts[128];
//#10400
extern QString numerExpr[128];
//#10500
extern QString numerExprM[128];
//#10600
extern QString numerPersYear[128];
//#10700
extern QString numerPersDate[128];
//#10800
extern QString numerHeartDesire[128];
//#10900
extern QString numerPersonality[128];
//#11000
extern QString numerHiddenPassion[128];
//#11100
extern QString numerMinorHeartDesire[128];
//#11200
extern QString numerKarmicLessons[128];
//#11300
extern QString numerSubconsConfid[128];
//#11400
extern QString numerBalance[128];
//#11500
extern QString numerChallenges[128];
//#11600
extern QString numerPinnacles[128];
//#11700
extern QString numerMaturity[128];
//#11800
extern QString numerRationalThoughts[128];
//#11900
extern QString numerTransit[128];
//#12000
extern QString numerEssence[128];
//#12100
extern QString numerDoubleDigitNumbers[128];
//#12200
extern QString numerCompoundNumbers[128];
//#12300
extern QString numerPlanesOfExpression[128];
//#12400
extern QString numerPlanesOfExpressionP[128];
//#12500
extern QString numerPlanesOfExpressionM[128];
//#12600
extern QString numerPlanesOfExpressionE[128];
//#12700
extern QString numerPlanesOfExpressionI[128];
//#12800
extern QString numerBridgeLPE[128];
//#12900
extern QString numerBridgeLPEDet[128];
//#13000
extern QString numerBridgeHDP[128];
//#13100
extern QString numerBridgeHDPDet[128];


// code 201XX
extern QString numerPurposeful[128];
// code 202XX
extern QString numerFamilyFormation[128];
// code 203XX
extern QString numerStability[128];
// code 204XX
extern QString numer1[128];
// code 205XX
extern QString numer2[128];
// code 206XX
extern QString numer3[128];
// code 207XX
extern QString numer4[128];
// code 208XX
extern QString numer5[128];
// code 209XX
extern QString numer6[128];
// code 210XX
extern QString numer7[128];
// code 211XX
extern QString numer8[128];
// code 212XX
extern QString numer9[128];
// code 213XX
extern QString numerSelfAppraisal[128];
// code 214XX
extern QString numerFinance[128];
// code 215XX
extern QString numerTalent[128];
// code 216XX
extern QString numerSpirituality[128];
// code 217XX
extern QString numerSexualLife[128];


// code 301XX
extern QString numerPetC[128];
// code 302XX
extern QString numerPetDForecast[128];

// code 401XX
extern QString numerSumComp[128];
// code 402XX
extern QString numerDetComp[128];
// code 403XX
extern QString numerNameComp[128];
// code 404XX
extern QString numerLPComp[128];

extern QString numerCompNumbers[128];
// code 405XX
extern QString numerPMCharacterComp[128];
// code 406XX
extern QString numerPMEnergyComp[128];

// code 501XX
extern QString numerNumInterp[128];


// code 602XX
extern QString numerCelLP[256];
// code 603XX
extern QString numerCelBD[256];
// code 604XX
extern QString numerCelExpr[256];

// code 601XX
extern QString numerCelName[256];
extern QString numerCelFName[256];
extern int numerCelBDT[256];
extern int numerCelILP[256];
extern int numerCelIDAY[256];
extern int numerCelIEXPR[256];


// code 70XXX
extern QString numerHelpers[256];


// code ---
extern QString numerCustomText[256];

// code 900XX
extern QString numerInfo[16];
