#include "inifile.h"

#define RECENT_TAG "[recent]"
#define MAIL_TAG "[mail]"

static const QString globDefFolder("BAPW");



CIniFile::CIniFile()
{
	m_MaxRecentFiles = 20;
	m_RecentFiles.reserve(m_MaxRecentFiles);
	//m_MailInfo.reserve(4);
}


CIniFile::~CIniFile()
{
}


void CIniFile::clear()
{
	m_RecentFiles.clear();
}


bool CIniFile::isRecentFileInList(QString FileName)
{
	for(unsigned int i = 0; i < m_RecentFiles.size(); i++)
	{
		if( QString::compare(m_RecentFiles[i], FileName, Qt::CaseInsensitive) == 0 )
			return true;
	}

	return false;
}


void CIniFile::addRecentFile(QString Name)
{
	Name.replace('\\', '/');

	bool bExist = false;
	for(unsigned int i = 0; i < m_RecentFiles.size(); i++)
	{
		if( QString::compare(m_RecentFiles[i], Name, Qt::CaseInsensitive) == 0 )
		{
			m_RecentFiles.erase(m_RecentFiles.begin()+i);
			m_RecentFiles.insert(m_RecentFiles.begin(), Name);
			bExist = true;
			break;
		}
	}
	if( bExist )
		return;

	m_RecentFiles.insert(m_RecentFiles.begin(), Name);

	if(m_RecentFiles.size() > (unsigned)m_MaxRecentFiles)
		m_RecentFiles.resize(m_MaxRecentFiles);
}


std::vector<QString> CIniFile::getRecentFiles()
{
	return m_RecentFiles;
}

/*
void CIniFile::getMailInfo(QString &SMTPServer, QString &MailFrom,QString &MailTo, QString &Login)
{
	if( m_MailInfo.size() > 0 )
		SMTPServer = m_MailInfo[0];
	if( m_MailInfo.size() > 1 )
		MailFrom = m_MailInfo[1];
	if( m_MailInfo.size() > 2 )
		MailTo = m_MailInfo[2];
	if( m_MailInfo.size() > 3 )
		Login = m_MailInfo[3];
}


void CIniFile::setMailInfo(QString &SMTPServer, QString &MailFrom,QString &MailTo, QString &Login)
{
	m_MailInfo.clear();
	m_MailInfo.push_back(SMTPServer);
	m_MailInfo.push_back(MailFrom);
	m_MailInfo.push_back(MailTo);
	m_MailInfo.push_back(Login);
}*/

QString getDefaultDocFolder(QString folder)
{
	QString Path;

#ifdef _WIN32
	//for widows
	QSettings settings(QSettings::UserScope, "Microsoft", "Windows");
	settings.beginGroup("CurrentVersion/Explorer/Shell Folders");

	Path = settings.value("Personal").toString() + QString("/") + globDefFolder;
	if (folder.size() > 0)
		Path += QString("/") + folder;

#else
	//for mac
	QStringList slist = QProcess::systemEnvironment();
	QString user = "";

	for ( int a=0; a<slist.size(); a++ )
	{
		if ( slist.at(a).startsWith("USER=") )
		{
			user = slist.at(a).mid(5);
			break;
		}
	}

        Path = "/Users/" + user + "/Documents" + QString("/") + globDefFolder;

	if (folder.size() > 0)
		Path += QString("/") + folder;

#endif

	return Path;
}



bool getDefaultFolder(QString &path)
{
	path = getDefaultDocFolder(QString(""));
	return true;
}


void CIniFile::load()
{
	clear();

	QString AppPath;
	getDefaultFolder(AppPath);

	QString FileName = AppPath + "/BAIEF.ini";

	QFile File(FileName);
	if( File.open(QIODevice::ReadOnly|QIODevice::Text) )
	{	
		enum {none = 0, recent = 1, mail = 2};
		int State = none;
		QTextStream in(&File);
		while (!in.atEnd()) 
		{
			QString Line = in.readLine();
			if( Line == RECENT_TAG )
			{
				State = recent;
				continue;
			}
			else if( Line == MAIL_TAG )
			{
				State = mail;
				continue;
			}

			if( State == recent && !Line.isEmpty() )
			{
				//check on file exists
				QString FilePath = Line;
				FilePath.replace('\\', '/');
				if( QFile::exists(Line) && !isRecentFileInList(FilePath) )
					m_RecentFiles.push_back(FilePath);
			}
			/*if( State == mail && !Line.isEmpty() )
				m_MailInfo.push_back(Line);*/
		}	
	}

	if( m_RecentFiles.size() > (unsigned)m_MaxRecentFiles )
		m_RecentFiles.resize(m_MaxRecentFiles);
	/*if( m_MailInfo.size() > 4 )
		m_MailInfo.resize(4);*/
}


void CIniFile::save()
{
	QString AppPath;
	getDefaultFolder(AppPath);

	QDir dir(AppPath);
	if (!dir.exists(AppPath))
		dir.mkpath(AppPath);


	QString FileName = AppPath + "/BAIEF.ini";

	QFile File(FileName);
	if( File.open(QIODevice::Truncate|QIODevice::WriteOnly|QIODevice::Text) )
	{
		QTextStream out(&File);
		out << RECENT_TAG << "\n";
		for(unsigned int i = 0; i < m_RecentFiles.size(); i++)
			out << m_RecentFiles[i] << "\n";

/*		if( !m_MailInfo.empty() )
		{
			out << MAIL_TAG << "\n";
			for( int i = 0; (i < 4) && (i < m_MailInfo.size()); i++ )
				out << m_MailInfo[i] << "\n";
		}*/
	}
}
