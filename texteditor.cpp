#include "texteditor.h"
#include "layout_painter/ccolordialog.h"

//-------------------------------------------------------------------
//------------                                         --------------
//-------------------------------------------------------------------
CTextEditor::CTextEditor(CAppStyle *style, QStringList *tL, QList<QImage> *iL, QList<bool> *dL, QString *contList, int type, int id, QWidget *parent) :
    QDialog(parent)
{
    contentList = contList;

    fontScale = 0;

    gType = type;

    p_textList = tL;
    p_imageLIst = iL;
    p_defailtList = dL;

    appstyle = style;
#ifdef Q_OS_MAC
    qsResPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/");
#else
    qsResPath = QCoreApplication::applicationDirPath() + QString("/images/");
#endif

    res = -1;

    textList = *p_textList;
    imageLIst = *p_imageLIst;
    defailtList = *p_defailtList;

    m_pOk = new QPushButton("Ok", this );
    m_pOk->resize(80, 20);
    connect(m_pOk, SIGNAL(clicked()), this, SLOT(OnOk()));

    m_pCancel = new QPushButton("Cancel", this );
    m_pCancel->resize(80, 20);
    connect(m_pCancel, SIGNAL(clicked()), this, SLOT(OnCancel()));

   // QSize WidSz(350, 300);

    int sy=10;

    m_pFontCombo = new QFontComboBox(this);
    m_pFontCombo->setGeometry(10 + 120, sy+5, 180, 25);
    m_pFontCombo->setFontFilters(QFontComboBox::ScalableFonts);
    m_pFontCombo->setCurrentFont(QFont("Arial"));
	m_pFontCombo->setCurrentIndex(m_pFontCombo->findText(QFontInfo(QFont("Arial")).family()));
    connect(m_pFontCombo, SIGNAL(activated(const QString &)), this, SLOT(OnTextFamily(const QString &)));

    m_pSizeCombo = new QComboBox(this);
    m_pSizeCombo->setGeometry(200 + 120, sy+5, 80, 25);
    m_pSizeCombo->setEditable(true);

    QFontDatabase db;
    foreach(int size, db.standardSizes())
        m_pSizeCombo->addItem(QString::number(size));
    connect(m_pSizeCombo, SIGNAL(activated(const QString &)), this, SLOT(OnTextSize(const QString &)));

    m_pTextEdit = new QTextEdit(this);
    m_pTextEdit->move(10, sy + 40);
    m_pTextEdit->setFontPointSize(16);
	m_pTextEdit->setFontFamily(QFont("Arial").family());

    //connect(m_pTextEdit, SIGNAL(cursorPositionChanged()), this, SLOT(cursorPositionChanged()));
    connect(m_pTextEdit, SIGNAL(currentCharFormatChanged(const QTextCharFormat &)), this, SLOT(currentCharFormatChanged(const QTextCharFormat &)));
    connect(m_pTextEdit, SIGNAL(textChanged()), this, SLOT(editText()));
    //m_pFontToolBar = new QToolBar(this);
    //m_pFontToolBar->move(10, sy+28);

    listBox = new QComboBox(this);
    listBox->setGeometry(200 + 300, sy+5, 80 + 30, 25);


    if (type == RTLoveCompatibility)
    {
        if (id == LCTHeader)
        {
            listBox->addItem("Heading", 0);
        }
        else
        if (id == LCTLifePath || id == LCTDetailedBirthday || id == LCTName)
        {
            for (int i = 11; i < 128; i++)
            {
                if (!contentList[i].isEmpty())
                {
                    listBox->addItem(QString::number(i).insert(1, " + "), i);
                }
            }
        }
        else
        if (id == LCTBirthday)
        {
            for (int i = 0; i < 128; i++)
            {
                if (!contentList[i].isEmpty())
                {
                    if (i == 0)
                        listBox->addItem("Heading", i);
                    else
                        listBox->addItem(QString::number(i), i);
                }
            }
        }
        else
        if (id == LCTFamily)
        {
            listBox->addItem("Heading", 1);
        }
        else
        if (id == LCTCarnalInterests)
        {
            listBox->addItem("Heading", 2);
        }
        else
        if (id == LCTFinal)
        {
            listBox->addItem("Heading", 3);
        }
        else
        if (id == LCTEnergy || id == LCTCharacter)
        {
            for (int i = 0; i < 128; i++)
            {
                if (!contentList[i].isEmpty() || (i==0))
                {
                    if (i == 0)
                        listBox->addItem("Heading", i);
                    else
                        listBox->addItem(QString::number(i/10-1) + " + " + QString::number(i%10-1), i);
                }
            }
        }
        else
        if (id == LCTCustomText)
        {
            listBox->addItem("Heading", 0);
        }

    }
    else
    {
        int offset = 0;

        if (type == RTNumerology &&
                ((id == NTChallenges)
                 || (id == NTBridgeLPE)
                 || (id == NTBridgeHDP)))
            offset = 1;

        int mSize = tL->size();

        if (type != RTCelebrities)
            mSize = 128;

        if ((type == RTNumerology) && ((id == NTBridgeLPEDet) || (id == NTBridgeHDPDet)))
        {
            //listBox->addItem("Heading", 0);
            for (int i = 11; i < 128; i++)
            {
                if (!contentList[i].isEmpty())
                {
                    listBox->addItem(QString::number(i).insert(1, " + "), i);
                }
            }
        } else {
            for (int i = 0; i < mSize; i++)
            {
                if (!contentList[i].isEmpty())
                {
                    if (i == 0 && type != RTCelebrities)
                        listBox->addItem("Heading", i);
                    else
                        listBox->addItem(QString::number(i - offset), i);
                }
            }
        }
    }


    connect(listBox, SIGNAL(currentIndexChanged(int)), this, SLOT(setCurList(int)));

    actionTextBold = new QToolButton(this);
    actionTextBold->setToolTip("Bold");
    actionTextBold->resize(30, 30);
    actionTextBold->move(10, sy);
    connect(actionTextBold, SIGNAL(clicked()), this, SLOT(OnTextBold()));

    actionTextBold->setIcon(QIcon(qsResPath+"text-bold.png"));
    actionTextBold->setIconSize(QSize(24,24));
    actionTextBold->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    actionTextBold->setCheckable(true);

    actionTextItalic = new QToolButton(this);
    actionTextItalic->setToolTip("Italic");
    actionTextItalic->resize(30, 30);
    actionTextItalic->move(45, sy);
    connect(actionTextItalic, SIGNAL(clicked()), this, SLOT(OnTextItalic()));
    actionTextItalic->setIcon(QIcon(qsResPath+"text-italic.png"));
    actionTextItalic->setIconSize(QSize(24,24));
    actionTextItalic->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    actionTextItalic->setCheckable(true);

    actionTextUnderline = new QToolButton(this);
    actionTextUnderline->setToolTip("Underline");
    actionTextUnderline->resize(30, 30);
    actionTextUnderline->move(80, sy);
    connect(actionTextUnderline, SIGNAL(clicked()), this, SLOT(OnTextUnderline()));
    actionTextUnderline->setIcon(QIcon(qsResPath+"text-under.png"));
    actionTextUnderline->setIconSize(QSize(24,24));
    actionTextUnderline->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    actionTextUnderline->setCheckable(true);

    actionTextColor = new QToolButton(this);
    actionTextColor->setToolTip("Color");
    actionTextColor->setText("Color");
    actionTextColor->resize(60, 30);
    actionTextColor->move(420, sy);
    connect(actionTextColor, SIGNAL(clicked()), this, SLOT(OnTextColor()));
    actionTextColor->setIconSize(QSize(24,24));
    actionTextColor->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

    actionSetImage = new QToolButton(this);
    actionSetImage->setToolTip("Image");
    actionSetImage->setText("Image");
    actionSetImage->resize(60, 30);
    actionSetImage->move(600 + 30, sy);
    connect(actionSetImage, SIGNAL(clicked()), this, SLOT(OnImage()));
    actionSetImage->setIconSize(QSize(24,24));
    actionSetImage->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);

    defCheck = new QCheckBox("Default setting", this);
    defCheck->resize(160, 25);
    defCheck->move(680 + 30, sy + 3);
    connect(defCheck, SIGNAL(toggled(bool)), this, SLOT(defSet(bool)));

    m_pTextEdit->setFocus();


    appstyle->setWidgetStyle(this, WTDlg);

    appstyle->setWidgetStyle(m_pOk, WTButton);
    appstyle->setWidgetStyle(m_pCancel, WTButton);

    appstyle->setWidgetStyle(m_pTextEdit, WTEdit);
    appstyle->setWidgetStyle(m_pSizeCombo, WTCombo);
    appstyle->setWidgetStyle(m_pFontCombo, WTCombo);
    appstyle->setWidgetStyle(listBox, WTCombo);

    appstyle->setWidgetStyle(actionTextBold, WTButton);
    appstyle->setWidgetStyle(actionTextItalic, WTButton);
    appstyle->setWidgetStyle(actionTextUnderline, WTButton);
    appstyle->setWidgetStyle(actionTextColor, WTButton);
    appstyle->setWidgetStyle(actionSetImage, WTButton);
    appstyle->setWidgetStyle(defCheck, WTDlg);

    setMinimumSize(800, 600);
    setWindowTitle("Text Editor");


    if (listBox->count() > 0)
    {
        listBox->setCurrentIndex(0);
        setCurList(0);
        setCurList(0);
    }

    setWindowFlags(Qt::Dialog | Qt::WindowTitleHint | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint | Qt::CustomizeWindowHint);
}
//-------------------------------------------------------------------
void CTextEditor::OnCancel()
{
    res = -1;
    close();
}
//-------------------------------------------------------------------
void CTextEditor::OnOk()
{
    *p_textList = textList;
    *p_imageLIst = imageLIst;
    *p_defailtList = defailtList;

    res = 0;
    close();
}
//-------------------------------------------------------------------
void CTextEditor::OnTextItalic()
{
    QTextCharFormat fmt;
    fmt.setFontItalic(actionTextItalic->isChecked());
    mergeFormatOnWordOrSelection(fmt);
}
//-------------------------------------------------------------------
void CTextEditor::OnTextBold()
{
    QTextCharFormat fmt;
    fmt.setFontWeight(actionTextBold->isChecked() ? QFont::Bold : QFont::Normal);
    mergeFormatOnWordOrSelection(fmt);
}
//-------------------------------------------------------------------
void CTextEditor::OnTextUnderline()
{
    QTextCharFormat fmt;
    fmt.setFontUnderline(actionTextUnderline->isChecked());
    mergeFormatOnWordOrSelection(fmt);
}
//-------------------------------------------------------------------
void CTextEditor::OnTextFamily(const QString &Family)
{
    QTextCharFormat fmt;
    fmt.setFontFamily(Family);
    mergeFormatOnWordOrSelection(fmt);
}
//-------------------------------------------------------------------
void CTextEditor::OnTextSize(const QString &SizeStr)
{
    QTextCharFormat fmt;
    fmt.setFontPointSize(SizeStr.toFloat() + fontScale);
    mergeFormatOnWordOrSelection(fmt);
}
//-------------------------------------------------------------------
void CTextEditor::mergeFormatOnWordOrSelection(const QTextCharFormat &format)
{
    QTextCursor cursor = m_pTextEdit->textCursor();
    if (!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);
    cursor.mergeCharFormat(format);
    m_pTextEdit->mergeCurrentCharFormat(format);
}
//-------------------------------------------------------------------
void CTextEditor::OnTextColor()
{
    CColorDialog *dlg = new CColorDialog(true);
    dlg->slotSetColor(globalColor);
    dlg->slotUpdate();
    dlg->exec();
    if (dlg->ground.drawMode != CANCEL_DLG)
    {
       globalColor = dlg->ground.color;
       QTextCharFormat fmt;
       fmt.setForeground(globalColor);
       mergeFormatOnWordOrSelection(fmt);
       colorChanged(globalColor);
    }
    delete dlg;
}
//-------------------------------------------------------------------
void CTextEditor::OnImage()
{
    int id = 0;

    if (gType == RTCelebrities)
        id = listBox->itemData(listBox->currentIndex()).toInt();

    CImagePreview *dlg = new CImagePreview(appstyle, &imageLIst[id], gType, this);
    dlg->exec();
    delete dlg;
}
//-------------------------------------------------------------------
void CTextEditor::resizeEvent(QResizeEvent *event)
{
    int w = event->size().width();
    int h = event->size().height();

#ifdef Q_WS_WIN
    m_pOk->move(w/2-90, h-30);
    m_pCancel->move(w/2+10, h-30);
#else
    m_pOk->move(w/2+10, h-30);
    m_pCancel->move(w/2-90, h-30);
#endif

    m_pTextEdit->resize(w - 20, h - 100);

}
//-------------------------------------------------------------------
void CTextEditor::currentCharFormatChanged(const QTextCharFormat &format)
{
    fontChanged(format.font());
    colorChanged(format.foreground().color());
}
//-------------------------------------------------------------------
void CTextEditor::fontChanged(const QFont &f)
{
    m_pFontCombo->setCurrentIndex(m_pFontCombo->findText(QFontInfo(f).family()));
    m_pSizeCombo->setCurrentIndex(m_pSizeCombo->findText(QString::number(f.pointSize() - fontScale)));
    actionTextBold->setChecked(f.bold());
    actionTextItalic->setChecked(f.italic());
    actionTextUnderline->setChecked(f.underline());
}
//-------------------------------------------------------------------
void CTextEditor::colorChanged(const QColor &c)
{
    globalColor = c;
}
//-------------------------------------------------------------------
QString CTextEditor::upHtmlFontSize(QString html, int addSize)
{
    QString out = html;
    int pos = 0;
    int indxOff = 0;
    while (true)
    {
        indxOff = out.indexOf("font-size:", pos);
        if (indxOff < 0)
            return out;

        pos = indxOff + 10;

        indxOff = out.indexOf("pt;", pos);
        if (indxOff < 0)
            return html;

        QString num = out.mid(pos, indxOff - pos);

        bool ok;
        int size = num.toInt(&ok);

        if (!ok)
            return html;

        out.replace(pos, indxOff - pos, QString::number(size + addSize));
    }
    return out;
}
//-------------------------------------------------------------------
void CTextEditor::setCurList(int list)
{
    disconnect(m_pTextEdit, SIGNAL(textChanged()), this, SLOT(editText()));
    if (!textList.at(listBox->itemData(list).toInt()).isNull())
    {
        QString text = textList.at(listBox->itemData(list).toInt());
        text.replace("<!-- md --><br>", "");
        m_pTextEdit->document()->setHtml(text);
    }
    else
    {
        m_pTextEdit->setText(contentList[listBox->itemData(list).toInt()]);
    }

#ifdef Q_OS_MAC
    fontScale = 4;
    QString html = m_pTextEdit->document()->toHtml();
    html = upHtmlFontSize(html, fontScale);
    m_pTextEdit->document()->setHtml(html);
#endif

    disconnect(defCheck, SIGNAL(toggled(bool)), this, SLOT(defSet(bool)));
    defCheck->setChecked(defailtList.at(listBox->itemData(list).toInt()));
    connect(defCheck, SIGNAL(toggled(bool)), this, SLOT(defSet(bool)));

    QFont font("Arial", 16);
    font.setPointSize(16);
    m_pTextEdit->setFont(font);

    connect(m_pTextEdit, SIGNAL(textChanged()), this, SLOT(editText()));
}
//-------------------------------------------------------------------
void CTextEditor::defSet(bool val)
{
    defailtList[listBox->itemData(listBox->currentIndex()).toInt()] = val;
    if (val == false)
        editText();
}
//-------------------------------------------------------------------
void CTextEditor::editText()
{
    defCheck->setChecked(false);


    if (m_pTextEdit->toPlainText().isEmpty())
    {
        textList[listBox->itemData(listBox->currentIndex()).toInt()].clear();
    }
    else
    {
        QString text = m_pTextEdit->document()->toHtml("UTF-8");
        text.replace("<p", "<!-- md --><br><p");
#ifdef Q_OS_MAC
        textList[listBox->itemData(listBox->currentIndex()).toInt()] = upHtmlFontSize(text, (~fontScale + 1));
#else
        textList[listBox->itemData(listBox->currentIndex()).toInt()] = text;
#endif
    }
}
//-------------------------------------------------------------------
//------------                                         --------------
//-------------------------------------------------------------------
CImagePreview::CImagePreview(CAppStyle *style, QImage *img, int type, QWidget *parent) :
    QDialog(parent)
{
    gType = type;

    image = img;
    gImg = *image;
    appstyle = style;

    m_pOk = new QPushButton("Ok", this );
    m_pOk->resize(80, 20);
    connect(m_pOk, SIGNAL(clicked()), this, SLOT(OnOk()));

    m_pCancel = new QPushButton("Cancel", this );
    m_pCancel->resize(80, 20);
    connect(m_pCancel, SIGNAL(clicked()), this, SLOT(OnCancel()));

    m_pDef = new QPushButton("Default", this );
    m_pDef->resize(80, 20);
    connect(m_pDef, SIGNAL(clicked()), this, SLOT(OnDefImage()));

    m_pOpen = new QPushButton("Open", this );
    m_pOpen->resize(80, 20);
    connect(m_pOpen, SIGNAL(clicked()), this, SLOT(OnImage()));

    label = new QLabel("Default", this);
    label->resize(126, 126);
    label->setAlignment(Qt::AlignCenter);
    label->setStyleSheet("border: 1px solid white; font-size: 12px;");

    appstyle->setWidgetStyle(m_pOk, WTButton);
    appstyle->setWidgetStyle(m_pCancel, WTButton);
    appstyle->setWidgetStyle(m_pDef, WTButton);
    appstyle->setWidgetStyle(m_pOpen, WTButton);


    setFixedSize(400, 320);

    updateImage();
    setWindowFlags(Qt::Tool);
}
//-------------------------------------------------------------------
void CImagePreview::resizeEvent(QResizeEvent *event)
{
    int w = event->size().width();
    int h = event->size().height();

    label->move((w - label->width()) / 2, ((h - label->height()) / 2) - 30);

#ifdef Q_WS_WIN
    m_pOk->move(w/2-140, h-30);
    m_pCancel->move(w/2 - 40, h-30);
    m_pDef->move(w/2+60, h-30);
    m_pOpen->move(m_pCancel->x(), label->y() + label->height() + 20);
#else
    m_pCancel->move(w/2-140, h-30);
    m_pOk->move(w/2 - 40, h-30);
    m_pDef->move(w/2+60, h-30);
    m_pOpen->move(m_pOk->x(), label->y() + label->height() + 20);
#endif



}
//-------------------------------------------------------------------
void CImagePreview::OnOk()
{
    *image = gImg;
    close();
}
//-------------------------------------------------------------------
void CImagePreview::OnCancel()
{
    close();
}
//-------------------------------------------------------------------
void CImagePreview::OnImage()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Open File", QString(), tr("Image Files (*.png *.jpg *.jpeg *.tiff *.tif *.bmp)"));
    if (fileName.isEmpty() || fileName.isNull())
        return;

    gImg.load(fileName);
    if (gType == RTCelebrities)
    {
        if (gImg.width() > 460 || gImg.height() > 460)
            gImg = gImg.scaled(460, 460, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    }
    else
    {
        gImg = gImg.scaled(126, 126, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    }
    updateImage();
}
//-------------------------------------------------------------------
void CImagePreview::OnDefImage()
{
    gImg = QImage();
    updateImage();
}
//-------------------------------------------------------------------
void CImagePreview::updateImage()
{
    if (gImg.isNull())
    {
        label->setText("Default");
    }
    else
    {
        label->setPixmap(QPixmap::fromImage(gImg.scaled(126, 126, Qt::KeepAspectRatio, Qt::SmoothTransformation)));
    }
}
//-------------------------------------------------------------------
//------------                                         --------------
//-------------------------------------------------------------------
