#ifndef NUMWIDGETS_H
#define NUMWIDGETS_H

#include <QtGui>
#include <QtCore>
#include "global.h"
#include "appstyle.h"
#include "includes.h"

class CInfoWidget : public QWidget
{
    Q_OBJECT

public:
    CInfoWidget(CAppStyle *style, QImage titleBar, QWidget *parent = 0, Qt::WFlags flags = 0);
    ~CInfoWidget();

    CAppStyle *appstyle;

    int sizeWith;
	bool hideHover;
	bool enableHover;
	int mouse_x, mouse_y;
	int maxHStrings;

    struct SRes
    {
        QImage titleBar;
    } m_Res;

    QStringList parList;
    QStringList parListVal;
	QLabel *wtitle;
	bool skipMNH;

	void setWindowTitle(QString title)
	{
		wtitle->setText(title);
	}

protected:
    void paintEvent(QPaintEvent * event);
	void mouseMoveEvent(QMouseEvent * event);
	void leaveEvent(QEvent * event);
	void enterEvent(QEvent * event);
	void mousePressEvent(QMouseEvent * event);

signals:
	void itemCliked(int idx);
};


class CInputWidget : public QWidget
{
    Q_OBJECT

public:
    //CInputWidget(QImage topRes, QImage botRes, QImage inpRes, QWidget *parent = 0, Qt::WFlags flags = 0);
    CInputWidget(QWidget *parent = 0);
    ~CInputWidget();


//    struct SRes
//    {
//        QImage topRes;
//        QImage botRes;
//        QImage inpRes;
//    } m_Res;

    QStringList parList;
    QList<QWidget*> parListVal;

    QString wlabel;

  //  void updateStructure();

protected:
  //  void paintEvent(QPaintEvent * event);
};



class CInfoNameWidget : public CInfoWidget
{
    Q_OBJECT

public:
    CInfoNameWidget(CAppStyle *style, QImage titleBar, QWidget *parent = 0, Qt::WFlags flags = 0);
    ~CInfoNameWidget();

    bool m_bSymTrackEna;
	bool m_bMNEna;

    QLabel *m_pMarkLabel;

protected:
    void paintEvent(QPaintEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void leaveEvent(QEvent *);

signals:
    void symbolCliked(int row, int idx);
};


class CInfoEssenceWidget : public CInfoWidget
{
    Q_OBJECT

	bool hideHover;
	bool enableHover;
	int mouse_x, mouse_y;

public:
    CInfoEssenceWidget(CAppStyle *style, QImage titleBar, QWidget *parent = 0, Qt::WFlags flags = 0);
    ~CInfoEssenceWidget();

    int m_Essence[128], m_EssenceS[128];
    int startAge, m_byear;

    QPushButton *m_pSWButton;
    QPixmap cImg;

protected:
    void paintEvent(QPaintEvent * event);

public slots:
    void OnSwitchAge();
    void updateData();
};


class CInfoCalendarWidget : public CInfoWidget
{
    Q_OBJECT

public:
    CInfoCalendarWidget(CAppStyle *style, QImage titleBar, QWidget *parent = 0, Qt::WFlags flags = 0);
    ~CInfoCalendarWidget();

    int m_PersYear;
    QDate cdate;

protected:
    void paintEvent(QPaintEvent * event);
};


class CInfoTodayWidget : public CInfoWidget
{
	Q_OBJECT

public:
    CInfoTodayWidget(CAppStyle *style, QImage titleBar, QWidget *parent = 0, Qt::WFlags flags = 0);
	~CInfoTodayWidget();

signals:
	void clicked();

protected:
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *) { emit clicked(); }
};


class CInfoExprPlanWidget : public CInfoWidget
{
    Q_OBJECT

	bool hideHover;
	bool enableHover;
	int mouse_x, mouse_y;

public:
    CInfoExprPlanWidget(CAppStyle *style, QImage titleBar, QWidget *parent = 0, Qt::WFlags flags = 0);
    ~CInfoExprPlanWidget();

protected:
    void paintEvent(QPaintEvent * event);
};


class CInfoMatrWidget : public QWidget
{
    Q_OBJECT

public:
	bool hideHover;
	bool enableHover;
	int mouse_x, mouse_y;

public:
    CInfoMatrWidget(CAppStyle *style, QImage titleBarQWidget, QWidget *parent = 0, Qt::WFlags flags = 0);
    ~CInfoMatrWidget();

    CAppStyle *appstyle;

    struct SRes
    {
        QImage titleBar;
    } m_Res;

    QStringList parList;
    QStringList parListVal;
    int pmatr[4*4+4];

    void drawSq(QPainter &painter, int x, int y, QString str1, QString str2, int val);

	QLabel *wtitle;

	void setWindowTitle(QString title)
	{
		wtitle->setText(title);
	}

protected:
	void paintEvent(QPaintEvent * event);
	void mouseMoveEvent(QMouseEvent * event);
	void leaveEvent(QEvent * event);
	void enterEvent(QEvent * event);
	void mousePressEvent(QMouseEvent * event);

signals:
	void itemCliked(int idx);
};


class CInfoCompMatrWidget : public QWidget
{
    Q_OBJECT

public:
    CInfoCompMatrWidget(CAppStyle *style, QImage titleBar, QWidget *parent = 0, Qt::WFlags flags = 0);
    ~CInfoCompMatrWidget();

    CAppStyle *appstyle;

    struct SRes
    {
        QImage titleBar;
    } m_Res;

    QStringList parList;
    QStringList parListVal;
    int pmatr[4*4+4];
    int pmatr2[4*4+4];

    void drawSq(QPainter &painter, int x, int y, QString str1, QString str2, int val, int val2);

	QLabel *wtitle;

	void setWindowTitle(QString title)
	{
		wtitle->setText(title);
	}


protected:
    void paintEvent(QPaintEvent * event);
};


class CExpandWidget : public QWidget
{
    Q_OBJECT

private:
	int globalWidth;
	int animTime;
	QPoint startPos;
    QLabel *butLab;
	QPushButton *button;
	QPropertyAnimation *animation;

public:

    CExpandWidget(CAppStyle *style, QWidget *parent, int width = 600, int timeMsAnimation = 200);
    ~CExpandWidget();

    CAppStyle *appstyle;
    QLabel *infoLabel;
	QWidget *bkg;

	void setPos(int x, int y)
	{ startPos = QPoint(x,y); }

public slots:
    void slotExpand();
    void slotUpdate();

protected:
	void paintEvent(QPaintEvent * event);
};

#endif // NUMWIDGETS_H
