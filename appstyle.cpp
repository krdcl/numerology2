#include "appstyle.h"



void ASCheckScreenBoundary(QWidget *winwidg)
{
    if (winwidg!=NULL)
    {
        QDesktopWidget desktop;
        int px=winwidg->x(), py=winwidg->y();
        int ww=winwidg->width(), wh=winwidg->height();
        QRect desktopWid = desktop.availableGeometry(desktop.screenNumber(winwidg));
        if (px+ww>desktopWid.right())
            px=px+ww-desktopWid.right();
        if (py+wh>desktopWid.bottom())
            py=py+wh-desktopWid.bottom();
        if (px<desktopWid.x()) px=desktopWid.x(); if (py<desktopWid.y()) py=desktopWid.y();
        if ((winwidg->x() != px) || (winwidg->y() != py))
            winwidg->move(px, py);
    }
}




CAppStyle::CAppStyle(QString resPath)
{
    dataPath = resPath;
    QString respath = dataPath;


    icnTextBold = QIcon(respath+"text-bold.png");
    icnTextItalic = QIcon(respath+"text-italic.png");
    icnTextUnder = QIcon(respath+"text-under.png");
    lmitem = QPixmap(respath + "lmitem.png");

	sStyleInfo si;
	// Style 0
    stylesNames << "Chocolate";

    prigressBar = "QProgressBar {color: white; text-align: center; border-radius: 5px;} \
            QProgressBar:horizontal {border: 1px solid #173749; background: qlineargradient(x1:0, y1:0, x0:1, y1:1, stop: 0 #76a5ba, stop: 1 #a3c9da); border-radius: 5px;} \
            QProgressBar::chunk:horizontal {background: qlineargradient(x1:0, y1:0, x0:1, y1:1, stop: 0 #40a4cc, stop: 0.40 #6dd4fa, stop: 0.5 #6dd4fa, stop: 0.9 #1f9acd, stop: 1 #24abd6); border-radius: 5px;}";

    QString scrBarStyle0 = "QScrollBar {background-color: #2c2420;} \
                QScrollBar:horizontal { border: 0px; border-top: 1px solid #403935; background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #2c2420, stop:0.5 #352b27 stop:1 #312824); } \
                QScrollBar::handle:horizontal { background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #44372f, stop:1 #4b3d34); border: none; min-width: 20px; border-radius: 4px; margin: 3px 3px 3px 3px; } \
                QScrollBar::handle:horizontal:hover { background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #5b4a3f, stop:1 #5e4c41); } \
                QScrollBar::add-line:horizontal, QScrollBar::sub-line:horizontal { border: none; background: transparent; height: 0px; width: 0px; subcontrol-position: top; subcontrol-origin: margin; } \
                QScrollBar::add-page:horizontal, QScrollBar::sub-page:horizontal { background: none; } \
                QScrollBar:vertical { border: 0px; border-left: 1px solid #403935; background: qlineargradient(x1:0, y1:0, x2:1, y2:0, stop:0 #2c2420, stop:0.5 #352b27 stop:1 #312824); margin: 0px 0px 0px 0px; } \
                QScrollBar::handle:vertical { background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #44372f, stop:1 #4b3d34); border: none; min-height: 20px; border-radius: 4px; margin: 3px 3px 3px 3px; } \
                QScrollBar::handle:vertical:hover { background: qlineargradient(x1:0, y1:0, x2:1, y2:1, stop:0 #5b4a3f, stop:1 #5e4c41); } \
                QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical { border: none; background: transparent; height: 0px; width: 0px; subcontrol-position: top; subcontrol-origin: margin; } \
                QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical { background: none; }\
                QCheckBox { background: none; color: rgb(237,219,178); border:none; }\
               QCheckBox::indicator:unchecked { background: #2c2420; border: 1px solid #594f49; width: 12px; height: 12px; }\
               QCheckBox::indicator:checked { image:url(" + dataPath + "/cbc.png); background: #2c2420; border: 1px solid #594f49; width: 12px; height: 12px; }";
    QString editBoxStyle0 = "QLineEdit { background-color: #2c2420; border: 1px solid #5c524c; color: #eddbb2; } \
            QLineEdit[isMassEdit=\"true\"], QPlainTextEdit[isMassEdit=\"true\"] { color:black; padding-left: 5px; padding-right: 5px; background:  #C0C0C0; border: 1px solid #B0B0B0; border-radius: 4px; /*STYLE_MOD*/ } \
            QLineEdit:disabled, QLineEdit:read-only { background: #DDDDDD; border: 1px solid #B0B0B0; border-radius: 4px; } \
            QLineEdit:disabled[isMassEdit=\"true\"], QLineEdit:read-only[isMassEdit=\"true\"] { background: #C0C0C0; border: 1px solid #B0B0B0; border-radius: 4px; } \
            QTextEdit, QPlainTextEdit { padding-left: 5px; padding-right: 2px; background: white; border: 1px solid #5c524c; border-radius: 0px; /*STYLE_MOD*/ } \
            QTextEdit:disabled, QTextEdit:read-only, QPlainTextEdit:disabled, QPlainTextEdit:read-only { background: #DDDDDD; border: 1px solid #B0B0B0; border-radius: 4px; }";

    QString comboBoxStyle0 = "QDateEdit QWidget { background-color: #2c2420; border: 1px solid #5c524c; color: #eddbb2; selection-background-color: #54443a; }\
            QComboBox, QDateEdit { background-color: #2c2420; border: 1px solid #5c524c; color: rgb(237,219,178); padding-left: 10px; border-radius: 0px; font-size: 12px;}\
            QComboBox::drop-down, QDateEdit::drop-down { border:none; }\
            QComboBox::down-arrow, QDateEdit::down-arrow { image:url(" + dataPath + "/darr.png); }\
            QComboBox::down-arrow:hover, QDateEdit::down-arrow:hover { image:url(" + dataPath + "/darrh.png); }\
            QComboBox QAbstractItemView { background: #44372f; border: 1px solid #5c524c; color: #eddbb2; selection-background-color: #54443a; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px; }";
    QString tableStyle0 = "QTableWidget {margin: 0px; padding: 0px; color: rgb(237,219,178); background: #2c2420; border: 1px solid #403935; selection-color: rgb(237,219,178); selection-background-color: #2c2420; alternate-background-color: #e6f0fe; /*FONT_SCALE_12PX*/}\
                          QHeaderView { border-radius: 0px; background: #2c2420; border: 0px; } \
                          QHeaderView::section, QTableCornerButton::section, QTableCornerButton { border-radius: 0px; border: 0px solid #5c524c; border-right-width: 0px; color: rgb(237,219,178); background: #44372f; min-height: 25px; }\
						  QHeaderView::down-arrow { subcontrol-origin: margin; subcontrol-position: top right; margin-right: 10px; margin-top: 1px; width: 10px; height: 10px; }\
						  QHeaderView::up-arrow { subcontrol-origin: margin; subcontrol-position: top right; margin-right: 10px; margin-top: 1px; width: 10px; height: 10px; }\
                          QTableWidget::item  { border-radius: 0px; border: 1px solid transparent; }";
    QString dateStyle0 = "QDateEdit QWidget { background-color: #2c2420; border: 1px solid #5c524c; color: #eddbb2; selection-background-color: #54443a; }\
           QComboBox, QDateEdit { background-color: #2c2420; border: 1px solid #5c524c; color: rgb(237,219,178); padding-left: 10px; border-radius: 0px; font-size: 12px;}\
           QComboBox::drop-down, QDateEdit::drop-down { border:none; }\
           QComboBox::down-arrow, QDateEdit::down-arrow { image:url(" + dataPath + "/darr.png); }\
           QComboBox::down-arrow:hover, QDateEdit::down-arrow:hover { image:url(" + dataPath + "/darrh.png); }\
           QComboBox QAbstractItemView { background: #44372f; border: 1px solid #5c524c; color: #eddbb2; selection-background-color: #54443a; padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;}";
    QString listStyle0 = "QListWidget, QListView { color:black; background: #ffffff; border: 0px solid #403935; border-radius: 4px; /*STYLE_MOD*/ }\
            QListWidget[isMassEdit=\"true\"] { color:black; background: #C0C0C0; border: 0px solid #403935; border-radius: 4px; /*STYLE_MOD*/}\
            QListWidget::corner { background: none; border: none; }\
            QListWidget QLineEdit { border: 1px solid #000000;}";

    scrBarStyle0 += editBoxStyle0 + comboBoxStyle0 + tableStyle0 + dateStyle0;

    si.ss = "QWidget { color:black; background: #ffffff; border: 1px solid #403935; border-radius: 0px; /*STYLE_MOD*/ } \
            QWidget[isPLayout=\"true\"] { color:black; background: #ffffff; border: 0px solid #99C701; border-radius: 0px; /*STYLE_MOD*/}\
            QWidget[isMassEdit=\"true\"] { color:black; background: #C0C0C0; border: 1px solid #403935; border-radius: 0px; }\
            QToolButton[isRectButton=\"true\"], QPushButton[isRectButton=\"true\"] { color: #111111; /*FONT_SCALE_12PX*/ background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #eeeeee, stop:0.5 #dddddd stop:1.0 #bbbbbb); border: 0px solid #888888; border-radius: 0px; } \
              QToolButton:hover[isRectButton=\"true\"], QPushButton:hover[isRectButton=\"true\"] { background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #ffffff, stop:0.5 #eeeeee stop:1.0 #cccccc);  border-radius: 0px;}\
              QToolButton:pressed[isRectButton=\"true\"], QPushButton:pressed[isRectButton=\"true\"] { background: #bbbbbb; }\
              QToolButton:disabled[isRectButton=\"true\"], QPushButton:disabled[isRectButton=\"true\"] { color: #dddddd; background: qlineargradient(x1:0, y1:0, x2:0, y2:1, stop:0 #dddddd, stop:0.5 #cccccc stop:1.0 #aaaaaa); border-radius: 0px; } \
            QPushButton,QToolButton {  background: #44372f; border: 0px; border-radius: 6px; color: rgb(237,219,178); font-size: 12px;} \
              QPushButton:hover, QToolButton:hover { background: #54443a; }\
              QPushButton:pressed, QToolButton:pressed { background: #2d241f; }\
              QPushButton:disabled, QToolButton:disabled {color: black; background: #312722; border: 0px; border-radius: 6px; } " + scrBarStyle0 + listStyle0;
    si.dlgss = "QWidget { background-color: #1e1917; } QToolTip { background-color: #1e1917; color: #9cb62d; font: bold 9px;/*color:black; background: #9f9f9f; border: 0px solid #403935;*/ }\
            QScrollArea { background: #2c2420; border: 1px solid #403935; } \
            QLabel{ color: rgb(237,219,178); background: transparent; border: none; font-size: 10px; } \
            QMenu { background: #2c2420; border: 1px solid #5c524c; color: #eddbb2; selection-background-color: #54443a; }" + scrBarStyle0;

    si.labss = "QWidget { background: transparent; border: none; font-size: 12px; }";
    si.mainwss = "QWidget { color: rgb(237,219,178); font-size: 12px; background: transparent; border: none; }\
            QTreeWidget, QListWidget, QListView { color: rgb(237,219,178); background: #2c2420; border: 1px solid #3e3733; border-radius: 0px; /*STYLE_MOD*/ }\
            QListWidget::corner, QTreeWidget::corner { background: none; border: none; }\
            QTreeView::branch:has-children:!has-siblings:closed, QTreeView::branch:closed:has-children:has-siblings { border-image: none; image: url(" + dataPath + "/node_close.png); }\
            QTreeView::branch:open:has-children:!has-siblings, QTreeView::branch:open:has-children:has-siblings { border-image: none; image: url(" + dataPath + "/node_open.png); }\
            QListWidget::item {\
                color: rgb(237,219,178); height: 22px; /*TREE_ITEM_HEIGHT_22PX*/ \
                 border: 0px solid #d9d9d9;\
                border-top-color: transparent;\
                border-bottom-color: transparent;\
            }\
            QListWidget::item:hover {\
                background: #44372f;\
                border: 1px solid #44372f; color: rgb(237,219,178);\
            }\
            QListWidget::item:selected {\
                border: 0px solid #8fbae7; background: #44372f; color: rgb(237,219,178);\
            }\
            QListWidget::item:selected:active{\
                background: #44372f; color: rgb(237,219,178);\
            }\
            QListWidget::item::has-children {\
                margin: 2px 2px 2px 2px; color: rgb(237,219,178);\
                background: #44372f; \
                border: 0px solid #BBBBBB; border-radius: 0px;\
            } \
            QWebView, QGraphicsView { color:black; background: #ffffff; border: 0px solid #99C701; border-radius: 0px; } " + scrBarStyle0;

    si.okbut = "QPushButton { background: #44372f; border: 0px; border-radius: 6px; color: #eddbb2; font-size: 12px;} \
            QPushButton:hover, QToolButton:hover { background: #54443a; }\
            QPushButton:pressed, QToolButton:pressed { background: #2d241f; }\
            QPushButton:disabled, QToolButton:disabled { color: #eddbb2; background: #1d140f; border: 0px; border-radius: 6px; }";
    si.canbut = "QPushButton { background: #44372f; border: 0px; border-radius: 6px; color: #eddbb2; font-size: 12px;} \
            QPushButton:hover, QToolButton:hover { background: #54443a; }\
            QPushButton:pressed, QToolButton:pressed { background: #2d241f; }\
            QPushButton:disabled, QToolButton:disabled { color: #eddbb2; background: #1d140f; border: 0px; border-radius: 6px; }";

    si.qsCtrlButStyle = "QWidget { color: rgb(237,219,178)/*#eddbb2*/; font-size: 12px; background: transparent; border: none; text-align: left; margin: 5px 5px 5px 10px; }";

    si.qsPersSelButStyle = "QPushButton, QPushButton:pressed { background: #44372f; border: 0px; border-radius: 6px; border-image: url(" + dataPath + "/pselbut.png); color: rgb(237,219,178); padding-right:50px; font-size: 14px; } \
                                         QPushButton:hover { background: #54443a; border-image: url(" + dataPath + "/pselbuth.png); }";
    si.qsSelLabStyle = "QLabel { background: #44372f; border: none; }";
    si.qsPersBkgStyle = "background-color: #1e1917; border: 1px solid #5c524c;";
    si.qsSelPersIWidgStyle = "QWidget { background: #2c2420; border: 1px solid #3e3733; }";

    QString tw = "QTreeWidget { show-decoration-selected: 1; selection-background-color: transparent; border-radius: 0px; /*FONT_SCALE_12PX*/}\
        QTreeWidget::item {\
            color: rgb(237,219,178); height: 22px; /*TREE_ITEM_HEIGHT_22PX*/ \
             border: 0px solid #d9d9d9;\
            border-top-color: transparent;\
            border-bottom-color: transparent;\
        }\
        QTreeWidget::item:hover {\
            background: #44372f;\
            border: 1px solid #44372f; color: rgb(237,219,178);\
        }\
        QTreeWidget::item:selected {\
            border: 0px solid #8fbae7; background: #44372f; color: rgb(237,219,178);\
        }\
        QTreeWidget::item:selected:active{\
            background: #44372f; color: rgb(237,219,178);\
        }\
        QTreeWidget::item::has-children {\
            margin: 2px 2px 2px 2px; color: rgb(237,219,178);\
            background: #44372f; \
            border: 0px solid #BBBBBB; border-radius: 0px;\
        }";

    si.qsButRect = "QPushButton {background-color: #2c2420; border: 1px solid #5c524c; color: #eddbb2; image:url(" + dataPath + "IMAGE_S);}\
            QPushButton:hover { image:url(" + dataPath + "IMAGE_H);}\
            QPushButton:pressed { image:url(" + dataPath + "IMAGE_P);}";

    si.repss = tw;


	si.genuine = true;
	styleInfo << si;

	QColor gscol;
	int st=0;
	si.genuine = false;

	// Style 1
	stylesNames << "Blue";
	gscol = QColor::fromRgb(200, 220, 255);
	si.gs = gscol;
	si.strength = 0;
	si.ss = genStyle(styleInfo.at(0).ss, gscol, 0);
    si.dlgss = genStyle(styleInfo.at(0).dlgss, gscol, 0);
	si.labss = genStyle(styleInfo.at(0).labss, gscol, 0);
	si.mainwss = genStyle(styleInfo.at(0).mainwss, gscol, 0);
	si.canbut = styleInfo.at(0).canbut;
	si.okbut = styleInfo.at(0).okbut;
    si.repss = genStyle(styleInfo.at(0).repss, gscol, 0);
    si.qsCtrlButStyle = genStyle(styleInfo.at(0).qsCtrlButStyle, gscol, 0);
    si.qsPersSelButStyle = genStyle(styleInfo.at(0).qsPersSelButStyle, gscol, 0);
    si.qsSelLabStyle = genStyle(styleInfo.at(0).qsSelLabStyle, gscol, 0);
    si.qsPersBkgStyle = genStyle(styleInfo.at(0).qsPersBkgStyle, gscol, 0);
    si.qsSelPersIWidgStyle = genStyle(styleInfo.at(0).qsSelPersIWidgStyle, gscol, 0);
    si.qsButRect = genStyle(styleInfo.at(0).qsButRect, gscol, 0);
	styleInfo << si;

	// Style 2
	stylesNames << "Blue Sky";
#ifdef Q_WS_WIN
	gscol = QColor::fromRgb(125, 200, 250);
#else
	gscol = QColor::fromRgb(99, 188, 249);
#endif

	si.gs = gscol;
	si.strength = 0;
	si.ss = genStyle(styleInfo.at(0).ss, gscol, 0);
    si.dlgss = genStyle(styleInfo.at(0).dlgss, gscol, 0);
	si.labss = genStyle(styleInfo.at(0).labss, gscol, 0);
	si.mainwss = genStyle(styleInfo.at(0).mainwss, gscol, 0);
	si.canbut = styleInfo.at(0).canbut;
	si.okbut = styleInfo.at(0).okbut;
    si.repss = genStyle(styleInfo.at(0).repss, gscol, 0);
    si.qsCtrlButStyle = genStyle(styleInfo.at(0).qsCtrlButStyle, gscol, 0);
    si.qsPersSelButStyle = genStyle(styleInfo.at(0).qsPersSelButStyle, gscol, 0);
    si.qsSelLabStyle = genStyle(styleInfo.at(0).qsSelLabStyle, gscol, 0);
    si.qsPersBkgStyle = genStyle(styleInfo.at(0).qsPersBkgStyle, gscol, 0);
    si.qsSelPersIWidgStyle = genStyle(styleInfo.at(0).qsSelPersIWidgStyle, gscol, 0);
    si.qsButRect = genStyle(styleInfo.at(0).qsButRect, gscol, 0);
	styleInfo << si;

	// Style 3
	stylesNames << "Lavender";
    gscol = QColor::fromRgb(204, 185, 240);
	si.gs = gscol;
	si.strength = 0;
	si.ss = genStyle(styleInfo.at(0).ss, gscol, 0);
    si.dlgss = genStyle(styleInfo.at(0).dlgss, gscol, 0);
	si.labss = genStyle(styleInfo.at(0).labss, gscol, 0);
	si.mainwss = genStyle(styleInfo.at(0).mainwss, gscol, 0);
	si.canbut = styleInfo.at(0).canbut;
	si.okbut = styleInfo.at(0).okbut;
    si.repss = genStyle(styleInfo.at(0).repss, gscol, 0);
    si.qsCtrlButStyle = genStyle(styleInfo.at(0).qsCtrlButStyle, gscol, 0);
    si.qsPersSelButStyle = genStyle(styleInfo.at(0).qsPersSelButStyle, gscol, 0);
    si.qsSelLabStyle = genStyle(styleInfo.at(0).qsSelLabStyle, gscol, 0);
    si.qsPersBkgStyle = genStyle(styleInfo.at(0).qsPersBkgStyle, gscol, 0);
    si.qsSelPersIWidgStyle = genStyle(styleInfo.at(0).qsSelPersIWidgStyle, gscol, 0);
    si.qsButRect = genStyle(styleInfo.at(0).qsButRect, gscol, 0);
	styleInfo << si;

	// Style 4
	stylesNames << "Rose";
	gscol = QColor::fromRgb(255, 213, 236);
	si.gs = gscol;
	si.strength = 0;
	si.ss = genStyle(styleInfo.at(0).ss, gscol, 0);
    si.dlgss = genStyle(styleInfo.at(0).dlgss, gscol, 0);
	si.labss = genStyle(styleInfo.at(0).labss, gscol, 0);
	si.mainwss = genStyle(styleInfo.at(0).mainwss, gscol, 0);
	si.canbut = styleInfo.at(0).canbut;
	si.okbut = styleInfo.at(0).okbut;
    si.repss = genStyle(styleInfo.at(0).repss, gscol, 0);
    si.qsCtrlButStyle = genStyle(styleInfo.at(0).qsCtrlButStyle, gscol, 0);
    si.qsPersSelButStyle = genStyle(styleInfo.at(0).qsPersSelButStyle, gscol, 0);
    si.qsSelLabStyle = genStyle(styleInfo.at(0).qsSelLabStyle, gscol, 0);
    si.qsPersBkgStyle = genStyle(styleInfo.at(0).qsPersBkgStyle, gscol, 0);
    si.qsSelPersIWidgStyle = genStyle(styleInfo.at(0).qsSelPersIWidgStyle, gscol, 0);
    si.qsButRect = genStyle(styleInfo.at(0).qsButRect, gscol, 0);
	styleInfo << si;

	// Style 5
	stylesNames << "Moss";
	gscol = QColor::fromRgb(204, 237, 224);
	si.gs = gscol;
	si.strength = 0;
	si.ss = genStyle(styleInfo.at(0).ss, gscol, 0);
    si.dlgss = genStyle(styleInfo.at(0).dlgss, gscol, 0);
	si.labss = genStyle(styleInfo.at(0).labss, gscol, 0);
	si.mainwss = genStyle(styleInfo.at(0).mainwss, gscol, 0);
	si.canbut = styleInfo.at(0).canbut;
	si.okbut = styleInfo.at(0).okbut;
    si.repss = genStyle(styleInfo.at(0).repss, gscol, 0);
    si.qsCtrlButStyle = genStyle(styleInfo.at(0).qsCtrlButStyle, gscol, 0);
    si.qsPersSelButStyle = genStyle(styleInfo.at(0).qsPersSelButStyle, gscol, 0);
    si.qsSelLabStyle = genStyle(styleInfo.at(0).qsSelLabStyle, gscol, 0);
    si.qsPersBkgStyle = genStyle(styleInfo.at(0).qsPersBkgStyle, gscol, 0);
    si.qsSelPersIWidgStyle = genStyle(styleInfo.at(0).qsSelPersIWidgStyle, gscol, 0);
    si.qsButRect = genStyle(styleInfo.at(0).qsButRect, gscol, 0);
	styleInfo << si;

	// Style 6
	stylesNames << "Beige";
	gscol = QColor::fromRgb(233, 213, 192);
	si.gs = gscol;
	si.strength = 0;
	si.ss = genStyle(styleInfo.at(0).ss, gscol, 0);
    si.dlgss = genStyle(styleInfo.at(0).dlgss, gscol, 0);
	si.labss = genStyle(styleInfo.at(0).labss, gscol, 0);
	si.mainwss = genStyle(styleInfo.at(0).mainwss, gscol, 0);
	si.canbut = styleInfo.at(0).canbut;
	si.okbut = styleInfo.at(0).okbut;
    si.repss = genStyle(styleInfo.at(0).repss, gscol, 0);
    si.qsCtrlButStyle = genStyle(styleInfo.at(0).qsCtrlButStyle, gscol, 0);
    si.qsPersSelButStyle = genStyle(styleInfo.at(0).qsPersSelButStyle, gscol, 0);
    si.qsSelLabStyle = genStyle(styleInfo.at(0).qsSelLabStyle, gscol, 0);
    si.qsPersBkgStyle = genStyle(styleInfo.at(0).qsPersBkgStyle, gscol, 0);
    si.qsSelPersIWidgStyle = genStyle(styleInfo.at(0).qsSelPersIWidgStyle, gscol, 0);
    si.qsButRect = genStyle(styleInfo.at(0).qsButRect, gscol, 0);
	styleInfo << si;

	// Style 7
	//stylesNames << "Nut";
	gscol = QColor::fromRgb(163, 124, 88);
	st=60;
	si.gs = gscol;
	si.strength = st;
	si.ss = genStyle(styleInfo.at(0).ss, gscol, st);
	si.dlgss = genStyle(styleInfo.at(0).dlgss, gscol, st);
	si.labss = genStyle(styleInfo.at(0).labss, gscol, st);
	si.mainwss = genStyle(styleInfo.at(0).mainwss, gscol, st);
	si.canbut = styleInfo.at(0).canbut;
	si.okbut = styleInfo.at(0).okbut;
    si.repss = genStyle(styleInfo.at(0).repss, gscol, 0);
    si.qsCtrlButStyle = genStyle(styleInfo.at(0).qsCtrlButStyle, gscol, 0);
    si.qsPersSelButStyle = genStyle(styleInfo.at(0).qsPersSelButStyle, gscol, 0);
    si.qsSelLabStyle = genStyle(styleInfo.at(0).qsSelLabStyle, gscol, 0);
    si.qsPersBkgStyle = genStyle(styleInfo.at(0).qsPersBkgStyle, gscol, 0);
    si.qsSelPersIWidgStyle = genStyle(styleInfo.at(0).qsSelPersIWidgStyle, gscol, 0);
    si.qsButRect = genStyle(styleInfo.at(0).qsButRect, gscol, 0);
	//styleInfo << si;

	// Style 8
	//stylesNames << "Blackberry";
	gscol = QColor::fromRgb(146, 101, 116);
	st=55;
	si.gs = gscol;
	si.strength = st;
	si.ss = genStyle(styleInfo.at(0).ss, gscol, st);
	si.dlgss = genStyle(styleInfo.at(0).dlgss, gscol, st);
	si.labss = genStyle(styleInfo.at(0).labss, gscol, st);
	si.mainwss = genStyle(styleInfo.at(0).mainwss, gscol, st);
	si.canbut = styleInfo.at(0).canbut;
	si.okbut = styleInfo.at(0).okbut;
    si.repss = genStyle(styleInfo.at(0).repss, gscol, 0);
    si.qsCtrlButStyle = genStyle(styleInfo.at(0).qsCtrlButStyle, gscol, 0);
    si.qsPersSelButStyle = genStyle(styleInfo.at(0).qsPersSelButStyle, gscol, 0);
    si.qsSelLabStyle = genStyle(styleInfo.at(0).qsSelLabStyle, gscol, 0);
    si.qsPersBkgStyle = genStyle(styleInfo.at(0).qsPersBkgStyle, gscol, 0);
    si.qsSelPersIWidgStyle = genStyle(styleInfo.at(0).qsSelPersIWidgStyle, gscol, 0);
    si.qsButRect = genStyle(styleInfo.at(0).qsButRect, gscol, 0);
	//styleInfo << si;

	// Style 9
	//stylesNames << "Graphite";
	gscol = QColor::fromRgb(255, 255, 255);
	st=50;
	si.gs = gscol;
	si.strength = st;
	si.ss = genStyle(styleInfo.at(0).ss, gscol, st);
	si.dlgss = genStyle(styleInfo.at(0).dlgss, gscol, st);
	si.labss = genStyle(styleInfo.at(0).labss, gscol, st);
	si.mainwss = genStyle(styleInfo.at(0).mainwss, gscol, st);
	si.canbut = styleInfo.at(0).canbut;
	si.okbut = styleInfo.at(0).okbut;
    si.repss = genStyle(styleInfo.at(0).repss, gscol, 0);
    si.qsCtrlButStyle = genStyle(styleInfo.at(0).qsCtrlButStyle, gscol, 0);
    si.qsPersSelButStyle = genStyle(styleInfo.at(0).qsPersSelButStyle, gscol, 0);
    si.qsSelLabStyle = genStyle(styleInfo.at(0).qsSelLabStyle, gscol, 0);
    si.qsPersBkgStyle = genStyle(styleInfo.at(0).qsPersBkgStyle, gscol, 0);
    si.qsSelPersIWidgStyle = genStyle(styleInfo.at(0).qsSelPersIWidgStyle, gscol, 0);
    si.qsButRect = genStyle(styleInfo.at(0).qsButRect, gscol, 0);
	//styleInfo << si;


    // Page style
    loadImage(QString(respath+"repage-left.png"), spagerep.pageLeft);
    loadImage(QString(respath+"repage-right.png"), spagerep.pageRight);
    loadImage(QString(respath+"repage-top.png"), spagerep.pageTop);
    loadImage(QString(respath+"repage-bot.png"), spagerep.pageBottom);

    loadImage(QString(respath+"repage-lt.png"), spagerep.pageLT);
    loadImage(QString(respath+"repage-rt.png"), spagerep.pageRT);
    loadImage(QString(respath+"repage-lb.png"), spagerep.pageLB);
    loadImage(QString(respath+"repage-rb.png"), spagerep.pageRB);

    spagerep.canvasLT = QColor(spagerep.pageLT.pixel(0,0));
    spagerep.canvasRT = QColor(spagerep.pageRT.pixel(spagerep.pageLT.width()-1,0));
    spagerep.canvasLB = QColor(spagerep.pageLB.pixel(0,spagerep.pageLT.height()-1));
    spagerep.canvasRB = QColor(spagerep.pageRB.pixel(spagerep.pageLT.width()-1,spagerep.pageLT.height()-1));

    spagerep.pageColor = QColor::fromRgb(255, 255, 255, 255);

    loadImage(QString(respath+"move-s.png"), spagerep.iconMoveS);
    loadImage(QString(respath+"rotate-s.png"), spagerep.iconRotateS);
    loadImage(QString(respath+"scale-s.png"), spagerep.iconScaleS);
    loadImage(QString(respath+"scale-i.png"), spagerep.iconScaleI);
    loadImage(QString(respath+"scale-res-i.png"), spagerep.iconScaleResI);
    loadImage(QString(respath+"datafield-i.png"), spagerep.iconDataField);
    loadImage(QString(respath+"datafieldtitle-i.png"), spagerep.iconDataFieldTitle);
    loadImage(QString(respath+"repimage.png"), spagerep.imageDataField);
    loadImage(respath + "psq.png", titleBar1);
    loadImage(respath + "expandwidget.png", expandwidget);

    setGlobalStyle(0);
}
//-----------------------------------------------------------------
bool CAppStyle::loadImage(QString name, QImage &img)
{
    QImage image;

    bool res = image.load(name);
    img = image;
    return res;
}
//-----------------------------------------------------------------
QImage CAppStyle::drawRectToImage(QImage img, int x, int y, int w, int h, int size, QColor color)
{
    QPainter painter(&img);
    QPen pen;
    pen.setWidth(size);
    pen.setColor(color);
    painter.setPen(pen);
    painter.drawRect(x, y, w, h);
    return img;
}
//-----------------------------------------------------------------
QString CAppStyle::genStyleToItem(QFont font, QColor color, qreal scale)
{
    if (font.family().isEmpty())
    {
        font.setFamily("Arial");
        font.setPointSize(12 * scale);
    }
    else
    {
        font.setPointSize(font.pointSize() * scale);
    }

    QString style = "color:" + color.name() + "; ";
    style += "font-size:" + QString::number(font.pointSize()) + "px; ";
    style += "font-family:" + font.family() + ";";
    if (font.bold())
        style += " font-weight: bold;";
    if (font.italic())
        style += " font-style: italic;";
    if (font.underline())
        style += " text-decoration: underline;";

    return style;
}
//-----------------------------------------------------------------
void CAppStyle::genStyleToItem(QWidget *widget, QWidget *widget2, QMap <QString, QString> map, qreal scale)
{
    if (widget != NULL)
    {
        QFont font;
        font.fromString(map["Title/Font"]);
        QColor color;
        color.setNamedColor(map["Title/Color"]);
        QString styleT = genStyleToItem(font, color, scale);
        widget->setStyleSheet(styleT);
    }

    QFont font2;
    font2.fromString(map["Edit/Font"]);
    QColor color2;
    color2.setNamedColor(map["Edit/Color"]);
    QString styleT = genStyleToItem(font2, color2, scale);
    QString tmpS = widget2->styleSheet();
    tmpS.replace("/*STYLE_MOD*/", styleT);
    widget2->setStyleSheet(tmpS);
}
//-----------------------------------------------------------------
void CAppStyle::setGlobalStyle(int id)
{
    if ((id>=styleInfo.size()) || id<0)
        id = 0;

    cstyle = id;
    defss = styleInfo.at(cstyle).ss;
    defdlgss = styleInfo.at(cstyle).dlgss;
    deflabss = styleInfo.at(cstyle).labss;
    defmainwss = styleInfo.at(cstyle).mainwss;
    defokbut = styleInfo.at(cstyle).okbut;
    defcanbut = styleInfo.at(cstyle).canbut;
    defrepss = styleInfo.at(cstyle).repss;
    defQsCtrlButStyle = styleInfo.at(cstyle).qsCtrlButStyle;
    defQsPersSelButStyle = styleInfo.at(cstyle).qsPersSelButStyle;
    defQsSelLabStyle = styleInfo.at(cstyle).qsSelLabStyle;
    defQsPersBkgStyle = styleInfo.at(cstyle).qsPersBkgStyle;
    defQsSelPersIWidgStyle = styleInfo.at(cstyle).qsSelPersIWidgStyle;
    defButRect = styleInfo.at(cstyle).qsButRect;

    modifTitleBar1 = updateImage(titleBar1);

    //qDebug() << "appstyle";
}
//------------------------------------------------------------
void CAppStyle::setWidgetStyle(QWidget *widget, WidgetType wt)
{
    if (wt==WTDlg)
    {
        widget->setStyleSheet(defdlgss);
    }
    else if (wt==WTLabel)
    {
        widget->setStyleSheet(deflabss);
    }
    else if (wt==WTMainList)
    {
        widget->setStyleSheet(defmainwss);
    }
    else if (wt==WTEdit)
    {
        widget->setStyleSheet(defdlgss);
    }
/*    else if (wt==WTIOkButton)
    {
        widget->setStyleSheet(defokbut);
    } else if (wt==WTICancelButton)
    {
        widget->setStyleSheet(defcanbut);
    } */else if (wt==WTTree)
    {
        //widget->setStyleSheet(defsysmax);
    } else if (wt==WTCustomTree)
    {
        widget->setStyleSheet(defrepss);
    }
    else if (wt == WTCtrlButStyle)
    {
        widget->setStyleSheet(defQsCtrlButStyle);
    }
    else if (wt == WTQsPersSelBut)
    {
        widget->setStyleSheet(defQsPersSelButStyle);
    }
    else if (wt == WTQsSelLab)
    {
        widget->setStyleSheet(defQsSelLabStyle);
    }
    else if (wt == WTQsPersBkg)
    {
        widget->setStyleSheet(defQsPersBkgStyle);
    }
    else if (wt == WTQsSelPersIWidg)
    {
        widget->setStyleSheet(defQsSelPersIWidgStyle);
    }
    else if (wt == WTButRect)
    {
        widget->setStyleSheet(defButRect);
    }
    else
    {
        widget->setStyleSheet(defss);
    }
}
//------------------------------------------------------------
QString CAppStyle::genStyle(QString style, QColor gcol, int strength)
{
	QString r = "";
	int ch = gcol.hslHue();
	int cs = gcol.hsvSaturation();
	int skp=0;
	for (int a=0; a<style.size(); a++)
	{
		if (skp==0)
		{
			if (style.at(a) == '#')
			{
				bool ok=false;
				int col = style.mid(a+1, 6).toInt(&ok, 16);
				int l = ((col & 0xFF) + ((col>>8) & 0xFF) + ((col>>16) & 0xFF))/3;
				QColor rcol = QColor::fromHsl(ch, cs, l);
				if (strength!=0)
					rcol = QColor::fromHsv(ch, cs, (l*strength)/100);

				int rcode = (rcol.red() << 16) + (rcol.green() << 8) + (rcol.blue());

				r += "#" + QString("%1").arg(rcode, 6, 16, QChar('0'));
				skp = 6;
			} else
			{
				r += style.at(a);
			}
		} else
		{
			skp--;
		}
	}

	return r;
}
//------------------------------------------------------------
QColor CAppStyle::updateColor(QColor c)
{
	if (styleInfo.at(cstyle).genuine)
		return c;
	else
	{
		int ch = styleInfo.at(cstyle).gs.hslHue();
		int cs = styleInfo.at(cstyle).gs.hsvSaturation();
		int l = (c.red() + c.green() + c.blue())/3;
		QColor rcol = QColor::fromHsl(ch, cs, l);
		if (styleInfo.at(cstyle).strength!=0)
			rcol = QColor::fromHsv(ch, cs, (l*styleInfo.at(cstyle).strength)/100);
		return rcol;
	}
}
//------------------------------------------------------------
QImage CAppStyle::updateImage(QImage image)
{
    QImage outImg = image;
    for (int x = 0; x < outImg.width(); x++)
    {
        for (int y = 0; y < outImg.height(); y++)
        {
            outImg.setPixel(x, y, updateColor(QColor(image.pixel(x, y))).rgba());
        }
    }
    outImg.setAlphaChannel(image.alphaChannel());
    return outImg;
}
//------------------------------------------------------------
QString CAppStyle::updateColor(QString c)
{
	if (styleInfo.at(cstyle).genuine)
		return c;
	else
		return genStyle(c, styleInfo.at(cstyle).gs, styleInfo.at(cstyle).strength);
}
//------------------------------------------------------------
