#include "vbnum.h"
#include "numer_interp.h"


#define TCHAR char

char ckey[]="hd_fv4bv$Vd]]76sf[v)sfcd22";
char randstart[]="Afh9-lj[[[dsfm9i6jDgfbvBV1";

void Crypt(char *inp, unsigned int inplen, char* key, unsigned int keylen)
{
    TCHAR Sbox[257], Sbox2[257];
    unsigned long i, j, t, x;

    static const char OurUnSecuredKey[] = "VebestKeyDefault" ;
    static const unsigned int OurKeyLen = strlen(OurUnSecuredKey);
    TCHAR temp , k;
    i = j = k = t =  x = 0;
    temp = 0;

        for (int a=0;a<257;a++)
        {
            Sbox[a] = 0;
            Sbox2[a] = 0;
        }

    for(i = 0; i < 256U; i++)
    {
        Sbox[i] = (TCHAR)i;
    }

    j = 0;
    if(keylen)
    {
        for(i = 0; i < 256U ; i++)
        {
            if(j == keylen)
            {
                j = 0;
            }
            Sbox2[i] = key[j++];
        }
    }
    else
    {
        for(i = 0; i < 256U ; i++)
        {
            if(j == OurKeyLen)
            {
                j = 0;
            }
            Sbox2[i] = OurUnSecuredKey[j++];
        }
    }

    j = 0;

    for(i = 0; i < 256; i++)
    {
        j = (j + (unsigned long) Sbox[i] + (unsigned long) Sbox2[i]) % 256U ;
        temp =  Sbox[i];
        Sbox[i] = Sbox[j];
        Sbox[j] =  temp;
    }

    i = j = 0;
    for(x = 0; x < inplen; x++)
    {
        i = (i + 1U) % 256U;
        j = (j + (unsigned long) Sbox[i]) % 256U;
        temp = Sbox[i];
        Sbox[i] = Sbox[j] ;
        Sbox[j] = temp;
        t = ((unsigned long) Sbox[i] + (unsigned long) Sbox[j]) %  256U ;
        k = Sbox[t];
        inp[x] = (inp[x] ^ k);
    }
}

/*
bool passString( QString &cr )
{
    char buf[4096];
    int clen;

    strcpy( buf, cr.toAscii().data() );

    clen = strlen(buf);
    Crypt( buf, clen, randstart, strlen(randstart) );
    cr = "";
    for ( int a=0; a<clen; a++ )
    {
        cr += (buf[a] & 0x0f) + 'a';
        cr += ((buf[a] >> 4) & 0x0f) + 'A';
    }

    return true;
}
*/

bool getpString( QString &cr )
{
    char buf[4096];
    int clen;

    if ( cr.size() < 2 )
        return false;

    clen = cr.size()/2;
    for ( int a=0; a<clen; a++ )
    {
        buf[a] = (cr[a*2].toAscii() - 'a') + (cr[a*2+1].toAscii() - 'A')*16;
    }
    Crypt( buf, clen, randstart, strlen(randstart) );

    cr = "";

    for ( int a=0; a<clen; a++ )
        cr += buf[a];

    return true;
}


bool EncryptString( QString &cr )
{
    char buf[4096];
    int clen;

    if ( cr == "" )
    {
        cr = "JShd7w0-vcnb6943";
    }

    cr = cr + "A" + cr + "A";

#ifdef _MSC_VER
	strcpy_s(buf, 4000, cr.toAscii().data());
#else
	strcpy(buf, cr.toAscii().data());
#endif

    clen = strlen(buf);
    Crypt( buf, clen, ckey, strlen(ckey) );
    cr = "";
    for ( int a=0; a<clen; a++ )
    {
        cr += (buf[a] & 0x0f) + 'a';
        cr += ((buf[a] >> 4) & 0x0f) + 'A';
    }

    return true;
}


bool DecryptString( QString &cr )
{
    char buf[4096];
    int clen;

    if ( cr.size() < 8 )
        return false;

    clen = cr.size()/2;
    for ( int a=0; a<clen; a++ )
    {
        buf[a] = (cr[a*2].toAscii() - 'a') + (cr[a*2+1].toAscii() - 'A')*16;
    }
    Crypt( buf, clen, ckey, strlen(ckey) );

    cr = "";

    for ( int a=0; a<clen/2; a++ )
    {
        if ( buf[a] != buf[clen/2+a] )
            return false;
    }

    for ( int a=0; a<clen/2-1; a++ )
        cr += buf[a];

    if ( cr == "JShd7w0-vcnb6943" )
    {
        cr = "";
    }

    return true;
}



void CVBNum::loadDB()
{
    /*
    // Create DB
    {
        QString dbPath = QApplication::applicationDirPath()+QString("/");

        int lastvar=0, lastpos=0;

        QFile data(dbPath+"bapw.src");
        QFile odata(dbPath+"bapw.dat");
        odata.open(QFile::WriteOnly);
        if (data.open(QFile::ReadOnly))
        {
            QTextStream stream(&data);
            QTextStream ostream(&odata);
            QString line;
            do
            {
                line = stream.readLine();
                if (line.startsWith("#") && (line.size()>=6))
                {
                    line += QString::number(rand()) + "dsf4DS5jx" + QString::number(rand());
                    passString(line);
                    ostream << line << "\r\n";
                } else if (line.size()>0)
                {
                    passString(line);
                    ostream << line << "\r\n";
                }
            } while (!line.isNull());
        }
    }
    return;
    */

    for (int a=0; a<256; a++)
    {
        numerCelLP[a]="";
        numerCelBD[a]="";
        numerCelExpr[a]="";
        numerCelName[a]="";
        numerCelFName[a]="";
        numerCelBDT[a]=0;
        numerCelILP[a]=0;
        numerCelIDAY[a]=0;
        numerCelIEXPR[a]=0;
    }

#ifdef Q_OS_MAC
    QString dbPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/";//QApplication::applicationDirPath()+QString("/");
#else
    QString dbPath = QApplication::applicationDirPath()+QString("/");
#endif

    numerCustomText[0] = "Your custom text here";
    int lastvar=0, lastpos=0;

    QFile data(dbPath+"eng.dat");
    if (data.open(QFile::ReadOnly))
    {
        numerCompoundNumbers[0] = " ";
        for (int i = 10; i <= 99; i++)
            numerCompoundNumbers[i] = " ";

        QTextStream stream(&data);
        QString line;
        do
        {
            line = stream.readLine();
            getpString(line);
            if (line.startsWith("#") && (line.size()>=6))
            {
                lastvar = line.mid(1,3).toInt();
                lastpos = line.mid(4,2).toInt();
                if ((lastpos<0) || (lastpos>99))
                {
                    lastpos=0;
                    lastvar=0;
                }
            } else if (line.size()>0)
            {
                line = "<p>" + line + "</p>";

                if (lastvar==201)
                    numerPurposeful[lastpos] += line;
                else if (lastvar==202)
                    numerFamilyFormation[lastpos] += line;
                else if (lastvar==203)
                    numerStability[lastpos] += line;
                else if (lastvar==204)
                    numer1[lastpos] += line;
                else if (lastvar==205)
                    numer2[lastpos] += line;
                else if (lastvar==206)
                    numer3[lastpos] += line;
                else if (lastvar==207)
                    numer4[lastpos] += line;
                else if (lastvar==208)
                    numer5[lastpos] += line;
                else if (lastvar==209)
                    numer6[lastpos] += line;
                else if (lastvar==210)
                    numer7[lastpos] += line;
                else if (lastvar==211)
                    numer8[lastpos] += line;
                else if (lastvar==212)
                    numer9[lastpos] += line;
                else if (lastvar==213)
                    numerSelfAppraisal[lastpos] += line;
                else if (lastvar==214)
                    numerFinance[lastpos] += line;
                else if (lastvar==215)
                    numerTalent[lastpos] += line;
                else if (lastvar==216)
                    numerSpirituality[lastpos] += line;
                else if (lastvar==217)
                    numerSexualLife[lastpos] += line;
                else if (lastvar==101)
                    numerLifePath[lastpos] += line;
                else if (lastvar==102)
                    numerBirthDate[lastpos] += line;
                else if (lastvar==103)
                    numerKarmDebts[lastpos] += line;
                else if (lastvar==104)
                    numerExpr[lastpos] += line;
                else if (lastvar==105)
                    numerExprM[lastpos] += line;
                else if (lastvar==106)
                    numerPersYear[lastpos] += line;
                else if (lastvar==107)
                    numerPersDate[lastpos] += line;
                else if (lastvar==108)
                    numerHeartDesire[lastpos] += line;
                else if (lastvar==109)
                    numerPersonality[lastpos] += line;
                else if (lastvar==110)
                    numerHiddenPassion[lastpos] += line;
                else if (lastvar==111)
                    numerMinorHeartDesire[lastpos] += line;
                else if (lastvar==112)
                    numerKarmicLessons[lastpos] += line;
                else if (lastvar==113)
                    numerSubconsConfid[lastpos] += line;
                else if (lastvar==114)
                    numerBalance[lastpos] += line;
                else if (lastvar==115)
                    numerChallenges[lastpos] += line;
                else if (lastvar==116)
                    numerPinnacles[lastpos] += line;
                else if (lastvar==117)
                    numerMaturity[lastpos] += line;
                else if (lastvar==118)
                    numerRationalThoughts[lastpos] += line;
                else if (lastvar==119)
                    numerTransit[lastpos] += line;
                else if (lastvar==120)
                    numerEssence[lastpos] += line;
				else if (lastvar==121)
					numerDoubleDigitNumbers[lastpos] += line;
				else if (lastvar==122)
					numerCompoundNumbers[lastpos] += line;
				else if (lastvar==123)
					numerPlanesOfExpression[lastpos] += line;
				else if (lastvar==124)
					numerPlanesOfExpressionP[lastpos] += line;
				else if (lastvar==125)
					numerPlanesOfExpressionM[lastpos] += line;
				else if (lastvar==126)
					numerPlanesOfExpressionE[lastpos] += line;
				else if (lastvar==127)
                    numerPlanesOfExpressionI[lastpos] += line;
                else if (lastvar==128)
                    numerBridgeLPE[lastpos] += line;
                else if (lastvar==129)
                    numerBridgeLPEDet[lastpos] += line;
                else if (lastvar==130)
                    numerBridgeHDP[lastpos] += line;
                else if (lastvar==131)
                    numerBridgeHDPDet[lastpos] += line;
                else if (lastvar==301)
                    numerPetC[lastpos] += line;
                else if (lastvar==302)
                    numerPetDForecast[lastpos] += line;
                else if (lastvar==401)
                    numerSumComp[lastpos] += line;
                else if (lastvar==402)
                    numerDetComp[lastpos] += line;
                else if (lastvar==403)
                    numerNameComp[lastpos] += line;
                else if (lastvar==404)
                    numerLPComp[lastpos] += line;
				else if (lastvar==405)
					numerPMCharacterComp[lastpos] += line;
				else if (lastvar==406)
					numerPMEnergyComp[lastpos] += line;
                else if (lastvar==501)
                    numerNumInterp[lastpos] += line;
                else if (lastvar==602)
                    numerCelLP[lastpos] += line;
                else if (lastvar==612)
                    numerCelLP[lastpos+100] += line;
                else if (lastvar==603)
                    numerCelBD[lastpos] += line;
                else if (lastvar==613)
                    numerCelBD[lastpos+100] += line;
                else if (lastvar==604)
                    numerCelExpr[lastpos] += line;
                else if (lastvar==614)
                    numerCelExpr[lastpos+100] += line;
                else if ((lastvar==601) || (lastvar==611))
                {
                    int idx = lastpos;
                    if (lastvar==611)
                        idx += 100;

                    line = line.mid(3);
                    line.chop(4);
                    if (numerCelILP[idx]==0)
					{
                        numerCelILP[idx] = line.toInt();
						if (numerCelILP[idx]==0) numerCelILP[idx]=-1;
					}
                    else if (numerCelIDAY[idx]==0)
					{
                        numerCelIDAY[idx] = line.toInt();
						if (numerCelIDAY[idx]==0) numerCelIDAY[idx]=-1;
					}
					else if (numerCelIEXPR[idx]==0)
					{
                        numerCelIEXPR[idx] = line.toInt();
						if (numerCelIEXPR[idx]==0) numerCelIEXPR[idx]=-1;
					}
					else if (numerCelName[idx].size()==0)
                        numerCelName[idx] = line;
                    else if (numerCelFName[idx].size()==0)
                        numerCelFName[idx] = line;
                    else if (numerCelBDT[idx]==0)
                    {
                        QStringList dtl = line.split(".");
                        if (dtl.size()==3)
                            numerCelBDT[idx] = (dtl.at(0).toInt()) + (dtl.at(1).toInt()<<8) + (dtl.at(2).toInt()<<16);
                    }
                } else if ((lastvar==700) || (lastvar==701) || (lastvar==702) || (lastvar==703))
				{
					int subidx = (lastpos % 10);
					int cpos = subidx + (lastpos / 10)*4 + (lastvar-700)*10*4;
					if (subidx==0)
						if (numerHelpers[cpos].size()==0)
							line = "<h2>" + line + "</h2>";
					if (subidx==1)
						if (numerHelpers[cpos].size()==0)
							numerHelpers[cpos] = "<h2>Calculating</h2>";

					if (subidx==2)
						if (numerHelpers[cpos].size()==0)
							numerHelpers[cpos] = "<h2>Interpretation</h2>";

					numerHelpers[cpos] += line;
                } else if (lastvar==900)
                    numerInfo[lastpos] += line;
            }
        } while (!line.isNull());
    }
}



void CVBNum::OnSaveFile(bool closeProg)
{
    QString appName = Q_APP_TARGET;
    QString fileName = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
    if (fileName.endsWith(appName))
        fileName.chop(appName.size()+1);
    fileName += "/vbnum6.num";

    if (closeProg == false)
	{
		fileName = QFileDialog::getSaveFileName(this, "Save",
			QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation), "Files (*.num)");
	}

	if (!fileName.isEmpty())
	{
		QFile f(fileName);
		if( !f.open(QIODevice::WriteOnly) )
		{
            QMessageBox::critical(NULL, tr("Error"), tr("Can't create file!\r\n") + fileName, QMessageBox::Ok);
			return;
		}
		QTextStream stream(&f);
		stream.setCodec("UTF-8");
		QString s;

		if (closeProg)
		{
			for (int i = 0; i < m_pRecentEntries.size(); i++)
			{
				// stream << m_pBDate->date().toString("dd.MM.yyyy") << ",";
				stream << m_pRecentEntries[i].birthDate.toString("dd.MM.yyyy") << ",";

				//s = m_pFullNameEdit->text();
				s = m_pRecentEntries[i].fullName;
				s = s.remove(",");
				s = s.simplified();
				stream << s << ",";

				//s = m_pShortNameEdit->text();
				s = m_pRecentEntries[i].shortName;
				s = s.remove(",");
				s = s.simplified();
				stream << s << ",";

				//s = QString::number(m_pSex->currentIndex());
				s = QString::number(m_pRecentEntries[i].psex);
				s = s.remove(",");
				s = s.simplified();
				stream << s << ",";

				//stream << m_pPBDate->date().toString("dd.MM.yyyy") << ",";
				stream << m_pRecentEntries[i].partnerBirthDate.toString("dd.MM.yyyy") << ",";
				//s = m_pPFullNameEdit->text();
				s = m_pRecentEntries[i].partnerFullName;
				s = s.remove(",");
				s = s.simplified();
				stream << s << ",";

				//s = m_pPShortNameEdit->text();
				s = m_pRecentEntries[i].partnerShortName;
				s = s.remove(",");
				s = s.simplified();
				stream << s << ",";

				//stream << m_pPetBDate->date().toString("dd.MM.yyyy") << ",";
				stream << m_pRecentEntries[i].petBirthDate.toString("dd.MM.yyyy") << ",";

				//s = m_pPetName->text();
				s = m_pRecentEntries[i].petName;
				s = s.remove(",");
				s = s.simplified();
				stream << s << ",";

				stream << m_pBDateF->date().toString("dd.MM.yyyy") << ",";

				//            QString fulName;
				//            fulName.clear();
				//            for (unsigned int i = 0; i < m_viFnvw.size(); i++)
				//            {
				//                fulName += QString::number(m_viFnvw.at(i));
				//            }
				//            stream << fulName << ",";
				QString fulName;
				fulName.clear();
				for (signed int n = 0; n < m_pRecentEntries[i].fnvw.size(); n++)
				{
                    if (n==m_pRecentEntries[i].fullName.size())
                        break;
                    fulName += QString::number(m_pRecentEntries[i].fnvw.at(n));
				}
				stream << fulName << ",";


				//            QString shortName;
				//            shortName.clear();
				//            for (unsigned int i = 0; i < m_viSnvw.size(); i++)
				//            {
				//                shortName += QString::number(m_viSnvw.at(i));
				//            }
				//            stream << shortName << ",";
				QString shortName;
				shortName.clear();
				for (signed int n = 0; n < m_pRecentEntries[i].snvw.size(); n++)
				{
					shortName += QString::number(m_pRecentEntries[i].snvw.at(n));
				}
				stream << shortName << ",";

				//            QString pFulName;
				//            pFulName.clear();
				//            for (unsigned int i = 0; i < m_viPFnvw.size(); i++)
				//            {
				//                pFulName += QString::number(m_viPFnvw.at(i));
				//            }
				//            stream << pFulName << ",";
				QString pFulName;
				pFulName.clear();
				for (signed int n = 0; n < m_pRecentEntries[i].pfnvw.size(); n++)
				{
					pFulName += QString::number(m_pRecentEntries[i].pfnvw.at(n));
				}
				stream << pFulName << ",";

				//            QString pShortName;
				//            pShortName.clear();
				//            for (unsigned int i = 0; i < m_viPSnvw.size(); i++)
				//            {
				//                pShortName += QString::number(m_viPSnvw.at(i));
				//            }
				//            stream << pShortName;
				QString pShortName;
				pShortName.clear();
				for (signed int n = 0; n < m_pRecentEntries[i].psnvw.size(); n++)
				{
					pShortName += QString::number(m_pRecentEntries[i].psnvw.at(n));
				}
				stream << pShortName << ",";


				stream << QString::number(m_pRecentEntries[i].uCNC);

				if (m_pRecentEntries.size() > (i + 1))
				{
					stream << "\n";
				}
			}
		} else
		{
			stream << m_pBDate->date().toString("dd.MM.yyyy") << ",";

            QString fn = m_pFullNameEdit->text();
            s = fn;
			s = s.remove(",");
			s = s.simplified();
			stream << s << ",";

            QString sn = m_pShortNameEdit->text();
            s = sn;
			s = s.remove(",");
			s = s.simplified();
			stream << s << ",";

			s = QString::number(m_pSex->currentIndex());
			s = s.remove(",");
			s = s.simplified();
			stream << s << ",";

			stream << m_pPBDate->date().toString("dd.MM.yyyy") << ",";

            QString pfn = m_pPFullNameEdit->text();
            s = pfn;
			s = s.remove(",");
			s = s.simplified();
			stream << s << ",";

            QString psn = m_pPShortNameEdit->text();
            s = psn;
			s = s.remove(",");
			s = s.simplified();
			stream << s << ",";

			stream << m_pPetBDate->date().toString("dd.MM.yyyy") << ",";

			s = m_pPetName->text();
			s = s.remove(",");
			s = s.simplified();
			stream << s << ",";

			stream << m_pBDateF->date().toString("dd.MM.yyyy") << ",";

			QString fulName;
			fulName.clear();
			for (signed int i = 0; i < m_viFnvw.size(); i++)
			{
				fulName += QString::number(m_viFnvw.at(i));
                if (i>fn.size())
                    break;
			}
			stream << fulName << ",";


			QString shortName;
			shortName.clear();
			for (signed int i = 0; i < m_viSnvw.size(); i++)
			{
				shortName += QString::number(m_viSnvw.at(i));
                if (i>sn.size())
                    break;
			}
			stream << shortName << ",";

			QString pFulName;
			pFulName.clear();
			for (signed int i = 0; i < m_viPFnvw.size(); i++)
			{
				pFulName += QString::number(m_viPFnvw.at(i));
                if (i>pfn.size())
                    break;
			}
			stream << pFulName << ",";

			QString pShortName;
			pShortName.clear();
			for (signed int i = 0; i < m_viPSnvw.size(); i++)
			{
				pShortName += QString::number(m_viPSnvw.at(i));
                if (i>psn.size())
                    break;
			}
			stream << pShortName << ",";

			stream << QString::number(m_uCNC->isChecked());
		}
/*
        stream << m_pBDate->date().toString("dd.MM.yyyy") << ",";

        s = m_pFullNameEdit->text();
        s = s.remove(",");
        s = s.simplified();
        stream << s << ",";

        s = m_pShortNameEdit->text();
        s = s.remove(",");
        s = s.simplified();
        stream << s << ",";

        s = QString::number(m_pSex->currentIndex());
        s = s.remove(",");
        s = s.simplified();
        stream << s << ",";

        stream << m_pPBDate->date().toString("dd.MM.yyyy") << ",";

        s = m_pPFullNameEdit->text();
        s = s.remove(",");
        s = s.simplified();
        stream << s << ",";

        s = m_pPShortNameEdit->text();
        s = s.remove(",");
        s = s.simplified();
        stream << s << ",";

        stream << m_pPetBDate->date().toString("dd.MM.yyyy") << ",";

        s = m_pPetName->text();
        s = s.remove(",");
        s = s.simplified();
        stream << s << ",";

        stream << m_pBDateF->date().toString("dd.MM.yyyy") << ",";

        QString fulName;
        fulName.clear();
        for (unsigned int i = 0; i < m_viFnvw.size(); i++)
        {
            fulName += QString::number(m_viFnvw.at(i));
        }
        stream << fulName << ",";

        QString shortName;
        shortName.clear();
        for (unsigned int i = 0; i < m_viSnvw.size(); i++)
        {
            shortName += QString::number(m_viSnvw.at(i));
        }
        stream << shortName << ",";

        QString pFulName;
        pFulName.clear();
        for (unsigned int i = 0; i < m_viPFnvw.size(); i++)
        {
            pFulName += QString::number(m_viPFnvw.at(i));
        }
        stream << pFulName << ",";

        QString pShortName;
        pShortName.clear();
        for (unsigned int i = 0; i < m_viPSnvw.size(); i++)
        {
            pShortName += QString::number(m_viPSnvw.at(i));
        }
        stream << pShortName;
*/
    }
}


void CVBNum::OnOpenFile(bool openProg)
{
    QString appName = Q_APP_TARGET;
    QString fileName = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
    if (fileName.endsWith(appName))
        fileName.chop(appName.size()+1);
    fileName += "/vbnum6.num";

    if (openProg == false)
    {
        fileName = QFileDialog::getOpenFileName(this, "Open file",
        QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation), "Files (*.num)");
    }


    if (fileName.size()<2)
        return;
    QFile f(fileName);
    if (!f.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        if (openProg == false)
        QMessageBox::critical(NULL, tr("Error"), tr("Can't open file!"), QMessageBox::Ok);
        return;
    }
    if (openProg)
        OnResData();
    else
        OnResDataI();

	QTextStream stream(&f);
    stream.setCodec("UTF-8");
    QString src;
    src = stream.readAll();

    QStringList stringList = src.split("\n");

    if (stringList.size() == 0)
    {
        if (openProg == false)
        QMessageBox::critical(NULL, tr("Error"), tr("Wrong file format!"), QMessageBox::Ok);
        return;
    }

    src.clear();
    src = stringList.at(0);
    QStringList sl = src.split(",");

    if (sl.size() == 10)
    {
        //OnClearRecent();
        QDate d;
        d = d.fromString(sl.at(0), "dd.MM.yyyy");
        m_pBDate->setDate(d);

        m_pFullNameEdit->setText(sl.at(1));
        m_pShortNameEdit->setText(sl.at(2));

        int i = sl.at(3).toInt();
        if ((i<0) || (i>1))
            i = 0;
        m_pSex->setCurrentIndex(i);

        d = d.fromString(sl.at(4), "dd.MM.yyyy");
        m_pPBDate->setDate(d);

        m_pPFullNameEdit->setText(sl.at(5));
        m_pPShortNameEdit->setText(sl.at(6));

        d = d.fromString(sl.at(7), "dd.MM.yyyy");
        m_pPetBDate->setDate(d);

        m_pPetName->setText(sl.at(8));

        d = d.fromString(sl.at(9), "dd.MM.yyyy");
        //m_pBDateF->setDate(d);
		m_pBDateF->setDate(QDate::currentDate());
    }
    else if (sl.size() == 15)
    {
		if (openProg)
		{
			OnClearRecent();
            sl.clear();
			for (int pStr = 0; pStr < stringList.size(); pStr++)
			{
				src = stringList.at(pStr);
				sl = src.split(",");
                if (sl.size()==15)
                {
                    SUserInfo tmpStruct;
                    tmpStruct.birthDate = QDate::fromString(sl.at(0), "dd.MM.yyyy");
                    tmpStruct.fullName = sl.at(1);
                    tmpStruct.shortName = sl.at(2);
                    tmpStruct.psex = sl.at(3).toInt();
                    tmpStruct.partnerBirthDate = QDate::fromString(sl.at(4), "dd.MM.yyyy");
                    tmpStruct.partnerFullName = sl.at(5);
                    tmpStruct.partnerShortName = sl.at(6);
                    tmpStruct.petBirthDate = QDate::fromString(sl.at(7), "dd.MM.yyyy");
                    tmpStruct.petName = sl.at(8);
                    //m_pBDateF->setDate(QDate::fromString(sl.at(9), "dd.MM.yyyy"));

                    if (sl.at(10).size()<=tmpStruct.fullName.size()*2)
                        for (signed int i = 0; i < sl.at(10).size(); i++)
                            tmpStruct.fnvw.append(QString((sl.at(10)).at(i)).toInt());
                    else
                        tmpStruct.fnvw.clear();

                    if (sl.at(11).size()<=tmpStruct.shortName.size()*2)
                        for (signed int i = 0; i < sl.at(11).size(); i++)
                            tmpStruct.snvw.append(QString((sl.at(11)).at(i)).toInt());
                    else
                        tmpStruct.snvw.clear();

                    if (sl.at(12).size()<=tmpStruct.partnerFullName.size()*2)
                        for (signed int i = 0; i < sl.at(12).size(); i++)
                            tmpStruct.pfnvw.append(QString((sl.at(12)).at(i)).toInt());
                    else
                        tmpStruct.pfnvw.clear();

                    if (sl.at(13).size()<=tmpStruct.partnerShortName.size()*2)
                        for (signed int i = 0; i < sl.at(13).size(); i++)
                            tmpStruct.psnvw.append(QString((sl.at(13)).at(i)).toInt());
                    else
                        tmpStruct.psnvw.clear();

                    tmpStruct.uCNC = sl.at(14).toInt();
					tmpStruct.PMode = NUM_VOW_MODE_ENGLISH;
                    m_pRecentEntries.append(tmpStruct);
                }
			}
			m_pBDateF->setDate(QDate::currentDate());
			updateRecentList();
			RecentClicked = true;
			OnRecentClicked();
			RecentClicked = false;
		} else
		{
			int pStr=0;
			SUserInfo tmpStruct;
			src = stringList.at(pStr);
			sl = src.split(",");

            if (sl.size()==15)
            {
                tmpStruct.birthDate = QDate::fromString(sl.at(0), "dd.MM.yyyy");
                tmpStruct.fullName = sl.at(1);
                tmpStruct.shortName = sl.at(2);
                tmpStruct.psex = sl.at(3).toInt();
                tmpStruct.partnerBirthDate = QDate::fromString(sl.at(4), "dd.MM.yyyy");
                tmpStruct.partnerFullName = sl.at(5);
                tmpStruct.partnerShortName = sl.at(6);
                tmpStruct.petBirthDate = QDate::fromString(sl.at(7), "dd.MM.yyyy");
                tmpStruct.petName = sl.at(8);
                //m_pBDateF->setDate(QDate::fromString(sl.at(9), "dd.MM.yyyy"));

                if (sl.at(10).size()<=tmpStruct.fullName.size()*2)
                    for (signed int i = 0; i < sl.at(10).size(); i++)
                        tmpStruct.fnvw.append(QString((sl.at(10)).at(i)).toInt());
                else
                    tmpStruct.fnvw.clear();

                if (sl.at(11).size()<=tmpStruct.shortName.size()*2)
                    for (signed int i = 0; i < sl.at(11).size(); i++)
                        tmpStruct.snvw.append(QString((sl.at(11)).at(i)).toInt());
                else
                    tmpStruct.snvw.clear();

                if (sl.at(12).size()<=tmpStruct.partnerFullName.size()*2)
                    for (signed int i = 0; i < sl.at(12).size(); i++)
                        tmpStruct.pfnvw.append(QString((sl.at(12)).at(i)).toInt());
                else
                    tmpStruct.pfnvw.clear();

                if (sl.at(13).size()<=tmpStruct.partnerShortName.size()*2)
                    for (signed int i = 0; i < sl.at(13).size(); i++)
                        tmpStruct.psnvw.append(QString((sl.at(13)).at(i)).toInt());
                else
                    tmpStruct.psnvw.clear();

                tmpStruct.uCNC = sl.at(14).toInt();
				tmpStruct.PMode = NUM_VOW_MODE_ENGLISH;

                m_pBDateF->setDate(QDate::currentDate());
                m_pBDate->setDate(tmpStruct.birthDate);
                m_pFullNameEdit->setText(tmpStruct.fullName);
                m_pShortNameEdit->setText(tmpStruct.shortName);

                if (tmpStruct.psex)
                    m_pSex->setCurrentIndex(1);
                else
                    m_pSex->setCurrentIndex(0);

                m_pPBDate->setDate(tmpStruct.partnerBirthDate);

                m_pPFullNameEdit->setText(tmpStruct.partnerFullName);
                m_pPShortNameEdit->setText(tmpStruct.partnerShortName);

                m_viFnvw = tmpStruct.fnvw;
                m_viSnvw = tmpStruct.snvw;
                m_viPFnvw = tmpStruct.pfnvw;
                m_viPSnvw = tmpStruct.psnvw;

                m_uCNC->setChecked(tmpStruct.uCNC);
                OnPreCalcNum();
                OnPreCalcNumP();
            }
		}
    }
    else
    {
        if (openProg == false)
			QMessageBox::critical(NULL, tr("Error"), tr("Wrong file format!"), QMessageBox::Ok);
        return;
    }
/*
    QStringList sl = src.split(",");
//    if (sl.size() != 14)
//    {
//        QMessageBox::critical(NULL, tr("Error"), tr("Wrong file format!"), QMessageBox::Ok);
//        return;
//    }
//    else
//    if (sl.size() != 10)
//    {
//        QMessageBox::critical(NULL, tr("Error"), tr("Wrong file format!"), QMessageBox::Ok);
//        return;
//    }

    if (sl.size() == 14 || sl.size() == 10)
    {

    }
    else
    {
        QMessageBox::critical(NULL, tr("Error"), tr("Wrong file format!"), QMessageBox::Ok);
        return;
    }

    QDate d;
    d = d.fromString(sl.at(0), "dd.MM.yyyy");
    m_pBDate->setDate(d);

    m_pFullNameEdit->setText(sl.at(1));
    m_pShortNameEdit->setText(sl.at(2));

    int i = sl.at(3).toInt();
    if ((i<0) || (i>1))
        i = 0;
    m_pSex->setCurrentIndex(i);

    d = d.fromString(sl.at(4), "dd.MM.yyyy");
    m_pPBDate->setDate(d);

    m_pPFullNameEdit->setText(sl.at(5));
    m_pPShortNameEdit->setText(sl.at(6));

    d = d.fromString(sl.at(7), "dd.MM.yyyy");
    m_pPetBDate->setDate(d);

    m_pPetName->setText(sl.at(8));

    d = d.fromString(sl.at(9), "dd.MM.yyyy");
    m_pBDateF->setDate(d);


    if (sl.size() == 14)
    {
        m_viFnvw_l.clear();
        m_viSnvw_l.clear();
        m_viPFnvw_l.clear();
        m_viPSnvw_l.clear();

        for (int i = 0; i < (sl.at(10)).size(); i++)
        {
            m_viFnvw_l.append(QString((sl.at(10)).at(i)).toInt());
        }

        for (int i = 0; i < (sl.at(11)).size(); i++)
        {
            m_viSnvw_l.append(QString((sl.at(11)).at(i)).toInt());
        }

        for (int i = 0; i < (sl.at(12)).size(); i++)
        {
            m_viPFnvw_l.append(QString((sl.at(12)).at(i)).toInt());
        }

        for (int i = 0; i < (sl.at(13)).size(); i++)
        {
            m_viPSnvw_l.append(QString((sl.at(13)).at(i)).toInt());
        }

        QString tmp;
        OnTextChanged(tmp);
        OnPTextChanged(tmp);
    }
*/
}


bool CVBNum::resLoadImage(QString name, QImage &img)
{
    QImage image;

    bool res = image.load(m_folder+name);
    img = image;
    return res;
}


bool CVBNum::resLoadIcon(QString name, QIcon &icon)
{
    QIcon ic(m_folder+name);

    icon = ic;
    return true;
}


void CVBNum::loadRes(QString folder)
{
    m_folder = folder;

    resLoadIcon(QString("savebuth.png"), m_Res.fileSaveAs);
    resLoadIcon(QString("infobuth.png"), m_Res.helpAbout);
    resLoadIcon(QString("infobuth.png"), m_Res.helpHelp);
    resLoadIcon(QString("printbuth.png"), m_Res.filePrint);
    resLoadIcon(QString("savebuth.png"), m_Res.fileExportPDF);
	resLoadIcon(QString("menubuth.png"), m_Res.fileSettings);

    resLoadImage(QString("tbar.png"), m_Res.titleBar);
    resLoadImage(QString("tbar2.png"), m_Res.titleBar2);
    //resLoadImage(QString("psq_title.png"), m_Res.titleBar3);
    resLoadImage(QString("todaywidg.png"), m_Res.todayWidg);

	resLoadImage(QString("logo.png"), m_Res.logo);
    resLoadImage(QString("logoh.png"), m_Res.logoh);

    resLoadImage(QString("input-field.png"), m_Res.inpRes);
    resLoadImage(QString("input-bot.png"), m_Res.botRes);
    resLoadImage(QString("input-top.png"), m_Res.topRes);
    resLoadImage(QString("input-top-opt.png"), m_Res.topResOpt);

    resLoadImage(QString("input2-field.png"), m_Res.inpRes2);
    resLoadImage(QString("input2-bot.png"), m_Res.botRes2);
    resLoadImage(QString("input2-top-opt.png"), m_Res.topResOpt2);
}
