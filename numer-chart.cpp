#include "numer-chart.h"


CChartNumerology::CChartNumerology(QObject *parent)
: QObject(parent)
{

}


CChartNumerology::~CChartNumerology()
{
}


void CChartNumerology::resetData()
{
	clear();
}


void CChartNumerology::clear()
{
	for (int a=0; a<64; a++)
		m_BaseNums[a]=0;

	QPainter pntr(&numChart);
    //pntr.drawImage(0,0, bkg);
    pntr.drawImage(0,0, texture);
}


bool CChartNumerology::isBase(int num)
{
	if ( ((num>=0) && (num<=9)) || (num==11) || (num==22) || (num==33))
		return true;
	else
		return false;
}


bool CChartNumerology::isCarmic(int num)
{
	if ((num==13) || (num==14) || (num==16) || (num==19))
		return true;
	else
		return false;
}


int CChartNumerology::downOnce(int num)
{
	if (isBase(num))
		return num;

	int res = num % 10 + ((num/10) % 10) + ((num/100) % 10) + ((num/1000) % 10);
	return res;
}


int CChartNumerology::downToBase(int num)
{
	int rnum = num;

	if (isCarmic(rnum) || isBase(rnum))
		m_BaseNums[rnum]++;

	while(!isBase(rnum))
	{
		rnum = downOnce(rnum);
		if (isCarmic(rnum) || isBase(rnum))
			m_BaseNums[rnum]++;
	}
	return rnum;
}


int CChartNumerology::statDownToBase(int num)
{
	int rnum = num;

	while(!isBase(rnum))
	{
		rnum = downOnce(rnum);
		if (rnum<=0)
			return 0;
	}
	return rnum;
}


int CChartNumerology::downToDigit(int num)
{
	int rnum = num;

	while(!isBase(rnum))
	{
		rnum = downOnce(rnum);
		if (rnum<=0)
			return 0;
	}

	if (rnum>9)
		rnum = rnum % 10 + ((rnum/10) % 10);

	return rnum;
}


void CChartNumerology::preloadRes(QImage texture_)
{
    texture = texture_;
    numChart = QImage(texture_.width(), texture_.height(), QImage::Format_ARGB32);
    numChart.fill(0xffffffff);
    QPainter pntr(&numChart);
    pntr.drawImage(0,0, texture);
}
