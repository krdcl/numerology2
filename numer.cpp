#include "numer.h"

//#10100
QString numerLifePath[128];
//#10200
QString numerBirthDate[128];
//#10300
QString numerKarmDebts[128];
//#10400
QString numerExpr[128];
//#10500
QString numerExprM[128];
//#10600
QString numerPersYear[128];
//#10700
QString numerPersDate[128];
//#10800
QString numerHeartDesire[128];
//#10900
QString numerPersonality[128];
//#11000
QString numerHiddenPassion[128];
//#11100
QString numerMinorHeartDesire[128];
//#11200
QString numerKarmicLessons[128];
//#11300
QString numerSubconsConfid[128];
//#11400
QString numerBalance[128];
//#11500
QString numerChallenges[128];
//#11600
QString numerPinnacles[128];
//#11700
QString numerMaturity[128];
//#11800
QString numerRationalThoughts[128];
//#11900
QString numerTransit[128];
//#12000
QString numerEssence[128];
//#12100
QString numerDoubleDigitNumbers[128];

//#12300
QString numerPlanesOfExpression[128];
//#12400
QString numerPlanesOfExpressionP[128];
//#12500
QString numerPlanesOfExpressionM[128];
//#12600
QString numerPlanesOfExpressionE[128];
//#12700
QString numerPlanesOfExpressionI[128];


//#12800
QString numerBridgeLPE[128];
//#12900
QString numerBridgeLPEDet[128];
//#13000
QString numerBridgeHDP[128];
//#13100
QString numerBridgeHDPDet[128];

// ccode 501XX
QString numerNumInterp[128];


// code 602XX
QString numerCelLP[256];
// code 603XX
QString numerCelBD[256];
// code 604XX
QString numerCelExpr[256];


// code 601XX
QString numerCelName[256];
QString numerCelFName[256];
int numerCelBDT[256];
int numerCelILP[256];
int numerCelIDAY[256];
int numerCelIEXPR[256];

// code 70XXX
QString numerHelpers[256];

QString numerPetC[128];
QString numerPetDForecast[128];


// code 900XX
QString numerInfo[16];


// code -----
QString numerCustomText[256];


#ifdef Q_WS_WIN
const QString qsWKStyle = "::-webkit-scrollbar { width: 17px; height: 17px; }\r\n\
		::-webkit-scrollbar:vertical { background: -webkit-gradient(linear, left top, right top, color-stop(0%, #EEEEEE), color-stop(50%, #FFFFFF), color-stop(100%, #F2F2F2)); border: 1px solid #DDDDDD; } \r\n\
		::-webkit-scrollbar-thumb:vertical { height: 50px; background: -webkit-gradient(linear, left top, right top, color-stop(0%, #EEEEEE), color-stop(20%, #EEEEEE), color-stop(21%, #AAAAAA), color-stop(79%, #AAAAAA), color-stop(80%, #F2F2F2), color-stop(100%, #F2F2F2)); border-radius: 12px; }\r\n\
		::-webkit-scrollbar-thumb:vertical:hover { height: 50px; background: -webkit-gradient(linear, left top, right top, color-stop(0%, #EEEEEE), color-stop(20%, #EEEEEE), color-stop(21%, #A0A0A0), color-stop(79%, #A0A0A0), color-stop(80%, #F2F2F2), color-stop(100%, #F2F2F2)); border-radius: 12px; }\r\n";
#else
const QString qsWKStyle = "";
#endif

CNumerology::CNumerology(QObject *parent)
: CReportTools(parent)
{
	m_CalcModeID = CM_WESTERN;
    m_MasterNumberModeID = MNM_11TO33;
	resetData();
}


CNumerology::~CNumerology()
{
}


void CNumerology::resetData()
{
	clear();
	m_LifePath = -1;
	//m_TextDoc.clear();
	htmlDoc = "";
    for (int a=0; a<128; a++)
	{
		m_PELet[a]=0;
		m_Let[a]=0;
	}
	for (int a=0; a<20; a++)
	{
		numDet.PE[a] = "";
		numDet.PENum[a] = "";
		numDet.PECnt[a] = "";
		numDet.PERep[a] = "";
	}

    m_sTransit1="";
    m_sTransit2="";
    m_sTransit3="";
    for (int a=0; a<128; a++)
    {
        m_EssenceS[a]=0;
        m_Essence[a]=0;
    }
}


void CNumerology::clear()
{
    for (int a=0; a<128; a++)
		m_BaseNums[a]=0;
}


bool CNumerology::isMasterNumber(int num, int extendedTO99)
{
    if (extendedTO99==MNM_11TO99)
    {
        if ( (num==11) || (num==22) || (num==33) || (num==44) || (num==55) || (num==66)  || (num==77)  || (num==88)  || (num==99))
            return true;
        else
            return false;
    } else
    {
        if ( (num==11) || (num==22) || (num==33))
            return true;
        else
            return false;
    }
}


bool CNumerology::isBase(int num)
{
    if (m_MasterNumberModeID==MNM_11TO99) {
        if ( ((num>=0) && (num<=9)) || (num==11) || (num==22) || (num==33) || (num==44) || (num==55) || (num==66)  || (num==77)  || (num==88)  || (num==99))
            return true;
        else
            return false;
    } else {
        if ( ((num>=0) && (num<=9)) || (num==11) || (num==22) || (num==33))
            return true;
        else
            return false;
    }
}


bool CNumerology::isCarmic(int num)
{
	if ((num==13) || (num==14) || (num==16) || (num==19))
		return true;
	else
		return false;
}


int CNumerology::downOnce(int num)
{
	if (isBase(num))
		return num;

	int res = num % 10 + ((num/10) % 10) + ((num/100) % 10) + ((num/1000) % 10);
	return res;
}


int CNumerology::downToBase(int num)
{
    if ((num>9999) || (num<=0))
        return 0;

	int rnum = num;

    if ((isCarmic(rnum) || isBase(rnum)) && (rnum<128))
		m_BaseNums[rnum]++;

	while(!isBase(rnum))
	{
		rnum = downOnce(rnum);
        if ((isCarmic(rnum) || isBase(rnum)) && (rnum<128))
			m_BaseNums[rnum]++;
	}
	return rnum;
}


int CNumerology::downToBase(int num, int customBaseNums[])
{
    if ((num>9999) || (num<=0))
        return 0;

    int rnum = num;

    if ((isCarmic(rnum) || isBase(rnum)) && (rnum<100))
        customBaseNums[rnum]++;

    while(!isBase(rnum))
    {
        rnum = downOnce(rnum);
        if ((isCarmic(rnum) || isBase(rnum)) && (rnum<100))
            customBaseNums[rnum]++;
    }
    return rnum;
}


int CNumerology::statDownToBase(int num)
{
	int rnum = num;

	while(!isBase(rnum))
	{
		rnum = downOnce(rnum);
		if (rnum<=0)
			return 0;
	}
	return rnum;
}


int CNumerology::downToDigit(int num)
{
	int rnum = num;

    while(rnum>9)
	{
        rnum = rnum % 10 + ((rnum/10) % 10) + ((rnum/100) % 10) + ((rnum/1000) % 10);
		if (rnum<=0)
			return 0;
	}

	return rnum;
}


int CNumerology::letterToNumber(QChar ch)
{
	if (m_CalcModeID==CM_CHALDEAN)
		return letterToNumberChaldean(ch);

	if ((ch.toAscii()>='0') && (ch.toAscii()<='9'))
		return ch.toAscii()-'0';

	int r = ch.toAscii()-'A'+1;

	if ((r>=1) && (r<=26))
    {
        if (m_GlobSettings.calcNameDD)
            return downToDigit(r);
        else
        {
            if (isMasterNumber(r, m_MasterNumberModeID))
                return r;
            else
                return downToDigit(r);
        }
    } else
		return 0;
}


int CNumerology::letterToNumberChaldean(QChar ch)
{
	if ((ch.toAscii()>='0') && (ch.toAscii()<='9'))
		return ch.toAscii()-'0';

	switch (ch.toAscii())
	{
	case 'A':
	case 'I':
	case 'J':
	case 'Q':
	case 'Y':
		return 1;
	case 'B':
	case 'K':
	case 'R':
		return 2;
	case 'C':
	case 'G':
	case 'L':
	case 'S':
		return 3;
	case 'D':
	case 'M':
	case 'T':
		return 4;
	case 'E':
	case 'H':
	case 'N':
	case 'X':
		return 5;
	case 'U':
	case 'V':
	case 'W':
		return 6;
	case 'Z':
	case 'O':
		return 7;
	case 'F':
	case 'P':
		return 8;
	default:
		return 0;
	}
}



int CNumerology::wordToNumber(QString str)
{
	int r = 0;
	for (int a=0; a<str.size(); a++)
		r += letterToNumber(str[a]);

	//r = statDownToBase(r);

	return r;
}



void CNumerology::setCalcMode(int mode)
{
	m_CalcModeID = mode;
}

void CNumerology::setMasterNumbersMode(int mode)
{
    m_MasterNumberModeID = mode;
}

void CNumerology::calcNamesNumbers(QString str, QList<int> &res)
{
	QStringList nlist = str.split(" ");

	for (int a=0; a<nlist.size(); a++)
		res.push_back(wordToNumber(nlist[a]));
}


int CNumerology::isVow(QString str, int pos, int mode)
{
	QChar sym = str.at(pos);
	sym = sym.toUpper();
	char s = sym.toAscii();

    if (mode==NUM_VOW_MODE_ENGLISH)
    {
        if (s == 'B'
            || s == 'C'
            || s == 'D'
            || s == 'F'
            || s == 'G'
            || s == 'H'
            || s == 'J'
            || s == 'K'
            || s == 'L'
            || s == 'M'
            || s == 'N'
            || s == 'P'
            || s == 'Q'
            || s == 'R'
            || s == 'S'
            || s == 'T'
            || s == 'V'
            || s == 'X'
            || s == 'Z')
            return -1;

        if (s == 'A'
            || s == 'E'
            || s == 'I'
            || s == 'O'
            || s == 'U')
            return 1;

        if (s == 'W')
        {
            if (pos == 0)
                return -1;

            QChar symn = str.at(pos-1).toUpper();
            char n = symn.toAscii();
            if ((n<'A') || (n>'Z'))
                return -1;

            if (pos == str.size()-1)
                return 1;

            symn = str.at(pos+1).toUpper();
            n = symn.toAscii();
            if ((n<'A') || (n>'Z'))
                return 1;

            symn = str.at(pos+1).toUpper();
            n = symn.toAscii();
            if (n == 'A'
                || n == 'E'
                || n == 'I'
                || n == 'O'
                || n == 'U'
                || n == 'Y'
                || n == 'H')
                return -1;

            return 1;
        }
        if (s == 'Y')
        {
            if (pos == str.size()-1)
            {
                if (pos>0)
                {
                    QChar symn = str.at(pos-1).toUpper();
                    char n = symn.toAscii();
                    if (n == 'A'
                        || n == 'E'
                        || n == 'I'
                        || n == 'O'
                        || n == 'U')
                        return -1;
                }
                return 1;
            }

            QChar symns = str.at(pos+1).toUpper();
            char ns = symns.toAscii();
            if ((ns<'A') || (ns>'Z'))
            {
                if (pos>0)
                {
                    QChar symn = str.at(pos-1).toUpper();
                    char n = symn.toAscii();
                    if (n == 'A'
                        || n == 'E'
                        || n == 'I'
                        || n == 'O'
                        || n == 'U')
                        return -1;
                }
                return 1;
            }

            if (pos == 0)
            {
                QChar symn = str.at(pos+1).toUpper();
                char n = symn.toAscii();
                if (n == 'A'
                    || n == 'E'
                    || n == 'I'
                    || n == 'O'
                    || n == 'U')
                    return -1;
                return 1;
            }

            QChar symps = str.at(pos-1).toUpper();
            char ps = symps.toAscii();

            if ((ps<'A') || (ps>'Z'))
            {
                QChar symn = str.at(pos+1).toUpper();
                char n = symn.toAscii();
                if (n == 'A'
                    || n == 'E'
                    || n == 'I'
                    || n == 'O'
                    || n == 'U')
                    return -1;
                return 1;
            }

            char n = ps;
            if (n == 'A'
                || n == 'E'
                || n == 'I'
                || n == 'O'
                || n == 'U'
                || n == 'Y')
                return -1;

            return 1;
        }

        return 0;
    } else if (mode==NUM_VOW_MODE_LATIN)
    {
        if (s == 'B'
            || s == 'C'
            || s == 'D'
            || s == 'F'
            || s == 'G'
            || s == 'H'
            || s == 'J'
            || s == 'K'
            || s == 'L'
            || s == 'M'
            || s == 'N'
            || s == 'P'
            || s == 'Q'
            || s == 'R'
            || s == 'S'
            || s == 'T'
            || s == 'V'
            || s == 'X'
            || s == 'Z'
            || s == 'W')
            return -1;

        if (s == 'A'
            || s == 'E'
            || s == 'I'
            || s == 'O'
            || s == 'U'
            || s == 'Y')
            return 1;

        return 0;
    }

    return 0;
}


int CNumerology::getPEIndex(QChar ch)
{
	ch = ch.toUpper();
	char s = ch.toAscii();

	if (s == 'A')
		return 0;
	else if (s == 'E')
		return 1;
	else if (s == 'O')
		return 2;
	else if (s == 'R')
		return 2;
	else if (s == 'I')
		return 2;
	else if (s == 'Z')
		return 2;
	else if (s == 'K')
		return 3;
	else if (s == 'H')
		return 4;
	else if (s == 'J')
		return 4;
	else if (s == 'N')
		return 4;
	else if (s == 'P')
		return 4;
	else if (s == 'W')
		return 5;
	else if (s == 'B')
		return 6;
	else if (s == 'S')
		return 6;
	else if (s == 'T')
		return 6;
	else if (s == 'X')
		return 6;
	else if (s == 'F')
		return 7;
	else if (s == 'Q')
		return 7;
	else if (s == 'U')
		return 7;
	else if (s == 'Y')
		return 7;
	else if (s == 'G')
		return 8;
	else if (s == 'L')
		return 8;
	else if (s == 'D')
		return 9;
	else if (s == 'M')
		return 9;
	else if (s == 'C')
		return 11;
	else if (s == 'V')
		return 11;

	return -1;
}


void CNumerology::calcNamesNumbersP(QString str, QList<int> &res, bool Vow, QVector<int> &vw, int pmode)
{
	QStringList nlist = str.split(" ");

    int k=0;
	for (int a=0; a<nlist.size(); a++)
	{
		QString sres;
		for (int n=0; n<nlist[a].size(); n++)
		{
			if (Vow)
			{
                int v = isVow(nlist[a],n, pmode);
                if (k<vw.size())
                {
                    if (vw.at(k)==1)
                        v=1;
                    else if (vw.at(k)==2)
                        v=-1;
                }
                if (v>0)
					sres += nlist[a].at(n);
			} else
			{
                int v = isVow(nlist[a],n, pmode);
                if (k<vw.size())
                {
                    if (vw.at(k)==1)
                        v=1;
                    else if (vw.at(k)==2)
                        v=-1;
                }
                if (v<0)
					sres += nlist[a].at(n);
			}
            k++;
		}
		res.push_back(wordToNumber(sres));
        k++;
	}
}

void CNumerology::calcNamesNumbersP(QString str, QList<int> &res, bool Vow, int pmode)
{
    QVector<int> tmpv;
    calcNamesNumbersP(str, res, Vow, tmpv, pmode);
}


bool CNumerology::calculatePet(QString fullName, QString, QDate bdate, QDate cdate, bool, RepSettings *repList, int curRep)
{
    int select = curRep - 1;

    if (select >= 0)
    {
        if (repList->size() > 0)
        {
            if (repList->size() <= select)
                select = -1;
        }
        else
        {
            select = -1;
        }
    }

    bool showImages=true;
    if (select>=0) {
        if (repList->at(select).extendedSettings.value("RTPet-hide-images", "FALSE")=="TRUE")
           showImages=false;
    }


	fullName = fullName.toUpper();
	resetData();

    const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };


	int bnum = downToDigit(downToDigit(bdate.day()) + downToDigit(bdate.month()) + downToDigit(bdate.year()));
	int cnum = downToDigit(downToDigit(cdate.day()) + downToDigit(cdate.month()) + downToDigit(cdate.year()));


	numDet.birthDay = QString::number(bnum);


	int m_expression;
	clear();
	QList<int> nnums;
	calcNamesNumbers(fullName, nnums);
	//int fnnum = nnums[0];
	int sum=0;
	for(int a=0; a<nnums.size(); a++)
	{
		sum += downToBase(nnums[a]);
	}
	m_expression = downToDigit(sum);

	numDet.expression = QString::number(m_expression);
	if (sum != m_expression)
		numDet.expression += "/" + QString::number(sum);


	numDet.fnameR = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR += "+";

		int v = nnums[a];
		int vb = downToBase(nnums[a]);
		numDet.fnameR += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR += '/' + QString::number(v);
	}
	numDet.fnameR += "=" + QString::number(sum);
	if (sum != m_expression)
		numDet.fnameR += "=" + QString::number(m_expression);


	nnums.clear();
	calcNamesNumbersP(fullName, nnums, true, NUM_VOW_MODE_ENGLISH);
	sum=0;
	for(int a=0; a<nnums.size(); a++)
		sum += downToBase(nnums[a]);
	int m_hearDesire = downToDigit(sum);

	numDet.fnameR1 = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR1 += "+";

		int v = nnums[a];
		int vb = downToBase(nnums[a]);
		numDet.fnameR1 += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR1 += '/' + QString::number(v);
	}
	numDet.fnameR1 += "=" + QString::number(sum);
	if (sum != m_hearDesire)
		numDet.fnameR1 += "=" + QString::number(m_hearDesire);

	numDet.heartDesire = QString::number(m_hearDesire);
	if (sum != m_hearDesire)
		numDet.heartDesire += "/" + QString::number(sum);



	nnums.clear();
	calcNamesNumbersP(fullName, nnums, false, NUM_VOW_MODE_ENGLISH);
	sum=0;
	for(int a=0; a<nnums.size(); a++)
		sum += downToBase(nnums[a]);
	int m_personality = downToDigit(sum);

	numDet.fnameR2 = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR2 += "+";

		int v = nnums[a];
		int vb = downToBase(nnums[a]);
		numDet.fnameR2 += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR2 += '/' + QString::number(v);
	}
	numDet.fnameR2 += "=" + QString::number(sum);
	if (sum != m_personality)
		numDet.fnameR2 += "=" + QString::number(m_personality);

	numDet.personality = QString::number(m_personality);
	if (sum != m_personality)
		numDet.personality += "/" + QString::number(sum);


	numDet.fnameN1 = "";
	numDet.fnameN2 = "";
	for (int a=0; a<fullName.size(); a++)
	{
		int v = isVow(fullName, a, NUM_VOW_MODE_ENGLISH);
		if (v>0)
		{
            numDet.fnameN1 += QString::number(downToDigit(letterToNumber(fullName[a])));
			numDet.fnameN2 += " ";
		} else if (v<0)
		{
            numDet.fnameN2 += QString::number(downToDigit(letterToNumber(fullName[a])));
			numDet.fnameN1 += " ";
		} else
		{
			numDet.fnameN1 += " ";
			numDet.fnameN2 += " ";
		}
	}

	if (m_expression==0)
	{
		QString Msg = tr("Error! Incorrect pet name!");
		QMessageBox::StandardButton Res;
		Res = QMessageBox::critical(NULL, tr(""), Msg, QMessageBox::Ok);

		return false;
	}

	int fdcode = downToDigit(m_expression+bnum+cnum);
	numDet.persYear = QString::number(fdcode);


	QString str;

	// Summary
    str = "Pet number " + numDet.expression;
	str+="<br />\r\n";

	str += "Pet Birthday Code " + numDet.birthDay;
	str +="<br />\r\n";

	str += "Forecast Day Code " + numDet.persYear;
	str +="<br />\r\n";

    startDoc("Pet Numerology Report");

    if (select >= 0)
    {
        const QList<repItem> *Pet = &repList->at(select).Pet;

        for (int i = 0; i < Pet->size(); i++)
        {
            const repItem *item = &Pet->at(i);
            if (item->id == PTHeader)
            {
                if (item->isContentDefault)
                {
                    startChap();
                    addHead("Pet Numerology Chart");
                    endChap();

                    startChap();
                    addPar(str);
                    addPar("");
                    endChap();
                }
                else
                {
                    startChap();
                    addHead(item->headerName, item->headerColor, item->headerFont);
                    endChap();

                    startChap();
                    addPar(str, item->contentColor, item->contentFont);
                    addPar("");
                    endChap();
                }
            }
            else
            if (item->id == PTPet)
            {
                //startChap();
                addStandItem("Pet number", numDet.expression, numerPetC[0], numerPetC[m_expression], m_expression, item, !showImages);
                //endChap();
            }
            else
            if (item->id == PTDayForecast)
            {
                //startChap();
                addStandItem("Day Forecast for", QString(mname[cdate.month()-1]) + " " + QString::number(cdate.day()) + " " + QString::number(cdate.year()), QString(), numerPetDForecast[fdcode], fdcode, item, !showImages);
                //endChap();
            }
            else
            if (item->id == PTCustomText)
            {
                addStandItem("Custom Text", QString(), numerCustomText[0], QString(), -1, item, !showImages);
            }
        }
    }
    else
    {
        startChap();
        addHead("Pet Numerology Chart");
        endChap();

        startChap();
        addPar(str);
        addPar("");
        endChap();


        // Details
        startChap();
        addHead("Pet number " + numDet.expression);
        addPar(numerPetC[0] + "<br />" + numerPetC[m_expression]);
        endChap();


        startChap();
        addHead("Day Forecast for "
            + QString(mname[cdate.month()-1]) + " " + QString::number(cdate.day()) + " " + QString::number(cdate.year()));
        addPar(numerPetDForecast[fdcode]);
        endChap();
    }
    endDoc();

	return true;
}

bool CNumerology::calculateCelDynamic(QDate bdate, RepSettings *repList, int curRep, bool trial)
{
    const QList<repItem> *Celebrities = &repList->at(curRep).Celebrities;
    const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

    bool showImages=true;
    if (repList->at(curRep).extendedSettings.value("RTCelebrities-hide-images", "FALSE")=="TRUE")
        showImages=false;

    startDoc("Celebrities Report");

    for (int i = 0; i < Celebrities->size(); i++)
    {
        if (trial && i > 1)
            break;

        const repItem *item = &Celebrities->at(i);
        if (item->id == CTHeader)
        {

            if (item->isContentDefault)
            {
                startChap();
                addHead("Celebrities Report for");
                endChap();

                startChap();
                addHead(fullName + ", " + QString(mname[bdate.month()-1]) + " " + QString::number(bdate.day()) + " " + QString::number(bdate.year()));
                addPar("Date Number " + numDet.lifePath + "<br />Name Number " + numDet.expression);
                endChap();
            }
            else
            {
                startChap();
                addHead(item->headerName, item->headerColor, item->headerFont);
                endChap();

                startChap();
                addHead(fullName + ", " + QString(mname[bdate.month()-1]) + " " + QString::number(bdate.day()) + " " + QString::number(bdate.year()), item->contentColor, item->contentFont);
                addPar("Date Number " + numDet.lifePath + "<br />Name Number " + numDet.expression, item->contentColor, item->contentFont);
                endChap();
            }

        }
        else
        if (item->id == CTLifePath)
        {
            bool isAdd = false;

            addStandItem("Celebrities with same Life Path", QString(), QString(), QString(), -1, item, true);
            for (int a=0; a<200; a++)
            {
                if (numerCelILP[a] == m_LifePath)
                {
                    isAdd = true;

                    if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                        addImage(QString("%1").arg(a, 3, 10, QChar('0')) + ".jpg");
                    if (item->isContentDefault)
                    {
                        startChap(false);
                        addHead(numerCelName[a] + ", "
                                + QString::number((numerCelBDT[a]>>8) & 0xff)
                                + "/" + QString::number(numerCelBDT[a] &0xff)
                                + "/" + QString::number((numerCelBDT[a]>>16) & 0xffff));
                        addPar("<br />" + numerCelLP[a]);
                        endChap();
                    }
                    else
                    {
                        startChap(false);
                        addHead(numerCelName[a] + ", "
                                + QString::number((numerCelBDT[a]>>8) & 0xff)
                                + "/" + QString::number(numerCelBDT[a] &0xff)
                                + "/" + QString::number((numerCelBDT[a]>>16) & 0xffff), item->headerColor, item->headerFont, item->imgList.at(a));
                        addPar("<br />" + numerCelLP[a], item, a);
                    }

                    if (trial)
                    {
                        startChap();
#ifndef VER_FREE
                        addPar("<br />" + numerInfo[9] + "<br />");
#endif
                        endChap();
                        break;
                    }
                }
            }
            if (isAdd)
            {
                startChap();
                addPar("<br />");
                endChap();
            }
        }
        else
        if (item->id == CTBrithday)
        {
            bool isAdd = false;
            bool addH = true;
            for (int a=0; a<200; a++)
            {
                if ((((numerCelBDT[a]) & 0xff) == m_bdate.day()) && (numerCelIDAY[a] > 0))
                {
                    isAdd = true;

                    if (addH)
                    {
                        //startChap();
                        addStandItem("Celebrities with same Birth day", QString(), QString(), QString(), -1, item, true);
                        addH = false;
                        //endChap();
                    }
                    if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                        addImage(QString("%1").arg(a, 3, 10, QChar('0')) + ".jpg");
                    if (item->isContentDefault)
                    {
                        startChap(false);
                        addHead(numerCelName[a] + ", "
                                + QString::number((numerCelBDT[a]>>8) & 0xff)
                                + "/" + QString::number(numerCelBDT[a] &0xff)
                                + "/" + QString::number((numerCelBDT[a]>>16) & 0xffff));

                        addPar("<br />" + numerCelBD[a]);
                        endChap();
                    }
                    else
                    {
                        startChap(false);
                        addHead(numerCelName[a] + ", "
                                + QString::number((numerCelBDT[a]>>8) & 0xff)
                                + "/" + QString::number(numerCelBDT[a] &0xff)
                                + "/" + QString::number((numerCelBDT[a]>>16) & 0xffff), item->headerColor, item->headerFont, item->imgList.at(a));

                        addPar("<br />" + numerCelBD[a], item, a);
                    }
                    if (trial)
                    {
                        startChap();
#ifndef VER_FREE
                        addPar("<br />" + numerInfo[9] + "<br />");
#endif
                        endChap();
                        break;
                    }
                }
            }

            if (isAdd)
            {
                startChap();
                addPar("<br />");
                endChap();
            }
        }
        else
        if (item->id == CTExpression)
        {
            bool addH = true;
            bool isAdd = false;
            for (int a=0; a<200; a++)
            {
                if (numerCelIEXPR[a] == m_expression)
                {
                    isAdd = true;
                    //startChap();
                    if (addH)
                    {
                        addStandItem("Celebrities with same Expression number", QString(), QString(), QString(), -1, item, true);
                        addH = false;
                    }
                    if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                        addImage(QString("%1").arg(a, 3, 10, QChar('0')) + ".jpg");
                    if (item->isContentDefault)
                    {
                        startChap(false);
                        addHead(numerCelName[a] + ", "
                                + QString::number((numerCelBDT[a]>>8) & 0xff)
                                + "/" + QString::number(numerCelBDT[a] &0xff)
                                + "/" + QString::number((numerCelBDT[a]>>16) & 0xffff));

                        addPar("<br />" + numerCelExpr[a]);
                        endChap();
                    }
                    else
                    {
                        startChap(false);
                        addHead(numerCelName[a] + ", "
                                + QString::number((numerCelBDT[a]>>8) & 0xff)
                                + "/" + QString::number(numerCelBDT[a] &0xff)
                                + "/" + QString::number((numerCelBDT[a]>>16) & 0xffff), item->headerColor, item->headerFont, item->imgList.at(a));

                        addPar("<br />" + numerCelExpr[a], item, a);
                    }
                    //endChap();

                    if (trial)
                    {
                        startChap();
#ifndef VER_FREE
                        addPar("<br />" + numerInfo[9] + "<br />");
#endif
                        endChap();
                        break;
                    }
                }
            }
            if (isAdd)
            {
                startChap();
                addPar("<br />");
                endChap();
            }
        }
        else
        if (item->id == CTCustomText)
        {
            addStandItem("Custom Text", QString(), numerCustomText[0], QString(), -1, item, !showImages);
        }
    }
    endDoc();
    return true;
}

bool CNumerology::calculateCel(QString sfullName, QDate bdate, bool useCNC, bool trial, RepSettings *repList, int curRep, int pmode)
{
    int select = curRep - 1;

    if (select >= 0)
    {
        if (repList->size() > 0)
        {
            if (repList->size() <= select)
                select = -1;
        }
        else
        {
            select = -1;
        }
    }

    const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	if (useCNC)
		setCalcMode(CM_CHALDEAN);
	else
		setCalcMode(CM_WESTERN);

	fullName = sfullName.toUpper();

    int baseNums[128];
    int nbaseNums[128];
	int snum;

	resetData();

	// Date calc
	m_bdate = bdate;

	int dday = downToBase(bdate.day());
	int dmonth = downToBase(bdate.month());
	int dyear = downToBase(bdate.year());

	int lp1=0, lp2=0, lp3=0;
	int snum1=0, snum2=0, snum3=0;
	m_LifePath=0;
	if (m_GlobSettings.calcLP3)
	{
		snum3 = 0;
		snum3 += bdate.day()/10;
		snum3 += bdate.day()%10;

		snum3 += bdate.month()/10;
		snum3 += bdate.month()%10;

		snum3 += bdate.year()%10;
		snum3 += (bdate.year()/10)%10;
		snum3 += (bdate.year()/100)%10;
		snum3 += (bdate.year()/1000)%10;

		lp3 = downToBase(snum3);
	}
	if (m_GlobSettings.calcLP2)
	{
		snum2 = bdate.day()+bdate.month()+bdate.year();
		lp2 = downToBase(snum2);
	}
	if ((m_GlobSettings.calcLP1) || ((!m_GlobSettings.calcLP1) && (!m_GlobSettings.calcLP2) && (!m_GlobSettings.calcLP3)))
	{
		snum1 = dday+dmonth+dyear;
		lp1 = downToBase(snum1);
	}

	if ((m_LifePath==0) && (lp1!=0))
	{
		m_LifePath = lp1;
		snum = snum1;
	}
	if ((m_LifePath==0) && (lp2!=0))
	{
		m_LifePath = lp2;
		snum = snum2;
	}
	if ((m_LifePath==0) && (lp3!=0))
	{
		m_LifePath = lp3;
		snum = snum3;
	}

    if (isMasterNumber(lp1, m_MasterNumberModeID) && (!isMasterNumber(m_LifePath, m_MasterNumberModeID)))
	{
		m_LifePath = lp1;
		snum = snum1;
	}
    if (isMasterNumber(lp2, m_MasterNumberModeID) && (!isMasterNumber(m_LifePath, m_MasterNumberModeID)))
	{
		m_LifePath = lp2;
		snum = snum2;
	}
    if (isMasterNumber(lp3, m_MasterNumberModeID) && (!isMasterNumber(m_LifePath, m_MasterNumberModeID)))
	{
		m_LifePath = lp3;
		snum = snum3;
	}

	if (snum==snum1)
		numDet.lifePath = QString::number(dday) + "+" + QString::number(dmonth) + "+" + QString::number(dyear) + "=" + QString::number(snum);
	else
		numDet.lifePath = QString::number(snum);
	if (m_LifePath != snum)
		numDet.lifePath += "=" + QString::number(m_LifePath);
	numDet.birthDay = QString::number(bdate.day());
	if (dday != bdate.day())
		numDet.birthDay += " (" + QString::number(dday) + ")";


    for(int a=0; a<128; a++)
		baseNums[a] = m_BaseNums[a];
	baseNums[m_LifePath]--;

	// Name calc

	clear();
	QList<int> nnums;
	calcNamesNumbers(fullName, nnums);
	//int fnnum = nnums[0];
	int sum=0;
	for(int a=0; a<nnums.size(); a++)
	{
		sum += downToBase(nnums[a]);
	}
    for(int a=0; a<128; a++)
		nbaseNums[a] = m_BaseNums[a];
	m_expression = downToBase(sum);

	numDet.expression = QString::number(m_expression);
	if (sum != m_expression)
		numDet.expression += "/" + QString::number(sum);


	numDet.fnameR = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR += "+";

		int v = nnums[a];
		int vb = downToBase(nnums[a]);
		numDet.fnameR += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR += '/' + QString::number(v);
	}
	numDet.fnameR += "=" + QString::number(sum);
	if (sum != m_expression)
		numDet.fnameR += "=" + QString::number(m_expression);


	numDet.karmicLessons = "";
	QList<int> m_KarmicLessons;
	m_KarmicLessons.clear();

	int letmap[10];
	for (int a=0; a<10; a++)
		letmap[a] = 0;
	for (int a=0; a<fullName.size(); a++)
	{
        int v = downToDigit(letterToNumber(fullName.at(a)));
		letmap[v]++;
	}
	int lmnum=0;
	int lmmax=0;
	for (int a=1; a<10; a++)
	{
		if (letmap[a]>lmmax)
			lmmax = letmap[a];

		if (letmap[a]!=0)
		{
			lmnum++;
		} else
		{
			if (numDet.karmicLessons.size()!=0)
				numDet.karmicLessons += ", ";
			numDet.karmicLessons += QString::number(a);
			m_KarmicLessons.append(a);
		}
	}
	if (numDet.karmicLessons.size()==0)
		numDet.karmicLessons = "-";

	QList<int> m_HiddenPassion;
	m_HiddenPassion.clear();
	numDet.hiddenPassion = "";
	for (int a=1; a<10; a++)
	{
		if (letmap[a]==lmmax)
		{
			if (numDet.hiddenPassion.size()!=0)
				numDet.hiddenPassion += ", ";
			numDet.hiddenPassion += QString::number(a);
			m_HiddenPassion.append(a);
		}
	}

	numDet.subconsciousConfidence = QString::number(lmnum);
	//int m_SubconsciousConfidence = lmnum;


	nnums.clear();
	calcNamesNumbersP(fullName, nnums, true, pmode);
	sum=0;
	for(int a=0; a<nnums.size(); a++)
		sum += downToBase(nnums[a]);
	int m_hearDesire = downToBase(sum);

	numDet.fnameR1 = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR1 += "+";

		int v = nnums[a];
		int vb = downToBase(nnums[a]);
		numDet.fnameR1 += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR1 += '/' + QString::number(v);
	}
	numDet.fnameR1 += "=" + QString::number(sum);
	if (sum != m_hearDesire)
		numDet.fnameR1 += "=" + QString::number(m_hearDesire);

	numDet.heartDesire = QString::number(m_hearDesire);
	if (sum != m_hearDesire)
		numDet.heartDesire += "/" + QString::number(sum);



	nnums.clear();
	calcNamesNumbersP(fullName, nnums, false, pmode);
	sum=0;
	for(int a=0; a<nnums.size(); a++)
		sum += downToBase(nnums[a]);
	int m_personality = downToBase(sum);

	numDet.fnameR2 = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR2 += "+";

		int v = nnums[a];
		int vb = downToBase(nnums[a]);
		numDet.fnameR2 += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR2 += '/' + QString::number(v);
	}
	numDet.fnameR2 += "=" + QString::number(sum);
	if (sum != m_personality)
		numDet.fnameR2 += "=" + QString::number(m_personality);

	numDet.personality = QString::number(m_personality);
	if (sum != m_personality)
		numDet.personality += "/" + QString::number(sum);


	numDet.fnameN1 = "";
	numDet.fnameN2 = "";
	for (int a=0; a<fullName.size(); a++)
	{
		int v = isVow(fullName, a, pmode);
		if (v>0)
		{
            numDet.fnameN1 += QString::number(downToDigit(letterToNumber(fullName[a])));
			numDet.fnameN2 += " ";
		} else if (v<0)
		{
            numDet.fnameN2 += QString::number(downToDigit(letterToNumber(fullName[a])));
			numDet.fnameN1 += " ";
		} else
		{
			numDet.fnameN1 += " ";
			numDet.fnameN2 += " ";
		}
	}


	int lvals[16];
	for (int a=0; a<16; a++)
		lvals[a] = 0;
	for (int a=0; a<fullName.size(); a++)
	{
		//int i = getPEIndex(fullName.at(a));
		int i = getPEIndex(fullName.at(a));
		if ((i>=0) && (i<12))
		{
			m_PELet[i]++;
            lvals[i] += downToDigit(letterToNumber(fullName.at(a)));
		}

		i = fullName.at(a).toUpper().toAscii() - 'A';
        if ((i>=0) && (i<128))
			m_Let[i]++;
	}
	for (int a=0; a<12; a++)
		lvals[a] = statDownToBase(lvals[a]);


	for (int a=0; a<20; a++)
	{
		numDet.PE[a] = "";
		numDet.PENum[a] = "";
		numDet.PECnt[a] = "";
	}

	int xsum[5], ysum[5], xcnt[5], ycnt[5];
	int pecells[20], pecnt[20];
	for (int a=0; a<20; a++)
	{
		pecells[a] = 0;
		pecnt[a] = 0;
	}
	for (int a=0; a<5; a++)
	{
		xsum[a] = 0;
		ysum[a] = 0;
		xcnt[a] = 0;
		ycnt[a] = 0;
	}

	for (int a='A'; a<='Z'; a++)
	{
		int i = getPEIndex(QChar(a));
		if ((i>=0) && (i<12))
		{
			int p = i%4;
			int l = i/4;
			if (m_Let[a-'A']>0)
			{
				numDet.PE[l*5+p] += a + QString::number(m_Let[a-'A']);
				int c = m_Let[a-'A'];
                int v = m_Let[a-'A']*downToDigit(letterToNumber(QChar(a)));
				pecnt[l*5+p] += c;
				pecells[l*5+p] += v;
				xcnt[p] += c;
				ycnt[l] += c;
			}
		}
	}

	for (int l=0; l<4; l++)
	{
		for (int p=0; p<5; p++)
		{
			int v = statDownToBase(pecells[l*5+p]);
			xsum[p] += v;
			ysum[l] += v;
		}
	}

	for (int a=0; a<20; a++)
	{
		if (numDet.PE[a].size() > 0)
		{
			numDet.PERep[a] = numDet.PE[a] + "=";
			int v = statDownToBase(pecells[a]);
			if (v != pecells[a])
				numDet.PERep[a] += QString::number(v) + "/" + QString::number(pecells[a]) + ", ";
			else
				numDet.PERep[a] += QString::number(v) + ", ";
			numDet.PERep[a] += QString::number(pecnt[a]);
		}
	}
	for (int l=0; l<4; l++)
	{
		int p = 4;
		int a = l*5+p;

		int v = statDownToBase(ysum[l]);
		if (v != ysum[l])
			numDet.PERep[a] += QString::number(v) + "/" + QString::number(ysum[l]) + ", ";
		else
			numDet.PERep[a] += QString::number(v) + ", ";
		numDet.PERep[a] += QString::number(ycnt[l]);
	}
	for (int p=0; p<5; p++)
	{
		int l = 3;
		int a = l*5+p;

		int v = statDownToBase(xsum[p]);
		if (v != xsum[p])
			numDet.PERep[a] += QString::number(v) + "/" + QString::number(xsum[p]) + ", ";
		else
			numDet.PERep[a] += QString::number(v) + ", ";
		numDet.PERep[a] += QString::number(xcnt[p]);
	}

    if (select >= 0)
        return calculateCelDynamic(bdate, repList, select, trial);

	startDoc("VeBest Numerology Celebrities Report");
	startChap();
	addHead("Celebrities Report for");
	addHead(fullName + ", " + QString(mname[bdate.month()-1]) + " " + QString::number(bdate.day()) + " " + QString::number(bdate.year()));
	addPar("Date Number " + numDet.lifePath + "<br />Name Number " + numDet.expression);
	addPar("");

	addHead("Celebrities with same Life Path");
	endChap();

	for (int a=0; a<200; a++)
	{
		if (numerCelILP[a] == m_LifePath)
		{
			startChap();
			addImage(QString("%1").arg(a, 3, 10, QChar('0')) + ".jpg");
			addHead(numerCelName[a] + ", "
					+ QString::number((numerCelBDT[a]>>8) & 0xff)
					+ "/" + QString::number(numerCelBDT[a] &0xff)
					+ "/" + QString::number((numerCelBDT[a]>>16) & 0xffff));
			addPar("<br />" + numerCelLP[a]);
			endChap();
			if (trial)
				break;
		}
	}

	if (trial)
	{
		startChap();
#ifndef VER_FREE
        addPar("<br />" + numerInfo[9] + "<br />");
#endif
		endChap();
	} else
	{
		startChap();
		addPar("<br />");
		endChap();

		//addHead("Celebrities with same birth day");
		bool addH = true;
		for (int a=0; a<200; a++)
		{
			if ((((numerCelBDT[a]) & 0xff) == m_bdate.day()) && (numerCelIDAY[a] > 0))
			{
				startChap();
				if (addH)
				{
					addHead("Celebrities with same Birth day");
					addH = false;
				}
				addImage(QString("%1").arg(a, 3, 10, QChar('0')) + ".jpg");
				addHead(numerCelName[a] + ", "
						+ QString::number((numerCelBDT[a]>>8) & 0xff)
						+ "/" + QString::number(numerCelBDT[a] &0xff)
						+ "/" + QString::number((numerCelBDT[a]>>16) & 0xffff));
				
				addPar("<br />" + numerCelBD[a]);
				endChap();
			}
		}

		startChap();
		addPar("<br />");
		endChap();

		addH = true;
		//addHead("Celebrities with same expression number");
		for (int a=0; a<200; a++)
		{
			if (numerCelIEXPR[a] == m_expression)
			{
				startChap();
				if (addH)
				{
					addHead("Celebrities with same Expression number");
					addH = false;
				}
				addImage(QString("%1").arg(a, 3, 10, QChar('0')) + ".jpg");
				addHead(numerCelName[a] + ", "
						+ QString::number((numerCelBDT[a]>>8) & 0xff)
						+ "/" + QString::number(numerCelBDT[a] &0xff)
						+ "/" + QString::number((numerCelBDT[a]>>16) & 0xffff));
				
				addPar("<br />" + numerCelExpr[a]);
				endChap();
			}
		}
	}


	endDoc();
	return true;
}


bool CNumerology::calculateAn(QString sfullName, QDate bdate, bool)
{
	fullName = sfullName.toUpper();

    int baseNums[128];
    int nbaseNums[128];
	int snum;
    const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	resetData();

	// Date calc
	m_bdate = bdate;

	int dday = downToBase(bdate.day());
	int dmonth = downToBase(bdate.month());
	int dyear = downToBase(bdate.year());

	//int m_LifePathF = dday+dmonth+dyear;
	snum = dday+dmonth+dyear;
	m_LifePath = downToBase(snum);

	numDet.lifePath = QString::number(dday) + "+" + QString::number(dmonth) + "+" + QString::number(dyear) + "=" + QString::number(snum);
	if (m_LifePath != snum)
		numDet.lifePath += "=" + QString::number(m_LifePath);

    for(int a=0; a<128; a++)
		baseNums[a] = m_BaseNums[a];
	baseNums[m_LifePath]--;

	// Name calc

	clear();
	QList<int> nnums;
	calcNamesNumbers(fullName, nnums);
	//int fnnum = nnums[0];
	int sum=0;
	for(int a=0; a<nnums.size(); a++)
	{
		sum += downToBase(nnums[a]);
	}
    for(int a=0; a<128; a++)
		nbaseNums[a] = m_BaseNums[a];
	m_expression = downToBase(sum);

	numDet.expression = QString::number(m_expression);
	if (sum != m_expression)
		numDet.expression += "/" + QString::number(sum);

    numDet.compound = sum;

	numDet.fnameR = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR += "+";

		int v = nnums[a];
		int vb = downToBase(nnums[a]);
		numDet.fnameR += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR += '/' + QString::number(v);
	}
	numDet.fnameR += "=" + QString::number(sum);
	if (sum != m_expression)
		numDet.fnameR += "=" + QString::number(m_expression);


	numDet.karmicLessons = "";
	QList<int> m_KarmicLessons;
	m_KarmicLessons.clear();

	int letmap[10];
	for (int a=0; a<10; a++)
		letmap[a] = 0;
	for (int a=0; a<fullName.size(); a++)
	{
        int v = downToDigit(letterToNumber(fullName.at(a)));
		letmap[v]++;
	}
	int lmnum=0;
	int lmmax=0;
	for (int a=1; a<10; a++)
	{
		if (letmap[a]>lmmax)
			lmmax = letmap[a];

		if (letmap[a]!=0)
		{
			lmnum++;
		} else
		{
			if (numDet.karmicLessons.size()!=0)
				numDet.karmicLessons += ", ";
			numDet.karmicLessons += QString::number(a);
			m_KarmicLessons.append(a);
		}
	}
	if (numDet.karmicLessons.size()==0)
		numDet.karmicLessons = "-";

	QList<int> m_HiddenPassion;
	m_HiddenPassion.clear();
	numDet.hiddenPassion = "";
	for (int a=1; a<10; a++)
	{
		if (letmap[a]==lmmax)
		{
			if (numDet.hiddenPassion.size()!=0)
				numDet.hiddenPassion += ", ";
			numDet.hiddenPassion += QString::number(a);
			m_HiddenPassion.append(a);
		}
	}

	numDet.subconsciousConfidence = QString::number(lmnum);
	//int m_SubconsciousConfidence = lmnum;


	nnums.clear();
	calcNamesNumbersP(fullName, nnums, true, NUM_VOW_MODE_ENGLISH);
	sum=0;
	for(int a=0; a<nnums.size(); a++)
		sum += downToBase(nnums[a]);
	int m_hearDesire = downToBase(sum);

	numDet.fnameR1 = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR1 += "+";

		int v = nnums[a];
		int vb = downToBase(nnums[a]);
		numDet.fnameR1 += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR1 += '/' + QString::number(v);
	}
	numDet.fnameR1 += "=" + QString::number(sum);
	if (sum != m_hearDesire)
		numDet.fnameR1 += "=" + QString::number(m_hearDesire);

	numDet.heartDesire = QString::number(m_hearDesire);
	if (sum != m_hearDesire)
		numDet.heartDesire += "/" + QString::number(sum);



	nnums.clear();
	calcNamesNumbersP(fullName, nnums, false, NUM_VOW_MODE_ENGLISH);
	sum=0;
	for(int a=0; a<nnums.size(); a++)
		sum += downToBase(nnums[a]);
	int m_personality = downToBase(sum);

	numDet.fnameR2 = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR2 += "+";

		int v = nnums[a];
		int vb = downToBase(nnums[a]);
		numDet.fnameR2 += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR2 += '/' + QString::number(v);
	}
	numDet.fnameR2 += "=" + QString::number(sum);
	if (sum != m_personality)
		numDet.fnameR2 += "=" + QString::number(m_personality);

	numDet.personality = QString::number(m_personality);
	if (sum != m_personality)
		numDet.personality += "/" + QString::number(sum);


	numDet.fnameN1 = "";
	numDet.fnameN2 = "";
	for (int a=0; a<fullName.size(); a++)
	{
		int v = isVow(fullName, a, NUM_VOW_MODE_ENGLISH);
		if (v>0)
		{
            numDet.fnameN1 += QString::number(downToDigit(letterToNumber(fullName[a])));
			numDet.fnameN2 += " ";
		} else if (v<0)
		{
            numDet.fnameN2 += QString::number(downToDigit(letterToNumber(fullName[a])));
			numDet.fnameN1 += " ";
		} else
		{
			numDet.fnameN1 += " ";
			numDet.fnameN2 += " ";
		}
	}


	int lvals[16];
	for (int a=0; a<16; a++)
		lvals[a] = 0;
	for (int a=0; a<fullName.size(); a++)
	{
		//int i = getPEIndex(fullName.at(a));
		int i = getPEIndex(fullName.at(a));
		if ((i>=0) && (i<12))
		{
			m_PELet[i]++;
            lvals[i] += downToDigit(letterToNumber(fullName.at(a)));
		}

		i = fullName.at(a).toUpper().toAscii() - 'A';
        if ((i>=0) && (i<128))
			m_Let[i]++;
	}
	for (int a=0; a<12; a++)
		lvals[a] = statDownToBase(lvals[a]);


	for (int a=0; a<20; a++)
	{
		numDet.PE[a] = "";
		numDet.PENum[a] = "";
		numDet.PECnt[a] = "";
	}

	int xsum[5], ysum[5], xcnt[5], ycnt[5];
	int pecells[20], pecnt[20];
	for (int a=0; a<20; a++)
	{
		pecells[a] = 0;
		pecnt[a] = 0;
	}
	for (int a=0; a<5; a++)
	{
		xsum[a] = 0;
		ysum[a] = 0;
		xcnt[a] = 0;
		ycnt[a] = 0;
	}

	for (int a='A'; a<='Z'; a++)
	{
		int i = getPEIndex(QChar(a));
		if ((i>=0) && (i<12))
		{
			int p = i%4;
			int l = i/4;
			if (m_Let[a-'A']>0)
			{
				numDet.PE[l*5+p] += a + QString::number(m_Let[a-'A']);
				int c = m_Let[a-'A'];
                int v = m_Let[a-'A']*downToDigit(letterToNumber(QChar(a)));
				pecnt[l*5+p] += c;
				pecells[l*5+p] += v;
				xcnt[p] += c;
				ycnt[l] += c;
			}
		}
	}

	for (int l=0; l<4; l++)
	{
		for (int p=0; p<5; p++)
		{
			int v = statDownToBase(pecells[l*5+p]);
			xsum[p] += v;
			ysum[l] += v;
		}
	}

	for (int a=0; a<20; a++)
	{
		if (numDet.PE[a].size() > 0)
		{
			numDet.PERep[a] = numDet.PE[a] + "=";
			int v = statDownToBase(pecells[a]);
			if (v != pecells[a])
				numDet.PERep[a] += QString::number(v) + "/" + QString::number(pecells[a]) + ", ";
			else
				numDet.PERep[a] += QString::number(v) + ", ";
			numDet.PERep[a] += QString::number(pecnt[a]);
		}
	}
	for (int l=0; l<4; l++)
	{
		int p = 4;
		int a = l*5+p;

		int v = statDownToBase(ysum[l]);
		if (v != ysum[l])
			numDet.PERep[a] += QString::number(v) + "/" + QString::number(ysum[l]) + ", ";
		else
			numDet.PERep[a] += QString::number(v) + ", ";
		numDet.PERep[a] += QString::number(ycnt[l]);
	}
	for (int p=0; p<5; p++)
	{
		int l = 3;
		int a = l*5+p;

		int v = statDownToBase(xsum[p]);
		if (v != xsum[l])
			numDet.PERep[a] += QString::number(v) + "/" + QString::number(xsum[p]) + ", ";
		else
			numDet.PERep[a] += QString::number(v) + ", ";
		numDet.PERep[a] += QString::number(xcnt[p]);
	}

	startDoc("VeBest Numerology Report");
	startChap();
    addHead("Express Numerology chart for");
	addHead(fullName + ", " + QString(mname[bdate.month()-1]) + " " + QString::number(bdate.day()) + " " + QString::number(bdate.year()));
	addPar("Date Number " + numDet.lifePath + "<br />Name Number " + numDet.expression);
    addPar("<br />");


	// Details
    addHead("Your name number: "  + numDet.expression);
    addPar(numerNumInterp[m_expression] + "<br />");
    addPar("");
    if ((numDet.compound>9) && (numDet.compound<100)) {
        addHead("People with double-digit name number "  + QString::number(numDet.compound));
        addPar(numerDoubleDigitNumbers[numDet.compound] + "<br />");
    }

    addHead("Your date number:");
    addPar(numerNumInterp[m_LifePath] + "<br />");
    endChap();
	endDoc();
	//m_TextDoc.setHtml(htmlDoc);

	return true;
}

QString CNumerology::getCalculateHeaderC(int *baseNums, int *m_spNum, int *m_pNum, bool useCNC, QList<int>ids)
{
    QString str;

    // Summary
    if (ids.contains(NTLifePath)) {
        str += "Life path number " + numDet.lifePath;
        str +="<br />\r\n";
    }

    if (ids.contains(NTBrithday)) {
        str += "Birthday " + numDet.birthDay;
        str +="<br />\r\n";
    }

    if (ids.contains(NTKarmicDebt)||ids.contains(NTKarmicDebts)) {
        if ((baseNums[13]!=0) || (baseNums[14]!=0) || (baseNums[16]!=0) || (baseNums[19]!=0))
        {
            str += "Karmic Debts ";
            QString ksl;
            if (baseNums[13]!=0)
                ksl = "13";
            if (baseNums[14]!=0)
            {
                if (ksl.size()>0)
                    ksl += ", ";
                ksl += "14";
            }
            if (baseNums[16]!=0)
            {
                if (ksl.size()>0)
                    ksl += ", ";
                ksl += "16";
            }
            if (baseNums[19]!=0)
            {
                if (ksl.size()>0)
                    ksl += ", ";
                ksl += "19";
            }
            str += ksl;

            str+="<br />\r\n";
        }
    }

    if (ids.contains(NTExpression)) {
        str += "Expression number " + numDet.expression;
        str+="<br />\r\n";
    }

    if (ids.contains(NTMinorExpression)) {
        str += "Minor Expression number " + numDet.mexpression;
        str+="<br />\r\n";
    }

    if (ids.contains(NTHeartDesire)) {
        str += "Heart Desire number " + numDet.heartDesire;
        str+="<br />\r\n";
    }

    if (ids.contains(NTMinorHeartDesire)) {
        str += "Minor Heart Desire number " + numDet.mheartDesire;
        str+="<br />\r\n";
    }

    if (ids.contains(NTCompound)) {
        if (useCNC && (numDet.compound>=10)) {
            str += "Compound number " + QString::number(numDet.compound);
            str+="<br />\r\n";
        }
    }

    if (ids.contains(NTPersonality)) {
        str += "Personality number " + numDet.personality;
        str+="<br />\r\n";
    }

    if (ids.contains(NTMinorPersonality)) {
        str += "Minor personality number " + numDet.mpersonality;
        str+="<br />\r\n";
    }

    if (ids.contains(NTMaturity)) {
        str += "Maturity number " + numDet.maturity;
        str+="<br />\r\n";
    }

    if (ids.contains(NTRationalThought)) {
        str += "Rational Thought number " + numDet.rationalThought;
        str+="<br />\r\n";
    }

    if (ids.contains(NTKarmicLessons)) {
        str += "Karmic Lessons numbers " + numDet.karmicLessons;
        str+="<br />\r\n";
    }

    if (ids.contains(NTBalanceNumber)) {
        str += "Balance number " + numDet.balance;
        str+="<br />\r\n";
    }

    if (ids.contains(NTHiddenPassion)) {
        str += "Hidden Passion number " + numDet.hiddenPassion;
        str+="<br />\r\n";
    }

    if (ids.contains(NTSubconsciousConfidence)) {
        str += "Subconscious Confidence number " + numDet.subconsciousConfidence;
        str+="<br />\r\n";
    }

    if (ids.contains(NTChallenges)||ids.contains(NTPinnacles)) {
        str += "Age span periods for challenges and pinnacles " + numDet.ageSpan;
        str+="<br />\r\n";
    }

    if (ids.contains(NTChallenges)) {
        str += "Challenge numbers: " + QString::number(m_cNum1) + ", " + QString::number(m_cNum2) + ", " + QString::number(m_cNum3) + ", " + QString::number(m_cNum4);
        str+="<br />\r\n";
    }

    if (ids.contains(NTPinnacles)) {
        str += "Pinnacle numbers: ";
        if (m_pNum[0] != m_spNum[0])
            str += QString::number(m_pNum[0]) + "/" + QString::number(m_spNum[0]) + ", ";
        else
            str += QString::number(m_pNum[0]) + ", ";
        if (m_pNum[1] != m_spNum[1])
            str += QString::number(m_pNum[1]) + "/" + QString::number(m_spNum[1]) + ", ";
        else
            str += QString::number(m_pNum[1]) + ", ";
        if (m_pNum[2] != m_spNum[2])
            str += QString::number(m_pNum[2]) + "/" + QString::number(m_spNum[2]) + ", ";
        else
            str += QString::number(m_pNum[2]) + ", ";
        if (m_pNum[3] != m_spNum[3])
            str += QString::number(m_pNum[3]) + "/" + QString::number(m_spNum[3]);
        else
            str += QString::number(m_pNum[3]);
        str+="<br />\r\n";
    }

    if (ids.contains(NTLifePath)||ids.contains(NTChallenges)||ids.contains(NTPinnacles)) {
        str += "Life Path periods " + numDet.lpCycles;
        str+="<br />\r\n";

        str += "Time span of Life Path periods " + numDet.lpAgeSpan;
        str+="<br />\r\n";
    }

    if (ids.contains(NTBridgeLPE)) {
        str += "Life Path - Expression Bridge number " + numDet.brLPE;
        str+="<br />\r\n";
    }

    if (ids.contains(NTBridgeHDP)) {
        str += "Heart's Desire - Personality Bridge number " + numDet.brHDP;
        str+="<br />\r\n";
    }

    return str;
}

void CNumerology::s_spNum(int *m_spNum, int *m_pNum)
{
    int dday = downToBase(m_bdate.day());
    int dmonth = downToBase(m_bdate.month());
    int dyear = downToBase(m_bdate.year());
    int m_spNum1 = downToBase(downToBase(dday)+downToBase(dmonth));
    int m_spNum2 = downToBase(downToBase(dday)+downToBase(dyear));
    int m_spNum3 = downToBase(m_spNum1+m_spNum2);
    int m_spNum4 = downToBase(downToBase(dyear)+downToBase(dmonth));
    int m_pNum1 = downToDigit(m_spNum1);
    int m_pNum2 = downToDigit(m_spNum2);
    int m_pNum3 = downToDigit(m_spNum3);
    int m_pNum4 = downToDigit(m_spNum4);

    m_spNum[0] = m_spNum1;
    m_spNum[1] = m_spNum2;
    m_spNum[2] = m_spNum3;
    m_spNum[3] = m_spNum4;

    m_pNum[0] = m_pNum1;
    m_pNum[1] = m_pNum2;
    m_pNum[2] = m_pNum3;
    m_pNum[3] = m_pNum4;
}

void CNumerology::calculateGenTable(QList<int> ids)
{
	const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

    QString dstyle = " style=\"font-size: 12px; line-height: 1.2;\" ";
    if (ids.contains(NTExpression)||ids.contains(NTMinorExpression)||ids.contains(NTPersonality)||ids.contains(NTHeartDesire)) {
        addPlain("<table class=maindoc border=\"0\" cellspacing=\"3\" >");
        addPlain("<tr><td colspan=\"" + QString("__fullname__") + "\"><b>Full name</b></td></tr>\r\n");

        addPlain("<tr"+dstyle+">");
        for (int a=0; a<numDet.fnameN1.size(); a++)
        {
            QChar sym = fullName.at(a);
            sym = sym.toUpper();
            char s = sym.toAscii();
            if (s == 'K')
            {
                if (m_GlobSettings.calcNameDD)
                    addPlain(tr("<td><b>") + numDet.fnameN1.at(a) + tr("</b></td>"));
                else
                    addPlain(tr("<td><b>") + tr("11") + tr("</b></td>"));
            } else if (s == 'V')
            {
                if (m_GlobSettings.calcNameDD)
                    addPlain(tr("<td><b>") + numDet.fnameN1.at(a) + tr("</b></td>"));
                else
                    addPlain(tr("<td><b>") + tr("22") + tr("</b></td>"));
            } else
                addPlain(tr("<td>") + numDet.fnameN1.at(a) + tr("</td>"));
        }
        addPlain(tr("<td>") + numDet.fnameR1 + tr("</td>"));
        addPlain("</tr>\r\n");

        addPlain("<tr"+dstyle+">");
        for (int a=0; a<fullName.size(); a++)
        {
            QChar sym = fullName.at(a);
            sym = sym.toUpper();
            char s = sym.toAscii();
            if ((s == 'K') || (s == 'V'))
                addPlain(tr("<td><b>") + fullName.at(a) + tr("</b></td>"));
            else
                addPlain(tr("<td>") + fullName.at(a) + tr("</td>"));
        }
        addPlain(tr("<td>") + numDet.fnameR + tr("</td>"));
        addPlain("</tr>\r\n");

        addPlain("<tr"+dstyle+">");
        for (int a=0; a<numDet.fnameN2.size(); a++)
        {
            QChar sym = fullName.at(a);
            sym = sym.toUpper();
            char s = sym.toAscii();
            if (s == 'K')
            {
                if (m_GlobSettings.calcNameDD)
                    addPlain(tr("<td><b>") + numDet.fnameN2.at(a) + tr("</b></td>"));
                else
                    addPlain(tr("<td><b>") + tr("11") + tr("</b></td>"));
            } else if (s == 'V')
            {
                if (m_GlobSettings.calcNameDD)
                    addPlain(tr("<td><b>") + numDet.fnameN2.at(a) + tr("</b></td>"));
                else
                    addPlain(tr("<td><b>") + tr("22") + tr("</b></td>"));
            } else
                addPlain(tr("<td>") + numDet.fnameN2.at(a) + tr("</td>"));
        }
        addPlain(tr("<td>") + numDet.fnameR2 + tr("</td>"));
        addPlain("</tr>\r\n");

        addPlain("<tr><td><p></p></td></tr></table><p></p>\r\n");


        addPlain("<table class=maindoc border=\"0\" cellspacing=\"3\">");
        addPlain("<tr><td colspan=\"" + QString("__shortname__") + "\"><b>Short name</b></td></tr>\r\n");

        addPlain("<tr"+dstyle+">");
        for (int a=0; a<numDet.snameN1.size(); a++)
        {
            QChar sym = shortName.at(a);
            sym = sym.toUpper();
            char s = sym.toAscii();
            if (s == 'K')
            {
                if (m_GlobSettings.calcNameDD)
                    addPlain(tr("<td><b>") + numDet.snameN1.at(a) + tr("</b></td>"));
                else
                    addPlain(tr("<td><b>") + tr("11") + tr("</b></td>"));
            } else if (s == 'V')
            {
                if (m_GlobSettings.calcNameDD)
                    addPlain(tr("<td><b>") + numDet.snameN1.at(a) + tr("</b></td>"));
                else
                    addPlain(tr("<td><b>") + tr("22") + tr("</b></td>"));
            } else
                addPlain(tr("<td>") + numDet.snameN1.at(a) + tr("</td>"));
        }
        addPlain(tr("<td>") + numDet.snameR1 + tr("</td>"));
        addPlain("</tr>\r\n");

        addPlain("<tr"+dstyle+">");
        for (int a=0; a<shortName.size(); a++)
        {
            QChar sym = shortName.at(a);
            sym = sym.toUpper();
            char s = sym.toAscii();
            if ((s == 'K') || (s == 'V'))
                addPlain(tr("<td><b>") + shortName.at(a) + tr("</b></td>"));
            else
                addPlain(tr("<td>") + shortName.at(a) + tr("</td>"));
        }
        addPlain(tr("<td>") + numDet.snameR + tr("</td>"));
        addPlain("</tr>\r\n");

        addPlain("<tr"+dstyle+">");
        for (int a=0; a<numDet.snameN2.size(); a++)
        {
            QChar sym = shortName.at(a);
            sym = sym.toUpper();
            char s = sym.toAscii();
            if (s == 'K')
            {
                if (m_GlobSettings.calcNameDD)
                    addPlain(tr("<td><b>") + numDet.snameN2.at(a) + tr("</b></td>"));
                else
                    addPlain(tr("<td><b>") + tr("11") + tr("</b></td>"));
            } else if (s == 'V')
            {
                if (m_GlobSettings.calcNameDD)
                    addPlain(tr("<td><b>") + numDet.snameN2.at(a) + tr("</b></td>"));
                else
                    addPlain(tr("<td><b>") + tr("22") + tr("</b></td>"));
            } else
                addPlain(tr("<td>") + numDet.snameN2.at(a) + tr("</td>"));
        }
        addPlain(tr("<td>") + numDet.snameR2 + tr("</td>"));
        addPlain("</tr>\r\n");

        addPlain("<tr><td><p></p></td></tr></table><p></p>\r\n");
        //endChap();
    }

    if (ids.contains(NTPlanesExpression)||ids.contains(NTPlanesExpressionE)||ids.contains(NTPlanesExpressionI)||ids.contains(NTPlanesExpressionM)||ids.contains(NTPlanesExpressionP)) {
        QStringList hlist;
        hlist.append("Mental<br />" + numDet.PERep[3*5+0]);
        hlist.append("Physical<br />" + numDet.PERep[3*5+1]);
        hlist.append("Emotional<br />" + numDet.PERep[3*5+2]);
        hlist.append("Intuitive<br />" + numDet.PERep[3*5+3]);

        QStringList vlist;
        vlist.append("");
        vlist.append("Creative<br />" + numDet.PERep[0*5+4]);
        vlist.append("Dual<br />" + numDet.PERep[1*5+4]);
        vlist.append("Grounded<br />" + numDet.PERep[2*5+4]);

        addPlain("<table class=maindoc border=\"0\" cellspacing=\"3\">");
        addPlain("<tr><td colspan=\"5\"><b>Planes of expression</b></td></tr>\r\n");
        for (int y=0; y<4; y++)
        {
            addPlain("<tr"+dstyle+">");
            for (int x=0; x<5; x++)
            {
                if ((x==0) && (y==0))
                    addPlain(tr("<td width=\"100\">") + tr(" ") + tr("</td>"));
                if (y==0)
                {
                    if (x>0)
                        addPlain(tr("<td width=\"120\" align=\"center\"><b>") + hlist.at(x-1) + tr("</b></td>"));
                } else
                {
                    if (x>0)
                        addPlain(tr("<td align=\"center\">") + numDet.PERep[(y-1)*5+(x-1)] + tr("</td>"));
                    else
                        addPlain(tr("<td align=\"center\"><b>") + vlist.at(y) + tr("</b></td>"));
                }
            }
            addPlain("</tr>\r\n");
        }
        addPlain("<tr><td><p></p></td></tr></table><p></p>\r\n");

    }

    htmlDoc.replace("11", "<b>11</b>");
    htmlDoc.replace("22", "<b>22</b>");
    htmlDoc.replace("33", "<b>33</b>");
    htmlDoc.replace("44", "<b>44</b>");
    htmlDoc.replace("55", "<b>55</b>");
    htmlDoc.replace("66", "<b>66</b>");
    htmlDoc.replace("77", "<b>77</b>");
    htmlDoc.replace("88", "<b>88</b>");
    htmlDoc.replace("99", "<b>99</b>");

    htmlDoc.replace("__shortname__", QString::number(shortName.size()+1));
    htmlDoc.replace("__fullname__", QString::number(fullName.size()+1));



    if (ids.contains(NTPersonalYear)||ids.contains(NTPersonalMonth)||ids.contains(NTPersonalDay)) {
        addPlain("<table class=maindoc border=\"0\" cellspacing=\"3\">");
        addPlain("<tr><td colspan=\"13\" align=\"left\"><b>Personal numbers calendar</b></td></tr>\r\n");
        {
            addPlain("<tr"+dstyle+"><td><b>Years</b></td>");
            int m_cyear=m_cdate.year();
            for (int i=0; i<12; i++)
            {
                int y = m_cyear+i;
                int PersYear = CNumerology::downToDigit(m_PersYear + CNumerology::downToDigit(y));

                addPlain(tr("<td width=\"60\" align=\"center\">") + QString::number(y)+"<br/>("+QString::number(PersYear)+")" + tr("</td>"));
            }
            addPlain("</tr>\r\n");

            addPlain("<tr"+dstyle+"><td><b>Months</b></td>");
            for (int i=0; i<12; i++)
            {
                QDate cd;
                cd = m_cdate;
                cd = cd.addMonths(i);
                int m_CPersYear = CNumerology::downToDigit(m_PersYear + CNumerology::downToDigit(cd.year()));
                int c = CNumerology::downToDigit(m_CPersYear + CNumerology::downToDigit(cd.month()));

                QString ms = QString(mname[cd.month()-1]) +" ("+QString::number(c)+")";
                addPlain(tr("<td width=\"60\" align=\"center\">") + ms + tr("</td>"));
            }
            addPlain("</tr>\r\n");

            addPlain("<tr"+dstyle+"><td><b>Days</b></td>");
            //int m_CPersDate = CNumerology::downToDigit(m_CPersMonth + CNumerology::downToDigit(cdate.day()));
            for (int i=0; i<12; i++)
            {
                QDate cd;
                cd = m_cdate;
                cd = cd.addDays(i);
                int m_CPersYear = CNumerology::downToDigit(m_PersYear + CNumerology::downToDigit(cd.year()));
                int m_CPersMonth = CNumerology::downToDigit(m_CPersYear + CNumerology::downToDigit(cd.month()));
                int c = CNumerology::downToDigit(m_CPersMonth + CNumerology::downToDigit(cd.day()));

                QString ms = QString::number(cd.day()) +" ("+QString::number(c)+")";
                addPlain(tr("<td width=\"60\" align=\"center\">") + ms + tr("</td>"));
            }
            addPlain("</tr>\r\n");
        }
        addPlain("<tr><td><p></p></td></tr></table>\r\n");
    }


    if (ids.contains(NTEssence)||ids.contains(NTTransits)) {
        QString sETFormat="<td width=\"18\" align=\"center\">";
        addPlain("<table class=maindoc border=\"0\" cellspacing=\"3\">");
        addPlain("<tr><td colspan=\"101\" align=\"left\"><b>Essence and Transits Cycles</b></td></tr>\r\n");
        {
            for (int c=0; c<4; c++)
            {
                int astart=c*25, alen=25;
                addPlain("<tr"+dstyle+"><td><b>Age</b></td>");
                for (int i=astart; i<astart+alen; i++)
                {
                    addPlain(sETFormat + QString::number(i) + tr("</td>"));
                }
                addPlain("</tr>\r\n");

                addPlain("<tr"+dstyle+"><td><b>Year</b></td>");
                for (int i=astart; i<astart+alen; i++)
                {
                    addPlain(sETFormat + QString::number((i+m_bdate.year())%100) + tr("</td>"));
                }
                addPlain("</tr>\r\n");


                addPlain("<tr"+dstyle+"><td><b>Physical</b></td>");
                for (int i=astart; i<astart+alen; i++)
                {
                    addPlain(sETFormat + m_sTransit1.at(i) + tr("</td>"));
                }
                addPlain("</tr>\r\n");

                addPlain("<tr"+dstyle+"><td><b>Mental</b></td>");
                for (int i=astart; i<astart+alen; i++)
                {
                    addPlain(sETFormat + m_sTransit2.at(i) + tr("</td>"));
                }
                addPlain("</tr>\r\n");

                addPlain("<tr"+dstyle+"><td><b>Spiritual</b></td>");
                for (int i=astart; i<astart+alen; i++)
                {
                    addPlain(sETFormat + m_sTransit3.at(i) + tr("</td>"));
                }
                addPlain("</tr>\r\n");

                addPlain("<tr"+dstyle+"><td></td>");
                for (int i=astart; i<astart+alen; i++)
                {
                    addPlain(sETFormat + QString::number(m_EssenceS[i]) + tr("</td>"));
                }
                addPlain("</tr>\r\n");

                addPlain("<tr"+dstyle+"><td><b>Essence</b></td>");
                for (int i=astart; i<astart+alen; i++)
                {
                    addPlain(sETFormat + QString::number(m_Essence[i]) + tr("</td>"));
                }
                addPlain("</tr>\r\n");

                addPlain("<tr><td colspan=\"101\" align=\"left\"><br /></td></tr>\r\n");
            }
        }
        addPlain("<tr><td><p></p></td></tr></table>\r\n");
    }
}

bool CNumerology::calculateDynamic(bool useCNC, bool trial, RepSettings *repList, int curRep, int* baseNums, int m_mexpression, int m_hearDesire, int m_mhearDesire, int m_personality, int m_mpers, int m_Maturity, int m_RationalThought, QList<int> *m_KarmicLessons, int m_Balance,
                                   QList<int> *m_HiddenPassion, int m_SubconsciousConfidence, int planesExpression[4], int m_LPEBridge, int m_LPEBridgeDet, int m_HDPBridge, int m_HDPBridgeDet)
{
    const QList<repItem> *Numerology = &repList->at(curRep).Numerology;
    const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

    bool showImages=true;
    if (repList->at(curRep).extendedSettings.value("RTNumerology-hide-images", "FALSE")=="TRUE")
       showImages=false;

    QString as[4];
    as[0] = "0-" + QString::number(36-downToDigit(m_LifePath)),
    as[1] = QString::number(36-downToDigit(m_LifePath)) + "-" + QString::number(36+9-downToDigit(m_LifePath)),
    as[2] = QString::number(36+9-downToDigit(m_LifePath)) + "-" + QString::number(36+9*2-downToDigit(m_LifePath)),
    as[3] = QString::number(36+9*2-downToDigit(m_LifePath)) + "-E";

    if (useCNC)
        startDoc("Chaldean Numerology Report");
    else
        startDoc("Numerology Report");

   // bool trialMes = false;

    int m_spNum[4];
    int m_pNum[4];
    s_spNum(m_spNum, m_pNum);

    QList<int> allIds;
    for (int i = 0; i < Numerology->size(); i++)
        allIds.append(Numerology->at(i).id);

    for (int i = 0; i < Numerology->size(); i++)
    {
        if (trial == true && i > 3)
        {
            startChap();
#ifndef VER_FREE
            addPar("<br />" + numerInfo[9] + "<br />");
#endif
            endChap();
            break;
        }

        const repItem *item = &Numerology->at(i);
        if (item->id == NTHeader)
        {
            if (item->isContentDefault)
            {
                if (useCNC)
                {
                    startChap();
                    addHead("Chaldean Numerology Chart for");
                } else
                {
                    startChap();
                    addHead("Numerology Chart for");
                }
                endChap();
            }
            else
            {
                startChap();
                addHead(item->headerName, item->headerColor, item->headerFont);
                endChap();
            }

            startChap();
            addHead(fullName + " (" + shortName + "), " + QString(mname[m_bdate.month()-1]) + " " + QString::number(m_bdate.day()) + " " + QString::number(m_bdate.year()),
                    item->contentColor, item->contentFont);

#ifndef VER_MOBILE
            addPar(getCalculateHeaderC(baseNums, m_spNum, m_pNum, useCNC, allIds), item->contentColor, item->contentFont);
			addPar("");
#endif
            endChap();
            QString txt;
            if (item->contentFont.underline())
                txt += "<u>";
            if (item->contentFont.italic())
                txt += "<i>";

            txt += "<font style=\"";
            int fsz = item->contentFont.pixelSize();
            if (fsz>0)
                txt += "font-size: " + QString::number(fsz) + "px;";
            if (item->contentFont.family().size()>0)
                txt += " face:" + item->contentFont.family() + ";";
            txt += " color:" + item->contentColor.name() + ";\">";
            addPlain(txt);
#ifndef VER_MOBILE
            calculateGenTable(allIds);
#endif
            txt = "</font>";
            if (item->contentFont.italic())
                txt += "</i>";
            if (item->contentFont.underline())
                txt += "</u>";
            addPlain(txt);
        }
        else
        if (item->id == NTLifePath)
        {
            //startChap();
            addPar("<br />");
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res01.jpg");
            addStandItem("Life Path Number", numDet.lifePath, numerLifePath[0], numerLifePath[m_LifePath], m_LifePath, item, !showImages);
            //endChap();
        }
        else
        if (item->id == NTBrithday)
        {
            addPar("<br />");
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res02.jpg");
            addStandItem("Birthday Number", QString::number(m_bdate.day()), numerBirthDate[0], numerBirthDate[m_bdate.day()], m_bdate.day(), item, !showImages);
            //endChap();
        }
        else
        if (item->id == NTExpression)
        {
            addPar("<br />");
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res03.jpg");
            addStandItem("Expression Number", numDet.expression, numerExpr[0], numerExpr[m_expression], m_expression, item, !showImages);
            //endChap();
        }
        else
        if (item->id == NTMinorExpression)
        {
            addPar("<br />");
            //if (!trial)
            {
                //startChap();
                parImageCode = "";
                addStandItem("Minor Expression Number", numDet.mexpression, numerExprM[0], numerExprM[m_mexpression] + "<br />", m_mexpression, item, !showImages);
                //endChap();
            }
        }
        else
        if (item->id == NTHeartDesire)
        {
            addPar("<br />");
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res04.jpg");
            addStandItem("Heart Desire Number", numDet.heartDesire, numerHeartDesire[0], numerHeartDesire[m_hearDesire] + "<br />", m_hearDesire, item, !showImages);
            //endChap();
        }
        else
        if (item->id == NTMinorHeartDesire)
        {
            addPar("<br />");
            //startChap();
            parImageCode = "";
            addStandItem("Minor Heart Desire Number", numDet.mheartDesire, numerMinorHeartDesire[0], numerMinorHeartDesire[m_mhearDesire] + "<br />", m_mhearDesire, item, !showImages);
            //endChap();
        }
        else
        if (item->id == NTCompound && useCNC)
        {
            addPar("<br />");
            if (numDet.compound >= 10 && numDet.compound <= 99)
            {
                bool isWrite = true;

                if ((numerCompoundNumbers[numDet.compound].isEmpty() || numerCompoundNumbers[numDet.compound] == " ")
                        && (numerCompoundNumbers[0].isEmpty() || numerCompoundNumbers[0] == " ")
                    && item->content.at(numDet.compound).isEmpty() && item->content.at(0).isEmpty())
                {
                    isWrite = false;
                }

                if (isWrite)
                {
                    //startChap();
                    parImageCode = "";
                    addStandItem("Compound Number", QString::number(numDet.compound), numerCompoundNumbers[0], numerCompoundNumbers[numDet.compound] + "<br />", numDet.compound, item, !showImages);
                    //endChap();
                }
            }
        }
		else if (item->id == NTPlanesExpression)
		{
            addPar("<br />");
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res03.jpg");
            addStandItem("Planes of Expression", "", numerPlanesOfExpression[0], "", -1, item, !showImages);
		}
		else if (item->id == NTPlanesExpressionM && (planesExpression[0]!=0))
		{
            addStandItem("Planes of Expression Mental", numDet.PERep[3*5+0], QString(), numerPlanesOfExpressionM[planesExpression[0]], planesExpression[0], item, !showImages);
		}
		else if (item->id == NTPlanesExpressionP && (planesExpression[1]!=0))
		{
            addStandItem("Planes of Expression Physical", numDet.PERep[3*5+1], QString(), numerPlanesOfExpressionP[planesExpression[1]], planesExpression[1], item, !showImages);
		}
		else if (item->id == NTPlanesExpressionE && (planesExpression[2]!=0))
		{
            addStandItem("Planes of Expression Emotional", numDet.PERep[3*5+2], QString(), numerPlanesOfExpressionE[planesExpression[2]], planesExpression[2], item, !showImages);
		}
		else if (item->id == NTPlanesExpressionI && (planesExpression[3]!=0))
		{
            addStandItem("Planes of Expression Intuitive", numDet.PERep[3*5+3], QString(), numerPlanesOfExpressionI[planesExpression[3]], planesExpression[3], item, !showImages);
		}
        else
        if (item->id == NTPersonality)
        {
            addPar("<br />");
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res05.jpg");
            addStandItem("Personality Number", numDet.personality, numerPersonality[0], numerPersonality[m_personality] + "<br />", m_personality, item, !showImages);
            //endChap();
        }
        else
        if (item->id == NTMinorPersonality)
        {
            if (m_personality != m_mpers)
            {
                addPar("<br />");
                //startChap();
                parImageCode = "";
                addStandItem("Minor Personality Number", numDet.mpersonality, QString(), numerPersonality[m_mpers] + "<br />",  m_mpers, item, !showImages);
                //endChap();
            }
        }
        else
        if (item->id == NTKarmicDebts)
        {
            if ((baseNums[13]!=0) || (baseNums[14]!=0) || (baseNums[16]!=0) || (baseNums[19]!=0))
            {
                addPar("<br />");
                //startChap();
                if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                    addImage("res06.jpg");
                addStandItem("Karmic Debts Numbers", QString(), numerKarmDebts[0] + "<br />", QString(), -1, item, !showImages);
                //endChap();
            }
//        }
//        else
//        if (item->id == NTKarmicDebt)
//        {
            if (baseNums[13]!=0)
            {
                //startChap();
                parImageCode = "";
                addStandItem("Karmic Debt Number:", "13", QString(), numerKarmDebts[13] + "<br />", 13, item, !showImages);
                //endChap();
            }
            if (baseNums[14]!=0)
            {
                //startChap();
                parImageCode = "";
                addStandItem("Karmic Debt Number:", "14", QString(), numerKarmDebts[14] + "<br />", 14, item, !showImages);
                //endChap();
            }
            if (baseNums[16]!=0)
            {
                //startChap();
                parImageCode = "";
                addStandItem("Karmic Debt Number:", "16", QString(), numerKarmDebts[16] + "<br />", 16, item, !showImages);
                //endChap();
            }
            if (baseNums[19]!=0)
            {
                //startChap();
                parImageCode = "";
                addStandItem("Karmic Debt Number:", "19", QString(), numerKarmDebts[19] + "<br />", 19, item, !showImages);
                //endChap();
            }
        }
        else
        if (item->id == NTMaturity)
        {
            addPar("<br />");
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res07.jpg");
            addStandItem("Maturity Number", numDet.maturity, numerMaturity[0], numerMaturity[m_Maturity] + "<br />", m_Maturity, item, !showImages);
            //endChap();
        }
        else
        if (item->id == NTRationalThought)
        {
            addPar("<br />");
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res08.jpg");
            addStandItem("Rational Thought Number", numDet.rationalThought, numerRationalThoughts[0], numerRationalThoughts[m_RationalThought] + "<br />", m_RationalThought, item, !showImages);
            //endChap();
        }
        else
        if (item->id == NTKarmicLessons)
        {
            if (m_KarmicLessons->size() > 0)
            {
                addPar("<br />");
                //startChap();
                if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                    addImage("res09.jpg");
                addStandItem("Karmic Lessons Number", numDet.karmicLessons, numerKarmicLessons[0], QString(), -1, item, !showImages);
                //endChap();

                for (int a = 0; a < m_KarmicLessons->size(); a++)
                {
                    //startChap();
                    if (item->isContentDefault)
                    {
                        startChap();
                        addPar(numerKarmicLessons[m_KarmicLessons->at(a)]);
                        endChap();
                    }
                    else
                    {
                        startChap();
                        addPar(numerKarmicLessons[m_KarmicLessons->at(a)], item, m_KarmicLessons->at(a));
                    }
                    //endChap();
                }
            }
        }
        else
        if (item->id == NTBalanceNumber)
        {
            addPar("<br />");
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res10.jpg");
            addStandItem("Balance Number", numDet.balance, numerBalance[0], numerBalance[m_Balance] + "<br />", m_Balance, item, !showImages);
            //endChap();
        }
        else
        if (item->id == NTHiddenPassion)
        {
            if (m_HiddenPassion->size() > 0)
            {
                addPar("<br />");
                //startChap();
                if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                    addImage("res11.jpg");
                addStandItem("Hidden Passion Number", numDet.hiddenPassion, numerHiddenPassion[0], QString(), -1, item, !showImages);
                //endChap();
                for (int a=0; a<m_HiddenPassion->size(); a++)
                {
                    //startChap();
                    if (item->isContentDefault)
                    {
                        startChap();
                        addPar(numerHiddenPassion[m_HiddenPassion->at(a)]);
                        endChap();
                    }
                    else
                    {
                        startChap();
                        addPar(numerHiddenPassion[m_HiddenPassion->at(a)], item, m_HiddenPassion->at(a));
                    }
                    //endChap();
                }
            }
        }
        else
        if (item->id == NTSubconsciousConfidence)
        {
            addPar("<br />");
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res12.jpg");
            addStandItem("Subconscious Confidence Number", numDet.subconsciousConfidence, numerSubconsConfid[0], numerSubconsConfid[m_SubconsciousConfidence] + "<br />", m_SubconsciousConfidence, item, !showImages);
            //endChap();
        }
        else
        if (item->id == NTPinnacles)
        {
            startChap();
            addPar("<br />");
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res13.jpg");
            addHead("Pinnacles Numbers " + numDet.pinnacles, item->headerColor, item->headerFont, item->imgList.at(0));
            addHead("Age span for life cycles " + numDet.ageSpan, item->headerColor, item->headerFont);
            if (item->isContentDefault)
            {
                addPar(numerPinnacles[0]);
                endChap();
            }
            else
            {
                addPar(numerPinnacles[0], item, 0);
            }
            //endChap();

            for (int i = 0; i < 4; i++)
            {
                //startChap();
                if (item->isContentDefault)
                {
                    startChap();
                    addPar("<b>Life cycle " + QString::number(i + 1) + " (" + as[i] + ")</b> " + numerPinnacles[m_pNum[i]]);
                    endChap();
                }
                else
                {
                    startChap();
                    addPar("<b>Life cycle " + QString::number(i + 1) + " (" + as[i] + ")</b> ", item->contentColor, item->contentFont);
                    endChap();
                    startChap();
                    addPar(numerPinnacles[m_pNum[i]], item, m_pNum[i]);
                }
                //endChap();
            }
        }
        else
        if (item->id == NTChallenges)
        {
            startChap();
            addPar("<br />");
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res14.jpg");
            addHead("Challenges Numbers " + numDet.challenges, item->headerColor, item->headerFont, item->imgList.at(0));
            if (item->isContentDefault)
            {
                addPar(numerChallenges[0] + "<br />");
                endChap();
            }
            else
            {
                addPar(numerChallenges[0] + "<br />", item, 0);
                endChap();
            }
            //endChap();

            int m_cNum[4];
            m_cNum[0] = m_cNum1;
            m_cNum[1] = m_cNum2;
            m_cNum[2] = m_cNum3;
            m_cNum[3] = m_cNum4;

            for (int i = 0; i < 4; i++)
            {
                startChap();
                if (item->isContentDefault)
                {
                    addPar("<b>Life cycle " + QString::number(i + 1) + " (" + as[i] + ")</b> " + numerChallenges[m_cNum[i]+1]);
                    endChap();
                }
                else
                {
                    addPar("<b>Life cycle " + QString::number(i + 1) + " (" + as[i] + ")</b> ", item->contentColor, item->contentFont);
                    endChap();
                    startChap();
                    addPar(numerChallenges[m_cNum[i]+1], item, m_cNum[i]+1);
                }
                //endChap();
            }
        }
        else if (item->id == NTBridgeLPE && (m_LPEBridge<9) && (m_LPEBridge>=0))
        {
            addPar("<br />");
            //if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                //addImage("res10.jpg");
            addStandItem("Bridge Life Path - Expression", QString::number(m_LPEBridge), numerBridgeLPE[0], numerBridgeLPE[m_LPEBridge+1], m_LPEBridge+1, item, !showImages);
        }
        else if (item->id == NTBridgeLPEDet && (m_LPEBridgeDet<100) && (m_LPEBridgeDet>=0))
        {
            addPar("<br />");
            addStandItem("", QString(), QString(), numerBridgeLPEDet[m_LPEBridgeDet], m_LPEBridgeDet, item, !showImages);
        }
        else if (item->id == NTBridgeHDP && (m_HDPBridge<9) && (m_HDPBridge>=0))
        {
            addPar("<br />");
            //if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                //addImage("res10.jpg");
            addStandItem("Bridge Heart's Desire - Personality", QString::number(m_HDPBridge), numerBridgeHDP[0], numerBridgeHDP[m_HDPBridge+1], m_HDPBridge+1, item, !showImages);
        }
        else if (item->id == NTBridgeHDPDet && (m_HDPBridgeDet<100) && (m_HDPBridgeDet>=0))
        {
            addPar("<br />");
            addStandItem("", QString(), QString(), numerBridgeHDPDet[m_HDPBridgeDet], m_HDPBridgeDet, item, !showImages);
        }
        else
        if (item->id == NTPersonalYear)
        {
            addPar("<br />");
            int m_CPersYear = downToDigit(m_PersYear + downToDigit(m_cyear));
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res15.jpg");
            addStandItem("Personal Year Number:", QString::number(m_CPersYear), numerPersYear[0], QString(), -1, item, !showImages);
            //endChap();
            for (int a=0; a<9; a++)
            {
                startChap();
                int y = m_cyear+a;
                int PersYear = downToDigit(m_PersYear + downToDigit(y));
                if (item->isContentDefault)
                {
                    addPar(QString::number(y) + QString(" (") +QString::number(PersYear) + QString("). ") + numerPersYear[PersYear]);
                    endChap();
                }
                else
                {
                    addPar(QString::number(y) + QString(" (") +QString::number(PersYear) + QString("). "), item->contentColor, item->contentFont);
                    endChap();
                    startChap();
                    addPar(numerPersYear[PersYear], item, PersYear);
                }
                //endChap();
            }
        }
        else
        if (item->id == NTPersonalMonth)
        {
            addPar("<br />");
            int m_CPersYear = downToDigit(m_PersYear + downToDigit(m_cyear));
            int m_CPersMonth = downToDigit(m_CPersYear + downToDigit(m_cdate.month()));
            //startChap();
            if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                addImage("res15.jpg");
            addStandItem("Personal Month Number:", QString::number(m_CPersMonth), numerPersDate[0], QString(), -1, item, !showImages);
            //endChap();
            for (int a=0; a<12; a++)
            {
                startChap();
                QDate cd;
                cd = m_cdate;
                cd = cd.addMonths(a);
                int m_CPersYear = downToDigit(m_PersYear + downToDigit(cd.year()));
                int c = downToDigit(m_CPersYear + downToDigit(cd.month()));
                if (item->isContentDefault)
                {
                    addPar(QString::number(cd.month()) + "/" + QString::number(cd.year()) + QString(" (") +QString::number(c) + QString("). ") + numerPersDate[c]);
                    endChap();
                }
                else
                {
                    addPar(QString::number(cd.month()) + "/" + QString::number(cd.year()) + QString(" (") +QString::number(c) + QString("). "), item->contentColor, item->contentFont);
                    endChap();
                    startChap();
                    addPar(numerPersDate[c], item, c);
                }
                //endChap();
            }
        }
        else
        if (item->id == NTPersonalDay)
        {
            addPar("<br />");
            int m_CPersYear = downToDigit(m_PersYear + downToDigit(m_cyear));
            int m_CPersMonth = downToDigit(m_CPersYear + downToDigit(m_cdate.month()));
            int m_CPersDate = downToDigit(m_CPersMonth + downToDigit(m_cdate.day()));
            //startChap();
            //if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                //addImage("res15.jpg");
            addStandItem("Personal Day Number:", QString::number(m_CPersDate), QString(), QString(), -1, item, !showImages);
            //endChap();
            for (int a=0; a<9; a++)
            {
                startChap();
                QDate cd;
                cd = m_cdate;
                cd = cd.addDays(a);
                int m_CPersYear = downToDigit(m_PersYear + downToDigit(cd.year()));
                int m_CPersMonth = downToDigit(m_CPersYear + downToDigit(cd.month()));
                int c = downToDigit(m_CPersMonth + downToDigit(cd.day()));
                if (item->isContentDefault)
                {
                    addPar(QString::number(cd.month()) + "/" + QString::number(cd.day()) + "/" + QString::number(cd.year()) + QString(" (") +QString::number(c) + QString("). ") + numerPersDate[c]);
                    endChap();
                }
                else
                {
                    addPar(QString::number(cd.month()) + "/" + QString::number(cd.day()) + "/" + QString::number(cd.year()) + QString(" (") +QString::number(c) + QString("). "), item->contentColor, item->contentFont);
                    endChap();
                    startChap();
                    addPar(numerPersDate[c], item, c);
                }
                //endChap();
            }
        }
        else
        if (item->id == NTTransits)
        {
            int age=m_cdate.year() - m_bdate.year()-1;
            if (m_cdate.month()>m_bdate.month())
                age++;
            else if ((m_cdate.month()==m_bdate.month())&&(m_cdate.day()>=m_bdate.day()))
                age++;
            if ((age>=0) && (age<100))
            {
                addPar("<br />");
                //startChap();
                //if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                    //addImage("res15.jpg");
                addStandItem("Transits for age", QString::number(age), numerTransit[0], QString(), -1, item, !showImages);
                //endChap();

                if (item->isContentDefault)
                {
                    startChap();
                    addPar("Physical");
                    int ci=m_sTransit1.at(age).toAscii()-'A'+1;
                    if ((ci>=1) && (ci<=26))
                        addPar(numerTransit[ci]);
                    endChap();

                    startChap();
                    addPar("Mental");
                    ci=m_sTransit2.at(age).toAscii()-'A'+1;
                    if ((ci>=1) && (ci<=26))
                        addPar(numerTransit[ci]);
                    endChap();

                    startChap();
                    addPar("Spiritual");
                    ci=m_sTransit3.at(age).toAscii()-'A'+1;
                    if ((ci>=1) && (ci<=26))
                        addPar(numerTransit[ci]);
                    endChap();
                }
                else
                {
                    startChap();
                    addPar("Physical", item->contentColor, item->contentFont);
                    endChap();
                    int ci=m_sTransit1.at(age).toAscii()-'A'+1;
                    if ((ci>=1) && (ci<=26))
                    {
                        startChap();
                        addPar(numerTransit[ci], item, ci);
                    }


                    startChap();
                    addPar("Mental", item->contentColor, item->contentFont);
                    endChap();
                    ci=m_sTransit2.at(age).toAscii()-'A'+1;
                    if ((ci>=1) && (ci<=26))
                    {
                        startChap();
                        addPar(numerTransit[ci], item, ci);
                    }

                    startChap();
                    addPar("Spiritual", item->contentColor, item->contentFont);
                    endChap();
                    ci=m_sTransit3.at(age).toAscii()-'A'+1;
                    if ((ci>=1) && (ci<=26))
                    {
                        startChap();
                        addPar(numerTransit[ci], item, ci);
                    }

                }
            }
        }
        else
        if (item->id == NTEssence)
        {
            int age=m_cdate.year() - m_bdate.year()-1;
            if (m_cdate.month()>m_bdate.month())
                age++;
            else if ((m_cdate.month()==m_bdate.month())&&(m_cdate.day()>=m_bdate.day()))
                age++;
            if ((age>=0) && (age<100))
            {
                addPar("<br />");
                //startChap();
                //if (showImages && (item->isContentDefault || (item->extendedSettings.value("hide-image","FALSE")!="TRUE")))
                   //addImage("res15.jpg");
                addStandItem("Essence for age", QString::number(age) + ": " + QString::number(m_Essence[age]) + "/" + QString::number(m_EssenceS[age]), numerEssence[0], numerEssence[m_Essence[age]], m_Essence[age], item, !showImages);
                //endChap();
            }
        }
        else
        if (item->id == NTCustomText)
        {
            addStandItem("Custom Text", QString(), numerCustomText[0], QString(), -1, item, !showImages);
        }
//        else
//        if (item->id == NTCompatible)
//        {
//            startChap();
//            //addImage("res06.jpg");
//            addStandItem("Compatible Number", QString(), numerCompNumbers[0], QString(), -1, item);
//            endChap();
//        }

    }
    endDoc();

    return true;
}

bool calculateDynamic()
{
    return true;
}

bool CNumerology::calculate(QString sfullName, QString sshortName, QVector<int> &fnvw, QVector<int> &snvw, bool useCNC, QDate bdate, QDate cdate, bool trial, RepSettings *repList, int curRep, int pmode, bool skipReport)
{
    int select = curRep - 1;

    if (select >= 0)
    {
        if (repList->size() > 0)
        {
            if (repList->size() <= select)
                select = -1;
        }
        else
        {
            select = -1;
        }
    }



	if (useCNC)
		setCalcMode(1);
	else
		setCalcMode(0);

	fullName = sfullName.toUpper();
	shortName = sshortName.toUpper();

    const char *mname[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

    int baseNums[128];
    int nbaseNums[128];
    int nsbaseNums[128];
    int snum=0;
	int planesExpression[4];

	resetData();

	m_cdate = cdate;
	m_bdate = bdate;
	m_cyear = cdate.year();

	int dday = downToBase(bdate.day());
	int dmonth = downToBase(bdate.month());
	int dyear = downToBase(bdate.year());

    int lp1=0, lp2=0, lp3=0;
    int snum1=0, snum2=0, snum3=0;
    m_LifePath=0;
    if (m_GlobSettings.calcLP3)
    {
        resetData();
        snum3 = 0;
        snum3 += bdate.day()/10;
        snum3 += bdate.day()%10;

        snum3 += bdate.month()/10;
        snum3 += bdate.month()%10;

        snum3 += bdate.year()%10;
        snum3 += (bdate.year()/10)%10;
        snum3 += (bdate.year()/100)%10;
        snum3 += (bdate.year()/1000)%10;

        lp3 = downToBase(snum3);
    }
    if (m_GlobSettings.calcLP2)
    {
        resetData();
        snum2 = bdate.day()+bdate.month()+bdate.year();
        lp2 = downToBase(snum2);
    }
    if ((m_GlobSettings.calcLP1) || ((!m_GlobSettings.calcLP1) && (!m_GlobSettings.calcLP2) && (!m_GlobSettings.calcLP3)))
    {
        resetData();
        snum1 = dday+dmonth+dyear;
        lp1 = downToBase(snum1);
    }

    if ((m_LifePath<=0) && (lp1!=0))
    {
        m_LifePath = lp1;
        snum = snum1;
    }
    if ((m_LifePath<=0) && (lp2!=0))
    {
        m_LifePath = lp2;
        snum = snum2;
    }
    if ((m_LifePath<=0) && (lp3!=0))
    {
        m_LifePath = lp3;
        snum = snum3;
    }

    if (isMasterNumber(lp1, m_MasterNumberModeID) && (!isMasterNumber(m_LifePath, m_MasterNumberModeID)))
    {
		resetData();
		int dday = downToBase(bdate.day());
		int dmonth = downToBase(bdate.month());
		int dyear = downToBase(bdate.year());
		int snum1 = dday+dmonth+dyear;
		downToBase(snum1);


		m_LifePath = lp1;
        snum = snum1;
    }
    if (isMasterNumber(lp2, m_MasterNumberModeID) && (!isMasterNumber(m_LifePath, m_MasterNumberModeID)))
    {
		resetData();
        /*int dday = */downToBase(bdate.day());
        /*int dmonth = */downToBase(bdate.month());
        /*int dyear = */downToBase(bdate.year());
        int snum2 = bdate.day()+bdate.month()+bdate.year();
		downToBase(snum2);

        m_LifePath = lp2;
        snum = snum2;
    }
    if (isMasterNumber(lp3, m_MasterNumberModeID) && (!isMasterNumber(m_LifePath, m_MasterNumberModeID)))
    {
		resetData();
        /*int dday = */downToBase(bdate.day());
        /*int dmonth = */downToBase(bdate.month());
        /*int dyear = */downToBase(bdate.year());
		int snum3 = 0;
		snum3 += bdate.day()/10;
		snum3 += bdate.day()%10;

		snum3 += bdate.month()/10;
		snum3 += bdate.month()%10;

		snum3 += bdate.year()%10;
		snum3 += (bdate.year()/10)%10;
		snum3 += (bdate.year()/100)%10;
		snum3 += (bdate.year()/1000)%10;

		downToBase(snum3);


        m_LifePath = lp3;
        snum = snum3;
    }

    if (snum==snum1)
    {
        int nsum = bdate.day()/10 + bdate.day()%10;
        if (nsum>9)
            numDet.lifePath = QString::number(dday) + "/" + QString::number(nsum);
        else
            numDet.lifePath = QString::number(dday);

        numDet.lifePath += "+" + QString::number(dmonth);

        nsum = bdate.year()%10;
        nsum += (bdate.year()/10)%10;
        nsum += (bdate.year()/100)%10;
        nsum += (bdate.year()/1000)%10;

        if (nsum>9)
            numDet.lifePath += "+" + QString::number(dyear) + "/" + QString::number(nsum);
        else
            numDet.lifePath += "+" + QString::number(dyear);

        numDet.lifePath += "=" + QString::number(snum);
    } else
        numDet.lifePath = QString::number(snum);
	if (m_LifePath != snum)
    {
		numDet.lifePath += "=" + QString::number(m_LifePath);
        snum = downOnce(snum);
        if ((snum>9) && (m_LifePath != snum))
            numDet.lifePath += "/" + QString::number(snum);
    }
	numDet.birthDay = QString::number(bdate.day());
	if (dday != bdate.day())
		numDet.birthDay += " (" + QString::number(dday) + ")";

	downToBase(bdate.day()); // KD for birth day

    for(int a=0; a<128; a++)
		baseNums[a] = m_BaseNums[a];
	//baseNums[m_LifePath]--;


	m_cNum1 = abs(downToDigit(dday) - downToDigit(dmonth));
	m_cNum2 = abs(downToDigit(dday) - downToDigit(dyear));
	m_cNum3 = abs(m_cNum1-m_cNum2);
	m_cNum4 = abs(downToDigit(dyear) - downToDigit(dmonth));

	int m_spNum1 = downToBase(downToBase(dday)+downToBase(dmonth));
	int m_spNum2 = downToBase(downToBase(dday)+downToBase(dyear));
	int m_spNum3 = downToBase(m_spNum1+m_spNum2);
	int m_spNum4 = downToBase(downToBase(dyear)+downToBase(dmonth));
	int m_pNum1 = downToDigit(m_spNum1);
	int m_pNum2 = downToDigit(m_spNum2);
	int m_pNum3 = downToDigit(m_spNum3);
	int m_pNum4 = downToDigit(m_spNum4);

	m_PersYear = downToDigit(downToDigit(dday) + downToDigit(dmonth));

	numDet.persYear = QString::number(downToDigit(m_PersYear+downToDigit(cdate.year())));
	numDet.challenges = QString::number(m_cNum1) + ", " + QString::number(m_cNum2) + ", " + QString::number(m_cNum3) + ", " + QString::number(m_cNum4);
	numDet.pinnacles = "";
	if (m_pNum1!=m_spNum1)
		numDet.pinnacles += QString::number(m_pNum1) + "/" + QString::number(m_spNum1) + ", ";
	else
		numDet.pinnacles += QString::number(m_pNum1) + ", ";
	if (m_pNum2!=m_spNum2)
		numDet.pinnacles += QString::number(m_pNum2) + "/" + QString::number(m_spNum2) + ", ";
	else
		numDet.pinnacles += QString::number(m_pNum2) + ", ";
	if (m_pNum3!=m_spNum3)
		numDet.pinnacles += QString::number(m_pNum3) + "/" + QString::number(m_spNum3) + ", ";
	else
		numDet.pinnacles += QString::number(m_pNum3) + ", ";
	if (m_pNum4!=m_spNum4)
		numDet.pinnacles += QString::number(m_pNum4) + "/" + QString::number(m_spNum4);
	else
		numDet.pinnacles += QString::number(m_pNum4);


	numDet.ageSpan = "0-" + QString::number(36-downToDigit(m_LifePath))
		+ "-" + QString::number(36+9-downToDigit(m_LifePath))
		+ "-" + QString::number(36+9*2-downToDigit(m_LifePath))
		+ "-E";

    QString as1 = "0-" + QString::number(36-downToDigit(m_LifePath)),
            as2 = QString::number(36-downToDigit(m_LifePath)) + "-" + QString::number(36+9-downToDigit(m_LifePath)),
            as3 = QString::number(36+9-downToDigit(m_LifePath)) + "-" + QString::number(36+9*2-downToDigit(m_LifePath)),
            as4 = QString::number(36+9*2-downToDigit(m_LifePath)) + "-E";

    numDet.lpCycles = QString::number(downToDigit(dmonth))
            + ", " + QString::number(downToDigit(dday))
            + ", " + QString::number(downToDigit(dyear));

    char lpspan1[10] = { 0, 26, 25, 24, 23, 31, 30, 29, 28, 27 };
    char lpspan2[10] = { 0, 53, 52, 51, 59, 58, 57, 56, 55, 54 };
    numDet.lpAgeSpan = "0-" + QString::number(lpspan1[downToDigit(m_LifePath)])
            + ","+ QString::number(lpspan1[downToDigit(m_LifePath)]+1) + "-" + QString::number(lpspan2[downToDigit(m_LifePath)])
            + ","+ QString::number(lpspan2[downToDigit(m_LifePath)]+1) + "-E";

	clear();
	QList<int> nnums;
	calcNamesNumbers(fullName, nnums);
	int fnnum = nnums[0];
    int sum=0;
	for(int a=0; a<nnums.size(); a++)
	{
        if (m_GlobSettings.calcNameWD)
        {
            sum += downToBase(nnums[a]); //downToBase(nnums[a], baseNums);
        } else
            sum += nnums[a];
	}
    for(int a=0; a<128; a++)
		nbaseNums[a] = m_BaseNums[a];
    m_expression = downToBase(sum); downToBase(sum, baseNums);

	numDet.expression = QString::number(m_expression);
	if (sum != m_expression)
		numDet.expression += "/" + QString::number(sum);

    numDet.compound = sum;

	numDet.fnameR = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR += "+";

		int v = nnums[a];
        int vb = downToBase(nnums[a]);
        numDet.fnameR += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR += '/' + QString::number(v);
	}
	numDet.fnameR += "=" + QString::number(sum);
	if (sum != m_expression)
		numDet.fnameR += "=" + QString::number(m_expression);


	numDet.karmicLessons = "";
	QList<int> m_KarmicLessons;
	m_KarmicLessons.clear();

	int letmap[10];
	for (int a=0; a<10; a++)
		letmap[a] = 0;
	for (int a=0; a<fullName.size(); a++)
	{
        int v = downToDigit(letterToNumber(fullName.at(a)));
		letmap[v]++;
	}
	int lmnum=0;
	int lmmax=0;
	for (int a=1; a<10; a++)
	{
		if (letmap[a]>lmmax)
			lmmax = letmap[a];

		if (letmap[a]!=0)
		{
			lmnum++;
		} else
		{
			if (numDet.karmicLessons.size()!=0)
				numDet.karmicLessons += ", ";
			numDet.karmicLessons += QString::number(a);
			m_KarmicLessons.append(a);
		}
	}
	if (numDet.karmicLessons.size()==0)
		numDet.karmicLessons = "-";

	QList<int> m_HiddenPassion;
	m_HiddenPassion.clear();
	numDet.hiddenPassion = "";
	for (int a=1; a<10; a++)
	{
		if (letmap[a]==lmmax)
		{
			if (numDet.hiddenPassion.size()!=0)
				numDet.hiddenPassion += ", ";
			numDet.hiddenPassion += QString::number(a);
			m_HiddenPassion.append(a);
		}
	}

	numDet.subconsciousConfidence = QString::number(lmnum);
	int m_SubconsciousConfidence = lmnum;


	nnums.clear();
    calcNamesNumbersP(fullName, nnums, true, fnvw, pmode);
	sum=0;
	for(int a=0; a<nnums.size(); a++)
        if (m_GlobSettings.calcNameWD) {
            sum += downToBase(nnums[a]); //downToBase(nnums[a], baseNums);
        } else
            sum += nnums[a];
    int m_hearDesire = downToBase(sum); downToBase(sum, baseNums);

	numDet.fnameR1 = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR1 += "+";

		int v = nnums[a];
        int vb = downToBase(nnums[a]);
        numDet.fnameR1 += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR1 += '/' + QString::number(v);
	}
	numDet.fnameR1 += "=" + QString::number(sum);
	if (sum != m_hearDesire)
		numDet.fnameR1 += "=" + QString::number(m_hearDesire);

	numDet.heartDesire = QString::number(m_hearDesire);
	if (sum != m_hearDesire)
		numDet.heartDesire += "/" + QString::number(sum);



	nnums.clear();
    calcNamesNumbersP(fullName, nnums, false, fnvw, pmode);
	sum=0;
	for(int a=0; a<nnums.size(); a++)
        if (m_GlobSettings.calcNameWD) {
            sum += downToBase(nnums[a]); //downToBase(nnums[a], baseNums);
        } else
            sum += nnums[a];
    int m_personality = downToBase(sum); downToBase(sum, baseNums);

	numDet.fnameR2 = "=";
	for(int a=0; a<nnums.size(); a++)
	{
		if (a!=0)
			numDet.fnameR2 += "+";

		int v = nnums[a];
        int vb = downToBase(nnums[a]);
        numDet.fnameR2 += QString::number(downToBase(v));
		if (v!=vb)
			numDet.fnameR2 += '/' + QString::number(v);
	}
	numDet.fnameR2 += "=" + QString::number(sum);
	if (sum != m_personality)
		numDet.fnameR2 += "=" + QString::number(m_personality);

	numDet.personality = QString::number(m_personality);
	if (sum != m_personality)
		numDet.personality += "/" + QString::number(sum);


	numDet.fnameN1 = "";
	numDet.fnameN2 = "";
	for (int a=0; a<fullName.size(); a++)
	{
		int v = isVow(fullName, a, pmode);
        if (a<fnvw.size())
        {
            if (fnvw.at(a)==1)
                v=1;
            else if (fnvw.at(a)==2)
                v=-1;
        }

        if (v>0)
		{
            numDet.fnameN1 += QString::number(downToDigit(letterToNumber(fullName[a])));
			numDet.fnameN2 += " ";
		} else if (v<0)
		{
            numDet.fnameN2 += QString::number(downToDigit(letterToNumber(fullName[a])));
			numDet.fnameN1 += " ";
		} else
		{
			numDet.fnameN1 += " ";
			numDet.fnameN2 += " ";
		}
	}


	int lvals[16];
	for (int a=0; a<16; a++)
		lvals[a] = 0;
	for (int a=0; a<fullName.size(); a++)
	{
		//int i = getPEIndex(fullName.at(a));
		int i = getPEIndex(fullName.at(a));
		if ((i>=0) && (i<12))
		{
			m_PELet[i]++;
            lvals[i] += downToDigit(letterToNumber(fullName.at(a)));
		}

		i = fullName.at(a).toUpper().toAscii() - 'A';
        if ((i>=0) && (i<128))
			m_Let[i]++;
	}
	for (int a=0; a<12; a++)
		lvals[a] = statDownToBase(lvals[a]);


	for (int a=0; a<20; a++)
	{
		numDet.PE[a] = "";
		numDet.PENum[a] = "";
		numDet.PECnt[a] = "";
	}

	int xsum[5], ysum[5], xcnt[5], ycnt[5];
	int pecells[20], pecnt[20];
	for (int a=0; a<20; a++)
	{
		pecells[a] = 0;
		pecnt[a] = 0;
	}
	for (int a=0; a<5; a++)
	{
		xsum[a] = 0;
		ysum[a] = 0;
		xcnt[a] = 0;
		ycnt[a] = 0;
	}

	for (int a='A'; a<='Z'; a++)
	{
		int i = getPEIndex(QChar(a));
		if ((i>=0) && (i<12))
		{
			int p = i%4;
			int l = i/4;
			if (m_Let[a-'A']>0)
			{
				numDet.PE[l*5+p] += a + QString::number(m_Let[a-'A']);
				int c = m_Let[a-'A'];
                int v = m_Let[a-'A']*downToDigit(letterToNumber(QChar(a)));
				pecnt[l*5+p] += c;
				pecells[l*5+p] += v;
				xcnt[p] += c;
				ycnt[l] += c;
			}
		}
	}

	for (int l=0; l<4; l++)
	{
		for (int p=0; p<5; p++)
		{
			int v = statDownToBase(pecells[l*5+p]);
			xsum[p] += v;
			ysum[l] += v;
		}
	}

	for (int a=0; a<20; a++)
	{
		if (numDet.PE[a].size() > 0)
		{
			numDet.PERep[a] = numDet.PE[a] + "=";
			int v = statDownToBase(pecells[a]);
			if (v != pecells[a])
				numDet.PERep[a] += QString::number(v) + "/" + QString::number(pecells[a]) + ", ";
			else
				numDet.PERep[a] += QString::number(v) + ", ";
			numDet.PERep[a] += QString::number(pecnt[a]);
		}
	}
	for (int l=0; l<4; l++)
	{
		int p = 4;
		int a = l*5+p;

		int v = statDownToBase(ysum[l]);
		if (v != ysum[l])
			numDet.PERep[a] += QString::number(v) + "/" + QString::number(ysum[l]) + ", ";
		else
			numDet.PERep[a] += QString::number(v) + ", ";
		numDet.PERep[a] += QString::number(ycnt[l]);
	}
	for (int p=0; p<5; p++)
	{
		int l = 3;
		int a = l*5+p;

		int v = statDownToBase(xsum[p]);
		if (v != xsum[p])
			numDet.PERep[a] += QString::number(v) + "/" + QString::number(xsum[p]) + ", ";
		else
			numDet.PERep[a] += QString::number(v) + ", ";
		numDet.PERep[a] += QString::number(xcnt[p]);

        if (p<4)
            planesExpression[p] = v;
	}


	clear();
	QList<int> nsnums;
	calcNamesNumbers(shortName, nsnums);
	sum=0;
	for(int a=0; a<nsnums.size(); a++)
        if (m_GlobSettings.calcNameWD)
            sum += downToBase(nsnums[a]);
        else
            sum += nsnums[a];
    for(int a=0; a<128; a++)
		nsbaseNums[a] = m_BaseNums[a];
	int m_mexpression = downToBase(sum); downToBase(sum, baseNums);

	numDet.snameR = "=";
	for(int a=0; a<nsnums.size(); a++)
	{
		if (a!=0)
			numDet.snameR += "+";

		int v = nsnums[a];
		int vb = downToBase(nsnums[a]);
		numDet.snameR += QString::number(downToBase(v));
		if (v!=vb)
			numDet.snameR += '/' + QString::number(v);
	}
	numDet.snameR += "=" + QString::number(sum);
	if (sum != m_mexpression)
		numDet.snameR += "=" + QString::number(m_mexpression);

	numDet.mexpression = QString::number(m_mexpression);
	if (sum != m_mexpression)
		numDet.mexpression += "/" + QString::number(sum);


	nsnums.clear();
    calcNamesNumbersP(shortName, nsnums, true, snvw, pmode);
	sum=0;
	for(int a=0; a<nsnums.size(); a++)
        if (m_GlobSettings.calcNameWD)
            sum += downToBase(nsnums[a]);
        else
            sum += nsnums[a];
	int m_mhearDesire = downToBase(sum); downToBase(sum, baseNums);

	numDet.snameR1 = "=";
	for(int a=0; a<nsnums.size(); a++)
	{
		if (a!=0)
			numDet.snameR1 += "+";

		int v = nsnums[a];
		int vb = downToBase(nsnums[a]);
		numDet.snameR1 += QString::number(downToBase(v));
		if (v!=vb)
			numDet.snameR1 += '/' + QString::number(v);
	}
	numDet.snameR1 += "=" + QString::number(sum);
	if (sum != m_mhearDesire)
		numDet.snameR1 += "=" + QString::number(m_mhearDesire);

	numDet.mheartDesire = QString::number(m_mhearDesire);
	if (sum != m_mhearDesire)
		numDet.mheartDesire += "/" + QString::number(sum);


	nsnums.clear();
    calcNamesNumbersP(shortName, nsnums, false, snvw, pmode);
	sum=0;
	for(int a=0; a<nsnums.size(); a++)
        if (m_GlobSettings.calcNameWD)
            sum += downToBase(nsnums[a]);
        else
            sum += nsnums[a];
	int m_mpers = downToBase(sum); downToBase(sum, baseNums);


	numDet.snameR2 = "=";
	for(int a=0; a<nsnums.size(); a++)
	{
		if (a!=0)
			numDet.snameR2 += "+";

		int v = nsnums[a];
		int vb = downToBase(nsnums[a]);
		numDet.snameR2 += QString::number(downToBase(v));
		if (v!=vb)
			numDet.snameR2 += '/' + QString::number(v);
	}
	numDet.snameR2 += "=" + QString::number(sum);
	if (sum != m_mpers)
		numDet.snameR2 += "=" + QString::number(m_mpers);


	numDet.mpersonality = QString::number(m_mpers);
	if (sum != m_mpers)
		numDet.mpersonality += "/" + QString::number(sum);


	numDet.snameN1 = "";
	numDet.snameN2 = "";
	for (int a=0; a<shortName.size(); a++)
	{
		int v = isVow(shortName, a, pmode);
        if (a<snvw.size())
        {
            if (snvw.at(a)==1)
                v=1;
            else if (snvw.at(a)==2)
                v=-1;
        }
		if (v>0)
		{
            numDet.snameN1 += QString::number(downToDigit(letterToNumber(shortName[a])));
			numDet.snameN2 += " ";
		} else if (v<0)
		{
            numDet.snameN2 += QString::number(downToDigit(letterToNumber(shortName[a])));
			numDet.snameN1 += " ";
		} else
		{
			numDet.snameN1 += " ";
			numDet.snameN2 += " ";
		}
	}



    if (!skipReport)
    {
        if ((m_expression==0) || (m_hearDesire==0) || (m_personality==0) || (m_mexpression==0) || (m_mhearDesire==0))
        {
            QString Msg = tr("Error! Incorrect full or short name!");
            QMessageBox::StandardButton Res;
            Res = QMessageBox::critical(NULL, tr(""), Msg, QMessageBox::Ok);

            return false;
        }
    }

	int m_Maturity = downToBase(m_LifePath+m_expression); //realization
    int b11 = downToDigit(m_LifePath);
    int b12 = downToDigit(m_expression);
    int b21 = downToDigit(m_hearDesire);
    int b22 = downToDigit(m_personality);
    if (b11<b12) { int t = b12; b12 = b11; b11 = t; }
    if (b21<b22) { int t = b22; b22 = b21; b21 = t; }
    int m_LPEBridge = abs(downToDigit(m_LifePath)-downToDigit(m_expression));
    int m_LPEBridgeDet = b11*10 + b12;
	int m_HDPBridge = abs(downToDigit(m_hearDesire)-downToDigit(m_personality));
    int m_HDPBridgeDet = b21*10 + b22;
    int m_RationalThought = downToDigit(fnnum+dday);

	numDet.maturity = QString::number(m_Maturity);
	if (m_Maturity != (m_LifePath+m_expression))
		numDet.maturity += "/" + QString::number(m_LifePath+m_expression);

	numDet.rationalThought = QString::number(m_RationalThought);
	if (m_RationalThought != (fnnum+dday))
		numDet.rationalThought += "/" + QString::number(fnnum+dday);


	numDet.brHDP = QString::number(m_HDPBridge);
	numDet.brLPE = QString::number(m_LPEBridge);


	sum = 0;
	QStringList tstr = fullName.split(" ");
	for (int a=0; a<tstr.size(); a++)
        if (tstr.at(a).size()>0)
            sum += downToDigit(letterToNumber(tstr.at(a).at(0)));
	int m_Balance = downToDigit(sum);

	numDet.balance = QString::number(m_Balance);
	if (m_Balance != sum)
		numDet.balance += "/" + QString::number(sum);

	numDet.karmicDebts = "";
	if ((baseNums[13]!=0) || (baseNums[14]!=0) || (baseNums[16]!=0) || (baseNums[19]!=0))
	{
		QString ksl;
		if (baseNums[13]!=0)
			ksl = "13";
		if (baseNums[14]!=0)
		{
			if (ksl.size()>0)
				ksl += ", ";
			ksl += "14";
		}
		if (baseNums[16]!=0)
		{
			if (ksl.size()>0)
				ksl += ", ";
			ksl += "16";
		}
		if (baseNums[19]!=0)
		{
			if (ksl.size()>0)
				ksl += ", ";
			ksl += "19";
		}

		numDet.karmicDebts = ksl;
	}
	if (numDet.karmicDebts.size()==0)
		numDet.karmicDebts = "-";

    // Transit & Essence
    QStringList trList = fullName.split(" ");
    QString tran1, tran2, tran3;
    QString tranP, tranM, tranS;
    if (trList.size()>0)
    {
        tran1 = trList.at(0); //physical
        tran3 = trList.at(trList.size()-1);//spiritual
        if (m_GlobSettings.skipTransitFS)
        {
            if (trList.size()==1)
                tran3 = "";
        }
        if (trList.size()<=2)
        {
            if (!m_GlobSettings.skipTransitFS)
                tran2 = tran3; //mental
        } else
        {
            for (int a=1; a<trList.size()-1; a++)
                tran2 += trList.at(a);
        }
    }

    tran1 = tran1.toUpper();
    for (int a=0; a<tran1.size(); a++)
        if (tran1.at(a).isLetter())
            tranP += tran1.at(a);
    tran2 = tran2.toUpper();
    for (int a=0; a<tran2.size(); a++)
        if (tran2.at(a).isLetter())
            tranM += tran2.at(a);
    tran3 = tran3.toUpper();
    for (int a=0; a<tran3.size(); a++)
        if (tran3.at(a).isLetter())
            tranS += tran3.at(a);

    if (tranP.size()==0)
        tranP = "X";

    int sidx=0;
    do {
        for (int a=0; a<letterToNumber(tranP.at(sidx)); a++)
            m_sTransit1 += tranP.at(sidx);
        sidx = (sidx+1) % tranP.size();
    } while (m_sTransit1.size()<128);
    m_sTransit1.truncate(128);


    if (tranM.size()!=0)
    {
        sidx=0;
        do {
            for (int a=0; a<letterToNumber(tranM.at(sidx)); a++)
                m_sTransit2 += tranM.at(sidx);
            sidx = (sidx+1) % tranM.size();
        } while (m_sTransit2.size()<128);
        m_sTransit2.truncate(128);
    } else
    {
        for (int a=0; a<128; a++)
            m_sTransit2 += " ";
    }

    if (tranS.size()!=0)
    {
        sidx=0;
        do {
            for (int a=0; a<letterToNumber(tranS.at(sidx)); a++)
                m_sTransit3 += tranS.at(sidx);
            sidx = (sidx+1) % tranS.size();
        } while (m_sTransit3.size()<128);
        m_sTransit3.truncate(128);
    } else
    {
        for (int a=0; a<128; a++)
            m_sTransit3 += " ";
    }

    for (int a=0; a<128; a++)
    {
        m_EssenceS[a] = letterToNumber(m_sTransit1.at(a));
        if (tranM.size()!=0)
            m_EssenceS[a] += letterToNumber(m_sTransit2.at(a));
        if (tranS.size()!=0)
            m_EssenceS[a] += letterToNumber(m_sTransit3.at(a));

        //m_EssenceS[a] = letterToNumber(m_sTransit1.at(a)) + letterToNumber(m_sTransit3.at(a));
        if ((m_EssenceS[a]==16)||(m_EssenceS[a]==19))
            m_Essence[a] = m_EssenceS[a];
        else
            m_Essence[a] = downToBase(m_EssenceS[a]);
    }


    if (skipReport)
        return true;

    if (select >= 0)
        return calculateDynamic(useCNC, trial, repList, select, baseNums, m_mexpression, m_hearDesire, m_mhearDesire, m_personality, m_mpers, m_Maturity, m_RationalThought, &m_KarmicLessons, m_Balance,
                                &m_HiddenPassion, m_SubconsciousConfidence, planesExpression, m_LPEBridge, m_LPEBridgeDet, m_HDPBridge, m_HDPBridgeDet);



	if (useCNC)
	{
		startDoc("VeBest Chaldean Numerology Report");
		startChap();
        addHead("Chaldean Numerology Chart for");
	} else
	{
		startDoc("VeBest Numerology Report");
		startChap();
		addHead("Numerology Chart for");
	}
	addHead(fullName + " (" + shortName + "), "
		+ QString(mname[bdate.month()-1]) + " " + QString::number(bdate.day()) + " " + QString::number(bdate.year()));
    int m_spNum[4];
    m_spNum[0] = m_spNum1;
    m_spNum[1] = m_spNum2;
    m_spNum[2] = m_spNum3;
    m_spNum[3] = m_spNum4;

    int m_pNum[4];
    m_pNum[0] = m_pNum1;
    m_pNum[1] = m_pNum2;
    m_pNum[2] = m_pNum3;
    m_pNum[3] = m_pNum4;

    QList<int> allIds;
    for (int i=0; i<NT_END; i++)
        allIds.append(i);

#ifndef VER_MOBILE
    addPar(getCalculateHeaderC(baseNums, m_spNum, m_pNum, useCNC, allIds));
#endif
	addPar("");
	endChap();


	//startChap();
#ifndef VER_MOBILE
    calculateGenTable(allIds);
#endif

	/*if (trial)
	{
		cursor.beginEditBlock();
		cursor.setBlockFormat(h2p);
		cursor.setCharFormat(h2);
		cursor.insertText("Warning! Trial period, only suitable for predefined persons calculation!\r\n");
		cursor.insertText("Register software to get your own calculation!\r\n");
		cursor.insertText("\r\n");
		cursor.endEditBlock();
	}*/


	/*QTextFrameFormat imgformatframe;
	imgformatframe.setMargin(10);
	imgformatframe.setPosition(QTextFrameFormat::FloatRight);*/


	// Details
    startChap();
    addImage("res39.jpg");
    addPar(numerInfo[0] + "<br /><br />");
    endChap();

    startChap();
    addImage("res01.jpg");
	addHead("Life Path Number " + numDet.lifePath);
    addPar(numerLifePath[0]);
    endChap();
    startChap();
    addPar(addSplitParagraph(numerLifePath[m_LifePath] + "<br />", "", ""));

	startChap();
    addImage("res02.jpg");
	addHead("Birthday Number " + QString::number(bdate.day()));
    addPar(numerBirthDate[0]);
	endChap();
    startChap();
    addPar(addSplitParagraph(numerBirthDate[bdate.day()] + "<br />", "", ""));


	startChap();
    addImage("res03.jpg");
	addHead("Expression Number " + numDet.expression);
    addPar(numerExpr[0]);
	endChap();
    startChap();
    addPar(addSplitParagraph(numerExpr[m_expression] + "<br />", "", ""));


	if (!trial)
	{
		startChap();
		addHead("Minor Expression Number " + numDet.mexpression);
        addPar(numerExprM[0]);
		endChap();
        startChap();
        addPar(addSplitParagraph(numerExprM[m_mexpression] + "<br />", "", ""));
    }

	if (trial)
	{
		startChap();
#ifndef VER_FREE
        addPar("<br />" + numerInfo[9] + "<br />");
#endif
        endChap();
    } else
	{
        startChap();
        addImage("res04.jpg");
        addHead("Heart Desire Number " + numDet.heartDesire);
        addPar(numerHeartDesire[0]);
        endChap();
        startChap();
        addPar(addSplitParagraph(numerHeartDesire[m_hearDesire] + "<br />", "", ""));


		startChap();
		addHead("Minor Heart Desire Number " + numDet.mheartDesire);
        addPar(numerMinorHeartDesire[0] + "<br />" + numerMinorHeartDesire[m_mhearDesire] + "<br />");
		endChap();

        if (useCNC && numDet.compound >= 10 && numDet.compound <= 99)
        {
			startChap();
			addHead("Compound Number " + QString::number(numDet.compound));
            addPar(numerCompoundNumbers[0] + "<br />" + numerCompoundNumbers[numDet.compound] + "<br />");
			endChap();
        }

		startChap();
        addImage("res05.jpg");
		addHead("Personality Number " + numDet.personality);
        addPar(numerPersonality[0] + "<br />" + numerPersonality[m_personality] + "<br />");
		endChap();

        if (m_personality != m_mpers)
        {
            startChap();
            addHead("Minor Personality Number " + numDet.mpersonality);
            addPar(numerPersonality[m_mpers] + "<br />");
            endChap();
        }

		//
		if ((baseNums[13]!=0) || (baseNums[14]!=0) || (baseNums[16]!=0) || (baseNums[19]!=0))
		{
			startChap();
            addImage("res06.jpg");
			addHead("Karmic Debts Numbers");
            addPar(numerKarmDebts[0]);
			endChap();

			if (baseNums[13]!=0)
			{
				startChap();
				addHead("Karmic Debt Number: 13");
				addPar(numerKarmDebts[13] + "<br />");
				endChap();
			}
			if (baseNums[14]!=0)
			{
				startChap();
				addHead("Karmic Debt Number: 14");
				addPar(numerKarmDebts[14] + "<br />");
				endChap();
			}
			if (baseNums[16]!=0)
			{
				startChap();
				addHead("Karmic Debt Number: 16");
				addPar(numerKarmDebts[16] + "<br />");
				endChap();
			}
			if (baseNums[19]!=0)
			{
				startChap();
				addHead("Karmic Debt Number: 19");
				addPar(numerKarmDebts[19] + "<br />");
				endChap();
			}
		}

		startChap();
        addImage("res07.jpg");
		addHead("Maturity Number " + numDet.maturity);
        addPar(numerMaturity[0]);
		endChap();
        startChap();
        addPar(addSplitParagraph(numerMaturity[m_Maturity] + "<br />", "", ""));


		startChap();
        addImage("res08.jpg");
		addHead("Rational Thought Number " + numDet.rationalThought);
        addPar(numerRationalThoughts[0]);
		endChap();
        startChap();
        addPar(addSplitParagraph(numerRationalThoughts[m_RationalThought] + "<br />", "", ""));

		if (m_KarmicLessons.size() > 0)
		{
			startChap();
			addImage("res09.jpg");
			addHead("Karmic Lessons Number " + numDet.karmicLessons);
			addPar(numerKarmicLessons[0]);
			endChap();
			for (int a=0; a<m_KarmicLessons.size(); a++)
			{
				startChap();
				addPar(numerKarmicLessons[m_KarmicLessons.at(a)]);
                if (a==m_KarmicLessons.size()-1)
                    addPar("<br />");
				endChap();
			}
		}

		startChap();
        addImage("res10.jpg");
		addHead("Balance Number " + numDet.balance);
        addPar(numerBalance[0] + "<br />" + numerBalance[m_Balance] + "<br />");
		endChap();

		if (m_HiddenPassion.size() > 0)
		{
			startChap();
            addImage("res11.jpg");
			addHead("Hidden Passion Number " + numDet.hiddenPassion);
			addPar(numerHiddenPassion[0]);
			endChap();
			for (int a=0; a<m_HiddenPassion.size(); a++)
			{
				startChap();
				addPar(numerHiddenPassion[m_HiddenPassion.at(a)]);
                if (a==m_HiddenPassion.size()-1)
                    addPar("<br />");
				endChap();
			}
		}

		startChap();
        addImage("res12.jpg");
		addHead("Subconscious Confidence Number " + numDet.subconsciousConfidence);
        addPar(numerSubconsConfid[0] + "<br />" + numerSubconsConfid[m_SubconsciousConfidence] + "<br />");
		endChap();

		startChap();
		addImage("res03.jpg");
		addHead("Planes of Expression");
        addPar(numerPlanesOfExpression[0] + "<br />");
        endChap();
        if (planesExpression[0]>0)
		{
            startChap();
            addHead("Planes of Expression Mental Number " + numDet.PERep[3*5+0]);
            addPar(numerPlanesOfExpressionM[planesExpression[0]] + "<br />");
            endChap();
        }
		if (planesExpression[1]>0)
		{
            startChap();
            addHead("Planes of Expression Physical Number " + numDet.PERep[3*5+1]);
            addPar(numerPlanesOfExpressionP[planesExpression[1]] + "<br />");
            endChap();
        }
		if (planesExpression[2]>0)
		{
            startChap();
            addHead("Planes of Expression Emotional Number " + numDet.PERep[3*5+2]);
            addPar(numerPlanesOfExpressionE[planesExpression[2]] + "<br />");
            endChap();
        }
		if (planesExpression[3]>0)
		{
            startChap();
            addHead("Planes of Expression Intuitive Number " + numDet.PERep[3*5+3]);
            addPar(numerPlanesOfExpressionI[planesExpression[3]] + "<br />");
            endChap();
        }


		startChap();
        addImage("res13.jpg");
		addHead("Pinnacles Numbers " + numDet.pinnacles);
		addHead("Age span for life cycles " + numDet.ageSpan);
        addPar(numerPinnacles[0]);
		endChap();
		startChap();
        addPar("<b>Life cycle 1 (" + as1 + ")</b> " + numerPinnacles[m_pNum1]);
		endChap();
		startChap();
        addPar("<b>Life cycle 2 (" + as2 + ")</b> " + numerPinnacles[m_pNum2]);
		endChap();
		startChap();
        addPar("<b>Life cycle 3 (" + as3 + ")</b> " + numerPinnacles[m_pNum3]);
		endChap();
		startChap();
        addPar("<b>Life cycle 4 (" + as4 + ")</b> " + numerPinnacles[m_pNum4] + "<br />");
		endChap();

		startChap();
        addImage("res14.jpg");
        addHead("Challenges Numbers " + numDet.challenges);
		addPar(numerChallenges[0] + "<br />");
		endChap();
		startChap();
        addPar("<b>Life cycle 1 (" + as1 + ")</b> " + numerChallenges[m_cNum1+1]);
		endChap();
		startChap();
        addPar("<b>Life cycle 2 (" + as2 + ")</b> " + numerChallenges[m_cNum2+1]);
		endChap();
		startChap();
        addPar("<b>Life cycle 3 (" + as3 + ")</b> " + numerChallenges[m_cNum3+1]);
		endChap();
		startChap();
        addPar("<b>Life cycle 4 (" + as4 + ")</b> " + numerChallenges[m_cNum4+1] + "<br />");
		endChap();

        startChap();
        //addImage("res14.jpg");
        addHead("Bridge Life Path - Expression " + numDet.brLPE);
        addPar(numerBridgeLPE[0]);
        endChap();
        startChap();
        addPar(numerBridgeLPE[m_LPEBridge+1]);
        if (numerBridgeLPEDet[m_LPEBridgeDet].size()>0) {
            addPar("<br />" + numerBridgeLPEDet[m_LPEBridgeDet]);
        }
        addPar("<br />");
        endChap();

        startChap();
        //addImage("res14.jpg");
        addHead("Bridge Heart's Desire - Personality " + numDet.brHDP);
        addPar(numerBridgeHDP[0]);
        endChap();
        startChap();
        addPar(numerBridgeHDP[m_HDPBridge+1]);
        if (numerBridgeHDPDet[m_HDPBridgeDet].size()>0) {
            addPar("<br />" + numerBridgeHDPDet[m_HDPBridgeDet] + "<br />");
        }
        endChap();

		int m_CPersYear = downToDigit(m_PersYear + downToDigit(m_cyear));
		startChap();
        addImage("res15.jpg");
		addHead("Personal Year Number: " + QString::number(m_CPersYear));
		addPar(numerPersYear[0]);
		endChap();
		for (int a=0; a<9; a++)
		{
			startChap();
			int y = m_cyear+a;
			int PersYear = downToDigit(m_PersYear + downToDigit(y));
			addPar(QString::number(y) + QString(" (") +QString::number(PersYear) + QString("). ") + numerPersYear[PersYear]);
            if (a==8)
                addPar("<br />");
			endChap();
		}


		int m_CPersMonth = downToDigit(m_CPersYear + downToDigit(cdate.month()));
		startChap();
        addImage("res15.jpg");
		addHead("Personal Month Number: " + QString::number(m_CPersMonth));
		addPar(numerPersDate[0]);
		endChap();
		for (int a=0; a<12; a++)
		{
			startChap();
			QDate cd;
			cd = cdate;
			cd = cd.addMonths(a);
			int m_CPersYear = downToDigit(m_PersYear + downToDigit(cd.year()));
			int c = downToDigit(m_CPersYear + downToDigit(cd.month()));
			addPar(QString::number(cd.month()) + "/" + QString::number(cd.year()) + QString(" (") +QString::number(c) + QString("). ") + numerPersDate[c]);
            if (a==11)
                addPar("<br />");
			endChap();
		}


		int m_CPersDate = downToDigit(m_CPersMonth + downToDigit(cdate.day()));
		startChap();
        //addImage("res15.jpg");
		addHead("Personal Day Number: " + QString::number(m_CPersDate));
		endChap();
		for (int a=0; a<9; a++)
		{
			startChap();
			QDate cd;
			cd = cdate;
			cd = cd.addDays(a);
			int m_CPersYear = downToDigit(m_PersYear + downToDigit(cd.year()));
			int m_CPersMonth = downToDigit(m_CPersYear + downToDigit(cd.month()));
			int c = downToDigit(m_CPersMonth + downToDigit(cd.day()));
			addPar(QString::number(cd.month()) + "/" + QString::number(cd.day()) + "/" + QString::number(cd.year()) + QString(" (") +QString::number(c) + QString("). ") + numerPersDate[c]);
            if (a==8)
                addPar("<br />");
			endChap();
		}

        // Essence and Transits
        int age=cdate.year() - bdate.year()-1;
        if (cdate.month()>bdate.month())
            age++;
        else if ((cdate.month()==bdate.month())&&(cdate.day()>=bdate.day()))
            age++;
        if ((age>=0) && (age<100))
        {
            startChap();
            //addImage("res15.jpg");
            addHead("Transits for age " + QString::number(age));
            addPar(numerTransit[0]);
            endChap();

            startChap();
            addPar("Physical");
            int ci=m_sTransit1.at(age).toAscii()-'A'+1;
            if ((ci>=1) && (ci<=26))
                addPar(numerTransit[ci]);
            endChap();

            startChap();
            addPar("Mental");
            ci=m_sTransit2.at(age).toAscii()-'A'+1;
            if ((ci>=1) && (ci<=26))
                addPar(numerTransit[ci]);
            endChap();

            startChap();
            addPar("Spiritual");
            ci=m_sTransit3.at(age).toAscii()-'A'+1;
            if ((ci>=1) && (ci<=26))
                addPar(numerTransit[ci]);
            endChap();

            startChap();
            //addImage("res15.jpg");
            addPar("<br />");
            addHead("Essence for age " + QString::number(age) + ": " + QString::number(m_Essence[age]) + "/" + QString::number(m_EssenceS[age]));
            addPar(numerEssence[0]);
            endChap();

            startChap();
            addPar(numerEssence[m_Essence[age]]);
            endChap();
        }
	}

	endDoc();
	//m_TextDoc.setHtml(htmlDoc);


	return true;
}


void CNumerology::draw(QPainter *)
{
	if (m_LifePath <= 0)
		return;

	//m_TextDoc.drawContents(pPntr);
}
