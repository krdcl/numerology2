#include "settingsdlg.h"
#include "ui_settingsdlg.h"

extern QString qsGlobalDlgStyle;


CSettingsDlg::CSettingsDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CSettingsDlg)
{
    ui->setupUi(this);
    setWindowFlags( windowFlags() & ~Qt::WindowMinimizeButtonHint );
	setStyleSheet(qsGlobalDlgStyle);
	setWindowTitle("Settings");

#ifndef VER_PREM
    ui->checkBoxExtendedMMto99->setEnabled(false);
    ui->checkBoxExtendedMMto99->setVisible(false);
#endif
}

CSettingsDlg::~CSettingsDlg()
{
    delete ui;
}

void CSettingsDlg::on_defaultSettingsBut_clicked()
{
    ui->checkBoxLP1->setChecked(true);
    ui->checkBoxLP2->setChecked(false);
    ui->checkBoxLP3->setChecked(false);
    ui->checkBoxNameDD->setChecked(true);
    ui->checkBoxNameWD->setChecked(true);
    ui->checkBoxSkipTrans->setChecked(false);
    ui->checkBoxExtendedMMto99->setChecked(false);
    ui->checkBoxDateFormat->setChecked(false);
}
