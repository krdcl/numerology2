#include "lic.h"
#include <string.h>
#include <stdio.h>
#include <QSettings>
#include <QDir>


char lcrandstart[]="DCdgfd$98-+65V6hgW6f21hlkG";
char lcckey[]="[39f[Ed_sdvvs;lebnx).3HnmC";

#ifdef Q_WS_WIN
#include <windows.h>
#include <iostream>
#include <malloc.h>
#endif

#ifdef Q_WS_MAC
#include <CoreFoundation/CoreFoundation.h>

#include <IOKit/IOKitLib.h>
#include <IOKit/network/IOEthernetInterface.h>
#include <IOKit/network/IONetworkInterface.h>
#include <IOKit/network/IOEthernetController.h>

static kern_return_t FindEthernetInterfaces(io_iterator_t *matchingServices);
static kern_return_t GetMACAddress(io_iterator_t intfIterator, UInt8 *MACAddress, UInt8 bufferSize);

// Returns an iterator containing the primary (built-in) Ethernet interface. The caller is responsible for
// releasing the iterator after the caller is done with it.
static kern_return_t FindEthernetInterfaces(io_iterator_t *matchingServices)
{
	kern_return_t           kernResult; 
	CFMutableDictionaryRef	matchingDict;
	CFMutableDictionaryRef	propertyMatchDict;

	matchingDict = IOServiceMatching(kIOEthernetInterfaceClass);
	if (NULL == matchingDict) {
		//printf("IOServiceMatching returned a NULL dictionary.\n");
	}
	else {
		propertyMatchDict = CFDictionaryCreateMutable(kCFAllocatorDefault, 0,
			&kCFTypeDictionaryKeyCallBacks,
			&kCFTypeDictionaryValueCallBacks);

		if (NULL == propertyMatchDict) {
			//printf("CFDictionaryCreateMutable returned a NULL dictionary.\n");
		}
		else {
			CFDictionarySetValue(propertyMatchDict, CFSTR(kIOPrimaryInterface), kCFBooleanTrue); 
			CFDictionarySetValue(matchingDict, CFSTR(kIOPropertyMatchKey), propertyMatchDict);
			CFRelease(propertyMatchDict);
		}
	}
	kernResult = IOServiceGetMatchingServices(kIOMasterPortDefault, matchingDict, matchingServices);    
	if (KERN_SUCCESS != kernResult) {
		//printf("IOServiceGetMatchingServices returned 0x%08x\n", kernResult);
	}

	return kernResult;
}

// Given an iterator across a set of Ethernet interfaces, return the MAC address of the last one.
// If no interfaces are found the MAC address is set to an empty string.
static kern_return_t GetMACAddress(io_iterator_t intfIterator, UInt8 *MACAddress, UInt8 bufferSize)
{
	io_object_t		intfService;
	io_object_t		controllerService;
	kern_return_t	kernResult = KERN_FAILURE;

	if (bufferSize < kIOEthernetAddressSize) {
		return kernResult;
	}
	bzero(MACAddress, bufferSize);
	while ((intfService = IOIteratorNext(intfIterator)))
	{
		CFTypeRef	MACAddressAsCFData;        

		kernResult = IORegistryEntryGetParentEntry(intfService,
			kIOServicePlane,
			&controllerService);
		if (KERN_SUCCESS != kernResult) {
			//printf("IORegistryEntryGetParentEntry returned 0x%08x\n", kernResult);
		}
		else {
			MACAddressAsCFData = IORegistryEntryCreateCFProperty(controllerService,
				CFSTR(kIOMACAddress),
				kCFAllocatorDefault,
				0);
			if (MACAddressAsCFData) {
				//CFShow(MACAddressAsCFData); // for display purposes only; output goes to stderr

                               CFDataGetBytes((const __CFData*) MACAddressAsCFData, CFRangeMake(0, kIOEthernetAddressSize), MACAddress);
				CFRelease(MACAddressAsCFData);
			}

			(void) IOObjectRelease(controllerService);
		}
		(void) IOObjectRelease(intfService);
	}
	return kernResult;
}

void CopySerialNumber(CFStringRef *serialNumber)
{
	if (serialNumber != NULL) {
		*serialNumber = NULL;

		io_service_t    platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault,
			IOServiceMatching("IOPlatformExpertDevice"));

		if (platformExpert) {
			CFTypeRef serialNumberAsCFString =
				IORegistryEntryCreateCFProperty(platformExpert,
				CFSTR(kIOPlatformSerialNumberKey),
				kCFAllocatorDefault, 0);
			if (serialNumberAsCFString) {
                                *serialNumber = (const __CFString*)serialNumberAsCFString;
			}

			IOObjectRelease(platformExpert);
		}
	}
}
#endif

void gethwcode(QString &data)
{
#ifdef Q_WS_WIN
	QString str = "";
	QStringList stlist;

	QSettings *set;
	
	set = new QSettings("HKEY_LOCAL_MACHINE\\HARDWARE\\DESCRIPTION\\System", QSettings::NativeFormat);
	str += set->value("SystemBiosDate").toString();
	stlist = set->value("SystemBiosVersion").toStringList();
	for (int a=0; a<stlist.count(); a++)
		str += stlist.at(a);
	delete set;

	set = new QSettings("HKEY_LOCAL_MACHINE\\HARDWARE\\DESCRIPTION\\System\\BIOS", QSettings::NativeFormat);
	str += set->value("BaseBoardManufacturer").toString();
	str += set->value("BaseBoardProduct").toString();
	str += set->value("BaseBoardVersion").toString();
	str += set->value("BIOSVersion").toString();
	str += set->value("BIOSReleaseDate").toString();
	delete set;

	set = new QSettings("HKEY_LOCAL_MACHINE\\HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0", QSettings::NativeFormat);
	str += set->value("Identifier").toString();
	delete set;

	str.remove(QChar(' '));

	QString volsn = "00000000";
	QString sdrv = QDir::rootPath();
	sdrv.truncate(3);

	//LPCTSTR szHD = (LPCTSTR) sdrv.toAscii().constData();
	LPCSTR szHD = (LPCSTR)"C:\\";
	UCHAR szFileSys[255],szVolNameBuff[255];
	DWORD dwSerial,dwMFL,dwSysFlags;
	BOOL bSuccess;

	bSuccess = GetVolumeInformationA(szHD,(LPSTR)szVolNameBuff,
		255,&dwSerial, &dwMFL,&dwSysFlags,
		(LPSTR)szFileSys,255);

	if(bSuccess){
		if(szVolNameBuff[0] != NULL)
			volsn = QString("%1").arg(dwSerial, 8, 16, QChar('0'));
	}

	str += volsn;

	data = str;
#endif
#ifdef Q_WS_MAC
	QString str = "";

	kern_return_t	kernResult = KERN_SUCCESS;
	io_iterator_t	intfIterator;
	UInt8			MACAddress[kIOEthernetAddressSize];
	char buf[256];

	kernResult = FindEthernetInterfaces(&intfIterator);

	if (KERN_SUCCESS != kernResult) {
                //sprintf(buf, "FindEthernetInterfaces returned 0x%08x\n", kernResult);
	}
	else {
		kernResult = GetMACAddress(intfIterator, MACAddress, sizeof(MACAddress));

		if (KERN_SUCCESS != kernResult) {
			sprintf(buf, "%08x", kernResult);
		}
		else {
			sprintf(buf, "%02x:%02x:%02x:%02x:%02x:%02x",
				MACAddress[0], MACAddress[1], MACAddress[2], MACAddress[3], MACAddress[4], MACAddress[5]);
		}
	}

	(void) IOObjectRelease(intfIterator);	// Release the iterator.
        str += buf;

        CFStringRef sncfstr;
        CopySerialNumber(&sncfstr);
        CFStringGetCString(sncfstr,buf,sizeof(buf),kCFStringEncodingMacRoman);
        str += buf;

        data = str;
#endif
}


void dprocess(char *inp, unsigned int inplen, char* key, unsigned int keylen)
{
	unsigned char Sbox[257], Sbox2[257];
	unsigned long i, j, t, x;

	static const char OurUnSecuredKey[] = "1234567891234567" ;
	static const unsigned int OurKeyLen = strlen(OurUnSecuredKey);
	unsigned char temp , k;
	i = j = k = t =  x = 0;
	temp = 0;

	for (unsigned int a=0; a<sizeof(Sbox); a++)
	{
		Sbox[a] = 0;
		Sbox2[a] = 0;
	}

	for(i = 0; i < 256U; i++)
	{
		Sbox[i] = (unsigned char)i;
	}

	j = 0;
	if(keylen)
	{
		for(i = 0; i < 256U ; i++)
		{
			if(j == keylen)
			{
				j = 0;
			}
			Sbox2[i] = key[j++];
		}    
	}
	else
	{
		for(i = 0; i < 256U ; i++)
		{
			if(j == OurKeyLen)
			{
				j = 0;
			}
			Sbox2[i] = OurUnSecuredKey[j++];
		}
	}

	j = 0;
	for(i = 0; i < 256; i++)
	{
		j = (j + (unsigned long) Sbox[i] + (unsigned long) Sbox2[i]) % 256U ;
		temp =  Sbox[i];
		Sbox[i] = Sbox[j];
		Sbox[j] =  temp;
	}

	i = j = 0;
	for(x = 0; x < inplen; x++)
	{
		i = (i + 1U) % 256U;
		j = (j + (unsigned long) Sbox[i]) % 256U;
		temp = Sbox[i];
		Sbox[i] = Sbox[j] ;
		Sbox[j] = temp;
		t = ((unsigned long) Sbox[i] + (unsigned long) Sbox[j]) %  256U ;
		k = Sbox[t];
		inp[x] = (inp[x] ^ k);
	}
}


bool pString(QString &cr)
{
	char buf[4096];
	int clen;

#ifdef _MSC_VER
	strcpy_s(buf, 4000, cr.toAscii().data());
#else
	strcpy(buf, cr.toAscii().data());
#endif

	clen = strlen(buf);
	dprocess( buf, clen, lcrandstart, strlen(lcrandstart) );
	cr = "";
	for ( int a=0; a<clen; a++ )
	{
		cr += (buf[a] & 0x0f) + 'a';
		cr += ((buf[a] >> 4) & 0x0f) + 'A';
	}

	return true;
}


bool gString(QString &cr)
{
	char buf[4096];
	int clen;

	if ( cr.size() < 2 )
		return false;

	clen = cr.size()/2;
	for ( int a=0; a<clen; a++ )
	{
		buf[a] = (cr[a*2].toAscii() - 'a') + (cr[a*2+1].toAscii() - 'A')*16;
	}
	dprocess( buf, clen, lcrandstart, strlen(lcrandstart) );

	cr = "";

	for ( int a=0; a<clen; a++ )
		cr += buf[a];

	return true;
}


CSecLic::CSecLic(QObject *parent)
	: QObject(parent)
{
	manager = new QNetworkAccessManager(this);
	connect(manager, SIGNAL(finished(QNetworkReply*)),
		this, SLOT(replyFinished(QNetworkReply*)));

	rm1 = "Registration in progress...";
	rm2 = "Error: Registration fault!";
	rm3 = "Successfully registered for";
	rm4 = "Error: Incorrect serial number!";

	retMessage = "";
	pvalid = 0;
	status = 0;
	initLic();
}


CSecLic::~CSecLic()
{
	delete manager;
}


void CSecLic::initLic()
{
	QSettings *pSettings = new QSettings(LICREGGROUP, LICREGGROUP);
	pSettings->sync();
	QString v1 = pSettings->value("/val1", "").toString();
	QString v2 = pSettings->value("/val2", "").toString();
	QString v3 = pSettings->value("/val3", "").toString();
	QString v4 = pSettings->value("/val4", "").toString();
	delete pSettings;


	if (!v1.isEmpty())
		gString(v1);
	if (!v2.isEmpty())
		gString(v2);
	if (!v3.isEmpty())
		gString(v3);
	if (!v4.isEmpty())
		gString(v4);

	startUpdates = 0;
	if(v1.isEmpty()) //first start
	{
		pvalid = 0;
		status = 0;
		regName = "";
		serialNum = "";
	} else
	{
		if (!v2.isEmpty())
			regName = v2;
		if (!v3.isEmpty())
			serialNum = v3;
		if (!v4.isEmpty())
			pvalid = (v4.toInt() & 0x0001);

		startUpdates = v1.toInt();
        if ((startUpdates>0) && (startUpdates<300) && (serialNum.startsWith(LICPREF)||serialNum.startsWith(LICPREF2)))
		{
			status = pvalid;
		} else
		{
			status = 0;
		}
	}

    if (!(serialNum.startsWith(LICPREF)||serialNum.startsWith(LICPREF2)))
		status = 0;

	startUpdates++;

	storeData();
}


void CSecLic::startLic()
{
    if ((serialNum.startsWith(LICPREF) || serialNum.startsWith(LICPREF2)) && (serialNum.size()>8))
	{
		bool act = false;
		if (regName.size()==0)
			act = true;

		validateSN(serialNum, checkHW(), "", "", act);
	}
}

//#include <QMessageBox>

QString CSecLic::checkHW()
{
	QString hwcode;
	gethwcode(hwcode);

        //QMessageBox::critical(NULL, tr(""), hwcode, QMessageBox::Ok);

	unsigned char hpc[8];
	for (int a=0; a<8; a++)
		hpc[a] = 0;
	for (int a=0; a<hwcode.count(); a++)
		hpc[a%8] += (unsigned char)hwcode.at(a).toAscii();

	hwcode = "";
	for (int a=0; a<8; a++)
		hwcode += QString("%1").arg(hpc[a], 2, 16, QChar('0'));

	return hwcode;
}


void CSecLic::registerSN(QString sn)
{
	status = -1;
	serialNum = sn;
	validateSN(serialNum, checkHW(), "", "", true);
}


void CSecLic::storeData()
{
	QSettings *pSettings = new QSettings(LICREGGROUP, LICREGGROUP);
	pSettings->sync();

	QString v1 = QString::number(startUpdates);
	QString v2, v3;
        QString v4 = QString::number((rand()&0xFFFF)*2+status);

	v2 = regName;
	v3 = serialNum;
	pString(v1);
	pString(v2);
	pString(v3);
	pString(v4);

	pSettings->setValue("/val1", v1);
	pSettings->setValue("/val2", v2);
	pSettings->setValue("/val3", v3);
    pSettings->setValue("/val4", v4);

	delete pSettings;
}


int CSecLic::validateSN(QString sn, QString hwcode1, QString hwcode2, QString hwcode3, bool activate)
{
	Q_UNUSED(hwcode2); Q_UNUSED(hwcode3);
    if ((!(serialNum.startsWith(LICPREF)||serialNum.startsWith(LICPREF2))) || (serialNum.size()<8))
	{
		status = 0;
		serialNum = "";
		retMessage = rm4;

		storeData();
		return -1;
	}

	QNetworkRequest request;

	retMessage = rm1;

	request.setUrl(QUrl(LICSRVURL));

	request.setRawHeader("User-Agent", "License Processing");

	CDENDE16 da;
	char i1[2048];
	int sz;

	{
        sn = QString("%1").arg(rand()&0xFFFF, 4, 16, QChar('0')) + sn;
		sz = sn.size();
		for (int n=0; n<sz; n++)
			i1[n] = sn.at(n).toAscii();
		i1[sz]=0;
		da.plainlen=sz;
		da.ascipherlen=2*da.plainlen;

		da.ascii_encrypt128(i1);
		sn = da.ascCipherText;
	}

	{
                hwcode1 = QString("%1").arg(rand()&0xFFFF, 4, 16, QChar('0')) + hwcode1;
		sz = hwcode1.size();
		for (int n=0; n<sz; n++)
			i1[n] = hwcode1.at(n).toAscii();
		i1[sz]=0;
		da.plainlen=sz;
		da.ascipherlen=2*da.plainlen;

		da.ascii_encrypt128(i1);
		hwcode1 = da.ascCipherText;
	}

	postData.clear();
	postData.append("sn=");
	postData.append(sn);
	postData.append("&hw1=");
	postData.append(hwcode1);
	if ((regName.size() == 0) || activate)
		postData.append("&stat=A");

	request.setHeader(QNetworkRequest::ContentTypeHeader,QVariant("application/x-www-form-urlencoded"));
	manager->post(request, postData);

	return 0;
}

//extern void sprocess(char i[], char o[], int len);

void CSecLic::replyFinished(QNetworkReply *reply)
{
	int err=0;
	if (reply->error() == QNetworkReply::NoError)
	{
		QByteArray bytes = reply->readAll();
		QString repstr(bytes);

		QStringList replistc = repstr.split(",");
		QStringList replist;

		char i1[2048];

		CDENDE16 da;

		for (int a=0; a<replistc.size(); a++)
		{
			int sz = replistc.at(a).size();
			if ((sz>0) && (sz%2==0) && (sz<1024))
			{
				for (int n=0; n<sz; n++)
					i1[n] = replistc.at(a).at(n).toAscii();
				i1[sz]=0;
				da.plainlen=sz/2;
				da.ascipherlen=2*da.plainlen;

				da.ascCipherText = i1;
				da.ascii_decrypt128(da.ascCipherText);
				QString strres = da.plainText;

				if (strres.size() > 4)
					strres.remove(0, 4);

				replist << strres;
			}
		}

		if (replist.size()>0)
		{
			if (replist.at(0) == "22")
			{
				status = 1;
				startUpdates = 1;
				pvalid = 1;
			} else if (repstr == "09")
			{
				status = 0;
				serialNum = "";
				pvalid = 0;
				err = 1;
				retMessage = QString("Error 1: Incorrect serial number!");
			} else
			{
				status = 0;
				serialNum = "";
				pvalid = 0;
				err = 3;
				retMessage = QString("Error 3: Network fault") + QString("\r\nBlocked by firewall");
			}
			if (replist.size()>1)
				regName = replist.at(1);
		} else {
			err = 4;
			retMessage = QString("Error 4: Network fault") + QString("\r\nBlocked by firewall");
		}
	} else {
		err = 2;
		retMessage = QString("Error 2: Network fault ") + QString::number(reply->error()) + QString("\r\nInternet connection unavailable");
	}

	if (status != 1)
	{
		status = 0;
		serialNum = "";
	}

	storeData();

	if (status != 1)
	{
		status = 0;
		if (err==0)
			retMessage = rm2;
	} else
	{
		retMessage = rm3 + "\r\n" + regName;
	}
}


////////////////////////////////////////////////////////////////

CSecLicDlg::CSecLicDlg(QWidget *parent, Qt::WFlags flags)
: QDialog(parent, flags)
{
	setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
	setWindowTitle("Registering...");

	statLabel = new QLabel("Registration", this);
	statLabel->setGeometry(10, 5, 210, 35);

	okBut = new QPushButton("Ok", this);
	okBut->setDisabled(true);
	okBut->setGeometry(80, 85, 70, 24);
	connect(okBut, SIGNAL(clicked()), this, SLOT(OnOk()));

	progressBar = new QProgressBar(this);
	progressBar->setGeometry(10, 50, 210, 22);
	progressBar->setRange(0, 60);
	progressBar->setValue(0);

	tmr = new QTimer();
	connect(tmr, SIGNAL(timeout()), this, SLOT(OnTmr()));

	int dw=230, dh=120;
	setMinimumSize(dw, dh);
	setMaximumSize(dw, dh);
	resize(dw, dh);
}


CSecLicDlg::~CSecLicDlg()
{
	tmr->stop();
	delete tmr;
}


void CSecLicDlg::startRegistration(CSecLic *liccl)
{
	progressBar->setValue(0);
	tmr->start(250);
	lic = liccl;
	
	exec();
}


void CSecLicDlg::OnTmr()
{
	int val = progressBar->value();

	statLabel->setText(lic->getStatusText());

	int stat = lic->getStatus();

	if ((val<60) && (stat!=0))
	{
		val++;
		progressBar->setValue(val);
	} else
	{
		progressBar->setValue(60);
		tmr->stop();
		okBut->setDisabled(false);
		if (val>=60) {
			QString retMessage = QString("Error 6: Network fault ") + QString("\r\nInternet connection unavailable");
			statLabel->setText(retMessage);
		}
	}

	if (stat == 1)
	{
		progressBar->setValue(60);
		tmr->stop();
		okBut->setDisabled(false);
	}

	update();
}


void CSecLicDlg::OnOk()
{
	accept();
}



///////////////////////////////////////////////////////////////

CDENDE16::CDENDE16()
{
	int j;
	for (j=0;j<=16;j++)	{
		cle[j]=0;
	}
	for (j=0;j<=8;j++)	{
		x1a0[j]=0;
	}

	pkax=0;
	pkbx=0;
	pkcx=0;
	pkdx=0;
	pksi=0;
	pktmp=0;
	x1a2=0;
	pkres=0;
	pki=0;
	inter=0;
	cfc=0;
	cfd=0;
	compte=0;
	pkc=0;

#ifdef _MSC_VER
	strcpy_s(key, 256, "0123456789ABCDEF");
#else
	strcpy(key, "0123456789ABCDEF");
#endif
}


void CDENDE16::pkfin()
{
	int j;
	for (j=0;j<=16;j++)	{
		cle[j]=0;
	}
	for (j=0;j<=8;j++)	{
		x1a0[j]=0;
	}

	pkax=0;
	pkbx=0;
	pkcx=0;
	pkdx=0;
	pksi=0;
	pktmp=0;
	x1a2=0;
	pkres=0;
	pki=0;
	inter=0;
	cfc=0;
	cfd=0;
	compte=0;
	pkc=0;

}
void CDENDE16::pkcode()
{

	pkdx=x1a2+pki;
	pkax=x1a0[pki];
	pkcx=0x015a;
	pkbx=0x4e35;

	pktmp=pkax;
	pkax=pksi;
	pksi=pktmp;

	pktmp=pkax;
	pkax=pkdx;
	pkdx=pktmp;

	if (pkax!=0)	{
		pkax=pkax*pkbx;
	}

	pktmp=pkax;
	pkax=pkcx;
	pkcx=pktmp;

	if (pkax!=0)	{
		pkax=pkax*pksi;
		pkcx=pkax+pkcx;
	}

	pktmp=pkax;
	pkax=pksi;
	pksi=pktmp;
	pkax=pkax*pkbx;
	pkdx=pkcx+pkdx;

	pkax++;

	x1a2=pkdx;
	x1a0[pki]=pkax;

	pkres=pkax^pkdx;
	pki++;
}
void CDENDE16::pkassemble(void)
{
	x1a0[0]= ( cle[0]*256 )+ cle[1];
	pkcode();
	inter=pkres;

	x1a0[1]= x1a0[0] ^ ( (cle[2]*256) + cle[3] );
	pkcode();
	inter=inter^pkres;

	x1a0[2]= x1a0[1] ^ ( (cle[4]*256) + cle[5] );
	pkcode();
	inter=inter^pkres;

	x1a0[3]= x1a0[2] ^ ( (cle[6]*256) + cle[7] );
	pkcode();
	inter=inter^pkres;


	x1a0[4]= x1a0[3] ^ ( (cle[8]*256) + cle[9] );
	pkcode();
	inter=inter^pkres;

	x1a0[5]= x1a0[4] ^ ( (cle[10]*256) + cle[11] );
	pkcode();
	inter=inter^pkres;

	x1a0[6]= x1a0[5] ^ ( (cle[12]*256) + cle[13] );
	pkcode();
	inter=inter^pkres;

	x1a0[7]= x1a0[6] ^ ( (cle[14]*256) + cle[15] );
	pkcode();
	inter=inter^pkres;

	pki=0;
}

void CDENDE16::ascii_encrypt128(char *in)
{
	int count, k=0;
	short pkd, pke;

	pkfin();

	for (count=0;count<16;count++) {
		cle[count]=key[count];
	}
	cle[count]='\0';

	ascCipherText = (char*)malloc(2*plainlen*sizeof(char)+1);
	for (count=0;count<=plainlen-1;count++) {
		pkc=in[count];

		pkassemble();
		cfc=inter>>8;
		cfd=inter&255;

		for (compte=0;compte<=15;compte++) {
			cle[compte]=cle[compte]^pkc;
		}
		pkc = pkc ^ (cfc^cfd);

		pkd =(pkc >> 4);
		pke =(pkc & 15);

		ascCipherText[k] = 0x61+pkd; k++;
		ascCipherText[k] = 0x61+pke; k++;
	}
	ascCipherText[k] = '\0';

}

void CDENDE16::ascii_decrypt128(char *in)
{
	int count, k=0;
	short pkd, pke;

	pkfin();

	for (count=0;count<16;count++) {
		cle[count]=key[count];
	}
	cle[count]='\0';

	plainText = (char*)malloc(ascipherlen/2*sizeof(char)+1);

	for (count=0;count<ascipherlen/2;count++) {
		pkd =in[k]; k++;
		pke =in[k]; k++;

		pkd=pkd-0x61;
		pkd=pkd<<4;

		pke=pke-0x61;
		pkc=pkd+pke;

		pkassemble();
		cfc=inter>>8;
		cfd=inter&255;

		pkc = pkc ^ (cfc^cfd);

		for (compte=0;compte<=15;compte++)
		{
			cle[compte]=cle[compte]^pkc;
		}
		plainText[count] = pkc;

	}
	plainText[count] = '\0';

}
