#ifndef REPORTTOOLLS_H
#define REPORTTOOLLS_H

#include "QObject"
#include "layoutmanagerdlg.h"

class CReportTools : public QObject
{
    Q_OBJECT
public:

    CReportTools(QObject *parent);

    QString htmlDoc;
    QString parImageCode;
    QString resPath;

    void startDoc(QString tit);
    void addHead(QString text, QColor color = Qt::black, QFont font = QFont(QString(), 12), QImage image = QImage());
    void addPar(QString text, QColor color, QFont font, QImage image = QImage());
    void addPar(QString text, QImage image = QImage());
    void addPar(QString defText, const repItem *item, int id);
    void addPlain(QString text);
    void addTabPar(QString text);
    void addImage(QString img);
    void startChap(bool clearImg = true);
    void endChap();
    void endDoc();
    void addStandItem(QString header, QString headerAdd, QString str, QString strAdd, int id, const repItem *item, bool noImg);
    QString addSplitParagraph(QString str, QString tagsOpen = QString(), QString tagClose = QString());

    static QString delHtmlHeaderTags(QString text);

    void preloadRes(QString rpath);
};

#endif // REPORTTOOLLS_H
