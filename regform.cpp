#include "regform.h"

#include <QDesktopServices>
#include <QUrl>
#include <QFileInfo>

extern QString qsGlobalDlgStyle;

CRegform::CRegform(QWidget *parent)
	: QDialog(parent)
{
    ui.setupUi(this);
	setWindowFlags( windowFlags() & ~Qt::WindowMinimizeButtonHint );
	setStyleSheet(qsGlobalDlgStyle);
	setWindowTitle("Registration");
    ui.textEdit->setReadOnly(true);
#ifdef Q_OS_MAC
    QString resPath = QFileInfo(QCoreApplication::applicationDirPath()).path() + "/Resources/" + QString("images/doc/");
#else
    QString resPath = QApplication::applicationDirPath()+QString("/images/doc/");
#endif
    //<img src=\"" + resPath + img + "\" style=\"margin-right: 8px;\" align=\"left\" />

    QString ptext = "<p>Upgrade your calculator now!<br /></p>\
<p><img src=\"" + resPath + "u03.png\" align=\"right\" />\
- Description for 30+ numerology numbers (40+ printed pages);<br /></p>\
<p><img src=\"" + resPath + "u02.png\" align=\"right\" />\
- Detailed Love compatibility reports;</p><p>&nbsp;</p>\
<p><img src=\"" + resPath + "u01.png\" align=\"right\" />\
- Forecast for any date (Personal Year, Month, Day; Transits and Essence Cycles);<br /></p>\
<p><img src=\"" + resPath + "u04.png\" align=\"right\" />\
- Graphical charts;</p>\
<p>- Interactive Guide;</p>\
<p>- Numerology manual and advices.</p>";

    ui.textEdit->setHtml(ptext);
}

CRegform::~CRegform()
{

}

void CRegform::on_pushButton_clicked()
{
	QDesktopServices::openUrl(QUrl(caddr, QUrl::TolerantMode));
}
